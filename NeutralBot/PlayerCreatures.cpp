#pragma once
#include "PlayerCreatures.h"
#include "Core\HunterCore.h"
#include <mutex>

Json::Value NaviPlayerCreatures::parse_class_to_json(){
	return BattleList::get()->parse_class_to_json();
}
void NaviPlayerCreatures::parse_json_to_class(Json::Value jsonValue){
	((std::mutex*)mtx_access)->lock();
	creatures_on_screen = jsonValue["creatures_on_screen"].asInt();
	current_creature_name = jsonValue["current_creature_name"].asString();

	current_creature_coordinate = Coordinate(jsonValue["current_creature_coordinate_x"].asInt(),
		jsonValue["current_creature_coordinate_y"].asInt(), jsonValue["current_creature_coordinate_z"].asInt());

	current_creature_is_monster = jsonValue["current_creature_is_monster"].asBool();
	current_creature_is_npc = jsonValue["current_creature_is_npc"].asBool();
	current_creature_is_player = jsonValue["current_creature_is_player"].asBool();
	have_player_on_screen = jsonValue["have_player_on_screen"].asBool();
	have_creature_on_screen = jsonValue["have_creature_on_screen"].asBool();
	is_trapped_by_creatures = jsonValue["is_trapped_by_creatures"].asBool();
	((std::mutex*)mtx_access)->unlock();
}
NaviPlayerCreatures::NaviPlayerCreatures(){
	mtx_access = (uint32_t*)new std::mutex();
}