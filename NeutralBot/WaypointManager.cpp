#include "WaypointManager.h"
#include "./Core/TibiaProcess.h"
#include <boost/lexical_cast.hpp>
#include "./Core/WaypointerCore.h"

WaypointManager::WaypointManager(){
	Json::Value json = loadJsonFile(".\\conf\\defaults\\actionsList.json");
	updateActionList(json);
} 

void WaypointManager::initVariables(){
	enabled = false;
	currentWaypointPathId = 0; 
	setcurrentWaypointInfoId(0);
	lastWaypointInfoId = 0;
	lastWaypointPathId = 0;
}

void WaypointManager::clear(){
	initVariables();
	waypointPathList.clear();
} 

std::shared_ptr<WaypointPath> WaypointManager::getCurrentWaypointPath(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);

	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second;
}

void WaypointManager::setCurrentWaypointPathByLabel(std::string label, int waypointInfoIndex){
	for (auto waypointPath : waypointPathList) {
		std::string label_ = string_util::lower(waypointPath.second->getName());

		if (label_ != string_util::lower(label))
			continue;

		lastWaypointPathId = currentWaypointPathId;
		currentWaypointPathId = waypointPath.first;

		setcurrentWaypointInfoId(waypointInfoIndex);
	}
}

void WaypointManager::setCurrentWaypointPathByLabel(std::string label, std::string waypointLabel){
	for (auto waypointPath : waypointPathList) {
		if (string_util::lower(waypointPath.second->getName()) != string_util::lower(label))
			continue;

		lastWaypointPathId = currentWaypointPathId;
		currentWaypointPathId = waypointPath.first;
		if (!waypointLabel.empty())
			setcurrentWaypointInfoId(waypointPath.second->getWaypointInfoIdByLabel(string_util::lower(waypointLabel)));
		else
			setcurrentWaypointInfoId(0);

		if (getcurrentWaypointInfoId() < 0)
			setcurrentWaypointInfoId(0);
	}
}

void WaypointManager::updateActionList(Json::Value jsonObject) {
	Json::Value componentArray = jsonObject["ComponentList"];
	Json::Value::Members membersList = componentArray.getMemberNames();

	for (auto members : membersList) {
		Json::Value componetInfoList = componentArray[members];
		std::map<int, std::shared_ptr<actionInfo>> actionsList;

		for (uint32_t index = 0; index < componetInfoList.size(); index++) {
			std::shared_ptr<actionInfo> newActionInfo(new actionInfo());
			Json::Value currentInfo = componetInfoList[index];

			newActionInfo->set_type(currentInfo["type"].asString());
			newActionInfo->set_id(currentInfo["id"].asString());
			newActionInfo->set_text(currentInfo["text"].asString());
			auto members = currentInfo["options"].getMemberNames();
			auto& options = currentInfo["options"];
			for (auto member : members){
				newActionInfo->add_option(member, options[member].asString());
			}
			actionsList[index] = newActionInfo;
		}

		this->actionInfoList[std::stoi(members)] = actionsList;
	}
}

std::map<int, std::shared_ptr<actionInfo>> WaypointManager::getActionInfoList(int key) {
	auto info = this->actionInfoList.find(key);

	if (info == this->actionInfoList.end())
		return std::map<int, std::shared_ptr<actionInfo>>();

	return info->second;
}

int WaypointManager::requestNewWaypointPathId(){
	int currentId = 0;

	while (waypointPathList.find(currentId) != waypointPathList.end())
		currentId++;

	waypointPathList[currentId] = std::shared_ptr<WaypointPath>(new WaypointPath);
	waypointPathList[currentId]->setName("New");
	return currentId;
}

void WaypointManager::setWaypointPath(int id, std::shared_ptr<WaypointPath> newWaypoint){
	auto waypointPath = waypointPathList.find(id);

	if (waypointPath != waypointPathList.end())
		waypointPath->second = newWaypoint;
}

std::shared_ptr<WaypointPath>WaypointManager::getLastWaypointPath(){
	auto waypointPath = waypointPathList.find(lastWaypointPathId);

	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second;
}

std::shared_ptr<WaypointPath> WaypointManager::getWaypointPathByLabel(std::string label){

	auto waypointPath = std::find_if(waypointPathList.begin(), waypointPathList.end(),
		[&](std::pair<int, std::shared_ptr<WaypointPath>> mIt){
		return _stricmp(&mIt.second->getName()[0], &label[0]) == 0;
	});

	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second;
}

std::shared_ptr<WaypointPath> WaypointManager::getWaypointPath(int id){
	auto waypointPath = waypointPathList.find(id);

	if (waypointPath == waypointPathList.end())
		return nullptr;

	if (!waypointPath->second)
		return nullptr;

	return waypointPath->second;
}

void WaypointManager::removeWaypointPath(int id){
	auto waypointPath = waypointPathList.find(id);

	if (waypointPath == waypointPathList.end())
		return;

	waypointPathList.erase(waypointPath);
}

void WaypointManager::removeWaypointPathByLabel(std::string label){
	for (auto infoIt = waypointPathList.begin(); infoIt != waypointPathList.end(); infoIt++){
		
		if (_stricmp(&infoIt->second->getName()[0], &label[0]) == 0){
			waypointPathList.erase(infoIt);
			return;
		}
	}
}

void WaypointManager::advanceCurrentWaypointInfo(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return;

	int nextWaypointInfoId = waypointPath->second->getNextWaypointInfoId(getcurrentWaypointInfoId());
	setcurrentWaypointInfoId(nextWaypointInfoId);
}

std::shared_ptr<WaypointPath> WaypointManager::get_waypoint_path_by_raw_pointer(WaypointPath* pointer){
	for (auto it = waypointPathList.begin(); it != waypointPathList.end(); it++){
		if (it->second.get() == pointer)
			return it->second;
	}
	return nullptr;
}

std::shared_ptr<WaypointInfo> WaypointManager::getCurrentWaypointInfo(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end()){
		if (waypointPathList.size()){
			waypointPath = waypointPathList.begin();
			currentWaypointPathId = waypointPath->first;
			setcurrentWaypointInfoId(0);
		}
		else{
			return nullptr;
		}
	}
	return waypointPath->second->getWaypointInfo(getcurrentWaypointInfoId());
}

std::shared_ptr<WaypointInfo> WaypointManager::getLastWaypointInfo(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second->getWaypointInfo(lastWaypointInfoId);
}

std::shared_ptr<WaypointInfo> WaypointManager::getBeforeWaypointInfo(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second->getBeforeWaypointInfo(getcurrentWaypointInfoId());
}

std::shared_ptr<WaypointInfo> WaypointManager::getBeforeLureWaypointInfo(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second->getBeforeWaypointInfoLure(getcurrentWaypointInfoId(), current_lure_recursive_index);
}

WaypointManager* WaypointManager::get(){
	static WaypointManager* mWaypointManager = nullptr;
	if (!mWaypointManager)
		mWaypointManager = new WaypointManager();
	return mWaypointManager;
}

bool WaypointManager::get_lua_stay_after_lure(){
	std::shared_ptr<WaypointInfo> before = getBeforeLureWaypointInfo();
	if (!before)
		return false;
	waypoint_t waypoint_type = before.get()->get_action();
	if (waypoint_t::waypoint_lure_here_distance == waypoint_type){
		return
			_stricmp(&before->get_additionalInfo("stayafterlure")[0], "True") == 0;
	}
	return false;
}

bool WaypointManager::get_lure_advance_reached(){
	std::shared_ptr<WaypointInfo> before = getBeforeLureWaypointInfo();
	if (!before)
		return false;
	waypoint_t waypoint_type = before.get()->get_action();
	if (waypoint_t::waypoint_lure_here_distance == waypoint_type){

		return
			_stricmp(&before->get_additionalInfo("advanceiflurereached")[0], "True") == 0;
	}
	return false;
}

bool WaypointManager::get_lure_recursive(){
	std::shared_ptr<WaypointInfo> before = getBeforeLureWaypointInfo();
	if (!before)
		return false;
	waypoint_t waypoint_type = before.get()->get_action();
	if (waypoint_t::waypoint_lure_here_distance == waypoint_type){
		return
			_stricmp(&before->get_additionalInfo("recursivenextlure")[0], "True") == 0;
	}
	return false;
}

uint32_t WaypointManager::get_lure_increase_sqm_lure(){
	std::shared_ptr<WaypointInfo> before = getBeforeLureWaypointInfo();
	if (!before)
		return 0;
	waypoint_t waypoint_type = before.get()->get_action();
	if (waypoint_t::waypoint_lure_here_distance == waypoint_type){
		std::string value = before->get_additionalInfo("increasecreaturesdistancesqm");
		int32_t int_value = 0;
		try{
			int_value = boost::lexical_cast<int32_t>(value);
		}
		catch (...){
		}
		return int_value;
	}
	return 0;
}

Coordinate WaypointManager::get_lure_coord(){
	std::shared_ptr<WaypointInfo> before = getBeforeLureWaypointInfo();
	if (!before)
		return Coordinate();
	waypoint_t waypoint_type = before.get()->get_action();
	if (waypoint_t::waypoint_lure_here_distance == waypoint_type || waypoint_t::waypoint_lure_here_knight_mode == waypoint_type){
		return before->get_destination_coord();
	}
	return Coordinate();
}


void WaypointManager::changeIndexWaypointPath(int pathId, int newPosition){
	lock_guard mtx_(mtx_access);

	std::map<int, std::shared_ptr<WaypointPath>> mapRetval;
	auto it = waypointPathList.find(pathId);
	if (it == waypointPathList.end())
		return;

	int oldPosition = it->first;
	if (newPosition == oldPosition)
		return;
	
	for (auto path = waypointPathList.begin(); path != waypointPathList.end(); path++){
		int current_index = path->first;
		if (current_index == oldPosition)
			continue;

		if (newPosition == 0){
			if (current_index < newPosition || current_index > oldPosition){
				mapRetval[current_index] = path->second;
				continue;
			}

			mapRetval[current_index + 1] = path->second;
			continue;
		}
		else if (newPosition == waypointPathList.size() - 1){
			if (current_index > newPosition || current_index < oldPosition){
				mapRetval[current_index] = path->second;
				continue;
			}

			mapRetval[current_index - 1] = path->second;
			continue;
		}
		else if (newPosition > oldPosition){
			if (current_index > newPosition || current_index < oldPosition){
				mapRetval[current_index] = path->second;
				continue;
			}

			mapRetval[current_index - 1] = path->second;
			continue;
		}
		else if (newPosition < oldPosition){
			if (current_index < newPosition || current_index > oldPosition){
				mapRetval[current_index] = path->second;
				continue;
			}

			mapRetval[current_index + 1] = path->second;
			continue;
		}
	}
	
	mapRetval[newPosition] = it->second;
	waypointPathList = mapRetval;
}
waypoint_t WaypointManager::getStringAsWaypoint(std::string alert_as_str){
	uint32_t retval_int = waypoint_t::waypoint_none;
	std::string reverse = GET_REVERSE_TR(alert_as_str);
	if (reverse.find("waypoint_t_") == std::string::npos)
		return (waypoint_t)retval_int;
	std::string num = reverse.substr(strlen("waypoint_t_"), reverse.length() - strlen("waypoint_t_"));
	if (num.length()){
		try{ retval_int = (waypoint_t)boost::lexical_cast<uint32_t>(num); }
		catch (...){}
	}

	return (waypoint_t)retval_int;
}

std::string WaypointManager::getWaypointAsString(waypoint_t type){
	return GET_TR("waypoint_t_" + std::to_string(type));
}

std::map<int, std::shared_ptr<WaypointPath>> WaypointManager::getwaypointPathList(){
	return waypointPathList;
}

std::shared_ptr<WaypointInfo> WaypointManager::getNextWapointInfo(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return nullptr;

	return waypointPath->second->getNextWaypointInfo(currentWaypointInfoId);
}

void WaypointManager::setCurrentWaypointInfo(std::string& label){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return;

	int waypointInfoId = waypointPath->second->getWaypointInfoIdByLabel(label);
	if (waypointInfoId < 0)
		return;

	lastWaypointInfoId = getcurrentWaypointInfoId();
	setcurrentWaypointInfoId(waypointInfoId);
}

void WaypointManager::setCurrentWaypointInfo(int index){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end())
		return;

	std::shared_ptr<WaypointInfo> waypointInfoId = waypointPath->second->getWaypointInfo(index);
	if (!waypointInfoId)
		return;

	lastWaypointInfoId = getcurrentWaypointInfoId();
	setcurrentWaypointInfoId(index);
}

void WaypointManager::increase_lure_recursive_index(){
	current_lure_recursive_index++;
}

void WaypointManager::advanceCurrentWaypointPath(){
	int currentPathId = currentWaypointPathId + 1;
	if (currentPathId > waypointPathList.size() - 1)
		currentPathId = 0;

	auto waypointPath = waypointPathList.find(currentPathId);
	if (waypointPath == waypointPathList.end()){
		lastWaypointPathId = currentWaypointPathId;
		currentWaypointPathId = 0;

		setcurrentWaypointInfoId(0);
		return;
	}

	lastWaypointPathId = currentWaypointPathId;
	currentWaypointPathId = waypointPath->first;

	setcurrentWaypointInfoId(0);
}
void WaypointManager::advanceCurrentWaypointInfoIgnoreLures(){
	auto waypointPath = waypointPathList.find(currentWaypointPathId);
	if (waypointPath == waypointPathList.end()){
		return;
	}
	setcurrentWaypointInfoId(waypointPath->second->getNextWaypointInfoIdIgnoreLure(getcurrentWaypointInfoId()));
}

int WaypointManager::getcurrentWaypointInfoId(){
	return currentWaypointInfoId;
}

void WaypointManager::setcurrentWaypointInfoId(int id){
	WaypointerCore::get()->on_waypoint_changed();
	current_lure_recursive_index = 0;
	currentWaypointInfoId = id;
}

Json::Value WaypointManager::parse_class_to_json(){
	Json::Value waypointClass;
	Json::Value waypointList;

	for (auto waypointPath : waypointPathList)
		waypointList[std::to_string(waypointPath.first)] = waypointPath.second->parse_class_to_json();	

	waypointClass["waypointPath"] = waypointList;
	return waypointClass;
}

void WaypointManager::parse_json_to_class(Json::Value jsonObject,int version) {
	if (!jsonObject["waypointPath"].empty() || !jsonObject["waypointPath"].isNull()){
		Json::Value waypointPathList = jsonObject["waypointPath"];
		Json::Value::Members membersList = waypointPathList.getMemberNames();

		for (auto members : membersList) {
			std::shared_ptr<WaypointPath> newWaypointPath(new WaypointPath());
			newWaypointPath->parse_json_to_class(waypointPathList[members],version);

			this->waypointPathList[std::stoi(members)] = newWaypointPath;
		}
	}
}

bool WaypointManager::is_require_lure(){
	std::shared_ptr<WaypointInfo> before = getBeforeWaypointInfo();
	if (!before)
		return false;
	waypoint_t waypoint_type = before.get()->get_action();
	if (waypoint_t::waypoint_lure_here_distance == waypoint_type){
		return true;
	}
	return false;
}

bool WaypointManager::is_location(uint32_t dist_near){
	auto current = getCurrentWaypointInfo();
	if (!current)
		return false;
	return is_location(current->get_position_x(), current->get_position_y(), current->get_position_z(), dist_near);
}

bool WaypointManager::is_location(uint32_t x, uint32_t y, int8_t z, uint32_t dist_near){
	auto self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (z != self_coord.z)
		return false;

	return self_coord.get_axis_max_dist(Coordinate(x, y, z)) <= dist_near;
}