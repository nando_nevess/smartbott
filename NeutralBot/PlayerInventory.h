#pragma once 
#include "PlayerInfo.h"

class NaviPlayerInventory{
public:
	TimeChronometer time_last_update;

	uint32_t* mtx_access;

	uint32_t body_helmet;
	uint32_t body_amulet;
	uint32_t body_bag;
	uint32_t body_armor;
	uint32_t body_shield;
	uint32_t body_weapon;
	uint32_t body_leg;
	uint32_t body_boot;
	uint32_t body_ring;
	uint32_t body_rope;
	uint32_t weapon_count;
	uint32_t ammunation_count;

	bool maximize;
	bool minimize;

	NaviPlayerInventory();

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonValue);
};

typedef std::shared_ptr<NaviPlayerInventory> NaviPlayerInventoryPtr;
