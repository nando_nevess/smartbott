function dectohex(num)
	return tonumber(num, 16);
end

lookAddressIdChar					= dectohex("0x6D102C")
lookAddressPrimeiroIdBattle 		= dectohex("0x72cc50")
--72cc50
--CB0
AddressSkullModeNew					= dectohex("0x3D889C")
AddressSkullModeOld					= dectohex("0x3DACC3")

--ok							--byte 1=not skull. byte 0=skull -- 41336 -- ok

AddressActualHotkeySchema 			= dectohex("0x51DC08")
AddressTreino						= dectohex("0x53361C")
XorAddress							= dectohex("0x533658")
AddressExp							= dectohex("0x533660")
AddressLvl							= dectohex("0x533670")
AddressMagicLvl						= dectohex("0x533678")
AddressMagicLvlPc					= dectohex("0x533680")
AddressTargetRed					= dectohex("0x533684")
AddressMp							= dectohex("0x533688")
AddressFirstPc						= dectohex("0x533690")
AddressHWND							= dectohex("0x533654")
AddressWhiteTarget					= dectohex("0x5336AC")
AddressStamina						= dectohex("0x5336BC")
AddressStatus						= dectohex("0x5336C4")-- 5336c4
lookAddressLogged					= dectohex("0x53370C")
AddressOfBpPointer					= dectohex("0x533970")
AddressOfWindow						= dectohex("0x53395C")
AdressEquip							= dectohex("0x533970")
AddressSoul							= dectohex("0x533674")
AddressReceivBuffer					= dectohex("0x533394")
--53497C
AddressMouseX1						= dectohex("0x5446D4")
AddressFollow						= dectohex("0x5438D0")
AddressCooldownBar					= dectohex("0x5447EA")
AddressTelaY						= dectohex("0x5451AC")
address_mouse						= dectohex("0x5451CC")
address_mouse_2						= address_mouse;

AddressTelaX						= dectohex("0x5451D8")

AddressServerMessage				= dectohex("0x586FF0")
AddressMessagePlayer				= AddressServerMessage
AddressMessageWarning				= dectohex("0x586DD0")
AddressMessageNotPossible			= AddressMessageWarning 
address_pointer_spells_basic		= dectohex("0x588A54")
address_pointer_spells				= address_pointer_spells_basic 	+ 8
address_total_spells				= address_pointer_spells 		+ 4
AddressExpHour						= dectohex("0x588AEC")

addressOfFirstMap					= dectohex("0x58CA08")
pointer_vip_players					= dectohex("0x6CDF00")
AddressBaseLogList					= dectohex("0x6AB838")
AddressCtrl							= dectohex("0x6CD180")
AddressSchemaHotkeyReference		= dectohex("0x6CD234")
address_item_to_be_used				= dectohex("0x6CD440")
AddressMouseX2						= dectohex("0x6CD458")
AddressMouseY2						= AddressMouseX2 + 4;
address_item_to_be_moved			= dectohex("0x6CD468")

AddressCap							= dectohex("0x6D101C")
AddressHp							= dectohex("0x6D1030")
AddressZGO							= dectohex("0x6D1034")
AddressFirst						= dectohex("0x6D1000")

AddressYGO							= dectohex("0x6D1020")
AddressMaxHp						= dectohex("0x6D1024")
AddressXGO							= dectohex("0x6D1028")

AddressX							= dectohex("0x6D1038")
AddressY							= AddressX + 4;
AddressZ							= dectohex("0x6D1040")

base_address_in_gui					= dectohex("0x773914")- dectohex("0xCC0")--
AddressHelmet						= dectohex("0x773A98")- dectohex("0xCB8")--
PointerInicioMap					= dectohex("0x773ADC")- dectohex("0xCB8")--
PointerInicioOffsetMap				= dectohex("0x7754B0")+ dectohex("0x24A4")--
--777954
address_bp_base_new					= dectohex("0x779E30")- dectohex("0xD5C")--
address_tibia_time					= dectohex("0x77A9C0")- dectohex("0xD9C")--
--779C24
AddressMouseY1						= AddressMouseX1 + 4
AddressMaxMp						= XorAddress + 4;


AddressClubPc					= AddressFirstPc + 4;
AddressSwordPc					= AddressFirstPc + 4 * 2;
AddressAxePc					= AddressFirstPc + 4 * 3;
AddressDistancePc				= AddressFirstPc + 4 * 4;
AddressShieldingPc				= AddressFirstPc + 4 * 5;
AddressFishingPc				= AddressFirstPc + 4 * 6;

AddressMouse_fix_x				= AddressMouseX1;
AddressMouse_fix_y				= AddressMouse_fix_x + 4;

AddressClub						= AddressFirst		+ 4;
AddressSword					= AddressFirst		+ 4 *2;
AddressAxe						= AddressFirst		+ 4 *3;
AddressDistance					= AddressFirst		+ 4 *4;
AddressShielding				= AddressFirst		+ 4 *5;
AddressFishing					= AddressFirst		+ 4 *6;

offset_beetwen_body_item		= dectohex("0x20");	

AddressAmulet					= AddressHelmet		- offset_beetwen_body_item * 1;
AddressBag						= AddressHelmet		- offset_beetwen_body_item * 2;
AddressMainBp					= AddressBag;
AddressArmor					= AddressHelmet		- offset_beetwen_body_item * 3;
AddressShield					= AddressHelmet		- offset_beetwen_body_item * 4;
AddressWeapon					= AddressHelmet		- offset_beetwen_body_item * 5;
AddressLeg						= AddressHelmet		- offset_beetwen_body_item * 6;
AddressBoot						= AddressHelmet		- offset_beetwen_body_item * 7;
AddressRing						= AddressHelmet		- offset_beetwen_body_item * 8;
AddressRope						= AddressHelmet		- offset_beetwen_body_item * 9;
AddressLogged					= lookAddressLogged;	
AddressIdChar					= lookAddressIdChar;
AddressPrimeiroNomeBattle		= lookAddressPrimeiroIdBattle + 4;




AddressAcc							= dectohex("0x5D1380")--ignore deprecated
AddressPass							= dectohex("0x5D138C")--ignore deprecated
AddressIndexChar					= dectohex("0x5D13BC")--ignore deprecated
