--Example declaring and registering class and method
--Register class for autocomplete
LuaLibrary.register_class("easy");

--register method in class for autocomplete
LuaLibrary.register_method("easy","example_method");

--Add class description if user need to know about it
LuaLibrary.add_table_description("easy", "this is an example of class library");

--Add class method description if user needs to know about it.
LuaLibrary.add_method_description("easy","example_method", "this is an example of method library");

--Add class method suggestion for autocomplete
LuaLibrary.add_method_suggestion("easy","example_method", "example_method(message_to_print)")

--declare global table for hold methods and variables for our class
easy = {};

--finally we can create and declare body of our method
function easy.example_method(message_to_print)
    if(message_to_print == nil) then
        print("hello world function");
    else
        print(message_to_print);
    end
end


--Example declaring and registering function
--Register function for autocomplete
LuaLibrary.register_function("easy_example_method")
--Add function description if user needs to know about.
LuaLibrary.add_function_description("example_method", "this is a example of function description");
--Add completion pre-wroten example
LuaLibrary.add_function_suggestion("example_function", "example_method(message_to_print)")
--finally create function declaration and body
function example_function(message_to_print)
    if(message_to_print == nil) then
        print("hello world function");
    else
        print(message_to_print);
    end
end
