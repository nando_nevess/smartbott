--Register class for autocomplete
LuaLibrary.register_class("kakatua");

--Add class description if user need to know about it
LuaLibrary.add_table_description("kakatua", "this is an example of class library");

--register method in class for autocomplete
LuaLibrary.register_method("kakatua","walk_around");
LuaLibrary.register_method("kakatua","get_points_rect");
LuaLibrary.register_method("kakatua","get_special_areas_near");
LuaLibrary.register_method("kakatua","check_player_on_screen");
LuaLibrary.register_method("kakatua","check_all_repot");
LuaLibrary.register_method("kakatua","check_stamina");
LuaLibrary.register_method("kakatua","check_cap");
LuaLibrary.register_method("kakatua","timer_start");
LuaLibrary.register_method("kakatua","timer_seconds_ellapsed");

--Add class method description if user needs to know about it.
LuaLibrary.add_method_description("kakatua","walk_around", "this is an example of method library");
LuaLibrary.add_method_description("kakatua","get_points_rect", "this is an example of method library");
LuaLibrary.add_method_description("kakatua","get_special_areas_near", "this is an example of method library");

--Add class method suggestion for autocomplete
LuaLibrary.add_method_suggestion("kakatua","walk_around", "walk_around(table_coord)")
LuaLibrary.add_method_suggestion("kakatua","check_stamina", "check_stamina(stamina, label_to_go)")
LuaLibrary.add_method_suggestion("kakatua","check_cap", "check_cap(min_cap, label_to_go)")
LuaLibrary.add_method_suggestion("kakatua","check_all_repot", "check_all_repot(label_to_go)")
LuaLibrary.add_method_suggestion("kakatua","check_player_on_screen", "check_player_on_screen(table_names,min_creature)")
LuaLibrary.add_method_suggestion("kakatua","get_points_rect", "get_points_rect(metatable specialarea)")
LuaLibrary.add_method_suggestion("kakatua","get_special_areas_near", "get_special_areas_near(table_names_coords)")


--declare global table for hold methods and variables for our class
kakatua = {
	num_to_go = 1,	
	start_time = 0
};


--finally we can create and declare body of our method
function kakatua.get_points_rect(specialarea)
	local rect = specialarea:get_rect();
	local width_sa = specialarea:get_width();
	local current_coord = Self.coordinate();
	local current_z = current_coord.z;

	local coord_one = {x = rect.x + width_sa ,y = rect.y + width_sa, z = current_z};
	local coord_two = {x = rect.x + width_sa ,y = rect.y + rect.height - width_sa, z = current_z};
	local coord_three={x = rect.x + rect.height - width_sa ,y = rect.y + rect.height - width_sa, z = current_z};
	local coord_four ={x = rect.x + rect.height - width_sa ,y = rect.y + width_sa, z = current_z};

	local local_coords = {
		coord_one,
		coord_two,
		coord_three,
		coord_four
	}

	return local_coords;
end

function kakatua.check_player_on_screen(list_char,min_creature)
    if Creatures.get_creatures_around(list_char,5) < min_creature then
        return false
    end
    
   return true;
end

function kakatua.get_special_areas_near(table_names_coords)
	local root = SpecialAreas.get_root_area();
	local specialareaslist = SABaseElement.get_childrens_by_names(table_names_coords)
	local specialarea;
	local last_dist = 999999;

	for _, Section in ipairs(specialareaslist) do
		local current_dist = Util.get_axis_min_dist(Section:get_center())
		if(current_dist < last_dist)then
			specialarea = Section;
			last_dist = current_dist;
		end
	end

	return specialarea;
end

function kakatua.walk_around(table_coord,use_diagonal)
	if kakatua.num_to_go > table.getn(table_coord) then
    	kakatua.num_to_go = 1
    end

	if(WaypointManager.is_location(table_coord[kakatua.num_to_go],1) == false) then
		Pathfinder.step_to_coordinate(table_coord[kakatua.num_to_go],use_diagonal);
		return false;
	else
		kakatua.num_to_go = kakatua.num_to_go +1
        return true;
	end
	
	return false;
end

function kakatua.check_all_depot(label_to_go)
	if Repoter.is_necesary_some_depot() then
  		WaypointManager.set_current_waypoint_by_label(label_to_go);
  		return true
	end
	return false
end

function kakatua.check_all_repot(label_to_go)
	if Repoter.is_necessary_all_repot() then
    	WaypointManager.set_current_waypoint_by_label(label_to_go);
    	return true
	end
	return false
end

function kakatua.check_stamina(stamina,label_to_go)
	local stam = stamina * 60

	if Self.stamina() <= stam then
    	WaypointManager.set_current_waypoint_by_label(label_to_go);
    	return true
	end
	return false
end

function kakatua.check_cap(min_cap,label_to_go)
	if Self.cap() <= min_cap then
	    WaypointManager.set_current_waypoint_by_label(label_to_go);
	    return true
	end
	return false
end

function kakatua.timer_start()
    kakatua.start_time = os.time()
end

function kakatua.timer_seconds_ellapsed()
    return os.time() - kakatua.start_time
end