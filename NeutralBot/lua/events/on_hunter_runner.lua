
cancel_event = false;
local comfirm_dist = -1;

local target_coord = nil;
local current_target = nil;

local current_target = Creatures.get_target();
if current_target == nil then
    return; 
end
 
target_coord = current_target.going_step;
if target_coord == nil then 
    target_coord = current_target.coordinate;
end

local current_coord = Self.get_going_step();
if(current_coord == nil) then
     current_coord = Self.coordinate();
end
    
local max_axis_dist = math.max(math.abs(target_coord.x - current_coord.x), math.abs(target_coord.y - current_coord.y));
local dist_to_keep = 3;
local dist_dif = dist_to_keep - max_axis_dist; 
local dist_abs_dif = math.abs(dist_dif);
if dist_dif > 0 then --[[Hurry the creature is scaping!!]]
    --[[Hey bot please dont call your internal function to keep distance let'me manage all here.]]
    cancel_event = true;
    --[[
        Settings passed to internal keep distance algorithm, danger is used by the function to know which sides are more dangeours and calculate to avoid them.
        param_type is an enum of types of possibles types of variables passed, 
    ]]
    keep_distance_params = 
    {
            --[[
            distance types takes extra argument position where it is
            ]]

        --[[
            creature arg, takes extra argument name of creatures that will be affected by this dangers.
        ]]
        {
        param_type = distance_param_row_t.distance_param_creature, danger = 1, name = "Dragon"
        }
    }

    HunterActions.keep_distance(target_coord, keep_distance_params);

    if dist_abs_dif <= 1 then
       HunterActions.wait_creature_out_of_range(current_target.id, dist_to_keep - 1, dist_to_keep + 1, math.min(500, Info.get_last_average_ping() * 2), true)
    end
    
elseif dist_dif < 0 then --[[Hurry the creature is getting us gogogogo!]]
     --[[Hey bot please dont call your internal function to keep distance let'me manage all here.]]
    cancel_event = true;
    --please try to go one step to creature direction.
    Pathfinder.step_to_coordinate(target_coord, true)
    cancel_event = true;
    if dist_abs_dif <= 1 then
       HunterActions.wait_creature_out_of_range(current_target.id, dist_to_keep - 1, dist_to_keep + 1, math.min(500, Info.get_last_average_ping() * 2), true)
    end
end
