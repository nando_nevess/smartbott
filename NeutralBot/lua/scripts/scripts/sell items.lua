function sell_all_items(...)               
    local items = ...
    while Actions.check_bp_is_open("Blue Backpack") do
        for i,j in pairs(items) do
            local itemId = ItemsManager.getItemIdFromName(items[i])
            local itemCount = Containers.get_item_count(itemId)
            
            if  itemCount > 0 then
                
                while Containers.get_item_count(itemId) > 0 do
                    NpcTrade.sell_item(itemId,100)
                    Thread.sleep(800)
                end 
            end
        end     
        Actions.up_backpack("Blue Backpack")
    end
end 

Actions.reach_creature("Rafzan")
Thread.sleep(1500)

local try_open = 0
nostack_list = {"Ratana","Leather Harness","Spike Shield","Life Preserver","Spiky Club"}

while NpcTrade.is_open() == false do
    if try_open > 3 then
        return
    end
    
    Repoter.open_trade()
    try_open = try_open + 1
end

sell_all_items(nostack_list)



