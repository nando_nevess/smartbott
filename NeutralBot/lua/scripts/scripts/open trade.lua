local HUDPos = 1

local creator = "Scripter Name"
local vocation = "Vocation"
local name = "Script Name"
local version = "1,0"


if not general_initiated then
    function expgained()
        if type(exp_earn) ~= "nnumber" or exp_earn < 1 or exp_name ~= Self.name() then
            exp_earn = Self.exp()
            exp_name = Self.name()
        end
    return Self.exp() - exp_earn
end

--SET INFOS
HudManager.set_font("Tahoma");
HudManager.set_color_border({a=255,r=255,g=255,b=255}, 1)
HudManager.set_font_border({a=255,r=255,g=255,b=255}, 1)
HudManager.set_font_size(int font_size)

