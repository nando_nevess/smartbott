local HUD_Colors = {
Font = HudManager.color(255, 255, 255, 0),
SectionHeaderBackground = {0.0, HudManager.color(170,36, 68, 105), 0.23, HudManager.color(170,39, 73,114), 0.76, HudManager.color(170,21, 39, 60)},
EntryNameBackground = {0.0, HudManager.color(170,75, 75, 75), 0.23, HudManager.color(170,45, 45, 45), 0.76, HudManager.color(170,19, 19, 19)},
EntryValueBackground = {0.0, HudManager.color(140,145, 95, 0), 0.23, HudManager.color(140,158, 104, 0), 0.76, HudManager.color(140,84, 55, 0)},
EntryValueEnabledBackground = {0.0, HudManager.color(140,65, 96, 12), 0.23, HudManager.color(140,67, 99, 13), 0.76, HudManager.color(140,36, 52, 6)},
EntryValueDisabledBackground = {0.0, HudManager.color(140,90, 12, 15), 0.23, HudManager.color(140,98, 13, 17), 0.76, HudManager.color(140,52, 6, 9)},
}

function firstToUpper(str)
    return (str:gsub("^%l", string.upper))
end

function comma_value(n)
    local left, num, right = string.match(n, '^([^%d]*%d)(%d*)(.-)$')
    return left..(num:reverse() : gsub('(%d%d%d)', '%1,') : reverse())..right
end

local positionYNeg = 15 + (18 * Info.monster_killer_count());

local positionY = HudManager.get_game_height() - positionYNeg;
local positionX = 2;

local StringWidth = HudManager.measure_string_width('TEMP')
local StringHeight = HudManager.measure_string_height('TEMP')

local rectSizeWidth = 240;
local rectSizeHeight = 20;

local rectTwoSizeWidth = 110;
local rectTwoSizeHeight = 16;

HudManager.set_font_style('Tahoma', 10,1, HudManager.color(255,255,255,255) , 2, HudManager.color(170,0,0,0))
HudManager.set_border_color(HudManager.color(220,0,0,0))

HudManager.set_font_size(11)

HudManager.add_grad_colors(unpack(HUD_Colors.SectionHeaderBackground))
HudManager.draw_round_rect(2, positionY, rectSizeWidth, rectSizeHeight, 3, 3)
HudManager.draw_text('Task Counter',3 ,positionY + 2)

HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueEnabledBackground))
HudManager.draw_round_rect(222, positionY, 20, rectSizeHeight, 3, 3)
HudManager.draw_text('X', 226, positionY + 2)

positionY = positionY + 22

HudManager.set_font_size(9)

local monster_killed = Info.tibia_messages_vector()
for key, MonsterEntry in pairs(monster_killed) do
    Notify.print_console(monster_killed[1])
end

















