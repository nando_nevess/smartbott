local HUD_Colors = {
Font = HudManager.color(255, 255, 255, 0),
SectionHeaderBackground = {0.0, HudManager.color(170,36, 68, 105), 0.23, HudManager.color(170,39, 73,114), 0.76, HudManager.color(170,21, 39, 60)},
EntryNameBackground = {0.0, HudManager.color(170,75, 75, 75), 0.23, HudManager.color(170,45, 45, 45), 0.76, HudManager.color(170,19, 19, 19)},
EntryValueBackground = {0.0, HudManager.color(140,145, 95, 0), 0.23, HudManager.color(140,158, 104, 0), 0.76, HudManager.color(140,84, 55, 0)},
EntryValueEnabledBackground = {0.0, HudManager.color(140,65, 96, 12), 0.23, HudManager.color(140,67, 99, 13), 0.76, HudManager.color(140,36, 52, 6)},
EntryValueDisabledBackground = {0.0, HudManager.color(140,90, 12, 15), 0.23, HudManager.color(140,98, 13, 17), 0.76, HudManager.color(140,52, 6, 9)},
}


function expgained()
    if type(exp_earn) ~= "number" or exp_earn < 1 or exp_name ~= Self.name() then
        exp_earn = Self.exp()
        exp_name = Self.name()
    end
    return Self.exp() - exp_earn
end 

function findweapontype()
    Skills = {
    distance = Self.distance(), 
    club = Self.club(), 
    axe = Self.axe(), 
    sword = Self.sword(), 
    magic = Self.mllevel()}
    higher_skill_lvl = 10
    higher_skill = ''
    
    for v, k in pairs(Skills) do
        if k > higher_skill_lvl then
            higher_skull_lvl = k
            higher_skill = v
        end
    end
    return higher_skill
end

function WeaponSkill()
    local SkillTypes =     {
    ['axe'] = {type = 'axe', skill = Self.axe(), skillpc = Self.axepc()},
    ['club'] = {type = 'club', skill = Self.club(), skillpc = Self.clubpc()},
    ['sword'] = {type = 'sword', skill = Self.sword(), skillpc = Self.swordpc()},
    ['distance'] = {type = 'distance', skill = Self.distance(), skillpc = Self.distancepc()},
    ['magic'] = {type = 'magic', skill = Self.mllevel(), skillpc = Self.mlevelpc()},
    }
    return SkillTypes[findweapontype()]
end

function comma_value(n)
    local left , num , right = string.match(n,'^([^%d]*%d)(%d*)(.-)$')
    return left..(num:reverse():gsub('(%d%d%d)','%1,'):reverse())..right
end

function SecondsToClock(sSeconds)
    local nSeconds = sSeconds
    if nSeconds == 0 then
        return "00:00:00";
    else
        nHours = string.format("%02.f", math.floor(nSeconds/3600));
        nMins = string.format("%02.f", math.floor(nSeconds/60 - (nHours*60)));
        nSecs = string.format("%02.f", math.floor(nSeconds - nHours*3600 - nMins *60));
        if nHours == '00' then
            return nMins..":"..nSecs
        elseif nHours == '00' and nMins == '00' then
            return nSecs
        else
            return nHours..":"..nMins..":"..nSecs
        end
    end
end

function timetolevel()
    if Info.exp_hour() > 0 then 
        return SecondsToClock((Info.exp_to_next_lvl()/Info.exp_hour())*60*60) 
    else 
        return '0'
    end
end

local HUD_Sections =         {
{Name = 'OTHERS', State = true, Items =  {
{'Ping', function() return Info.get_ping() .. '(avg: ' .. Info.get_last_average_ping() .. ')' end},
{'Bank Balance', function() return comma_value(Info.last_balance()) end}
}
},
{Name = 'CHARACTER STATS', State = true, Items =     {
{'Level', function() return Self.level() end}, --.. ' (' .. Self.level() .. '%)'},
{'Experience', function() return comma_value(Self.exp()) end},
{'Magic Level', function() return Self.mllevel() .. ' (' .. 100 - Self.mlevelpc() .. '%)' end},
{'Weapon Skill', function() return 0 .. ' (' .. 100 - 0 .. '%)' end},
{'Shielding', function() return Self.shielding() .. ' (' .. 100 - Self.shieldingpc() .. '%)' end},
{'Fishing', function() return Self.fishing() .. ' (' .. 100 - Self.fishingpc() .. '%)' end}
}        
},
{Name = 'ENGINE STATES', State = true, Items =     {
{'General Status', function() return BotManager.get_bot_state() end},
{'Spell Healer', function() return BotManager.get_spellcaster_state() end},
{'Lua', function() return BotManager.get_lua_state() end},
{'Cavebot',function() return BotManager.get_waypointer_state() end},
{'Looting', function() return BotManager.get_looter_state() end},
{'Targeting', function() return BotManager.get_hunter_state() end}
}
},
{Name = 'BOTTING STATS', State = true, Items =     {
{'Experience per Hour', function() return comma_value(Info.exp_hour()) end},
{'Experience Left', function() return comma_value(Info.exp_to_next_lvl()) end},
{'Experience Today', function() return comma_value(expgained()) end},
{'Time to Next Level', function() return timetolevel() end},
{'Played Time', function() return SecondsToClock(math.floor(BotManager.get_info_time_bot_running()/1000)) end},
{'Stamina', function() return SecondsToClock(Self.stamina()) end}
}
}
}
 
HudManager.set_font_style('Tahoma', 10, 1, HudManager.color(255,255,255,255) , 2, HudManager.color(160,0,0,0))
HudManager.set_border_color(HudManager.color(220,0,0,0))

local StringWidth = HudManager.measure_string_width('TEMP')
local StringHeight = HudManager.measure_string_height('TEMP') - 2

--setfillstyle('gradient', 'linear', 2, 0, 0, 0, 22)
HudManager.add_grad_colors(unpack(HUD_Colors.SectionHeaderBackground))
HudManager.draw_round_rect(0, 3, 240, 21, 3, 3)
HudManager.draw_text('SCRIPT INFO', 3, 21 / 2 - StringHeight * 0.5+3)
 
--setfillstyle('gradient', 'linear', 2, 0, 0, 0, 22)
HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueBackground))
HudManager.draw_round_rect(130, 3, 110, 21, 3, 3)
HudManager.draw_text('CREATOR NAME', 136, 21 / 2 - StringHeight * 0.5+3)
 
HudManager.set_font_size(9)
 
local StringWidth = HudManager.measure_string_width('TEMP')
local StringHeight = HudManager.measure_string_height('TEMP') - 1
 
HudManager.add_grad_colors(unpack(HUD_Colors.EntryNameBackground))
HudManager.draw_round_rect(0, 24 + 0 * 18+3, 240, 15, 3, 3)
HudManager.draw_text('Voc - Script Name', 3, 24 + 0 * 18 + 15 / 2 - StringHeight * 0.5 + 4)
 
HudManager.add_grad_colors(unpack(HUD_Colors.EntryNameBackground))
HudManager.draw_round_rect(0, 24 + 1 * 18+3, 240, 15, 3, 3)
HudManager.draw_text('Script version:', 3, 24 + 1 * 18 + 15 / 2 - StringHeight * 0.5 + 4)
 
HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueBackground))
HudManager.draw_round_rect(130, 24 + 1 * 18+3, 110, 15, 3, 3)
HudManager.draw_text('1.0', 136, 24 + 1 * 18 + 15 / 2 - StringHeight * 0.5 + 4)
 
local YPosition, SectionRow, SectionItemsRow = 22 + 2 * 19, 0, 0

for _, Section in ipairs(HUD_Sections) do
    HudManager.set_font_size(10)
    
    local StringWidth = HudManager.measure_string_width('TEMP')
    local StringHeight = HudManager.measure_string_height('TEMP') + 1
    
    HudManager.add_grad_colors(unpack(HUD_Colors.SectionHeaderBackground))
    HudManager.draw_round_rect(0, YPosition + (SectionRow * 23) + (SectionItemsRow * 19)+3, 240, 20, 3, 3)
    HudManager.draw_text(Section.Name, 3, YPosition + (SectionRow * 23) + (SectionItemsRow * 19) + 20 / 2 - StringHeight * 0.5 + 4)
    
    if (Section.State) then
        HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueEnabledBackground))
    else
        HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueDisabledBackground))
    end
    
    Section.StateSwitch = HudManager.draw_round_rect(220, YPosition + (SectionRow * 23) + (SectionItemsRow * 19+3), 20, 20, 3, 3)
    HudManager.draw_text('X', 225, YPosition + (SectionRow * 23) + (SectionItemsRow * 19) + 20 / 2 - StringHeight * 0.5 + 4)
    
    SectionRow = SectionRow + 1
    
    if (Section.State) then
        HudManager.set_font_size(9)
        
        local StringWidth = HudManager.measure_string_width('TEMP')
        local StringHeight = HudManager.measure_string_height('TEMP') + 1
        
        for _, SectionItem in ipairs(Section.Items) do  
            HudManager.add_grad_colors(unpack(HUD_Colors.EntryNameBackground))
            HudManager.draw_round_rect(0, YPosition + (SectionRow * 23) + (SectionItemsRow * 19)+3, 240, 16, 3, 3)
            HudManager.draw_text(SectionItem[1], 3, YPosition + (SectionRow * 23) + (SectionItemsRow * 19) + 16 / 2 - StringHeight * 0.5 + 4)
            
            if (Section.Name == 'ENGINE STATES') then
                local EngineCurrentState = SectionItem[2]()
                
                if (EngineCurrentState == true) then
                    HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueEnabledBackground))
                else
                    HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueDisabledBackground))
                end
                HudManager.draw_round_rect(130, YPosition + (SectionRow * 23) + (SectionItemsRow * 19)+3, 110, 16, 3, 3)
                HudManager.draw_text((EngineCurrentState == true and ('on')) or ('off'), 136, YPosition + (SectionRow * 23) + (SectionItemsRow * 19) + 16 / 2 - StringHeight * 0.5 + 4)
            else
                HudManager.add_grad_colors(unpack(HUD_Colors.EntryValueBackground))
                HudManager.draw_round_rect(130, YPosition + (SectionRow * 23) + (SectionItemsRow * 19)+3, 110, 16, 3, 3)
                HudManager.draw_text(SectionItem[2](), 136, YPosition + (SectionRow * 23) + (SectionItemsRow * 19) + 16 / 2 - StringHeight * 0.5 + 4)
            end
            
            SectionItemsRow = SectionItemsRow + 1
        end
    end
end
