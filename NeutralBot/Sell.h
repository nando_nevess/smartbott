#pragma once

#include "GeneralManager.h"
#include "DepoterManager.h"
#include "SellManager.h"
#include "SellCore.h"
#include "ManagedUtil.h"
#include "AutoGenerateLoot.h"
#include "LanguageManager.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Sell : public Telerik::WinControls::UI::RadForm{
	public:	Sell(void){
		InitializeComponent();
		this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
		NeutralBot::FastUIPanelController::get()->install_controller(this);
	}

	protected:	~Sell(){
		unique = nullptr;
		if (components)
			delete components;
	}
	public: static void CloseUnique(){
		if (unique)unique->Close();
	}
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_item_price;
	private: Telerik::WinControls::UI::RadDropDownList^  box_item_name;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_item_qty;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadButton^  bt_delete;
	private: Telerik::WinControls::UI::RadButton^  bt_new;
	private: Telerik::WinControls::UI::RadMenu^  radMenu1;
	private: Telerik::WinControls::UI::RadMenuItem^  menu;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_save;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_load;
	public: Telerik::WinControls::UI::RadButton^  bt_auto_generate;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	public: Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
	private:
	public: Telerik::WinControls::UI::RadLabel^  radLabel5;
	public: Telerik::WinControls::UI::RadDropDownList^  box_backpack;
	protected: System::Windows::Forms::PictureBox^  pictureBoxDepotBox;
	public:
	public:
	private:
	private:
	public:
	protected: static Sell^ unique;

	public: static void ReloadForm(){
		unique->Sell_Load(nullptr, nullptr);
	}
	public: static void ShowUnique(){
		if (unique == nullptr)
			unique = gcnew Sell();

		unique->BringToFront();
		unique->Show();
	}
	public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew Sell();
	}

	private: Telerik::WinControls::UI::RadPageView^  PageSell;
	protected:
	protected:
	protected:
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void){
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem5 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem6 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem7 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem8 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem9 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem10 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem11 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem12 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem13 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem14 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem15 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem16 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem17 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem18 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 this->PageSell = (gcnew Telerik::WinControls::UI::RadPageView());
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_item_qty = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_item_price = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->box_item_name = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->bt_delete = (gcnew Telerik::WinControls::UI::RadButton());
				 this->bt_new = (gcnew Telerik::WinControls::UI::RadButton());
				 this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				 this->menu = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->bt_save = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->bt_load = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->bt_auto_generate = (gcnew Telerik::WinControls::UI::RadButton());
				 this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->box_backpack = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->pictureBoxDepotBox = (gcnew System::Windows::Forms::PictureBox());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageSell))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_item_qty))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_item_price))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_item_name))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_new))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_auto_generate))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
				 this->radGroupBox3->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_backpack))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDepotBox))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // PageSell
				 // 
				 this->PageSell->Location = System::Drawing::Point(0, 17);
				 this->PageSell->Name = L"PageSell";
				 this->PageSell->PageBackColor = System::Drawing::Color::Transparent;
				 this->PageSell->Size = System::Drawing::Size(337, 252);
				 this->PageSell->TabIndex = 0;
				 this->PageSell->Text = L"PageSell";
				 this->PageSell->ThemeName = L"ControlDefault";
				 this->PageSell->NewPageRequested += gcnew System::EventHandler(this, &Sell::PageSell_NewPageRequested);
				 this->PageSell->PageRemoved += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEventArgs^ >(this, &Sell::PageSell_PageRemoved);
				 this->PageSell->SelectedPageChanged += gcnew System::EventHandler(this, &Sell::PageSell_SelectedPageChanged);
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageSell->GetChildAt(0)))->NewItemVisibility = Telerik::WinControls::UI::StripViewNewItemVisibility::End;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageSell->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageSell->GetChildAt(0)))->ItemFitMode = Telerik::WinControls::UI::StripViewItemFitMode::None;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageSell->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->radLabel4);
				 this->radGroupBox1->Controls->Add(this->spin_item_qty);
				 this->radGroupBox1->Controls->Add(this->radLabel3);
				 this->radGroupBox1->Controls->Add(this->radLabel2);
				 this->radGroupBox1->Controls->Add(this->radLabel1);
				 this->radGroupBox1->Controls->Add(this->spin_item_price);
				 this->radGroupBox1->Controls->Add(this->box_item_name);
				 this->radGroupBox1->HeaderText = L"";
				 this->radGroupBox1->Location = System::Drawing::Point(343, 168);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(285, 66);
				 this->radGroupBox1->TabIndex = 1;
				 // 
				 // radLabel4
				 // 
				 this->radLabel4->Location = System::Drawing::Point(5, 62);
				 this->radLabel4->Name = L"radLabel4";
				 this->radLabel4->Size = System::Drawing::Size(2, 2);
				 this->radLabel4->TabIndex = 13;
				 // 
				 // spin_item_qty
				 // 
				 this->spin_item_qty->Location = System::Drawing::Point(207, 36);
				 this->spin_item_qty->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1215752191, 23, 0, 0 });
				 this->spin_item_qty->Name = L"spin_item_qty";
				 this->spin_item_qty->Size = System::Drawing::Size(73, 20);
				 this->spin_item_qty->TabIndex = 2;
				 this->spin_item_qty->TabStop = false;
				 this->spin_item_qty->ValueChanged += gcnew System::EventHandler(this, &Sell::spin_item_qty_ValueChanged);
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(153, 37);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(48, 18);
				 this->radLabel3->TabIndex = 6;
				 this->radLabel3->Text = L"item qty";
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(5, 37);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(56, 18);
				 this->radLabel2->TabIndex = 5;
				 this->radLabel2->Text = L"Item price";
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(5, 10);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(60, 18);
				 this->radLabel1->TabIndex = 4;
				 this->radLabel1->Text = L"Item name";
				 // 
				 // spin_item_price
				 // 
				 this->spin_item_price->Location = System::Drawing::Point(71, 36);
				 this->spin_item_price->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1215752191, 23, 0, 0 });
				 this->spin_item_price->Name = L"spin_item_price";
				 this->spin_item_price->Size = System::Drawing::Size(73, 20);
				 this->spin_item_price->TabIndex = 1;
				 this->spin_item_price->TabStop = false;
				 this->spin_item_price->ValueChanged += gcnew System::EventHandler(this, &Sell::spin_item_price_ValueChanged);
				 // 
				 // box_item_name
				 // 
				 this->box_item_name->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->box_item_name->Location = System::Drawing::Point(71, 10);
				 this->box_item_name->Name = L"box_item_name";
				 this->box_item_name->Size = System::Drawing::Size(209, 20);
				 this->box_item_name->TabIndex = 0;
				 this->box_item_name->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Sell::box_item_name_SelectedIndexChanged);
				 // 
				 // bt_delete
				 // 
				 this->bt_delete->Location = System::Drawing::Point(343, 138);
				 this->bt_delete->Name = L"bt_delete";
				 this->bt_delete->Size = System::Drawing::Size(285, 24);
				 this->bt_delete->TabIndex = 2;
				 this->bt_delete->Text = L"Remove";
				 this->bt_delete->Click += gcnew System::EventHandler(this, &Sell::bt_delete_Click);
				 // 
				 // bt_new
				 // 
				 this->bt_new->Location = System::Drawing::Point(343, 108);
				 this->bt_new->Name = L"bt_new";
				 this->bt_new->Size = System::Drawing::Size(285, 24);
				 this->bt_new->TabIndex = 3;
				 this->bt_new->Text = L"New";
				 this->bt_new->Click += gcnew System::EventHandler(this, &Sell::bt_new_Click);
				 // 
				 // radMenu1
				 // 
				 this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->menu });
				 this->radMenu1->Location = System::Drawing::Point(0, 0);
				 this->radMenu1->Name = L"radMenu1";
				 this->radMenu1->Size = System::Drawing::Size(631, 20);
				 this->radMenu1->TabIndex = 4;
				 this->radMenu1->Text = L"radMenu1";
				 // 
				 // menu
				 // 
				 this->menu->AccessibleDescription = L"radMenuItem1";
				 this->menu->AccessibleName = L"radMenuItem1";
				 this->menu->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->bt_save, this->bt_load });
				 this->menu->Name = L"menu";
				 this->menu->Text = L"Menu";
				 // 
				 // bt_save
				 // 
				 this->bt_save->AccessibleDescription = L"radMenuItem2";
				 this->bt_save->AccessibleName = L"radMenuItem2";
				 this->bt_save->Name = L"bt_save";
				 this->bt_save->PopupDirection = Telerik::WinControls::UI::RadDirection::Right;
				 this->bt_save->Text = L"Save";
				 this->bt_save->Click += gcnew System::EventHandler(this, &Sell::bt_save_Click);
				 // 
				 // bt_load
				 // 
				 this->bt_load->AccessibleDescription = L"radMenuItem3";
				 this->bt_load->AccessibleName = L"radMenuItem3";
				 this->bt_load->Name = L"bt_load";
				 this->bt_load->Text = L"Load";
				 this->bt_load->Click += gcnew System::EventHandler(this, &Sell::bt_load_Click);
				 // 
				 // bt_auto_generate
				 // 
				 this->bt_auto_generate->Location = System::Drawing::Point(343, 240);
				 this->bt_auto_generate->Name = L"bt_auto_generate";
				 this->bt_auto_generate->Size = System::Drawing::Size(285, 24);
				 this->bt_auto_generate->TabIndex = 11;
				 this->bt_auto_generate->Text = L"Auto Generate Depot List";
				 this->bt_auto_generate->ThemeName = L"ControlDefault";
				 this->bt_auto_generate->Click += gcnew System::EventHandler(this, &Sell::bt_auto_generate_Click);
				 // 
				 // radGroupBox3
				 // 
				 this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox3->Controls->Add(this->radLabel5);
				 this->radGroupBox3->Controls->Add(this->box_backpack);
				 this->radGroupBox3->Controls->Add(this->pictureBoxDepotBox);
				 this->radGroupBox3->HeaderText = L"";
				 this->radGroupBox3->Location = System::Drawing::Point(343, 40);
				 this->radGroupBox3->Name = L"radGroupBox3";
				 this->radGroupBox3->Size = System::Drawing::Size(285, 62);
				 this->radGroupBox3->TabIndex = 26;
				 this->radGroupBox3->ThemeName = L"ControlDefault";
				 // 
				 // radLabel5
				 // 
				 this->radLabel5->Location = System::Drawing::Point(69, 6);
				 this->radLabel5->Name = L"radLabel5";
				 this->radLabel5->Size = System::Drawing::Size(87, 18);
				 this->radLabel5->TabIndex = 25;
				 this->radLabel5->Text = L"Backpack Loot : ";
				 this->radLabel5->ThemeName = L"ControlDefault";
				 // 
				 // box_backpack
				 // 
				 this->box_backpack->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 radListDataItem1->Text = L"Depot Chest";
				 radListDataItem2->Text = L"Depot Box I";
				 radListDataItem3->Text = L"Depot Box II";
				 radListDataItem4->Text = L"Depot Box III";
				 radListDataItem5->Text = L"Depot Box IV";
				 radListDataItem6->Text = L"Depot Box V";
				 radListDataItem7->Text = L"Depot Box VI";
				 radListDataItem8->Text = L"Depot Box VII";
				 radListDataItem9->Text = L"Depot Box VIII";
				 radListDataItem10->Text = L"Depot Box IX";
				 radListDataItem11->Text = L"Depot Box X";
				 radListDataItem12->Text = L"Depot Box XI";
				 radListDataItem13->Text = L"Depot Box XII";
				 radListDataItem14->Text = L"Depot Box XIII";
				 radListDataItem15->Text = L"Depot Box XIV";
				 radListDataItem16->Text = L"Depot Box XV";
				 radListDataItem17->Text = L"Depot Box XVI";
				 radListDataItem18->Text = L"Depot Box XVII";
				 this->box_backpack->Items->Add(radListDataItem1);
				 this->box_backpack->Items->Add(radListDataItem2);
				 this->box_backpack->Items->Add(radListDataItem3);
				 this->box_backpack->Items->Add(radListDataItem4);
				 this->box_backpack->Items->Add(radListDataItem5);
				 this->box_backpack->Items->Add(radListDataItem6);
				 this->box_backpack->Items->Add(radListDataItem7);
				 this->box_backpack->Items->Add(radListDataItem8);
				 this->box_backpack->Items->Add(radListDataItem9);
				 this->box_backpack->Items->Add(radListDataItem10);
				 this->box_backpack->Items->Add(radListDataItem11);
				 this->box_backpack->Items->Add(radListDataItem12);
				 this->box_backpack->Items->Add(radListDataItem13);
				 this->box_backpack->Items->Add(radListDataItem14);
				 this->box_backpack->Items->Add(radListDataItem15);
				 this->box_backpack->Items->Add(radListDataItem16);
				 this->box_backpack->Items->Add(radListDataItem17);
				 this->box_backpack->Items->Add(radListDataItem18);
				 this->box_backpack->Location = System::Drawing::Point(69, 30);
				 this->box_backpack->Name = L"box_backpack";
				 this->box_backpack->Size = System::Drawing::Size(210, 20);
				 this->box_backpack->TabIndex = 26;
				 this->box_backpack->ThemeName = L"ControlDefault";
				 this->box_backpack->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Sell::box_backpack_SelectedIndexChanged);
				 // 
				 // pictureBoxDepotBox
				 // 
				 this->pictureBoxDepotBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				 this->pictureBoxDepotBox->Location = System::Drawing::Point(9, 6);
				 this->pictureBoxDepotBox->Name = L"pictureBoxDepotBox";
				 this->pictureBoxDepotBox->Size = System::Drawing::Size(54, 50);
				 this->pictureBoxDepotBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				 this->pictureBoxDepotBox->TabIndex = 24;
				 this->pictureBoxDepotBox->TabStop = false;
				 // 
				 // Sell
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(631, 271);
				 this->Controls->Add(this->radGroupBox3);
				 this->Controls->Add(this->bt_auto_generate);
				 this->Controls->Add(this->radMenu1);
				 this->Controls->Add(this->bt_new);
				 this->Controls->Add(this->bt_delete);
				 this->Controls->Add(this->radGroupBox1);
				 this->Controls->Add(this->PageSell);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->MaximizeBox = false;
				 this->Name = L"Sell";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->Text = L"Sell";
				 this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Sell::Sell_FormClosing);
				 this->Load += gcnew System::EventHandler(this, &Sell::Sell_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageSell))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_item_qty))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_item_price))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_item_name))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_new))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_auto_generate))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
				 this->radGroupBox3->ResumeLayout(false);
				 this->radGroupBox3->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_backpack))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDepotBox))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion

			 //VARIAVES
			 bool disable_item_update;
			 String^ valueOld;

			 //EVENTOS
	private: System::Void PageSell_NewPageRequested(System::Object^ sender, System::EventArgs^  e) {
		creatNewPage(gcnew String(SellManager::get()->request_new_id().c_str()));
		close_button();
	}
	private: System::Void bt_new_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!PageSell->SelectedPage)
			return;

		addItemNewOnView("new", 0, 0);
		disable_group();
	}
	private: System::Void ListViewSell_ItemChanged(System::Object^ sender, System::EventArgs^  e) {
		updateSelectedData();
	}
	private: System::Void ListViewSell_ItemRemoving(System::Object^ sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
		Telerik::WinControls::UI::RadListView^ radListView = getItemListView();

		if (!radListView){
			e->Cancel = true;
			return;
		}

		Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;

		if (!itemRow){
			e->Cancel = true;
			return;
		}

		std::shared_ptr<SellItems> sellItems = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

		if (!sellItems){
			e->Cancel = true;
			return;
		}

		sellItems->removeItemStruct(managed_util::fromSS(itemRow->Text));

	}
	private: System::Void PageSell_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e) {
		SellManager::get()->removeSellItems(managed_util::fromSS(e->Page->Text));
		close_button();
	}
	private: System::Void bt_delete_Click(System::Object^  sender, System::EventArgs^  e) {
		Telerik::WinControls::UI::RadListView^ radListView = getItemListView();

		if (!radListView)
			return;

		Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;

		if (!itemRow)
			return;

		std::shared_ptr<SellItems> sellItems = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

		if (!sellItems)
			return;

		radListView->Items->Remove(itemRow);
		sellItems->removeItemStruct(managed_util::fromSS(itemRow->Text));
		disable_group();
	}
	private: System::Void Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e){
		valueOld = e->Value->ToString();
	}
	private: System::Void PageRename_TextChanged(System::Object^ sender, System::EventArgs^ e){
		String^ newValue = PageSell->SelectedPage->Text;
		String^ oldValue = valueOld;

		if (newValue == "" || oldValue == "")
			return;

		if (newValue == oldValue)
			return;

		int count = 0;
		for each(auto items in PageSell->Pages){
			if (items->Text == newValue){
				if (count >= 2){
					PageSell->SelectedPage->Text = oldValue;
					return;
				}
				count++;
			}
		}

		std::string newName = managed_util::to_std_string(newValue);
		std::string oldName = managed_util::to_std_string(oldValue);

		if (!SellManager::get()->changeSellItems(newName, oldName)){
			PageSell->SelectedPage->Text = oldValue;
			return;
		}

		valueOld = newValue;

		array<Control^>^ getRadListView = PageSell->SelectedPage->Controls->Find("list_" + oldValue, true);

		if (getRadListView->Length == 0)
			return;

		Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
		radListView->Name = "list_" + newValue;
	}
	private: System::Void box_item_name_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
		if (disable_item_update)
			return;

		Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

		if (!radListview)
			return;

		if (!radListview->SelectedItem)
			return;

		Telerik::WinControls::UI::ListViewDataItem^ radItem = (Telerik::WinControls::UI::ListViewDataItem^)radListview->SelectedItem;
		radListview->SelectedItem[0] = box_item_name->Text;
		radListview->SelectedItem->Image = managed_util::get_item_image(box_item_name->Text, 20);

		std::shared_ptr<SellItems> srd_bp = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

		if (!srd_bp)
			return;

		std::shared_ptr<ItemStruct> temp = srd_bp->getItemStruct(managed_util::fromSS(radListview->SelectedItem->Text));

		if (!temp)
			return;

		temp->set_item_name(managed_util::fromSS(box_item_name->Text));
	}
	private: System::Void spin_item_price_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (disable_item_update)
			return;

		Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

		if (!radListview)
			return;

		if (!radListview->SelectedItem)
			return;

		std::shared_ptr<SellItems> srd_bp = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

		if (!srd_bp)
			return;

		std::shared_ptr<ItemStruct> temp = srd_bp->getItemStruct(managed_util::fromSS(radListview->SelectedItem->Text));

		if (!temp)
			return;

		temp->set_item_price(Convert::ToInt32(spin_item_price->Value));
	}
	private: System::Void spin_item_qty_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (disable_item_update)
			return;

		Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

		if (!radListview)
			return;

		if (!radListview->SelectedItem)
			return;

		std::shared_ptr<SellItems> srd_bp = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

		if (!srd_bp)
			return;

		std::shared_ptr<ItemStruct> temp = srd_bp->getItemStruct(managed_util::fromSS(radListview->SelectedItem->Text));

		if (!temp)
			return;

		temp->set_item_qty(Convert::ToInt32(spin_item_qty->Value));
	}
	private: System::Void Sell_Load(System::Object^  sender, System::EventArgs^  e) {
		std::vector<std::string> arrayitems = ItemsManager::get()->get_itens();
		box_item_name->BeginUpdate();
		for (auto item : arrayitems)
			box_item_name->Items->Add(gcnew String(item.c_str()));
		box_item_name->EndUpdate();

		PageSell->ViewElement->AllowEdit = true;
		
		loadAll();
		update_idiom();

		disable_item_update = true;
		std::vector<std::string> arraycontainer = ItemsManager::get()->get_containers();
		box_backpack->BeginUpdate();
		for (auto item : arraycontainer)
			box_backpack->Items->Add(gcnew String(item.c_str()));
		box_backpack->EndUpdate();
		disable_item_update = false;

		close_button();
		createEvent();
		disable_group();
	}
	private: System::Void ListViewSell_ItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
		disable_group();
	}
	private: System::Void bt_save_Click(System::Object^  sender, System::EventArgs^  e) {
		System::Windows::Forms::SaveFileDialog fileDialog;
		fileDialog.Filter = "Sell Script|*.Sell";
		fileDialog.Title = "Save Script";

		if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
			return;

		if (!save_script(managed_util::fromSS(fileDialog.FileName)))
			Telerik::WinControls::RadMessageBox::Show(this, "Error Saved", "Save/Load");
		else
			Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved", "Save/Load");
	}
	private: System::Void bt_load_Click(System::Object^  sender, System::EventArgs^  e) {
		System::Windows::Forms::OpenFileDialog fileDialog;
		fileDialog.Filter = "Sell Script|*.Sell";
		fileDialog.Title = "Open Script";

		if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
			return;

		PageSell->Pages->Clear();
		SellManager::get()->clear();

		if (!load_script(managed_util::fromSS(fileDialog.FileName)))
			Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
		else
			Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

		loadAll();
	}
	private: System::Void PageSell_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 if (!PageSell->SelectedPage)
					 return;

				 std::shared_ptr<SellItems> sellItem = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));
				 if (!sellItem)
					 return;

				 String^ backpack_name = gcnew String(sellItem->get_backpack_loot().c_str());

				 disable_item_update = true;
				 box_backpack->Text = backpack_name;
				 disable_item_update = false;




				 updateSelectedData();
				 disable_group();
	}

			 //FUN��ES
			 System::Void createEvent(){
				 PageSell->ViewElement->AllowEdit = true;
				 PageSell->ViewElement->EditorInitialized += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEditorEventArgs^>(this, &Sell::Page_EditorInitialized);
				 PageSell->SelectedPage->TextChanged += gcnew System::EventHandler(this, &Sell::PageRename_TextChanged);
			 }
			 System::Void creatNewPage(String^ PageNameId){
				 Telerik::WinControls::UI::RadPageViewPage^ radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
				 Telerik::WinControls::UI::RadListView^ radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
				 Telerik::WinControls::UI::RadDropDownList^ dropDownList = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0", L"Item"));

				 // radPageViewPage1
				 radPageViewPage1->Controls->Add(radListView1);
				 radPageViewPage1->ItemSize = System::Drawing::SizeF(39, 28);
				 radPageViewPage1->Location = System::Drawing::Point(10, 37);
				 radPageViewPage1->Name = PageNameId;
				 radPageViewPage1->Size = System::Drawing::Size(773, 392);
				 radPageViewPage1->Text = PageNameId;

				 // radListView1
				 listViewDetailColumn7->HeaderText = L"Item";
				 listViewDetailColumn7->Width = 312;
				 listViewDetailColumn7->MaxWidth = 312;
				 listViewDetailColumn7->MinWidth = 312;
				 radListView1->AutoScroll = false;
				 radListView1->AllowEdit = false;
				 radListView1->ShowGridLines = true;
				 radListView1->Columns->AddRange(gcnew cli::array<Telerik::WinControls::UI::ListViewDetailColumn^>(1) { listViewDetailColumn7 });
				 radListView1->ItemSpacing = -1;
				 radListView1->Location = System::Drawing::Point(0, 0);
				 radListView1->Name = "list_" + Convert::ToString(PageNameId);
				 radListView1->Size = System::Drawing::Size(312, 180);
				 radListView1->TabIndex = 0;
				 radListView1->Text = L"";
				 radListView1->Dock = System::Windows::Forms::DockStyle::Fill;
				 radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				 radListView1->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Sell::ListViewSell_ItemRemoving);
				 radListView1->SelectedItemChanged += gcnew System::EventHandler(this, &Sell::ListViewSell_ItemChanged);
				 radListView1->ItemRemoved += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &Sell::ListViewSell_ItemRemoved);
				 radListView1->ListViewElement->ItemSize = System::Drawing::Size(radListView1->ListViewElement->ItemSize.Width, 25);

				 PageSell->Controls->Add(radPageViewPage1);
			 }
			 System::Void updateSelectedData(){
				 if (disable_item_update)
					 return;

				 if (!PageSell->SelectedPage)
					 return;

				 Telerik::WinControls::UI::RadListView^ listView = getItemListView();

				 if (!listView->SelectedItem)
					 return;

				 std::shared_ptr<SellItems> sellItem = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

				 if (!sellItem)
					 return;

				 std::string temp = managed_util::fromSS(listView->SelectedItem->Text);
				 std::shared_ptr<ItemStruct> itemStruct = sellItem->getItemStruct(temp);

				 if (!itemStruct)
					 return;

				 String^ ItemName = gcnew String(itemStruct->get_item_name().c_str());
				 uint32_t ItemPrice = itemStruct->get_item_price();
				 uint32_t ItemQty = itemStruct->get_item_qty();

				 disable_item_update = true;
				 box_item_name->Text = ItemName;
				 spin_item_price->Value = ItemPrice;
				 spin_item_qty->Value = ItemQty;
				 disable_item_update = false;
			 }
			 System::Void close_button(){
				 if (PageSell->Pages->Count <= 1)
					 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageSell->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
				 else
					 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageSell->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
			 }
			 Telerik::WinControls::UI::RadListView^ getItemListView(){
				 auto selected_page = this->PageSell->SelectedPage;
				 if (!selected_page)
					 return nullptr;

				 array<Control^>^ getRadListView = selected_page->Controls->Find("list_" + PageSell->SelectedPage->Name, true);
				 if (!getRadListView->Length)
					 return nullptr;

				 return (Telerik::WinControls::UI::RadListView^) getRadListView[0];
			 }
			 System::Void addItemNewOnView(std::string ItemName, int Item_price, int Item_qty){
				 Telerik::WinControls::UI::RadListView^ radListView = getItemListView();

				 if (!radListView)
					 return;

				 std::shared_ptr<SellItems> sellItem = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));

				 if (!sellItem)
					 return;

				 std::string sell_id = sellItem->request_new_id();
				 String^ sellItem_id = gcnew String(sell_id.c_str());

				 String^ itemName = gcnew String(ItemName.c_str());

				 array<String^>^ columnArrayItems = { itemName, itemName };
				 disable_item_update = true;
				 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(sellItem_id), columnArrayItems);
				 newItem->Text = Convert::ToString(sellItem_id);
				 newItem->Image = managed_util::get_item_image(itemName, 20);
				 radListView->Items->Add(newItem);
				 radListView->SelectedIndex = radListView->Items->Count - 1;
				 disable_item_update = false;

				 auto SellPtr = sellItem->getItemStruct(sell_id);

				 SellPtr->set_item_id(ItemsManager::get()->getitem_idFromName(ItemName));
				 SellPtr->set_item_name(managed_util::fromSS(itemName));
				 SellPtr->set_item_price(Item_price);
				 SellPtr->set_item_qty(Item_qty);

				 updateSelectedData();
				 radGroupBox1->Enabled = true;
			 }
			 System::Void loadAll(){
				std::map<std::string, std::shared_ptr<SellItems>> map = SellManager::get()->get_mapSellItems();
				
				for (auto it = map.begin(); it != map.end(); it++){
					 creatNewPage(gcnew String(it->first.c_str()));
					 loadItemOnView(gcnew String(it->first.c_str()));
				 }
			 }
			 System::Void loadItemOnView(String^ current_id) {
				 array<Control^>^ getRadListView = Controls->Find("list_" + current_id, true);

				 Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];

				 std::shared_ptr<SellItems> sellItems = SellManager::get()->getSellItems(managed_util::fromSS(current_id));
				 if (!sellItems)
					 return;

				 std::map<std::string, std::shared_ptr<ItemStruct>> map = sellItems->get_mapItemStruc();
				 radListView->BeginUpdate();
				 for (auto i = map.begin(); i != map.end(); i++) {
					 array<String^>^ columnArrayItems = { gcnew String(i->second->get_item_name().c_str()) };
					 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(i->first.c_str()), columnArrayItems);
					 newItem->Image = managed_util::get_item_image(gcnew String(i->second->item_name.c_str()), 20);
					 radListView->Items->Add(newItem);
				 }
				 radListView->EndUpdate();
			 }
			 void disable_group(){
				 auto listView = getItemListView();
				 if (!listView)
					 return;

				 if (listView->Items->Count > 0){
					 if (!radGroupBox1->Enabled)
						 radGroupBox1->Enabled = true;
				 }
				 else if ((listView->Items->Count <= 0)){
					 if (radGroupBox1->Enabled)
						 radGroupBox1->Enabled = false;
				 }
			 }

			 bool save_script(std::string file_name){
				 Json::Value file;
				 file["version"] = 0100;

				 file["SellManager"] = SellManager::get()->parse_class_to_json();

				 return saveJsonFile(file_name, file);
			 }
			 bool load_script(std::string file_name){
				 Json::Value file = loadJsonFile(file_name);
				 if (file.isNull())
					 return false;

				 SellManager::get()->parse_json_to_class(file["SellManager"]);
				 return true;
			 }

			 void update_idiom(){
				 bt_auto_generate->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_auto_generate->Text))[0]);
				 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
				 bt_delete->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete->Text))[0]);
				 bt_new->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_new->Text))[0]);
				 radMenu1->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenu1->Text))[0]);
				 menu->Text = gcnew String(&GET_TR(managed_util::fromSS(menu->Text))[0]);
				 bt_save->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save->Text))[0]);
				 bt_load->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load->Text))[0]);
			 }

	private: System::Void bt_auto_generate_Click(System::Object^  sender, System::EventArgs^  e) {
		NeutralBot::AutoGenerateLoot^ tempForm = gcnew NeutralBot::AutoGenerateLoot("");
		tempForm->ShowDialog();
		bool check_exist = true;
		if (tempForm->comfirmed){
			for each(String^ item in tempForm->selectedItems){
				check_exist = false;
				if (item == String::Empty)
					continue;

				if (!managed_util::check_exist_item(item->ToLower())){
					continue;
				}


				for each(auto list_item in getItemListView()->Items){
					if (list_item->Text->ToLower() == item->ToLower()){
						check_exist = true;
						break;
					}
				}

				if (check_exist)
					continue;
				
				if (item == String::Empty)
					return;

				if (!PageSell->SelectedPage)
					return;


				addItemNewOnView(managed_util::fromSS(item), 0, 0);
			}
		}
	}
	private: System::Void Sell_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
		if (GeneralManager::get()->get_practice_mode()){
			this->Hide();
			e->Cancel = true;
			return;
		}
	}
	private: System::Void box_backpack_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_item_update)
					 return;

				 std::shared_ptr<SellItems> srd_bp = SellManager::get()->getSellItems(managed_util::fromSS(PageSell->SelectedPage->Text));
				 if (!srd_bp)
					 return;
				 srd_bp->set_backpack_loot(managed_util::fromSS(box_backpack->Text));
	}
};
}