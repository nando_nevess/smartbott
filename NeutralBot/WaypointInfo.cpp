#include "WaypointInfo.h"

WaypointInfo::WaypointInfo(){
}


std::map<std::string, std::string> WaypointInfo::getadditionalInfoCopy(){
	return additionalInfo;
}

std::string WaypointInfo::get_visual_name(){
	return visual_name; 
} 

side_t::side_t WaypointInfo::get_side_to_go(){
	return side_to_go;
}

waypoint_type WaypointInfo::get_waypointType(){
	return waypointType;
}

Coordinate WaypointInfo::get_position(){
	return position; 
}

void WaypointInfo::set_visual_name(std::string name){
	visual_name = name;
}

void WaypointInfo::set_side_to_go(side_t::side_t s){
	side_to_go = s;
}

void WaypointInfo::set_waypointType(waypoint_type _type){
	waypointType = _type;
}

void WaypointInfo::set_position(Coordinate coord){
	position = coord;
}

void WaypointInfo::set_label(std::string z){
	label = z;
}

std::string WaypointInfo::get_label(){
	return label;
}

WaypointInfo::WaypointInfo(std::string label, waypoint_t action, Coordinate position,side_t::side_t side){
	this->set_label(label);
	this->set_action(action);
	this->set_position(position);
	this->set_side_to_go(side);
}

std::pair<waypoint_t,side_t::side_t> WaypointInfo::migrate_to_new(uint32_t type){
	waypoint_t action = waypoint_walk_to;
	side_t::side_t	side = side_t::side_t::side_center;
	switch (type){
	case 1:
		action = waypoint_walk_to;
		break;
	case 2:
		action = waypoint_message;
		break;
	case 3:
		action = waypoint_message_npc;
		break;
	case 4:
		action = waypoint_goto_label;
		break;
	case 5:
		action = waypoint_lua_script;
		break;
	case 6:
		action = waypoint_use;
		side = side_t::side_t::side_east;
		break;
	case 7:
		action = waypoint_use;
		side = side_t::side_t::side_south;
		break;
	case 8:
		action = waypoint_use;
		side = side_t::side_t::side_west;
		break;
	case 9:
		action = waypoint_use;
		side = side_t::side_t::side_north;
		break;
	case 10:
		action = waypoint_hole_or_step;
		side = side_t::side_t::side_east;
		break;
	case 11:
		action = waypoint_hole_or_step;
		side = side_t::side_t::side_south;
		break;
	case 12:
		action = waypoint_hole_or_step;
		side = side_t::side_t::side_west;
		break;
	case 13:
		action = waypoint_hole_or_step;
		side = side_t::side_t::side_north;
		break;
	case 14:
		action = waypoint_exani_hur_up;
		side = side_t::side_t::side_east;
		break;
	case 15:
		action = waypoint_exani_hur_up;
		side = side_t::side_t::side_south;
		break;
	case 16:
		action = waypoint_exani_hur_up;
		side = side_t::side_t::side_west;
		break;
	case 17:
		action = waypoint_exani_hur_up;
		side = side_t::side_t::side_north;
		break;
	case 18:
		action = waypoint_exani_hur_down;
		side = side_t::side_t::side_east;
		break;
	case 19:
		action = waypoint_exani_hur_down;
		side = side_t::side_t::side_south;
		break;
	case 20:
		action = waypoint_exani_hur_down;
		side = side_t::side_t::side_west;
		break;
	case 21:
		action = waypoint_exani_hur_down;
		side = side_t::side_t::side_north;
		break;
	case 22:
		action = waypoint_check_necesary_deposit_go_to_label;
		break;
	case 23:
		action = waypoint_check_necesary_deposit_any_goto_label;
		break;
	case 24:
		action = waypoint_check_necesary_repot_go_to_label;
		break;
	case 25:
		action = waypoint_check_necesary_repot_any_go_to_label;
		break;
	case 26:
		action = waypoint_walk_to;
		break;
	case 27:
		action = waypoint_walk_to;
		break;
	case 28:
		action = waypoint_deposit_items_by_depot_id;
		break;
	case 29:
		action = waypoint_deposit_items_all_depot;
		break;
	case 30:
		action = waypoint_withdraw_all_repots_if_fail_goto_label;
		break;
	case 31:
		action = waypoint_repot_here_by_id;
		break;
	case 32:
		action = waypoint_rope;
		break;
	case 33:
		action = waypoint_shovel;
		break;
	case 34:
		action = waypoint_pick;
		break;
	case 35:
		action = waypoint_machete;
		break;
	case 36:
		action = waypoint_scythe;
		break;
	case 37:
		action = waypoint_tool_id;
		break;
	case 38:
		action = waypoint_use_item;
		side = side_t::side_t::side_east;
		break;
	case 39:
		action = waypoint_use_item;
		side = side_t::side_t::side_south;
		break;
	case 40:
		action = waypoint_use_item;
		side = side_t::side_t::side_west;
		break;
	case 41:
		action = waypoint_use_item;
		side = side_t::side_t::side_north;
		break;
	case 42:
		action = waypoint_remove_item;
		side = side_t::side_t::side_east;
		break;
	case 43:
		action = waypoint_remove_item;
		side = side_t::side_t::side_south;
		break;
	case 44:
		action = waypoint_remove_item;
		side = side_t::side_t::side_west;
		break;
	case 45:
		action = waypoint_remove_item;
		side = side_t::side_t::side_north;
		break;
	case 46:
		action = waypoint_wait_milliseconds;
		break;
	case 47:
		action = waypoint_up_ladder;
		break;
	case 48:
		action = waypoint_use_item_id_to_down_or_up;
		break;
	case 49:
		action = waypoint_reopen_containers;
		break;
	case 50:
		action = waypoint_close_containers;
		break;
	case 51:
		action = waypoint_loggout_this_location;
		break;
	case 52:
		action = waypoint_close_tibia_this_location;
		break;
	case 53:
		action = waypoint_if_way_block_goto_label;
		side = side_t::side_t::side_east;
		break;
	case 54:
		action = waypoint_if_way_block_goto_label;
		side = side_t::side_t::side_south;
		break;
	case 55:
		action = waypoint_if_way_block_goto_label;
		side = side_t::side_t::side_west;
		break;
	case 56:
		action = waypoint_if_way_block_goto_label;
		side = side_t::side_t::side_north;
		break;
	case 57:
		action = waypoint_if_way_block_goto_label;
		side = side_t::side_t::side_center;
		break;
	case 58:
		action = waypoint_use_lever;
		side = side_t::side_t::side_east;
		break;
	case 59:
		action = waypoint_use_lever;
		side = side_t::side_t::side_south;
		break;
	case 60:
		action = waypoint_use_lever;
		side = side_t::side_t::side_west;
		break;
	case 61:
		action = waypoint_use_lever;
		side = side_t::side_t::side_north;
		break;
	case 62:
		action = waypoint_rearch_creature;
		break;
	case 63:
		action = waypoint_walk_to;
		break;
	case 64:
		action = waypoint_teleport_else_go_to_label;
		side = side_t::side_t::side_north;
		break;
	case 65:
		action = waypoint_teleport_else_go_to_label;
		side = side_t::side_t::side_east;
		break;
	case 66:
		action = waypoint_teleport_else_go_to_label;
		side = side_t::side_t::side_south;
		break;
	case 67:
		action = waypoint_teleport_else_go_to_label;
		side = side_t::side_t::side_west;
		break;
	case 68:
		action = waypoint_walk_to_random;
		break;
	case 69:
		action = waypoint_walk_to;
		break;
	case 70:
		action = waypoint_check_cap_less_than_goto_label;
		break;
	case 71:
		action = waypoint_destroy_field;
		break;
	case 72:
		action = waypoint_open_door;
		side = side_t::side_t::side_north;
		break;
	case 73:
		action = waypoint_open_door;
		side = side_t::side_t::side_east;
		break;
	case 74:
		action = waypoint_open_door;
		side = side_t::side_t::side_south;
		break;
	case 75:
		action = waypoint_open_door;
		side = side_t::side_t::side_west;
		break;
	case 76:
		action = waypoint_go_to_waypoint_path;
		break;
	case 77:
		action = waypoint_deposit_money;
		break;
	case 78:
		action = waypoint_repot_from_depot_by_id;
		break;
	case 79:
		action = waypoint_lure_here_distance;
		break;
	case 80:
		action = waypoint_lure_here_knight_mode;
		break;
	case 81:
		action = waypoint_sell_items_by_sell_id;
		break;
	case 82:
		action = waypoint_sell_all_items;
		break;
	case 83:
		action = waypoint_auto_haste;
		break;
	case 84:
		action = waypoint_auto_utura;
		break;
	case 85:
		action = waypoint_cast_spell;
		break;
	case 86:
		action = waypoint_pulse_target;
		break;
	case 87:
		action = waypoint_pulse_looter;
		break;
	default:
		action = waypoint_walk_to;
		break;
	}

	return std::make_pair(action, side);
}

Coordinate WaypointInfo::get_destination_coord() {
	return this->position;
}

void WaypointInfo::set_position_x(int x) {
	this->position.x = x; 
}

void WaypointInfo::set_position_y(int y) {
	this->position.y = y;
}

void WaypointInfo::set_position_z(int z) {
	this->position.z = z;
}

int WaypointInfo::get_position_x() {
	return this->position.x;
}

int WaypointInfo::get_position_y() {
	return this->position.y;
}

int WaypointInfo::get_position_z() {
	return this->position.z;
}

void WaypointInfo::set_action(waypoint_t action) {
	if (action < waypoint_none || action > waypoint_t_last)
		return;

	if (action == waypoint_lure_here_distance || action == waypoint_lure_here_knight_mode)
		this->waypointType = waypoint_type::type_lure;
	else
		this->waypointType = waypoint_type::type_none;

	this->action = (waypoint_t) action;
}


waypoint_t WaypointInfo::get_action() {
	return this->action;
}

void WaypointInfo::set_additionalInfo(std::string key, std::string value) {
	this->additionalInfo[key] = value;
}

std::string WaypointInfo::get_additionalInfo(std::string key) {
	auto additionalInfo = this->additionalInfo.find(key);
	
	if (additionalInfo == this->additionalInfo.end())
		return "";

	return additionalInfo->second;
}

int WaypointInfo::get_additional_info_as_int(std::string key){
	try{
		std::string str_value = get_additionalInfo(key);
		return boost::lexical_cast<int>(str_value);
	}
	catch (...){}
	return 0;
}

bool WaypointInfo::get_additional_info_as_bool(std::string key){
	return string_util::lower(get_additionalInfo(key)) == "true";
}

std::vector<std::string> WaypointInfo::get_additionalKeys() {
	std::vector<std::string> keys;

	for (auto key : additionalInfo)
		keys.push_back(key.first);

	return keys;
}

Json::Value WaypointInfo::parse_class_to_json() {
	Json::Value waypointInfo;
	
	waypointInfo["side_to_go"] = this->get_side_to_go();
	waypointInfo["label"] = this->get_label();

	Json::Value position;
	position["x"] = this->position.x;
	position["y"] = this->position.y;
	position["z"] = this->position.z;

	waypointInfo["position"] = position;
	waypointInfo["action"] = this->action;

	Json::Value waypointInfoMap;
	for (auto info : this->additionalInfo)
		waypointInfoMap[info.first] = info.second;

	waypointInfo["infoList"] = waypointInfoMap;
	return waypointInfo;
}

void WaypointInfo::parse_json_to_class(Json::Value jsonObject, int version) {
	if (!jsonObject["side_to_go"].empty() || !jsonObject["side_to_go"].isNull())
		set_side_to_go((side_t::side_t)jsonObject["side_to_go"].asInt());
	else
		set_side_to_go(side_t::side_t::side_center);

	if (!jsonObject["label"].empty() || !jsonObject["label"].isNull())
		set_label(jsonObject["label"].asString());

	if (!jsonObject["action"].empty() || !jsonObject["action"].isNull()){
		if (version < 200){
			auto action_side = migrate_to_new(jsonObject["action"].asInt());
			
			this->set_side_to_go(action_side.second);
			this->set_action(action_side.first);
		}
		else
			this->set_action((waypoint_t)jsonObject["action"].asInt());		
	}

	if (!jsonObject["position"].empty() || !jsonObject["position"].isNull()){
		Json::Value position = jsonObject["position"];
		Coordinate coordinate(
			position["x"].asInt(),
			position["y"].asInt(),
			position["z"].asInt()
			);

		this->set_position(coordinate);
	}
	if (!jsonObject["infoList"].empty() || !jsonObject["infoList"].isNull()){
		Json::Value waypointInfoList = jsonObject["infoList"];
		Json::Value::Members membersList = waypointInfoList.getMemberNames();

		for (auto members : membersList) {
			additionalInfo[members] = waypointInfoList[members].asString();
		}
	}
}
