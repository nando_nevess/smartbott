#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include <functional>

class Scheduler{
	boost::asio::io_service& _svc;
public:
	Scheduler(boost::asio::io_service& svc) : _svc(svc){
	}

	void add_task(std::function<void()> _task_handler, uint32_t from_now = 0){
		boost::shared_ptr<boost::asio::deadline_timer> _volatile_timer(new boost::asio::deadline_timer(_svc));

		_volatile_timer->expires_from_now(boost::posix_time::milliseconds(from_now));

		_volatile_timer->async_wait(
			std::bind([](boost::shared_ptr<boost::asio::deadline_timer> _timer_holder, boost::function<void()> __task_handler) -> void {
			__task_handler();
		},
			_volatile_timer, _task_handler));
	}
};