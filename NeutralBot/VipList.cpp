#include "Core\VipList.h"
#include "DevelopmentManager.h"

void VipList::swap(){
	lock_guard lock(mtx_access);
	std::vector<std::shared_ptr<VipPlayer>>* temp = mVipPlayers;
	mVipPlayers = mVipPlayers_swap;
	mVipPlayers_swap = temp;
}

void VipList::update(){
	LOG_TIME_INIT
	mVipPlayers_swap->clear();

	int address_base = AddressManager::get()->getAddress(ADDRESS_POINTER_VIP_PLAYERS);
	int first = TibiaProcess::get_default()->read_int(address_base);
	int address = TibiaProcess::get_default()->read_int(first, false);
	int last = 0;
	while (first != address){
		std::shared_ptr<VipPlayer> VipPlayerPtr(new VipPlayer);
		int next = TibiaProcess::get_default()->read_int(address, false);
		int info_address = TibiaProcess::get_default()->read_int(address + 0x18, false);
		if (next == first){

			VipPlayerPtr->address_name = address + 0x18;
			VipPlayerPtr->address_desc = address + 0x30;
			*(uint32_t*)(&(VipPlayerPtr->type)) = TibiaProcess::get_default()->read_int(address + 0x18 - 4, false);
		}
		else
			if (TibiaProcess::get_default()->read_memory_block(address, VipPlayerPtr.get(), sizeof(VipPlayer), false) != sizeof(VipPlayer))
				return;
		mVipPlayers_swap->push_back(VipPlayerPtr);
		last = address;
		address = next;
	}
	swap();
}


VipList::VipList(){
	mVipPlayers = new std::vector<std::shared_ptr<VipPlayer>>();
	mVipPlayers_swap = new std::vector<std::shared_ptr<VipPlayer>>();
}

std::shared_ptr<VipPlayer> VipList::get_vip_by_player_name(std::string& name){
	update();
	for (auto it = mVipPlayers->begin(); it != mVipPlayers->end(); it++)
		if (it->get()->get_name() == name)
			return *it;
	return 0;
}

std::vector<std::shared_ptr<VipPlayer>> VipList::get_vip_players(){
	update();
	return *mVipPlayers;
}



std::string VipPlayer::get_name(){
	if (this->name_len < 16)
		return std::string((char*)&this->address_name);
	std::string name = TibiaProcess::get_default()->read_string(address_name, false);
	return name;
}
std::string VipPlayer::get_desc(){
	return TibiaProcess::get_default()->read_string(address_desc, false);
}