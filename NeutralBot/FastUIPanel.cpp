#pragma once
#include "FastUIPanel.h"
#include "FastUIPanelController.h"
#include "Core\TibiaProcess.h"
#include "GeneralManager.h"
#include <Windows.h>

using namespace NeutralBot;

System::Void FastUIPanel::timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	RECT rctDesktop;
	SystemParametersInfo(SPI_GETWORKAREA, NULL, &rctDesktop, NULL);
	int x = rctDesktop.left;
	int y = rctDesktop.top;
	int width = rctDesktop.right - x;
	int height = rctDesktop.bottom - y;
	this->Location = System::Drawing::Point(rctDesktop.left, rctDesktop.top);
	this->Size = System::Drawing::Size(65, height);
	

	/*if (GeneralManager::get()->get_interface_most_top()){
		if (TibiaProcess::get_default()->get_window_handler() != GetForegroundWindow() && topMost){
			FastUIPanelController::get()->on_top_most_option_change(false);
			topMost = false;
		}
		else if (TibiaProcess::get_default()->get_window_handler() == GetForegroundWindow() && !topMost){
			FastUIPanelController::get()->on_top_most_option_change(true);
			topMost = true;
		}
	}*/

	/*if (GeneralManager::get()->get_interface_auto_resize()){
		if (GetActiveWindow() != GetForegroundWindow() && autoResize){
			FastUIPanelController::get()->on_resize_option_change(true);
			autoResize = true;
		}
		if (GetActiveWindow() == GetForegroundWindow() && !autoResize){
			FastUIPanelController::get()->on_resize_option_change(false);
			autoResize = false;
		}
	}*/
}

void FastUIPanel::add_form_button(String^ id, String^ text, String^ image){
	/*Telerik::WinControls::UI::RadTileElement^ new_tile = (gcnew Telerik::WinControls::UI::RadTileElement());
	
	for each(auto item in radPanorama1->Items){
		String^ Name = item->Name;
		if(Name == id)
			return;
	}

	radPanorama1->Items->Add(new_tile);
	
	new_tile->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F));
	new_tile->AccessibleDescription = L"radTileElement1";
	new_tile->AccessibleName = L"radTileElement1";
	new_tile->Name = id;
	new_tile->Text = id;
	try{
		new_tile->Image = Image::FromFile(image);
	}
	catch (...){
	}
	new_tile->Click += gcnew System::EventHandler(this, &FastUIPanel::show_form_by_id);
	if (!is_shown){
		Show();
		timer1->Enabled = true;
	}*/
}

void FastUIPanel::show_form_by_id(System::Object^  sender, System::EventArgs^  e){
	Telerik::WinControls::UI::RadTileElement^ tile = (Telerik::WinControls::UI::RadTileElement^)sender;
	FastUIPanelController::get()->show_form_by_id(tile->Name);
}

void FastUIPanel::remove_form(String^ id,bool hide){
	int removed_x = 0;
	int removed_y = 0;
	bool removed = false;
	for each(auto item in radPanorama1->Items){
		if (item->Name == id){
			Telerik::WinControls::UI::RadTileElement^ tile = 
				(Telerik::WinControls::UI::RadTileElement^)item;
			removed_x = tile->Column;
			removed_y = tile->Row;
			/*if (hide)
				item->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			else
				item->Visibility = Telerik::WinControls::ElementVisibility::Visible;*/
		radPanorama1->Items->Remove(item);
			removed = true;
			for each(auto item_sub in radPanorama1->Items){
				Telerik::WinControls::UI::RadTileElement^ tile_sub = (Telerik::WinControls::UI::RadTileElement^)item_sub;
				if (tile_sub->Row > removed_y){
					tile_sub->Row--;
				}
			}
			break;
		}
	}

	if (!radPanorama1->Items->Count){
		is_shown = false;
		Hide();
		timer1->Enabled = false;
		return;
	}

	
}

FastUIPanel::FastUIPanel(void){
	InitializeComponent();

	 topMost = GeneralManager::get()->get_interface_most_top();
	 autoResize = GeneralManager::get()->get_interface_auto_resize();

}

FastUIPanel::~FastUIPanel(){
	if (components){
		delete components;
	}
}

System::Void FastUIPanel::FastUIPanel_Load(System::Object^  sender, System::EventArgs^  e) {

}