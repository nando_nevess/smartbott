#pragma once

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms; 
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class FastUIPanel : public System::Windows::Forms::Form{
	public:
		FastUIPanel(void);

	protected:
		~FastUIPanel();
	private: System::Windows::Forms::Timer^  timer1;
	private: Telerik::WinControls::UI::RadPanorama^  radPanorama1;
	private: Telerik::WinControls::UI::RadTitleBar^  radTitleBar1;
	private: Telerik::WinControls::UI::RadPanel^  radPanel1;
	private: Telerik::WinControls::UI::RadPanel^  radPanel2;
	protected:
	private: System::ComponentModel::IContainer^  components;
	private:
#pragma region Windows Form Designer generated code
		void InitializeComponent(void){
			this->components = (gcnew System::ComponentModel::Container());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->radPanorama1 = (gcnew Telerik::WinControls::UI::RadPanorama());
			this->radTitleBar1 = (gcnew Telerik::WinControls::UI::RadTitleBar());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanorama1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTitleBar1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			this->SuspendLayout();
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &FastUIPanel::timer1_Tick);
			// 
			// radPanorama1
			// 
			this->radPanorama1->AllowDragDrop = false;
			this->radPanorama1->BackColor = System::Drawing::Color::Purple;
			this->radPanorama1->CellSize = System::Drawing::Size(63, 63);
			this->radPanorama1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanorama1->Location = System::Drawing::Point(0, 0);
			this->radPanorama1->Name = L"radPanorama1";
			this->radPanorama1->RowsCount = 1000;
			this->radPanorama1->Size = System::Drawing::Size(102, 0);
			this->radPanorama1->TabIndex = 0;
			this->radPanorama1->Text = L"radPanorama1";
			(cli::safe_cast<Telerik::WinControls::UI::RadPanoramaElement^>(this->radPanorama1->GetChildAt(0)))->BackColor = System::Drawing::Color::Purple;
			(cli::safe_cast<Telerik::WinControls::UI::RadScrollBarElement^>(this->radPanorama1->GetChildAt(0)->GetChildAt(0)))->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->radPanorama1->GetChildAt(0)->GetChildAt(0)->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::FillPrimitive^>(this->radPanorama1->GetChildAt(0)->GetChildAt(0)->GetChildAt(1)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radPanorama1->GetChildAt(0)->GetChildAt(2)))->Opacity = 0;
			// 
			// radTitleBar1
			// 
			this->radTitleBar1->BackColor = System::Drawing::Color::Purple;
			this->radTitleBar1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radTitleBar1->Enabled = false;
			this->radTitleBar1->Location = System::Drawing::Point(0, 0);
			this->radTitleBar1->Name = L"radTitleBar1";
			// 
			// 
			// 
			this->radTitleBar1->RootElement->Opacity = 0;
			this->radTitleBar1->Size = System::Drawing::Size(102, 45);
			this->radTitleBar1->TabIndex = 0;
			this->radTitleBar1->TabStop = false;
			(cli::safe_cast<Telerik::WinControls::UI::RadTitleBarElement^>(this->radTitleBar1->GetChildAt(0)))->Text = L"";
			(cli::safe_cast<Telerik::WinControls::UI::RadTitleBarElement^>(this->radTitleBar1->GetChildAt(0)))->Opacity = 0;
			(cli::safe_cast<Telerik::WinControls::UI::RadImageButtonElement^>(this->radTitleBar1->GetChildAt(0)->GetChildAt(2)->GetChildAt(1)->GetChildAt(0)))->Opacity = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadImageButtonElement^>(this->radTitleBar1->GetChildAt(0)->GetChildAt(2)->GetChildAt(1)->GetChildAt(1)))->Opacity = 1;
			(cli::safe_cast<Telerik::WinControls::UI::RadImageButtonElement^>(this->radTitleBar1->GetChildAt(0)->GetChildAt(2)->GetChildAt(1)->GetChildAt(2)))->Opacity = 1;
			// 
			// radPanel1
			// 
			this->radPanel1->AutoSize = true;
			this->radPanel1->Controls->Add(this->radTitleBar1);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel1->Location = System::Drawing::Point(0, 0);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(102, 45);
			this->radPanel1->TabIndex = 1;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radPanorama1);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel2->Location = System::Drawing::Point(0, 45);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(102, 0);
			this->radPanel2->TabIndex = 2;
			this->radPanel2->Text = L"radPanel2";
			// 
			// FastUIPanel
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(102, 44);
			this->Controls->Add(this->radPanel2);
			this->Controls->Add(this->radPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"FastUIPanel";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"FastUIPanel";
			this->TopMost = true;
			this->TransparencyKey = System::Drawing::Color::Purple;
			this->Load += gcnew System::EventHandler(this, &FastUIPanel::FastUIPanel_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanorama1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radTitleBar1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
public:
	bool topMost;
	bool autoResize;

	bool is_shown = false;

	System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e);

	void add_form_button(System::String^ id, System::String^ text, System::String^ image);

	void show_form_by_id(System::Object^  sender, System::EventArgs^  e);

	void remove_form(System::String^ id,bool hide);

private: System::Void FastUIPanel_Load(System::Object^  sender, System::EventArgs^  e);
};
}


