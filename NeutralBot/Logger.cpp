#include "Logger.h"

void Logger::add_error_log(std::string file, std::string error_str){
	std::ofstream out;
	out.open(file, std::ios::app);
	out << "\n" << error_str;
}