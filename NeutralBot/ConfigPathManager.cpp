#pragma once
#include "ConfigPathManager.h"
#include "Core\ItemsManager.h"


void  ConfigPathManager::creat_items_vector(){
	std::vector<uint32_t> items_fields = ItemsManager::get()->items_as_fields_runes;
	int index = 0;
	for (auto field : items_fields){
		std::shared_ptr<ConfigPathProperty> newPath = std::shared_ptr<ConfigPathProperty>(new ConfigPathProperty());

		newPath->path_id = field;
		newPath->property_type = (property_t)1;
		newPath->path_name = "item" + std::to_string(index);

		index++;
		vector_path_property.push_back(newPath);
	}
}