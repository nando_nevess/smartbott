#include "Core\Pvp.h"
#include "Core\Input.h"
#include "Core\Inventory.h"
#include "Core\Interface.h"
#include "Core\Time.h"

pvp_new_t PvpManager::get_skull_mode_new(){
	return (pvp_new_t)TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_SKULL_MODE_NEW));
}

pvp_old_t PvpManager::get_skull_mode_old(){
	return (pvp_old_t)TibiaProcess::get_default()->read_byte(AddressManager::get()->getAddress(ADDRESS_SKULL_MODE_OLD));
}

bool PvpManager::set_skull_mode_old(pvp_old_t style){
	if (get_skull_mode_new() != pvp_new_1)
		if (!set_skull_mode_new(pvp_new_1))
			return false;

	Point point = ClientGeneralInterface::get()->get_game_window_size();
	if (point.is_null())
		return false;

	auto equipment = ClientGeneralInterface::get()->getEquipmentChildOnRightPane();
	if (!equipment)
		return false;

	if (get_skull_mode_old() == style)
		return false;

	if (Inventory::get()->is_maximized()){
		point.x -= COORDINATES::COORDINATE_BUTTON_SKULL_OLD.X;
		point.y = equipment->y + COORDINATES::COORDINATE_BUTTON_SKULL_OLD.Y;
	} else {
		point.x -= COORDINATES::COORDINATE_BUTTON_SKULL_OLD.X;
		point.y = equipment->y + COORDINATES::COORDINATE_BUTTON_SKULL_OLD.Y;
	}

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(point.x, point.y);
	return true;
}

bool PvpManager::set_skull_mode_new(pvp_new_t mode){
	if (get_skull_mode_new() == mode)
		return true;

	COORD BUTTON_OFFSET;
	if (Inventory::get()->is_maximized()){
		switch (mode){
		case pvp_new_1:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_0;
		}break;
		case pvp_new_2:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_1;
		}break;
		case pvp_new_3:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_2;
		}break;
		case pvp_new_4:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_3;
		}break;
		default:
			return false;
			break;
		}
	} else {
		switch (mode){
		case pvp_new_1:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_0_minimized;
		}break;
		case pvp_new_2:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_1_minimized;
		}break;
		case pvp_new_3:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_2_minimized;
		}break;
		case pvp_new_4:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_SKULL_NEW_3_minimized;
		}break;
		default:
			return false;
			break;
		}
	}

	Point point = ClientGeneralInterface::get()->get_game_window_size();
	if (point.is_null())
		return false;

	auto equipment = ClientGeneralInterface::get()->getEquipmentChildOnRightPane();
	if (!equipment)
		return false;

	point.x -= BUTTON_OFFSET.X;
	point.y = equipment->y + BUTTON_OFFSET.Y;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(point.x, point.y);

	return true;
}

bool PvpManager::set_balanced_attack_mode(){
	return set_attack_mode(attack_mode_balanced);
}
bool PvpManager::set_offensive_attack_mode(){
	if (get_attack_mode() == attack_mode_offensive)
		return false;

	return set_attack_mode(attack_mode_offensive);
}
bool PvpManager::set_defensive_attack_mode(){
	if (get_attack_mode() == attack_mode_defensive)
		return false;

	return set_attack_mode(attack_mode_defensive);
}

bool PvpManager::set_attack_mode(attack_mode_t mode){
	attack_mode_t current_mode = get_attack_mode();
	if (mode == current_mode)
		return true;

	COORD BUTTON_OFFSET;

	if (Inventory::get()->is_maximized()){
		switch (mode){
		case attack_mode_balanced:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_BALANCED_MODE;
		}
			break;
		case attack_mode_defensive:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_DEFENSIVE_MODE;
		}
			break;
		case attack_mode_offensive:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_ATTACK_MODE;
		}
			break;
		}
	}
	else{
		switch (mode){
		case attack_mode_balanced:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_BALANCED_MODE_MINIMIZED;
		}
			break;
		case attack_mode_defensive:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_DEFENSIVE_MODE_MINIMIZED;
		}
			break;
		case attack_mode_offensive:{
			BUTTON_OFFSET = COORDINATES::COORDINATE_BUTTON_ATTACK_MODE_MINIMIZED;
		}
			break;
		}
	}
	Coordinate coordinate_temp = ClientGeneralInterface::get()->get_client_dimension();
	Point point = Point(coordinate_temp.x, coordinate_temp.y);
	if (point.is_null())
		return false;

	auto equipment = ClientGeneralInterface::get()->getEquipmentChildOnRightPane();
	if (!equipment)
		return false;

	point.x = point.x - BUTTON_OFFSET.X;
	point.y = equipment->y + BUTTON_OFFSET.Y;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_left(point.x, point.y);
	return true;
}



attack_mode_t PvpManager::get_attack_mode(){
	return (attack_mode_t)TibiaProcess::get_default()
		->read_int(AddressManager::get()->getAddress(ADDRESS_ATTACK_MODE));
}


PvpManager* PvpManager::get(){
	static PvpManager* mPvpManager = nullptr;
	if (!mPvpManager)
		mPvpManager = new PvpManager;
	return mPvpManager;
}


