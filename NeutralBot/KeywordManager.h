#pragma once
#include <string>
#include <map>
#include <vector>
#include <algorithm>
#include <stdint.h>

enum lua_table_t{
	lua_table_class,
	lua_table_table,
	lua_table_metatable,
	lua_table_enum,
	lua_table_total,
	lua_table_none,
	lua_table_any
};

class LuaVarInfo{
public:
	std::string name;
	LuaVarInfo(std::string _name){
		name = _name;
	}

	void add_description(std::string desc){
		auto _found = std::find_if(descriptions.begin(), descriptions.end(), [&](std::string& str_it){ return desc == str_it; });
		if (_found != descriptions.end()){
			return;
		}
		descriptions.push_back(desc);
	}

	std::vector<std::string> descriptions;
};

class LuaFunctionMehotdInfo{
public:
	LuaFunctionMehotdInfo(std::string _method_string);
	bool add_suggestion(std::string suggestion);
	bool add_description(std::string description);

	std::vector<std::string> empty();

	std::string method_string;
	std::vector<std::string> suggestions;
	std::vector<std::string> descriptions;
};

class LuaTableInfo{
public:

	lua_table_t type;

	LuaTableInfo(std::string _name, lua_table_t _type);

	bool add_method(std::string method);

	bool add_method_description(std::string method, std::string method_description);

	bool add_method_suggestion(std::string method, std::string method_suggestion);

	bool add_variable(std::string variable);

	bool add_variable_description(std::string variable_name, std::string description);

	LuaFunctionMehotdInfo* get_method_by_name(std::string method);

	LuaVarInfo* get_var_by_name(std::string varname);

	bool add_description(std::string description);

	std::vector<std::string> empty();

	std::string name;
	std::vector<std::string> descriptions;
	std::vector<LuaFunctionMehotdInfo*> methods;
	std::vector<LuaVarInfo*> variables;
};

class KeywordManager{
public:
	/*additional_info.libdat*/
	KeywordManager();

	std::string last_error;
	std::map<std::string/*lib*/, std::vector<LuaTableInfo*>> class_vector;
	std::vector<std::string> missing_sugestion;
	std::map<std::string/*lib*/, std::vector<LuaFunctionMehotdInfo*>> function_vector;
	std::map<std::string/*lib*/, std::vector<LuaVarInfo*>> variables_vector;

	std::string current_lib_parsing;

	void set_lib_parsing(std::string lib_name){
		current_lib_parsing = lib_name;
	}

	std::string getLastError();

	void resetLastError();

	std::vector<std::string> check_missing(bool classes, bool functions, bool tables, bool var);

	LuaTableInfo* get_class_by_class_string(std::string libname, std::string class_string);

	LuaFunctionMehotdInfo* get_function_by_function_string(std::string lib_name, std::string function_string);

	void process_additional_info(std::vector<std::string>& additional_info_splited_vector, std::pair<char*, uint32_t>& file_pair);

	bool registerClass(std::string class_name);

	bool registerTable(std::string table_name);

	bool registerMetatable(std::string metatable_name);

	bool registerEnumTable(std::string enum_table);

	bool registerTableVariable(std::string table_name, std::string var_name);

	void addTableVariableDescription(std::string table_name, std::string variable, std::string description);

	void addVariableDescription(std::string variable, std::string description);

	bool addVariable(std::string variable);

	bool registerTableMethod(std::string class_name, std::string method);

	bool addTableDescription(std::string class_name, std::string description);

	bool addMethodDescription(std::string class_name, std::string method, std::string description);

	bool addMethodSuggestion(std::string class_name, std::string method, std::string autoCompleteText);

	bool registerFunction(std::string function);

	bool addFunctionDescription(std::string function, std::string description);

	bool addFunctionSuggestion(std::string function, std::string autocompleteText);

	std::string get_lib_description(std::string libname);

	std::string get_table_description(std::string libname, std::string class_name);

	std::string get_method_description(std::string libname, std::string class_name, std::string method_name);

	std::string get_function_description(std::string libname, std::string function_name);

	std::string get_variable_description(std::string lib_name, std::string variable_name){
		auto lib = variables_vector.find(lib_name);
		if (lib == variables_vector.end())
			return "";

		auto variable = std::find_if(lib->second.begin(), lib->second.end(), [&](LuaVarInfo* lib_ptr){ return lib_ptr->name == variable_name; });
		if (variable == lib->second.end())
			return "";

		std::string retval = "";
		for (auto desc : (*variable)->descriptions){
			retval += desc + "\n";
		}
		return retval;
	}

	std::string get_table_variable_description(std::string lib_name, std::string table_name, std::string variable_name);

	static KeywordManager* get();
};