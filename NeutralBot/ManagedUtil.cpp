#pragma once
#include "Core\Neutral.h"
#include "ManagedUtil.h"
#include <msclr\marshal_cppstd.h>
#include "LanguageManager.h"
#include "GifManager.h"
#include "Core\LooterCore.h"
#include "Core\ItemsManager.h"
#include "SpellManager.h"

namespace managed_util{

	bool print_missings_hotkeys(){
		auto missing_hk = SpellManager::get()->missing_hotkeys();

		if (!missing_hk.size())
			return false;

		std::string missing_kh_text = "";

		for (auto hk_text : missing_hk)
			missing_kh_text += "\n" + hk_text.first;

		if (Telerik::WinControls::RadMessageBox::Show("The hotkey of" + gcnew String(missing_kh_text.c_str()) + "\nwas not gounded in your tibia hotkeys.\nConfigure it before to start", "WARNING", MessageBoxButtons::OKCancel, Telerik::WinControls::RadMessageIcon::Exclamation) == System::Windows::Forms::DialogResult::OK)
			return true;

		return false;
	}

	bool findString(System::String^ str, System::String^ to_find){
		return (str->IndexOf(to_find) >= 0);
	}

	std::string fromSS(System::String^ in){
		std::string str = "";
		if (in != ""){
			using namespace System::Runtime::InteropServices;

			str = std::string((char*)(void*)Marshal::StringToHGlobalAnsi(in));
		}
		return str;
	}

	System::String^ fromString(std::string in){
		return gcnew String(in.c_str());
	}

	array<System::String^>^ convert_vector_to_array(std::vector<std::string> vct){
		array<String^>^ Arr = gcnew array<String^>(vct.size());

		for (uint32_t i = 0; i < vct.size(); i++){
			Arr[i] = gcnew String(vct[i].c_str());
		}
		return Arr;
	}
	std::string to_std_string(String^ in){
		return msclr::interop::marshal_as<std::string>(in);
	}

	void ManagedControlsHook::updateButtonsByVars(){
		Telerik::WinControls::UI::RadToggleButton^ button = gcnew Telerik::WinControls::UI::RadToggleButton();

		button->Name = "radToggleButtonWaypointer";
		button->Text = "Waypointer";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_WAYPOINTER) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonHunter";
		button->Text = "Hunter";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_HUNTER) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonSpellcaster";
		button->Text = "Spellcaster";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_SPELLCASTER) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonLooter";
		button->Text = "Looter";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_LOOTER) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonAlerts";
		button->Text = "Alerts";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_ALERTS) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonLua";
		button->Text = "Lua";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_LUA) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonGeneral";
		button->Text = "General";
		button->ToggleState = NeutralManager::get()->get_core_states(CORE_GENERAL) == NEUTRAL_CORE_TASK_STATE::ENABLED ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonSpellHeal";
		button->Text = "Spell:Heal";
		button->ToggleState = SpellManager::get()->get_health_spells_state() ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

		button->Name = "radToggleButtonSpellAttack";
		button->Text = "Spell:Attack";
		button->ToggleState = SpellManager::get()->get_attack_spells_state() ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
		buttonCheckChange(button, nullptr);

	}

	System::Void ManagedControlsHook::buttonCheckChange(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
		Telerik::WinControls::UI::RadToggleButton^ button = (Telerik::WinControls::UI::RadToggleButton^) sender;


		String^ name = button->Name->Substring((gcnew String("radToggleButton"))->Length);
		NEUTRAL_CORE_TASK_STATE state = button->ToggleState == Telerik::WinControls::Enumerations::ToggleState::On ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;


		if (name == "Waypointer"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_WAYPOINTER, state);
		}
		else if (name == "Hunter"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_HUNTER, state);
		}
		else if (name == "Spellcaster"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_SPELLCASTER, state);
		}
		else if (name == "Looter"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_LOOTER, state);
			LootListQueue::get()->clear_loot_list();
		}
		else if (name == "Alerts"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_ALERTS, state);
		}
		else if (name == "Lua"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_LUA, state);
		}
		else if (name == "General"){
			NeutralManager::get()->set_core_states(NEUTRAL_CORE_ID::CORE_GENERAL, state);
		}
		else if (name == "SpellHeal"){
			SpellManager::get()->set_health_spells_state(state == NEUTRAL_CORE_TASK_STATE::ENABLED);
		}
		else if (name == "SpellAttack"){
			SpellManager::get()->set_attack_spells_state(state == NEUTRAL_CORE_TASK_STATE::ENABLED);
		}

		for each(auto button_it in toggleButtonsList){
			auto temp_name1 = button_it->Name;
			auto temp_name2 = button->Name;
			if (button_it->Name == button->Name){
				String^ reverse_text = gcnew String(&LanguageManager::get()->get_reverse_translation(fromSS(button_it->Text))[0]);
				if (reverse_text == "Enabled" || reverse_text == "Disabled"){
					button_it->Text = name;
					button->Text = name;
					(button->ToggleState == Telerik::WinControls::Enumerations::ToggleState::On) ?
						gcnew String(&LanguageManager::get()->get_translation("Enabled")[0]) :
						gcnew String(&LanguageManager::get()->get_translation("Disabled")[0]);
				}
				if (button->ToggleState == Telerik::WinControls::Enumerations::ToggleState::On)
					button_it->Image = Image::FromFile("img\\controls\\toggle_button_state\\check_on.png");
				else
					button_it->Image = Image::FromFile("img\\controls\\toggle_button_state\\check_off.png");
				if (button_it != button){
					button_it->ToggleState = button->ToggleState;
				}
					
			}
		}
	}

	void ManagedControlsHook::addToggleButtons(Telerik::WinControls::UI::RadToggleButton^ button){
		toggleButtonsList.Add(button);
	}

	void ManagedControlsHook::removeToggleButtons(Telerik::WinControls::UI::RadToggleButton^ button){
		toggleButtonsList.Remove(button);
	}

	bool getCoreStateByName(String^ control_name){
		String^ name = control_name->Substring((gcnew String("radToggleButton"))->Length);

		if (name == "Waypointer"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_WAYPOINTER) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "Hunter"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_HUNTER) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "Spellcaster"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_SPELLCASTER) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "Looter"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_LOOTER) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "Alerts"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_ALERTS) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "Lua"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_LUA) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "General"){
			return NeutralManager::get()->get_core_states(NEUTRAL_CORE_ID::CORE_GENERAL) == NEUTRAL_CORE_TASK_STATE::ENABLED;
		}
		else if (name == "Heal"){
			return SpellManager::get()->get_health_spells_state();
		}
		else if (name == "Attack"){
			return SpellManager::get()->get_attack_spells_state();
		}
		return false;
	}

	void setToggleCheckButtonStyle(Telerik::WinControls::UI::RadToggleButton^ button){

		ManagedControlsHook::get()->addToggleButtons(button);
		//button->Size = System::Drawing::Size(100,22);
		button->Image = Image::FromFile("img\\controls\\toggle_button_state\\check_off.png");
		button->ImageAlignment = System::Drawing::ContentAlignment::MiddleLeft;
		button->Text = L"Disabled";
		button->TextAlignment = System::Drawing::ContentAlignment::MiddleCenter;
		button->TextImageRelation = System::Windows::Forms::TextImageRelation::ImageBeforeText;
		button->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(
			ManagedControlsHook::get(), &ManagedControlsHook::buttonCheckChange);
		//button->Parent->Size = button->Size;
		button->ToggleState = getCoreStateByName(button->Name) ? Telerik::WinControls::Enumerations::ToggleState::On : Telerik::WinControls::Enumerations::ToggleState::Off;
	}

	void unsetToggleCheckButtonStyle(Telerik::WinControls::UI::RadToggleButton^ button){
		ManagedControlsHook::get()->removeToggleButtons(button);
	}


	Image ^ResizeImage(Image^ source, RectangleF destinationBounds)
	{
		RectangleF^ sourceBounds = gcnew RectangleF(0.0f, 0.0f, (float)source->Width, (float)source->Height);
		RectangleF^ scaleBounds = gcnew RectangleF();

		Image^ destinationImage = gcnew Bitmap((int)destinationBounds.Width, (int)destinationBounds.Height);
		Graphics^ graph = Graphics::FromImage(destinationImage);
		graph->InterpolationMode =
			System::Drawing::Drawing2D::InterpolationMode::HighQualityBicubic;

		// Fill with background color
		graph->FillRectangle(gcnew SolidBrush(System::Drawing::Color::Transparent), destinationBounds);

		float resizeRatio, sourceRatio;
		float scaleWidth, scaleHeight;

		sourceRatio = (float)source->Width / (float)source->Height;

		if (sourceRatio >= 1.0f)
		{
			//landscape
			resizeRatio = destinationBounds.Width / sourceBounds->Width;
			scaleWidth = destinationBounds.Width;
			scaleHeight = sourceBounds->Height * resizeRatio;
			float trimValue = destinationBounds.Height - scaleHeight;
			graph->DrawImage(source, 0, (uint32_t)(trimValue / 2), (uint32_t)destinationBounds.Width, (uint32_t)scaleHeight);
		}
		else
		{
			//portrait
			resizeRatio = destinationBounds.Height / sourceBounds->Height;
			scaleWidth = sourceBounds->Width * resizeRatio;
			scaleHeight = destinationBounds.Height;
			float trimValue = destinationBounds.Width - scaleWidth;
			graph->DrawImage(source, (uint32_t)(trimValue / 2), 0, (uint32_t)scaleWidth, (uint32_t)destinationBounds.Height);
		}

		return destinationImage;

	}


	int get_color_border_average(Bitmap^ bmp, Color color, int border_width){

		System::Drawing::Rectangle rect = System::Drawing::Rectangle(0, 0, bmp->Width, bmp->Height);
		auto bits_locked = bmp->LockBits(rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, System::Drawing::Imaging::PixelFormat::Format32bppArgb);
		IntPtr ptr = bits_locked->Scan0;
		uint32_t bytes = bmp->Width *  bmp->Height * 4;
		array<Byte>^rgbValues = gcnew array<Byte>(bytes);
		pin_ptr<unsigned char> p = &rgbValues[0];
		uint32_t* chaceMapLayerDataPtr = (uint32_t*)p;

		uint32_t totals[4];

		uint32_t width = bmp->Width;
		uint32_t height = bmp->Height;
		float total_count = 0;
		for (int x = 0; x < bmp->Width; x++){
			for (int y = 0; y < border_width; y++){
				int post_w = x;
				int post_h = y * width;

				if (color.ToArgb() == chaceMapLayerDataPtr[post_w + post_h]){
					totals[0]++;
				}
				total_count++;
			}
		}

		for (int x = 0; x < bmp->Width; x++){
			for (int y = bmp->Height - 1; y >(bmp->Height - 1) - border_width; y--){
				int post_w = x;
				int post_h = y * width;

				if (color.ToArgb() == chaceMapLayerDataPtr[post_w + post_h]){
					totals[1]++;
				}
				total_count++;
			}
		}

		for (int y = 0; y < bmp->Height; y++){
			for (int x = 0; x < border_width; x++){
				int post_w = x;
				int post_h = y * width;

				if (color.ToArgb() == chaceMapLayerDataPtr[post_w + post_h]){
					totals[2]++;
				}
				total_count++;
			}
		}

		for (int y = 0; y < bmp->Height; y++){
			for (int x = bmp->Width - 1; x >(bmp->Width - 1) - border_width; x--){
				int post_w = x;
				int post_h = y * width;

				if (color.ToArgb() == chaceMapLayerDataPtr[post_w + post_h]){
					totals[3]++;
				}
				total_count++;
			}
		}

		return (int)((float)(totals[0] + totals[1] + totals[2] + totals[3]) / total_count);
		System::Runtime::InteropServices::Marshal::Copy(rgbValues, 0, ptr, bytes);
		bmp->UnlockBits(bits_locked);
	}

	Bitmap^ get_color_average(Bitmap^ bmp){
		return nullptr;
	}

	Image^ make_transparent_background(Image^ in_out){
		Bitmap^bmp = gcnew Bitmap(in_out);
		System::Collections::Generic::List<System::Drawing::Color> colors;

		colors.Add(bmp->GetPixel(0, 0));
		colors.Add(bmp->GetPixel(0, bmp->Height));
		colors.Add(bmp->GetPixel(bmp->Width, 0));
		colors.Add(bmp->GetPixel(bmp->Width, bmp->Height));


		int equals_count = 0;
		for each(auto color in colors){
			for each(auto color2_loop in colors){
				if (color == color2_loop)
					equals_count++;
			}
			if (equals_count >= 3){

				break;
			}
		}
		return nullptr;
	}
	void init(){


	}

	ManagedGiffs::ManagedGiffs(){
		GifManager::get()->request_load_creatures();
		GifManager::get()->request_items_load();

		GifManager::get()->wait_load_creatures();
		GifManager::get()->wait_load_items();
	}

	void ManagedGiffs::update(){
	}

	Image^ ManagedGiffs::get_creature_image(String^ creature_image_name, uint32_t rect_sice_len){
		if (creature_image_name == "")
			return nullptr;

		creature_image_name = creature_image_name->ToLower();

		Image^ new_image;

		if (item_images->ContainsKey(creature_image_name)){
			new_image = item_images[creature_image_name];
		}

		if (!new_image){
			std::pair<char*, uint32_t> gif_pair = GifManager::get()->get_item_creature_buffer(managed_util::fromSS(creature_image_name) + ".gif");
			if (!gif_pair.first)
				return nullptr;

			System::IO::UnmanagedMemoryStream^ readStream =
				gcnew System::IO::UnmanagedMemoryStream
				((unsigned char*)gif_pair.first, gif_pair.second);

			item_images[creature_image_name] = new_image = System::Drawing::Image::FromStream(readStream);
		}

		if (new_image->Height > (float)rect_sice_len){
			auto ret = ResizeImage(new_image, System::Drawing::RectangleF(0, 0, (float)rect_sice_len, (float)rect_sice_len));
			return ret;
		}

		return new_image;
	}

	ManagedGiffs^ ManagedGiffs::get(){
		if (!mManagedGiffs)
			mManagedGiffs = gcnew ManagedGiffs();
		return mManagedGiffs;
	}

	Image^ ManagedGiffs::get_item_image(String^ item_image_name, uint32_t rect_sice_len){
		if (item_image_name == "")
			return nullptr;

		item_image_name = item_image_name->ToLower();
		Image^ new_image;
		if (item_images->ContainsKey(item_image_name)){
			new_image = item_images[item_image_name];
		}
		if (!new_image){
			std::pair<char*, uint32_t> gif_pair = GifManager::get()->get_item_gif_buffer(managed_util::fromSS(item_image_name) + ".gif");
			if (!gif_pair.first)
				return nullptr;

			System::IO::UnmanagedMemoryStream^ readStream =
				gcnew System::IO::UnmanagedMemoryStream
				((unsigned char*)gif_pair.first, gif_pair.second);

			item_images[item_image_name] = new_image = System::Drawing::Image::FromStream(readStream);
		}

		if (new_image->Height > (float)rect_sice_len){
			auto ret = ResizeImage(new_image, System::Drawing::RectangleF(0, 0, (float)rect_sice_len, (float)rect_sice_len));
			return ret;
		}

		return new_image;
	}
	
	Image^ get_item_image(String^ item_name, uint32_t rect_side_size){
		return ManagedGiffs::get()->get_item_image(item_name, rect_side_size);
	}

	Image^ get_creature_image(String^ creature, uint32_t rect_side_size){
		return ManagedGiffs::get()->get_creature_image(creature, rect_side_size);
	}

	bool check_exist_item(String^ item){
		auto array_itens = ItemsManager::get()->get_itens();
		auto it = std::find(array_itens.begin(), array_itens.end(), managed_util::fromSS(item));
		if (it != array_itens.end())
			return true;
		return false;
	}
}
