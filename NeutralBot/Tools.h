#pragma once
#include "Core\Login.h"
#include "ManagedUtil.h"
#include "Useful.h"
#include "Core\Util.h"
#include "ItemsInfo.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Tools : public Telerik::WinControls::UI::RadForm{
	public: Tools(void){
			InitializeComponent();
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}
	protected: ~Tools(){
				   unique = nullptr;
				   if (components)
					   delete components;
	}
	private: Telerik::WinControls::UI::RadButton^  bt_tibiamc;
	private: Telerik::WinControls::UI::RadButton^  bt_items;

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	public:

	protected: static Tools^ unique;

	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew Tools();

				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Tools();
	}
	public: static void CloseUnique(){
				if (unique)unique->Close();
	}


	public:

	protected:







	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void){
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Tools::typeid));
				 this->bt_tibiamc = (gcnew Telerik::WinControls::UI::RadButton());
				 this->bt_items = (gcnew Telerik::WinControls::UI::RadButton());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_tibiamc))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_items))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // bt_tibiamc
				 // 
				 this->bt_tibiamc->Location = System::Drawing::Point(12, 12);
				 this->bt_tibiamc->Name = L"bt_tibiamc";
				 this->bt_tibiamc->Size = System::Drawing::Size(116, 42);
				 this->bt_tibiamc->TabIndex = 1;
				 this->bt_tibiamc->Text = L"SmartMc";
				 this->bt_tibiamc->Click += gcnew System::EventHandler(this, &Tools::bt_tibiamc_Click);
				 // 
				 // bt_items
				 // 
				 this->bt_items->Location = System::Drawing::Point(134, 12);
				 this->bt_items->Name = L"bt_items";
				 this->bt_items->Size = System::Drawing::Size(116, 42);
				 this->bt_items->TabIndex = 2;
				 this->bt_items->Text = L"Items Manager";
				 this->bt_items->Click += gcnew System::EventHandler(this, &Tools::bt_items_Click);
				 // 
				 // Tools
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(265, 66);
				 this->Controls->Add(this->bt_items);
				 this->Controls->Add(this->bt_tibiamc);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->MaximizeBox = false;
				 this->Name = L"Tools";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"Tools";
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_tibiamc))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_items))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion
	private: System::Void bt_autorelogin_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void bt_tibiamc_Click(System::Object^  sender, System::EventArgs^  e) {
				 STARTUPINFO startupInfo = { 0 };
				 startupInfo.cb = sizeof(startupInfo);
				 PROCESS_INFORMATION processInformation;

				 memset(&processInformation, 0, sizeof(processInformation));
				 memset(&startupInfo, 0, sizeof(startupInfo));
				 char buf[MAX_PATH] = { 0 };

				 GetFullPathName(".\\TibiaMc.exe", MAX_PATH, buf, NULL);
				 BOOL result = ::CreateProcess(
					 buf,
					 NULL,
					 NULL,
					 NULL,
					 FALSE,
					 NORMAL_PRIORITY_CLASS,
					 NULL,
					 ".\\",
					 &startupInfo,
					 &processInformation
					 );
	}
	private: System::Void bt_useful_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew Useful)->ShowUnique();
	}
	private: System::Void bt_items_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew ItemsInfo)->ShowUnique();
	}
	};
}
