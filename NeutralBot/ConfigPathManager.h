#pragma once
#include "Core\Util.h"

enum property_t{
	property_none,
	property_free,
	property_block,
	property_trap,
	property_total
};

struct ConfigPathProperty{
	uint32_t path_id = 0;
	property_t property_type = property_none;
	std::string path_name = "";
	uint32_t walk_dificult = 0;

public:
	ConfigPathProperty(){
		 path_id = 0;
		 walk_dificult = 0;
		 property_type = property_none;
		 path_name = "";
	}

	DEFAULT_GET_SET(uint32_t, walk_dificult);
	DEFAULT_GET_SET(uint32_t, path_id);
	DEFAULT_GET_SET(property_t, property_type);
	DEFAULT_GET_SET(std::string, path_name);
};

class ConfigPathManager{
	uint32_t consider_near_yellow_coords = 6;
	uint32_t time_to_wait = 250;
	std::vector<std::shared_ptr<ConfigPathProperty>> vector_path_property;
	bool use_diagonal = false;

public:
	DEFAULT_GET_SET(bool, use_diagonal);
	DEFAULT_GET_SET(uint32_t, consider_near_yellow_coords);
	DEFAULT_GET_SET(uint32_t, time_to_wait);

	ConfigPathManager(){
		if (vector_path_property.size())
			return;

		creat_items_vector();
	}

	void creat_items_vector();

	void remove_to_vector_path(uint32_t index){
		if (index >= vector_path_property.size() || index < 0)
			return;

		vector_path_property.erase(vector_path_property.begin() + index);
	}

	void creat_new_config_path_property(){
		std::shared_ptr<ConfigPathProperty> newPath = std::shared_ptr<ConfigPathProperty>(new ConfigPathProperty());

		newPath->path_id = 0;
		newPath->property_type = (property_t)0;
		newPath->path_name = "New";

		vector_path_property.push_back(newPath);
	}
	
	std::vector<std::shared_ptr<ConfigPathProperty>> get_vector_path_property(){
		return vector_path_property;
	}

	std::shared_ptr<ConfigPathProperty> get_rule_path_property(uint32_t item_id){
		if (item_id <= 0 || item_id == UINT32_MAX)
			return nullptr;

		auto config_find = std::find_if(vector_path_property.begin(), vector_path_property.end(),
			[&](std::shared_ptr<ConfigPathProperty> mIt){
			return (mIt->get_path_id() == item_id);
		});

		if (config_find == vector_path_property.end())
			return nullptr;
		
		return *config_find;
	}

	std::shared_ptr<ConfigPathProperty> get_rule_path_property_by_index(uint32_t index){
		std::shared_ptr<ConfigPathProperty> rule_temp = vector_path_property[index];
		if (!rule_temp)
			return nullptr;
		 
		return rule_temp;
	}

	void clear(){
		vector_path_property.clear();
	}

	Json::Value parse_class_to_json(){
		Json::Value configPathManager;

		configPathManager["consider_near_yellow_coords"] = consider_near_yellow_coords;
		configPathManager["time_to_wait"] = time_to_wait;

		Json::Value vector_path_property_json;

		for (auto path_property : vector_path_property){
			Json::Value pathListItem;

			pathListItem["get_path_id"] = (int)path_property->get_path_id();
			pathListItem["get_path_name"] = path_property->get_path_name();
			pathListItem["get_property_type"] = (int)path_property->get_property_type();
			pathListItem["get_walk_dificult"] = (int)path_property->get_walk_dificult();

			vector_path_property_json.append(pathListItem);
		}

		configPathManager["vector_path_property_json"] = vector_path_property_json;
		return configPathManager;
	}

	void parse_json_to_class(Json::Value configPathManager){
		vector_path_property.clear();

		if (!configPathManager["vector_path_property_json"].isNull() && !configPathManager["vector_path_property_json"].empty()){
			Json::Value vector_path_property_json = configPathManager["vector_path_property_json"];

			for (auto member : vector_path_property_json){
				std::shared_ptr<ConfigPathProperty> newItem = std::shared_ptr<ConfigPathProperty>(new ConfigPathProperty());

				if (!member["get_path_id"].empty() || !member["get_path_id"].isNull())
					newItem->set_path_id(member["get_path_id"].asInt());
				
				if (!member["get_path_name"].empty() || !member["get_path_name"].isNull())
					newItem->set_path_name(member["get_path_name"].asString());

				if (!member["get_property_type"].empty() || !member["get_property_type"].isNull())
					newItem->set_property_type((property_t)member["get_property_type"].asInt());

				if (!member["get_walk_dificult"].empty() || !member["get_walk_dificult"].isNull())
					newItem->set_walk_dificult(member["get_walk_dificult"].asInt());

				vector_path_property.push_back(newItem);
			}

			if (vector_path_property.size())
				return;

			creat_items_vector();
		}
	}

	static ConfigPathManager* get(){
		static ConfigPathManager* m = nullptr;
		if (!m)
			m = new ConfigPathManager();
		return m;
	}
};