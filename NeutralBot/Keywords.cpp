#include "Keywords.h"

using namespace NeutralBot;

System::Void Keywords::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	std::vector<std::string> missings = KeywordManager::get()->check_missing(check_class->Checked,
		check_functions->Checked, check_tables->Checked, radCheckBox1->Checked);
	
	String^ text_missing = "";
	cli::array<String^>^ items = gcnew cli::array<String^>(missings.size());
	
	for (auto missing : missings)
		text_missing += "\n" + (gcnew String(&missing[0]));	

	richTextBox1->Text = text_missing;
}