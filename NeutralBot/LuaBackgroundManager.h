#pragma once
#include "Core\Util.h"
#include "Core\Time.h"
#include "Core\Util.h"


#pragma pack(push,1)


class LuaBackgroundCodes{
	neutral_mutex mtx_access;
	std::string InitCode;
	std::string name;
	std::string code;
	std::string refill_name;
	std::string itemname_orspellname;
	bool active;
	bool thread_on;
	bool _is_hud;
	uint32_t delay;
	uint32_t hast_code;
	uint32_t mana_use;

	TimeChronometer last_update;
	TimeChronometer last_init;
	TimeChronometer timer;
public:
	LuaBackgroundCodes();
	
	bool need_execute();
	
	int get_last_init_elapsed();
	int get_last_update_elapsed();
	int get_timer_elapsed();

	void reset_last_init();
	void reset_last_update();
	void reset_timer();

	uint32_t get_next_elapse_execution();
	uint32_t get_hast_code();
	void set_hast_code(uint32_t hast_code);
	uint32_t get_delay();

	uint32_t get_mana_use();
	void set_delay(uint32_t delay);

	void set_mana_use(uint32_t delay);

	void set_enabled(bool enabled);

	bool get_enabled();

	bool get_thread_on();
	
	void set_thread_on(bool in);

	bool is_running();

	void set_name(std::string name);

	std::string get_name();
	
	void set_refill_name(std::string code);

	void set_code(std::string code);

	std::string get_InitCode();

	void set_InitCode(std::string InitCode);

	std::string get_code();

	bool is_hud();

	void set_as_hud();

	bool get_active();

	std::string get_itemname_orspellname();

	void set_itemname_orspellname(std::string& in_itemname_orspellname);

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value& jsonObject);

};

class LuaBackgroundManager{
	neutral_mutex mtx_acess;
	std::map<std::string, std::shared_ptr<LuaBackgroundCodes>> mapBgLua;
	std::map<std::string, std::shared_ptr<LuaBackgroundCodes>> mapLuaTemporaryEvents;

	bool has_temporary_event;
	uint32_t temporary_hud_count = 0;
	uint32_t hud_count_active = 0;
	bool new_hud_redraw;
	bool hud_state = true;
	bool has_hud_event = false;

public:
	void clear();

	bool request_hud_redraw();
	
	std::map<std::string, std::shared_ptr<LuaBackgroundCodes>> getMap();

	LuaBackgroundManager();

	bool changeName(std::string current_id, std::string newId);

	bool removeLuaCode(std::string id_);

	std::string getLuaCode(std::string current_id);

	std::string getLuaInitCode(std::string current_id);

	std::string request_new_id(bool is_hud);

	std::shared_ptr<LuaBackgroundCodes> get_elapsed_time_event();

	void schedule_event(std::string event_id, uint32_t time_from_now, std::string _event, bool hud_type = false);

	void cancel_event(std::string event_id);

	uint32_t get_hud_count_active();

	void increase_active_huds();

	void decrease_active_huds();

	bool updateActive(std::string id, bool ative);

	bool updateInitCode(std::string id, std::string code);

	bool updateCode(std::string id, std::string code);

	bool updateDelay(std::string id, uint32_t delay);

	std::shared_ptr<LuaBackgroundCodes> getLuaScriptByName(std::string name);

	bool getLuaScriptActive(std::string name);

	void setLuaScriptDelay(std::string name, uint32_t delay);

	void setLuaScriptCode(std::string name, std::string code);

	uint32_t getLuaScriptDelay(std::string name);

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value jsonObject);
	
	bool get_hud_state();

	void set_hud_state(bool state);

	static LuaBackgroundManager* get();
};
#pragma pack(pop)




