#include "Core\Hotkey.h"
#include "Core\Input.h"
#include "Core\Map.h"
//#include "ManagedUtil.h"

std::string HotkeyState::getText(){
	hotkey_string[sizeof(hotkey_string) - 1] = 0;
	return hotkey_string;
}


bool HotkeyState::isAutoCast(){
	return auto_cast != 0;
}


uint32_t HotkeyState::getitem_id(){
	return item_id;
}


hotkey_cast_type_t HotkeyState::getCastType(){
	return (hotkey_cast_type_t)type_cast;
}


void HotkeyManager::add_in(int e){
	address_checked.push_back(e);
}


bool HotkeyManager::is_in(int e){
	return address_checked.end() != std::find(address_checked.begin(), address_checked.end(), e);
}


void HotkeyManager::clear_in(){
	address_checked.clear();
}


HotkeyManager::HotkeyManager(){
	
//	NeutralBot::FastUIPanelController::get()->install_controller(this);
	vector_size = sizeof(hotkeys);
}


HotkeyState HotkeyManager::getHotkey(hotkey_t type){
	if (type < hotkey_t::hotkey_f1 || type > hotkey_t::hotkey_f12_ctrl)
		return HotkeyState();
	refreshHotkeyInfo();
	return hotkeys[type - hotkey_f1];
}


hotkey_t HotkeyManager::findHotkeyByText(std::string& text,bool auto_cast){
	refreshHotkeyInfo();
	for (int i = 0; i < (hotkey_t_count); i = i + 1) {
		std::string hk_text = string_util::lower(hotkeys[i].getText());
		std::string hk_text_trim = string_util::trim(hk_text);
		std::string text_trim = string_util::trim(text);

		if (auto_cast){
			if (hk_text_trim == string_util::lower(text_trim) && hotkeys[i].isAutoCast())
				return (hotkey_t)(i + hotkey_f1);
		}else
			if (hk_text_trim == string_util::lower(text_trim))
				return (hotkey_t)(i + hotkey_f1);
	}
	return hotkey_none;
}


hotkey_t HotkeyManager::get_hotkey_match(std::string text, uint32_t item_id, hotkey_cast_type_t cast_type, bool auto_cast){
	if (text == "" && (item_id == 0 || item_id == UINT32_MAX))
		return hotkey_t::hotkey_none;
	char* text_as_ptr = 0;
	if (text != "")
		text_as_ptr = &text[0];

	auto found_hotkeys = query_hotkeys(text_as_ptr, item_id, cast_type, true, auto_cast);
	if (!found_hotkeys.size())
		return hotkey_none;
	return found_hotkeys[0];
}


hotkey_t HotkeyManager::get_key_code_hotkey(hotkey_t type){
	if (hotkey_t_count == type &&
		hotkey_any == type &&
		hotkey_none == type)
		return hotkey_none;
	if (type > hotkey_f12_shift)
		type = (hotkey_t)((uint32_t)hotkey_f12_shift - (uint32_t)type);
	else if (type > hotkey_f12)
		type = (hotkey_t)((uint32_t)hotkey_f12 - (uint32_t)type);
	return type;
}


std::vector<hotkey_t> HotkeyManager::query_hotkeys(char* str, uint32_t item_id, hotkey_cast_type_t cast_type,
	bool check_auto_cast, bool auto_cast){
	if ((!str || str == "") && (item_id == 0 || item_id == UINT32_MAX))
		return std::vector<hotkey_t>();

	std::vector<hotkey_t> retval;
	refreshHotkeyInfo();
	for (int i = 0; i < (hotkey_t_count); i = i + 1)
		if (!str || hotkeys[i].getText() == str)
			if (!item_id || hotkeys[i].getitem_id() == item_id)
				if (cast_type == hotkey_cast_any || hotkeys[i].getCastType() == cast_type)
					if (!check_auto_cast || auto_cast == hotkeys[i].isAutoCast())
						retval.push_back((hotkey_t)(i + hotkey_f1));
	return retval;
}


uint32_t HotkeyManager::getProfileCount(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_HOTKEYS_REFERENCE) + 4);
}


std::string HotkeyManager::getCurrentHotkeyProfileName(){
	bool is_ptr = TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_CURRENT_HOTKEY) + 20) != 15;
	std::string current;
	if (is_ptr){
		current = TibiaProcess::get_default()->read_string(TibiaProcess::get_default()->read_int(
			AddressManager::get()->getAddress(ADDRESS_CURRENT_HOTKEY)), false);
	}
	else{
		current = TibiaProcess::get_default()->read_string(
			AddressManager::get()->getAddress(ADDRESS_CURRENT_HOTKEY));
	}
	

	if (current != lastHotkeySchema){
		ZeroMemory(&hotkeys, vector_size);
		lastHotkeySchema = current;
	}
	return current;
}


int HotkeyManager::lookForAddressInThree(int Address, std::string Name, int BynaryThreeBegin, int& tg){
	tg++;
	bool is_ptr = TibiaProcess::get_default()->read_int(Address + OFFSET_INFO_HOTKEY_ADDRESS_PROFILE_NAME + 20, false) != 15;
	std::string name;
	if (is_ptr){
		name = TibiaProcess::get_default()->read_string(TibiaProcess::get_default()->read_int(Address + OFFSET_INFO_HOTKEY_ADDRESS_PROFILE_NAME, false), false);
	}
	else {
		name = TibiaProcess::get_default()->read_string(Address + OFFSET_INFO_HOTKEY_ADDRESS_PROFILE_NAME, false);
	}
	int compare_place = name.compare(Name);
	if (!compare_place){
		tg--;
		return  TibiaProcess::get_default()->read_int(Address + OFFSET_INFO_HOTKEY_ADDRESS_NEXT, false);
	}

	uint32_t threePointers[3];
	if (TibiaProcess::get_default()->read_memory_block(Address, (unsigned char*)threePointers, 12, false) != 12)
		return -1;

	for (int i = 0; i < 3; i++){
		if (threePointers[i] != BynaryThreeBegin && !is_in(threePointers[i])){
			add_in(threePointers[i]);
			int res = lookForAddressInThree(threePointers[i], Name, BynaryThreeBegin, tg);
			if (res != -1){
				tg--;
				return res;
			}
		}
	}
	tg--;
	return -1;
}


int HotkeyManager::getProfileAddressFromName(std::string schema_name){
	clear_in();
	int to_go = 0;
	int BynaryThreeBegin = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_HOTKEYS_REFERENCE));
	int ptrs[3];
	if (TibiaProcess::get_default()->read_memory_block(BynaryThreeBegin, ptrs, 12, false) != 12)
		return -1;

	for (int i = 0; i < 3; i++){
		if (ptrs[i] != BynaryThreeBegin){
			int res = lookForAddressInThree(ptrs[i], schema_name, BynaryThreeBegin, to_go);
			if (res != -1)
				return res;
		}
	}
	return -1;
}


void HotkeyManager::refreshHotkeyInfo(){
	if (TibiaProcess::get_default()->get_is_logged() == false)
		return;
	int address = getProfileAddressFromName(getCurrentHotkeyProfileName());
	TibiaProcess::get_default()->read_memory_block(address, (unsigned char*)&hotkeys[0], vector_size, false);
}


HotkeyManager* HotkeyManager::get(){
	static HotkeyManager* mHotkeyManager = nullptr;
	if (!mHotkeyManager)
		mHotkeyManager = new HotkeyManager;
	return mHotkeyManager;
}


bool HotkeyManager::useHotkey(hotkey_t type){
	bool ctrl = false;
	bool shift = false;

	if (type >= hotkey_t::hotkey_f1_ctrl){
		ctrl = true;
		if (type < hotkey_t::hotkey_f1_ctrl + 12)
			type = (hotkey_t)((int)(type)-(int)24);
		else 
			return false;
	}
	else if (type >= hotkey_t::hotkey_f1_shift){
		shift = true;
		type = (hotkey_t)((int)(type)-(int)12);
	} 

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->use_hotkey(type, ctrl, shift);
	return true;

}


bool HotkeyManager::useHotkeyAtLocation(hotkey_t type, Coordinate coordinate, uint32_t item_id, Coordinate axis_displacement){
	//AddressMouseState				= dectohex("0x717FC0")
	//checar hotkey TODO mouse state
	if (!useHotkey(type))
		return false;
	if (item_id != UINT32_MAX){
		if (TibiaProcess::get_default()->get_id_to_use() != item_id)
			return false;
	}

	//if (!gMapTilesPtr->check_sight_line(TibiaProcess::get_default()->character_info->get_self_coordinate(), coordinate))
	//	return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_tibia_coordinate_left(coordinate, axis_displacement);
	return true;
}


bool HotkeyManager::useItemAtLocation(uint32_t item_id, Coordinate coordinate, Coordinate axis_displacement){
	auto hotkeys = query_hotkeys(0, item_id, hotkey_cast_type_t::hotkey_cast_type_with_crosshairs);
	if (!hotkeys.size())
		return false;
	return useHotkeyAtLocation(hotkeys[0], coordinate, item_id, axis_displacement);
}


bool HotkeyManager::toggleEquipItem(uint32_t item_id){
	auto hotkeys = query_hotkeys(0, item_id, hotkey_cast_type_t::hotkey_cast_type_toggle_equip);
	if (hotkeys.size())
		return false;
	return useHotkey(hotkeys[0]);
}


bool HotkeyManager::useItemOnCrosshair(uint32_t item_id){
	auto hotkeys = query_hotkeys(0, item_id, hotkey_cast_type_t::hotkey_cast_type_with_crosshairs);
	if (!hotkeys.size())
		return false;
	return useHotkey(hotkeys[0]);

}
bool HotkeyManager::useItemOnSelf(uint32_t item_id){
	auto hotkeys = query_hotkeys(0, item_id, hotkey_cast_type_t::hotkey_cast_type_on_self);
	if (!hotkeys.size())
		return false;
	return useHotkey(hotkeys[0]);
}



