#include "AlertsManager.h"
#include "LanguageManager.h"
#include <boost\filesystem.hpp>

void AlertsStruct::parse_json_to_class(Json::Value& json){
	if (!json["logout"].empty() || !json["logout"].isNull())
		logout = json["logout"].asBool();

	if (!json["pause"].empty() || !json["pause"].isNull())
		pause = json["pause"].asBool();

	if (!json["sound"].empty() || !json["sound"].isNull())
		sound = json["sound"].asBool();

	if (!json["type"].empty() || !json["type"].isNull())
		type = (alert_t)json["type"].asInt();

	if (!json["value"].empty() || !json["value"].isNull())
	value = json["value"].asInt();
}

Json::Value AlertsStruct::parse_class_to_json(){
	Json::Value alertInfo;

	alertInfo["logout"] = get_logout();
	alertInfo["pause"] = get_pause();
	alertInfo["sound"] = get_sound();
	alertInfo["type"] = get_type();
	alertInfo["value"] = get_value();

	return alertInfo;
}

std::string AlertsStruct::to_string(){
	return AlertsManager::getAlertAsString(type);
}

void AlertsManager::updateitem(uint32_t id, std::string type, bool sound, bool pause, bool logout, int value){
	alertsmap[id]->set_type(alert_none);
	alertsmap[id]->set_sound(sound);
	alertsmap[id]->set_pause(pause);
	alertsmap[id]->set_logout(logout);
	updateValue(id, value);
}

uint32_t AlertsManager::CreatNewMapId(){
	int id = 0;
	while (alertsmap.find(id) != alertsmap.end())
		id++;	

	alertsmap[id] = std::shared_ptr<AlertsStruct>(new AlertsStruct);
	alertsmap[id]->set_type(alert_none);
	return id;
}

bool AlertsManager::creatNewMapById(uint32_t id){
	if (alertsmap.find(id) == alertsmap.end()){
		alertsmap[id] = std::shared_ptr<AlertsStruct>(new AlertsStruct);
		alertsmap[id]->set_type(alert_none);
		return true;
	}
	else
		return false;
}

std::shared_ptr<AlertsStruct> AlertsManager::GetRuleById(uint32_t id){
	auto it = alertsmap.find(id);
	if (it == alertsmap.end())
		return 0;
	return it->second;
}

bool AlertsManager::CheckMap(uint32_t id){
	auto it = alertsmap.find(id);
	if (it == alertsmap.end())
		return false;
	return true;
}

std::map<uint32_t, std::shared_ptr<AlertsStruct>> AlertsManager::GetMap(){
	return alertsmap;
}

void AlertsManager::deleteMap(uint32_t id_){
	auto i = alertsmap.find(id_);

	if (i != alertsmap.end())
		alertsmap.erase(i);
}

Json::Value AlertsManager::parse_class_to_json() {
	Json::Value alertManager;
	Json::Value alertList;

	for (auto alert : alertsmap)
		alertList[std::to_string(alert.first)] = alert.second->parse_class_to_json();

	alertManager["alertList"] = alertList;
	return alertManager;
}

void AlertsManager::parse_json_to_class(Json::Value jsonObject) {
	clear();

	if (!jsonObject["alertList"].empty() || !jsonObject["alertList"].isNull()){
		Json::Value alertManager = jsonObject["alertList"];

		Json::Value::Members members = alertManager.getMemberNames();
		for (auto member : members){
			std::shared_ptr<AlertsStruct> alert(new AlertsStruct);
			alert->parse_json_to_class(alertManager[member]);
			alertsmap[atoi(&member[0])] = alert;
		}
	}
}
bool AlertsManager::updateValue(uint32_t id, uint32_t value){
	auto it = alertsmap.find(id);

	if (it == alertsmap.end())
		return false;

	alertsmap[id]->value = value;
	return true;
}

AlertsManager::AlertsManager(){
}

void AlertsManager::clear(){
	alertsmap.clear();
}

void AlertsManager::reset(){
	mAlertsManager = std::shared_ptr<AlertsManager>(new AlertsManager);
}

std::string AlertsManager::getAlertAsString(alert_t type){
	return GET_TR("alert_t_" + std::to_string(type));
}

alert_t AlertsManager::getStringAsAlert(std::string alert_as_str){
	uint32_t retval_int = alert_t::alert_none;
	std::string reverse = GET_REVERSE_TR(alert_as_str);
	if (reverse.find("alert_t_") == std::string::npos)
		return (alert_t)retval_int;
	std::string num = reverse.substr(strlen("alert_t_"), reverse.length() - strlen("alert_t_"));
	if (num.length()){
		try{ retval_int = (alert_t)boost::lexical_cast<uint32_t>(num); }
		catch (...){}
	}
		
	return (alert_t)retval_int;
}

 std::shared_ptr<AlertsManager> AlertsManager::mAlertsManager;
