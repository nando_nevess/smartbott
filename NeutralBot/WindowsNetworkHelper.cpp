#include "WindowsNetworkHelper.h"
#include <windows.h>
#include <iphlpapi.h>
#include <vector>

#pragma comment(lib, "iphlpapi.lib")   

void killProcessConnections(uint32_t pid){
	std::vector <MIB_TCPROW_OWNER_PID >tablesToClose;


	MIB_TCPTABLE_OWNER_PID *pTCPInfo;
	MIB_TCPROW_OWNER_PID *owner;
	DWORD size;
	DWORD dwResult;

	dwResult = GetExtendedTcpTable(NULL, &size, false, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);
	pTCPInfo = (MIB_TCPTABLE_OWNER_PID*)malloc(size);
	dwResult = GetExtendedTcpTable(pTCPInfo, &size, false, AF_INET, TCP_TABLE_OWNER_PID_ALL, 0);

	if (dwResult != NO_ERROR)
		return;
	for (DWORD dwLoop = 0; dwLoop < pTCPInfo->dwNumEntries; dwLoop++){
		owner = &pTCPInfo->table[dwLoop];
		if (owner->dwOwningPid == pid){
			owner->dwState = MIB_TCP_STATE_DELETE_TCB;

			SetTcpEntry((MIB_TCPROW*)owner);
		}
	}
}