#pragma once
#include "Util.h"
#include "TibiaProcess.h"
#include "constants.h"

#define OFFSET_INFO_HOTKEY_ADDRESS_PROFILE_NAME 0x10
#define OFFSET_INFO_HOTKEY_ADDRESS_NEXT 0x28
#define OFFSET_INFO_IS_FATHER_NODE 0xc

#pragma pack(push,1)
class HotkeyState{
	char hotkey_string[256];
	unsigned char auto_cast;
	char other[3];
	uint32_t item_id;
	uint32_t idontknowwhatisthis;
	hotkey_cast_type_t type_cast;
public:
	std::string getText();

	bool isAutoCast();

	uint32_t getitem_id();

	hotkey_cast_type_t getCastType();
};

class HotkeyManager{
#pragma region PRIVATE
	HotkeyState hotkeys[hotkey_t_count];
	uint32_t vector_size;

	std::string lastHotkeySchema = "";

	/*Used by */
	std::vector<int> address_checked;
	void add_in(int e);
	bool is_in(int e);
	void clear_in();
#pragma endregion

public:
	HotkeyManager();

	HotkeyState getHotkey(hotkey_t type);

	hotkey_t findHotkeyByText(std::string& text, bool auto_cast = false);

	hotkey_t get_hotkey_match(std::string text = std::string(""), uint32_t item_id = 0, hotkey_cast_type_t cast_type = hotkey_cast_any, 
		bool auto_cast = false);

	hotkey_t get_key_code_hotkey(hotkey_t type);

	std::vector<hotkey_t> query_hotkeys(char* str = 0, uint32_t item_id = 0, hotkey_cast_type_t cast_type = hotkey_cast_any,
		bool check_auto_cast = false, bool auto_cast = false);

	uint32_t getProfileCount();

	std::string getCurrentHotkeyProfileName();

	int lookForAddressInThree(int Address, std::string Name, int BynaryThreeBegin, int& tg);

	int getProfileAddressFromName(std::string schema_name);

	void refreshHotkeyInfo();

	bool useHotkey(hotkey_t type);

	bool useHotkeyAtLocation(hotkey_t type, Coordinate coordinate, uint32_t item_id = UINT32_MAX, Coordinate axis_displacement = Coordinate());

	bool useItemAtLocation(uint32_t item_id, Coordinate coordinate, Coordinate axis_displacement = Coordinate());

	bool toggleEquipItem(uint32_t item_id);

	bool useItemOnCrosshair(uint32_t item_id);

	bool useItemOnSelf(uint32_t item_id);
	
	static HotkeyManager* get();
};





#pragma pack(pop)