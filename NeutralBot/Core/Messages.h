#pragma once
#include "Util.h"
#include "TibiaProcess.h"
#include <functional>

const int offset_between_message = 648;
const int offset_between_string = 40;

#pragma pack(push,1)
struct log_message{
	uint32_t total_messages;
	uint32_t last_sequencer_number;
	uint32_t time_left;/*100 ->> 10 seconds*/
	uint32_t idk3;
	uint32_t idk4;
	uint32_t idk5;
	char text[0x1FF];
};

struct tibia_message{
	uint32_t alive;
	uint32_t sequence_number;
	uint32_t time_left;
	uint32_t type;
	uint32_t x;
	uint32_t y;
	uint32_t z;
	int32_t next;/*total nao tenho certeza*/
	char text[600];
	uint32_t idk1;
	uint32_t idk2;
	uint32_t before_index;
	uint32_t next_index;
};
#pragma pack(pop)

#pragma pack(push,1)
class Messages{
	neutral_mutex mtx_access;
	Messages(){
	}
	int callback_id = 0;
public:
	std::vector<std::pair<int, std::function<void(std::string)>>> server_log_callbacks;
	std::vector<std::pair<int, std::function<void(std::string)>>> tibia_messsage_callbacks;
	std::vector<std::pair<int, std::function<void(std::string)>>> action_message_callbacks;

	tibia_message tibia_messages[9];
	log_message header;

	void run_async();

	int add_server_log_callbacks(std::function<void(std::string)> functionMessage);

	int add_tibia_message_callbacks(std::function<void(std::string)> functionMessage);

	int add_action_message_callbacks(std::function<void(std::string)> functionMessage);

	void on_server_log_message(std::string& message);

	void on_tibia_message(std::string& message);

	void on_action_message(std::string& message);

	void refresh_all();

	void refresh_server_log();

	void refresh_tibia_message();

	void refresh_header();

	void refresh_message_not_possible();

	void refresher();

	static void init();

	static Messages* mMessages;

	static Messages* get(){
		if (!mMessages)
			init();
		return mMessages;
	}


	void remove_action_message_callback(int id);
	void remove_server_message_callback(int id);
	void remove_tibia_message_callback(int id);

	static std::string get_tibia_message_where_contains(std::string str_to_find, uint32_t timeout = 2000,
		bool ignore_case = true);

	static std::string get_server_message_where_contains(std::string str_to_find, uint32_t timeout = 2000,
		bool ignore_case = true);

	static std::string get_action_message_where_contains(std::string str_to_find, uint32_t timeout = 2000,
		bool ignore_case = true);

};
#pragma pack(pop)

