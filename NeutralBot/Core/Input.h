#pragma once
#include "Util.h"
#include "TibiaProcess.h"
#include "AddressManager.h"
#include "Neutral.h"
#include "Interface.h"

#define MAX_LENGHT_TIBIA_MESSAGE 256

enum InputTypes{
	NO_TYPE,
	SINGLE_INPUT,
	MOUSE_SINGLE_KEY_PRESS
};

class BasicInput{
public:
	virtual void _click_right(uint16_t x, uint16_t y, bool ctrl = false, bool shift = false) = 0;
	virtual void _click_left(uint16_t x, uint16_t y, bool ctrl = false, bool shift = false) = 0;
	virtual void _mouse_right_down(uint16_t x, uint16_t y) = 0;
	virtual void _mouse_right_up(uint16_t x, uint16_t y) = 0;
	virtual void _mouse_left_down(uint16_t x, uint16_t y) = 0;
	virtual void _mouse_left_up(uint16_t x, uint16_t y) = 0;
	virtual void _mouse_move(uint16_t x, uint16_t y) = 0;
	virtual void _mouse_scroll_up(uint16_t x, uint16_t y, uint16_t count) = 0;
	virtual void _mouse_scroll_down(uint16_t x, uint16_t y, uint16_t count) = 0;
	virtual void _send_char(uint8_t value, LPARAM lparam) = 0;
	virtual void _send_string(std::string value) = 0;
	virtual void _send_virtual_key(uint32_t vk) = 0;
	virtual void _send_multiple_keys(std::vector<uint32_t> vks) = 0;

	virtual void _arrow_up() = 0;
	virtual void _arrow_right() = 0;
	virtual void _arrow_down() = 0;
	virtual void _arrow_left() = 0;

	virtual void _arrow_up_left() = 0;
	virtual void _arrow_up_rigth() = 0;
	virtual void _arrow_down_left() = 0;
	virtual void _arrow_down_right() = 0;
};

#ifdef SAFE
class SafeInput : public InputLocker, BasicInput{
public:
	static HWND CurrentHWND;

	void set_input_handler(HWND handle){
		CurrentHWND = handle;
	}
	SafeInput(bool useSecure){
		isSecureMode = useSecure;
	}
	//SYSTEM
	static const POINT getMousePos(){
		const POINT out;
		GetCursorPos((LPPOINT)&out);
		return out;
	}

	static bool isClassicControl(){
		return true;
	}

	//BOTH
	static bool setCtrlState(bool state){
		INPUT ip;
		ip.type = INPUT_KEYBOARD;
		ip.ki.wScan = 0;
		ip.ki.time = 0;
		ip.ki.dwExtraInfo = 0;
		if (state){
			ip.ki.wVk = VK_CONTROL;
			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));
			return true;
		}
		else{
			ip.ki.wVk = VK_CONTROL;
			ip.ki.dwFlags = KEYEVENTF_KEYUP;
			SendInput(1, &ip, sizeof(INPUT));
			return true;
		}
	}

	static bool setShiftState(bool state){
		INPUT ip;
		ip.type = INPUT_KEYBOARD;
		ip.ki.wScan = 0;
		ip.ki.time = 0;
		ip.ki.dwExtraInfo = 0;
		if (state){
			ip.ki.wVk = VK_SHIFT;
			ip.ki.dwFlags = 0; // 0 for key press
			SendInput(1, &ip, sizeof(INPUT));
			return true;
		}
		else{
			ip.ki.wVk = VK_SHIFT;
			ip.ki.dwFlags = KEYEVENTF_KEYUP;
			SendInput(1, &ip, sizeof(INPUT));
			return true;
		}
	}

	//MOUSE


	static bool sendMouseTo(COORD * pos){
		int try_times = 0;
		COORD tibiaCoord = getTibiaWindowCoord();
		POINT mousePos = getMousePos();
		while (mousePos.x != (tibiaCoord.X + pos->X) && mousePos.y != (tibiaCoord.Y + pos->Y)){

			int difx = mousePos.x - (tibiaCoord.X + pos->X);
			int dify = mousePos.y - (tibiaCoord.Y + pos->Y);
			if (difx > 0)
				difx = min(rand() % 10 + 5, difx);
			else if (difx < 0)
				difx = max(-(rand() % 10 + 5), dify);



			if (dify > 0)
				dify = min(rand() % 10 + 5, dify);
			else if (difx < 0)
				dify = max(-(rand() % 10 + 5), dify);

			INPUT input;
			input.type = INPUT_MOUSE;
			input.mi.mouseData = 0;
			input.mi.dx = (mousePos.x + difx)*(65536 / GetSystemMetrics(SM_CXSCREEN));//x being coord in pixels
			input.mi.dy = (mousePos.y + dify)*(65536 / GetSystemMetrics(SM_CYSCREEN));//y being coord in pixels
			input.mi.dwFlags = MOUSEEVENTF_ABSOLUTE | MOUSEEVENTF_MOVE;
			SendInput(1, &input, sizeof(input));

			mousePos = getMousePos();
			tibiaCoord = getTibiaWindowCoord();
			if (try_times++ > 10)
				return false;
		}
		return true;
	}

	static bool sendMouseDownLeft(COORD * pos){
		if (!sendMouseTo(pos))
			return false;

		COORD tibiaCoord;
		tibiaCoord = getTibiaWindowCoord();
		INPUT input;
		input.type = INPUT_MOUSE;
		input.mi.mouseData = 0;
		input.mi.dx = (tibiaCoord.X + pos->X)*(65536 / GetSystemMetrics(SM_CXSCREEN));//x being coord in pixels
		input.mi.dy = (tibiaCoord.Y + pos->Y)*(65536 / GetSystemMetrics(SM_CYSCREEN));//y being coord in pixels
		input.mi.dwFlags = MOUSEEVENTF_LEFTDOWN;
		SendInput(1, &input, sizeof(input));
		return true;
	}

	static bool sendMouseUpLeft(COORD * pos){
		if (!sendMouseTo(pos))
			return false;
		COORD tibiaCoord;
		tibiaCoord = getTibiaWindowCoord();
		INPUT input;
		input.type = INPUT_MOUSE;
		input.mi.mouseData = 0;
		input.mi.dx = (tibiaCoord.X + pos->X)*(65536 / GetSystemMetrics(SM_CXSCREEN));//x being coord in pixels
		input.mi.dy = (tibiaCoord.Y + pos->Y)*(65536 / GetSystemMetrics(SM_CYSCREEN));//y being coord in pixels
		input.mi.dwFlags = MOUSEEVENTF_LEFTUP;
		SendInput(1, &input, sizeof(input));
		return true;
	}

	static bool sendMouseDownRight(COORD * pos){
		if (!sendMouseTo(pos))
			return false;

		COORD tibiaCoord;
		tibiaCoord = getTibiaWindowCoord();
		INPUT input;
		input.type = INPUT_MOUSE;
		input.mi.mouseData = 0;
		input.mi.dx = (tibiaCoord.X + pos->X)*(65536 / GetSystemMetrics(SM_CXSCREEN));//x being coord in pixels
		input.mi.dy = (tibiaCoord.Y + pos->Y)*(65536 / GetSystemMetrics(SM_CYSCREEN));//y being coord in pixels
		input.mi.dwFlags = MOUSEEVENTF_RIGHTDOWN;
		SendInput(1, &input, sizeof(input));
		return true;
	}

	static bool sendMouseUpRight(COORD * pos){
		if (!sendMouseTo(pos))
			return false;
		COORD tibiaCoord;
		tibiaCoord = getTibiaWindowCoord();
		INPUT input;
		input.type = INPUT_MOUSE;
		input.mi.mouseData = 0;
		input.mi.dx = (tibiaCoord.X + pos->X)*(65536 / GetSystemMetrics(SM_CXSCREEN));//x being coord in pixels
		input.mi.dy = (tibiaCoord.Y + pos->Y)*(65536 / GetSystemMetrics(SM_CYSCREEN));//y being coord in pixels
		input.mi.dwFlags = MOUSEEVENTF_RIGHTUP;
		SendInput(1, &input, sizeof(input));
	}

	static bool sendClickLeft(COORD * pos, bool shift, bool ctrl){
		if (!sendMouseTo(pos))
			return false;
		if (ctrl)
			setCtrlState(true);
		if (shift)
			setShiftState(true);

		sendMouseDownLeft(pos);
		sendMouseUpLeft(pos);

		if (ctrl)
			setCtrlState(false);
		if (shift)
			setShiftState(false);
		return true;
	}

	static bool sendClickRight(COORD * pos, bool shift, bool ctrl){
		if (!sendMouseTo(pos))
			return false;

		if (ctrl)
			setCtrlState(true);
		if (shift)
			setShiftState(true);

		sendMouseDownRight(pos);
		sendMouseUpRight(pos);

		if (ctrl)
			setCtrlState(false);
		if (shift)
			setShiftState(false);

		return true;
	}

	static bool sendWheelCount(COORD * pos, int count){
		COORD tibiaCoord = getTibiaWindowCoord();
		for (int i = 0; i < abs(count); i++){
			if (!sendMouseTo(pos))
				return false;

			INPUT Input;
			Input.mi.dwFlags = MOUSEEVENTF_WHEEL;

			Input.mi.dx = (tibiaCoord.X + pos->X)*(65536 / GetSystemMetrics(SM_CXSCREEN));;
			Input.mi.dy = (tibiaCoord.Y + pos->Y)*(65536 / GetSystemMetrics(SM_CXSCREEN));;
			Input.mi.time = 0;
			Input.mi.dwExtraInfo = 0;
			Input.mi.mouseData = count > 0 ? 1 : (-count) * 120;   // since someonce said a wheel tick equals 120

			SendInput(1, &Input, sizeof(INPUT));
		}

		return true;
	}

	static bool sendDragItem(COORD* from, COORD* to, bool ctrl, bool shift, int item_id){
		if (ctrl)
			setCtrlState(true);
		if (shift)
			setShiftState(true);
		if (!sendMouseDownLeft(from))
			return false;
		/*if (!check_if_is_id(item_id)){
			sendMouseUpLeft(from);
			if (ctrl)
			setCtrlState(false);
			if (shift)
			setShiftState(false);
			return false;
			}*/
		sendMouseUpLeft(from);
		if (ctrl)
			setCtrlState(false);
		if (shift)
			setShiftState(false);
		return true;

	}

	static bool sendKeyDown(int vkCode){
		INPUT ip;
		ip.type = INPUT_KEYBOARD;
		ip.ki.wScan = 0;
		ip.ki.time = 0;
		ip.ki.dwExtraInfo = 0;

		ip.ki.wVk = vkCode;
		ip.ki.dwFlags = 0; // 0 for key press
		SendInput(1, &ip, sizeof(INPUT));
		return true;

	}

	static bool sendKeyUp(int vkCode){
		INPUT ip;
		ip.type = INPUT_KEYBOARD;
		ip.ki.wScan = 0;
		ip.ki.time = 0;
		ip.ki.dwExtraInfo = 0;
		ip.ki.wVk = vkCode;
		ip.ki.dwFlags = KEYEVENTF_KEYUP;
		SendInput(1, &ip, sizeof(INPUT));
		return true;
	}

	static bool sendWrite(char character){
		INPUT ip;

		ip.type = INPUT_KEYBOARD;
		ip.ki.wScan = 0; // hardware scan code for key
		ip.ki.time = 0;
		ip.ki.dwExtraInfo = 0;


		ip.ki.wVk = VkKeyScan(character); // virtual-key code for the "a" key
		ip.ki.dwFlags = 0; // 0 for key press
		SendInput(1, &ip, sizeof(INPUT));

		// Release the "A" key
		ip.ki.dwFlags = KEYEVENTF_KEYUP; // KEYEVENTF_KEYUP for key release
		SendInput(1, &ip, sizeof(INPUT));

		// Exit normally
		return 0;
	}

	static bool sendWrite(std::string word){
		for (auto it : word)
			sendWrite(it);
	}

	static bool sendWrite(int number){
		/*	std::stringstream os;
			os << number;
			sendWrite(os.str());*/
	}

	COORD getMinimapCoord(){
	}

	int getMinimapZoom(){
	}

	bool setMinimapZoom(){

	}

	COORD getMinimapCenter(){
		//tem que ser estilo 3 senao fica mto pequeno
		//1 pra 1 sqm
		//talvez stilo 4 
		// 1 pixel pra 2 sqm
		// mapa 107 pixel * 107 pixel
	}

	static bool sendGotoMapClick(){
	}

	static void setSecureMode(){
		isSecureMode = true;
	}

	static void setNomSecureMode(){
		isSecureMode = false;
	}

	static bool isSecureMode;
};
#endif

class VirtualInput : public BasicInput {
protected:
	uint32_t pid;
	uint32_t window_handler;
	std::shared_ptr<TibiaProcess> this_process;
public:
	VirtualInput();

	void _set_pid(uint32_t pid);
	uint32_t _get_pid();
	virtual void _click_right(uint16_t x, uint16_t y, bool ctrl = false, bool shift = false);
	void _click_right(Point& point, bool ctrl = false, bool shift = false);
	virtual void _click_left(uint16_t x, uint16_t y, bool ctrl = false, bool shift = false);
	void _click_left(Point& point, bool ctrl = false, bool shift = false);
	virtual void _mouse_right_down(uint16_t x, uint16_t y);
	void _mouse_right_down(Point& point);
	virtual void _mouse_right_up(uint16_t x, uint16_t y);
	void _mouse_right_up(Point& point);
	virtual void _mouse_left_down(uint16_t x, uint16_t y);
	void _mouse_left_down(Point& point);
	virtual void _mouse_left_up(uint16_t x, uint16_t y);
	void _mouse_left_up(Point& point);
	virtual void _mouse_move(uint16_t x, uint16_t y);
	void _mouse_move(Point& point);
	virtual void _mouse_scroll_up(uint16_t x, uint16_t y, uint16_t count);
	void _mouse_scroll_up(Point& point, uint16_t count);
	virtual void _mouse_scroll_down(uint16_t x, uint16_t y, uint16_t count);
	void _mouse_scroll_down(Point& point, uint16_t count);
	virtual void _send_char(uint8_t value, LPARAM lparam = 0);
	virtual void _send_string(std::string value);
	virtual void _send_virtual_key(uint32_t vk);
	virtual void _send_multiple_keys(std::vector<uint32_t> vks);
	virtual void _arrow_up();
	virtual void _arrow_right();
	virtual void _arrow_down();
	virtual void _arrow_left();
	virtual void _arrow_up_left();
	virtual void _arrow_up_rigth();
	virtual void _arrow_down_left();
	virtual void _arrow_down_right();
	void _send_esc();
	void _send_enter();
	void _sent_tab();
	void _send_ctrl_down();
	void _send_ctrl_up();
	void _press_ctrl_and_shift();
	void _send_shift_down();
	void _send_shift_up();
	bool _is_control_pressed();
	bool _is_shift_pressed();
	void _write_mouse_pos(uint16_t x, uint16_t y);
	void _release_mouse_buttons(bool left, bool right, uint16_t x = 5, uint16_t y = 5);
	bool _is_mouse_pressed();
	void _release_basic_buttons();
	LRESULT _mSendMessage(UINT Msg, WPARAM wParam, LPARAM lParam);
	LRESULT _mPostMessage(UINT Msg, WPARAM wParam, LPARAM lParam);
	bool _can_click(uint16_t x, uint16_t y);
	void _fix_click_coordinate(uint16_t& x, uint16_t& y);
};



class HighLevelVirtualInput : private VirtualInput {

	bool using_input = false;
	neutral_mutex mxt_access;
	int time_out = 1000;
public:
	static HighLevelVirtualInput* get_default();

	bool can_use_input();

	void enable_using_input();
	void disable_using_input();

	int get_time_out();

	void use_hotkey(hotkey_t type, bool ctrl = false, bool shift = false);
	void turn(side_nom_axis_t::side_nom_axis_t type){
		enable_using_input();
		release_basic_buttons();

		send_ctrl_down();

		switch (type){
		case side_nom_axis_t::side_east:{
											HighLevelVirtualInput::get_default()->arrow_right();

		}break;
		case side_nom_axis_t::side_north:{
											 HighLevelVirtualInput::get_default()->arrow_up();

		}break;
		case side_nom_axis_t::side_south:{
											 HighLevelVirtualInput::get_default()->arrow_down();
		}break;
		case side_nom_axis_t::side_west:{
											HighLevelVirtualInput::get_default()->arrow_left();
		}break;
		default:
			break;
		}

		send_ctrl_up();

		disable_using_input();
	}
	void scroll_count(uint32_t count_now, uint32_t count);

	bool look_pixel(uint16_t x, uint16_t y);
	bool look_pixel(Point& point);

	bool drag_mouse(Point& point_from, Point& point_to, bool ctrl = false);

	bool drag_item_on_screen(Point& point_from/*coordinate on screen in pixels*/, Point& point_to, bool ctrl,
		uint32_t item_id = UINT32_MAX, uint16_t count = 100);

	bool look_at_game_coordinate(Point& point);

	void send_escape_button();

	void get_mouse_location(int &x, int &y);

	void click_tibia_coordinate_right(Coordinate coord, Coordinate deslocation_from_axis = Coordinate(), bool ctrl = false, bool shift = false);
	void click_tibia_coordinate_left(Coordinate coord, Coordinate deslocation_from_axis = Coordinate(), bool ctrl = false, bool shift = false);
	void click_tile_left(Coordinate coord, Coordinate deslocation_from_axis = Coordinate(), bool ctrl = false, bool shift = false);

	//VirtualInput
	void set_pid(uint32_t pid);
	uint32_t get_pid();
	virtual void click_right(uint16_t x, uint16_t y, bool ctrl = false, bool shift = false);
	void click_right(Point& point, bool ctrl = false, bool shift = false);
	virtual void click_left(uint16_t x, uint16_t y, bool ctrl = false, bool shift = false);
	void click_left(Point& point, bool ctrl = false, bool shift = false);
	virtual void mouse_right_down(uint16_t x, uint16_t y);
	void mouse_right_down(Point& point);
	virtual void mouse_right_up(uint16_t x, uint16_t y);
	void mouse_right_up(Point& point);
	void mouse_left_down(uint16_t x, uint16_t y);
	void mouse_left_down(Point& point);
	virtual void mouse_left_up(uint16_t x, uint16_t y);
	void mouse_left_up(Point& point);
	virtual void mouse_move(uint16_t x, uint16_t y);
	void mouse_move(Point& point);
	virtual void mouse_scroll_up(uint16_t x, uint16_t y, uint16_t count);
	void mouse_scroll_up(Point& point, uint16_t count);
	virtual void mouse_scroll_down(uint16_t x, uint16_t y, uint16_t count);
	void mouse_scroll_down(Point& point, uint16_t count);
	virtual void send_char(uint8_t value, LPARAM lparam = 0);
	void send_char(std::vector<uint8_t> valueList, LPARAM lparam = 0);
	virtual void send_string(std::string value);
	virtual void send_virtual_key(uint32_t vk);
	virtual void send_multiple_keys(std::vector<uint32_t> vks);
	virtual void arrow_up();
	virtual void arrow_right();
	virtual void arrow_down();
	virtual void arrow_left();
	virtual void arrow_up_left();
	virtual void arrow_up_rigth();
	virtual void arrow_down_left();
	virtual void arrow_down_right();
	void send_esc();
	void send_enter();
	void sent_tab();
	void send_ctrl_down();
	void send_ctrl_up();
	void press_ctrl_and_shift();
	void send_shift_down();
	void send_shift_up();
	bool is_control_pressed();
	bool is_shift_pressed();
	void write_mouse_pos(uint16_t x, uint16_t y);
	void release_mouse_buttons(bool left, bool right, uint16_t x = 5, uint16_t y = 5);
	bool is_mouse_pressed();
	void release_basic_buttons();
	LRESULT mSendMessage(UINT Msg, WPARAM wParam, LPARAM lParam);
	LRESULT mPostMessage(UINT Msg, WPARAM wParam, LPARAM lParam);
	bool can_click(uint16_t x, uint16_t y);


};


