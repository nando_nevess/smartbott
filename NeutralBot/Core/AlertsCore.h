#pragma once
#include "..\AlertsManager.h"
#include "Util.h"
#include "Actions.h"
#include "InfoCore.h"
#include "..\\CoreBase.h"

class AlertsCore : public CoreBase{
	neutral_mutex mtx_access;

public:
	AlertsCore();

	void refresh();

	void run();

	void play_sound(alert_t type);

	void loggout(){
		Actions::loggout();
	}

	void pause_bot(){
		NeutralManager::get()->set_pause_state(true);
	}

	bool need_execute(alert_t type, int value = 0){
		switch (type){
		case alert_t::alert_player_attack:
			return InfoCore::get()->is_player_attack();
			break;
		case alert_t::alert_player_on_screen:
			return BattleList::get()->have_player();
			break;
		case alert_t::alert_cap:
			return TibiaProcess::get_default()->character_info->cap() < (uint32_t)value;
			break;
		case alert_t::alert_disconnect:
			return !TibiaProcess::get_default()->get_is_logged();
			break;
		case alert_t::alert_player_kill:
			return BattleList::get()->player_kill_on_screen(true,true);
			break;
		case alert_t::alert_low_health:
			return (TibiaProcess::get_default()->character_info->hppc() <= (uint32_t)value);			
			break;
			return false;
		}
		return false;
	}

	static AlertsCore* get(){
		static AlertsCore* m = nullptr;
		if (!m)
			m = new AlertsCore();
		return m;
	}
};