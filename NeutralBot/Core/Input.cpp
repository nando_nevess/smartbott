#include "TibiaProcess.h"
#include "Input.h"
#include "Interface.h"

#define INPUT_LOG FALSE
#if INPUT_LOG
#define LOG_INPUT std::cout << "\n " << GetCurrentThreadId() << "|" << __FUNCTION__;
#else
#define LOG_INPUT
#endif

VirtualInput::VirtualInput(){
	this_process = TibiaProcess::get_default();
}
void VirtualInput::_set_pid(uint32_t pid){
	this->pid = pid;
	window_handler = this_process->character_info->get_window_handler();
}
uint32_t VirtualInput::_get_pid(){
	return pid;
}
void VirtualInput::_click_right(uint16_t x, uint16_t y, bool ctrl, bool shift) {
	if (!this_process->get_is_logged())
		return;

	//if (!_can_click(x, y))
	//	return;
	_fix_click_coordinate(x, y);
	_release_mouse_buttons(true, true);

	if (ctrl)
		_send_ctrl_down();

	if (shift)
		_send_shift_down();

	_mouse_move(x, y);
	_mSendMessage(WM_NCHITTEST, NULL, MAKELPARAM(x, y));
	_mSendMessage(WM_SETCURSOR, window_handler, WM_RBUTTONDOWN);
	_mSendMessage(WM_RBUTTONDOWN, MK_RBUTTON, MAKELPARAM(x, y));
	_mSendMessage(WM_RBUTTONUP, MK_RBUTTON, MAKELPARAM(x, y));
	_mouse_move(x, y);
	if (shift)
		_send_shift_up();
	if (ctrl)
		_send_ctrl_up();
}
void VirtualInput::_click_right(Point& point, bool ctrl, bool shift){
	_click_right(point.x, point.y, ctrl, shift);
}
void VirtualInput::_click_left(uint16_t x, uint16_t y, bool ctrl, bool shift){
	/*if (!_can_click(x, y)) {
		return;
	}*/
	_fix_click_coordinate(x, y);

	_release_mouse_buttons(true, true);
	_mouse_move(x, y);

	_mSendMessage(WM_NCHITTEST, NULL, MAKELPARAM(x, y));
	_mSendMessage(WM_SETCURSOR, window_handler, WM_LBUTTONDOWN);
	_mSendMessage(WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x, y));
	_mSendMessage(WM_LBUTTONUP, MK_LBUTTON, MAKELPARAM(x, y));
}
void VirtualInput::_click_left(Point& point, bool ctrl, bool shift){
	_click_left(point.x, point.y, ctrl, shift);
}
void VirtualInput::_mouse_right_down(uint16_t x, uint16_t y) {
	if (!this_process->get_is_logged())
		return;
	//if (!_can_click(x, y))
	//	return;
	_fix_click_coordinate(x, y);
	_mouse_move(x, y);
	_mSendMessage(WM_NCHITTEST, NULL, MAKELPARAM(x, y));
	_mSendMessage(WM_SETCURSOR, (DWORD)window_handler, WM_RBUTTONDOWN);
	_mSendMessage(WM_RBUTTONDOWN, MK_RBUTTON, MAKELPARAM(x, y));
}
void VirtualInput::_mouse_right_down(Point& point) {
	_mouse_right_down(point.x, point.y);
}
void VirtualInput::_mouse_right_up(uint16_t x, uint16_t y) {
	if (!this_process->get_is_logged())
		return;
	//if (!_can_click(x, y))
	//	return;
	_fix_click_coordinate(x, y);
	_mouse_move(x, y);
	_mSendMessage(WM_NCHITTEST, NULL, MAKELPARAM(x, y));
	_mSendMessage(WM_SETCURSOR, (DWORD)window_handler, WM_RBUTTONDOWN);
	_mSendMessage(WM_RBUTTONUP, MK_RBUTTON, MAKELPARAM(x, y));
}
void VirtualInput::_mouse_right_up(Point& point) {
	_mouse_right_up(point.x, point.y);
}
void VirtualInput::_mouse_left_down(uint16_t x, uint16_t y) {
	if (!this_process->get_is_logged())
		return;

	//if (!_can_click(x, y))
	//	return;
	_fix_click_coordinate(x, y);
	_mouse_move(x, y);
	_mSendMessage(WM_NCHITTEST, NULL, MAKELPARAM(x, y));
	_mSendMessage(WM_SETCURSOR, (DWORD)window_handler, WM_LBUTTONDOWN);
	_mSendMessage(WM_LBUTTONDOWN, MK_LBUTTON, MAKELPARAM(x, y));
}
void VirtualInput::_mouse_left_down(Point& point) {
	_mouse_left_down(point.x, point.y);
}
void VirtualInput::_mouse_left_up(uint16_t x, uint16_t y) {
	if (NeutralManager::get()->is_paused() || this_process->get_is_logged() == false/* || !_can_click(x, y)*/)
		return;
	_fix_click_coordinate(x, y);
	_mouse_move(x, y);

	_mSendMessage(WM_NCHITTEST, NULL, MAKELPARAM(x, y));
	_mSendMessage(WM_SETCURSOR, (DWORD)window_handler, WM_LBUTTONDOWN);
	_mSendMessage(WM_LBUTTONUP, MK_LBUTTON, MAKELPARAM(x, y));
}
void VirtualInput::_mouse_left_up(Point& point) {
	_mouse_left_up(point.x, point.y);
}
void VirtualInput::_mouse_move(Point& point) {
	_mouse_move(point.x, point.y);
}
void VirtualInput::_mouse_scroll_up(Point& point, uint16_t count) {
	_mouse_scroll_up(point.x, point.y, count);
}
void VirtualInput::_mouse_scroll_down(Point& point, uint16_t count){
	_mouse_scroll_down(point.x, point.y, count);
}
bool VirtualInput::_is_mouse_pressed(){
	return (this_process->read_int(AddressManager::get()->getAddress(ADDRESS_MOUSE)) != 0 ||
		this_process->read_int(AddressManager::get()->getAddress(ADDRESS_MOUSE_2)) != 0);
}
void VirtualInput::_release_basic_buttons(){
	_mSendMessage(WM_KEYUP, 0x26, NULL); // S keyup
	_mSendMessage(WM_KEYUP, 0x27, NULL); // S keyup
	_mSendMessage(WM_KEYUP, 0x28, NULL); // S keyup
	_mSendMessage(WM_KEYUP, 0x25, NULL); // S keyup
}
bool VirtualInput::_can_click(uint16_t x, uint16_t y){
	Coordinate size = ClientGeneralInterface::get()->get_client_dimension();
	if (x < 0 || y < 0)
		return false;
	else if (size.x < x || size.y < y)
		return false;
	return true;
}

void VirtualInput::_fix_click_coordinate(uint16_t& x, uint16_t& y){
	if ((int16_t)x < 1)
		x = 1;
	if ((int16_t)y < 1)
		y = 1;

	Coordinate size = ClientGeneralInterface::get()->get_client_dimension();

	if (size.x <= x)
		x = size.x - 1;
	if (size.y <= y)
		y = size.y - 1;
}

void VirtualInput::_release_mouse_buttons(bool left, bool right, uint16_t x, uint16_t y){
	if (!this_process->get_is_logged())
		return;

	_mouse_move(x, y);

	_mSendMessage(WM_SETCURSOR, (DWORD)window_handler, WM_LBUTTONDOWN);
	if (left && right){
		_mSendMessage(WM_LBUTTONUP, MK_LBUTTON, MAKELPARAM(x, y));
		_mSendMessage(WM_RBUTTONUP, MK_RBUTTON, MAKELPARAM(x, y));
	}
	else if (left){
		_mSendMessage(WM_LBUTTONUP, MK_LBUTTON, MAKELPARAM(x, y));
	}
	else if (right){
		_mSendMessage(WM_RBUTTONUP, MK_RBUTTON, MAKELPARAM(x, y));
	}
}
void VirtualInput::_mouse_move(uint16_t x, uint16_t y){
	if (!this_process->get_is_logged())
		return;
	_fix_click_coordinate(x, y);
	//if (!_can_click(x, y))
		//return;
	int begin_x = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_MOUSE1_X));
	int begin_y = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_MOUSE1_Y));
	_write_mouse_pos((begin_x + x) / 2, (begin_y + y) / 2);
	Sleep(10);
	_write_mouse_pos(x, y);
}
void VirtualInput::_mouse_scroll_up(uint16_t x, uint16_t y, uint16_t count) {
	if (!this_process->get_is_logged())
		return;
	_mouse_move(x, y);
	count = abs(count);
	_mSendMessage(WM_MOUSEWHEEL, MAKEWPARAM(0, 120 * count), MAKELPARAM(x, y));
}
void VirtualInput::_mouse_scroll_down(uint16_t x, uint16_t y, uint16_t count) {

	if (!this_process->get_is_logged())
		return;
	_mouse_move(x, y);
	count = abs(count);
	_mSendMessage(WM_MOUSEWHEEL, MAKEWPARAM(0, -120 * count), MAKELPARAM(x, y));
}
void VirtualInput::_send_ctrl_down(){
	if (!this_process->get_is_logged())
		return;
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	this_process->write_int(address_base_of_message_proc_ctrl, 1024);
}
void VirtualInput::_send_ctrl_up(){
	if (!this_process->get_is_logged())
		return;
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	this_process->write_int(address_base_of_message_proc_ctrl, 0);
}
void VirtualInput::_press_ctrl_and_shift(){
	if (!this_process->get_is_logged())
		return;
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	this_process->write_int(address_base_of_message_proc_ctrl, 1536);
}
void VirtualInput::_send_shift_down(){
	if (!this_process->get_is_logged())
		return;
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	this_process->write_int(address_base_of_message_proc_ctrl, 512);
}
void VirtualInput::_send_shift_up(){
	if (!this_process->get_is_logged())
		return;
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	this_process->write_int(address_base_of_message_proc_ctrl, 0);
}
bool VirtualInput::_is_control_pressed(){
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	int res = this_process->read_int(address_base_of_message_proc_ctrl, false);
	return (res == 1024 || res == 1536 || res == 3584 || res == 3072);
}
bool VirtualInput::_is_shift_pressed(){
	int address_base_of_message_proc_ctrl = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_CTRL_SHIFT)) + 0x240;
	int res = this_process->read_int(address_base_of_message_proc_ctrl, false);
	return (res == 512 || res == 1536 || res == 3584 || res == 2560);
}
void VirtualInput::_write_mouse_pos(uint16_t x, uint16_t y){
	int baseaddress = TibiaProcess::get_default()->base_address;
	this_process->write_int(baseaddress + AddressManager::get()->getAddress(ADDRESS_MOUSE1_X), x);
	this_process->write_int(baseaddress + AddressManager::get()->getAddress(ADDRESS_MOUSE2_X), x);
	this_process->write_int(baseaddress + AddressManager::get()->getAddress(ADDRESS_MOUSE1_Y), y);
	this_process->write_int(baseaddress + AddressManager::get()->getAddress(ADDRESS_MOUSE2_Y), y);
	this_process->write_int(baseaddress + AddressManager::get()->getAddress(ADDRESS_MOUSE_FIX_X), x);
	this_process->write_int(baseaddress + AddressManager::get()->getAddress(ADDRESS_MOUSE_FIX_Y), y);
	_mPostMessage(WM_MOUSEMOVE, 0, MAKELPARAM(x, y));
}
void VirtualInput::_send_char(uint8_t value, LPARAM lparam) {
	_mSendMessage(WM_CHAR, value, lparam);
}
void VirtualInput::_send_string(std::string value) {
	for (uint32_t i = 0; i < value.length(); i++)
		_mSendMessage(WM_CHAR, value[i], NULL);
}
void VirtualInput::_send_virtual_key(uint32_t vk) {
	_mSendMessage(WM_KEYDOWN, vk, 0);
	_mSendMessage(WM_KEYUP, vk, 0);
}
void VirtualInput::_send_multiple_keys(std::vector<uint32_t> vks) {
	for (auto it = vks.begin(); it != vks.end(); it++){
		_mSendMessage(WM_KEYDOWN, *it, 0);
	}
	for (auto it = vks.rbegin(); it != vks.rend(); it++){
		_mSendMessage(WM_KEYDOWN, *it, 0);
	}
}
void VirtualInput::_arrow_up_rigth() {
	_mSendMessage(WM_KEYDOWN, 0x21, NULL); 
	_mSendMessage(WM_KEYUP, 0x21, NULL); 
}
void VirtualInput::_arrow_down_right() {
	_mSendMessage(WM_KEYDOWN, 0x22, NULL);
	_mSendMessage(WM_KEYUP, 0x22, NULL);
}
void VirtualInput::_arrow_up_left() {
	_mSendMessage(WM_KEYDOWN, 0x24, NULL);
	_mSendMessage(WM_KEYUP, 0x23, NULL);
}
void VirtualInput::_arrow_down_left() {
	_mSendMessage(WM_KEYDOWN, 0x23, NULL);
	_mSendMessage(WM_KEYUP, 0x24, NULL);
}
void VirtualInput::_arrow_left() {
	_mSendMessage(WM_KEYDOWN, 0x25, NULL);
	_mSendMessage(WM_KEYUP, 0x25, NULL);
}
void VirtualInput::_arrow_up() {
	_mSendMessage(WM_KEYDOWN, 0x26, NULL);
	_mSendMessage(WM_KEYUP, 0x26, NULL);
}
void VirtualInput::_arrow_right() {
	_mSendMessage(WM_KEYDOWN, 0x27, NULL);
	_mSendMessage(WM_KEYUP, 0x27, NULL);
}
void VirtualInput::_arrow_down() {
	_mSendMessage(WM_KEYDOWN, 0x28, NULL);
	_mSendMessage(WM_KEYUP, 0x28, NULL);
}
void VirtualInput::_send_esc(){
	_mPostMessage(WM_KEYDOWN, VK_ESCAPE, 0);
}
void VirtualInput::_send_enter(){
	_mPostMessage(WM_KEYDOWN, VK_RETURN, 0);
}
void VirtualInput::_sent_tab(){
	_mPostMessage(WM_KEYDOWN, VK_TAB, 0);
}
LRESULT VirtualInput::_mSendMessage(UINT Msg, WPARAM wParam, LPARAM lParam){
	return SendMessage((HWND)window_handler, Msg, wParam, lParam);
}
LRESULT VirtualInput::_mPostMessage(UINT Msg, WPARAM wParam, LPARAM lParam){
	return PostMessage((HWND)window_handler, Msg, wParam, lParam);
}

HighLevelVirtualInput* HighLevelVirtualInput::get_default(){
	static HighLevelVirtualInput* mHighLevelVirtualInput = nullptr;
	if (!mHighLevelVirtualInput)
		mHighLevelVirtualInput = new HighLevelVirtualInput;

	return mHighLevelVirtualInput;
}
bool HighLevelVirtualInput::can_use_input() {
	if (!using_input){
		return true;
	}
	return false;
}
void HighLevelVirtualInput::enable_using_input() {
	mxt_access.lock();
	using_input = true;
}
void HighLevelVirtualInput::disable_using_input() {
	using_input = false;
	mxt_access.unlock();
}
int HighLevelVirtualInput::get_time_out() {
	return time_out;
}
void HighLevelVirtualInput::use_hotkey(hotkey_t type, bool ctrl, bool shift)  {
	enable_using_input();
	release_basic_buttons();

	if (ctrl) {
		send_ctrl_down();
	}
	else if (shift) {
		send_shift_down();
	}

	mSendMessage(WM_KEYDOWN, type, 0);

	if (ctrl) {
		send_ctrl_up();
	}
	else if (shift) {
		send_shift_up();
	}

	disable_using_input();
}
void HighLevelVirtualInput::scroll_count(uint32_t countNow, uint32_t count) {
	enable_using_input();
	
	send_char('0');
	std::string char_num = std::to_string(count);
	for (auto letter : char_num)
		send_char(letter);

	disable_using_input();
}
bool HighLevelVirtualInput::look_pixel(uint16_t x, uint16_t y){
	enable_using_input();
	if (!this_process->get_is_logged()) {
		disable_using_input();
		return false;
	}

	/*if (!can_click(x, y)){
		disable_using_input();
		return false;
	}*/
	_fix_click_coordinate(x, y);
	release_mouse_buttons(true, true);

	mouse_move(x, y);
	click_left(x, y, false, true);
	disable_using_input();
	return true;
}
bool HighLevelVirtualInput::look_pixel(Point& point){
	uint16_t x = point.x;
	uint16_t y = point.y;
	return look_pixel(x, y);
}
bool HighLevelVirtualInput::drag_mouse(Point& point_from, Point& point_to, bool ctrl){
	LOG_INPUT
	enable_using_input();
	if (!this_process->get_is_logged()) {
		disable_using_input();
		return false;
	}

	release_mouse_buttons(true, true);

	/*if (!can_click(point_from.x, point_from.y) || !can_click(point_to.x, point_to.y)){
		disable_using_input();
		return false;
	}*/

	uint16_t from_x = point_from.x;
	uint16_t from_y = point_from.y;
	uint16_t to_x = point_to.x;
	uint16_t to_y = point_to.y;

	_fix_click_coordinate(from_x, from_y);
	_fix_click_coordinate(to_x, to_y);

	if (ctrl)
		send_ctrl_down();

	mouse_left_down(from_x, from_y);

	mouse_move(point_to.x, point_to.y);

	mouse_left_up(to_x, to_y);

	if (ctrl)
		send_ctrl_up();

	disable_using_input();
	return true;
}
bool HighLevelVirtualInput::drag_item_on_screen(Point& point_from/*coordinate on screen in pixels*/, 
	Point& point_to, bool ctrl, uint32_t item_id , uint16_t count){
	LOG_INPUT
	enable_using_input();
	if (!this_process->get_is_logged()){
		disable_using_input();
		return false;
	}

	release_mouse_buttons(true, true);
	/*if (!can_click(point_from.x, point_from.y) || !can_click(point_to.x, point_to.y)){
		disable_using_input();
		return false;
	}*/

	uint16_t from_x = point_from.x;
	uint16_t from_y = point_from.y;
	uint16_t to_x = point_to.x;
	uint16_t to_y = point_to.y;

	_fix_click_coordinate(from_x, from_y);
	_fix_click_coordinate(to_x, to_y);

	//drag all
	if (count <= 0 || count >= 100){	
		send_ctrl_down();
		mouse_left_down(from_x, from_y);
		Sleep(30);
		if (item_id != UINT32_MAX && item_id != 0){
			if (item_id != this_process->get_id_to_move()){
				mouse_left_up(from_x, from_y);
				send_ctrl_up();
				{
					if (Interface->get_dialog()){
						send_esc();
					}
					disable_using_input();
					return false;
				}
			}
		}

		mouse_move(to_x, to_y);
		Sleep(30);
		mouse_left_up(to_x, to_y);
		send_ctrl_up();
		disable_using_input();
		return true;
	}
	//with count

	mouse_left_down(from_x, from_y);
	Sleep(30);
	if (item_id != UINT32_MAX && item_id != 0){
		if (item_id != this_process->get_id_to_move()){
			mouse_left_up(from_x, from_y);
			send_ctrl_up();
			{
				if (Interface->get_dialog()){
					send_esc();
				}
				disable_using_input();
				return false;
			}
		}
	}

	
	mouse_move(to_x, to_y);
	Sleep(30);
	mouse_left_up(to_x, to_y);
	Sleep(30);
	auto dialog = Interface->get_dialog();
	if (!dialog){
		//send_esc();
		disable_using_input();
		return false;
	}		
	if (!dialog->set_scroll_count(count)){
		if (Interface->get_dialog())
			send_esc();
		disable_using_input();
		return false;
	}

	Sleep(30);
	if (Interface->get_dialog())
		send_enter();

	disable_using_input();
	
	return true;
}
bool HighLevelVirtualInput::look_at_game_coordinate(Point& point){
	Point p = Interface->get_coordinate_as_pixel(point);
	return look_pixel(p);
}
void HighLevelVirtualInput::send_escape_button(){
	enable_using_input();
	send_virtual_key(VK_ESCAPE);
	disable_using_input();
}
void HighLevelVirtualInput::get_mouse_location(int &x, int &y){
	enable_using_input();
	x = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_MOUSE1_X));
	y = this_process->read_int(AddressManager::get()->getAddress(ADDRESS_MOUSE1_Y));
	disable_using_input();
}
void HighLevelVirtualInput::click_tibia_coordinate_right(Coordinate coord, Coordinate deslocation_from_axis
	, bool ctrl, bool shift){
	enable_using_input();
	int dist = coord.get_axis_max_dist(TibiaProcess::get_default()->character_info->get_self_coordinate());
	if (dist <= 7){
		Point point = ClientGeneralInterface::get()->get_coordinate_as_pixel(Point(coord.x, coord.y));
		if (point.is_null()) {
			disable_using_input();
			return;
		}

		if (!deslocation_from_axis.is_null()){
			point.x += deslocation_from_axis.x;
			point.y += deslocation_from_axis.y;
		}

		click_right(point, ctrl, shift);
	}

	disable_using_input();
}
void HighLevelVirtualInput::click_tibia_coordinate_left(Coordinate coord, Coordinate deslocation_from_axis
	, bool ctrl, bool shift){
	enable_using_input();
	int dist = coord.get_axis_max_dist(TibiaProcess::get_default()->character_info->get_self_coordinate());
	if (dist <= 7){
		Point point = ClientGeneralInterface::get()->get_coordinate_as_pixel(Point(coord.x, coord.y));
		if (point.is_null()) {
			disable_using_input();
			return;
		}
		if (!deslocation_from_axis.is_null()){
			point.x += deslocation_from_axis.x;
			point.y += deslocation_from_axis.y;
		}

		click_left(point, ctrl, shift);
	}
	disable_using_input();
}
void HighLevelVirtualInput::click_tile_left(Coordinate coord, Coordinate deslocation_from_axis, bool ctrl, bool shift){
	enable_using_input();
	int dist = coord.get_axis_max_dist(TibiaProcess::get_default()->character_info->get_self_coordinate());
	if (dist <= 7){
		Point point = ClientGeneralInterface::get()->get_coordinate_as_pixel(Point(coord.x, coord.y));
		if (point.is_null()) {
			disable_using_input();
			return;
		}
		if (!deslocation_from_axis.is_null()){
			point.x += deslocation_from_axis.x;
			point.y += deslocation_from_axis.y;
		}
		click_left(point, ctrl, shift);
	}
	disable_using_input();
}

//VirtualInput
void HighLevelVirtualInput::set_pid(uint32_t pid) {
	_set_pid(pid);
}
uint32_t HighLevelVirtualInput::get_pid() {
	return _get_pid();
}
void HighLevelVirtualInput::click_right(uint16_t x, uint16_t y, bool ctrl, bool shift) {
	LOG_INPUT
	enable_using_input();
	_click_right(x, y, ctrl, shift);
	disable_using_input();
}
void HighLevelVirtualInput::click_right(Point& point, bool ctrl, bool shift){
	LOG_INPUT
	enable_using_input();
	_click_right(point, ctrl, shift);
	disable_using_input();
}
void HighLevelVirtualInput::click_left(uint16_t x, uint16_t y, bool ctrl, bool shift){
	LOG_INPUT

	enable_using_input();
	_click_left(x, y, ctrl, shift);
	disable_using_input();
}
void HighLevelVirtualInput::click_left(Point& point, bool ctrl, bool shift){
	LOG_INPUT
	enable_using_input();
	_click_left(point, ctrl, shift);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_right_down(uint16_t x, uint16_t y){
	LOG_INPUT
	enable_using_input();
	_mouse_right_down(x, y);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_right_down(Point& point){
	LOG_INPUT
	enable_using_input();
	_mouse_right_down(point);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_right_up(uint16_t x, uint16_t y){
	LOG_INPUT
	enable_using_input();
	_mouse_right_up(x, y);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_right_up(Point& point){
	LOG_INPUT
	enable_using_input();
	_mouse_right_up(point);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_left_down(uint16_t x, uint16_t y){
	LOG_INPUT
	enable_using_input();
	_mouse_left_down(x, y);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_left_down(Point& point){
	LOG_INPUT
	enable_using_input();
	_mouse_left_down(point);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_left_up(uint16_t x, uint16_t y){
	LOG_INPUT
	enable_using_input();
	_mouse_left_up(x, y);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_left_up(Point& point){
	LOG_INPUT
	enable_using_input();
	_mouse_left_up(point);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_move(uint16_t x, uint16_t y){

	enable_using_input();
	_mouse_move(x, y);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_move(Point& point){

	enable_using_input();
	_mouse_move(point);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_scroll_up(uint16_t x, uint16_t y, uint16_t count){
	enable_using_input();
	_mouse_scroll_up(x, y, count);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_scroll_up(Point& point, uint16_t count){
	enable_using_input();
	_mouse_scroll_up(point, count);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_scroll_down(uint16_t x, uint16_t y, uint16_t count){
	enable_using_input();
	_mouse_scroll_down(x, y, count);
	disable_using_input();
}
void HighLevelVirtualInput::mouse_scroll_down(Point& point, uint16_t count){
	enable_using_input();
	_mouse_scroll_down(point, count);
	disable_using_input();
}
void HighLevelVirtualInput::send_char(uint8_t value, LPARAM lparam){
	enable_using_input();
	_send_char(value, lparam);
	disable_using_input();
}
void HighLevelVirtualInput::send_char(std::vector<uint8_t> valueList, LPARAM lparam){
	enable_using_input();
	for (uint8_t value : valueList)
		_send_char(value, lparam);
	disable_using_input();
}
void HighLevelVirtualInput::send_string(std::string value){
	enable_using_input();
	_send_string(value);
	disable_using_input();
}
void HighLevelVirtualInput::send_virtual_key(uint32_t vk){
	enable_using_input();
	_send_virtual_key(vk);
	disable_using_input();
}
void HighLevelVirtualInput::send_multiple_keys(std::vector<uint32_t> vks){
	enable_using_input();
	_send_multiple_keys(vks);
	disable_using_input();
}
void HighLevelVirtualInput::arrow_up(){
	enable_using_input();
	_arrow_up();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_right(){
	enable_using_input();
	_arrow_right();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_down(){
	enable_using_input();
	_arrow_down();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_left(){
	enable_using_input();
	_arrow_left();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_up_left(){
	enable_using_input();
	_arrow_up_left();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_up_rigth(){
	enable_using_input();
	_arrow_up_rigth();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_down_left(){
	enable_using_input();
	_arrow_down_left();
	disable_using_input();
}
void HighLevelVirtualInput::arrow_down_right(){
	enable_using_input();
	_arrow_down_right();
	disable_using_input();
}
void HighLevelVirtualInput::send_esc(){
	LOG_INPUT
	enable_using_input();
	_send_esc();
	disable_using_input();
}
void HighLevelVirtualInput::send_enter(){
	LOG_INPUT
	enable_using_input();
	_send_enter();
	disable_using_input();
}
void HighLevelVirtualInput::sent_tab(){
	LOG_INPUT
	enable_using_input();
	_sent_tab();
	disable_using_input();
}
void HighLevelVirtualInput::send_ctrl_down(){
	LOG_INPUT
	enable_using_input();
	_send_ctrl_down();
	disable_using_input();
}
void HighLevelVirtualInput::send_ctrl_up(){
	LOG_INPUT
	enable_using_input();
	_send_ctrl_up();
	disable_using_input();
}
void HighLevelVirtualInput::press_ctrl_and_shift(){
	LOG_INPUT
	enable_using_input();
	_press_ctrl_and_shift();
	disable_using_input();
}
void HighLevelVirtualInput::send_shift_down(){
	LOG_INPUT
	enable_using_input();
	_send_shift_down();
	disable_using_input();
}
void HighLevelVirtualInput::send_shift_up(){
	LOG_INPUT
	enable_using_input();
	_send_shift_up();
	disable_using_input();
}
bool HighLevelVirtualInput::is_control_pressed(){
	LOG_INPUT
	enable_using_input();
	bool is_pressed = _is_control_pressed();
	disable_using_input();
	return is_pressed;
}
bool HighLevelVirtualInput::is_shift_pressed(){
	LOG_INPUT
	enable_using_input();
	bool is_pressed = _is_shift_pressed();
	disable_using_input();
	return is_pressed;
}
void HighLevelVirtualInput::write_mouse_pos(uint16_t x, uint16_t y){
	enable_using_input();
	_write_mouse_pos(x, y);
	disable_using_input();
}
void HighLevelVirtualInput::release_mouse_buttons(bool left, bool right, uint16_t x, uint16_t y){
	enable_using_input();
	_release_mouse_buttons(left, right, x, y);
	disable_using_input();
}
bool HighLevelVirtualInput::is_mouse_pressed(){
	LOG_INPUT
	enable_using_input();
	bool is_pressed = _is_mouse_pressed();
	disable_using_input();
	return is_pressed;
}
void HighLevelVirtualInput::release_basic_buttons(){
	enable_using_input();
	_release_basic_buttons();
	disable_using_input();
}
LRESULT HighLevelVirtualInput::mSendMessage(UINT Msg, WPARAM wParam, LPARAM lParam){
	enable_using_input();
	LRESULT result = _mSendMessage(Msg, wParam, lParam);
	disable_using_input();
	return result;
}
LRESULT HighLevelVirtualInput::mPostMessage(UINT Msg, WPARAM wParam, LPARAM lParam){
	enable_using_input();
	LRESULT result = _mPostMessage(Msg, wParam, lParam);
	disable_using_input();
	return result;
}
bool HighLevelVirtualInput::can_click(uint16_t x, uint16_t y){
	enable_using_input();
	bool can_click = _can_click(x, y);
	disable_using_input();
	return can_click;
}



