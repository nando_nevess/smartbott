#include "Util.h"
#include "Process.h"

class ProcessHider{
public:
	void RunApplication(std::string exeFullPath){
		STARTUPINFO startupInfo = { 0 };
		startupInfo.cb = sizeof(startupInfo);
		PROCESS_INFORMATION processInformation;

		memset(&processInformation, 0, sizeof(processInformation));
		memset(&startupInfo, 0, sizeof(startupInfo));
		char buf[MAX_PATH] = { 0 };


		BOOL result = ::CreateProcess(
			&exeFullPath[0],
			"",
			NULL,
			NULL,
			FALSE,
			NORMAL_PRIORITY_CLASS,
			NULL,
			".\\",
			&startupInfo,
			&processInformation
			);

	}


	static ProcessHider* get(){
		static ProcessHider* mProcessHider = nullptr;
		if (!mProcessHider)
			mProcessHider = new ProcessHider;
		return mProcessHider;
	}
};