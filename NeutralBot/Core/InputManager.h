#pragma once
#include "InputEvent.h"
#include <functional>
#include <memory>
#include "..\\CoreBase.h"

#pragma pack(push,1)

struct InputEventParams{
	InputEventParams(){}
	InputEventParams(bool _mouse, int _code, WPARAM _wp, LPARAM _lp):
		mouse(_mouse), code(_code), wp(_wp), lp(_lp) {
		if (mouse)
			mouse_struct = *(MSLLHOOKSTRUCT*)lp;
		else
			keyboard_struct = *(KBDLLHOOKSTRUCT*)lp;
	}
	bool mouse; int code; WPARAM wp; LPARAM lp;

	MSLLHOOKSTRUCT mouse_struct;
	KBDLLHOOKSTRUCT keyboard_struct;
};

struct input_match_params{
	input_match_params(){
		memset(this, 1, sizeof(input_match_params));
	}
	bool require_ctrl, require_alt, require_shift, require_insert_state, require_numlock, require_scrolllock, require_capslock,
		filter_mouse_moves, filter_mouse_inputs, filter_mouse_scroll, filter_keyboard_inputs;
	bool match(InputBaseEvent* input_event){
		if (input_event->is_mouse_event()){
			MouseInput* mouse_input = (MouseInput*)input_event;
			if (filter_mouse_moves && mouse_input->event_type == mouse_event_t::mouse_event_move)
				return true;
			else if (filter_mouse_inputs &&
				(mouse_input->event_type == mouse_event_t::mouse_event_button_down ||
				mouse_input->event_type == mouse_event_t::mouse_event_button_up ||
				mouse_input->event_type == mouse_event_t::mouse_event_double_click)
				)
				return true;
			else if (filter_mouse_scroll && mouse_input->event_type == mouse_event_t::mouse_event_wheel)
				return true;
			return false;
		}
		else if (input_event->is_keyboard_event()){
			if (!filter_keyboard_inputs)
				return false;

			KeyboardInput* keyboard_input = (KeyboardInput*)input_event;
			if (keyboard_input->event_type == keyboard_event_t::keyboard_key_up)
				return true;
		}
		return false;
	}
};

class InputManager : public CoreBase{
	bool has_action = false;
	void handle_event(std::shared_ptr<InputBaseEvent> input);

	std::vector<std::shared_ptr<InputBaseEvent>> inputs;


	bool has_event = false;
	
	
	std::vector<InputEventParams> events;

	neutral_mutex events_mutex;


	static LRESULT handlekeys(int code, WPARAM wp, LPARAM lp);

	static LRESULT handlemouse(int code, WPARAM wp, LPARAM lp);

	void add_mouse_event_code(int code, WPARAM wp, LPARAM lp);

	void add_keyboard_event_code(int code, WPARAM wp, LPARAM lp);

	InputEventParams get_event();

	void processor_thread();

	void process_mouse(InputEventParams& _event);

	void process_keyboard(InputEventParams& _event);

	
	neutral_mutex mtx_access;	
	neutral_mutex mtx_invoke;	
	void check_thread();
	void process_input_thread();

	std::map<uint32_t/*id*/, std::function<void(InputBaseEvent* input_event)> > callbacks;
public:

	std::shared_ptr<InputHolder> wait_input(uint32_t timeout, input_match_params& match = input_match_params());

	InputManager();

	int add_callback(uint32_t id, std::function<void(InputBaseEvent* input_event)> callback);

	void remove_callback(uint32_t id);

	static InputManager* get(){
		static InputManager* mInputManager = nullptr;
		if (!mInputManager)
			mInputManager = new InputManager();
		return mInputManager;
	}
};

#pragma pack(pop)
//TODO Liberar
