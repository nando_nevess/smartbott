#pragma once
#include <stdint.h>

#define pixel_offset_bp_places_x  105
#define pixel_offset_bp_begin_x  174
#define size_of_each_slot_x
#define size_of_each_slot_y
#define impossible  -1
#define did  -2
#define trying  -3
#define not_founded  -4
#define pixel_offset_hided  0x30
#define size_of_slot_y  37
#define size_of_slot_x  39
#define size_of_scroll_bar_bp  14
#define size_of_title_container_y  14
#define size_of_eachscroll  10
#define pixel_maximun_to_hide  30
#define pixel_offset_upbp  30
#define pixel_offset_closebp  -8
#define have_yellow_skull  0
#define have_black_skull  0
#define yellow_skull  2
#define pk_skull  3
#define red_skull  4
#define black_skull  5
#define SQM_COUNT_X	15
#define SQM_COUNT_Y 11

enum map_type_t{
	map_back_t = 0,
	map_front_t,
	map_front_special_area,
	map_front_advanced_t,
	map_front_clear_t,
	map_front_clear_minimap_t,
	map_back_mini_t,
	map_front_mini_t,
	map_front_advanced_mini_t,
	map_front_cached_t,
	map_front_cached_editable_t,
	map_front_cached_advanced_t,
	map_total_t,
	map_none_t = map_total_t
};

enum use_spell_type{
	use_spell_fast = 0,
	use_spell_by_mounster_count,
	use_spell_by_safety,
};

enum pathfinder_retval{
	pathfinder_not_reachable,
	pathfinder_reached,
	pathfinder_going,
	pathfinder_found,
};

enum pathfinder_line_t{
	pathfinder_line_1,
	pathfinder_line_2,
	pathfinder_line_3
};

enum pathfinder_state{
	pathfinder_walk_none,
	pathfinder_walk_request_start,
	pathfinder_walk_going,
	pathfinder_walk_not_reachable,
	pathfinder_walk_overriden,
	pathfinder_walk_canceled,
	pathfinder_walk_reached,
	pathfinder_walk_condition_has_to_stop,
	pathfinder_walk_killed,
	pathfinder_state_none
};

extern pathfinder_state pathfinder_state_placeholder;

enum status : unsigned char{
	poison = 1,
	fire = 2,
	energy = 4,
	drunk = 8,
	utamo = 16,
	slow = 32,
	haste = 64,
	battle = 128,
	water = 1,
	frozen = 2,
	holly = 4,
	curse = 8,
	streng_up = 16,
	battle_red = 32,
	pz = 64,
	blood = 128,
};

namespace side_t{
	enum side_t{
		side_north,
		side_north_east,
		side_east,
		side_south_east,
		side_south,
		side_south_west,
		side_west,
		side_north_west, 
		side_center
	};
};

namespace side_nom_axis_t{
	enum side_nom_axis_t{
		side_north,
		side_east,
		side_south,
		side_west
	};
}
enum spell_actions_t{
	spell_actions_none,
	spell_actions_cast,
	spell_actions_no_cast,
	spell_actions_do_waypoint_wait,
	spell_actions_do_looter_wait,
	spell_actions_do_hunter_wait,
	spell_actions_total
};
enum spells_conditions_t{
	spell_condition_none,
	spell_condition_not_looting,
	spell_condition_not_hunting,
	spell_condition_force_use_life_bellow_value,
	spell_condition_force_use_mana_bellow_value,
	spell_condition_force_use_life_percent_bellow_value,
	spell_condition_force_use_mana_percent_bellow_value,
	spell_condition_if_target_name,
	spell_condition_if_player_on_screen,
	spell_condition_trapped,
	spell_condition_total
};


enum lua_t{
	lua_waypointer,
	lua_hud,
	lua_background
};

enum hotkey_t{
	hotkey_f1 = 0x70,
	hotkey_f2,
	hotkey_f3,
	hotkey_f4,
	hotkey_f5,
	hotkey_f6,
	hotkey_f7,
	hotkey_f8,
	hotkey_f9,
	hotkey_f10,
	hotkey_f11,
	hotkey_f12,
	hotkey_f1_shift,
	hotkey_f2_shift,
	hotkey_f3_shift,
	hotkey_f4_shift,
	hotkey_f5_shift,
	hotkey_f6_shift,
	hotkey_f7_shift,
	hotkey_f8_shift,
	hotkey_f9_shift,
	hotkey_f10_shift,
	hotkey_f11_shift,
	hotkey_f12_shift,
	hotkey_f1_ctrl,
	hotkey_f2_ctrl,
	hotkey_f3_ctrl,
	hotkey_f4_ctrl,
	hotkey_f5_ctrl,
	hotkey_f6_ctrl,
	hotkey_f7_ctrl,
	hotkey_f8_ctrl,
	hotkey_f9_ctrl,
	hotkey_f10_ctrl,
	hotkey_f11_ctrl,
	hotkey_f12_ctrl,
	hotkey_t_count = 36,
	hotkey_any,
	hotkey_none
};

enum hotkey_cast_type_t{
	hotkey_cast_type_with_crosshairs,
	hotkey_cast_type_on_target,
	hotkey_cast_type_on_self,
	hotkey_cast_type_toggle_equip,
	hotkey_cast_any
};

enum XMLspell_type
{
	Missile,
	Area,
	Instant,
	Strike
};

enum XMLCategory
{
	Attack,
	Healing,
	Support,
	Special
};

enum FrameGroup : uint8_t {
	FrameGroupIdle = 0,
	FrameGroupMoving,
	FrameGroupDefault = FrameGroupIdle
};

enum ThingCategory : uint8_t {
	ThingCategoryItem = 0,
	ThingCategoryCreature,
	ThingCategoryEffect,
	ThingCategoryMissile,
	ThingInvalidCategory,
	ThingLastCategory = ThingInvalidCategory
};

enum ThingAttr : uint8_t {
	ThingAttrGround = 0,
	ThingAttrGroundBorder = 1,
	ThingAttrOnBottom = 2,
	ThingAttrOnTop = 3,
	ThingAttrContainer = 4,
	ThingAttrStackable = 5,
	ThingAttrForceUse = 6,
	ThingAttrMultiUse = 7,
	ThingAttrWritable = 8,
	ThingAttrWritableOnce = 9,
	ThingAttrFluidContainer = 10,
	ThingAttrSplash = 11,
	ThingAttrNotWalkable = 12,
	ThingAttrNotMoveable = 13,
	ThingAttrBlockProjectile = 14,
	ThingAttrNotPathable = 15,
	ThingAttrPickupable = 16,
	ThingAttrHangable = 17,
	ThingAttrHookSouth = 18,
	ThingAttrHookEast = 19,
	ThingAttrRotateable = 20,
	ThingAttrLight = 21,
	ThingAttrDontHide = 22,
	ThingAttrTranslucent = 23,
	ThingAttrDisplacement = 24,
	ThingAttrElevation = 25,
	ThingAttrLyingCorpse = 26,
	ThingAttrAnimateAlways = 27,
	ThingAttrMinimapColor = 28,
	ThingAttrLensHelp = 29,
	ThingAttrFullGround = 30,
	ThingAttrLook = 31,
	ThingAttrCloth = 32,
	ThingAttrMarket = 33,
	ThingAttrUsable = 34,

	// additional
	ThingAttrOpacity = 100,
	ThingAttrNotPreWalkable = 101,

	ThingAttrFloorChange = 252,
	ThingAttrNoMoveAnimation = 253, // 10.10: real value is 16, but we need to do this for backwards compatibility
	ThingAttrChargeable = 254, // deprecated
	ThingLastAttr = 255
};

enum SpriteMask {
	SpriteMaskRed = 1,
	SpriteMaskGreen,
	SpriteMaskBlue,
	SpriteMaskYellow
};

enum pvp_old_t{
	pvp_enabled = 0,
	pvp_disabled = 1
};

enum pvp_new_t{
	pvp_new_1 = 0,
	pvp_new_2 = 1,
	pvp_new_3 = 2,
	pvp_new_4 = 3
};

enum attack_mode_t{
	attack_mode_offensive = 1,
	attack_mode_balanced = 2,
	attack_mode_defensive = 3,
	attack_mode_no_change = 4,
	attack_mode_total = 5
};

enum loot_sequence_t{
	loot_sequence_inteligent,
	loot_sequence_instant,
	loot_sequence_after_kill_all,
	loot_sequence_wait_pulse,
	loot_sequence_t_total,
	loot_sequence_t_none = loot_sequence_t_total
};

enum lure_mode_t{
	lure_mode_t_none,
	lure_mode_t_lure,
	lure_mode_t_total
};

enum move_mode_t{
	move_mode_t_follow,
	move_mode_t_hotkey,
	move_mode_t_total
};

enum take_skin_condition_t{
	take_skin_condition_t_none,
	take_skin_condition_t_after_6_sec,
	take_skin_condition_t_total
};

enum loot_filter_t{
	loot_filter_t_from_target,
	loot_filter_t_from_all,
	loot_filter_t_from_all_on_hunter,
	loot_filter_t_total,
	loot_filter_t_none = loot_filter_t_total
};

enum loot_type_t{
	loot_type_t_default,
	loot_type_t_fast,
	loot_type_t_total,
	loot_type_t_none = loot_type_t_default
};

enum loot_item_priority_t{
	loot_item_priority_min,
	loot_item_priority_medium,
	loot_item_priority_max,
	loot_item_priority_total,
	loot_item_priority_none = loot_filter_t_total
};

enum loot_item_condition_t{
	loot_item_condition_none,
	drop_if_destination_full,
	drop_if_cap_bellow,
	drop_if_have_more_than,
	dont_loot_if_have_more_than,
	dont_loot_if_cap_bellow_than,
	drop,
	drop_from_own_backpack_if_have_more_than,
	drop_from_own_backpack_if_cap_bellow_than,
	loot_from_the_floor_while_cap_more_than,
	loot_item_condition_total
};

enum loot_item_action_t :uint32_t{
	loot_item_action_none,
	loot_item_action_move,
	loot_item_action_drop,
	loot_item_action_pick_up,
	loot_item_action_total
};

enum loot_item_new_condition_t:uint32_t{
	if_destination_full_cancel,
	if_cap_bellow_than_cancel,
	if_have_more_than_cancel,
	loot_item_new_condition_total
};

enum exani_hur_t{
	exani_hur_up = 0,
	exani_hur_down = 1
};

enum alert_t{
	alert_none,
	alert_player_on_screen,
	alert_player_attack,
	alert_disconnect,
	alert_player_kill,
	alert_cap,
	alert_low_health,
	alert_total
};

enum action_retval_t{
	action_retval_fail,
	action_retval_fail_send_input,
	action_retval_trying,
	action_retval_timeout,
	action_retval_not_near,
	action_retval_missing_parameter,
	action_retval_missing_depot_chest,
	action_retval_container_not_found,
	action_retval_has_more_work,
	action_retval_has_not_more_work,
	action_retval_success,
	action_retval_warning_message_not_owner,
	action_retval_warning_message_first_go_upstairs,
	action_retval_warning_message_first_go_downstairs,
	action_retval_warning_cannot_use,
	action_retval_move,
	action_retval_drop,
	action_retval_success_open_browser,
	action_retval_total,
	action_retval_none
};

enum spell_category_t{
	spell_category_attack,
	spell_category_recover,
	spell_category_support,
	spell_category_suply,
	spell_category_spell_end,
	spell_category_item,
	spell_category_total
};

enum spell_type_t{
	spell_type_healing,
	spell_type_on_target,
	spell_type_area,
	spell_type_rune_on_target,
	spell_type_rune_area,
};

enum spell_condition_t{
	OnlyUse,
	OnlyWithPlayerOnScreen,
	OnlyWithoutPlayerOnScreen,
};

enum pathfinder_walkability_t: unsigned char{
	pathinder_walkability_not_walkable,
	pathinder_walkability_walkable,
	pathinder_walkability_player,
	pathinder_walkability_monster,
	pathinder_walkability_summon,
	pathinder_walkability_npc,
	pathinder_walkability_furniture,
	pathinder_walkability_rune_field,
	pathinder_walkability_timed_not_walkable,
	pathinder_walkability_near_depot_walkable,
	pathinder_walkability_near_depot_unwalkable,
	pathinder_walkability_undefined = 255
};


enum attr_item_t{
	attr_item_none,
	attr_item_override_walkable,
	attr_item_override_unwalkable,
	attr_item_can_broke,
	attr_item_can_move,
	attr_item_step_up,
	attr_item_step_down,
	attr_item_avoid_when_targeting,
	attr_item_use_to_up,
	attr_item_use_to_down,
	attr_item_force_loot_browse,
	attr_item_use_to_teleport,
	attr_item_total
};

enum attr_additional_t{
	attr_additional_none,
	attr_additional_key,
	attr_additional_required_coord,
	attr_additional_destination_coord
};


enum creature_type{
	creature_type_player,
	creature_type_monster,
	creature_type_npc,
	creature_type_our_summon,
	creature_type_other_summon,
};

enum skull_type{
	skull_none,
	skull_yellow,
	skull_green,
	skull_white,
	skull_red,
	skull_black,
	skull_orange
};

enum vip_type_t{
	vip_type_none,
	vip_type_heart,
	vip_type_skull,
	vip_type_lightning,
	vip_type_target,
	vip_type_start,
	vip_type_element,
	vip_type_green_symbol,
	vip_type_x,
	vip_type_money,
	vip_type_cross
};

enum stuck_ctrl_shift_policy_t{
	stuck_ctrl_shift_policy_none,
	stuck_ctrl_shift_policy_do_nothing,
	stuck_ctrl_shift_policy_release_instantly,
	stuck_ctrl_shift_policy_release_after_1_sec,
	stuck_ctrl_shift_policy_release_after_2_secs,
	stuck_ctrl_shift_policy_release_after_3_secs,
	stuck_ctrl_shift_policy_if_cavebot_on_release_instantly,
	stuck_ctrl_shift_policy_if_cavebot_on_release_after_1_sec,
	stuck_ctrl_shift_policy_if_cavebot_on_release_after_2_secs,
	stuck_ctrl_shift_policy_if_cavebot_on_release_after_3_secs,
	stuck_ctrl_shift_policy_total
};

enum stuck_cursor_policy_t{
	stuck_cursor_policy_none,
	stuck_cursor_policy_do_nothing,
	stuck_cursor_policy_release_instantly,
	stuck_cursor_policy_release_after_1_sec,
	stuck_cursor_policy_release_after_2_secs,
	stuck_cursor_policy_release_after_3_secs,
	stuck_cursor_policy_if_cavebot_on_release_instantly,
	stuck_cursor_policy_if_cavebot_on_release_after_1_sec,
	stuck_cursor_policy_if_cavebot_on_release_after_2_secs,
	stuck_cursor_policy_if_cavebot_on_release_after_3_secs,
	stuck_cursor_policy_total
};

enum scroll_mode_t{
	scroll_mode_none,
	scroll_mode_mouse_wheel,
	scroll_mode_click_on_scrollbar,
	scroll_mode_total
};

enum move_speed_t{
	move_speed_none,
	move_speed_1,
	move_speed_2,
	move_speed_3,
	move_speed_4,
	move_speed_5,
	move_speed_6,
	move_speed_7,
	move_speed_8,
	move_speed_9,
	move_speed_instantaneous,
	move_speed_total
};



