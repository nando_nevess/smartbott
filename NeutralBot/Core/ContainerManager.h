#pragma once
#include "Util.h"
#include "ItemsManager.h"
#include "Interface.h"
#include "Time.h"
#include "..\\PlayerInfo.h"

#define MAX_CONTAINER 16

#pragma region OFFSETS
#define offset_bp_id 0xc
#define offset_address_container 0x4C
#define offset_total_slot 64
#define offset_occuped_slot 68
#define offset_between_slots 0x20
#pragma endregion

struct ContainerPosition {
	ContainerPosition() :container_index(UINT32_MAX), slot_index(UINT32_MAX){}
	ContainerPosition(uint32_t _container_index, uint32_t _slot_index){
		container_index = _container_index;
		slot_index = _slot_index;
	}
	uint32_t container_index, slot_index;
	bool is_null(){
		return container_index == UINT32_MAX && slot_index == UINT32_MAX;
	}
};

typedef std::shared_ptr<ContainerPosition> ContainerPositionPtr;

#pragma pack(push,1)
class tibia_slot {
public:
	tibia_slot() {
		ZeroMemory(this, sizeof(tibia_slot));
	}

	uint32_t idk;
public:
	uint32_t stack_count;
	uint32_t id;

	uint32_t idk2[5];
public:
	bool operator !=(tibia_slot& other){
		return !(*this == other);
	}
	bool operator ==(tibia_slot& other){
		return (other.id == id && other.stack_count == stack_count);
	}
};

typedef tibia_slot ContainerSlot;



class tibia_container_header{
	char idk[12];
public:
	uint32_t bp_item_id;

	char idk1[20];
	char caption[16];
	uint32_t idk22;
	uint32_t len;

	uint32_t idk_again;
public:
	uint32_t total_slots;
	uint32_t used_slots;

	uint32_t idk2;
public:
	uint32_t body_address;

	std::string get_caption();
	bool operator == (tibia_container_header& other){
		if (body_address != other.body_address)
			return false;
		if (bp_item_id != other.bp_item_id)
			return false;
		if (total_slots != other.total_slots)
			return false;
		if (used_slots != other.used_slots)
			return false;
		return true;
	}

	bool operator != (tibia_container_header& other){
		return !(*this == other);
	}
};


class ItemContainer{
	bool open;

	std::string caption;

	uint32_t container_index;

	std::shared_ptr<RightPaneContainer> get_this_interface();

	std::vector<uint32_t> get_visible_container_indexes();
public:

	std::vector<uint32_t> get_all_items_id();

	ContainerPosition get_random_container_slot();

	tibia_container_header* header_info;
	uint32_t memory = 0;
	tibia_slot* slots;
	bool resize();

	ItemContainer(uint32_t index);
	~ItemContainer();

	bool is_browse_field();

	bool is_creature_corpses();
	
	uint32_t get_item_count_by_id(uint32_t id);

	bool is_full();

	
	bool is_open;
	
	uint32_t find_item_slot(uint32_t item_id);
	uint32_t find_item_slot(uint32_t item_id, uint32_t lastIndex);
	std::vector<uint32_t> find_items_slots(std::vector<uint32_t> items);
	bool up_container();

	bool set_slot_row(uint32_t slot_index, int32_t timeout = 1000);
	/*
	get real location of and visible dimension of slot in cli view
	*/
	Rect get_slot_rect_client(uint32_t slot_index);
	Rect get_rect_client();
	bool look_at_index(uint32_t slot_index, int32_t timeout = 1000);

	ContainerPositionPtr get_container_position_to_add_item();

	bool wait_change_content(int32_t timeout = 2000);

	bool close();

	void minimize();

	bool is_minimized();

	void maximize();

	uint32_t get_reverse_lying_corpse_index(int index);

	bool compare_slots(tibia_slot* other_slots, uint32_t other_count);

	bool equals(ItemContainer* other, bool compare_content = false);
	uint32_t get_container_index();
	std::string get_caption();

	void set_caption(std::string caption);

	uint32_t get_id();

	uint32_t get_slot_count(uint32_t index);

	uint32_t get_slot_id(uint32_t index);

	uint32_t get_max_slots();

	uint32_t get_slot_index_to_move_item(uint32_t item_id = UINT32_MAX);

	uint32_t get_not_container_slot();

	uint32_t get_used_slots();

	tibia_slot get_slot_at(uint32_t index);

	bool advance_page();

	bool back_page();
	
	uint32_t current_page_index();

	bool has_next();

	action_retval_t open_next(uint32_t timeout = 2000);

	std::vector<uint32_t> get_slot_indexes_where(uint32_t item_id = 0xffffffff, uint8_t min_count = 0, uint8_t max_count = 100);

	uint32_t get_food_count();

	std::vector<uint32_t> get_visible_slot_indexes();

	Json::Value parse_class_to_json(){
		Json::Value containerItemInfo;
		Json::Value containerItemInfoItems;

		std::vector<uint32_t> containerItems = this->get_all_items_id();

		for (auto item : containerItems){
			Json::Value itemInfo;

			itemInfo["item_id"] = item;
			itemInfo["item_count"] = this->get_item_count_by_id(item);

			containerItemInfoItems.append(itemInfo);
		}

		containerItemInfo["slots_used"] = this->get_used_slots();
		containerItemInfo["slots_free"] = this->get_max_slots() - this->get_used_slots();
		containerItemInfo["slots_total"] = this->get_max_slots();
		containerItemInfo["container_id"] = this->get_id();
		containerItemInfo["container_name"] = this->get_caption();
		containerItemInfo["is_full"] = this->is_full();
		containerItemInfo["has_next"] = this->has_next();
		containerItemInfo["is_minimized"] = this->is_minimized();
		containerItemInfo["containerItemInfoItems"] = containerItemInfoItems;

		return containerItemInfo;
	}
};

typedef std::shared_ptr<ItemContainer> ItemContainerPtr;

class ContainerManager{
	std::map<uint32_t/*key index*/, ItemContainerPtr> *swap_containers;
	std::map<uint32_t/*key index*/, ItemContainerPtr> *containers;
	neutral_mutex mtx_access;
	neutral_mutex mtx_critical_refresh;
	uint32_t critical_refresh_count = 0;
	uint32_t refresh_count = 0;

	bool is_required_critical_refresh();

	void add_critical_refresh_count();

	void sub_critical_refresh_count();
	
public:

	std::shared_ptr<FunctionCallbackAuto> set_auto_critical_refresh();

	std::vector<uint32_t> get_all_items_is_in_open_containers();

	std::vector<uint32_t> get_all_containers_id_opened();

	void wait_refresh(){
		uint64_t current_count = refresh_count;
		while ((refresh_count - current_count) < 2){
			Sleep(1);
		}
	}

	ContainerManager();

	std::map<uint32_t, ItemContainerPtr> get_containers();

	std::map<uint32_t, ItemContainerPtr> get_containers_not_in(std::vector<uint32_t>& containers_id);

	std::map<uint32_t, ItemContainerPtr> get_containers_in(std::vector<uint32_t>& containers_id);

	uint32_t get_item_count_on_container_index(uint32_t item_id, uint32_t index);

	std::vector<ItemContainerPtr> get_browse_fields();

	ItemContainerPtr get_browse_field();

	ItemContainerPtr get_container_by_id(uint32_t id);

	std::vector<ItemContainerPtr> get_containers_by_id(uint32_t id);

	std::vector<ItemContainerPtr> get_containers_by_ids(std::vector<uint32_t>& ids);

	ItemContainerPtr get_container_by_index(uint32_t container_index);

	ItemContainerPtr get_container_by_interface_index(uint32_t cointainer_index);

	ItemContainerPtr get_container_by_caption(std::string caption);

	ItemContainerPtr get_depot_locker_container();

	ItemContainerPtr get_locker_container();

	ItemContainerPtr get_depot_chest_container();
	
	ItemContainerPtr get_depot_box_container(uint32_t item_id);
	
	int get_containers_open();

	void minimize_all_container();

	void maximize_all_container();

	bool is_container_id_completely_full(uint32_t id);

	bool is_container_index_completely_full(uint32_t index);

	bool is_some_container_completely_full();
	
	int find_item_id_from_vector(std::vector<uint32_t> item_ids);

	ContainerPositionPtr find_item_in_vector(std::vector<uint32_t> item_ids);

	ContainerPositionPtr find_pick_item();

	ContainerPositionPtr find_rope_item();

	ContainerPositionPtr find_shovel_item();

	ContainerPositionPtr find_machete_item();

	ContainerPositionPtr find_kitchen_item();

	ContainerPositionPtr find_knife_item();

	ContainerPositionPtr find_spoon_item();

	ContainerPositionPtr find_croowbar_item();

	ContainerPositionPtr find_scythe_item();

	ContainerPositionPtr find_sickle_item();

	bool is_container_index_full(uint32_t index);

	uint32_t get_total_money();

	bool wait_money_change(uint32_t timeout = 1000);

	int32_t wait_open_container_count_change(int32_t timeout, std::function<bool()> or_callback = std::function<bool()>());

	bool wait_container_id_close(uint32_t container_id, int32_t timeout = 1000);

	bool wait_container_index_close(uint32_t container_index, int32_t timeout = 1000);

	bool wait_open_trade(int32_t timeout = 1000);

	bool wait_browse_field_close(int32_t timeout = 1000);

	ContainerPosition find_item(uint32_t item_id, uint32_t container_id = UINT32_MAX);

	uint32_t get_item_count(uint32_t item_id);
	void update();
	void actualize_all_bp();
	void SearchContainerInThree(uint32_t address, uint32_t dept_level, std::vector<uint32_t>* found_addresses);
	void actualize_bp_info(uint32_t pointer, uint32_t container_num);
	void refresh_thread();
	bool reorganize_container();
	bool close_all_containers();
	bool close_containers_by_index(uint32_t index);
	bool close_all_browse_fields();
	bool close_all_creature_containers();
	bool resize_all_containers();
	bool resize_all_browse_fields();
	bool resize_all_creatures_containers();

	bool wait_container_id_open(uint32_t item_id, int32_t timeout = 1000, std::function<bool()> or_callback = std::function<bool()>());
	bool wait_depot_locker_open(int32_t timeout = 1000, std::function<bool()> or_callback = std::function<bool()>());
	bool wait_container_browse_field(int32_t timeout = 1000, std::function<bool()> or_callback = std::function<bool()>());
	
	static ContainerManager* get();

	std::vector<uint32_t> get_open_container_indexes();

	bool compare_containers(std::map<uint32_t, ItemContainerPtr>& containers/*get ref*/, bool compare_items = false);

	std::vector<ContainerPosition> get_container_positions_where(uint32_t container_id = 0xffffffff, uint32_t item_id = 0xffffffff, uint8_t count_min = 0, uint8_t count_max = 100);
	uint32_t get_food_count();
	
	Json::Value parse_class_to_json(){
		Json::Value containerClass;
		Json::Value containersInfo;

		containerClass["count_money_total"] = get_total_money();
		containerClass["count_container_open"] = get_all_containers_id_opened().size();

		std::map<uint32_t, ItemContainerPtr> containers_open = this->get_containers();

		for (auto container : containers_open)
			containersInfo.append(container.second->parse_class_to_json());

		containerClass["containersInfo"] = containersInfo;
		return containerClass;
	}

};
#pragma pack(pop)