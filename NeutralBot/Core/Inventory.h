#pragma once
#include "Util.h"
#include "ContainerManager.h"

enum inventory_slot_t{
	slot_helmet = 1,
	slot_amulet,
	slot_bag,
	slot_armor,
	slot_shield,
	slot_weapon,
	slot_leg,
	slot_boots,
	slot_ring,
	slot_rope,
	slot_none
};

#define offset_qty_body_items 4
#define offset_beetwen_body_item 0x20

class Inventory{
public:
	int body_helmet();

	int body_amulet();

	int body_bag();

	int body_armor();

	int body_shield();

	int body_weapon();

	int body_leg();

	int body_boot();

	int body_ring();

	int body_rope();

	int item_count(int id);

	int weapon_count();

	Coordinate get_slot_type_interface_coordinate(inventory_slot_t type);

	uint32_t get_item_id_by_slot_type(inventory_slot_t type);
	Coordinate get_slot_type_interface_coordinate(std::string str_type);
	static inventory_slot_t get_type_from_str(std::string in);

	bool maximize();
	bool minimize();
	bool look_at_inventory(std::string str_type);
	bool look_at_inventory(inventory_slot_t slot_type);
	bool use_at_inventory(inventory_slot_t slot_type);
	int ammunation_count();
	bool unequip_slot(inventory_slot_t type, uint32_t bp_id_destination = UINT32_MAX);
	bool equip_item(uint32_t item_id, inventory_slot_t inventory_type);

	bool put_item(uint32_t item_id, inventory_slot_t slot_type, uint32_t count);

	bool put_item(uint32_t item_id, inventory_slot_t slot_type, uint32_t count, ContainerPosition container_position);
	bool is_item_equiped(int id);
	bool is_maximized();
	bool set_maximized_state(bool state);
	static Inventory* get();

	Json::Value parse_class_to_json();
};



