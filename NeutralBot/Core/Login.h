#pragma once
#include "Util.h"
#include "TibiaProcess.h"
#include "Input.h"
#include "Time.h"
#include "Interface.h"

#define LOGIN_BUTTON_PADDING_LEFT 80
#define LOGIN_BUTTON_PADDING_DOWN 201
#define LOGIN_WINDOW_CAPTION_OFFSET 0x54

class LoginManager{
	bool automatic_relogin_state = false;
	std::string account = "";
	std::string password = "";
	uint32_t character_index = 0;
	neutral_mutex mtx_access;


public:

	LoginManager();

	void set_automatic_relogin_state(bool state);

	void set_account(std::string accountt);

	void set_password(std::string passwordd);

	void set_character_index(int character_indexx);

	std::string get_account(){
		return account;
	}

	std::string get_password(){
		return password;
	}

	bool get_automatic_relogin_state(){
		return automatic_relogin_state;
	}

	int get_character_index(){
		return character_index;
	}

	void run();

	void CancelAction();

	void refresh();

	void LoginWriteString(std::string str);

	void PressLoginButton();

	void PressEnter();

	bool wait_change_caption(std::string old){
		TimeChronometer time_temp;
		while (time_temp.elapsed_milliseconds() < 2500){
			Sleep(10);

			if (old != GetLoginWindowCaption())
				return true;

		}
		return false;
	}

	void selected_char(int character_index){
		int get_current_char_on_list = 0;
		int Address_window = AddressManager::get()->getAddress(ADDRESS_WINDOW);

		if (TibiaProcess::get_default()->get_current_tibia_version() >= 1095)
			get_current_char_on_list = TibiaProcess::get_default()->read_int(Address_window, { 0x2c, 0x2c, 0x40 });		
		else
			get_current_char_on_list = TibiaProcess::get_default()->read_int(Address_window, { 0x2c, 0x24, 0x10, 0x40 });
		
		if (character_index == UINT32_MAX || character_index < 0 || character_index == get_current_char_on_list)
			return;

		int num_times = get_current_char_on_list - (character_index);
		if (num_times != 0){
			int key_to_send = VK_DOWN;

			if (num_times > 0)
				key_to_send = VK_UP;

			for (int i = 0; i < abs(num_times); i++){
				if (!HighLevelVirtualInput::get_default()->can_use_input())
					continue;

				HighLevelVirtualInput::get_default()->send_virtual_key(key_to_send);
				Sleep(100);
			}			
		}
		return;
	}

	void SendTab();

	std::string GetLoginWindowCaption();

	void TryLogin(std::string account, std::string password, uint32_t character_index, int32_t timeout = 10000);

	static LoginManager* get();

};
