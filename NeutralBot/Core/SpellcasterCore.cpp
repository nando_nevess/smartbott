#pragma once
#include "SpellCasterCore.h" 
#include <thread>
#include "LooterCore.h"
#include "Time.h"
#include "Actions.h"
#include "..\\DevelopmentManager.h"

spell_category_t get_spell_category(SpellBase* spell_ptr){
	if (spell_ptr->get_item_id()){
		return spell_category_item;
	}
	bool is_custom = SpellCasterCore::get()->is_custom_spell(spell_ptr->get_visual_cast_value());
	if (is_custom) {
		return spell_category_recover;
	}
	auto _list = gXMLSpellManager.getspellsInfoList();
	for (auto it : _list){
		it.get_category();

		if (_stricmp(&it.get_cast()[0], &spell_ptr->get_visual_cast_value()[0]) == 0)
			return (spell_category_t)it.get_category();
		
	}
	return spell_category_recover;
}



bool SpellCasterCore::can_spell_condition_none(std::shared_ptr<SpellCondition> condition){
	return true;
}

bool SpellCasterCore::can_spell_condition_not_looting(std::shared_ptr<SpellCondition> condition){
	if (LooterCore::get()->need_operate())
		return false;

	return true;
}
bool SpellCasterCore::can_spell_condition_force_trapped(std::shared_ptr<SpellCondition> condition){
	if (!MapMinimap::is_trapped_around(map_front_t))
		return false;

	return true;
}

bool SpellCasterCore::can_spell_condition_not_hunting(std::shared_ptr<SpellCondition> condition){
	if (HunterCore::get()->need_operate())
		return false;

	return true;
}

bool SpellCasterCore::can_spell_condition_force_use_life_bellow_value(std::shared_ptr<SpellCondition> condition){
	if (fast_hp >= condition->get_integer_value())
		return false;	

	return true;
}

bool SpellCasterCore::can_spell_condition_force_use_mana_bellow_value(std::shared_ptr<SpellCondition> condition){ 
	if (fast_mp >= condition->get_integer_value())
		return false;
	
	return true;
}

bool SpellCasterCore::can_spell_condition_force_use_life_percent_bellow_value(std::shared_ptr<SpellCondition> condition){ 
	if (fast_hppc >= condition->get_integer_value())
		return false ;
	
	return true;
}

bool SpellCasterCore::can_spell_condition_force_use_mana_percent_bellow_value(std::shared_ptr<SpellCondition> condition){ 
	if (fast_mppc >= condition->get_integer_value())
		return false;	

	return true;
}

bool SpellCasterCore::can_spell_condition_force_if_has_target(std::shared_ptr<SpellCondition> condition){ 
	return true;
}

bool SpellCasterCore::can_spell_condition_force_if_target_name(std::shared_ptr<SpellCondition> condition){ 
	return true;
}

bool SpellCasterCore::can_spell_condition_need_to_cast(std::shared_ptr<SpellCondition> condition){
	return true;
}

bool SpellCasterCore::can_spell_condition_not_if_target(std::shared_ptr<SpellCondition> condition){ 
	if (HunterCore::get()->need_operate())
		return false;

	return true;
}

bool SpellCasterCore::can_spell_condition_not_if_target_name(std::shared_ptr<SpellCondition> condition){ 
	std::map<int, std::shared_ptr<CreatureOnBattle>> creatures = BattleList::get()->get_creatures_on_screen();
	for (auto it : creatures)
		if (it.second->name == condition->get_value())
			return false;
	
	return true;
}

bool SpellCasterCore::can_spell_condition_force_if_player_on_screen(std::shared_ptr<SpellCondition> condition){ 
	return true;
}

bool SpellCasterCore::can_spell_condition_not_if_player_on_screen(std::shared_ptr<SpellCondition> condition){
	if (BattleList::get()->player_kill_on_screen())
		return false;

	return true;
}

bool SpellCasterCore::can_spell_condition_require_target_pulsed(std::shared_ptr<SpellCondition> condition){
	if (!HunterCore::get()->is_pulsed())
		return false;

	return true;
}

bool SpellCasterCore::execute_spell_actions_cast(std::shared_ptr<SpellCondition> condition){
	return true;
}

bool SpellCasterCore::execute_spell_actions_none(std::shared_ptr<SpellCondition> condition){
	return true;
}

bool SpellCasterCore::execute_spell_actions_just_cast(std::shared_ptr<SpellCondition> condition){
	return true;
}

bool SpellCasterCore::execute_spell_actions_do_waypoint_wait(std::shared_ptr<SpellCondition> condition){
	timer_waypoint.reset();
	return false;
}

bool SpellCasterCore::execute_spell_actions_do_looter_wait(std::shared_ptr<SpellCondition> condition){
	timer_looter.reset();
	return false;
}

bool SpellCasterCore::execute_spell_actions_do_hunter_wait(std::shared_ptr<SpellCondition> condition){
	timer_hunter.reset();
	return false;
}

void SpellCasterCore::refresh_spell_cooldown() {	
	uint32_t temp_total = AddressManager::get()->getAddress(ADDRESS_TOTAL_SPELLS);
	uint32_t temp_total_adress = TibiaProcess::get_default()->read_int(temp_total, true);

	uint32_t address = AddressManager::get()->getAddress(ADDRESS_POINTER_SPELLS);
	address = TibiaProcess::get_default()->read_int(address, std::vector<uint32_t>{4});

	std::vector<int> temp_spell_in_use;

	for (uint32_t i = 0; i < temp_total_adress; i++){
		temp_spell_in_use.push_back(TibiaProcess::get_default()->read_int(address + 8, false));
		address = TibiaProcess::get_default()->read_int(address + 4, false);
	}

	lock_guard _lock(mtx_access);
	spell_in_use = temp_spell_in_use;
	total_spell_in_use = temp_total;
}

void SpellCasterCore::refresh_basics_spell_cooldown() {
	uint32_t first_time_address = AddressManager::get()->getAddress(ADDRESS_TIBIA_TIME);
	int32_t first_time = TibiaProcess::get_default()->read_int(first_time_address);

	uint32_t address = AddressManager::get()->getAddress(ADDRESS_POINTER_SPELLS_BASIC);
	address = TibiaProcess::get_default()->read_int(address);
	address = TibiaProcess::get_default()->read_int(address, false);

	for (uint32_t i = 0; i < 4; i++) {
		int end_time = (int32_t)TibiaProcess::get_default()->read_int(address + 0x10, false);

		if (first_time >= end_time)
			spell_types_in_use[i] = false;
		else {
			int time = end_time - (int32_t)TibiaProcess::get_default()->read_int(address + 0xc, false);

			if (time < (abs(end_time - first_time)))
				spell_types_in_use[i] = false;
			else
				spell_types_in_use[i] = true;
		}

		address = TibiaProcess::get_default()->read_int(address, false);
	}
}

bool SpellCasterCore::can_cast_spell(std::string words){
	uint32_t cooldownId = gXMLSpellManager.getSpellCooldownId(words);
	
	if (!can_cast_spell_by_cooldown_id(cooldownId)){
		return false;
	}
	return true;
}

bool SpellCasterCore::can_cast_item(int item_id, int cooldownTime){
	TimeChronometer cooldownId = itens_in_use[item_id];

	if (cooldownId.elapsed_milliseconds() < cooldownTime)
		return false;

	return true;
}

bool SpellCasterCore::can_cast_custom_spell_name(std::string spell_name, int cooldownTime){
	lock_guard _lock(mtx_access);
	auto cooldownId = custon_spell_in_use.find(spell_name);

	if (cooldownId != custon_spell_in_use.end()) {
		if (cooldownId->second.elapsed_milliseconds() < cooldownTime)
			return false;
	}

	return true;
}

bool SpellCasterCore::can_cast_spell_by_cooldown_id(int spell_id) {
	lock_guard _lock(mtx_access);
	if (std::find(spell_in_use.begin(), spell_in_use.end(), spell_id) == spell_in_use.end())
		return true;
	return false;
}

bool SpellCasterCore::cast_words(std::string words){
	if (!can_cast_spell(words))
		return false;

	return Actions::says(words) == action_retval_success;
}

bool SpellCasterCore::check_spell_condition(int spell_condition) {
	if (spell_condition == 0) {
		return true;
	}
	else if (spell_condition == 1) {
		if (BattleList::get()->have_other_player())
			return true;
	}
	else if (spell_condition == 2) {
		if (!BattleList::get()->have_other_player())
			return true;
	}
	return false;
}

bool SpellCasterCore::is_custom_spell(std::string words){
	uint32_t cooldownId = gXMLSpellManager.getSpellCooldownId(words);

	if (cooldownId == 0)
		return true;

	return false;
}

void SpellCasterCore::make_show_cooldown_bar() {
	uint32_t address = AddressManager::get()->getAddress(ADDRESS_COLLDOWN_BAR) + TibiaProcess::get_default()->base_address;
	uint32_t value = TibiaProcess::get_default()->read_byte(address, false);

	if (value == 0)
		TibiaProcess::get_default()->write_byte(address, 1);
}

void SpellCasterCore::update_fast_vars(){
	current_creature_value = HunterCore::get()->get_creature_points().first;
	fast_target_needs_work = (NeutralManager::get()->get_core_states(CORE_HUNTER) == ENABLED
		&& HunterCore::get()->get_need());
	fast_looter_needs_work = (NeutralManager::get()->get_core_states(CORE_LOOTER) == ENABLED
		&& LooterCore::get()->need_operate());
	fast_hppc = TibiaProcess::get_default()->character_info->hppc();
	fast_mppc = TibiaProcess::get_default()->character_info->mppc();
	fast_hp = TibiaProcess::get_default()->character_info->hp();
	fast_mp = TibiaProcess::get_default()->character_info->mp();

	auto target = BattleList::get()->get_target();
	fast_target_id = 0;
	if (target){
		fast_target_name = target->name;
		fast_target_id = target->id;
	}
	fast_player_on_screen = BattleList::get()->have_other_player();
}

void SpellCasterCore::cast_spell(){
	bool state_health = SpellManager::get()->get_health_spells_state();
	bool state_attack = SpellManager::get()->get_attack_spells_state();

	if (!state_health && !state_attack)
		return;

	update_fast_vars();
	
	std::vector<std::shared_ptr<SpellHealth>> current_health_spell_to_cast;	

	std::shared_ptr<SpellHealth> spell_healing_words;
	std::shared_ptr<SpellHealth> spell_healing_item;
	std::shared_ptr<SpellHealth> spell_support;
	
	if (state_health){
		current_health_spell_to_cast = get_health_spell_to_cast();
		for (auto it : current_health_spell_to_cast){
			switch (get_spell_category(it.get())){
				case spell_category_item:{
					spell_healing_item = it;
				}
				break;
				case spell_category_recover:{
					spell_healing_words = it;
				}
				break;
				case spell_category_support:{
					spell_support = it;
				}
				break;
			}
		}
		if (spell_healing_item)
			cast_health_spell(spell_healing_item);
		if (spell_healing_words)
			cast_health_spell(spell_healing_words);
		if (spell_support)
			cast_health_spell(spell_support);
	}

	Coordinate cast_coordinate;
	std::shared_ptr<SpellAttack> current_attack_spell_to_cast = (!gMapTilesPtr->get_another_creatures_on_self_tile().size() && state_attack)
		? get_attack_spell_to_cast(cast_coordinate) : nullptr;
	

	if (current_attack_spell_to_cast != nullptr)
		cast_attack_spell(current_attack_spell_to_cast, cast_coordinate);	
}

bool SpellCasterCore::spell_match_conditions(SpellBase* spell_ptr){
	bool retval = true;
	bool retval_temp = true;
	bool return_temp = true;
	if (spell_ptr->vector_condition.size() <= 0)
		return true;

	for (auto condition : spell_ptr->vector_condition){
		auto condition_rule = condition;
		switch (condition->condition_type){
		case spell_condition_none:{
			continue;
		}
			break;
		case spell_condition_not_looting:{
			retval = can_spell(spell_condition_not_looting);
			if (retval)
				continue;

			retval_temp = execute_action_temp(condition_rule, condition->action_type);

			if (!retval_temp)
				return_temp = false;
		}
			break;
		case spell_condition_not_hunting:{
			retval = can_spell(spell_condition_not_hunting);
			if (retval)
				continue;

			retval_temp = execute_action_temp(condition_rule, condition->action_type);

			if (!retval_temp)
				return_temp = false;
		}
			break;
		case spell_condition_force_use_life_bellow_value:{
			retval = can_spell(spell_condition_force_use_life_bellow_value);
			if (retval){
				retval_temp = execute_action_temp(condition_rule, condition->action_type);
				
				if (retval_temp)
				return_temp = true;
			}
		}
			break;
		case spell_condition_force_use_mana_bellow_value:{
			retval = can_spell(spell_condition_force_use_mana_bellow_value);
			if (retval){
				retval_temp = execute_action_temp(condition_rule, condition->action_type);
			
				if (retval_temp)
					return_temp = true;
			}
		}
			break;
		case spell_condition_force_use_life_percent_bellow_value:{
			retval = can_spell(spell_condition_force_use_life_percent_bellow_value);
			if (retval){
				retval_temp = execute_action_temp(condition_rule, condition->action_type);
				
				if (retval_temp)
					return_temp = true;
			}
		}
			break;
		case spell_condition_force_use_mana_percent_bellow_value:{
			retval = can_spell(spell_condition_force_use_mana_percent_bellow_value);
			if (retval){
				retval_temp = execute_action_temp(condition_rule, condition->action_type);
			
				if (retval_temp)
					return_temp = true;
			}
		}
			break;
		case spell_condition_if_target_name:{
			retval = can_spell(spell_condition_force_if_target_name);
			if (retval)
				continue;

			retval_temp = execute_action_temp(condition_rule, condition->action_type);

			if (!retval_temp)
				return_temp = false;
		}
			break;
		case spell_condition_if_player_on_screen:{
			retval = can_spell(spell_condition_force_if_player_on_screen)
			if (retval)
				continue;

			retval_temp = execute_action_temp(condition_rule, condition->action_type);

			if (!retval_temp)
				return_temp = false;
		}
			break;
		case spell_condition_trapped:{
			retval = can_spell(spell_condition_force_trapped);
			if (retval)
				continue;

			retval_temp = execute_action_temp(condition_rule, condition->action_type);

			if (!retval_temp)
				return_temp = false;
		}
			break;
		default:
			break;
		}
	}
	return return_temp;
}

std::vector<std::shared_ptr<SpellHealth>> SpellCasterCore::get_health_spell_to_cast(){
	std::vector<std::shared_ptr<SpellHealth>> retval;
	
	std::shared_ptr<SpellHealth> spell_healing_words;
	std::shared_ptr<SpellHealth> spell_healing_item;
	std::shared_ptr<SpellHealth> spell_support;
	
	std::function<uint32_t(spell_category_t)> get_current_spell_order = [&](spell_category_t cat) -> uint32_t {
		switch (cat){
			case spell_category_item:{
				if (!spell_healing_item)
					return 0;

				spell_healing_item->get_order();
			}
			break;
			case spell_category_recover:{
				if (!spell_healing_words)
					return 0;

				return spell_healing_words->get_order();
			}
			break;
			case spell_category_support:{
				if (!spell_support)
					return 0;

				return spell_support->get_order();
			}
			break;
		}
		return 0;
	};

	bool can_cast_healing_item = time_healing_item.elapsed_milliseconds() > 70;

	if (!health_spell_can_spam())
		return retval;

	if (spell_types_in_use[1])
		return retval;
	
	uint32_t player_health;
	uint32_t player_mana;

	auto healthList = SpellManager::get()->getHealthList();
	for (auto spell : healthList) {
		spell_category_t cat = get_spell_category(spell.second.get());
		
		if (get_current_spell_order(cat) > spell.second->get_order())
			continue;		

		if (spell.second->get_use_percent_hp())
			player_health = TibiaProcess::get_default()->character_info->hppc();		
		else
			player_health = TibiaProcess::get_default()->character_info->hp();
					
		if (spell.second->get_use_percent_mp())
			player_mana = TibiaProcess::get_default()->character_info->mppc();
		else
			player_mana = TibiaProcess::get_default()->character_info->mp();

		uint32_t health_type = spell.second->get_health_type();

		if (health_type == 0) {
			if (player_health > spell.second->get_max_hp() || player_health < spell.second->get_min_hp())
				continue;

			if (player_mana > spell.second->get_max_mp() || player_mana < spell.second->get_min_mp())
				continue;
		} 
		else if (health_type == 1) {
			if (player_mana > spell.second->get_max_mp() || player_mana < spell.second->get_min_mp())
				continue;

			if (player_health > spell.second->get_max_hp() || player_health < spell.second->get_min_hp())
				continue;
		}

		std::string spellName = spell.second.get()->get_visual_cast_value();

		if (spellName == "")
			continue;
		
		uint32_t item_id = spell.second->get_item_id();
		if (item_id > 0) {
			if (!can_cast_healing_item)
				continue;

			if (!can_cast_item(item_id, spell.second->get_cooldown()))
				continue;

			if (ContainerManager::get()->get_item_count(item_id) > 0)
				if (!spell_match_conditions((SpellBase*)spell.second.get()))
					continue;
				
			/*if ((SpellBase*)spell.second.get()->get_no_use_with_hunt())
				if (HunterCore::get()->need_operate())
					continue;

			if ((SpellBase*)spell.second.get()->get_no_use_with_loot())
				if (LooterCore::get()->need_operate())
					continue;*/

			hotkey_t hotkeyNumber = HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_on_self, false);
			if (hotkeyNumber == hotkey_none)
					continue;

		} else {
			if (!spell_match_conditions((SpellBase*)spell.second.get()))
				continue;

			/*if ((SpellBase*)spell.second.get()->get_no_use_with_hunt())
				if (HunterCore::get()->need_operate())
					continue;

			if ((SpellBase*)spell.second.get()->get_no_use_with_loot())
				if (LooterCore::get()->need_operate())
					continue;*/

			bool is_custom = is_custom_spell(spellName);
			if (is_custom) {
				if (!can_cast_custom_spell_name(spellName, spell.second->get_cooldown()))
					continue;
			}
			
			if (!can_cast_spell(spellName))
				continue;
		}

		switch (cat){
			case spell_category_item:{
				spell_healing_item = spell.second;
			}
			break;
			case spell_category_recover:{
				spell_healing_words = spell.second;
			}
			break;
			case spell_category_support:{
				spell_support = spell.second;
			}
			break;
		}
	}

	if (spell_healing_words)
		retval.push_back(spell_healing_words);
	if (spell_healing_item)
		retval.push_back(spell_healing_item);
	if (spell_support)
		retval.push_back(spell_support);

	return retval;
}

void SpellCasterCore::cast_health_spell(std::shared_ptr<SpellHealth> spell) {
	std::string spellName = spell->get_visual_cast_value();
	uint32_t item_id = spell->get_item_id();
	
	if (!spell->get_checked())
		return;

	timer_spells.reset();
	if (item_id > 0) {
		hotkey_t hotkeyNumber = HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_on_self, false);

		TimeChronometer time;
		itens_in_use[item_id] = time;

		HotkeyManager::get()->useHotkey(hotkeyNumber);

		upate_last_try_to_use_health_spell();
	} else {
		hotkey_t hotkeyNumber = HotkeyManager::get()->findHotkeyByText(spellName);
		if (hotkeyNumber != hotkey_none) 
			HotkeyManager::get()->useHotkey(hotkeyNumber);		
		else 
			Actions::get()->says(spellName, "");		

		if (is_custom_spell(spellName)) {
			lock_guard _lock(mtx_access);
			custon_spell_in_use[spellName].reset();
		}
		upate_last_try_to_use_health_spell();
	}
}

std::shared_ptr<SpellAttack> SpellCasterCore::get_attack_spell_to_cast(Coordinate& cast_out_coord) {

	if (!attack_spell_can_spam())
		return nullptr;

	if (spell_types_in_use[0]){
		return nullptr;
	}

	auto attackList = SpellManager::get()->getAttackList();
	std::shared_ptr<SpellAttack> possibleSpellToCast = nullptr;

	for (auto spell : attackList) {
		if (possibleSpellToCast != nullptr && spell.second->get_order() < possibleSpellToCast->get_order())
			continue;
		
		if (!spell.second->get_checked())
			continue;

		if (!check_spell_condition(spell.second->get_spell_condition()))
			continue;

		CreatureOnBattlePtr current_target = BattleList::get()->get_target();
		if (spell.second->get_spell_type() == spell_type_t::spell_type_on_target || spell.second->get_spell_type() == spell_type_t::spell_type_rune_on_target) {
			if (!current_target || !spell.second->contains_monster_in_list(std::string(current_target->name), current_target->life)){
				if (!current_target)
					continue;

				if (!spell.second->get_any_monster())
					continue;

				if (!current_target->is_monster())
					continue;
			}

			Coordinate self_pos = TibiaProcess::get_default()->character_info->get_self_coordinate();
			bool is_sight = gMapTilesPtr->check_sight_line(self_pos, current_target->get_coordinate_monster(), false);
			if (!is_sight)
				continue;
		}

		uint32_t player_health;
		if (spell.second->get_use_percent_hp())
			player_health = TibiaProcess::get_default()->character_info->hppc();
		else
			player_health = TibiaProcess::get_default()->character_info->hp();

		if (player_health < spell.second->get_min_hp())
			continue;

		uint32_t player_mana;
		if (spell.second->get_use_percent_mp())
			player_mana = TibiaProcess::get_default()->character_info->mppc();
		else
			player_mana = TibiaProcess::get_default()->character_info->mp();

		if (player_mana < spell.second->get_min_mp())
			continue;

		uint32_t currentMonsterCountInArea = 0;
		uint32_t currentMonsterCountInList = 0;
		std::map<int, std::shared_ptr<CreatureOnBattle>> monsterListInArea = BattleList::get()->get_creatures_on_screen(false, true);
		for (auto CreaturesOnBattle : monsterListInArea) {
			std::string monsterName = CreaturesOnBattle.second->name;
			if (!spell.second->contains_monster_in_list(monsterName, CreaturesOnBattle.second->life)){
				if (!CreaturesOnBattle.second)
					continue;

				if (!spell.second->get_any_monster())
					continue;

				if (!CreaturesOnBattle.second->is_monster())
					continue;
			}

			Coordinate monsterCoord = CreaturesOnBattle.second->get_coordinate_monster();
			Coordinate selfCoord = TibiaProcess::get_default()->character_info->get_self_coordinate();

			currentMonsterCountInArea++;

			if (selfCoord.get_axis_max_dist(monsterCoord) > spell.second->get_maximum_sqm())
				continue;

			currentMonsterCountInList++;
		}

		if (currentMonsterCountInArea < spell.second->get_monster_in_area_min())
			continue;
		if (currentMonsterCountInArea > spell.second->get_monster_in_area_max())
			continue;

		if (currentMonsterCountInList < spell.second->get_monster_qty_list_min())
			continue;
		if (currentMonsterCountInList > spell.second->get_monster_qty_list_max())
			continue;
		
		uint32_t item_id = spell.second->get_item_id();
		if (item_id > 0) {
			if (ContainerManager::get()->get_item_count(item_id) <= 0)
				continue;

			if (!spell_match_conditions((SpellBase*)spell.second.get()))
				continue;

			/*if ((SpellBase*)spell.second.get()->get_no_use_with_hunt())
				if (HunterCore::get()->need_operate())
					continue;

			if ((SpellBase*)spell.second.get()->get_no_use_with_loot())
				if (LooterCore::get()->need_operate())
					continue;*/

			if (!can_cast_item(item_id, spell.second->get_cooldown()))
				continue;

			hotkey_t hotkeyNumber;
			if (spell.second->get_spell_type() == spell_type_t::spell_type_rune_on_target)
				hotkeyNumber = HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_on_target, false);
			 else if (spell.second->get_spell_type() == spell_type_t::spell_type_rune_area)
				hotkeyNumber = HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_with_crosshairs, false);;			
		} else {
			if (!spell_match_conditions((SpellBase*)spell.second.get()))
				continue;

			/*if ((SpellBase*)spell.second.get()->get_no_use_with_hunt())
				if (HunterCore::get()->need_operate())
					continue;

			if ((SpellBase*)spell.second.get()->get_no_use_with_loot())
				if (LooterCore::get()->need_operate())
					continue;*/

			std::string spellName = spell.second->get_visual_cast_value();
			if (is_custom_spell(spellName)) {
				if (!can_cast_custom_spell_name(spellName, spell.second->get_cooldown())) {
					continue;
				}
			}

			if (!can_cast_spell(spellName))
				continue;
		}

		uint32_t type = spell.second->get_spell_type();
		if (type == spell_type_t::spell_type_rune_area){
			cast_out_coord = get_best_pos_to_send_item(spell.second);
			if (cast_out_coord.is_null())
				continue;
		}

		possibleSpellToCast = spell.second;
	}

	return possibleSpellToCast;
}

void SpellCasterCore::cast_attack_spell(std::shared_ptr<SpellAttack> spell, Coordinate& cast_coordinate) {
	uint32_t item_id = spell->get_item_id();
	std::string spellName = spell->get_visual_cast_value();

	timer_spells.reset();
	if (spell->get_spell_type() == spell_type_t::spell_type_on_target) {
		hotkey_t hotkeyNumber = HotkeyManager::get()->findHotkeyByText(spellName);
		if (hotkeyNumber != hotkey_none)
			HotkeyManager::get()->useHotkey(hotkeyNumber);
		else
			Actions::get()->says(spellName, "");
		
		if (is_custom_spell(spellName)) {
			lock_guard _lock(mtx_access);
			custon_spell_in_use[spellName].reset();
		}
	} 
	else if (spell->get_spell_type() == spell_type_t::spell_type_area) {
		if (!check_spell_condition(spell->get_spell_condition()))
			return;

		hotkey_t hotkeyNumber = HotkeyManager::get()->findHotkeyByText(spellName);
		if (hotkeyNumber != hotkey_none)
			HotkeyManager::get()->useHotkey(hotkeyNumber);
		else 
			Actions::get()->says(spellName, "");

		if (is_custom_spell(spellName)) {
			lock_guard _lock(mtx_access);
			custon_spell_in_use[spellName].reset();
		}
	} else if (spell->get_spell_type() == spell_type_t::spell_type_rune_on_target) {
		hotkey_t hotkeyNumber = HotkeyManager::get()->get_hotkey_match("", item_id, hotkey_cast_type_on_target, false);
		itens_in_use[item_id].reset();
		HotkeyManager::get()->useHotkey(hotkeyNumber);
	}
	else if (spell->get_spell_type() == spell_type_t::spell_type_rune_area) {
		itens_in_use[item_id].reset();
		auto coord = (TibiaProcess::get_default()->character_info->get_self_coordinate() - cast_coordinate);
		if (!HotkeyManager::get()->useItemAtLocation(item_id, cast_coordinate)){
			int total_try_count = 0;
			while (total_try_count < Actions::Max_Count) {
				Sleep(50);

				Actions::use_crosshair_item_on_coordinate(cast_coordinate, item_id);

				total_try_count++;
			}
		}
	}
	upate_last_try_to_use_attack_spell();
}

Coordinate SpellCasterCore::get_best_pos_to_send_item(std::shared_ptr<SpellAttack> spellAttack) {
	std::map<int, std::shared_ptr<CreatureOnBattle>> creatures = BattleList::get()->get_creatures_on_screen();
	if (!spellAttack.get()->get_any_monster()){
		for (auto creature = creatures.begin(); creature != creatures.end();){
			if (!creature->second->is_monster())
				creature = creatures.erase(creature);
			else if (!spellAttack.get()->contains_monster_in_list(std::string(creature->second->name), creature->second->life))
				creature = creatures.erase(creature);
			else
				creature++;
		}
	}
	else{
		for (auto creature = creatures.begin(); creature != creatures.end();){
			if (!creature->second->is_monster())
				creature = creatures.erase(creature);
			else
				creature++;
		}
	}

	if (creatures.size() < spellAttack.get()->get_monster_qty_list_min())
		return Coordinate();
	else if (creatures.size() > spellAttack.get()->get_monster_qty_list_max())
		return Coordinate();

	use_spell_type find_type = use_spell_by_safety;// SpellManager::get()->get_spell_type();

	Coordinate playerPos = TibiaProcess::get_default()->character_info->get_self_coordinate();
	Coordinate bestPos(0, 0, 0);

	playerPos.x = playerPos.x - 7;
	playerPos.y = playerPos.y - 5;
	
	std::vector < std::pair<Coordinate/*coord*/, std::vector<Coordinate>> >
		creatures_near;

	std::pair<uint32_t, uint32_t> last_creature_area_info(0, 0);
	for (int x = 0; x < 14; x++) {
		for (int y = 0; y < 10; y++) {
			Coordinate tempPos(playerPos.x + x, playerPos.y + y, playerPos.z);
			Coordinate self_pos = TibiaProcess::get_default()->character_info->get_self_coordinate();
			bool is_sight = gMapTilesPtr->check_sight_line(self_pos, tempPos, true);
			if (!is_sight)
				continue;

			area_type_t spell_area = SpellManager::get()->getSpellAreaEffectByitem_id(spellAttack.get()->get_item_id());
			std::vector<CreatureOnBattlePtr> monsterListInArea = BattleList::get_creatures_on_area_effect(creatures, tempPos, spell_area);
			uint32_t count = monsterListInArea.size();
			
			if (count < spellAttack.get()->get_monster_in_area_min())
				continue;
			else if (count > spellAttack.get()->get_monster_in_area_max())
				continue;

			std::vector<Coordinate> coordinates;
			for (auto it : monsterListInArea)
				coordinates.push_back(it->get_coordinate_monster());
			
			creatures_near.push_back(std::pair<Coordinate, std::vector<Coordinate>>( 
				tempPos, coordinates));
		}
	}

	/*
	switch (find_type){
		case use_spell_type::use_spell_by_mounster_count:{
			uint32_t last_max = 0;
			Coordinate retval;
			for (auto it : creatures_near){
				if (it.second.size() > last_max){
					last_max = it.second.size();
					retval = it.first;
				}
			}
			return retval;
		}
		case use_spell_type::use_spell_by_safety:
		case use_spell_type::use_spell_fast:
		{*/
			int max_dist = 15;
			int current_creature_count = 0;
			int last_middle_dist = 0;

			Coordinate retval;

			uint32_t max_now = 0;
			for (auto it : creatures_near){
				
			//	if (!gMapTilesPtr->check_sight_line(playerPos, it.first, true))
			//		continue;

				if (max_now <= it.second.size()){
					int max_dist_now = 0;
					for (auto creature : it.second){
						int dist = it.first.get_axis_max_dist(creature);
						if (dist > max_dist_now){
							max_dist_now = dist;
						}
					}

					
					if (max_now == it.second.size()){
						if (max_dist_now <= max_dist){

							//	current_creature_count = creature_count;
							retval = it.first;
							max_dist = max_dist_now;
							last_middle_dist = playerPos.get_axis_max_dist(retval);
							
							/*	int creature_count = it.second.size();
							if (creature_count >= current_creature_count){

							current_creature_count = creature_count;
							retval = it.first;
							max_dist = max_dist_now;
							last_middle_dist = playerPos.get_axis_max_dist(retval);
							}
							else if (creature_count == current_creature_count){
							int now_diff_from_middle = playerPos.get_axis_max_dist(retval);
							if (now_diff_from_middle < last_middle_dist){
							current_creature_count = creature_count;
							retval = it.first;
							max_dist = max_dist_now;
							last_middle_dist = now_diff_from_middle;
							}
							}*/
						}
					}
					else{
						retval = it.first;
						max_dist = max_dist_now;
						last_middle_dist = playerPos.get_axis_max_dist(retval);
						max_now = it.second.size();
					}
				}
				
			}
			return retval;
		/*}
		break;
	default:
		break;
	}*/
	return bestPos;
};

void SpellCasterCore::executor(){
	MONITOR_THREAD(__FUNCTION__)
	while (true) {
		Sleep(10);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!TibiaProcess::get_default()->get_is_logged())
			continue;

		LOG_TIME_INIT
		if (!can_execute()){
			LOG_TIME_SET_SUB			
			continue;
		}
		
		if (timer_spells.elapsed_milliseconds() > 1000)
			var_need_spells = false;
		else
			var_need_spells = true;

		if (timer_hunter.elapsed_milliseconds() > 2000)
			var_need_hunter = false;		
		else
			var_need_hunter = true;		

		if (timer_looter.elapsed_milliseconds() > 2000)
			var_need_looter = false;
		else
			var_need_looter = true;

		if (timer_waypoint.elapsed_milliseconds() > 2000)
			var_need_waypoint = false;
		else
			var_need_waypoint = true;

		LOG_TIME_SET_SUB
		refresh_basics_spell_cooldown();
		LOG_TIME_SET_SUB
		refresh_spell_cooldown();
		LOG_TIME_SET_SUB
		cast_spell();
		LOG_TIME_SET_SUB
	}
}

void SpellCasterCore::upate_last_try_to_use_health_spell(){
	last_try_cast_health_spell.reset();
}

void SpellCasterCore::upate_last_try_to_use_attack_spell(){
	last_try_cast_attack_spell.reset();
}

bool SpellCasterCore::attack_spell_can_spam(){
	return (40 < last_try_cast_attack_spell.elapsed_milliseconds());
}

bool SpellCasterCore::health_spell_can_spam(){
	return (20 < last_try_cast_health_spell.elapsed_milliseconds());
}

SpellCasterCore::SpellCasterCore(){
	default_spell_spam = 20;
	std::thread([&](){
		SpellCasterCore::executor();
	}).detach();
}

void SpellCasterCore::init(){
	static neutral_mutex mtx;
	mtx.lock();

	if (!mSpellCasterCore)
		mSpellCasterCore = new SpellCasterCore();

	mtx.unlock();
}

bool SpellCasterCore::can_execute(){
	if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_SPELLCASTER) != ENABLED)
		return false;

	return true;
}


bool SpellCasterCore::need_operate_spells(){
	return var_need_spells;
}

bool SpellCasterCore::need_operate_hunter(){
	return var_need_hunter;
}
bool SpellCasterCore::need_operate_looter(){
	return var_need_looter;
}
bool SpellCasterCore::need_operate_waypoint(){
	return var_need_waypoint;
}

SpellCasterCore* SpellCasterCore::mSpellCasterCore;