#pragma once
#include "Util.h"

#define Interface ClientGeneralInterface::get()

class COORDINATES{
public:
	/*
	FIX WARNINGS
	theses containers location are not fine, the x value in older version was
	client window.x - value
	now it is container.real_x + value
	*/
	static COORD COORDINATE_BUTTON_UP_CONTAINER;// { 31, 7 };
	static COORD COORDINATE_BUTTON_CLOSE_CONTAINER;// { 8, 7 };
	static COORD COORDINATE_BUTTON_MINIMIZE_CONTAINER;

	static COORD COORDINATE_BUTTON_FOLLOW;// { 15, 33 };
	static COORD COORDINATE_BUTTON_NOT_FOLLOW;// { 15, 12 };
	static COORD COORDINATE_BUTTON_FOLLOW_MINIMIZED;// { 86, 30 };
	static COORD COORDINATE_BUTTON_NOT_FOLLOW_MINIMIZED;// { 104, 30 };

	static COORD COORDINATE_BUTTON_ATTACK_MODE_MINIMIZED;// { 105, 12 };
	static COORD COORDINATE_BUTTON_ATTACK_MODE;// { 40, 12 };
	static COORD COORDINATE_BUTTON_BALANCED_MODE_MINIMIZED;// { 88, 12 };

	static COORD COORDINATE_BUTTON_BALANCED_MODE;// { 40, 33 };
	static COORD COORDINATE_BUTTON_DEFENSIVE_MODE_MINIMIZED;// { 67, 12 };
	static COORD COORDINATE_BUTTON_DEFENSIVE_MODE;// { 40, 56 };

	static COORD COORDINATE_BUTTON_SKULL_OLD;// { 15, 74 };
	static COORD COORDINATE_BUTTON_SKULL_OLD_MINIMIZED;// { 70, 40 };

	static COORD COORDINATE_BUTTON_SKULL_NEW_0;// { 40, 103 };
	static COORD COORDINATE_BUTTON_SKULL_NEW_2;// { 40, 125 };
	static COORD COORDINATE_BUTTON_SKULL_NEW_1;// { 15, 103 };
	static COORD COORDINATE_BUTTON_SKULL_NEW_3;// { 15, 125 };

	static COORD COORDINATE_BUTTON_SKULL_NEW_0_minimized;// { 40, 15 };
	static COORD COORDINATE_BUTTON_SKULL_NEW_2_minimized;// { 40, 35 };
	static COORD COORDINATE_BUTTON_SKULL_NEW_1_minimized;// { 15, 15 };
	static COORD COORDINATE_BUTTON_SKULL_NEW_3_minimized;// { 15, 35 };

	static COORD COORDINATE_BUTTON_MAXIMIZE_BODY;// { 158, 11 };

	static COORD COORDINATE_TRADE_BUTTON_OK;// { 25, 144 };
	static COORD COORDINATE_TRADE_BUTTON_SELL;// { 27, 30 };
	static COORD COORDINATE_TRADE_BUTTON_BUY;// { 76, 30 };
	static COORD COORDINATE_TRADE_FIRST_ITEM;// { 52, 51 };
	static COORD COORDINATE_TRADE_SCROLL;// { 9, 67 };
	static COORD COORDINATE_TRADE_SCROLL_COUNT;// { COORDINATE_TRADE_FIRST_ITEM.X, 103 };

	static COORD COORDINATE_HELMET;
	static COORD COORDINATE_AMULET;
	static COORD COORDINATE_BAG;
	static COORD COORDINATE_ARMOR;
	static COORD COORDINATE_SHIELD;
	static COORD COORDINATE_WEAPON;
	static COORD COORDINATE_LEG;
	static COORD COORDINATE_BOOTS;
	static COORD COORDINATE_RING;
	static COORD COORDINATE_ROPE;
};

class OFFSETS{
public:
	static int offset_right_pane_child_position_x;// 0x0; // deve ser revisado nao estava em uso porem, nao esta correto
	static int offset_right_pane_child_position_y;// 0x18;
	static int offset_right_pane_child_type;// 0x2c;
	static int offset_right_pane_child_height;// 0x20;
	static int offset_right_pane_child_width;// 0x0; //deve ser revisado nao estava em uso porem, nao esta correto

	static int pos_offset_x;// 0x14;
	static int pos_offset_y;// 0x18;
	static int size_offset_x;// 0x1C;
	static int size_offset_y;// 0x20;

	static int offset_game_display_left;// 0x14;
	static int offset_game_display_top;// 0x18;
	static int offset_game_display_width;// 0x1C;
	static int offset_game_display_height;// 0x20;
};

#define DEFAULT_CONTAINER_HEIGHT 60
#define DEFAULT_BROWSE_FIELD_CONTAINER_HEIGHT 84
#define CONTAINER_DEFAULT_WIDTH 176
/*
WARNING FIX CONTAINER_DEFAULT_WIDTH IS NOT CORRECT VALUE
*/
enum right_pane_child_t{
	right_pane_inventory_maximized = 1,
	right_pane_inventory_minimized = 2,
	right_pane_battle = 07,
	right_pane_trade = 10,
	right_pane_container_1 = 0x40,
	right_pane_container_2,
	right_pane_container_3,
	right_pane_container_4,
	right_pane_container_5,
	right_pane_container_6,
	right_pane_container_7,
	right_pane_container_8,
	right_pane_container_9,
	right_pane_container_10,
	right_pane_container_11,
	right_pane_container_12,
	right_pane_container_13,
	right_pane_container_14,
	right_pane_container_15,
	right_pane_container_16
};

struct RightPaneChildWidget{
	int width;
	int height;
	int x;
	int y;
	Rect get_rect_on_client_view();
};

/*DONOT EDIT STRUCTURE, STRUCTURE IS AS IN TIBIA MEMORY SPACE*/
#pragma pack(push,1)
class RightPaneContainer{
public:
	uint32_t self_address;// in tibia memory this is not the current addres, the real description is down content
	/*84 BE 46 01 
	uint32_t idk1;//provavel address do anterior 0
	*/
	uint32_t idk2;//30 7B CC 03 
	uint32_t idk3;//08 00 00 00 
	uint32_t idk4;//80 31 2F 00 
	uint32_t address_next;//60 65 7D 03
	uint32_t x;//00 00 00 00 
	uint32_t y;//13 00 00 00 
	uint32_t width;//	B0 00 00 00 
	uint32_t heigth;//66 00 00 00 
	uint32_t idk7;//88 AA BB 03 
	uint32_t idk8;//00 00 00 00 
	uint32_t type;//40 00 00 00

	
	uint32_t get_pixel_hidden_slots(bool is_browse_field = false);

	bool resize(bool is_browse_field = false);

	bool resize(Point dimension);

	Point get_real_location();

	Point get_real_close_button_location();

	Point get_real_minimize_button_location();

	Point get_real_up_button_location();

	bool up();
	bool close();
	bool minimize();
	bool maximize();
	bool is_minimized();

	Rect get_rect_on_client_view();
};

class RightPaneContainerBattle :RightPaneContainer{
public:
	uint32_t get_hidden_pixels();

	Rect get_listbox_rect_on_client_view();

	Point get_scroll_point();

	bool scroll_up(uint32_t count);

	bool scroll_down(uint32_t count);

	bool set_listbox_index_visible(uint32_t index);

	Rect get_clickable_area_to_listbox_index(uint32_t index);
};

class WindowDialog{
public:
	uint32_t address = 0;
	uint32_t get_move_scroll_count();
	bool set_scroll_count(uint32_t count);
	std::string get_window_caption();
};
#pragma pack(pop)

class ClientGeneralInterface{
public:
	static ClientGeneralInterface* get(){
		static ClientGeneralInterface* mClientGeneralInterface = nullptr;
		if (!mClientGeneralInterface)
			mClientGeneralInterface = new ClientGeneralInterface;
		return mClientGeneralInterface;
	}

	static Coordinate get_client_placement();

	static Coordinate get_client_dimension();

	int get_right_pane_child_address(right_pane_child_t id_type);

	std::shared_ptr<RightPaneChildWidget> findRightPaneClild(right_pane_child_t id_type);

	std::shared_ptr<RightPaneChildWidget> getEquipmentChildMinimized();

	std::shared_ptr<RightPaneChildWidget> getEquipmentChildMaximized();
	
	uint32_t get_game_window_pos_x();

	uint32_t get_game_window_pos_y();

	Point get_game_window_pos();

	Point get_game_window_size();

	Point get_coordinate_as_pixel(Point point);

	void set_maximized();

	void set_minimized();

	std::shared_ptr<RightPaneContainer> get_container_by_type(right_pane_child_t type);

	std::shared_ptr<RightPaneContainer> get_container_at_index(uint32_t index);

	std::shared_ptr<RightPaneChildWidget> getEquipmentChildOnRightPane();

	std::shared_ptr<RightPaneContainer> get_trade_container();
	
	std::shared_ptr<RightPaneContainer> get_battle_container();

	std::shared_ptr<RightPaneContainer> get_vips_container();

	std::shared_ptr<RightPaneContainer> get_skills_container();

	std::vector<std::shared_ptr<RightPaneContainer>> get_containers();

	Point get_containers_pane_placement();

	void open_battle();

	void close_battle();

	void open_vip();

	void close_vip();

	void open_skills();

	void close_skills();

	void click_follow();

	void click_not_follow();

	ClientGeneralInterface();

	void run_check_windialog();
	
	uint32_t get_dialog_address();

	std::shared_ptr<WindowDialog> get_dialog();

	bool close_dialog(int32_t timeout);
};

class ChatChannels{
	static uint32_t get_channel_address(std::string& channel_name);

	static uint32_t get_y();

	static uint32_t get_channel_x(std::string& channel_name);

	static Point get_channel_button_point(std::string& channel_name);

	static bool wait_channel_change(std::string channel_name, uint32_t timeout = 300);
public:
	static bool set_channel(std::string& channel_name);

	static uint32_t get_message_address();

	static std::string get_current_channel_name();

	static void set_text(std::string& text);

	static void send_message(std::string& message, std::string channel = "Default");
	/*int find_x_of_channel(string channel)
	{

	
	}
	int find_y_of_channel()
	{
		int y = ReadInt(ReadInt(ReadInt(ReadInt(ReadInt(base_address_in_gui) + 0x34) + 0x38) + 0x30) + 0x20);
		y = TamanhoTelaY() - y - 40;
		return y;
	}

	void set_normal_channel()
	{
		string name_channel = "Local Chat";
		int local_x = find_x_of_channel(name_channel);
		if (local_x != not_founded)
		{
			ClickIn(local_x, find_y_of_channel());
		}
	}
	void set_npc_channel()
	{
		string name_channel = "NPCs";
		int local_x = find_x_of_channel(name_channel);
		if (local_x != not_founded)
		{

			ClickIn(local_x, find_y_of_channel());
		}
		else
		{

			set_normal_channel();
		}
	}*/
};

