#pragma once
#include "Util.h"
#include "Time.h"
#include "HunterCore.h"
#include "..\HunterManager.h"
#include "ContainerManager.h"

#pragma pack(push,1)
enum loot_queue_remove_reason{
	remove_reason_looted,
	remove_reason_unreachable,
	remove_reason_timeout,
	remove_reason_open_timeout,
	remove_reason_total,
	remove_reason_cant_loot_all,
	remove_reason_not_match, 
	remove_reason_trying,
	remove_reason_warning_message_not_owner,
	remove_reason_action_retval_warning_cannot_use,
	remove_reason_action_retval_fail,
	remove_reason_out_of_range,
	remove_reason_warning_message_upstairs,
	remove_reason_warning_message_downstairs,
	remove_reason_warning_is_not_valious,
	remove_reason_none
};

struct ItemsInfoLooted{
public:	
	std::string name;
	int item_id;
	int count = 0;
	int count_temp = 0;
	int container_id = 0;
	int body_address = 0;
	int buy_price = 0;
	int sell_price = 0;
	bool visible_in_hud = true;

	DEFAULT_GET_SET(int, container_id);
	DEFAULT_GET_SET(int, buy_price);
	DEFAULT_GET_SET(int, sell_price);
	DEFAULT_GET_SET(bool, visible_in_hud);
	DEFAULT_GET_SET(int, body_address);
	DEFAULT_GET_SET(int, count_temp);

	void set_count(uint32_t temp_count){
		count += temp_count;
	}
	void reset_count(){
		count = 0;
		count_temp = 0;
	}
};

class LootCreatureOnQueue{
	neutral_mutex mtx_access;
	int32_t stack_pos;
	CreatureOnBattlePtr creature_died;
	bool looted = true;
	std::string loot_message;
	std::shared_ptr<TimeChronometer> died_time;
	std::map< std::string/*item name*/, uint32_t /*item_count*/ > loot_items;

	TimeChronometer temp_time;
	int32_t timeout_count;
	int32_t fail_count;
	int32_t count_warning_message;
	int32_t fail_move_item_count;
	int32_t count_cannot_use = 0;
	int32_t count_not_owner = 0;
	int32_t count_first_go_downstairs = 0;
	int32_t count_first_go_upstairs = 0;

public:
	loot_queue_remove_reason remove_reason = remove_reason_none;

	bool message_assigned = false;
	
	bool match_some_loot();

	static bool match_some_loot(std::map< std::string/*item name*/, uint32_t /*item_count*/ >& items);

	uint64_t queue_id;
	
	LootCreatureOnQueue();

	std::map<std::string/*item name*/, uint32_t /*item_count*/> get_loot_items();

	int32_t get_stack_pos();
	
	void set_stack_pos(int32_t stack_pos);

	CreatureOnBattlePtr get_creature_died();

	std::string get_loot_message();

	void set_loot_message(std::string message);

	std::shared_ptr<TimeChronometer> get_died_time();

	void set_string(std::string loot_message);

	void update_died_time();

	void process_loot_message();

	static std::map<std::string, uint32_t> get_items_from_message(std::string& message);

	void set_died(CreatureOnBattlePtr died_creature);

	void set_creature(CreatureOnBattlePtr creature);

	void add_timeout_count();

	void add_fail_count();

	void add_fail_move_item_count();

	int32_t get_timeout_count();

	int32_t get_fail_count();

	int32_t get_fail_move_item_count();

	int32_t get_count_first_go_downstairs();

	int32_t get_count_first_go_upstairs();

	void add_count_first_go_downstairs();

	void add_count_first_go_upstairs();

	int32_t get_count_cannot_use();

	void add_count_not_owner();

	int32_t get_count_not_owner();

	int32_t get_count_warning_message();

	void add_count_warning_message();

	void add_count_cannot_use();

	bool get_looted();

	void set_looted(bool in);

};
typedef std::shared_ptr<LootCreatureOnQueue> LootCreatureOnQueuePtr;

class LootControlStateAutoDisable{
public:
	~LootControlStateAutoDisable();
};

struct LastLootControl{
public:
	static uint32_t coord_monster_x;
	static uint32_t coord_monster_y;
	static bool lootcontrol_state;
	static bool add_to_fail;

	static void check_loot_position();
};


class LootListQueue{
	neutral_mutex mtx_access;
	neutral_mutex mtx_access_queue_count;
	TimeChronometer last_message_time;
	std::string last_message;
	uint64_t loot_queue_id = 0;
	TimeChronometer temp_time;
public:
	TimeChronometer last_update_loot_list;

	std::vector<std::shared_ptr<LootCreatureOnQueue>> RemovedLootsCreaturesOnQueue;
	std::vector<std::shared_ptr<LootCreatureOnQueue>> LootsCreaturesOnQueue;
	uint32_t creatures_on_queue = 0;

	LootListQueue();

	std::vector<std::shared_ptr<LootCreatureOnQueue>> getLootsCreaturesOnQueueCopy();

	std::vector<std::shared_ptr<LootCreatureOnQueue>> getRemovedLootsCreaturesOnQueueCopy();

	void clear_loot_list();

	void process_message_string(std::string message_str);

	void process_target_change(CreatureOnBattlePtr creature);

	std::shared_ptr<LootCreatureOnQueue> get_loot_creature_by_creature_id(uint32_t creature_id);

	void clear_not_reachable_loots();

	void clear_older_loots_than(uint32_t milliseconds);

	void find_message_owner();

	void process_creature_died(CreatureOnBattlePtr creature);
	//get ready loots and clear others.
	std::vector<std::shared_ptr<LootCreatureOnQueue>> get_ready_loots();

	std::shared_ptr<LootCreatureOnQueue> get_nearest_ready_loot(bool clear_not_rearchable = false);

	std::shared_ptr<LootCreatureOnQueue> get_latest_died();

	std::shared_ptr<LootCreatureOnQueue> get_latest_died_with_message();

	std::shared_ptr<LootCreatureOnQueue> get_latest_died_without_message();

	void add_creature_to_queue(std::shared_ptr<LootCreatureOnQueue> creature);

	void wake_looter_core();

	bool has_some_loot_ready();

	void remove_loot_from_loot(std::shared_ptr<LootCreatureOnQueue> loot_creature, loot_queue_remove_reason remove_reason);

	static LootListQueue* get();
};

struct loot_open_union{
	loot_open_union() : coord(Coordinate()){}
	bool only_near;
	int32_t stack_pos;
	Coordinate coord;
	int32_t max_timeout;
	action_retval_t result;
};



class LooterCore : public CoreBase{
	bool look_for_stackable_in_full_container = false;
	bool waken_state;
	neutral_mutex mtx_access;
	bool var_need_operate;
	bool var_is_looting = false;
	uint32_t next_delay_eat_from_corpses;
	loot_open_union loot_open;
	TimeChronometer looted_time;
	TimeChronometer last_try_eat_from_corpses;
	std::map<uint32_t, ItemContainerPtr> last_containers_state;
	TimeChronometer time_to_pause;
	int timeout_to_pause = 0;

	void refresh_containers_state();
	void open_loot();

	bool pick_up_item();
	void update_next_delay_eat_from_corpses();
	uint32_t elapsed_eat_from_corpses();
	bool try_eat();

	void close_all_non_looter_containers();

	static uint32_t max_try_loot_count;
	static uint32_t try_loot_count_warning_message;
	std::map <uint32_t, std::shared_ptr<ItemsInfoLooted>> map_items_looted;

	bool m_is_pulsed = false;
	bool check_pulse();
	
public:
	void reset_hud();

	void reset_item_count(int item_id){
		mtx_access.lock();
		map_items_looted[item_id]->count = 0;
		mtx_access.unlock();
	}

	void set_time_to_pause(int timeout){
		time_to_pause.reset();
		timeout_to_pause = timeout;
	}

	std::vector<std::pair<TimeChronometer, std::string>> valious_loot_time;
	void add_valious_loot_time(std::string message);

	bool is_near_valious_time(TimeChronometer* time);

	void clear_valious_loot_time(bool lock = true);

	virtual core_priority get_core_priority();

	void unset_pulse();

	bool is_pulsed();

	void remove_timeout(int timeout);

	void remove_max_distance(int max_distance);

	void remove_min_cap();

	bool wait_loot_complete(uint32_t timeout = 10000);

	bool wait_pulse_out(uint32_t timeout = 10000);

	bool wait_pulse(uint32_t timeout = 10000);

	void pulse();

	void run_check_looted();

	std::map <uint32_t, std::shared_ptr<ItemsInfoLooted>> get_map_items_looted();

	std::shared_ptr<ItemsInfoLooted> getRuleItemsLooted(uint32_t item_id);

	LooterCore();

	static void set_current_sequence_mode(loot_sequence_t in);

	static loot_sequence_t get_current_sequence_mode();

	static void set_current_filter_mode(loot_filter_t in);

	static loot_filter_t get_current_filter_mode();

	static void set_max_try_loot_count(uint32_t in);

	static uint32_t get_max_try_loot_count();

	void update_items_info(uint32_t temp_item_id, uint32_t temp_count);

	std::shared_ptr<LootControlStateAutoDisable> set_lootcontrol(Coordinate coord);

	void disable_loot_control();

	void set_wake_state(bool state);

	void start();

	void refresh_items_looted();

	LootCreatureOnQueuePtr get_loot_info_to_loot();

	void run_async();

	bool need_operate();

	void refresh_items_in_map_looted();

	bool can_execute();

	bool is_looting();

	bool move_item();

	action_retval_t move_items(int32_t timeout = 10000);

	static void init();
	
	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonObject);

	static LooterCore* mLooterCore;
	static LooterCore* get();
};
#pragma pack(pop)