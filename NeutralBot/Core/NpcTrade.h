#pragma once
#include "Util.h"
#include "Time.h"

#pragma pack(push,1)
struct item_trade{
	uint32_t item_id;
	uint32_t idk;
	char name[36];
	uint32_t idk2;
	uint32_t buy_price;
	uint32_t sell_price;
	uint32_t default_count;//pra vender ou comprar quando vem
	uint32_t index;
};
#pragma pack(pop)

class NpcTrade{
	neutral_mutex mtx_access;
	TimeChronometer timer;
	void reset_time(){
		timer.reset();
	}

	uint32_t get_elapsed_time(){
		return timer.elapsed_milliseconds();
	}


	bool is_buy_setted();

	bool scroll_up(int count);

	bool scroll_down(int count);

	bool set_sell();

	bool set_buy();

	bool press_ok();

	uint32_t selected_count();

	uint32_t hidden_item_count();

	uint32_t selected_item_id();

	bool set_selected_index(uint32_t index);

	bool select_item_by_id(uint32_t item_id);

	int selected_index();

	int index_of_item_id(uint32_t item_id);

	uint32_t visible_item_count();

	bool set_count(int count);

	bool do_selected_option(int32_t item_id, int32_t count, bool condition_count = true, bool reorganize = true);

public:

	bool is_open();

	bool resize();

	bool sell_item(int item_id, int qty);

	bool buy_item(int item_id, int qty);

	item_trade get_item_info();

	static NpcTrade* get();
};





