#pragma once
#include "..\HUDManager.h"
#include "Neutral.h"
#include "LuaCore.h"
#include "..\\NavigationManager.h"
#include "..\\SendInfoManager.h"

#include "..\\ConfigPathManager.h"
#include "..\\SellManager.h"
#include "..\\TakeSkinCore.h"
#include "..\\MonsterManager.h"
#include "..\\ControlManager2.h"
#include "..\\GeneralSettings.h"
#include "..\\TakeSkinManager.h"
#include "..\\UtilityCore.h"

#include "AddressManager.h"
#include "Process.h"
#include "WaypointerCore.h"
#include "AlertsCore.h"

#include "..\GifManager.h"
#include "..\LooterManager.h"
#include "..\HunterManager.h"
#include "..\RepoterManager.h"
#include "..\LuaBackgroundManager.h"
#include "..\DepoterManager.h"
#include "..\WaypointManager.h"
#include "..\SpellManager.h"
#include "..\LuaThread.h"
#include "..\\DevelopmentManager.h"
#include "..\\SpecialAreas.h"
#include "..\\HotkeysManager.h"
#include <boost\filesystem.hpp>
#include "HunterCore.h"
#include "AdvancedRepoterCore.h"
#include "Messages.h"
#include <direct.h>
#include "LooterCore.h"
#include "SpellCasterCore.h"
#include "Map.h"
#include "Login.h"
#include <thread>
#include "InfoCore.h"

std::vector<std::function<void()>> NeutralManager::on_close_callbacks;

void NeutralManager::start(){
	AddressManager::get();
}

void NeutralManager::run_cores(){
	core_pointers[NEUTRAL_CORE_ID::CORE_WAYPOINTER] = (CoreBase*)WaypointerCore::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_INPUT] = (CoreBase*)InputManager::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_ALERTS] = (CoreBase*)AlertsCore::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_LUA] = (CoreBase*)LuaThread::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_GENERAL] = (CoreBase*)NeutralManager::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_HUNTER] = (CoreBase*)HunterCore::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_LOOTER] = (CoreBase*)LooterCore::get();
	core_pointers[NEUTRAL_CORE_ID::CORE_SPELLCASTER] = (CoreBase*)SpellCasterCore::get();

	Messages::get();
}

void NeutralManager::delayed_init(){
	init_maps();

	ConfigPathManager::get();
	DepoterManager::get();
	LooterCore::get();
	LuaThread::get();
	LuaBackgroundManager::get();
	LoginManager::get();
	ClientGeneralInterface::get();
	AlertsManager::get();
	TakeSkinCore::get();
	AlertsCore::get();
	InfoCore::get();
	HotkeysManager::get();
	UtilityCore::get();
	MonsterManager::get();
	ItemsManager::get();
	NavigationManager::get();
	SendInfoManager::get();

	GifManager::get()->request_items_load();
	GifManager::get()->wait_load_items();
}

void NeutralManager::init(){	
	for (uint32_t i = 0; i < CORE_TOTAL; i++)
		core_states.push_back(NEUTRAL_CORE_TASK_STATE::DISABLED);

	for (uint32_t i = 0; i < TASK_TOTAL; i++)
		task_states.push_back(NEUTRAL_CORE_TASK_STATE::DISABLED);

	std::thread([&](){ 
		MONITOR_THREAD("BotManager::init")
			Sleep(1000);
		delayed_init();
		run_cores();
	}).detach();

	std::thread([&](){ 
		MONITOR_THREAD("BotManager::auto_save") 
		Sleep(2000); run_auto_save();}).detach();
}

NeutralManager::NeutralManager(){
	init();
}

void NeutralManager::set_core_states(NEUTRAL_CORE_ID key, NEUTRAL_CORE_TASK_STATE value){
	lock_guard _lock(mtx_access);
	core_states[key] = value;
	auto ptr = core_pointers.find(key);
	if (ptr != core_pointers.end())
		ptr->second->set_state(value == NEUTRAL_CORE_TASK_STATE::ENABLED);
}

NEUTRAL_CORE_TASK_STATE NeutralManager::get_core_states(NEUTRAL_CORE_ID key){
	lock_guard _lock(mtx_access);
	return core_states[key];
}

void NeutralManager::set_task_states(NEUTRAL_TASK_ID key, NEUTRAL_CORE_TASK_STATE value){
	lock_guard _lock(mtx_access);
	task_states[key] = value;
	auto ptr = task_pointers.find(key);
	if (ptr != task_pointers.end())
		ptr->second->set_state(value == NEUTRAL_CORE_TASK_STATE::ENABLED);
}

NEUTRAL_CORE_TASK_STATE NeutralManager::get_task_states(NEUTRAL_TASK_ID key){
	lock_guard _lock(mtx_access);
	return task_states[key];
}

bool NeutralManager::is_paused(){
	if (status == STATUS_PAUSED)
		return true;

	return false;
}

void NeutralManager::run_auto_save(){
	TimeChronometer time_to_save;
	TimeChronometer active_time_auto_save;

	while (true){
		Sleep(500);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (get_bot_state())
			continue;

		int timeout_to_save = GeneralManager::get()->get_auto_save_sec();		
		if (time_to_save.elapsed_milliseconds() <= timeout_to_save)
			continue;

		if (!TibiaProcess::get_default()->get_is_logged())
			continue;

		std::string filepath = DEFAULT_DIR_SETTINGS_PATH + "\\TEMPSCRIPT";
		if (!dirExists(filepath))
			_mkdir(filepath.c_str());

		mtx_access.lock();
		save_script(filepath + "\\" + save_temp_name());

		Sleep(500);

		remove_file_temp_old();
		active_time_auto_save.reset();
		time_to_save.reset();
		mtx_access.unlock();
	}
}

std::vector<std::pair<std::string/*filename*/, std::time_t/*file date*/>> NeutralManager::files_in_path(std::string file_path){
	std::vector<std::pair<std::string/*filename*/, std::time_t/*file date*/>> retval;
	if (!dirExists(file_path))
		return retval;

	boost::filesystem::path targetDir(file_path);
	boost::filesystem::directory_iterator end_itr;

	for (boost::filesystem::directory_iterator itr(targetDir); itr != end_itr; ++itr){
		if (!is_regular_file(itr->path()))
			continue;

		std::string current_file = itr->path().filename().string();
		std::time_t current_file_time = boost::filesystem::last_write_time(itr->path());
		retval.push_back(std::make_pair(current_file, current_file_time));
	}
	return retval;
}

void NeutralManager::remove_file_temp_old(){
	std::vector<std::pair<std::string/*filename*/, std::time_t/*file date*/>> files_temp = files_in_path(DEFAULT_DIR_SETTINGS_PATH + "\\TEMPSCRIPT");

	std::string file_to_remove = "";
	std::time_t end_t;
	int count = files_temp.size();
	time(&end_t);

	int diff = count - 100;
	if (diff < 0)
		return;

	for (int i = 0; i < diff; i++){
		int pos = 0;
		double older = 0;
		int pos_erase = -1;

		std::for_each(files_temp.begin(), files_temp.end(), [&](std::pair<std::string, std::time_t> info){
			pos++;

			double time_dif = std::difftime(end_t, info.second);
			if (time_dif < older)
				return;

			older = time_dif;
			pos_erase = pos;
		});

		if (pos_erase == -1)
			return;

		mtx_access.lock();
		std::string filename = files_temp[pos_erase-1].first;
		file_to_remove = DEFAULT_DIR_SETTINGS_PATH + "\\TEMPSCRIPT\\" + filename;

		const boost::filesystem::path path(file_to_remove);
		if (!boost::filesystem::exists(path)){
			mtx_access.unlock();
			continue;
		}

		files_temp.erase(files_temp.begin() + pos_erase-1);
		boost::filesystem::remove_all(path);
		mtx_access.unlock();
	}
}

std::string NeutralManager::save_temp_name(){
	_SYSTEMTIME Time;
	GetSystemTime(&Time);

	std::string date = "TEMP";
	date.append(" day-");
	if (Time.wDay < 10)
		date.append("0");

	date.append(std::to_string((long long)Time.wDay));
	date.append(" hour-");
	if (Time.wHour < 10)
		date.append("0");

	date.append(std::to_string((long long)Time.wHour));
	date.append(" minute-");
	if (Time.wMinute < 10)
		date.append("0");

	date.append(std::to_string((long long)Time.wMinute));
	date.append(".json");

	return date;
}

void NeutralManager::set_pause_callback(std::function<void(bool paused)> callback){
	pause_callback = callback;
}

uint32_t NeutralManager::getTibiaVersion(){
	return TIBIA_CLIENT_VERSION;
}

bool NeutralManager::save_script(std::string file_name){
	Json::Value file;
	file["version"] = 200;

	file["AdvancedRepoterCore"] = AdvancedRepoterManager::get()->parse_class_to_json();
	file["LooterManager"] = LooterManager::get()->parse_class_to_json();
	file["HunterManager"] = HunterManager::get()->parse_class_to_json();
	file["AlertsManager"] = AlertsManager::get()->parse_class_to_json();
	file["RepoterManager"] = RepoterManager::get()->parse_class_to_json();
	file["TakeSkinManager"] = TakeSkinManager::get()->parse_class_to_json();
	file["GeneralSettings"] = GeneralSettings::get()->parse_class_to_json();
	file["DepoterManager"] = DepoterManager::get()->parse_class_to_json();
	file["WaypointManager"] = WaypointManager::get()->parse_class_to_json();
	file["SpellManager"] = SpellManager::get()->parse_class_to_json();
	file["ControlManager"] = ControlManager::get()->parse_class_to_json();
	file["LuaBackgroundManager"] = LuaBackgroundManager::get()->parse_class_to_json();
	file["SpecialAreas"] = SpecialAreaManager::get()->get_root()->ClassToJson();
	file["SellManager"] = SellManager::get()->get()->parse_class_to_json();
	file["UtilityCore"] = UtilityCore::get()->get()->parse_class_to_json();


	file["BotManager"] = parse_class_to_json();

	return saveJsonFile(file_name, file);
}
bool NeutralManager::get_bool_load() {
	lock_guard _lock(mtx_access);
	return bool_load;
}

void NeutralManager::set_bool_load(bool in) {
	lock_guard _lock(mtx_access);
	bool_load = in;
}

void NeutralManager::disable_all(){
	for (int i = 0; i < (int)core_states.size(); i++)
		core_states[i] = NEUTRAL_CORE_TASK_STATE::DISABLED;	
}

bool NeutralManager::load_script(std::string file_name){
	Json::Value file = loadJsonFile(file_name);	
	if (file.isNull())
		return false;

	time_load.reset();

	LuaThread::get()->timer.reset();

	disable_all();

	clear();	
	
	WaypointManager::get()->parse_json_to_class(file["WaypointManager"], file["version"].asInt());	
	LooterManager::get()->parse_json_to_class(file["LooterManager"]);
	HunterManager::get()->parse_json_to_class(file["HunterManager"]);	
	RepoterManager::get()->parse_json_to_class(file["RepoterManager"]);
	SellManager::get()->parse_json_to_class(file["SellManager"]);
	DepoterManager::get()->parse_json_to_class(file["DepoterManager"]);
	SpellManager::get()->parse_json_to_class(file["SpellManager"]);
	LuaBackgroundManager::get()->parse_json_to_class(file["LuaBackgroundManager"]);	
	ControlManager::get()->parse_json_to_class(file["ControlManager"]);
	AdvancedRepoterManager::get()->parse_json_to_class(file["AdvancedRepoterCore"]);
	AlertsManager::get()->parse_json_to_class(file["AlertsManager"]);
	TakeSkinManager::get()->parse_json_to_class(file["TakeSkinManager"]);
	SpecialAreaManager::get()->get_root()->JsonToClass(file["SpecialAreas"]);
	UtilityCore::get()->get()->parse_json_to_class(file["UtilityCore"]);
	
	parse_json_to_class(file["BotManager"]);

	time_load.reset();

	LuaThread::get()->timer.reset();

	return true;
}
bool NeutralManager::dirExists(const std::string& dirName_in){
	DWORD ftyp = GetFileAttributesA(dirName_in.c_str());
	if (ftyp == INVALID_FILE_ATTRIBUTES)
		return false;

	if (ftyp & FILE_ATTRIBUTE_DIRECTORY)
		return true;

	return false;
}

bool NeutralManager::save_default_config(std::string file_name){
	Json::Value file;
	file["version"] = 100;

	file["Default"] = GeneralManager::get()->parse_class_to_json();
	std::string temp_folder = DEFAULT_DIR_SETTINGS_PATH;

	if (!dirExists(file_name))
		_mkdir(temp_folder.c_str());

	return saveJsonFile(file_name, file);
}
bool NeutralManager::load_default_config(std::string file_name){
	Json::Value file = loadJsonFile(file_name);

	if (file.isNull())
		return false;

	GeneralManager::get()->parse_json_to_class(file["Default"]);
	return true;
}

bool NeutralManager::save_items_config(std::string file_name){
	Json::Value file;
	file["version"] = 0100;

	file["Items"] = LooterCore::get()->parse_class_to_json();

	if (!dirExists(file_name))
		_mkdir(DEFAULT_DIR_SETTINGS_PATH.c_str());

	return saveJsonFile(file_name, file);
}
bool NeutralManager::load_items_config(std::string file_name){
	Json::Value file = loadJsonFile(file_name);

	if (file.isNull())
		return false;	

	LooterCore::get()->parse_json_to_class(file["Items"]);
	return true;
}

Json::Value NeutralManager::parse_class_to_json() {
	Json::Value neutralMgr;
	Json::Value& coreStates = neutralMgr["CoreStates"];
	Json::Value& taskStates = neutralMgr["TaskStates"];

	int index = 0;
	for (auto task : task_states){
		taskStates[std::to_string(index)] = (int)task;
		index++;
	}

	for (int index = 0; index < NEUTRAL_CORE_ID::CORE_TOTAL - 1; index++)
		coreStates[std::to_string(index)] = (int)core_states[index];	

	return neutralMgr;
}
void NeutralManager::parse_json_to_class(Json::Value neutralMgr) {
	Json::Value& coreStates = neutralMgr["CoreStates"];
	Json::Value& taskStates = neutralMgr["TaskStates"];

	Json::Value::Members coreMembers = coreStates.getMemberNames();
	Json::Value::Members taskMembers = taskStates.getMemberNames();

	for (std::string coreMember : coreMembers)
		set_core_states((NEUTRAL_CORE_ID)atoi(&coreMember[0]), (NEUTRAL_CORE_TASK_STATE)coreStates[coreMember].asInt());
	
	for (std::string taskMember : taskMembers)
		set_task_states((NEUTRAL_TASK_ID)atoi(&taskMember[0]), (NEUTRAL_CORE_TASK_STATE)taskStates[taskMember].asInt());
	
}

void NeutralManager::add_callback_on_close(std::function<void()> callback){
	on_close_callbacks.push_back(callback);
}

void NeutralManager::on_close(){
	for (auto callback = NeutralManager::on_close_callbacks.begin(); callback != NeutralManager::on_close_callbacks.end(); callback++)
		(*callback)();
	
	gMapCacheControlPtr->save_map_files();
}

void NeutralManager::close(){
	on_close();
	
	save_default_config(DEFAULT_DIR_SETTINGS_PATH + "\\default.json");
	save_items_config(DEFAULT_DIR_SETTINGS_PATH + "\\Items.json");
	
	TerminateProcess(GetCurrentProcess(), 0);
}

void NeutralManager::set_pause_state(bool state){
	if (state)
		status = STATUS_RUNNING;	
	else
		status = STATUS_PAUSED;	
}

bool NeutralManager::get_pause_state(){
	return status == STATUS_RUNNING;
}

bool NeutralManager::get_bot_state(){
	return core_states[CORE_GENERAL] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

void NeutralManager::set_bot_state(bool state){
	core_states[CORE_GENERAL] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}

bool NeutralManager::get_looter_state(){
	return core_states[CORE_LOOTER] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

bool NeutralManager::get_hunter_state(){
	return core_states[CORE_HUNTER] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

bool NeutralManager::get_spellcaster_state(){
	return core_states[CORE_SPELLCASTER] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

bool NeutralManager::get_waypointer_state(){
	return core_states[CORE_WAYPOINTER] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

bool NeutralManager::get_lua_state(){
	return core_states[CORE_LUA] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

bool NeutralManager::get_alerts_state(){
	return core_states[CORE_ALERTS] == NEUTRAL_CORE_TASK_STATE::ENABLED;
}

void NeutralManager::set_looter_state(bool state){
	core_states[CORE_LOOTER] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}

void NeutralManager::set_hunter_state(bool state){
	core_states[CORE_HUNTER] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}

void NeutralManager::set_spellcaster_state(bool state){
	core_states[CORE_SPELLCASTER] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}

void NeutralManager::set_waypointer_state(bool state){
	core_states[CORE_WAYPOINTER] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}

void NeutralManager::set_lua_state(bool state){
	core_states[CORE_LUA] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}

void NeutralManager::set_alerts_state(bool state){
	core_states[CORE_ALERTS] = state ? NEUTRAL_CORE_TASK_STATE::ENABLED : NEUTRAL_CORE_TASK_STATE::DISABLED;
}


void NeutralManager::clear(){
	LooterCore::get()->reset_hud();

	mtx_access.lock();

	LooterManager::get()->clear();
	DepoterManager::get()->clear();
	HunterManager::get()->Clear();
	AlertsManager::get()->clear();
	AdvancedRepoterManager::get()->clear();
	SpellManager::get()->clear_all();
	UtilityCore::get()->clear();
	WaypointManager::get()->clear();
	LuaBackgroundManager::get()->clear();

	mtx_access.unlock();
}

void NeutralManager::set_main_window_handle(HWND Hwindow){
	MainWindowHandle = Hwindow;
}

HWND NeutralManager::get_main_window_handle(){
	return MainWindowHandle;
}

void NeutralManager::show_bot_interface(){
	ShowWindow((HWND)MainWindowHandle, SW_SHOW);
}

void NeutralManager::hide_bot_interface(){
	ShowWindow((HWND)MainWindowHandle, SW_HIDE);
}

void NeutralManager::minimize_bot_interface(){
	ShowWindow((HWND)MainWindowHandle, SW_MINIMIZE);
}

void NeutralManager::restore_bot_interface(){
	ShowWindow((HWND)MainWindowHandle, SW_RESTORE);
}

void NeutralManager::request_close_bot_process(){
	::SendMessage(MainWindowHandle, WM_CLOSE, NULL, NULL);
}

void NeutralManager::kill_bot_process(){
	TerminateProcess(GetCurrentProcess(), 0);
}

void NeutralManager::kill_tibia_client(){
	TibiaProcess::get_default()->kill();
}

NeutralManager* NeutralManager::get(){
	static NeutralManager* mNeutral = nullptr;

	if (mNeutral)
		return mNeutral;

	mNeutral = new NeutralManager;
	return mNeutral;
}