#include "Map.h"
#include "Pathfinder.h"
#include "PathfinderManager.h"
#include "HunterCore.h"
#include <thread>
#include <functional>
#include "Time.h"
#include "boost/filesystem.hpp"
#include "..\\DevelopmentManager.h"
#include "..\\SpecialAreas.h"

#define USE_YELLOW_AS_LEVEL_CHANGE FALSE

std::string get_app_data_dir(){
	CHAR szPath[MAX_PATH];
	ZeroMemory(szPath, MAX_PATH);
	SHGetFolderPathA(NULL, CSIDL_APPDATA, NULL, 0, szPath);
	return szPath;
}

std::string chached_maps_dir = get_app_data_dir() + "\\Smart\\Automap";
std::string maps_dir = get_app_data_dir() + "\\Tibia\\Automap";

std::map<uint32_t, std::vector<std::pair<uint32_t, uint32_t>>> MapMinimap::current_maps_in_memory;
bool MapMinimap::valid_arround_maps[4];
uint32_t MapMinimap::quadrant_coords[4][2];
Coordinate MapMinimap::current_coord;
Rect MapMinimap::current_rect;

MiniMapFile* MapCacheControl::organized_tibia_map[3][3];
ChacheMiniMapFile* MapCacheControl::organized_smart_map[3][3];

unsigned char MapCacheControl::organized_tibia_map_buffer[256][256];
unsigned char MapCacheControl::organized_smart_map_buffer[256][256];
PureMap* MapMinimap::organized_arround_maps_ptr[3][3];

bool MapMinimap::changed_last_center = false;
bool MapMinimap::changed_last_x = false;
bool MapMinimap::changed_last_y = false;
bool MapMinimap::changed_last_z = false;

void ChacheMiniMapFile::save(){
	if (!loaded())
		return;
	if (central_coord.is_null())
		return;
	save(central_coord.z, central_coord.y, central_coord.x);
}

void MiniMapFile::update_hash(){
	data_hash = Util::generateHash(data_1, sizeof(data_1)* 2);
}

bool MiniMapFile::hash_match(){
	return data_hash == Util::generateHash(data_1, sizeof(data_1)* 2);
}

void ChacheMiniMapFile::update_hash(){
	data_hash = Util::generateHash(data_1, sizeof(data_1));
}

bool ChacheMiniMapFile::hash_match(){
	return data_hash == Util::generateHash(data_1, sizeof(data_1));
}

pathfinder_walkability_t MiniMapFile::get_walkability_at(uint32_t x, uint32_t y, map_type_t map_front_or_back){
	tile_front_t type_front = (tile_front_t)data_1[x][y];
	if (tile_unwalkable == type_front ||
		dark_green_unwalkable == type_front ||
		grey_dark_not_walkable == type_front)
		return pathfinder_walkability_t::pathinder_walkability_not_walkable;
	else if (lime_green_walkable == type_front ||
		grey_city_flor_walkable == type_front ||
		black_different_walkable == type_front ||
		blue_weak_snow_walkable == type_front ||
		grown_weak_walkable == type_front){
		return (pathfinder_walkability_t)MapMinimap::switch_walkability_back(data_2[x][y]);
	}
	else if (snow_walkable_back_check == type_front ||
		never_walkable2_back_check == type_front ||
		black_walkable_back_check == type_front ||
		black2_walkable_back_check == type_front ||
		blue_water_walkable_back_check == type_front ||
		blue_water2_walkable_back_check == type_front ||
		black_differet_walkable_check_back == type_front){
		return (pathfinder_walkability_t)MapMinimap::switch_walkability_back(data_2[x][y]);
	}
	else if (yellow == type_front){
		return (pathfinder_walkability_t)MapMinimap::switch_walkability_back(data_2[x][y]);
	}
	else/* if (type_front == 81 ||
		type_front == 139 ||
		type_front == 225 ||
		type_front == 41 ||
		type_front == 123 ||
		type_front == 106 ||
		type_front == 194)*/{

		return (pathfinder_walkability_t)MapMinimap::switch_walkability_back(data_2[x][y]);
	}
	return pathfinder_walkability_t::pathinder_walkability_walkable;
}

bool PureMap::get_walkability_at(uint32_t x, uint32_t y){
	if (x > 255 || y > 255)
		return false;
	if (x < 0 || y < 0)
		return false;
	if (raw_map_back[x][y])
		return true;
	return MapMinimap::switch_walkability_back(raw_map_back[x][y]) != 0;
}

int32_t PureMap::get_real_base_x(){
	return 0;
}

int32_t PureMap::get_real_base_y(){
	return 0;
}

unsigned char MapMinimap::walkability[map_total_t][256][256];
unsigned char MapMinimap::walkability_temp[map_total_t][256][256];

bool MiniMapFile::loaded(){
	return state == file_load_status_t::file_load_success;
}

bool ChacheMiniMapFile::loaded(){
	return state == file_load_status_t::file_load_success;
}

void MapMinimap::flush_special_area_maps(){
	ZeroMemory(&special_area_map[0][0][0], sizeof(special_area_map));
}




void MapMinimap::update_changed_positions(){
	changed_last_x = current_coord.x != last_x;
	changed_last_y = current_coord.y != last_y;
	changed_last_z = current_coord.z != last_z;

	changed_last_center = center_now != last_center;
	if (changed_last_center){
		last_center = center_now;
	}

	if (changed_last_z){
		gMapCacheControlPtr->notify_z_changed();
	}
}

void MapMinimap::update_last_positions(){
	last_x = current_coord.x;
	last_y = current_coord.y;
	last_z = current_coord.z;
}
unsigned char MapMinimap::special_area_map[2][256][256];


//void DrawLine(std::pair<Point, Point>& line, int stroke = 5)
//{
//	int x0 = (int)line.first.x, y0 = (int)line.first.y, x1 = (int)line.second.x, y1 = (int)line.second.y;
//	int dx = abs(x1 - x0), dy = abs(y1 - y0);
//	int sx = (x0 < x1) ? 1 : -1, sy = (y0 < y1) ? 1 : -1;
//	int err = dx - dy;
//
//	int start_x = MapMinimap::current_coord.x - 128;
//	int start_y = MapMinimap::current_coord.y - 128;
//
//	while (true)
//	{
//
//		int offset_x = x0 - start_x;
//		int offset_y = y0 - start_y;
//	
//		if (offset_x < 256 && offset_x > -1 && offset_y < 256 && offset_y > -1)
//			MapMinimap::special_area_map[0][offset_x][offset_y] = 1;
//
//		/*SetColor(y0, x0, color, stroke);
//		if (alpha != 255) SetAlpha(y0, x0, alpha, stroke);
//		*/
//
//		if (x0 == x1 && y0 == y1) return;
//		int e2 = (err << 1);
//		if (e2 > -dy)
//		{
//			err -= dy;
//			x0 += sx;
//		}
//		if (e2 < dx)
//		{
//			err += dx;
//			y0 += sy;
//		}
//	}
//}



void inline_draw_point_radius(uint32_t x, uint32_t y, uint32_t width);

void DrawLineWall(std::pair<Point, Point>& line, uint32_t width){
	if (width <= 0)
		width = 1;
	/*DrawLine(line,5);
	return;*/
	int start_x = MapMinimap::current_coord.x - 128;
	int start_y = MapMinimap::current_coord.y - 128;

	float x1 = (float)line.first.x;
	float y1 = (float)line.first.y;
	float x2 = (float)line.second.x;
	float y2 = (float)line.second.y;

	// Bresenham's line algorithm
	const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
	if (steep){
		std::swap(x1, y1);
		std::swap(x2, y2);
	}

	if (x1 > x2)
	{
		std::swap(x1, x2);
		std::swap(y1, y2);
	}

	const float dx = x2 - x1;
	const float dy = fabs(y2 - y1);

	float error = dx / 2.0f;
	const int ystep = (y1 < y2) ? 1 : -1;
	int y = (int)y1;

	const int maxX = (int)x2;

	for (int x = (int)x1; x < maxX; x++){


		int offset_x = x - start_x;
		int offset_y = y - start_y;
		if (steep){
			offset_x = y - start_x;
			offset_y = x - start_y;
			inline_draw_point_radius(y, x, width);
		}
		else{
			inline_draw_point_radius(x, y, width);
		}
		if (offset_x < 256 && offset_x > -1 && offset_y < 256 && offset_y > -1)
			MapMinimap::special_area_map[0][offset_x][offset_y] = 1;

		/*if (steep)
		{
		SetPixel(y, x, color);
		}
		else
		{
		SetPixel(x, y, color);
		}*/

		error -= dy;
		if (error < 0)
		{
			y += ystep;
			error += dx;
		}
	}
}


void DrawRectWalls(Rect& rect, uint32_t border_width){
	std::vector<std::pair<Point, Point>> rect_lines;

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x(), rect.get_y()),
		Point(rect.get_x() + rect.get_width(), rect.get_y())));

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x() + rect.get_width(), rect.get_y()),
		Point(rect.get_x() + rect.get_width(), rect.get_y() + rect.get_height())));

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x() + rect.get_width(), rect.get_y() + rect.get_height()),
		Point(rect.get_x(), rect.get_y() + rect.get_height())));

	rect_lines.push_back(std::pair<Point, Point>(
		Point(rect.get_x(), rect.get_y() + rect.get_height()),
		Point(rect.get_x(), rect.get_y())));

	for (auto line : rect_lines){
		DrawLineWall(line, border_width);
	}
}


void parse_element_area(SAArea* area_ptr){
	Rect rct = area_ptr->get_rect();
	if (!Util::rect_intersect_rect(MapMinimap::current_rect, rct))
		return;
	DrawRectWalls(rct, area_ptr->width);
}

void DrawPixel(int x_in, int y_in){
	int start_x = MapMinimap::current_coord.x - 128;
	int start_y = MapMinimap::current_coord.y - 128;
	int offset_store_x = x_in - start_x;
	int offset_store_y = y_in - start_y;
	if (offset_store_x < 256 && offset_store_x > -1 && offset_store_y < 256 && offset_store_y > -1){
		MapMinimap::special_area_map[0][offset_store_x][offset_store_y] = 1;
	}
}


void RegisterCircle(Point& point, int radius){
	for (int x = point.x - (radius - 1); x < point.x + (radius - 1); x++){
		for (int y = point.y - (radius - 1); y < point.y + (radius - 1); y++){
			DrawPixel(x, y); // Octant 1
		}
	}
	return;
	/*	int x0 = point.x, y0 = point.y;
	int x = radius;
	int y = 0;
	int decisionOver2 = 1 - x;   // Decision criterion divided by 2 evaluated at x=r, y=0

	int offset_store_x;
	int offset_store_y;
	while (y <= x){

	DrawPixel(x + x0, y + y0); // Octant 1
	DrawPixel(y + x0, x + y0); // Octant 2
	DrawPixel(-x + x0, y + y0); // Octant 4
	DrawPixel(-y + x0, x + y0); // Octant 3
	DrawPixel(-x + x0, -y + y0); // Octant 5
	DrawPixel(-y + x0, -x + y0); // Octant 6
	DrawPixel(x + x0, -y + y0); // Octant 7
	DrawPixel(y + x0, -x + y0); // Octant 8
	y++;
	if (decisionOver2 <= 0)
	{
	decisionOver2 += 2 * y + 1;   // Change in decision criterion for y -> y+1
	}
	else
	{
	x--;
	decisionOver2 += 2 * (y - x) + 1;   // Change for y -> y+1, x -> x-1
	}
	}*/
}
void inline_draw_point_radius(uint32_t x, uint32_t y, uint32_t width){

	RegisterCircle(Point(x, y), width);
}

void parse_element_point_avoidance(SAPointElement* point_ptr){
	if (Util::rect_intersect_rect(MapMinimap::current_rect, point_ptr->get_avoidance_rect())){

		/*	RegisterCircle(point_ptr->get_avoidance_rect().get_center(),
		point_ptr->get_avoidance_rect().height / 2);*/
	}
}

void parse_element_point_walkability(SAPointElement* point_ptr){
	if (Util::rect_intersect_rect(MapMinimap::current_rect, point_ptr->get_walkability_rect())){
	}
}

void parse_element_point(SAPointElement* point_ptr){
	parse_element_point_avoidance(point_ptr);
	parse_element_point_walkability(point_ptr);
}

void parse_element_wall(SAWallElement* wall_ptr){
	auto line = wall_ptr->get_line();

	if (Util::line_intersect_rect(line.first, line.second, MapMinimap::current_rect))
		DrawLineWall(line, wall_ptr->width);
}

bool match_sa_activation_inside_area(SABaseElement* base_element){
	if (!base_element->get_state())
		return false;

	return base_element->is_in_area(MapMinimap::current_coord.x, MapMinimap::current_coord.y, MapMinimap::current_coord.z);
}

bool match_sa_activation_on_target(SABaseElement* base_element){
	return TibiaProcess::get_default()->character_info->target_red() != 0;
}

bool match_sa_activation_on_hunter_creature(SABaseElement* base_element){
	auto creatures = BattleList::get()->get_creatures_on_screen();
	for (auto creature : creatures){
		if (HunterManager::get()->contains_creature_name(creature.second->name)){
			return true;
		}
	}
	return false;
}

bool match_sa_activation_on_creature(SABaseElement* base_element){
	return BattleList::get()->have_creature_on_screen();
}

bool match_sa_activation_on_player(SABaseElement* base_element){
	return BattleList::get()->have_other_player();
}

bool match_sa_activation_on_luring(SABaseElement* base_element){
	return HunterCore::get()->get_lure_state() == lure_state_luring;
}

bool match_sa_activation_on_lured_reached(SABaseElement* base_element){
	lure_state_t temp = HunterCore::get()->get_lure_state();
	return temp == lure_state_reached;
}

bool match_sa_activation_manual_lua(SABaseElement* base_element){
	return base_element->get_state();
}

bool match_sa_activation_on_lure_not_reachable(SABaseElement* base_element){
	return HunterCore::get()->get_lure_state() == lure_state_not_reachable;
}

#define MATCH_AREA(type, base_element)\
	match_##type(base_element);

bool match_area_node(SABaseElement* base_element){
	std::vector<std::shared_ptr<SAActivationTrigger>> myVector = base_element->get_vector_activation_trigger();
	if (!myVector.size())
		return false;

	bool retval = false;
	for (auto condition : myVector){
		switch (condition->get_activation_trigger()){
		case sa_activation_inside_area:{
										   retval = MATCH_AREA(sa_activation_inside_area, base_element)
		}
			break;
		case sa_activation_on_luring:{
										 retval = MATCH_AREA(sa_activation_on_luring, base_element)
		}
			break;
		case sa_activation_on_target:{
										 retval = MATCH_AREA(sa_activation_on_target, base_element)
		}
			break;
		case sa_activation_on_hunter_creature:{
												  retval = MATCH_AREA(sa_activation_on_hunter_creature, base_element)
		}
			break;
		case sa_activation_on_creature:{
										   retval = MATCH_AREA(sa_activation_on_creature, base_element)
		}
			break;
		case sa_activation_on_player:{
										 retval = MATCH_AREA(sa_activation_on_player, base_element)
		}
			break;
		case sa_activation_on_lured_reached:{
												retval = MATCH_AREA(sa_activation_on_lured_reached, base_element)
		}
			break;
		case sa_activation_manual_lua:{
										  retval = MATCH_AREA(sa_activation_manual_lua, base_element)
		}
			break;
		case sa_activation_on_lure_not_reachable:{
													 retval = MATCH_AREA(sa_activation_on_lure_not_reachable, base_element);
		}
			break;
		}

		if (retval)
			return true;
	}

	return false;
}

void refresh_event(){
	while (true){
		Sleep(10);

		SAArea* root = SpecialAreaManager::get()->get_root();
		std::vector<SABaseElement*> myVector = root->get_childrens();
		for (auto child : myVector){

		}
	}
}

void parse_map_special_area_node(SABaseElement* base_element){
	if (!base_element->get_state())
		return;

	if (base_element->get_owner()){
		switch (base_element->get_type()){
		case special_area_element_t::sa_element_area:{
														 if (base_element->get_owner() && (((SAArea*)base_element)->z != MapMinimap::current_coord.z || !match_area_node(base_element)))
															 return;

														 parse_element_area((SAArea*)base_element);
		}
			break;
		case special_area_element_t::sa_element_point:{
														  SAPointElement* point_ptr = (SAPointElement*)base_element;
														  if (base_element->get_owner() && (point_ptr->z != MapMinimap::current_coord.z || !match_area_node(base_element)))
															  return;

														  parse_element_point((SAPointElement*)base_element);
		}
			break;
		case special_area_element_t::sa_element_wall:{
														 SAWallElement* wall_ptr = (SAWallElement*)base_element;
														 if (base_element->get_owner() && (wall_ptr->z != MapMinimap::current_coord.z || !match_area_node(base_element)))
															 return;

														 parse_element_wall((SAWallElement*)base_element);
		}
			break;
		default:
			break;
		}
	}

	auto childs = base_element->get_childrens();
	for (auto child : childs){
		parse_map_special_area_node((SAArea*)child);
	}
}

void MapMinimap::update_map_special_areas(){
	auto map_ptr = walkability[map_front_special_area];

	current_rect = Rect(current_coord.x - 128, current_coord.y - 128,
		(current_coord.x + 128) - (current_coord.x - 128),
		(current_coord.y + 128) - (current_coord.y - 128));

	flush_special_area_maps();

	SAArea* root = SpecialAreaManager::get()->get_root();
	parse_map_special_area_node(root);
}

void MapMinimap::copy_special_areas_to_map_t(map_type_t mtype){
	special_area_map[2][256][256];
	auto& map_t_ref = walkability_temp[map_type_t::map_front_mini_t];
	auto& map_special_ref = special_area_map[0];
	for (int x = 0; x < 256; x++){
		for (int y = 0; y < 256; y++){
			if (map_special_ref[x][y]){
				map_t_ref[x][y] = 0;
			}
		}
	}
}


PureMap* MapMinimap::get_pure_map_at(uint32_t x_id, uint32_t y_id, uint32_t z){
	for (int x = 0; x < 3; x++){
		for (int y = 0; y < 3; y++){
			PureMap* curr_map = organized_arround_maps_ptr[x][y];
			if (!curr_map)
				continue;

			if (curr_map->get_real_base_x() == x_id && curr_map->get_real_base_y() == y_id
				&& curr_map->z == z){
				return curr_map;
			}
		}
	}
	return nullptr;
}

void MapMinimap::refresh(){

	LOG_TIME_INIT

		update_current_pos();

	update_changed_positions();

	update_last_positions();


	LOG_TIME_SET_RESET
		//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
		read_all_arround_maps();
	LOG_TIME_SET_RESET
		refresh_arround_maps();
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	refresh_back_front();
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	LOG_TIME_SET_RESET
		refresh_small_maps();
	LOG_TIME_SET_RESET
		//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)


		update_map_special_areas();

	copy_special_areas_to_map_t(map_front_mini_t);

	memcpy(&walkability[map_type_t::map_back_mini_t][0][0], &walkability_temp[map_type_t::map_back_mini_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);
	memcpy(&walkability[map_type_t::map_back_t][0][0], &walkability_temp[map_type_t::map_back_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);
	memcpy(&walkability[map_type_t::map_front_t][0][0], &walkability_temp[map_type_t::map_front_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);
	memcpy(&walkability[map_type_t::map_front_mini_t][0][0], &walkability_temp[map_type_t::map_front_mini_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);
	memcpy(&walkability[map_type_t::map_front_advanced_t][0][0], &walkability_temp[map_type_t::map_front_advanced_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);
	memcpy(&walkability[map_type_t::map_front_special_area][0][0], &walkability_temp[map_type_t::map_front_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);

	LOG_TIME_SET_RESET
		//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
		//BlockPos::getBlock()->updateMaps();
		//swap_accesible_data();
		//	FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
		LOG_TIME_SET_RESET
		gMapCacheControlPtr->generate_map_type_cache();
	memcpy(&walkability[map_type_t::map_front_cached_editable_t][0][0], &walkability[map_type_t::map_front_clear_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);
	if (Pathfinder::get()->has_blocked_coordinates()){
		std::vector<Coordinate> blocked_coords = Pathfinder::get()->get_blocked_coordinates();
		for (auto coord : blocked_coords){
			int x = coord.x;
			int y = coord.y;
			if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
				walkability[map_type_t::map_front_cached_editable_t][x][y] = UNWALKABLE;
			}
		}
	}
	LOG_TIME_SET_RESET
		//	FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
		update_last_positions();

	//FUNCTION_MEASURE_TIME_END(0)
}

bool MapMinimap::has_some_custom_change_in_walkability(){
	return has_some_custom_change;
}

void MapMinimap::refresh_back_front(){
	//FUNCTION_MEASURE_TIME_START(0)



	bool has_creature_to_attack = HunterCore::get()->exist_creature_to_attack();
	//clear backbuffer
	last_yellow_coords_swap->clear();
	//just will store the result to set at end
	bool temp_is_near_yellow = false;
	//Parte que transforma em mapa pequeno e organiza;
	int pos_x_in_organized = 128 + (current_coord.x - (center_now.x * 256));
	int pos_y_in_organized = 128 + (current_coord.y - (center_now.y * 256));
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	Coordinate map_real_coordinate((center_now.x - 1) * 256 + 128, (center_now.y - 1) * 256 + 128);
	std::vector<std::pair<uint32_t, uint32_t>> yellow_coords;
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	for (int x = 0; x < 256; x++){
		for (int y = 0; y < 256; y++){
			int tvalx = x + pos_x_in_organized;
			int tvaly = y + pos_y_in_organized;

			int x_now = (int)tvalx / 256;
			int y_now = (int)tvaly / 256;

			if (x_now > 2 || x_now < 0 || y_now > 2 || y_now < 0){
				continue;
			}
#pragma region REFRESH_MAP_BACK
			PureMap * now = organized_arround_maps_ptr[x_now][y_now];
			if (!now){
				continue;
			}

			int in_map_x = tvalx % 256;
			int in_map_y = tvaly % 256;
			walkability_temp[map_back_t][x][y] = switch_walkability_back(now->raw_map_back[in_map_x][in_map_y]);
#pragma endregion
#pragma region REFRESH_MAP_FRONT
			tile_front_t type_front = (tile_front_t)now->raw_map_front[in_map_x][in_map_y];

			unsigned char walkable_current;
			if (tile_unwalkable == type_front ||
				dark_green_unwalkable == type_front ||
				grey_dark_not_walkable == type_front)
				walkable_current = UNWALKABLE;
			else if (lime_green_walkable == type_front ||
				grey_city_flor_walkable == type_front ||
				black_different_walkable == type_front ||
				blue_weak_snow_walkable == type_front ||
				grown_weak_walkable == type_front){
				walkable_current = walkability_temp[map_back_t][x][y];// == WALKABLE ? WALKABLE : UNWALKABLE;
			}
			else if (snow_walkable_back_check == type_front ||
				never_walkable2_back_check == type_front ||
				black_walkable_back_check == type_front ||
				black2_walkable_back_check == type_front ||
				blue_water_walkable_back_check == type_front ||
				blue_water2_walkable_back_check == type_front ||
				black_differet_walkable_check_back == type_front){
				walkable_current = walkability_temp[map_back_t][x][y];
			}
			else if (yellow == type_front){
				yellow_coords.push_back(std::pair<uint32_t, uint32_t>(x, y));
#if USE_YELLOW_AS_LEVEL_CHANGE
				Coordinate current = Coordinate((center_now.x - 1) * 256 + x, (center_now.y - 1) * 256 + y);
				Coordinate real_coord_loop_now = map_real_coordinate = Coordinate(x, y);
				Coordinate dist_from = (current - current_coord).get_abs_coord();
				uint32_t dist_min = current.get_axis_min_dist(current_coord);
				if (dist_min < sqm_to_consider_near_yellow_points)
					temp_is_near_yellow = true;

				int dist_max = current.get_axis_max_dist(current_coord);
				if (dist_max < 100)
					last_yellow_coords_swap->push_back(real_coord_loop_now);

				std::map<int, std::shared_ptr<CreatureOnBattle>> creatures = BattleList::get()->get_creatures_at_coordinate(real_coord_loop_now);
				if (dist_from.y <= 5 && dist_from.x <= 7){
					if (has_creature_to_attack && creatures.size())
						walkable_current = UNWALKABLE;
					else
						walkable_current = walkability_temp[map_back_t][x][y];// == WALKABLE ? WALKABLE : UNWALKABLE;
				}

#else
				walkable_current = walkability_temp[map_back_t][x][y];


#endif
			}
			else/* if (type_front == 81 ||
				type_front == 139 ||
				type_front == 225 ||
				type_front == 41 ||
				type_front == 123 ||
				type_front == 106 ||
				type_front == 194)*/{

				walkable_current = walkability_temp[map_back_t][x][y];
			}
			/*else{
			MessageBox(0,&(std::string("Map front tile type undefined ") +std::to_string((long long)type_front))[0] ,0,MB_OK);
			walkable_current = walkability[map_back_t][x][y];// == WALKABLE ? WALKABLE : UNWALKABLE;
			}*/

			walkability_temp[map_front_t][x][y] = walkable_current;
			walkability_temp[map_front_advanced_t][x][y] = (pathfinder_walkability_t)walkable_current;
#pragma endregion
		}
	}
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
#pragma region CHECK_PZ_DEPOT_FURNITURE_WALK_THROUG_PLAYERS
	auto depot_tiles = gMapTilesPtr->get_depot_tiles();
	bool status_pz = TibiaProcess::get_default()->character_info->status_pz();
	Coordinate minimap_start_coord = current_coord - Coordinate(8, 6);

	//avoid asking to ptr every loop
	bool walk_through_players = PathfinderManager::get()->walk_through_players;
	auto creatures = gMapTilesPtr->get_creature_tiles();
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	std::function<bool(uint32_t, uint32_t)> is_yellow = [&](uint32_t x, uint32_t y)->bool{
		auto temp = yellow_coords;
		for (auto coord = yellow_coords.begin(); coord != yellow_coords.end(); coord++){
			if (coord->first == x && coord->second == y)
				return true;
		}
		return false;
	};
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	for (int x = 0; x < 15; x++){
		for (int y = 0; y < 11; y++){
			if (walkability_temp[map_front_t][128 - 7 + x][128 - 5 + y] == UNWALKABLE){
				std::shared_ptr<Tile> tile = gMapTilesPtr->get_tile_at(Coordinate(current_coord.x - 7 + x, current_coord.y - 5 + y, current_coord.z), false);
				if (!tile)
					continue;

				if (tile->is_step() || tile->is_hole() || is_yellow(128 - 7 + x, 128 - 5 + y)){
					last_yellow_coords_swap->push_back(Coordinate(current_coord.x - 7 + x, current_coord.y - 5 + y, current_coord.z));
					continue;
				}

				if ((tile->has_thing_attribute(ThingAttrNotPathable) || tile->override_walkable())
					&& (!tile->has_thing_attribute(ThingAttrNotWalkable) && !tile->override_unwalkable())){
					walkability_temp[map_front_advanced_t][128 - 7 + x][128 - 5 + y] = pathfinder_walkability_t::pathinder_walkability_walkable;
					walkability_temp[map_front_t][128 - 7 + x][128 - 5 + y] = WALKABLE;
				}
				else if (tile->contains_creature()){
					walkability_temp[map_front_advanced_t][128 - 7 + x][128 - 5 + y] = pathfinder_walkability_t::pathinder_walkability_walkable;
				}
			}
		}
	}


	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	auto furniture_tiles = gMapTilesPtr->get_furniture_tiles();
	for (std::shared_ptr<Tile> furniture : furniture_tiles){//furniture
		int x = furniture->coordinate.x;
		int y = furniture->coordinate.y;
		if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
			walkability_temp[map_front_advanced_t][x][y] = pathfinder_walkability_t::pathinder_walkability_walkable;
			walkability_temp[map_front_t][x][y] = UNWALKABLE;
		}
	}
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	auto rune_tiles = gMapTilesPtr->get_rune_item_tiles();
	for (std::shared_ptr<Tile> rune_field : rune_tiles){//Rune Field Tiles
		int x = rune_field->coordinate.x;
		int y = rune_field->coordinate.y;
		if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
			/*walkability_temp[map_front_advanced_t][x][y] = pathfinder_walkability_t::pathinder_walkability_rune_field;
			walkability_temp[map_front_t][x][y] = WALKABLE;
			walkability_temp[map_front_cached_editable_t][x][y] = WALKABLE;
			walkability_temp[map_front_cached_t][x][y] = WALKABLE;*/
		}
	}
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	for (std::shared_ptr<Tile> creature : creatures){
		Coordinate creature_coord = creature->coordinate;
		if (Pathfinder::tibia_coord_to_path_coord(creature_coord.x, creature_coord.y, current_coord)){
			if (creature->contains_player()){
				walkability_temp[map_front_advanced_t][creature_coord.x][creature_coord.y] = pathfinder_walkability_t::pathinder_walkability_player;
				walkability_temp[map_front_t][creature_coord.x][creature_coord.y] = (walk_through_players || status_pz) ? WALKABLE : UNWALKABLE;
				continue;
			}
			else if (creature->contains_furniture()){
				walkability_temp[map_front_advanced_t][creature_coord.x][creature_coord.y] = pathfinder_walkability_t::pathinder_walkability_furniture;
			}
			else if (creature->contains_summon()){
				walkability_temp[map_front_advanced_t][creature_coord.x][creature_coord.y] = pathfinder_walkability_t::pathinder_walkability_summon;
			}
			else if (creature->contains_npc()){
				walkability_temp[map_front_advanced_t][creature_coord.x][creature_coord.y] = pathfinder_walkability_t::pathinder_walkability_npc;
			}
			else{
				walkability_temp[map_front_advanced_t][creature_coord.x][creature_coord.y] = pathfinder_walkability_t::pathinder_walkability_monster;
				walkability_temp[map_front_t][creature_coord.x][creature_coord.y] = UNWALKABLE;
			}
		}
	}
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	//TODO
	/*
	trocar near tiles por tile que fica perto do depot
	*/
	for (auto depot_tile = depot_tiles.begin(); depot_tile != depot_tiles.end(); depot_tile++){
		int x = depot_tile->get()->coordinate.x;
		int y = depot_tile->get()->coordinate.y;
		if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
			auto creatures_near_this = BattleList::get()->get_creatures_near_coordinate(depot_tile->get()->coordinate);
			if (creatures_near_this.size()){
				for (auto creature = creatures_near_this.begin(); creature != creatures_near_this.end(); creature++){
					Coordinate creature_coord = creature->get()->get_coordinate_monster();
					if (Pathfinder::tibia_coord_to_path_coord(creature_coord.x, creature_coord.y, current_coord)){
						walkability_temp[map_front_advanced_t][creature_coord.x][creature_coord.y] = pathfinder_walkability_t::pathinder_walkability_near_depot_unwalkable;
						walkability_temp[map_front_t][creature_coord.x][creature_coord.y] = UNWALKABLE;
					}
				}
			}
		}
	}

	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	auto custom_tiles = gMapTilesPtr->get_custom_item_tiles();
	for (auto custom_tile : custom_tiles){
		int x = custom_tile.first->coordinate.x;
		int y = custom_tile.first->coordinate.y;

		if (!Pathfinder::tibia_coord_to_path_coord(x, y, current_coord))
			continue;

		if (custom_tile.second->get_property_type() == property_t::property_block){
			walkability_temp[map_front_mini_t][x][y] = UNWALKABLE;
			walkability_temp[map_front_t][x][y] = UNWALKABLE;
			walkability_temp[map_front_cached_t][x][y] = UNWALKABLE;
			walkability_temp[map_front_cached_editable_t][x][y] = WALKABLE;
		}
		else{
			int cost = custom_tile.second->get_walk_dificult();

			if (cost > 1){
				walkability_temp[map_front_mini_t][x][y] = WALKABLE + custom_tile.second->get_walk_dificult();
				walkability_temp[map_front_t][x][y] = WALKABLE + custom_tile.second->get_walk_dificult();
				walkability_temp[map_front_cached_t][x][y] = WALKABLE + custom_tile.second->get_walk_dificult();
				walkability_temp[map_front_cached_editable_t][x][y] = WALKABLE + custom_tile.second->get_walk_dificult();;
			}
		}
	}

	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	/*Coordinate going_step = TibiaProcess::get_default()->character_info->get_going_step();
	if (!going_step.is_null()){
	Coordinate going_axis;
	if (Pathfinder::tibia_coord_to_path_coord(going_step.x, going_step.y, going_axis)){
	walkability_temp[map_front_advanced_t][going_axis.x][going_axis.y] = pathfinder_walkability_t::pathinder_walkability_walkable;
	walkability_temp[map_back_t][going_axis.x][going_axis.y] = WALKABLE;
	walkability_temp[map_front_t][going_axis.x][going_axis.y] = WALKABLE;
	}
	}*/

#pragma endregion

#pragma region REFRESH_PATHFINDER_TILES
	bool to_lure_mage = HunterCore::get()->to_lure_mage;
	bool info_lure_status = HunterCore::get()->info_lure_status;
	bool hunter_needs_operate = HunterCore::get()->need_operate();
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	PathfinderManager::get()->lock();
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	auto end = PathfinderManager::get()->mapwalls_coordinates.end();
	for (auto it = PathfinderManager::get()->mapwalls_coordinates.begin(); it != end; it++){
		if (it->coord.z != current_coord.z)
			continue;
		if ((current_coord).get_axis_max_dist(it->coord) > 255)
			continue;
		int x = it->coord.x;
		int y = it->coord.y;
		if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
			switch (it->type){
			case wall_lure_t:{
								 if (!to_lure_mage && info_lure_status){
									 walkability_temp[map_front_t][x][y] = UNWALKABLE;
								 }
			}
				break;
			case wall_battle_t:{
								   if (hunter_needs_operate){
									   walkability_temp[map_front_t][x][y] = UNWALKABLE;
								   }
			}
				break;
			case wall_solid_t:{
								  walkability_temp[map_front_t][x][y] = UNWALKABLE;
			}
				break;
			}
		}
	}
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	PathfinderManager::get()->unlock();
#pragma endregion

	if (has_creature_to_attack){
		std::vector<TilePtr> teleport_tiles = gMapTilesPtr->get_teleport_tiles();
		std::vector<TilePtr> hole_or_step_tiles = gMapTilesPtr->get_hole_or_step_tiles();

		for (TilePtr teleport : teleport_tiles){
			int x = teleport->coordinate.x;
			int y = teleport->coordinate.y;
			if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
				walkability_temp[map_front_t][x][y] = UNWALKABLE;
			}
		}

		for (TilePtr hole_or_step : hole_or_step_tiles){
			int x = hole_or_step->coordinate.x;
			int y = hole_or_step->coordinate.y;
			if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
				walkability_temp[map_front_t][x][y] = UNWALKABLE;
			}
		}
	}

	if (Pathfinder::get()->has_blocked_coordinates()){
		std::vector<Coordinate> blocked_coords = Pathfinder::get()->get_blocked_coordinates();
		for (auto coord : blocked_coords){
			int x = coord.x;
			int y = coord.y;
			if (Pathfinder::tibia_coord_to_path_coord(x, y, current_coord)){
				walkability_temp[map_front_t][x][y] = UNWALKABLE;
			}
		}
	}


	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	swap_accesible_data();
	update_last_positions();
}

void MapMinimap::refresh_small_maps(){
	memcpy(&walkability_temp[map_back_mini_t][0][0], &walkability_temp[map_back_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);

	memcpy(&walkability_temp[map_front_mini_t][0][0], &walkability_temp[map_front_t][0][0], WALKABILITY_MAP_X * WALKABILITY_MAP_Y);


	/*
	isso aqui poderia ser mais bonito rsrs
	*/

	for (int x = 0; x < 15; x++){
		walkability_temp[map_back_mini_t][128 - 7 + x][128 - 8] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 7 + x][128 - 7] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 7 + x][128 - 6] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 7 + x][128 + 6] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 7 + x][128 + 7] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 7 + x][128 + 8] = UNWALKABLE;
	}
	for (int y = 0; y < 17; y++){
		walkability_temp[map_back_mini_t][128 - 10][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 9][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 - 8][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 + 8][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 + 9][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_back_mini_t][128 + 10][128 - 8 + y] = UNWALKABLE;
	}

	for (int x = 0; x < 15; x++){
		walkability_temp[map_front_mini_t][128 - 7 + x][128 - 8] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 7 + x][128 - 7] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 7 + x][128 - 6] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 7 + x][128 + 6] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 7 + x][128 + 7] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 7 + x][128 + 8] = UNWALKABLE;
	}
	for (int y = 0; y < 17; y++){
		walkability_temp[map_front_mini_t][128 - 10][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 9][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 - 8][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 + 8][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 + 9][128 - 8 + y] = UNWALKABLE;
		walkability_temp[map_front_mini_t][128 + 10][128 - 8 + y] = UNWALKABLE;
	}


	//TODO dimiuir pathfinder cost em alguns lugares
	/*Coordinate cur_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	for (int x = 0; x<21; x++){
	for (int y = 0; y < 11; y++){
	walkability[map_front_t][128 - 10 + x][128 - 5 + y] = Pathfinder::get()->is_reachable(cur_coord +
	Coordinate(-10 + x, -5 + y), map_front_mini_t);
	}
	}*/


}

std::vector<std::pair<TilePtr, std::shared_ptr<ConfigPathProperty>>> MapTiles::get_custom_item_tiles(){
	lock_guard _lock(mtx_access);

	std::vector<std::pair<TilePtr, std::shared_ptr<ConfigPathProperty>>> retval;
	for (auto tile : current_tiles){
		std::shared_ptr<ConfigPathProperty> configPathProperty = tile->have_custom_item_cost();
		if (!configPathProperty)
			continue;

		retval.push_back(std::make_pair(tile, configPathProperty));
	}

	return retval;
}

void MapMinimap::refresh_arround_maps(){
	current_maps_in_memory.clear();
	//clear pointers orders
	ZeroMemory(&organized_arround_maps_ptr, sizeof(organized_arround_maps_ptr));
	Coordinate center_organized(1, 1);
	for (int i = 0; i < MINIMAP_COUNT; i++){
		if (current_coord.z == arround_maps[i].z){
			Coordinate _final = (Coordinate(arround_maps[i].x, arround_maps[i].y) - center_now);
			_final = _final + center_organized;
			if (_final.x >= 0 && _final.x <= 2)
			if (_final.y >= 0 && _final.y <= 2)
				organized_arround_maps_ptr[_final.x][_final.y] = &arround_maps[i];
		}
		current_maps_in_memory[arround_maps[i].z].push_back(std::pair<uint32_t, uint32_t>(arround_maps[i].x, arround_maps[i].y));
	}


	auto current_z = current_maps_in_memory.find(current_coord.z);
	ZeroMemory(&valid_arround_maps[0], sizeof(valid_arround_maps));
	if (current_z == current_maps_in_memory.end())
		return;

	std::vector<std::pair<uint32_t, uint32_t>>& maps_z = current_z->second;
	if (current_coord.x && current_coord.y){
		uint32_t current_x_center = current_coord.x / 256;
		uint32_t current_y_center = current_coord.x / 256;
		int x_quadrant = (int)current_coord.x - (int)current_x_center * 256;
		int y_quadrant = (int)current_coord.y - (int)current_y_center * 256;
		x_quadrant = (x_quadrant != 0 ? x_quadrant / 128 : 0) ? 1 : -1;
		y_quadrant = (y_quadrant != 0 ? y_quadrant / 128 : 0) ? 1 : -1;

		int index = 0;
		for (int x = 0; x < 2; x++){
			for (int y = 0; y < 2; y++){
				auto _found = std::find_if(maps_z.begin(), maps_z.end(), [&](std::pair<uint32_t, uint32_t>& x_y){
					return x_y.first == (x * x_quadrant) && x_y.second == (y * y_quadrant);
				});
				if (_found != maps_z.end()){
					valid_arround_maps[index] = true;
				}
				quadrant_coords[index][0] = ((int)x * (int)x_quadrant) + (int)current_x_center;
				quadrant_coords[index][1] = ((int)y * (int)y_quadrant) + (int)current_y_center;
			}
			index++;
		}
	}
}

bool MapMinimap::get_walkability(Coordinate coord, map_type_t map_t){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (get_walkability_map(coord, map_t, current_coord))
		return true;
	return false;
}

pathfinder_walkability_t MapMinimap::get_walkability_value_at(Coordinate coord, map_type_t map_t){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	return get_walkability_value_at_map(coord, map_t, current_coord);
}

bool MapMinimap::get_walkability_map(Coordinate coord, map_type_t map_t, Coordinate& current_coord){

	if (!Pathfinder::tibia_coord_to_path_coord(coord.x, coord.y, current_coord))
		return false;

	if (walkability[map_t][coord.x][coord.y] == WALKABLE)
		return true;

	return false;
}

pathfinder_walkability_t MapMinimap::get_walkability_value_at_map(Coordinate coord, map_type_t map_t, Coordinate& current_coord){

	if (!Pathfinder::tibia_coord_to_path_coord(coord.x, coord.y, current_coord))
		return pathfinder_walkability_t::pathinder_walkability_walkable;

	return (pathfinder_walkability_t)walkability[map_t][coord.x][coord.y];
}

void MapMinimap::swap_accesible_data(){
	lock();
	if (last_yellow_coords_swap->size() <= 0){
		unlock();
		return;
	}
	void* ptr_holder = last_yellow_coords;
	last_yellow_coords = last_yellow_coords_swap;
	last_yellow_coords_swap = (std::vector<Coordinate>*)ptr_holder;
	unlock();
}

void MapMinimap::lock(){
	mtx_access.lock();
}

void MapMinimap::unlock(){
	mtx_access.unlock();
}

void MapMinimap::read_all_arround_maps(){
	TibiaProcess::get_default()->read_memory_block(
		AddressManager::get()->getAddress(ADDRESS_FIRST_MINIMAP) - 140
		/*TODO mudar o endereco no address.lua*/, &arround_maps, sizeof(arround_maps));
}
//FF 00 00 00 FF 00 00 00 7C 00 00 00 7C 00 00 00 0F 00 00 00 06 00 00 00
void MapMinimap::update_current_pos(){
	current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	center_now = Coordinate((int)current_coord.x / 256, (int)current_coord.y / 256, current_coord.z);
}

char MapMinimap::switch_walkability_back(unsigned char entrace){
	//resumao
	if (not_walkable == entrace)
		return UNWALKABLE;
	return WALKABLE;

	//detalhado
	switch (entrace){
	case not_walkable:
		return UNWALKABLE;
	case back_uptiles:
		return WALKABLE;
	case not_explored:
		return WALKABLE;
	case walkable_flor:
		return WALKABLE;
	}
	return WALKABLE;
}

void MapMinimap::add_block_id(uint32_t id){
	block_ids.push_back(id);
}

void MapMinimap::add_release_id(uint32_t id){
	release_ids.push_back(id);
}

void MapMinimap::remove_block_id(uint32_t id){
	auto it = std::find(block_ids.begin(), block_ids.end(), id);

	while (it != block_ids.end()){
		block_ids.erase(it);
		it = std::find(block_ids.begin(), block_ids.end(), id);
	}
}

void MapMinimap::remove_release_id(uint32_t id){
	auto it = std::find(release_ids.begin(), release_ids.end(), id);

	while (it != release_ids.end()){
		release_ids.erase(it);
		it = std::find(release_ids.begin(), release_ids.end(), id);
	}
}

const int map_title_buff_size = 1569152;
unsigned char map_title_buff[map_title_buff_size];
const int map_title_pointer_buff_size = 0x1F7C;
unsigned char map_title_pointer_buff[map_title_pointer_buff_size];
const int OffSet_title_title_map = 0x170;

void MapTiles::refresh(){
	//TODO melhorar o codigo
	auto kakatua = TibiaProcess::get_default()->get_name_char();
	uint32_t address_offset = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_POINTER_OFFSET_BUFFER_MAP));
	TibiaProcess::get_default()->read_memory_block(address_offset, map_title_pointer_buff, 8060, false);

	//read map
	uint32_t address_buffer = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_POINTER_BUFFER_MAP));

	int index = 0;
	int amount_each_time = 50 * OffSet_title_title_map;
	int last_num = UINT32_MAX;
	bool finished = false;
	while (last_num == UINT32_MAX){
		if (finished)
			break;

		int readen_bytes = TibiaProcess::get_default()->read_memory_block(address_buffer + index, (unsigned char *)((int)map_title_buff + index),
			amount_each_time, false);
		if (readen_bytes != 0){
			index += amount_each_time;
			last_num = *(int*)((int)map_title_buff + index - 4);
		}
		else{
			while (last_num == UINT32_MAX){
				if (TibiaProcess::get_default()->read_memory_block(address_buffer + index, (unsigned char *)((int)map_title_buff + index), OffSet_title_title_map) == 0)
					finished = true;

				if (finished)
					break;
				index += OffSet_title_title_map;
				last_num = *(int*)((int)map_title_buff + index - 4);
			}
		}
	}

	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	std::vector<TilePtr> current_tiles_temp;
	int consider_near_x = 8;
	int consider_near_y = 6;
	for (int x_offset = -consider_near_x; x_offset < consider_near_x; x_offset++){
		for (int y_offset = -consider_near_y; y_offset < consider_near_y; y_offset++){
			TilePtr tile = get_tile_from_raw(
				current_coordinate.x + x_offset,
				current_coordinate.y + y_offset,
				current_coordinate.z,
				false);
			if (tile)
				current_tiles_temp.push_back(tile);
		}
	}

	lock_guard _lock(mtx_access);
	current_tiles.assign(current_tiles_temp.begin(), current_tiles_temp.end());
}

TilePtr MapTiles::get_nearest_tile(std::vector<TilePtr>& tiles_vector, bool check_reachable, map_type_t map_type){
	if (!tiles_vector.size())
		return 0;

	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	TilePtr nearest_tile = nullptr;
	for (auto tile : tiles_vector){

		if (!nearest_tile || (self_coord.get_total_distance(tile->coordinate) <
			self_coord.get_total_distance(nearest_tile->coordinate))){
			if (check_reachable){
				if (self_coord.get_axis_max_dist(tile->coordinate) <= 1){
					nearest_tile = tile;
				}
				else if (PathFinderCore::get()->is_reachable(tile->coordinate, map_type))
					nearest_tile = tile;
			}
			else{
				nearest_tile = tile;
			}
		}
	}
	return nearest_tile;
}

Coordinate MapTiles::find_coordinate_with_top_id(uint32_t item_id, map_type_t map_type, bool ignore_creature){
	std::vector<TilePtr> tiles = get_tiles_with_top_id(item_id, ignore_creature);
	if (!tiles.size())
		return Coordinate();

	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	TilePtr nearest_tile = 0;
	for (auto tile : tiles){
		if (nearest_tile){
			//if iterator tile is near than last nearest_tile
			if ((self_coord.get_axis_max_dist(tile->coordinate) + self_coord.get_axis_min_dist(tile->coordinate)) <
				(self_coord.get_axis_max_dist(nearest_tile->coordinate) + self_coord.get_axis_min_dist(nearest_tile->coordinate))){
				if (PathFinderCore::get()->is_reachable(tile->coordinate, map_type, true) || (tile->coordinate == self_coord))
					nearest_tile = tile;
			}
		}
		else//check reachable
		if (PathFinderCore::get()->is_reachable(tile->coordinate, map_type, true) || (tile->coordinate == self_coord))
			nearest_tile = tile;
	}

	if (nearest_tile)
		return nearest_tile->coordinate;

	return Coordinate();
}

Coordinate MapTiles::find_coordinate_contains_id(uint32_t item_id, map_type_t map_type){
	std::vector<TilePtr> tiles = get_tiles_with_id(item_id);
	if (!tiles.size())
		return Coordinate();
	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	TilePtr nearest_tile = 0;
	for (auto tile : tiles){
		if (nearest_tile){
			//if iterator tile is near than last nearest_tile
			if ((self_coord.get_axis_max_dist(tile->coordinate) + self_coord.get_axis_min_dist(tile->coordinate)) <
				(self_coord.get_axis_max_dist(nearest_tile->coordinate) + self_coord.get_axis_min_dist(nearest_tile->coordinate))){
				if (PathFinderCore::get()->is_reachable(tile->coordinate, map_type))
					nearest_tile = tile;
			}
		}
		else//check reachable
		if (PathFinderCore::get()->is_reachable(tile->coordinate, map_type))
			nearest_tile = tile;

	}

	if (nearest_tile)
		return nearest_tile->coordinate;

	return Coordinate();
}

bool MapTiles::coordinate_is_traped(uint32_t x, uint32_t y, map_type_t map_type, uint32_t free_slots){
	uint32_t free_count = 0;
	for (int x_offset = -1; x_offset < 2; x_offset++)
	for (int y_offset = -1; y_offset < 2; y_offset++)
	if (MapMinimap::walkability[map_type][128 + x_offset][128 + y_offset]){
		free_count++;
		if (free_count >= free_slots)
			return false;
	}
	return true;
}

bool MapTiles::coordinate_is_traped(Coordinate coord, map_type_t map_type, uint32_t free_slot){
	return false;
	return coordinate_is_traped(coord.x, coord.y, map_type, free_slot);
}

TilePtr MapTiles::get_tile_from_raw(uint32_t x, uint32_t y, int8_t z, bool fix_level_offset){
	tile_raw* raw_address = get_raw_tile_at(x, y, z, fix_level_offset);
	if (!raw_address)
		return nullptr;
	return TilePtr(new Tile((tile_raw*)raw_address, Coordinate(x, y, z)));
}

uint32_t MapTiles::get_max_visible_level(){
	return TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_BASE_GUI), std::vector < uint32_t > {0x28, 0x5bc0}, true);
}

uint32_t MapTiles::get_max_visible_level_up(){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (current_coord.z > 7){
		return 0;
	}
	return get_max_visible_level() + (current_coord.z - 7);
}

uint32_t MapTiles::get_max_visible_level_down(){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (current_coord.z > 7){
		return get_max_visible_level();
	}
	return 7 - current_coord.z;
}

tile_raw* MapTiles::get_raw_tile_at(uint32_t x, uint32_t y, int8_t z, bool fix_level_offset){
	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (fix_level_offset){
		int32_t dif = ((int32_t)z - (int32_t)self_coordinate.z);
		if (dif){
			self_coordinate.x -= dif;
			self_coordinate.y -= dif;
		}
	}


	int DifX = x - self_coordinate.x;
	int DifY = y - self_coordinate.y;

	int ValorParaOffset = 117 + (DifY * 18) + DifX - 1;
	int temp;
	char* ptr = (char*)&map_title_pointer_buff;
	if (self_coordinate.z == 7 || self_coordinate.z < 7){
		int to_sek = (((int)&map_title_pointer_buff) + 4 * ValorParaOffset + ((7 - (z)) * 1008));
		if (to_sek < ((int)&map_title_pointer_buff) || to_sek > map_title_pointer_buff_size + ((int)&map_title_pointer_buff))
			return nullptr;
		temp = *(int*)to_sek;
	}
	else{
		int of = 4 * ValorParaOffset + 2016;
		int to_sek = (((int)&map_title_pointer_buff) + of);
		if (to_sek < ((int)&map_title_pointer_buff) || to_sek > map_title_pointer_buff_size + ((int)&map_title_pointer_buff))
			return nullptr;
		temp = *(int*)to_sek;
	}

	int raw_address = (((int)&map_title_buff) + OffSet_title_title_map * temp);
	if (raw_address < ((int)&map_title_buff) || raw_address > map_title_buff_size + ((int)&map_title_buff))
		return nullptr;
	return (tile_raw*)raw_address;
}

bool MapTiles::is_traped_on_screen(map_type_t map_type){
	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

	for (int x = 0; x < 15; x++){
		if (MapMinimap::walkability[map_type][128 - 7 + x][128 - 5] != 0){
			if (PathFinderCore::get()->is_reachable(Coordinate(self_coordinate.x - 7 + x, self_coordinate.y - 5, self_coordinate.z), map_type))
				return false;
		}
		if (MapMinimap::walkability[map_type][128 - 7 + x][128 + 5] != 0){
			if (PathFinderCore::get()->is_reachable(Coordinate(self_coordinate.x - 7 + x, self_coordinate.y + 5, self_coordinate.z), map_type))
				return false;
		}
	}

	for (int y = 0; y < 11; y++){
		if (MapMinimap::walkability[map_type][128 - 7][128 - 5 + y] != 0){
			if (PathFinderCore::get()->is_reachable(Coordinate(self_coordinate.x - 7, self_coordinate.y - 5, self_coordinate.z), map_type))
				return false;
		}
		if (MapMinimap::walkability[map_type][128 + 7][128 - 5 + y] != 0){
			if (PathFinderCore::get()->is_reachable(Coordinate(self_coordinate.x + 7, self_coordinate.y - 5 + y, self_coordinate.z), map_type))
				return false;
		}
	}
	return true;
}

std::vector<TilePtr> MapTiles::get_tiles_near_coordinate(Coordinate coordinate, bool check_reachable, map_type_t map_type, uint32_t consider_near){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (check_reachable){
		for (auto tile : current_tiles){
			if (tile->coordinate.get_axis_max_dist(current_coordinate) > consider_near)
				continue;
			if (coordinate != tile->coordinate && !PathFinderCore::get()->is_reachable(tile->coordinate, map_type))
				continue;
			retval.push_back(tile);
		}
	}
	else{
		for (auto tile : current_tiles)
		if (coordinate == tile->coordinate || tile->coordinate.get_axis_max_dist(current_coordinate) <= consider_near)
			retval.push_back(tile);
	}
	return retval;
}

TilePtr MapTiles::get_tile_at(Coordinate coord, bool fix_z){
	if (MapMinimap::current_coord.z != coord.z){
		return get_tile_from_raw(coord.x, coord.y, coord.z, fix_z);
	}
	else{
		lock_guard _lock(mtx_access);
		for (auto tile : current_tiles)
		if (tile->coordinate == coord)
			return tile;
	}
	return 0;
}

std::vector<TilePtr> MapTiles::get_tiles_with_id(uint32_t id){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles){
		if (tile->contains_id(id))
			retval.push_back(tile);
	}
	return retval;
}

std::vector<TilePtr> MapTiles::get_tiles_with_top_id(uint32_t id, bool ignore_player){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->get_top_item_id(ignore_player) == id)
		retval.push_back(tile);

	return retval;
}

bool MapTiles::contains_furniture_at(Coordinate coord){
	TilePtr tile = get_tile_at(coord, true);
	if (!tile)
		return false;

	return tile->contains_furniture();
}

std::vector<TilePtr> MapTiles::get_tiles_with_id_near_to(uint32_t item_id, Coordinate coord, uint32_t max_dist) {
	std::vector<TilePtr> retval;
	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	coord.update_if_null_member(self_coord);

	auto tiles = get_tiles_with_id(item_id);
	for (auto it = tiles.begin(); it != tiles.end(); it++)
	if (it->get()->coordinate.get_axis_min_dist(coord) <= max_dist)
		retval.push_back(*it);

	return retval;
}

std::vector<TilePtr> MapTiles::get_depot_tiles() {
	std::vector<uint32_t> depot_tiles_ids = ItemsManager::get()->get_depot_ids();
	std::vector<TilePtr> retval;

	for (auto depot_id = depot_tiles_ids.begin(); depot_id != depot_tiles_ids.end(); depot_id++){
		auto now = get_tiles_with_id(*depot_id);
		for (auto tile : now)
			retval.push_back(tile);
	}

	return retval;
}

std::vector<TilePtr> MapTiles::get_depot_chest() {
	std::vector<uint32_t> depot_tiles_ids = ItemsManager::get()->get_depot_ids();
	std::vector<TilePtr> retval;

	for (auto depot_id = depot_tiles_ids.begin(); depot_id != depot_tiles_ids.end(); depot_id++){
		auto now = get_tiles_with_id(*depot_id);
		for (auto tile : now)
			retval.push_back(tile);
	}

	return retval;
}

TilePtr MapTiles::get_nearest_depot_tile(bool check_reachable, map_type_t map_type){
	return get_nearest_tile(get_depot_tiles(), check_reachable, map_type);
}

std::vector<TilePtr> MapTiles::get_furniture_tiles(){
	std::vector<TilePtr> retval;
	return retval;
}

std::vector<TilePtr> MapTiles::get_rune_item_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->have_element_field())
		retval.push_back(tile);

	return retval;
}

std::vector<TilePtr> MapTiles::get_creature_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->contains_creature())
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_player_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->contains_player())
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_monster_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->contains_monster())
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_summon_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->contains_summon())
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_not_walkable_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->override_unwalkable() || tile->has_thing_attribute(ThingAttrNotWalkable))
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_not_pathable_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->has_thing_attribute(ThingAttrNotPathable))
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_hole_or_step_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->contains_hole_or_step())
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_teleport_tiles(){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles)
	if (tile->is_teleport())
		retval.push_back(tile);
	return retval;
}

std::vector<TilePtr> MapTiles::get_tiles_with_item_attributes(std::vector<attr_item_t> attributes){
	lock_guard _lock(mtx_access);
	std::vector<TilePtr> retval;
	for (auto tile : current_tiles){
		for (auto attr = attributes.begin(); attr != attributes.end(); attr++){
			if (tile->contains_item_attr(*attr)){
				retval.push_back(tile);
				break;
			}
		}
	}
	return retval;
}

bool MapTiles::have_id_near(int32_t x, int32_t y, uint32_t item_id){
	return (get_tiles_with_id_near_to(item_id, Coordinate(x, y))).size() > 0;
}

TilePtr MapTiles::get_tile_nearest_coordinate(Coordinate coord, bool check_reachable, map_type_t map_type, uint32_t consider_near){
	auto tiles = get_tiles_near_coordinate(coord, check_reachable, map_type, consider_near);
	if (!tiles.size())
		return 0;
	return get_nearest_tile(tiles);
}

bool MapTiles::is_cordinate_onscreen(Coordinate coord){
	Coordinate coord_self = TibiaProcess::get_default()->character_info->get_self_coordinate();
	coord = (coord - coord_self).get_abs_coord();

	if (coord.x > VISIBLE_TILE_X / 2)
		return false;
	else if (coord.y > VISIBLE_TILE_Y / 2)
		return false;
	return true;
}

bool MapTiles::can_click_coord(Coordinate& toPos){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (current_coord.z > 7)
		return true;
	else{
		uint32_t levels_visibles = gMapTilesPtr->get_max_visible_level_up();
		for (uint32_t i = 0; i < levels_visibles; i++){
			TilePtr tile = gMapTilesPtr->get_tile_at(Coordinate(toPos.x, toPos.y, current_coord.z - (i + 1)), false);
			if (!tile)
				continue;

			if (tile->get_thing_count() > 0)
				return false;
		}
		return true;
	}
	return true;
}


bool MapTiles::check_sight_line(Coordinate& fromPos, Coordinate& toPos, bool check_z){
	if (fromPos == toPos)
		return true;

	Coordinate start(fromPos.z > toPos.z ? toPos : fromPos);
	Coordinate destination(fromPos.z > toPos.z ? fromPos : toPos);

	const int8_t mx = start.x < destination.x ? 1 : start.x == destination.x ? 0 : -1;
	const int8_t my = start.y < destination.y ? 1 : start.y == destination.y ? 0 : -1;

	int32_t A = Coordinate::getOffsetY(destination, start);
	int32_t B = Coordinate::getOffsetX(start, destination);
	int32_t C = -(A * destination.x + B * destination.y);


	TilePtr tile = gMapTilesPtr->get_tile_at(start, false);
	/*	if (!tile)
	return false;
	if (tile->has_thing_attribute(ThingAttrBlockProjectile)
	|| (!gMapMinimapPtr->get_walkability(tile->coordinate, map_front_cached_t) && tile->contains_creature())){
	return false;
	}*/

	TilePtr tile_dest = gMapTilesPtr->get_tile_at(toPos, false);
	if (!tile_dest)
		return false;

	if (tile_dest->get_thing_count() == 0)
		return false;


	if (tile_dest->has_thing_attribute(ThingAttrBlockProjectile) || (!gMapMinimapPtr->get_walkability(tile_dest->coordinate, map_front_cached_t) && !tile_dest->contains_creature())){
		return false;
	}


	while (!Coordinate::areInRange<0, 0, 15>(start, destination)) {
		int32_t move_hor = std::abs(A * (start.x + mx) + B * (start.y) + C);
		int32_t move_ver = std::abs(A * (start.x) + B * (start.y + my) + C);
		int32_t move_cross = std::abs(A * (start.x + mx) + B * (start.y + my) + C);

		if (start.y != destination.y && (start.x == destination.x || move_hor > move_ver || move_hor > move_cross)) {
			start.y += my;
		}

		if (start.x != destination.x && (start.y == destination.y || move_ver > move_hor || move_ver > move_cross)) {
			start.x += mx;
		}

		TilePtr tile = gMapTilesPtr->get_tile_at(start, false);
		if (tile && tile->has_thing_attribute(ThingAttrBlockProjectile)) {
			return false;
		}
	}
	if (check_z){
		return can_click_coord(toPos);
	}
	return true;
}

MapTiles* gMapTilesPtr;
MapMinimap* gMapMinimapPtr;
MapCacheControl* gMapCacheControlPtr;

MapTiles::MapTiles(){

}

void MapMinimap::GeneralRefresh(){	
	THREAD_TIMER_DECLARE;

	Sleep(5000);

	while (true){
		THREAD_TIMER_WAIT_TOTAL(100);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		THREAD_TIMER_RESET;
		//	FUNCTION_MEASURE_TIME_START(0)
		gMapTilesPtr->refresh();

		//	FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
		gMapMinimapPtr->refresh();

		MapTiles::refreshed_count++;
		//	FUNCTION_MEASURE_TIME_END(0)
	}
}

uint64_t MapTiles::refreshed_count = 0;

std::vector<CreatureOnBattlePtr> MapTiles::get_another_creatures_on_self_tile(){
	TilePtr tile = get_tile_at(TibiaProcess::get_default()->character_info->get_mixed_coordinate());
	uint32_t m_id = TibiaProcess::get_default()->character_info->character_id();
	std::vector<CreatureOnBattlePtr> retval;
	if (tile){
		auto creatures = tile->get_creatures();
		for (auto it = creatures.begin(); it != creatures.end(); it++){
			if (it->second->id != m_id){
				retval.push_back(it->second);
			}
		}
	}
	return retval;
}

std::vector<CreatureOnBattlePtr> MapTiles::get_monsters_on_self_tile(){
	TilePtr tile = get_tile_at(TibiaProcess::get_default()->character_info->get_mixed_coordinate());
	uint32_t m_id = TibiaProcess::get_default()->character_info->character_id();
	std::vector<CreatureOnBattlePtr> retval;
	if (tile){
		auto creatures = tile->get_creatures();
		for (auto it = creatures.begin(); it != creatures.end(); it++){
			if (it->second->is_monster()){
				retval.push_back(it->second);
			}
		}
	}
	return retval;
}

std::vector<CreatureOnBattlePtr> MapTiles::get_another_players_on_self_tile(){
	TilePtr tile = get_tile_at(TibiaProcess::get_default()->character_info->get_mixed_coordinate());
	uint32_t m_id = TibiaProcess::get_default()->character_info->character_id();
	std::vector<CreatureOnBattlePtr> retval;
	if (tile){
		auto creatures = tile->get_creatures();
		for (auto it = creatures.begin(); it != creatures.end(); it++){
			if (it->second->is_player() && it->second->id != m_id){
				retval.push_back(it->second);
			}
		}
	}
	return retval;
}

void MapTiles::wait_refresh(){
	uint64_t current_count = refreshed_count;
	while (current_count == refreshed_count){
		Sleep(1);
	}
}

uint32_t MapTiles::wait_delay_near_yellow_coord(uint32_t consider_nearest, uint32_t delay_start){
	uint32_t distance = gMapMinimapPtr->distance_near_yellow_coord();
	if (distance == UINT32_MAX)
		return UINT32_MAX;

	if (distance > consider_nearest)
		return UINT32_MAX;

	int result = distance - consider_nearest;
	int delay = (std::abs(result)) * delay_start;
	Sleep(delay);

	//TibiaProcess::get_default()->wait_ping_delay(std::abs(result));

	return delay;
}

uint32_t MapMinimap::distance_near_yellow_coord(){
	for (auto coord_yellow = last_yellow_coords->begin(); coord_yellow != last_yellow_coords->end(); coord_yellow++){
		Coordinate current_coord_yellow = Coordinate(coord_yellow->getX(), coord_yellow->getY(), coord_yellow->getZ());
		current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

		return current_coord.get_axis_max_dist(current_coord_yellow);
	}
	return UINT32_MAX;
}

bool MapMinimap::check_nearest_yellow_coordinate(Coordinate current_coord, uint32_t consider_nearest){
	for (auto coord_yellow = last_yellow_coords->begin(); coord_yellow != last_yellow_coords->end(); coord_yellow++){
		Coordinate current_coord_yellow = Coordinate(coord_yellow->getX(), coord_yellow->getY(), coord_yellow->getZ());

		if (current_coord.get_axis_max_dist(current_coord_yellow) <= consider_nearest)
			return true;
	}
	return false;
}

bool MapMinimap::is_coordinate_yellow(Coordinate coord){
	for (auto coord_yellow = last_yellow_coords->begin(); coord_yellow != last_yellow_coords->end(); coord_yellow++)
	if (coord_yellow->getX() == coord.getX() && coord_yellow->getY() == coord.getY())
		return true;

	return false;
}

MapMinimap::MapMinimap(){

	changed_last_x = false;
	changed_last_y = false;;
	changed_last_z = false;

	last_x = 0;
	last_y = 0;
	last_z = 0;

	sqm_to_consider_near_yellow_points = 10;
	last_yellow_coords = new std::vector < Coordinate >();
	last_yellow_coords_swap = new std::vector < Coordinate >();

	std::thread( 
		[&](){
		MONITOR_THREAD("MapMinimap::MapMinimap");
		GeneralRefresh();
	}
	).detach();
}

unsigned char** MapCacheControl::get_map_ptr(uint32_t map_id_x, uint32_t map_id_y, uint32_t z, bool clean){
	auto& map_of_maps = clean ? clean_cached_maps : cached_maps;
	auto find_z = map_of_maps.find(z);
	if (find_z == map_of_maps.end()){
		if (!create_map(map_id_x, map_id_y, z, clean)){
			return nullptr;
		}
		return get_map_ptr(map_id_x, map_id_y, z, false);
	}
	auto find_y = find_z->second.find(map_id_y);
	if (find_y == find_z->second.end()){
		if (!create_map(map_id_x, map_id_y, z, clean)){
			return nullptr;
		}
		return get_map_ptr(map_id_x, map_id_y, z, false);
	}
	auto find_x = find_y->second.find(map_id_x);
	if (find_x == find_y->second.end()){
		if (!create_map(map_id_x, map_id_y, z, clean)){
			return nullptr;
		}
		return get_map_ptr(map_id_x, map_id_y, z, false);
	}
	return (unsigned char**)find_x->second;
}

void MapCacheControl::unload_not_necessary_cached_maps(){
	//tibia
	for (auto map_it = cached_maps.begin(); map_it != cached_maps.end();){
		if (abs((int)map_it->first - (int)last_z_center) > 1){
			auto& map_y = map_it->second;
			for (auto map_it_y = map_y.begin(); map_it_y != map_y.end(); map_it_y++){
				auto& map_x = map_it_y->second;
				for (auto map_it_x = map_x.begin(); map_it_x != map_x.end(); map_it_x++){
					delete[]map_it_x->second;
				}
			}
			map_it = cached_maps.erase(map_it);
			continue;
		}
		auto& map_y = map_it->second;
		for (auto map_it_y = map_y.begin(); map_it_y != map_y.end();){
			if (abs((int)map_it_y->first - (int)last_y_center) > 1){
				auto& map_x = map_it_y->second;
				for (auto map_it_x = map_x.begin(); map_it_x != map_x.end(); map_it_x++){
					delete[]map_it_x->second;
				}
				map_it_y = map_y.erase(map_it_y);
				continue;
			}
			auto& map_x = map_it_y->second;
			for (auto map_it_x = map_x.begin(); map_it_x != map_x.end();){
				if (abs((int)map_it_x->first - (int)last_x_center) > 1){
					delete[]map_it_x->second;
					map_it_x = map_x.erase(map_it_x);
					continue;
				}
				map_it_x++;
			}
			map_it_y++;
		}
		map_it++;
	}

	for (auto map_it = clean_cached_maps.begin(); map_it != clean_cached_maps.end();){
		if (abs((int)map_it->first - (int)last_z_center) > 1){
			map_it = clean_cached_maps.erase(map_it);
			continue;
		}
		auto& map_y = map_it->second;
		for (auto map_it_y = map_y.begin(); map_it_y != map_y.end();){
			if (abs((int)map_it_y->first - (int)last_y_center) > 1){
				map_it_y = map_y.erase(map_it_y);
				continue;
			}
			auto& map_x = map_it_y->second;
			for (auto map_it_x = map_x.begin(); map_it_x != map_x.end();){
				if (abs((int)map_it_x->first - (int)last_x_center) > 1){
					map_it_x = map_x.erase(map_it_x);
					continue;
				}
				map_it_x++;
			}
			map_it_y++;
		}
		map_it++;
	}
}

bool MapCacheControl::create_map_from_file(uint32_t map_id_x, uint32_t map_id_y, uint32_t z, bool clean){
	if (clean){
		ChacheMiniMapFile* map = get_smart_map_cached(map_id_x, map_id_y, z);
		if (!map)
			return false;
		if (map->loaded()){
			clean_cached_maps[z][map_id_y][map_id_x] = (unsigned char(*)[256][256])  &map->data_1[0][0];
			return true;
		}
		return false;
	}
	else{

		/*unsigned char(*mcached_new_map)[256][256] = (unsigned char(*)[256][256]) new unsigned char[256 * 256];

		clean_cached_maps[z][map_id_y][map_id_x] = (unsigned char(*)[256][256])&map->data_1[0][0];
		cached_maps[z][map_id_y][map_id_x] = mcached_new_map;

		unsigned char(*mcached_new_map)[256][256] = (unsigned char(*)[256][256]) new unsigned char[256 * 256];
		clean_cached_maps[z][map_id_y][map_id_x] = (unsigned char(*)[256][256])&map->data_1[0][0];
		return true;*/
	}
	return false;
}

bool MapCacheControl::create_map(uint32_t map_id_x, uint32_t map_id_y, uint32_t z, bool clean){
	if (create_map_from_file(map_id_x, map_id_y, z, clean))
		return false;

	Coordinate current_coord = gMapMinimapPtr->current_coord;
	int start_x = map_id_x * 256;
	int start_y = map_id_y * 256;
	int current_z = current_coord.z;




	if (clean){
		unsigned char(*mclean_cached_new_map)[256][256] = nullptr;
		
		if (clean_cached_maps[z][map_id_y].find(map_id_x) == clean_cached_maps[z][map_id_y].end()){
			mclean_cached_new_map = (unsigned char(*)[256][256]) new unsigned char[256 * 256];
		}
		else{
			mclean_cached_new_map = clean_cached_maps[z][map_id_y][map_id_x];
		}
		//KKT PERGUNTA AO ANDREI SE � PARA DELETA ESSE NEW USINGNED CHAR
		
		for (int x = 0; x < 256; x++){
			for (int y = 0; y < 256; y++){
				Coordinate now_coord(start_x + x, start_y + y, z);
				(*mclean_cached_new_map)[x][y] = pathfinder_walkability_t::pathinder_walkability_undefined;
			}
		}
		clean_cached_maps[z][map_id_y][map_id_x] = mclean_cached_new_map;

		return false;
	}
	else{
		unsigned char(*mcached_new_map)[256][256] = (unsigned char(*)[256][256]) new unsigned char[256 * 256];
		for (int x = 0; x < 256; x++)
		for (int y = 0; y < 256; y++){
			Coordinate now_coord(start_x + x, start_y + y, z);
			(*mcached_new_map)[x][y] = pathfinder_walkability_t::pathinder_walkability_undefined;
		}
		cached_maps[z][map_id_y][map_id_x] = mcached_new_map;
		return true;
	}

	/*unsigned char(*mcached_new_map)[256][256] = (unsigned char(*)[256][256]) new unsigned char[256 * 256];

	int start_x = map_id_x * 256;
	int start_y = map_id_y * 256;
	int current_z = current_coord.z;

	for (int x = 0; x < 256; x++)
	for (int y = 0; y < 256; y++){
	Coordinate now_coord(start_x + x, start_y + y, z);
	(*mcached_new_map)[x][y] = pathfinder_walkability_t::pathinder_walkability_undefined;

	(*mclean_cached_new_map)[x][y] = pathfinder_walkability_t::pathinder_walkability_undefined;
	/*
	if ((current_coord).get_axis_max_dist(now_coord) > 127){
	(*mcached_new_map)[x][y] = pathfinder_walkability_t::pathinder_walkability_undefined;

	(*mclean_cached_new_map)[x][y] = pathfinder_walkability_t::pathinder_walkability_undefined;
	}
	else{
	(*mcached_new_map)[x][y] = gMapMinimapPtr->get_walkability(now_coord, map_front_t) ? WALKABLE
	: UNWALKABLE;
	(*mclean_cached_new_map)[x][y] = gMapMinimapPtr->get_walkability_value_at(now_coord, map_front_advanced_t);
	}
	*/
	/*	}


	cached_maps[z][map_id_y][map_id_x] = mcached_new_map;
	clean_cached_maps[z][map_id_y][map_id_x] = mclean_cached_new_map;*/
	return false;
}

bool MapMinimap::is_trapped_around(map_type_t map_type){
	for (int x = -1; x < 2; x++){
		for (int y = -1; y < 2; y++){
			if (x == 0 && y == 0)
				continue;
			if (walkability[map_type][128 + x][128 + y] == WALKABLE)
				return false;
		}
	}
	return true;
}

void MapCacheControl::update_screen(){
	Coordinate current_coord = gMapMinimapPtr->current_coord;
	std::function<void(uint32_t x, uint32_t y, unsigned char value, unsigned char value2)> update_map_pos = [&](uint32_t x, uint32_t y, unsigned char value, unsigned char value2) ->void {
		gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
			value);
		gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
			value2, true);
	};

	Coordinate diff_last = current_coord - last_refresh_coord;
	Coordinate diff_last_full = current_coord - last_full_refresh_coord;

	int dif_x = std::min(abs(diff_last.x) + 5, 256);
	int dif_y = std::min(abs(diff_last.y) + 5, 256);
	if (MapMinimap::changed_last_x || MapMinimap::changed_last_y || MapMinimap::changed_last_z){
		//changed_last_zd

		if (MapMinimap::changed_last_center){
			check_load_tibia_maps(current_coord.x, current_coord.y, current_coord.z);
			unload_not_necessary_cached_maps();
			Coordinate center_now(MapMinimap::current_coord.x / 256, MapMinimap::current_coord.y / 256);

			//Parte que transforma em mapa pequeno e organiza;
			int pos_x_in_organized = 128 + (MapMinimap::current_coord.x - (center_now.x * 256));
			int pos_y_in_organized = 128 + (MapMinimap::current_coord.y - (center_now.y * 256));
			//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
			Coordinate map_real_coordinate((center_now.x - 1) * 256 + 128, (center_now.y - 1) * 256 + 128);
			std::vector<std::pair<uint32_t, uint32_t>> yellow_coords;
			//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
			for (int x = 0; x < 256; x++){
				for (int y = 0; y < 256; y++){
					int tvalx = x + pos_x_in_organized;
					int tvaly = y + pos_y_in_organized;

					int x_now = (int)tvalx / 256;
					int y_now = (int)tvaly / 256;

					if (x_now > 2 || x_now < 0 || y_now > 2 || y_now < 0){
						continue;
					}

					void* current_ptr = nullptr;
					unsigned char value;


					int in_map_x = tvalx % 256;
					int in_map_y = tvaly % 256;
					value = organized_tibia_map_buffer[x][y];
					/*if (current_ptr = organized_smart_map[x_now][y_now]){//smart map
					value = organized_smart_map_buffer[x][y];
					}
					else if (current_ptr = MapMinimap::organized_arround_maps_ptr[x_now][y_now]){//current tibia memory map*/
					value = (unsigned char)MapMinimap::walkability[map_type_t::map_front_advanced_t][x][y];
					/*}
					else if (current_ptr = organized_tibia_map[x_now][y_now]){//tibia map file
					value = organized_tibia_map_buffer[x][y];
					}
					else{
					continue;
					}*/
					update_map_pos(x, y, MapMinimap::walkability[map_front_t][x][y], value);
				}
			}
		}

		/*	std::map<uint32_t, std::map<uint32_t, std::map<uint32_t, char*[256]>>> arround_maps;
		for (int x = 0; x < 256; x++){
		for (int y = 0; y < 256; y++){
		gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
		MapMinimap::walkability[map_front_t][x][y]);
		gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
		MapMinimap::walkability[map_front_t][x][y], true);
		}
		}*/
		last_full_refresh_coord = current_coord;
		last_refresh_coord = current_coord;
	}
	/*	if (diff_last.x < 0){ // walked left
	for (int x = 0; x < dif_x + 1; x++){
	for (int y = 0; y < 256; y++){
	gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
	MapMinimap::walkability[map_front_t][x][y]);

	gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
	MapMinimap::walkability[map_front_t][x][y], true);
	}
	}
	}
	else if (diff_last.x > 0){// walked right
	for (int x = 0; x < dif_x + 1; x++){
	for (int y = 0; y < 256; y++){
	gMapCacheControlPtr->update_undef_walkability(current_coord.x + 127 - x, current_coord.y - 128 + y, current_coord.z,
	MapMinimap::walkability[map_front_t][255 - x][y]);

	gMapCacheControlPtr->update_undef_walkability(current_coord.x + 127 - x, current_coord.y - 128 + y, current_coord.z,
	MapMinimap::walkability[map_front_t][255 - x][y], true);
	}
	}
	}
	if (diff_last.y > 0){//walked down
	for (int x = 0; x < 256; x++){
	for (int y = 0; y < dif_y + 1; y++){
	gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y + 127 - y, current_coord.z,
	MapMinimap::walkability[map_front_t][x][255 - y]);

	gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y + 127 - y, current_coord.z,
	MapMinimap::walkability[map_front_t][x][255 - y], true);
	}
	}
	}
	else if (diff_last.y < 0){//walked up
	for (int x = 0; x < 256; x++){
	for (int y = 0; y < dif_y + 1; y++){
	gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
	MapMinimap::walkability[map_front_t][x][y]);

	gMapCacheControlPtr->update_undef_walkability(current_coord.x - 128 + x, current_coord.y - 128 + y, current_coord.z,
	MapMinimap::walkability[map_front_t][x][y], true);
	}
	}
	}*/
	last_refresh_coord = current_coord;
	for (int x = 0; x < 15; x++){
		for (int y = 0; y < 11; y++){
			gMapCacheControlPtr->set_walkability(current_coord.x - 7 + x, current_coord.y - 5 + y, current_coord.z,
				MapMinimap::walkability[map_front_t][128 - 7 + x][128 - 5 + y]);
			gMapCacheControlPtr->set_walkability(current_coord.x - 7 + x, current_coord.y - 5 + y, current_coord.z,
				MapMinimap::walkability[map_front_advanced_t][128 - 7 + x][128 - 5 + y], true);
		}
	}
}

std::vector<map_ptr_pair> fast_maps;
std::vector<map_ptr_pair> clean_fast_maps;

inline map_ptr_array find_local_map(uint32_t x, uint32_t y) {
	int sz = fast_maps.size();
	for (int i = 0; i < sz; i++){
		map_ptr_pair* _pair = &fast_maps[i];
		if (_pair->x == x && _pair->y == y){
			return _pair->ptr;
		}
	}
	return nullptr;
};

inline map_ptr_array find_local_map_clean(uint32_t x, uint32_t y) {
	int sz = clean_fast_maps.size();
	for (int i = 0; i < sz; i++){
		map_ptr_pair* _pair = &clean_fast_maps[i];
		if (_pair->x == x && _pair->y == y){
			return _pair->ptr;
		}
	}
	return nullptr;
};

void MapCacheControl::generate_map_type_cache(){
	fast_maps.clear();
	clean_fast_maps.clear();

	//FUNCTION_MEASURE_TIME_START(0)
	update_screen();
	static unsigned char temp_map[256][256];
	static unsigned char clean_temp_map[256][256];
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	Coordinate current_coord = gMapMinimapPtr->current_coord;
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)

	uint32_t center_now_x = current_coord.x / 256;
	uint32_t center_now_y = current_coord.y / 256;
	for (int x = -1; x < 2; x++){
		for (int y = -1; y < 2; y++){
			int current_start_map_x = center_now_x + x;
			int current_start_map_y = center_now_y + y;
			if (current_start_map_x <= 0 || current_start_map_y <= 0)
				continue;
#pragma region NORMAL
			bool found_map = false;
			for (auto map : fast_maps){
				if (map.x == current_start_map_x && map.y == current_start_map_y){
					found_map = true;
					break;
				}
			}
			if (found_map)
				continue;

			unsigned char(*ptr_fast)[256][256] = (unsigned char(*)[256][256])get_map_ptr(current_start_map_x,
				current_start_map_y, current_coord.z);

			if (ptr_fast){
				fast_maps.push_back(
					map_ptr_pair((uint32_t)current_start_map_x, (uint32_t)current_start_map_y, ptr_fast));
			}
#pragma endregion
#pragma region CLEAR
			found_map = false;
			for (auto map : clean_fast_maps){
				if (map.x == current_start_map_x && map.y == current_start_map_y){
					found_map = true;
					break;
				}
			}
			if (found_map)
				continue;

			ptr_fast = (unsigned char(*)[256][256])get_map_ptr(current_start_map_x,
				current_start_map_y, current_coord.z, true);
			if (ptr_fast){
				clean_fast_maps.push_back(
					map_ptr_pair((uint32_t)current_start_map_x, (uint32_t)current_start_map_y, ptr_fast));
			}
#pragma endregion
		}
	}
	//	FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	int current_start_map_x = 0;
	int current_offset_map_x = 0;
	int current_start_map_y = 0;
	int current_offset_map_y = 0;
	map_ptr_array fast_m;
	map_ptr_array fast_clean_m;

	for (int x = 0; x < 256; x++){
		current_start_map_x = (current_coord.x - 128 + x) / 256;
		current_offset_map_x = (current_coord.x - 128 + x) % 256;
		for (int y = 0; y < 256; y++){
			current_start_map_y = (current_coord.y - 128 + y) / 256;
			current_offset_map_y = (current_coord.y + y - 128) % 256;

			fast_m = find_local_map(current_start_map_x, current_start_map_y);

			fast_clean_m = find_local_map_clean(current_start_map_x, current_start_map_y);

			if (fast_m)
				temp_map[x][y] = (fast_m)[0][current_offset_map_x][current_offset_map_y];
			if (!fast_clean_m)
				clean_temp_map[x][y] = WALKABLE;// (fast_clean_m)[0][current_offset_map_x][current_offset_map_y];
			else
				clean_temp_map[x][y] = (fast_clean_m)[0][current_offset_map_x][current_offset_map_y];
		}
	}

	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)

	memcpy(&MapMinimap::walkability[map_front_cached_t][0][0], &temp_map[0][0], 256 * 256);

	memcpy(&MapMinimap::walkability[map_front_cached_advanced_t][0][0], &clean_temp_map[0][0], 256 * 256);

	for (int x = 0; x < 255; x++){
		for (int y = 0; y < 255; y++){
			MapMinimap::walkability[map_front_clear_t][x][y]
				= MapMinimap::walkability[map_front_cached_advanced_t][x][y] != 0;
		}
	}

	ZeroMemory(&MapMinimap::walkability[map_front_clear_minimap_t][0][0], 256 * 256);
	for (int x = 128 - 7; x < 128 + 7; x++){
		for (int y = 128 - 5; y < 128 + 5; y++){
			MapMinimap::walkability[map_front_clear_minimap_t][x][y]
				= MapMinimap::walkability[map_front_cached_advanced_t][x][y] != 0;
		}
	}

	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
}

void MapCacheControl::save_map_files(){
	/*for (auto map_z = clean_cached_maps.begin(); map_z != clean_cached_maps.end(); map_z++){
	for (auto map_y = map_z->second.begin(); map_y != map_z->second.end(); map_y++){
	for (auto map_x = map_y->second.begin(); map_x != map_y->second.end(); map_x++){
	ChacheMiniMapFile::save(map_z->first, map_y->first, map_x->first,&(*map_x->second)[0][0]);
	}
	}
	}*/
}

MapCacheControl::MapCacheControl() : MemoryCounter("MapCacheControl"){
	//	NeutralManager::add_callback_on_close(boost::bind(&MapCacheControl::save_map_files, this));
}

void MapCacheControl::set_walkability(uint32_t x, uint32_t y, uint32_t z, unsigned char walkability, bool clean){
	int x_now = (int)x / 256;
	int y_now = (int)y / 256;
	unsigned char(*mmap)[256][256] = (unsigned char(*)[256][256])get_map_ptr(x_now, y_now, z, clean);

	int rest_x = x % 256;
	int rest_y = y % 256;
	if (mmap)
		(*mmap)[rest_x][rest_y] = walkability;
}

void MapCacheControl::update_undef_walkability(uint32_t x, uint32_t y, uint32_t z, unsigned char walkability, bool clean){

	int x_now = (int)x / 256;
	int y_now = (int)y / 256;
	unsigned char(*mmap)[256][256] = (unsigned char(*)[256][256])get_map_ptr(x_now, y_now, z, clean);
	if (!mmap){
		mmap = (unsigned char(*)[256][256])get_map_ptr(x_now, y_now, z, clean);
		if (!mmap)
			return;
	}

	int rest_x = x % 256;
	int rest_y = y % 256;
	if ((*mmap)[rest_x][rest_y] == UNDEFINED){
		(*mmap)[rest_x][rest_y] = walkability;
	}
}

ChacheMiniMapFile::~ChacheMiniMapFile(){
	if (!hash_match()){
		save();
	}
}

MiniMapFile::~MiniMapFile(){
}

MiniMapFile::MiniMapFile() : MemoryCounter("MiniMapFile"){
	state = file_load_none;
}

void MiniMapFile::load(int z, int y, int x){
	central_coord = Coordinate(x, y, z);
	std::string x_str = std::to_string(x);
	while (x_str.length() < 3){
		x_str += "0" + x_str;
	}

	std::string y_str = std::to_string(y);
	while (y_str.length() < 3){
		y_str += "0" + y_str;
	}

	std::string z_str = std::to_string(z);
	while (z_str.length() < 2){
		z_str = "0" + z_str;
	}

	std::string file_name = x_str + y_str + z_str + ".map";

	FILE *f = nullptr;
	errno_t error = fopen_s(&f, &(maps_dir + std::string("\\") + file_name)[0], "rb");
	if (!f || error){
		state = file_load_fail;
		return;
	}


	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);


	int count = fread(&data_1[0][0], 1, (256 * 256 * 2), f);
	state = count == (256 * 256 * 2) ? file_load_success : file_load_fail;
	fclose(f);

	update_hash();
}

ChacheMiniMapFile::ChacheMiniMapFile() : MemoryCounter("CreatureOnBattle"){
	state = file_load_none;
}

void ChacheMiniMapFile::load(int z, int y, int x){
	central_coord = Coordinate(x, y, z);
	std::string x_str = std::to_string(x);
	while (x_str.length() < 3){
		x_str += "0" + x_str;
	}

	std::string y_str = std::to_string(y);
	while (y_str.length() < 3){
		y_str += "0" + y_str;
	}

	std::string z_str = std::to_string(z);
	while (z_str.length() < 2){
		z_str = "0" + z_str;
	}

	std::string file_name = x_str + y_str + z_str + ".smart_map";

	FILE *f = nullptr;
	errno_t error = fopen_s(&f, &(chached_maps_dir + std::string("\\") + file_name)[0], "rb");
	if (!f || error){
		state = file_load_fail;
		return;
	}


	fseek(f, 0, SEEK_END);
	long fsize = ftell(f);
	fseek(f, 0, SEEK_SET);


	int count = fread(&data_1[0][0], 1, (256 * 256), f);
	state = count == (256 * 256) ? file_load_success : file_load_fail;
	fclose(f);
	update_hash();
}

void ChacheMiniMapFile::save(int z, int y, int x){
	state = save(z, y, x, &data_1[0][0]);
}

file_load_status_t ChacheMiniMapFile::save(int z, int y, int x, unsigned char* data){

	boost::filesystem::path rootPath(chached_maps_dir + "\\");

	if (!boost::filesystem::exists(rootPath)){
		boost::system::error_code returnedError;
		boost::filesystem::create_directories(rootPath, returnedError);
		if (returnedError)
			MessageBox(0, "Fail to access map directory", "error", MB_OK);
	}

	std::string x_str = std::to_string(x);
	while (x_str.length() < 3){
		x_str += "0" + x_str;
	}

	std::string y_str = std::to_string(y);
	while (y_str.length() < 3){
		y_str += "0" + y_str;
	}

	std::string z_str = std::to_string(z);
	while (z_str.length() < 2){
		z_str = "0" + z_str;
	}

	std::string file_name = x_str + y_str + z_str + ".smart_map";

	FILE *f = nullptr;
	errno_t error = fopen_s(&f, &(chached_maps_dir + std::string("\\") + file_name)[0], "w+");
	if (!f || error){
		return file_load_fail;
	}

	int count = fwrite(&data[0], 1, (256 * 256), f);
	fclose(f);
	return count == (256 * 256) ? file_save_success : file_save_fail;
}

void init_maps(){
	gMapTilesPtr = new MapTiles;
	gMapMinimapPtr = new MapMinimap;
	gMapCacheControlPtr = new MapCacheControl;
}

ChacheMiniMapFile* MapCacheControl::get_smart_map_cached(uint32_t x, uint32_t y, uint32_t z){
	auto& y_map = chache_smart_maps.find(z);
	if (y_map == chache_smart_maps.end())
		return nullptr;

	auto& x_map = y_map->second.find(y);
	if (x_map == y_map->second.end())
		return nullptr;

	auto& value_it = x_map->second.find(x);
	if (value_it == x_map->second.end())
		return nullptr;

	return value_it->second;
}

MiniMapFile* MapCacheControl::get_tibia_map_cached(uint32_t x, uint32_t y, uint32_t z){
	auto& y_map = chache_tibia_maps.find(z);
	if (y_map == chache_tibia_maps.end())
		return nullptr;

	auto& x_map = y_map->second.find(y);
	if (x_map == y_map->second.end())
		return nullptr;

	auto& value_it = x_map->second.find(x);
	if (value_it == x_map->second.end())
		return nullptr;

	return value_it->second;
}

void MapCacheControl::unload_not_arround_maps(){
	for (auto map_it = chache_tibia_maps.begin(); map_it != chache_tibia_maps.end();){
		if (abs((int)map_it->first - (int)last_z_center) > 1){
			auto& map_y = map_it->second;
			for (auto map_it_y = map_y.begin(); map_it_y != map_y.end(); map_it_y++){
				auto& map_x = map_it_y->second;
				for (auto map_it_x = map_x.begin(); map_it_x != map_x.end(); map_it_x++){
					if (map_it_x->second)
						delete map_it_x->second;
				}
			}
			map_it = chache_tibia_maps.erase(map_it);
			continue;
		}
		auto& map_y = map_it->second;
		for (auto map_it_y = map_y.begin(); map_it_y != map_y.end();){
			if (abs((int)map_it_y->first - (int)last_y_center) > 1){
				auto& map_x = map_it_y->second;
				for (auto map_it_x = map_x.begin(); map_it_x != map_x.end();){
					if (map_it_x->second)
						delete map_it_x->second;
					map_it_x++;
				}
				map_it_y = map_y.erase(map_it_y);
				continue;
			}
			auto& map_x = map_it_y->second;
			for (auto map_it_x = map_x.begin(); map_it_x != map_x.end();){
				if (abs((int)map_it_x->first - (int)last_x_center) > 1){
					if (map_it_x->second)
						delete map_it_x->second;
					map_it_x = map_x.erase(map_it_x);
					continue;
				}
				map_it_x++;
			}
			map_it_y++;
		}
		map_it++;
	}
}

void MapCacheControl::unload_not_arround_smart_bot_maps(){
	for (auto map_it = chache_smart_maps.begin(); map_it != chache_smart_maps.end();){
		if (abs((int)map_it->first - (int)last_z_center) > 1){
			auto& map_y = map_it->second;
			for (auto map_it_y = map_y.begin(); map_it_y != map_y.end(); map_it_y++){
				auto& map_x = map_it_y->second;
				for (auto map_it_x = map_x.begin(); map_it_x != map_x.end(); map_it_x++){
					delete map_it_x->second;
				}
			}
			map_it = chache_smart_maps.erase(map_it);
			continue;
		}
		auto& map_y = map_it->second;
		for (auto map_it_y = map_y.begin(); map_it_y != map_y.end();){
			if (abs((int)map_it_y->first - (int)last_y_center) > 1){
				auto& map_x = map_it_y->second;
				for (auto map_it_x = map_x.begin(); map_it_x != map_x.end(); map_it_x++){
					delete map_it_x->second;
				}
				map_it_y = map_y.erase(map_it_y);
				continue;
			}
			auto& map_x = map_it_y->second;
			for (auto map_it_x = map_x.begin(); map_it_x != map_x.end();){
				if (abs((int)map_it_x->first - (int)last_x_center) > 1){
					delete map_it_x->second;
					map_it_x = map_x.erase(map_it_x);
					continue;
				}
				map_it_x++;
			}
			map_it_y++;
		}
		map_it++;
	}
}

void MapCacheControl::load_arround_maps(){
	std::vector<std::pair<uint32_t, std::pair<uint32_t, uint32_t>>> required_maps;

	for (uint32_t y = last_y_center - 1; y < last_y_center + 2; y++){
		for (uint32_t x = last_x_center - 1; x < last_x_center + 2; x++){
			required_maps.push_back(std::pair<uint32_t, std::pair<uint32_t, uint32_t>>
				(last_z_center, std::pair<uint32_t, uint32_t>(y, x)));
		}
	}


	for (int x = 0; x < 3; x++){
		for (int y = 0; y < 3; y++){
			organized_tibia_map[x][y] = nullptr;
			organized_tibia_map[x][y] = nullptr;
		}
	}

	for (auto map_required : required_maps){
		load_cache_tibia_map(map_required.second.second, map_required.second.first, map_required.first);
		load_cache_smart_map(map_required.second.second, map_required.second.first, map_required.first);
	}
}

void MapCacheControl::load_cache_tibia_map(uint32_t x, uint32_t y, uint32_t z){
	MiniMapFile* map_ptr = get_tibia_map_cached(x, y, z);
	if (map_ptr)
		return;

	organized_tibia_map[x][y] = nullptr;
	organized_tibia_map[x][y] = nullptr;

	map_ptr = new MiniMapFile();
	chache_tibia_maps[z][y][x] = map_ptr;
	map_ptr->load(z, y, x);

	if (map_ptr->loaded() && MapMinimap::current_coord.z == z){
		Coordinate _final = (Coordinate(x, y) - Coordinate(last_x_center, last_y_center));
		Coordinate center_organized(1, 1);

		_final = _final + center_organized;
		if (_final.x >= 0 && _final.x <= 2)
		if (_final.y >= 0 && _final.y <= 2)
			organized_tibia_map[_final.x][_final.y] = map_ptr;
	}
}

void MapCacheControl::load_cache_smart_map(uint32_t x, uint32_t y, uint32_t z){
	ChacheMiniMapFile* map_ptr = get_smart_map_cached(x, y, z);
	if (map_ptr)
		return;

	map_ptr = new ChacheMiniMapFile();
	chache_smart_maps[z][y][x] = map_ptr;
	map_ptr->load(z, y, x);
	if (!map_ptr->loaded()){
		auto map = get_tibia_map_cached(x, y, z);
		if (!map || !map->loaded())
			return;

		for (int x = 0; x < 256; x++){
			for (int y = 0; y < 256; y++)
				map_ptr->data_1[x][y] = map->get_walkability_at(x, y, map_front_t);
		}
		map_ptr->state = file_load_status_t::file_load_success;
	}

	if (map_ptr->loaded() && MapMinimap::current_coord.z == z){
		int center_x = x / 256;
		int center_y = y / 256;
		Coordinate _final = (Coordinate(x, y) - Coordinate(center_x, center_y));
		Coordinate center_organized(1, 1);

		_final = _final + center_organized;
		if (_final.x >= 0 && _final.x <= 2)
		if (_final.y >= 0 && _final.y <= 2)
			organized_smart_map[_final.x][_final.y] = map_ptr;
	}
}

void MapCacheControl::update_cached_arround_maps(){
	memset(organized_tibia_map_buffer, pathfinder_walkability_t::pathinder_walkability_undefined, sizeof(organized_tibia_map_buffer));
	memset(organized_smart_map_buffer, pathfinder_walkability_t::pathinder_walkability_undefined, sizeof(organized_smart_map_buffer));

	Coordinate center_now(MapMinimap::current_coord.x / 256, MapMinimap::current_coord.y / 256);

	//Parte que transforma em mapa pequeno e organiza;
	int pos_x_in_organized = 128 + (MapMinimap::current_coord.x - (center_now.x * 256));
	int pos_y_in_organized = 128 + (MapMinimap::current_coord.y - (center_now.y * 256));
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	Coordinate map_real_coordinate((center_now.x - 1) * 256 + 128, (center_now.y - 1) * 256 + 128);
	std::vector<std::pair<uint32_t, uint32_t>> yellow_coords;
	//FUNCTION_MEASURE_TIME_ELAPSED_RESET(0)
	for (int x = 0; x < 256; x++){
		for (int y = 0; y < 256; y++){
			int tvalx = x + pos_x_in_organized;
			int tvaly = y + pos_y_in_organized;

			int x_now = (int)tvalx / 256;
			int y_now = (int)tvaly / 256;

			if (x_now > 2 || x_now < 0 || y_now > 2 || y_now < 0){
				continue;
			}

			int in_map_x = tvalx % 256;
			int in_map_y = tvaly % 256;

#pragma region REFRESH_MAP_BACK
			ChacheMiniMapFile* now_smart_map = organized_smart_map[x_now][y_now];
			if (now_smart_map)
				organized_smart_map_buffer[x][y] = now_smart_map->data_1[in_map_x][in_map_y];

			MiniMapFile* now_tibia_map = organized_tibia_map[x_now][y_now];
			if (now_tibia_map){
				organized_tibia_map_buffer[x][y] = now_tibia_map->get_walkability_at(in_map_x, in_map_y, map_front_t);
			}
		}
	}
}

void MapCacheControl::check_load_tibia_maps(uint32_t current_x, uint32_t current_y, uint32_t current_z){
	int current_center_x = current_x / 256;
	int current_center_y = current_y / 256;

	int new_quadrant_x = (current_x - (current_center_x * 256)) / 128;
	int new_quadrant_y = (current_y - (current_center_y * 256)) / 128;


	bool has_some_change = (current_x != last_x_center || current_y != last_y_center || current_z != last_z_center
		|| last_quadrant_x != new_quadrant_x || last_quadrant_y != new_quadrant_y);

	if (!has_some_change)
		return;

	last_x_center = current_x / 256;
	last_y_center = current_y / 256;
	last_z_center = current_z;
	last_quadrant_x = new_quadrant_x;
	last_quadrant_y = new_quadrant_y;

	unload_not_arround_maps();
	unload_not_arround_smart_bot_maps();
	load_arround_maps();
	update_cached_arround_maps();
}
