#pragma once
#include "..//DepoterManager.h"
#include "Map.h"

class DepoterCore{
public:
	static int lastDestinationBackpackIndex;

	static void request_deposit_all_items();

	static void request_deposit(std::string DepotContainerId);

	static action_retval_t deposit_items(std::string DepotContainerId);
	
	static TilePtr search_depot_chest();

	static action_retval_t go_to_depot_chest(); 

	static action_retval_t deposit_items_new(std::string DepotContainerId);

	static action_retval_t move_items(ItemContainerPtr mainBackpack, std::shared_ptr<DepotContainer> depotContainer);

	static action_retval_t try_to_move_item(ItemContainerPtr mainBackpack, int destinyBackpackId, std::shared_ptr<DepotItem> item, bool use_last = false);

	static action_retval_t open_depot();

	static action_retval_t open_depot_chest();

	static action_retval_t open_depot_box(uint32_t depotId);
};

