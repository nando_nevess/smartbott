#include "Input.h"


//TODO falta checar o tipo do window aberto, com o address que aponta pro id do window aberto
//tipo floating menu eh 12

#pragma pack(push, 1)
struct raw_floating_menu_option{// internal option in tibia menu structure
	char idk[16];
	uint32_t next_address;
	uint32_t x;
	uint32_t y;
	uint32_t width;
	uint32_t heigth;
	char caption[128];
	Rect get_rect(){
		return Rect(x, y, width, heigth);
	}
};

struct raw_floating_menu{// internal tibia structure
	uint32_t current_address;//this value is not current_address, but we will use as it.
	char idk[16];
	uint32_t x;
	uint32_t y;
	uint32_t idk1;
	uint32_t idk2;
	uint32_t first_option_address;

	bool is_open();
	std::vector<std::shared_ptr<raw_floating_menu_option>> get_options();
};


class FloatingMiniMenu{
	raw_floating_menu raw;
public:

	std::shared_ptr<raw_floating_menu_option> get_option(std::string option_caption);

	bool click_attack(){ return click_at_option("Attack"); }//Attack
	bool click_follow(){ return click_at_option("Follow"); };//Follow
	bool click_copy_name(){ return click_at_option("Copy Name"); };//Copy Name
	bool click_set_outfit(){ return click_at_option("Set Outfit"); };//Set Outfit
	bool click_dismount(){ return click_at_option("Dismount"); };//Dismount
	bool click_mount(){ return click_at_option("Mount"); };//Mount
	bool click_use(){ return click_at_option("Use"); };//Use
	bool click_use_with(){ return click_at_option("Use with ..."); };//Use with ...
	bool click_browse_field(){ return click_at_option("Browse Field"); };//Browse Field
	bool click_look(){ return click_at_option("Look"); };//Look
	bool click_open(){ return click_at_option("Open"); };//Open
	bool click_open_new_window(){ return click_at_option("Open in new window"); };//Open in new window
	bool click_trade_with(){ return click_at_option("Trade with ..."); };//Trade with ...
	
	static std::shared_ptr<FloatingMiniMenu> open_at_coordinate(Coordinate& coord, int32_t timeout);
	static bool open_option_at_coordinate(Coordinate& coord, std::string option_caption, int32_t timeout);
	static bool open_browse_field_at_coordinate(Coordinate& coord, int32_t timeout = 1500){
		return open_option_at_coordinate(coord, "Browse Field", timeout);
	}	
	static bool use(Coordinate& coord, int32_t timeout = 1500){
		return open_option_at_coordinate(coord, "Use", timeout);
	}
	static bool mount(int32_t timeout = 1500){
		return open_option_at_coordinate(TibiaProcess::get_default()->character_info->get_self_coordinate(), "Mount", timeout);
	}	
	static bool dismount(int32_t timeout = 1500){
		return open_option_at_coordinate(TibiaProcess::get_default()->character_info->get_self_coordinate(), "Dismount", timeout);
	}
	
	bool click_at_option(std::string option);
};


#pragma pack(pop)