#include "Pathfinder.h"
#include <thread>
#include "HunterCore.h"
#include "Input.h"
#include "Time.h"
#include "..\\DevelopmentManager.h"
#include "Actions.h"
#include "InfoCore.h"

/*
I PUT THESES VARIABLES HERE TO AVOID HAVE TO ACCESS THEIR POINTERSIN THE CLASS
OPTIMIZATION
*/
uint32_t null_cost = 0;
int on_closed_list[map_type_t::map_total_t];
int openList[numberPeople + 1][mapWidth*mapHeight + 2]; //1 dimensional array holding ID# of open list items
int whichList[numberPeople + 1][mapWidth + 1][mapHeight + 1];  //2 dimensional array used to record whether a cell is on the open list or on the closed list.
int openX[numberPeople + 1][mapWidth*mapHeight + 2]; //1d array stores the x location of an item on the open list
int openY[numberPeople + 1][mapWidth*mapHeight + 2]; //1d array stores the y location of an item on the open list
int parentX[numberPeople + 1][mapWidth + 1][mapHeight + 1]; //2d array to store parent of each cell (x)
int parentY[numberPeople + 1][mapWidth + 1][mapHeight + 1]; //2d array to store parent of each cell (y)
int Fcost[numberPeople + 1][mapWidth*mapHeight + 2];	//1d array to store F cost of a cell on the open list
int Gcost[numberPeople + 1][mapWidth + 1][mapHeight + 1]; 	//2d array to store G cost for each cell.
int Hcost[numberPeople + 1][mapWidth*mapHeight + 2];	//1d array to store H cost of a cell on the open list

int pathLength[numberPeople + 1];     //stores length of the found path for critter
int pathLocation[numberPeople + 1];   //stores current position along the chosen path for critter		
int* pathBank[numberPeople + 1];

//Path reading variables
int pathStatus[numberPeople + 1];
int xPath[numberPeople + 1];
int yPath[numberPeople + 1];

Pathfinder::Pathfinder(){
	//if creature appears or walk in blocked coordinate this shoulb be removed from list cuzz it was invisible monster or something other reason,
	//but if creature is under this them its possible walk on another time.
	BattleList::get()->add_event(hunter_event_t::hunter_event_creature_enter_screen, [&](CreatureOnBattlePtr creature_ptr){
		remove_temporary_blocked_coord(creature_ptr->get_coordinate_monster());
	});
	std::thread( 
		[&](){

		while (true){
			Sleep(10);

			if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
				continue;

			verifier_going_steps();
		}
	}
	).detach();

	callback_reachable_only_cache_map = [](Coordinate* coord) -> bool {
		if (coord){
			if (coord->is_null()){
				return false;
			}
			std::shared_ptr<Tile> tile = gMapTilesPtr->get_tile_at(*coord);
			if (!tile)
				return false;
			if (tile->contains_furniture()){
				return false;
			}
			else if (tile->contains_brokable()){
				return false;
			}
			else if (tile->contains_creature()){
				if (tile->contains_player()){
					if (Actions::get()->move_creature(*coord))
						return true;
				}
				else if (tile->contains_monster()){
					std::map<int, CreatureOnBattlePtr> creature = BattleList::get()->get_creatures_at_coordinate(tile->coordinate);
					if (!creature.size())
						return false;
					
					std::map<int, CreatureOnBattlePtr> monsters = tile->get_monsters();
					if (monsters.size()){

						auto creature_info = HunterManager::get()->get_target_by_values(
							monsters.begin()->second->name,	monsters.begin()->second->life,
							monsters.begin()->second->get_distance());

						if (creature_info.first){
							if (!creature_info.first->get_attack_if_block_path()){
								return false;
							}
						}

						uint32_t creature_id = monsters.begin()->second->id;
						HunterActions::get()->attack_creature_id(creature_id, std::function<bool()>(), 1500);
						HunterActions::get()->add_temporary_target(creature_id, 5000,
							std::bind([](uint32_t creature_id, Coordinate creature_current_coord) -> bool {
							CreatureOnBattlePtr creature = BattleList::get()->find_creature_by_id(creature_id);
							if (!creature)
								return false;

							return creature->get_coordinate_monster() == creature_current_coord;
						}
						, creature.begin()->second->id, creature.begin()->second->get_coordinate_monster()));

						return true;
					}

				}
				else if (tile->contains_npc()){
					return false;
				}
				else if (tile->contains_summon()){
					return false;
				}
			}
		}
		return false;
	};

	std::thread([&](){path_walker(); }).detach(); 
}

void Pathfinder::set_coordinate_walkability(Coordinate coord, bool _walkable, map_type_t map, int32_t timeout){
	/*TODO*/
}

bool Pathfinder::is_reachable(Coordinate coord, map_type_t map_type, bool consider_target_rearchable){
	return PathFinderCore::get()->is_reachable(coord, map_type, consider_target_rearchable);
}

pathfinder_state Pathfinder::set_to_go_while(Coordinate coord, uint32_t consider_near, map_type_t map_type,
	pathfinder_state condition_keep, std::function<bool()> condition_function, bool consider_target_walkable, uint32_t try_count, uint32_t try_delay,
	std::function<bool()> force_arrow, uint32_t arrow_delay){

	PathfinderConditionVariablePtr callback(new PathfinderConditionVariable);
	(*(Pathfinder::get())).set_go_to(coord, callback, consider_near, map_type, condition_function, consider_target_walkable, try_count, try_delay,
		force_arrow, arrow_delay);
	pathfinder_state state = callback->get_current_state();

	do{
		state = callback->wait_state();
		if (state != condition_keep){
			callback->kill();
			return state;
		}
		Sleep(1);
	} while (callback->is_alive());

	return state;
}

void Pathfinder::cancel_all(){
	if (GetCurrentThreadId() == pathfinder_thread)
		return;

	mtx_access.lock();
	if (current_go_state){
		current_go_state->fire_callback(pathfinder_walk_canceled);
		current_go_state = 0;
	}
	mtx_access.unlock();
}

pathfinder_retval Pathfinder::walk_near_to(Coordinate coord_target, map_type_t map_type, uint32_t distance_near){
	Coordinate self_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (coord_target.z != self_coord.z)
		return pathfinder_not_reachable;

	Coordinate coord_to_go;


	if (gMapMinimapPtr->get_walkability(Coordinate(self_coord.x + 1, self_coord.y), map_type)
		&& coord_target.get_axis_max_dist(Coordinate(self_coord.x + 1, self_coord.y)) < distance_near)
		coord_to_go = Coordinate(self_coord.x + 1, self_coord.y, coord_target.z);

	else if (gMapMinimapPtr->get_walkability(Coordinate(self_coord.x - 1, self_coord.y), map_type)
		&& coord_target.get_axis_max_dist(Coordinate(self_coord.x - 1, self_coord.y)) < distance_near)
		coord_to_go = Coordinate(self_coord.x - 1, self_coord.y, coord_target.z);

	else if (gMapMinimapPtr->get_walkability(Coordinate(self_coord.x, self_coord.y + 1), map_type)
		&& coord_target.get_axis_max_dist(Coordinate(self_coord.x, self_coord.y + 1)) < distance_near)
		coord_to_go = Coordinate(self_coord.x, self_coord.y + 1, coord_target.z);

	else if (gMapMinimapPtr->get_walkability(Coordinate(self_coord.x, self_coord.y - 1), map_type)
		&& coord_target.get_axis_max_dist(Coordinate(self_coord.x, self_coord.y - 1)) < distance_near)
		coord_to_go = Coordinate(self_coord.x, self_coord.y - 1, coord_target.z);


	if (coord_to_go.is_null())
		return pathfinder_not_reachable;

	return go_with_hotkey(coord_to_go, true);
}


void Pathfinder::set_go_to(Coordinate coord_target, PathfinderConditionVariablePtr condition_callback,
	uint32_t distance_near, map_type_t map_type /*auto try use map 1 if not possible try map 2*/,
	std::function<bool()> condition_function, bool consider_target_walkable, uint32_t try_count, uint32_t try_delay
	, std::function<bool()> force_arrow, uint32_t arrow_delay){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (current_coord.z != coord_target.z){
		condition_callback->set_state(pathfinder_state::pathfinder_walk_not_reachable, true);
		return;
	}

	uint32_t distance = coord_target.get_axis_max_dist(current_coord);
	if (distance > MAX_DISTANCE_TO_GO_PATHFINDER){
		condition_callback->set_state(pathfinder_state::pathfinder_walk_not_reachable, true);
		return;
	}

	//sents to another waiting core that it has overriden
	if (current_go_state)
		current_go_state->fire_callback(pathfinder_walk_overriden);

	auto new_go_state = std::shared_ptr<CurrentPathfinderGoState>(new CurrentPathfinderGoState);
	new_go_state->try_count = try_count;
	new_go_state->try_delay = try_delay;
	new_go_state->consider_target_walkable = consider_target_walkable;
	new_go_state->condition_callback = condition_callback;
	new_go_state->near_distance = distance_near;
	new_go_state->map_type = map_type;
	new_go_state->current_destination = coord_target;
	new_go_state->state = pathfinder_walk_request_start;
	new_go_state->condition_function = condition_function;
	new_go_state->force_arrow = force_arrow;
	new_go_state->arrow_delay = arrow_delay;

	mtx_access.lock();
	current_go_state = new_go_state;
	mtx_access.unlock();
}

//TODO: Melhorar nome, Terminar metodos
bool Pathfinder::is_shotable(Coordinate pos) {
	return true;
}

pathfinder_retval Pathfinder::go_with_hotkey(Coordinate target_coord, bool consider_target_walkable, bool force_reach,
	map_type_t first_try_map, map_type_t second_try_map, bool use_diagonal){

	second_try_map = map_front_cached_editable_t;
	cancel_all();

	Coordinate original_target_coord = target_coord;
	CoordinatePtr to_where = PathFinderCore::get()->find_path_way(target_coord, first_try_map, consider_target_walkable, null_cost, use_diagonal);

	target_coord = original_target_coord;
	CoordinatePtr cached_to_where = nullptr;
	Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

	bool temp_callback = (!force_reach || !Pathfinder::get()->callback_reachable_only_cache_map);
	if (target_coord == self_coordinate)
		return pathfinder_reached;

	if (!to_where && temp_callback)
		return pathfinder_not_reachable;

	else if (!to_where){
		if (second_try_map == map_none_t)
			return pathfinder_not_reachable;

		cached_to_where = PathFinderCore::get()->find_path_way(target_coord, second_try_map, consider_target_walkable,null_cost, use_diagonal);
		if (!cached_to_where)
			return pathfinder_not_reachable;

		if (gMapMinimapPtr->walkability[first_try_map][cached_to_where->x][cached_to_where->y] != WALKABLE){
			Coordinate orientation = self_coordinate + ((*cached_to_where) - Coordinate(128, 128));

			if (!callback_reachable_only_cache_map(&orientation))
				return pathfinder_not_reachable;

			return pathfinder_reached;
		}

		to_where = cached_to_where;
	}

	/*if (!tibia_coord_to_path_coord(target_coord, self_coordinate))
	return pathfinder_not_reachable;*/

	Coordinate going = TibiaProcess::get_default()->character_info->get_going_step();
	bool is_walking = false;
	if (!going.is_null())
		is_walking = true;
	

	Coordinate orientation = Coordinate(128, 128) - (*to_where);
	if (is_walking){
		if (to_where){
			Coordinate going_to = self_coordinate - orientation;
			if (going == going_to)
				return pathfinder_reached;
		}
	}
	TimeChronometer time;
	switch (orientation.x){
	case -1:{//right
				switch (orientation.y){
				case -1:{//down
							Actions::step_side(side_t::side_south_east);
				}
					break;
				case 0:{//middle y
						   Actions::step_side(side_t::side_east);
				}
					break;
				case 1:{//up
						   Actions::step_side(side_t::side_north_east);
				}
					break;
				default:
					return pathfinder_reached;
					break;
				}
	}
		break;
	case 0:{// middle x
			   switch (orientation.y){
			   case -1:{//down
						   Actions::step_side(side_t::side_south);
			   }
				   break;
			   case 1:{//up
						  Actions::step_side(side_t::side_north);
			   }
			   default:
				   return pathfinder_reached;
				   break;
			   }
	}
		break;
	case 1:{//left
			   switch (orientation.y){
			   case -1:{//down
						   Actions::step_side(side_t::side_south_west);
			   }
				   break;
			   case 0:{//middle y
						  Actions::step_side(side_t::side_west);
			   }
				   break;
			   case 1:{//up
						  Actions::step_side(side_t::side_north_west);
			   }
			   default:
				   return pathfinder_reached;
				   break;
			   }
	}
	default:
		return pathfinder_reached;
		break;
	}

	return pathfinder_reached;
}


bool Pathfinder::must_ignore_mouse_clicks(){
	if (has_blocked_coordinates())
		return true;
	
	return false;
}

void Pathfinder::path_walker(){
	MONITOR_THREAD(__FUNCTION__);

	pathfinder_thread = GetCurrentThreadId();
	uint32_t not_reachable_count = 0;

	while (true) {
		Sleep(1);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		std::shared_ptr<CurrentPathfinderGoState> current_go_state;
		do{
			current_go_state = get_current_go_state();

			if (current_go_state && !current_go_state->finalized)
				break;

			Sleep(1);//yield avoid cpu overload.
		} while (true);

		/*
		if (current_go_state->state == pathfinder_walk_request_start)
		current_go_state->fire_callback(pathfinder_walk_going, false);
		*/

		Coordinate self_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
		Coordinate target_coordinate = current_go_state->current_destination;

		bool continue_loop = false;
		TimeChronometer time_delay_count;
		while (time_delay_count.elapsed_milliseconds() < (int32_t)current_go_state->try_delay) {
			if (self_coordinate.z != target_coordinate.z){
				current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
				continue_loop = true;
				break;
			}

			/*!TibiaProcess::get_default()->get_is_logged() || TODO FALTA CORRIGIR AF AUNCAO*/
			if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state()){
				current_go_state->fire_callback(pathfinder_walk_canceled, true);
				continue_loop = true;
				break;
			}

			if (current_go_state->current_destination.get_axis_max_dist(self_coordinate) <= current_go_state->near_distance){
				current_go_state->fire_callback(pathfinder_walk_reached, true);
				continue_loop = true;
				break;
			}

			//we can set an conditional  function that will be checked at every tries to go to our destination,
			//this can avoid infitine loops
			bool has_to_stop = !current_go_state->condition_function ? false : !current_go_state->condition_function();
			if (has_to_stop){
				current_go_state->fire_callback(pathfinder_walk_condition_has_to_stop, true);
				continue_loop = true;
				break;
			}
			Sleep(1);
		}

		if (continue_loop)
			continue;

		/*
		TibiaAutoWalkCoordinate
		*/
		
		current_go_state->map_type = current_go_state->map_type == map_type_t::map_front_t ? map_type_t::map_front_cached_t : current_go_state->map_type;
		switch (current_go_state->map_type){
		case map_back_t: {
			if (!PathFinderCore::get()->is_reachable(current_go_state->current_destination, map_back_t, current_go_state->consider_target_walkable)) {
				if (not_reachable_count >= current_go_state->try_count)
					current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
				else {
					not_reachable_count++;
					Sleep(10);
				}
			}
			else {
				TibiaAutoWalkCoordinate::get()->set_to_go(current_go_state->current_destination);
				not_reachable_count = 0;
			}
			continue;
		}
			break;
		case map_front_cached_t: {
			if (must_ignore_mouse_clicks()){
				pathfinder_retval retval = go_with_hotkey(current_go_state->current_destination,
					current_go_state->consider_target_walkable,
					true, map_front_cached_t, map_front_cached_editable_t);
				if (retval == pathfinder_not_reachable) {
					if (not_reachable_count >= current_go_state->try_count){
						current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
					}
					else {
						not_reachable_count++;
						Sleep(10);
					}
				}
				//Sleep(1000);
				not_reachable_count = 0;
				continue;
			}
			
			/* try first on map back cuzz this is best it's use tibia own pathfinder  */
			uint32_t cost_front;
			uint32_t cost_back;

			bool reachable_front = PathFinderCore::get()->is_reachable(current_go_state->current_destination,
				map_front_cached_t, current_go_state->consider_target_walkable, true, cost_front);

			bool reachable_back = PathFinderCore::get()->is_reachable(current_go_state->current_destination,
				map_back_t, current_go_state->consider_target_walkable, true, cost_back);

			if ((!current_go_state->force_arrow || !current_go_state->force_arrow()) && reachable_front && reachable_back){
				if (!cost_back || !cost_front)
					continue;
				
				if (cost_back <= cost_front){
					TibiaAutoWalkCoordinate::get()->set_to_go(current_go_state->current_destination);
					not_reachable_count = 0;
				}
				else {
					pathfinder_retval retval = go_with_hotkey(current_go_state->current_destination, current_go_state->consider_target_walkable, true, map_front_cached_t);
					if (retval == pathfinder_not_reachable) {
						if (not_reachable_count >= current_go_state->try_count)
							current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
						else {
							not_reachable_count++;
							Sleep(10);
						}
					}
					//Sleep(1000);
					not_reachable_count = 0;
					/*
					float each = (float)cost_back / 100.0f;
					float dif = (float)(cost_back - cost_front) / each;

					if (dif > 10){//se for maior que 10%

					pathfinder_retval retval = go_with_hotkey(current_go_state->current_destination, map_front_t, current_go_state->consider_target_walkable);
					if (retval == pathfinder_not_reachable) {
					if (not_reachable_count >= current_go_state->try_count)
					current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
					else
					not_reachable_count++;
					}
					not_reachable_count = 0;
					}
					else{

					TibiaAutoWalkCoordinate::get()->set_to_go(current_go_state->current_destination);
					not_reachable_count = 0;
					}
					*/
				}
			}
			else if ((!current_go_state->force_arrow || !current_go_state->force_arrow()) && reachable_back) {
				TibiaAutoWalkCoordinate::get()->set_to_go(current_go_state->current_destination);
				not_reachable_count = 0;
			}
			else if (reachable_front) {

				pathfinder_retval retval = go_with_hotkey(current_go_state->current_destination,
					current_go_state->consider_target_walkable);
				if (retval == pathfinder_not_reachable) {
					if (not_reachable_count >= current_go_state->try_count)
						current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
					else {
						not_reachable_count++;
						Sleep(50);
						if (current_go_state->force_arrow && current_go_state->force_arrow()){
							if (!current_go_state->arrow_delay){
								time_delay_count.wait_complete_delay(
									TibiaProcess::get_default()->get_action_wait_delay(1.5));
							}
							else{
								time_delay_count.wait_complete_delay(current_go_state->arrow_delay);
							}
						}
					}
				}
				else if (current_go_state->force_arrow && current_go_state->force_arrow()){
					if (!current_go_state->arrow_delay){
						time_delay_count.wait_complete_delay(
							TibiaProcess::get_default()->get_action_wait_delay(1.5));
					}
					else{
						time_delay_count.wait_complete_delay(current_go_state->arrow_delay);
					}
				}
				//Sleep(1000);
				not_reachable_count = 0;
			}
			else {
				pathfinder_retval retval = go_with_hotkey(current_go_state->current_destination, current_go_state->consider_target_walkable);

				if (retval == pathfinder_not_reachable) {
					if (not_reachable_count >= current_go_state->try_count)
						current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
					else {
						not_reachable_count++;
						Sleep(50);
						if (current_go_state->force_arrow)
							time_delay_count.wait_complete_delay(current_go_state->arrow_delay);
					}
				}
				else if (current_go_state->force_arrow){

					time_delay_count.wait_complete_delay(current_go_state->arrow_delay);
				}

				//Sleep(1000);
			}
			continue;
		}
			break;
		case map_back_mini_t:{
								 if (!PathFinderCore::get()->is_reachable(current_go_state->current_destination, map_back_mini_t, current_go_state->consider_target_walkable)){
									 if (not_reachable_count >= current_go_state->try_count) {
										 current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
										 continue;
									 }
									 else {
										 not_reachable_count++;
										 Sleep(50);
									 }
								 }
								 TibiaAutoWalkCoordinate::get()->set_to_go(current_go_state->current_destination);
								 not_reachable_count = 0;
		}
			break;
		case map_front_mini_t:{

								  map_type_t m_type = map_front_clear_minimap_t;
								  if (must_ignore_mouse_clicks())
									  m_type = map_front_mini_t;
								  
								  if (!PathFinderCore::get()->is_reachable(current_go_state->current_destination, m_type, current_go_state->consider_target_walkable)){
									  if (not_reachable_count >= current_go_state->try_count) {
										  current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
										  continue;
									  }
									  else {
										  not_reachable_count++;
										  Sleep(50);
									  }
								  }

								  pathfinder_retval retval = go_with_hotkey(current_go_state->current_destination, current_go_state->consider_target_walkable);
								  if (retval == pathfinder_not_reachable)
								  if (not_reachable_count >= current_go_state->try_count)
									  current_go_state->fire_callback(pathfinder_walk_not_reachable, true);
								  else {
									  not_reachable_count++;
									  Sleep(50);
								  }
		}
		}

	}
}

TibiaAutoWalkCoordinate::TibiaAutoWalkCoordinate(){
	x = 0; y = 0; z = 0; state = false;
	last_set_state = boost::chrono::steady_clock::now();
}

bool TibiaAutoWalkCoordinate::get_state(){
	lock_guard _lock(mtx_access);
	return (state &&
		(boost::chrono::duration_cast<boost::chrono::milliseconds>(boost::chrono::steady_clock::now() - last_set_state)).count() > 0);
}

void TibiaAutoWalkCoordinate::set_to_go(Coordinate coord, bool state, int32_t timeout){
	lock_guard _lock(mtx_access);
	uint32_t addressXGO = AddressManager::get()->getAddress(ADDRESS_X_GO);
	uint32_t addressYGO = AddressManager::get()->getAddress(ADDRESS_Y_GO);
	uint32_t addressZGO = AddressManager::get()->getAddress(ADDRESS_Z_GO);

	TibiaProcess::get_default()->write_int(TibiaProcess::get_default()->base_address + addressXGO, coord.x);
	TibiaProcess::get_default()->write_int(TibiaProcess::get_default()->base_address + addressYGO, coord.y);
	TibiaProcess::get_default()->write_int(TibiaProcess::get_default()->base_address + addressZGO, coord.z);

	set_state(true);
	last_set_state = boost::chrono::steady_clock::now();
}

void TibiaAutoWalkCoordinate::set_state(bool state){
	lock_guard _lock(mtx_access);
	this->state = state;
	uint32_t current_player_id = TibiaProcess::get_default()->character_info->character_id();
	CreatureOnBattle curr_character;
	uint32_t address_of_walk_state = 0;
	for (int i = 0; i < 100; i++){
		address_of_walk_state = AddressManager::get()->getAddress(ADDRESS_BATTLE_FIRST_ID)
			+ (i * sizeof(CreatureOnBattle));
		if (TibiaProcess::get_default()->read_memory_block(AddressManager::get()->getAddress(ADDRESS_BATTLE_FIRST_ID)
			+ (i * sizeof(CreatureOnBattle)), &curr_character, sizeof(CreatureOnBattle)) != sizeof(CreatureOnBattle))
			return;
		if (curr_character.id == current_player_id)
			break;
	}
	if (curr_character.id != current_player_id)
		return;
	TibiaProcess::get_default()->write_int(TibiaProcess::get_default()->base_address + address_of_walk_state + 80, (uint32_t)state);
}

#pragma region PathFinderCore

PathFinderCore::PathFinderCore(){
	for (int i = 0; i < map_type_t::map_total_t; i++)
		on_closed_list[i] = 100;


	for (int x = 0; x < numberPeople + 1; x++)
		pathBank[x] = (int*)malloc(4);
}

PathFinderCore::~PathFinderCore(){
	for (int x = 0; x < numberPeople + 1; x++)
		free(pathBank[x]);
}

int PathFinderCore::find_path(map_type_t map_type, int _x_target, int _y_target, bool get_reverse_way,
	bool consider_target_coordinate_walkable,bool use_diagonal){
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (abs(_x_target - current_coord.x) > MAX_DISTANCE_TO_GO_PATHFINDER
		|| abs(_y_target - current_coord.y) > MAX_DISTANCE_TO_GO_PATHFINDER){
		return nonexistent;
	}

	if (!Pathfinder::tibia_coord_to_path_coord(_x_target, _y_target, current_coord)){
		return nonexistent;
	}

	int onOpenList = 0, parentXval = 0, parentYval = 0,
		a = 0, b = 0, m = 0, u = 0, v = 0, temp = 0, corner = 0, numberOfOpenListItems = 0,
		addedGCost = 0, tempGcost = 0, path = 0,
		tempx, pathX, pathY,
		newOpenListitem_id = 0;
	unsigned int cellPosition = 0;

	int startX = startingX;
	int startY = startingY;

	if (current_coord.x == _x_target && current_coord.y == _y_target)
		return found;

	//3.Reset some variables that need to be cleared
	if (on_closed_list[map_type] > 1000000){
		for (int x = 0; x < mapWidth; x++) {
			for (int y = 0; y < mapHeight; y++)
				whichList[map_type][x][y] = 0;
		}
		on_closed_list[map_type] = 10;
	}

	on_closed_list[map_type] = on_closed_list[map_type] + 2; //changing the values of onOpenList and onClosed list is faster than redimming whichList() array
	onOpenList = on_closed_list[map_type] - 1;
	pathLength[map_type] = notStarted;//i.e, = 0
	pathLocation[map_type] = notStarted;//i.e, = 0
	Gcost[map_type][startX][startY] = 0; //reset starting square's G value to 0

	//4.Add the starting location to the open list of squares to be checked.
	numberOfOpenListItems = 1;
	openList[map_type][1] = 1;//assign it as the top (and currently only) item in the open list, which is maintained as a binary heap (explained below)
	openX[map_type][1] = startX; openY[map_type][1] = startY;

	//5.Do the following until a path is found or deemed nonexistent.
	do{
		//6.If the open list is not empty, take the first cell off of the list.
		//	This is the lowest F cost cell on the open list.
		if (numberOfOpenListItems != 0)
		{

			//7. Pop the first item off the open list.
			parentXval = openX[map_type][openList[map_type][1]];
			parentYval = openY[map_type][openList[map_type][1]]; //record cell coordinates of the item
			whichList[map_type][parentXval][parentYval] = on_closed_list[map_type];//add the item to the closed list

			//	Open List = Binary Heap: Delete this item from the open list, which
			//  is maintained as a binary heap. For more information on binary heaps, see:
			//	http://www.policyalmanac.org/games/binaryHeaps.htm
			numberOfOpenListItems = numberOfOpenListItems - 1;//reduce number of open list items by 1	

			//	Delete the top item in binary heap and reorder the heap, with the lowest F cost item rising to the top.
			openList[map_type][1] = openList[map_type][numberOfOpenListItems + 1];//move the last item in the heap up to slot #1
			v = 1;

			//	Repeat the following until the new item in slot #1 sinks to its proper spot in the heap.
			do
			{
				u = v;
				if (2 * u + 1 <= numberOfOpenListItems) //if both children exist
				{
					//Check if the F cost of the parent is greater than each child.
					//Select the lowest of the two children.
					if (Fcost[map_type][openList[map_type][u]] >= Fcost[map_type][openList[map_type][2 * u]])
						v = 2 * u;
					if (Fcost[map_type][openList[map_type][v]] >= Fcost[map_type][openList[map_type][2 * u + 1]])
						v = 2 * u + 1;
				}
				else
				{
					if (2 * u <= numberOfOpenListItems) //if only child #1 exists
					{
						//Check if the F cost of the parent is greater than child #1	
						if (Fcost[map_type][openList[map_type][u]] >= Fcost[map_type][openList[map_type][2 * u]])
							v = 2 * u;
					}
				}

				if (u != v) //if parent's F is > one of its children, swap them
				{
					temp = openList[map_type][u];
					openList[map_type][u] = openList[map_type][v];
					openList[map_type][v] = temp;
				}
				else
					break; //otherwise, exit loop

			} while (true);//reorder the binary heap


			//7.Check the adjacent squares. (Its "children" -- these path children
			//	are similar, conceptually, to the binary heap children mentioned
			//	above, but don't confuse them. They are different. Path children
			//	are portrayed in Demo 1 with grey pointers pointing toward
			//	their parents.) Add these adjacent child squares to the open list
			//	for later consideration if appropriate (see various if statements
			//	below).
			for (b = parentYval - 1; b <= parentYval + 1; b++){
				for (a = parentXval - 1; a <= parentXval + 1; a++){

					//	If not off the map (do this first to avoid array out-of-bounds errors)
					if (a != -1 && b != -1 && a != mapWidth && b != mapHeight){

						//	If not already on the closed list (items on the closed list have
						//	already been considered and can now be ignored).			
						if (whichList[map_type][a][b] != on_closed_list[map_type]) {
							//	If not a wall/obstacle square.
							//
							if (MapMinimap::walkability[map_type][a][b] != UNWALKABLE ||
								(consider_target_coordinate_walkable && _x_target == a && _y_target == b)) {
								//corner = WALKABLE;
								//if (corner == WALKABLE) {

								int result_cost_normal = 9 + (int)MapMinimap::walkability[map_type][a][b];
								double result_diagonal = result_cost_normal*1.4;
								double result_diagonal_2 = result_cost_normal*2.2;

								if (result_cost_normal > 254)
									result_cost_normal = 254;

								if (result_diagonal > 254)
									result_diagonal = 254;

								if (result_diagonal_2 > 254)
									result_diagonal_2 = 254;

								if (ConfigPathManager::get()->get_use_diagonal() || use_diagonal){
									result_diagonal = 10;
									result_diagonal_2 = 10;
									result_cost_normal = 10;
								}

								//Figure out the G cost of this possible new path
								//	If not already on the open list, add it to the open list.			
								if (whichList[map_type][a][b] != onOpenList)
								{

									//Create a new open list item in the binary heap.
									newOpenListitem_id = newOpenListitem_id + 1; //each new item has a unique ID #
									m = numberOfOpenListItems + 1;
									openList[map_type][m] = newOpenListitem_id;//place the new open list item (actually, its ID#) at the bottom of the heap
									openX[map_type][newOpenListitem_id] = a;
									openY[map_type][newOpenListitem_id] = b;//record the x and y coordinates of the new item

									//Figure out its G cost
									if (abs(a - parentXval) == 1 && abs(b - parentYval) == 1)
										addedGCost = (int)result_diagonal_2;//cost of going to diagonal squares	
									else
										addedGCost = result_cost_normal;//cost of going to non-diagonal squares				
									Gcost[map_type][a][b] = Gcost[map_type][parentXval][parentYval] + addedGCost;

									//Figure out its H and F costs and parent
									Hcost[map_type][openList[map_type][m]] = 10 * (abs(a - _x_target) + abs(b - _y_target));
									Fcost[map_type][openList[map_type][m]] = Gcost[map_type][a][b] + Hcost[map_type][openList[map_type][m]];
									parentX[map_type][a][b] = parentXval; parentY[map_type][a][b] = parentYval;

									//Move the new open list item to the proper place in the binary heap.
									//Starting at the bottom, successively compare to parent items,
									//swapping as needed until the item finds its place in the heap
									//or bubbles all the way to the top (if it has the lowest F cost).
									while (m != 1) //While item hasn't bubbled to the top (m=1)	
									{
										//Check if child's F cost is < parent's F cost. If so, swap them.	
										if (Fcost[map_type][openList[map_type][m]] <= Fcost[map_type][openList[map_type][m / 2]])
										{
											temp = openList[map_type][m / 2];
											openList[map_type][m / 2] = openList[map_type][m];
											openList[map_type][m] = temp;
											m = m / 2;
										}
										else
											break;
									}
									numberOfOpenListItems = numberOfOpenListItems + 1;//add one to the number of items in the heap

									//Change whichList to show that the new item is on the open list.
									whichList[map_type][a][b] = onOpenList;
								}

								//8.If adjacent cell is already on the open list, check to see if this 
								//	path to that cell from the starting location is a better one. 
								//	If so, change the parent of the cell and its G and F costs.	
								else //If whichList(a,b) = onOpenList
								{

									//Figure out the G cost of this possible new path
									if (abs(a - parentXval) == 1 && abs(b - parentYval) == 1)
										addedGCost = (int)result_diagonal;//cost of going to diagonal tiles	
									else
										addedGCost = result_cost_normal;//cost of going to non-diagonal tiles				
									tempGcost = Gcost[map_type][parentXval][parentYval] + addedGCost;

									//If this path is shorter (G cost is lower) then change
									//the parent cell, G cost and F cost. 		
									if (tempGcost < Gcost[map_type][a][b]) //if G cost is less,
									{
										parentX[map_type][a][b] = parentXval; //change the square's parent
										parentY[map_type][a][b] = parentYval;
										Gcost[map_type][a][b] = tempGcost;//change the G cost			

										//Because changing the G cost also changes the F cost, if
										//the item is on the open list we need to change the item's
										//recorded F cost and its position on the open list to make
										//sure that we maintain a properly ordered open list.
										for (int x = 1; x <= numberOfOpenListItems; x++) //look for the item in the heap
										{
											if (openX[map_type][openList[map_type][x]] == a && openY[map_type][openList[map_type][x]] == b) //item found
											{
												Fcost[map_type][openList[map_type][x]] = Gcost[map_type][a][b] + Hcost[map_type][openList[map_type][x]];//change the F cost

												//See if changing the F score bubbles the item up from it's current location in the heap
												m = x;
												while (m != 1) //While item hasn't bubbled to the top (m=1)	
												{
													//Check if child is < parent. If so, swap them.	
													if (Fcost[map_type][openList[map_type][m]] < Fcost[map_type][openList[map_type][m / 2]])
													{
														temp = openList[map_type][m / 2];
														openList[map_type][m / 2] = openList[map_type][m];
														openList[map_type][m] = temp;
														m = m / 2;
													}
													else
														break;
												}
												break; //exit for x = loop
											} //If openX(openList(x)) = a
										} //For x = 1 To numberOfOpenListItems
									}//If tempGcost < Gcost(a,b)

								}//else If whichList(a,b) = onOpenList	
								//}//If not cutting a corner
							}//If not a wall/obstacle square.
						}//If not already on the closed list 
					}//If not off the map
				}//for (a = parentXval-1; a <= parentXval+1; a++){
			}//for (b = parentYval-1; b <= parentYval+1; b++){

		}//if (numberOfOpenListItems != 0)

		//9.If open list is empty then there is no path.	
		else{
			path = nonexistent; 
			break;
		}

		//If target is added to open list then path has been found.
		if (whichList[map_type][_x_target][_y_target] == onOpenList){
			path = found;
			break;
		}

	} while (1);//Do until path is found or deemed nonexistent


	if (!get_reverse_way){
		return path;
	}

	//10.Save the path if it exists.
	if (path == found){
		//a.Working backwards from the target to the starting location by checking
		//	each cell's parent, figure out the length of the path.
		pathX = _x_target;
		pathY = _y_target;
		do{
			//Look up the parent of the current cell.	
			tempx = parentX[map_type][pathX][pathY];
			pathY = parentY[map_type][pathX][pathY];
			pathX = tempx;

			//Figure out the path length
			pathLength[map_type] = pathLength[map_type] + 1;
		} while (pathX != startX || pathY != startY);

		//b.Resize the data bank to the right size in bytes
		pathBank[map_type] = (int*)realloc(pathBank[map_type],
			pathLength[map_type] * 8);

		//c. Now copy the path information over to the databank. Since we are
		//	working backwards from the target to the start location, we copy
		//	the information to the data bank in reverse order. The result is
		//	a properly ordered set of path data, from the first step to the
		//	last.
		pathX = _x_target; pathY = _y_target;
		cellPosition = pathLength[map_type] * 2;//start at the end	
		do{
			cellPosition = cellPosition - 2;//work backwards 2 integers
			pathBank[map_type][cellPosition] = pathX;
			pathBank[map_type][cellPosition + 1] = pathY;

			//d.Look up the parent of the current cell.	
			tempx = parentX[map_type][pathX][pathY];
			pathY = parentY[map_type][pathX][pathY];
			pathX = tempx;

			//e.If we have reached the starting square, exit the loop.	
		} while (pathX != startX || pathY != startY);

		//11.Read the first path step into xPath/yPath arrays
		read_path(startingX, startingY, map_type);
	}

	return path;
	//13.If there is no path to the selected target, set the pathfinder's
	//	xPath and yPath equal to its current location and return that the
	//	path is nonexistent.
	//	noPath:
	/*xPath[map_type] = startingX;
	yPath[map_type] = startingY;
	return nonexistent;*/
}

void PathFinderCore::read_path(int x, int y, map_type_t map_type){
	int ID = map_type;
	if (pathStatus[ID] == found){

		if (pathLocation[ID] < pathLength[ID]){
			if (pathLocation[ID] == 0 ||
				(abs(x - xPath[ID]) < pixelsPerFrame && abs(y - yPath[ID]) < pixelsPerFrame))
				pathLocation[ID] = pathLocation[ID] + 1;
		}

		xPath[ID] = read_path_x(map_type, pathLocation[ID]);
		yPath[ID] = read_path_y(map_type, pathLocation[ID]);

		if (pathLocation[ID] == pathLength[ID]){
			if (abs(x - xPath[ID]) < pixelsPerFrame
				&& abs(x - yPath[ID]) < pixelsPerFrame) //if close enough to center of square
				pathStatus[ID] = notStarted;
		}
	}
	else{
		xPath[ID] = x;
		yPath[ID] = y;
	}
}

int PathFinderCore::read_path_x(map_type_t map_type, int pathLocation){
	int x;
	if (pathLocation <= pathLength[map_type]){
		x = pathBank[map_type][pathLocation * 2 - 2];
		x = static_cast<int>(tileSize*x + .5*tileSize);
	}
	return x;
}

int PathFinderCore::read_path_y(map_type_t map_type, int pathLocation){
	int y;
	if (pathLocation <= pathLength[map_type]){
		y = pathBank[map_type][pathLocation * 2 - 1];
		y = static_cast<int>(static_cast<double>(tileSize*y + .5*tileSize));
	}
	return y;
}

bool PathFinderCore::is_reachable(Coordinate& coord, map_type_t map_type, bool consider_target_rearchable, bool get_cost, uint32_t& cost){
	lock_guard _lock(mtx_access[map_type]);
	int res = find_path(map_type, coord.x, coord.y, get_cost, consider_target_rearchable) == found;
	if (res) {
		if (get_cost){
			cost = pathLength[map_type];
		}
		return true;
	}
	return false;
}

CoordinatePtr PathFinderCore::find_path_way(Coordinate& coord, map_type_t map_type, bool consider_target_walkable,
	uint32_t& cost,bool use_diagonal){
	lock_guard _lock(mtx_access[map_type]);
	CoordinatePtr sucess = nullptr;
	Coordinate temp_coordinate = coord;
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();

	if (!Pathfinder::tibia_coord_to_path_coord(temp_coordinate, current_coordinate))
		return false;

	pathStatus[map_type] = find_path(map_type, coord.x, coord.y, true, consider_target_walkable, use_diagonal);

	if (pathStatus[map_type] == found){
		cost = pathLength[map_type];

		read_path(128, 128, map_type);

		coord.x = xPath[map_type];
		coord.y = yPath[map_type];

		if (map_type == map_back_mini_t || map_type == map_front_mini_t){
			int dist_to_go_x = abs(temp_coordinate.x - coord.x);
			int dist_to_go_y = abs(temp_coordinate.x - coord.y);
			
			if (dist_to_go_x > 7 || dist_to_go_y > 5)
				return false;
		}
		sucess = CoordinatePtr(new Coordinate(coord.x, coord.y));
	}
	return sucess;
}



Coordinate Pathfinder::get_nearest_coordinate_from_another(Coordinate target, map_type_t map_type,
	bool reachable_from_us, bool reachable_from_dest,
	uint32_t range, bool most_near_from_center, bool ignore_target){
	Coordinate retval;
	//Coordinate retval_new;
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();

	uint32_t max_cost = INT32_MAX;
	uint32_t max_cost_pathfinder = INT32_MAX;


	for (int x = -((int)range); x <= (int)range; x++){
		for (int y = -((int)range); y <= (int)range; y++){
			if (ignore_target && (x == 0 && y == 0)){
				continue;
			}
			Coordinate real_coord = target + Coordinate(x, y);
			Coordinate cost = (current_coord - real_coord).get_abs_coord();
			//uint32_t final_cost = cost.x + cost.y;

			//if (final_cost < (max_cost + 3)){
			uint32_t current_pathfinder_cost;

			if (reachable_from_dest && !PathFinderCore::get()->is_reachable(real_coord, map_type, false, true, current_pathfinder_cost))
				continue;
			if (reachable_from_dest)
			if (current_pathfinder_cost >= max_cost_pathfinder)
				continue;
			max_cost_pathfinder = current_pathfinder_cost;
			retval = real_coord;
			//	max_cost = final_cost;
			//}
		}
	}

	if (!retval.is_null()){
		retval.z = current_coord.z;
	}
	/*if (retval.z != 0)
	retval.z = current_coord.z;*/
	/*else{
	range += 1;

	if (range >= 4)
	return retval;

	retval_new = get_nearest_coordinate_from_another(target, map_type, reachable_from_us, reachable_from_dest, range);
	retval = retval_new;
	}*/

	return retval;
}


bool Pathfinder::wait_reachability(Coordinate target, int32_t timeout, map_type_t map_type,
	uint32_t check_delay_ms, bool consider_target_rearchable){
	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < timeout){
		if (is_reachable(target, map_type, consider_target_rearchable))
			return true;
		Sleep(check_delay_ms);
	}
	return false;
}

PathFinderCore* PathFinderCore::get(){
	static PathFinderCore* mPathFinderCore = nullptr;
	if (!mPathFinderCore)
		mPathFinderCore = new PathFinderCore;
	return mPathFinderCore;
}


void TemporaryCoordinate::reset(){
	registered_time.reset();
}

bool TemporaryCoordinate::has_expired(){
	return registered_time.elapsed_milliseconds() > (int32_t)timeout;
}

void Pathfinder::remove_temporary_blocked_coord(Coordinate& coord){
	temporary_coords_lock.lock();
	for (auto it = temporary_blocked_coordinates.begin(); it != temporary_blocked_coordinates.end();){
		if (it->coord == coord){
			it = temporary_blocked_coordinates.erase(it);
		}
		else
			it++;
	}
	has_blocked_coord = temporary_blocked_coordinates.size() != 0;
	temporary_coords_lock.unlock();
}

void Pathfinder::check_remove_expired_temporary_coordinates(){
	for (auto it = temporary_blocked_coordinates.begin(); it != temporary_blocked_coordinates.end();){
		if (it->has_expired()){
			it = temporary_blocked_coordinates.erase(it);
		}
		else
			it++;
	}
	has_blocked_coord = temporary_blocked_coordinates.size() != 0;
}

void Pathfinder::add_temporary_blocked_coord(Coordinate& coord){
	temporary_coords_lock.lock();
	has_blocked_coord = true;
	for (auto it : temporary_blocked_coordinates){
		if (it.coord == coord){
			it.reset();
			temporary_coords_lock.unlock();
			return;
		}
	}

	TemporaryCoordinate temp_coord;
	temp_coord.coord = coord;
	temp_coord.timeout = 10000;
	temporary_blocked_coordinates.push_back(temp_coord);
	temporary_coords_lock.unlock();
}

void Pathfinder::verifier_going_steps(){


	static Coordinate current_going;
	static TimeChronometer last_going_match;
	static TimeChronometer last_change_going;
	static uint64_t count_of_not_possible = 0;

	auto going = TibiaProcess::get_default()->character_info->get_going_step();
	if (!going.is_null()){
		if (going != current_going){
			last_change_going.reset();
			last_going_match.reset();
			current_going = going;
			count_of_not_possible = InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_sorry_not_possible)->count;
		}
		else {
			last_going_match.reset();
		}

		if (last_change_going.elapsed_milliseconds() > 10000)
			add_temporary_blocked_coord(going);
		else if ((InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_sorry_not_possible)->count - count_of_not_possible) > 4)
			add_temporary_blocked_coord(going);

	}
	else{
		if (last_going_match.elapsed_milliseconds() > 5000){
			current_going = Coordinate();
		}
	}
}

bool Pathfinder::has_blocked_coordinates(){
	return has_blocked_coord;
}

std::vector<Coordinate> Pathfinder::get_blocked_coordinates(){
	temporary_coords_lock.lock();
	check_remove_expired_temporary_coordinates();
	std::vector<Coordinate> retval;
	if (!has_blocked_coordinates()){
		temporary_coords_lock.unlock();
		return retval;
	}

	for (auto it : temporary_blocked_coordinates)
		retval.push_back(it.coord);

	temporary_coords_lock.unlock();
	return retval;
}


#pragma endregion