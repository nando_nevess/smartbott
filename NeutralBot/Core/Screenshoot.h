#pragma once
#include "Interface.h"
#include "TibiaProcess.h"

class ScreenShoot{
	void CreateScreenshot(LPCWSTR filename);
	inline int GetFilePointer(HANDLE FileHandle);
	bool SaveBMPFile(LPCWSTR filename, HBITMAP bitmap, HDC bitmapDC, int width, int height);
	bool ScreenCapture(int x, int y, int width, int height, LPCWSTR filename);

public:
	int last_level = 0;
	void take_screen_shot(std::string path = "");

	static ScreenShoot* get(){
		static ScreenShoot* mScreenShoot = nullptr;
		if (!mScreenShoot) 
			mScreenShoot = new ScreenShoot;
		return mScreenShoot;
	}
};



