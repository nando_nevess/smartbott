#include "Inventory.h"
#include "TibiaProcess.h"
#include "ContainerManager.h"
#include "Input.h"
#include "Interface.h"
#include "Actions.h"

int Inventory::body_helmet(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_HELMET));
}

int Inventory::body_amulet(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_AMULET));
}

int Inventory::body_bag(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_BAG));
}

int Inventory::body_armor(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_ARMOR));
}

int Inventory::body_shield(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_SHIELD));
}

int Inventory::body_weapon(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_WEAPON));
}

int Inventory::body_leg(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_LEG));
}

int Inventory::body_boot(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_BOOTS));
}

int Inventory::body_ring(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_RING));
}

int Inventory::body_rope(){
	return TibiaProcess::get_default()->read_int(
		AddressManager::get()->getAddress(ADDRESS_ROPE));
}

int Inventory::item_count(int id){
	int AddressHelmet_temp = AddressManager::get()->getAddress(ADDRESS_HELMET);
	int total_qty = 0;
	for (int i = 0; i < 10; i++)
	{
		int act_id = TibiaProcess::get_default()->read_int(AddressHelmet_temp);
		if (act_id == id)
		{
			int qty = TibiaProcess::get_default()->read_int(AddressHelmet_temp - offset_qty_body_items);
			if (qty == 0)
			{
				total_qty++;
			}
			else
			{
				total_qty += qty;
			}
		}
		AddressHelmet_temp -= offset_beetwen_body_item;
	}
	return total_qty;
}

int Inventory::weapon_count(){
	if (TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WEAPON)) == 0)
		return 0;

	int qty = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_WEAPON) - offset_qty_body_items);

	if (qty == 0)
		return 1;
	else
		return qty;

	return 0;
}

Coordinate Inventory::get_slot_type_interface_coordinate(inventory_slot_t type){
	auto equipment = Interface->getEquipmentChildMaximized();
	
	if (!equipment)
		return Coordinate();

	auto rect = equipment->get_rect_on_client_view();

	Coordinate retval;
	switch (type){
	case slot_helmet:
		retval = COORDINATES::COORDINATE_HELMET;
		break;
	case slot_amulet:
		retval = COORDINATES::COORDINATE_AMULET;
		break;
	case slot_bag:
		retval = COORDINATES::COORDINATE_BAG;
		break;
	case slot_armor:
		retval = COORDINATES::COORDINATE_ARMOR;
		break;
	case slot_shield:
		retval = COORDINATES::COORDINATE_SHIELD;
		break;
	case slot_weapon:
		retval = COORDINATES::COORDINATE_WEAPON;
		break;
	case slot_leg:
		retval = COORDINATES::COORDINATE_LEG;
		break;
	case slot_boots:
		retval = COORDINATES::COORDINATE_BOOTS;
		break;
	case slot_ring:
		retval = COORDINATES::COORDINATE_RING;
		break;
	case slot_rope:
		retval = COORDINATES::COORDINATE_ROPE;
		break;
	}

	return retval + Coordinate(rect.get_x(), rect.get_y());
}

uint32_t Inventory::get_item_id_by_slot_type(inventory_slot_t type){
	uint32_t id = UINT32_MAX;
	switch (type){
	case slot_helmet:
		id = body_helmet();
		break;
	case slot_amulet:
		id = body_amulet();
		break;
	case slot_bag:
		id = body_bag();
		break;
	case slot_armor:
		id = body_armor();
		break;
	case slot_shield:
		id = body_shield();
		break;
	case slot_weapon:
		id = body_weapon();
		break;
	case slot_leg:
		id = body_leg();
		break;
	case slot_boots:
		id = body_boot();
		break;
	case slot_ring:
		id = body_ring();
		break;
	case slot_rope:
		id = body_rope();
		break;
	}
	return id;
}

inventory_slot_t Inventory::get_type_from_str(std::string str_type){
	if (string_util::string_compare(str_type, "ring", true))return (slot_ring);
	else if (string_util::string_compare(str_type, "boots", true))return (slot_boots);
	else if (string_util::string_compare(str_type, "helmet", true))return (slot_helmet);
	else if (string_util::string_compare(str_type, "weapon", true))return (slot_weapon);
	else if (string_util::string_compare(str_type, "amulet", true))return (slot_amulet);
	else if (string_util::string_compare(str_type, "amunation", true))return (slot_rope);
	else if (string_util::string_compare(str_type, "armor", true))return (slot_armor);
	else if (string_util::string_compare(str_type, "shield", true))return (slot_shield);
	else if (string_util::string_compare(str_type, "bag", true))return (slot_bag);
	else if (string_util::string_compare(str_type, "leg", true))return (slot_leg);
	else if (string_util::string_compare(str_type, "ammunation", true))return (slot_rope);
	else if (string_util::string_compare(str_type, "rope", true))return (slot_rope);
	return inventory_slot_t::slot_none;
}

Coordinate Inventory::get_slot_type_interface_coordinate(std::string str_type){
	Coordinate temp;
	auto slot = get_type_from_str(str_type);
	if (slot == inventory_slot_t::slot_none)
		return temp;
	return get_slot_type_interface_coordinate(slot);
}

bool Inventory::maximize(){
	Coordinate client_post = ClientGeneralInterface::get()->get_client_dimension();
	auto equipment = Interface->getEquipmentChildMinimized();
	if (!equipment)
		return true;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	for (int i = 0; i < 3; i++){
		if (!is_maximized()) {
			int x_location = client_post.x - COORDINATES::COORDINATE_BUTTON_MAXIMIZE_BODY.X;
			int y_location = equipment->y + COORDINATES::COORDINATE_BUTTON_MAXIMIZE_BODY.Y;
			
			if (!input->can_use_input())
				return false;

			input->click_left(x_location, y_location);
			Sleep(50);
		}
		else
			return true;
	}

	return false;
}

bool Inventory::minimize(){
	Coordinate client_post = ClientGeneralInterface::get()->get_client_dimension();
	auto equipment = Interface->getEquipmentChildMaximized();
	if (!equipment)
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	for (int i = 0; i < 3; i++){
		if (is_maximized()){
			int x_location = client_post.x - COORDINATES::COORDINATE_BUTTON_MAXIMIZE_BODY.X;
			int y_location = equipment->y + COORDINATES::COORDINATE_BUTTON_MAXIMIZE_BODY.Y;
			
			if (!input->can_use_input())
				return false;

			input->click_left(x_location, y_location);
			Sleep(100);
		}
		else
			return true;
	}
	return false;
}

bool Inventory::look_at_inventory(std::string str_type){
	if (!is_maximized()){
		if (!maximize())
			return false;
	}

	Coordinate location_to_look = get_slot_type_interface_coordinate(str_type);
	if (location_to_look.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->look_pixel(location_to_look.to_point());
	return true;
}

bool Inventory::look_at_inventory(inventory_slot_t type){
	if (!is_maximized()){
		if (!maximize())
			return false;
	}

	Coordinate location_to_look = get_slot_type_interface_coordinate(type);
	if (location_to_look.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->look_pixel(location_to_look.to_point());
	return true;
}

bool Inventory::use_at_inventory(inventory_slot_t slot_type){
	if (!is_maximized()){
		if (!maximize())
			return false;
	}
	Coordinate use_coordinate = get_slot_type_interface_coordinate(slot_type);
	if (use_coordinate.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->click_right(use_coordinate.to_point());
	return true;
}

Json::Value Inventory::parse_class_to_json(){
	Json::Value playerInventory;

	playerInventory["body_helmet"] = body_helmet();
	playerInventory["body_amulet"] = body_amulet();
	playerInventory["body_bag"] = body_bag();
	playerInventory["body_armor"] = body_armor();
	playerInventory["body_shield"] = body_shield();
	playerInventory["body_weapon"] = body_weapon();
	playerInventory["body_leg"] = body_leg();
	playerInventory["body_boot"] = body_boot();
	playerInventory["body_ring"] = body_ring();
	playerInventory["weapon_count"] = weapon_count();
	playerInventory["ammunation_count"] = ammunation_count();
	playerInventory["maximize"] = is_maximized();

	return playerInventory;
}

int Inventory::ammunation_count(){
	/*if (!is_maximized()){
		if (!maximize())
			return false;
	}*/
	int id = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_ROPE));
	if (id != 0){
		int count = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_ROPE) - offset_qty_body_items);
		if (count == 0)
			return 1;
		else
			return count;
	}
	return 0;
}

bool Inventory::unequip_slot(inventory_slot_t type, uint32_t bp_id_destination ){

	//TODO checar hotkey tipo 4
	/*
	uint32_t item_id =
	*/
	if (!is_maximized()){
		if (!maximize())
			return false;
	}
	Coordinate temp = get_slot_type_interface_coordinate(type);
	if (temp.is_null())
		return false;
	uint32_t id = get_item_id_by_slot_type(type);
	if (id == 0)
		return false;

	uint32_t container_destination = 0;
	ItemContainerPtr container;
	if (bp_id_destination == UINT32_MAX){
		uint32_t main_bp = body_bag();
		if (!main_bp)
			Actions::open_main_bp();

		ContainerManager::get()->wait_container_id_open(main_bp);

		container = ContainerManager::get()->get_container_by_id(main_bp);
	}
	else{
		Actions::open_container_id(bp_id_destination);
		container = ContainerManager::get()->get_container_by_id(bp_id_destination);
	}

	if (!container)
		return false;

	return Actions::move_item_slot_to_container(type, container->get_random_container_slot(), 0, id) == action_retval_t::action_retval_success;
}

bool Inventory::equip_item(uint32_t item_id, inventory_slot_t inventory_type){

	return false;
}

bool Inventory::put_item(uint32_t item_id, inventory_slot_t slot_type, uint32_t count){
	if (slot_type == inventory_slot_t::slot_none)
		return false;

	if (!is_maximized())
		if (!maximize())
			return false;

	ContainerPosition container_position = ContainerManager::get()->find_item(item_id);
	if (container_position.is_null())
		return false;

	TibiaProcess::get_default()->wait_ping_delay(2);
		
	return Actions::move_item_container_to_slot(container_position, slot_type, count, item_id) == action_retval_success;
}

bool Inventory::put_item(uint32_t item_id, inventory_slot_t slot_type, uint32_t count, ContainerPosition container_position){
	if (slot_type == inventory_slot_t::slot_none)
		return false;

	if (container_position.is_null())
		return false;

	if (!is_maximized())
		if (!maximize())
			return false;

	TibiaProcess::get_default()->wait_ping_delay(4);

	return Actions::move_item_container_to_slot(container_position, slot_type, count, item_id) == action_retval_success;
}

bool Inventory::is_item_equiped(int id){
	return item_count(id) > 0;
}


bool Inventory::is_maximized(){
	return Interface->getEquipmentChildMaximized() != 0;
}

bool Inventory::set_maximized_state(bool state){
	bool current_state = is_maximized();
	if (current_state == state)
		return true;
	if (state)
		return maximize();
	else
		return minimize();
}


Inventory* Inventory::get(){
	static Inventory* mInventory = nullptr;
	if (!mInventory)
		mInventory = new Inventory;
	return mInventory;
}