
#pragma once

#include "LuaCore.h"
#include "..\\NavigationManager.h"
#include "Login.h"
#include "RepoterCore.h"
#include "..\LuaThread.h"
#include "FloatingMiniMenu.h"
#include "SpellcasterCore.h"
#include "Util.h"
#include "TibiaProcess.h"
#include "..\LuaBackgroundManager.h"
#include "..\ControlManager2.h"
#include "Actions.h"
#include "NpcTrade.h"
#include "Map.h"
#include "Inventory.h"
#include "Pathfinder.h"
#include "Neutral.h"
#include "..\\SendInfoManager.h"
#include "ContainerManager.h"
#include "ItemsManager.h"
#include "Actions.h"
#include <thread>
#include "Pvp.h"
#include "HunterCore.h"
#include "VipList.h"
#include "Screenshoot.h"
#include "..\\KeywordManager.h"
#include "AlertsCore.h"
#include "ItemsManager.h"
#include "..\\WaypointManager.h"
#include <boost\filesystem.hpp>
#include "..\\HUDManager.h"
#include "..\\LuaBackgroundManager.h"
#include "..\\LooterManager.h"
#include "..\\SpellManager.h"
#include "..\\HunterManager.h"
#include "..\\AlertsManager.h"
#include "..\\WaypointManager.h"
#include "WaypointerCore.h"
#include "Neutral.h"
#include "..\\SpecialAreas.h"
#include "..\\ConfigPathManager.h"

#pragma comment(lib,"lua5.1.lib")
LuaLibCache::LuaLibCache(){
	loadLibs();
}

std::vector<std::string> LuaGetStringVector(lua_State* state, int32_t stack_index){
	std::vector<std::string> retval;
	if (lua_istable(state, stack_index)){
		lua_pushnil(state);
		while (lua_next(state, stack_index) != 0) {
			retval.push_back(LuaCore::getString(state, -1)); 
			lua_pop(state, 1);
		}
	}
	return retval;
}

std::vector<uint32_t> LuaGetIntVector(lua_State* state, int32_t stack_index){
	std::vector<uint32_t> retval;
	if (lua_istable(state, stack_index)){
		lua_pushnil(state);
		if (stack_index < 0)
			stack_index--;
		else if (stack_index > 0)
			stack_index++;

		while (lua_next(state, stack_index) != 0) {
			retval.push_back(LuaCore::getNumber<uint32_t>(state, -1));
			lua_pop(state, 1);
		}
	}

	return retval;
}

LuaLibCache* LuaLibCache::get(){
	static LuaLibCache* mLuaLibCache;
	if (!mLuaLibCache)
		mLuaLibCache = new LuaLibCache;	

	return mLuaLibCache;
}
int LuaPushIntVector(lua_State* state, std::vector<uint32_t>& items_vector){
	lua_newtable(state);
	int index = 1;
	for (auto it : items_vector){
		lua_pushnumber(state, it);
		lua_rawseti(state, -1, index);
		index++;
	}
	return 1;
}

int LuaPushStringVector(lua_State* state, std::vector<std::string>& items_vector){
	lua_newtable(state);
	int index = 1;
	for (auto it : items_vector){
		lua_pushstring(state, &it[0]);
		lua_rawseti(state, -1, index);
		index++;
	}
	return 1;
}

void LuaLibCache::loadLibs(){
	using namespace boost::filesystem;
	boost::filesystem::path libs_dir(".\\lua\\lualibs\\libs\\");
	if (!boost::filesystem::exists(libs_dir)){
		return;
	}

	directory_iterator end_iter;
	for (directory_iterator dir_itr(libs_dir);
		dir_itr != end_iter;
		++dir_itr){
		if (is_regular_file(dir_itr->path())){
			std::string file_name = dir_itr->path().filename().string();

			std::pair<char*, uint32_t >file = load_file_data(dir_itr->path().string());
			if (!file.second)
				continue;
			std::string file_content;
			file_content.assign((char*)file.first, file.second);
			libs_files.push_back(file_content);
		}
	}
}
bool click_on_rectangle(HUDRECT hudrect, Point point){
	double pointX = point.x;
	double pointY = point.y;

	if (pointX < (hudrect.X + (.5*hudrect.Width)) && pointX >(hudrect.X - (.5*hudrect.Width)) &&
		pointY < (hudrect.Y + (.5*hudrect.Height)) && pointY >(hudrect.Y - (.5*hudrect.Height)))
		return true;
	else
		return false;
}

void LuaCore::lua_push_table_input(InputBaseEvent* input){
	lua_newtable(m_luaState);
	if (input->is_mouse_event()){
		auto input_retval = new MouseInput(*(MouseInput*)input);
		lua_pushnumber(m_luaState, input_retval->x);
		lua_setfield(m_luaState, -2, "x");
		lua_pushnumber(m_luaState, input_retval->y);
		lua_setfield(m_luaState, -2, "y");
		lua_pushnumber(m_luaState, input_retval->event_type);
		lua_setfield(m_luaState, -2, "event_type");
		lua_pushboolean(m_luaState, input_retval->wheel_delta);
		lua_setfield(m_luaState, -2, "wheel_delta");
	}
	else if (input->is_keyboard_event()){
		auto input_retval = new KeyboardInput(*(KeyboardInput*)input);
		lua_pushnumber(m_luaState, input_retval->character);
		lua_setfield(m_luaState, -2, "character");
		lua_pushnumber(m_luaState, input_retval->event_type);
		lua_setfield(m_luaState, -2, "event_type");
	}

	lua_pushboolean(m_luaState, input->alt_pressed);
	lua_setfield(m_luaState, -2, "alt_pressed");
	lua_pushnumber(m_luaState, input->button_code);
	lua_setfield(m_luaState, -2, "button_code");
	lua_pushboolean(m_luaState, input->capslock_state);
	lua_setfield(m_luaState, -2, "capslock_state");
	lua_pushboolean(m_luaState, input->ctrl_pressed);
	lua_setfield(m_luaState, -2, "ctrl_pressed");
	lua_pushboolean(m_luaState, input->numlock_state);
	lua_setfield(m_luaState, -2, "numlock_state");
	lua_pushboolean(m_luaState, input->scrolllock_state);
	lua_setfield(m_luaState, -2, "scrolllock_state");
	lua_pushboolean(m_luaState, input->shift_pressed);
	lua_setfield(m_luaState, -2, "shift_pressed");

	uint32_t element_id = 0;

	std::map<uint32_t , std::shared_ptr<HUDInfo>> map = HUDManager::get_default()->get_mapHUDInfo();
	for (auto it : map){
		if (!it.second->hudrect.is_valid())
			continue;

		if (!input->is_mouse_event())
			continue;

		auto input_retval = new MouseInput(*(MouseInput*)input);

		Rect rect_in = Rect{ it.second->hudrect.X, it.second->hudrect.Y,
			it.second->hudrect.Width, it.second->hudrect.Height };

		Coordinate coord_placement = ClientGeneralInterface::get()->get_client_placement();
		Point point = Point{ coord_placement.x + input_retval->x, coord_placement.y + input_retval->y };

		if (Util::rect_contains_point(rect_in, point))
			element_id = it.first;
	}


	lua_pushnumber(m_luaState, element_id);
	lua_setfield(m_luaState, -2, "elementid");
}

bool LuaCore::getGlobal(std::string& function, InputBaseEvent* input){
	lua_getglobal(m_luaState, &function[0]);
	if (lua_isfunction(m_luaState, -1)){
		lua_push_table_input(input);
		lua_pcall(m_luaState, 1, 1, 0);
		return true;
	}
	else{
		lua_pop(m_luaState, 1);
	}
	return false;
}

std::string LuaCore::getGlobal(std::string global_name){
	lua_getglobal(m_luaState, &global_name[0]);
	if (lua_isboolean(m_luaState, -1)){
		return lua_toboolean(m_luaState, -1) ? "true" : "false";
	}
	else if (lua_isnumber(m_luaState, -1)){
		return std::to_string((long long) lua_tonumber(m_luaState, -1));
	}
	if (lua_isstring(m_luaState, -1))
		return lua_tostring(m_luaState, -1);
	return "";
}

neutral_mutex LuaCore::static_mtx_access;

std::map<uint32_t, std::shared_ptr<LuaCore>> LuaCore::cores;
std::shared_ptr<LuaCore> LuaCore::getState(uint32_t id){
	lock_guard _lock(static_mtx_access);
	auto state = cores.find(id);
	if (state != cores.end())
		return state->second;
	auto core = std::shared_ptr<LuaCore>(new LuaCore);
	cores[id] = core;
	return core;
}


void lua_push_hud_rect(lua_State* state, HUDRECT rect);

bool ftime = true;
neutral_mutex mtx_test;
LuaCore::LuaCore(){
	mtx_test.lock();

	m_luaState = lua_open();
	luaopen_base(m_luaState);
	luaL_openlibs(m_luaState);

	BindLua();

	auto& libs = LuaLibCache::get()->libs_files;
	for (auto lib_it = libs.begin(); lib_it != libs.end(); lib_it++){
		RunScript(*lib_it, "lib_load");
	}
	mtx_test.unlock();
}

uint32_t LuaCore::get_uin32_t(std::string var){
	lock_guard _lock(mtx_access);
	luaL_dostring(m_luaState, "");
	lua_getglobal(m_luaState, &var[0]);

	if (lua_isnumber(m_luaState, -1))
		return (int)lua_tonumber(m_luaState, -1);
	else if (lua_istable(m_luaState, -1)){
		std::vector<uint32_t> address_table = LuaGetIntVector(m_luaState, -1);
		uint32_t temp = address_table[0];
		address_table.erase(address_table.begin());
		return TibiaProcess::get_default()->read_int(temp, address_table, true, false);
	}
	return 0;
}

bool LuaCore::execute(std::string script, lua_t lua_type){
	return false;
}

void LuaCore::RunScript(std::string& script, std::string name){
	RunScript(&script[0], &name[0]);
}

void LuaCore::RunScript(char* script, char* name){
	lock_guard _lock(mtx_access);
	if (luaL_dostring(m_luaState, script)){
		AddErrorLog((char*)lua_tostring(m_luaState, -1), name);
	}
}

bool LuaCore::LoadLib(std::string filePath){
	std::ifstream file(&filePath[0]);
	if (file.is_open()){
		std::string str((std::istreambuf_iterator<char>(file)), std::istreambuf_iterator<char>());
		RunScript(str);
		return true;
	}
	else
		AddErrorLog(&("File " + filePath + " Not found")[0]);
	return false;
}

void LuaCore::AddErrorLog(char* error, std::string name){
	std::string temp_error = error;
	std::string temp = "SCRIPT:  " + name + "\n" + "  LUA ERROR:\n" + temp_error + "\n";
	//LuaThread::get()->lua_error_log(temp);
	LuaHandler::get()->add_lua_error_log(temp);
}



void LuaCore::registerClass(std::string className, std::string baseClass, lua_CFunction newFunction/* = nullptr*/){
	KeywordManager::get()->registerClass(className);
	// className = {}
	lua_newtable(m_luaState);
	lua_pushvalue(m_luaState, -1);
	lua_setglobal(m_luaState, className.c_str());
	int32_t methods = lua_gettop(m_luaState);

	// methodsTable = {}
	lua_newtable(m_luaState);
	int32_t methodsTable = lua_gettop(m_luaState);

	if (newFunction) {
		// className.new = newFunction
		// className.__call = newFunction
		lua_pushcfunction(m_luaState, newFunction);
		lua_pushvalue(m_luaState, -1);
		lua_setfield(m_luaState, methods, "new");
		lua_setfield(m_luaState, methodsTable, "__call");
	}

	uint32_t parents = 0;
	if (!baseClass.empty()) {
		lua_getglobal(m_luaState, baseClass.c_str());
		lua_rawgeti(m_luaState, -1, 'p');
		parents = (uint32_t)lua_tonumber(m_luaState, -1) + 1;
		lua_setfield(m_luaState, methodsTable, "__index");
	}

	// setmetatable(className, methodsTable)
	lua_setmetatable(m_luaState, methods);

	// className.metatable = {}
	luaL_newmetatable(m_luaState, className.c_str());
	int32_t metatable = lua_gettop(m_luaState);

	// className.metatable.__metatable = className
	lua_pushvalue(m_luaState, methods);
	lua_setfield(m_luaState, metatable, "__metatable");

	// className.metatable.__index = className
	lua_pushvalue(m_luaState, methods);
	lua_setfield(m_luaState, metatable, "__index");

	// className.metatable['h'] = hash
	lua_pushnumber(m_luaState, std::hash<std::string>()(className));
	lua_rawseti(m_luaState, metatable, 'h');

	// className.metatable['p'] = parents
	lua_pushnumber(m_luaState, parents);
	lua_rawseti(m_luaState, metatable, 'p');

	// className.metatable['t'] = type
	/*	if (className == "Item") {
	lua_pushnumber(m_luaState, LuaData_Item);
	}
	else if (className == "Container") {
	lua_pushnumber(m_luaState, LuaData_Container);
	}
	else if (className == "Player") {
	lua_pushnumber(m_luaState, LuaData_Player);
	}
	else if (className == "Monster") {
	lua_pushnumber(m_luaState, LuaData_Monster);
	}
	else if (className == "Npc") {
	lua_pushnumber(m_luaState, LuaData_Npc);
	}
	else {
	lua_pushnumber(m_luaState, LuaData_Unknown);
	}
	lua_rawseti(m_luaState, metatable, 't');*/

	// pop className, className.metatable
	lua_pop(m_luaState, 2);
}

// GET
std::string LuaCore::getString(int32_t arg){
	return LuaCore::getString(m_luaState, arg);
}

bool LuaCore::getBoolean(int32_t arg)
{
	return getBoolean(m_luaState, arg) != 0;
}

Position LuaCore::getPosition(int32_t arg)
{
	return LuaCore::getPosition(m_luaState, arg);
}

uint32_t LuaCore::getitem_id(int32_t arg){
	return LuaCore::getitem_id(m_luaState, arg);
}

std::string LuaCore::getFieldString(int32_t arg, std::string key){
	return getFieldString(m_luaState, arg, key);
}

std::string LuaCore::getFieldString(lua_State* state, int32_t arg, std::string key){
	lua_getfield(state, arg, key.c_str());
	return getString(state, -1);
}

bool LuaCore::isNil(int32_t arg){
	return lua_isnil(m_luaState, arg);
}

bool LuaCore::isNumber(int32_t arg){
	return lua_isnumber(m_luaState, arg) != 0;
}

bool LuaCore::isString(int32_t arg){
	return lua_isstring(m_luaState, arg) != 0;
}

bool LuaCore::isBoolean(int32_t arg){
	return lua_isboolean(m_luaState, arg);
}

bool LuaCore::isTable(int32_t arg){
	return lua_istable(m_luaState, arg);
}

bool LuaCore::isFunction(int32_t arg){
	return lua_isfunction(m_luaState, arg);
}

bool LuaCore::isUserdata(int32_t arg){
	return lua_isuserdata(m_luaState, arg) != 0;
}

// Push
void LuaCore::pushBoolean(bool value){
	LuaCore::pushBoolean(m_luaState, value ? 1 : 0);
}

void LuaCore::pushNil(){
	LuaCore::pushNil(m_luaState);
}

void LuaCore::pushString(std::string str){
	LuaCore::pushString(m_luaState, &str[0]);
}

void LuaCore::pushPosition(Position& position){
	LuaCore::pushPosition(m_luaState, position);
}

void LuaCore::pushContainerPosition(const ContainerPosition& container_position){
	lua_createtable(m_luaState, 0, 2);

	pushNumber(container_position.container_index);
	lua_setfield(m_luaState, -2, "container_index");

	pushNumber(container_position.slot_index);
	lua_setfield(m_luaState, -2, "slot_index");
}

void LuaCore::pushContainerPosition(lua_State* state, ContainerPosition& container_position){
	lua_createtable(state, 0, 2);

	pushNumber(state, container_position.container_index);
	lua_setfield(state, -2, "container_index");

	pushNumber(state, container_position.slot_index);
	lua_setfield(state, -2, "slot_index");
}

void LuaCore::pushContainerSlot(const ContainerSlot& container_position){
	lua_createtable(m_luaState, 0, 2);

	pushNumber(container_position.stack_count);
	lua_setfield(m_luaState, -2, "count");

	pushNumber(container_position.id);
	lua_setfield(m_luaState, -2, "item_id");
}

void LuaCore::pushContainerSlot(lua_State* state, ContainerSlot& container_position){
	lua_createtable(state, 0, 2);

	pushNumber(container_position.stack_count);
	lua_setfield(state, -2, "count");

	pushNumber(container_position.id);
	lua_setfield(state, -2, "item_id");
}

ContainerSlot LuaCore::getContainerSlot(int32_t arg){
	ContainerSlot container_slot;
	container_slot.stack_count = getField<uint32_t>(arg, "count");
	container_slot.id = getField<uint32_t>(arg, "item_id");
	lua_pop(m_luaState, 2);
	return container_slot;
}

ContainerPosition LuaCore::getContainerPosition(int32_t arg){
	return LuaCore::getContainerPosition(m_luaState, arg);
}

void LuaCore::registerTable(std::string tableName){
	// _G[tableName] = {}
	KeywordManager::get()->registerTable(tableName);
	lua_newtable(m_luaState);
	lua_setglobal(m_luaState, tableName.c_str());
}

void LuaCore::registerMethod(std::string globalName, std::string methodName, lua_CFunction func){
	KeywordManager::get()->registerTableMethod(globalName, methodName);
	// globalName.methodName = func
	lua_getglobal(m_luaState, globalName.c_str());
	lua_pushcfunction(m_luaState, func);
	lua_setfield(m_luaState, -2, methodName.c_str());

	// pop globalName
	lua_pop(m_luaState, 1);
}

void LuaCore::registerMetaMethod(std::string className, std::string methodName, lua_CFunction func){
	// className.metatable.methodName = func
	luaL_getmetatable(m_luaState, className.c_str());
	lua_pushcfunction(m_luaState, func);
	lua_setfield(m_luaState, -2, methodName.c_str());

	// pop className.metatable
	lua_pop(m_luaState, 1);
}

void LuaCore::registerGlobalMethod(std::string functionName, lua_CFunction func){
	// _G[functionName] = func
	lua_pushcfunction(m_luaState, func);
	lua_setglobal(m_luaState, functionName.c_str());
}

void LuaCore::registerVariable(std::string tableName, std::string name, lua_Number value){
	// KeywordManager::get()->registerTableMethod(tableName, name);
	// tableName.name = value
	lua_getglobal(m_luaState, tableName.c_str());
	lua_pushnumber(m_luaState, value);
	lua_setfield(m_luaState, -2, name.c_str());

	// pop tableName
	lua_pop(m_luaState, 1);
}

void LuaCore::registerGlobalVariable(std::string name, lua_Number value){
	// _G[name] = value
	lua_pushnumber(m_luaState, value);
	lua_setglobal(m_luaState, name.c_str());
}

void LuaCore::setMetatable(int32_t index, std::string string){
	LuaCore::setMetatable(m_luaState, index, string);
}

void LuaCore::pushNumber(lua_Number number) {
	LuaCore::pushNumber(m_luaState, (int)number);
}

void LuaCore::pushContainer(ItemContainer* container){
	LuaCore::pushContainer(m_luaState, container);
}
/*
Creature{
int direction;
Position axis_displacement;
Position coordinate;
int id, life_percent, light, light_color, mount_id, skull_type, summon;
string name;
bool visible, walking;
}
*/

/*
Pathfinder
checgar ate x
checar se tem cehcar
Manter distancia de x
*/
/*Waypointer*/
#ifndef DEV_MODE
#define DEV_MODE FALSE
#endif 

#pragma region BIND_INVENTORY
int luaInventoryammunation_count(lua_State* state){
	LUA_RET_INT(Inventory::get()->ammunation_count())
}
int luaInventorybody_amulet(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_amulet())
}
int luaInventorybody_armor(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_armor())
}
int luaInventorybody_bag(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_bag())
}
int luaInventorybody_boot(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_boot())
}
int luaInventorybody_helmet(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_helmet())
}
int luaInventorybody_leg(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_leg())
}
int luaInventorybody_ring(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_ring())
}
int luaInventorybody_rope(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_rope())
}
int luaInventorybody_shield(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_shield())
}
int luaInventorybody_weapon(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_weapon())
}
int luaInventoryis_item_equiped(lua_State* state){
	uint32_t item_id = LuaCore::getitem_id(state, 1);
	if (item_id == UINT32_MAX)
		LUA_RET_BOOL(false);
	LUA_RET_BOOL(Inventory::get()->is_item_equiped(item_id))
}
int luaInventoryis_maximized(lua_State* state){
	LUA_RET_INT(Inventory::get()->is_maximized())
}
int luaInventory_maximize(lua_State* state){
	LUA_RET_INT(Inventory::get()->maximize())
}
int luaInventory_minimize(lua_State* state){
	LUA_RET_INT(Inventory::get()->minimize())
}
int luaInventoryweapon_count(lua_State* state){
	LUA_RET_INT(Inventory::get()->weapon_count())
}
int luaInventoryput_item(lua_State* state){
	//equip_item(item_id, "legs", optional count)
	uint32_t count = 0;
	if (lua_gettop(state) < 2){
		LUA_RET_BOOL(false);
	}
	if (lua_gettop(state) == 3)
		count = (uint32_t)lua_tonumber(state, 3);


	uint32_t item_id = LuaCore::getitem_id(state, 1);
	if (item_id == UINT32_MAX){
		LUA_RET_BOOL(false)
	}
	inventory_slot_t slot;
	if (lua_isnumber(state, 2)){
		slot = (inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2);
	}
	else
		slot = Inventory::get_type_from_str(LuaCore::getString(state, 2));

	bool value = Inventory::get()->put_item(item_id, slot, count);
	LUA_RET_BOOL(value);
}
int luaInventoryunequip_slot(lua_State* state){
	if (lua_gettop(state) < 1){
		LUA_RET_BOOL(false);
	}
	inventory_slot_t slot;
	uint32_t bp_id = UINT32_MAX;
	if (lua_isnumber(state, 1))
		slot = (inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1);
	else
		slot = Inventory::get_type_from_str(LuaCore::getString(state, 1));

	if (lua_isnumber(state, 2))
		bp_id = LuaCore::getNumber<uint32_t>(state, 2);

	LUA_RET_BOOL(Inventory::get()->unequip_slot(slot, bp_id))
}
int luaInventorylook_at_inventory(lua_State* state){
	bool value;
	if (lua_isnumber(state, 1)){
		value = Inventory::get()->look_at_inventory((inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1));
	}
	else {
		value = Inventory::get()->look_at_inventory(LuaCore::getString(state, 1));
	}
	LUA_RET_BOOL(value)
}
int luaInventoryitem_count(lua_State* state){
	uint32_t item_id = LuaCore::getitem_id(state, 1);
	LUA_RET_INT(Inventory::get()->item_count(item_id))
}

#pragma endregion
#pragma region BIND_CONTAINER
int luaContainersget_container_by_caption(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_caption(LuaCore::getString(state, 1));
	if (!container){
		LUA_RET_NULL()
	}
	LuaCore::pushContainer(state, container.get());
	return 1;
}
int luaget_battle_container(lua_State* state){
	auto battle = ClientGeneralInterface::get()->get_battle_container();
	if (!battle)
		LUA_RET_BOOL(false);

	LUA_RET_BOOL(true);
}

int luaopen_battle(lua_State* state){
	ClientGeneralInterface::get()->open_battle();
	LUA_RET_NULL();
}

int luaclose_battle(lua_State* state){
	ClientGeneralInterface::get()->close_battle();
	LUA_RET_NULL();
}

int luaSpellsManagerneed_operate(lua_State* state){
	LUA_RET_BOOL(SpellCasterCore::get()->need_operate_spells());
}
int luacan_cast(lua_State* state){	
	LUA_RET_BOOL(SpellCasterCore::get()->can_cast_spell(LuaCore::getString(state, 1)));
}

int luagetEquipmentChildMinimized(lua_State* state){
	auto equipment = ClientGeneralInterface::get()->getEquipmentChildMinimized();
	if (!equipment)
		LUA_RET_BOOL(false);

	LUA_RET_BOOL(true);
}



int luaContainersget_container_by_id(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_id(LuaCore::getNumber<uint32_t>(state, 1));
	if (!container){
		LUA_RET_NULL()
	}
	LuaCore::pushContainer(state, container.get());
	return 1;
}

int luaContainersget_depot_chest_container(lua_State* state){
	auto container = ContainerManager::get()->get_depot_chest_container();
	if (!container){
		LUA_RET_NULL()
	}
	LuaCore::pushContainer(state, container.get());
	return 1;
}

int lua_containers_get_containers_open(lua_State* state){
	lua_pushnumber(state, ContainerManager::get()->get_containers_open());
	return 1;
}

int lua_containers_get_used_slots(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_index(LuaCore::getNumber<uint32_t>(state, 1));
	if (container)
		lua_pushnumber(state, container->get_used_slots());
	return 1;
}

int lua_containers_find_item_slot(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_id(LuaCore::getNumber<uint32_t>(state, 1));
	if (!container)
		return 0;
	lua_pushnumber(state, container->find_item_slot(LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}

int lua_containers_get_slot_count(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_index(LuaCore::getNumber<uint32_t>(state, 1));
	if (container)
		lua_pushnumber(state, container->get_slot_count(LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}

int lua_containers_is_open(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_index(LuaCore::getNumber<uint32_t>(state, 1));;
	
	if (!container)
		LUA_RET_BOOL(false);
	
	LUA_RET_BOOL(true);
}
int lua_containers_get_slot_id(lua_State* state){
	auto container = ContainerManager::get()->get_container_by_index(LuaCore::getNumber<uint32_t>(state, 1));;
	if (container)
		lua_pushnumber(state, container->get_slot_id(LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}

int luaContainersget_depot_locker_container(lua_State* state){
	auto container = ContainerManager::get()->get_depot_locker_container();
	if (!container){
		LUA_RET_NULL()
	}
	LuaCore::pushContainer(state, container.get());
	return 1;
}

int luaContainersget_browse_field(lua_State* state){
	auto container = ContainerManager::get()->get_browse_field();
	if (!container){
		LUA_RET_NULL()
	}
	LuaCore::pushContainer(state, container.get());
	return 1;
}

int luaContainersget_total_money(lua_State* state){
	lua_pushnumber(state, ContainerManager::get()->get_total_money());
	return 1;
}

int luaContainersget_item_count(lua_State* state){
	lua_pushnumber(state, ContainerManager::get()->get_item_count(LuaCore::getitem_id(state, 1)));
	return 1;
}

int luaContainerget_browse_fields(lua_State* state){
	auto browses = ContainerManager::get()->get_browse_fields();
	if (!browses.size())
		return 0;
	lua_newtable(state);
	int index = 1;
	for (auto it : browses){
		lua_pushnumber(state, index);
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaContainerget_container_positions_where(lua_State* state){
	uint32_t args = lua_gettop(state);
	std::vector<ContainerPosition> retval;
	switch (args)
	{
	case 1:{
			   retval = ContainerManager::get()->get_container_positions_where(
				   LuaCore::getNumber<uint32_t>(state, 1));
	}
		break;
	case 2:{
			   retval = ContainerManager::get()->get_container_positions_where(
				   LuaCore::getNumber<uint32_t>(state, 1), LuaCore::getNumber<uint32_t>(state, 2));
	}
		break;
	case 3:
	{
			  retval = ContainerManager::get()->get_container_positions_where(
				  LuaCore::getNumber<uint32_t>(state, 1), LuaCore::getNumber<uint32_t>(state, 2), LuaCore::getNumber<uint32_t>(state, 3));
	}
		break;
	case 4:{
			   retval = ContainerManager::get()->get_container_positions_where(
				   LuaCore::getNumber<uint32_t>(state, 1), LuaCore::getNumber<uint32_t>(state, 2), LuaCore::getNumber<uint32_t>(state, 3), LuaCore::getNumber<uint32_t>(state, 4));
	}
		break;
	}

	lua_newtable(state);
	int index = 1;
	for (auto it : retval){
		LuaCore::pushContainerPosition(state, it);
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}





int luaContainerget_food_count(lua_State* state){
	LUA_RET_INT(ContainerManager::get()->get_food_count());
}
int luaContainerclose_all_creature_containers(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->close_all_creature_containers())
}
int luaContainerclose_all_containers(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->close_all_containers())
}
int luaContainerclose_all_browse_fields(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->close_all_browse_fields())
}
int luaContainerclose_containers_by_index(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->close_containers_by_index(LuaCore::getNumber<uint32_t>(state, 1)))
}
int luaContainerget_all_containers_id_opened(lua_State* state){
	LuaPushIntVector(state, ContainerManager::get()->get_all_containers_id_opened());
	return 1;
}
int luaContainerget_all_items_is_in_open_containers(lua_State* state){
	LuaPushIntVector(state, ContainerManager::get()->get_all_items_is_in_open_containers());
	return 1;
}

int luaContainerwait_browse_field_close(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_browse_field_close(LuaCore::getNumber<uint32_t>(state, 1)))
}


int luaContainerwait_container_browse_field(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_container_browse_field(LuaCore::getNumber<uint32_t>(state, 1)))
}


int luaContainerwait_container_id_close(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_container_id_close(LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2)))
}

int luaContainerwait_container_id_open(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_container_id_open(LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2)))
}

int luaContainerwait_container_index_close(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_container_index_close(LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2)))
}

int luaContainerwait_depot_locker_open(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_depot_locker_open(LuaCore::getNumber<uint32_t>(state, 1)))
}


int luaContainerwait_open_container_count_change(lua_State* state){
	LUA_RET_INT(ContainerManager::get()->wait_open_container_count_change(LuaCore::getNumber<uint32_t>(state, 1)))
}

int luaContainerwait_open_trade(lua_State* state){
	LUA_RET_BOOL(ContainerManager::get()->wait_open_trade(LuaCore::getNumber<uint32_t>(state, 1)))
}




#pragma endregion
#pragma region BIND_PATHFINDER
//is_reachable(Position, optional only_on_screen = false)
int luaPathfinderis_reachable(lua_State* state){
	map_type_t map_type = map_type_t::map_front_t;
	if (lua_gettop(state) > 1)
		map_type = map_type_t::map_front_mini_t;
	bool retval = Pathfinder::get()->is_reachable(LuaCore::getPosition(state, 1), map_type);
	LUA_RET_BOOL(retval)
}

//set_go_to(Position, optional distance_near = 1, optional only_on_screen = false)
int luaPathfinderset_go_to(lua_State* state){
	uint32_t consider_near = 1;
	map_type_t map_type = map_type_t::map_front_t;
	if (lua_gettop(state) > 1)
		consider_near = LuaCore::getNumber<uint32_t>(state, 2);
	if (lua_gettop(state) > 2 && LuaCore::getBoolean(state, 3))
		map_type = map_type_t::map_front_mini_t;
	int retval = Actions::goto_coord(LuaCore::getPosition(state, 1), consider_near, map_type);
	LUA_RET_INT(retval)
}

//walk_near_to(Position, optional distance_near = 1, optional only_on_screen = false )
int luaPathfinderwalk_near_to(lua_State* state){
	uint32_t consider_near = 1;
	map_type_t map_type = map_type_t::map_front_t;
	if (lua_gettop(state) > 1)
		consider_near = LuaCore::getNumber<uint32_t>(state, 2);
	if (lua_gettop(state) > 2 && LuaCore::getBoolean(state, 3))
		map_type = map_type_t::map_front_mini_t;
	int retval = Pathfinder::get()->walk_near_to(LuaCore::getPosition(state, 1), map_type, consider_near);
	LUA_RET_INT(retval)
}

//cancel_all()
int luaPathfindercancel_all(lua_State* state){
	Pathfinder::get()->cancel_all();
	return 0;
}
//step_to_coordinate(Position, optional only_on_screen = false)
int luaPathfinderstep_to_coordinate(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(Pathfinder::get()->go_with_hotkey(LuaCore::getPosition(state, 1),false,false, map_front_t, map_front_t, LuaCore::getBoolean(state, 2)))
	}
	else{
		LUA_RET_INT(Pathfinder::get()->go_with_hotkey(LuaCore::getPosition(state, 1)))
	}
}
#pragma endregion
#pragma region BIND_PVPMANAGER

int luaPvpManagerget_attack_mode(lua_State* state){
	LUA_RET_INT(PvpManager::get()->get_attack_mode())
}

int luaPvpManagerget_skull_mode_new(lua_State* state){
	LUA_RET_INT(PvpManager::get()->get_skull_mode_new())
}

int luaPvpManagerget_skull_mode_old(lua_State* state){
	LUA_RET_INT(PvpManager::get()->get_skull_mode_old())
}
//(attack_mode_t)
int luaPvpManagerset_attack_mode(lua_State* state){
	LUA_RET_BOOL(PvpManager::get()->set_attack_mode((attack_mode_t)LuaCore::getNumber<uint32_t>(state, 1)))
}

int luaPvpManagerset_balanced_attack_mode(lua_State* state){
	LUA_RET_BOOL(PvpManager::get()->set_balanced_attack_mode())
}

int luaPvpManagerset_defensive_attack_mode(lua_State* state){
	LUA_RET_BOOL(PvpManager::get()->set_defensive_attack_mode())
}

int luaPvpManagerset_offensive_attack_mode(lua_State* state){
	LUA_RET_BOOL(PvpManager::get()->set_offensive_attack_mode())
}
//(pvp_net_t)
int luaPvpManagerset_skull_mode_new(lua_State* state){
	LUA_RET_BOOL(PvpManager::get()->set_skull_mode_new((pvp_new_t)LuaCore::getNumber<uint32_t>(state, 1)))
}
//(bool style)
int luaPvpManagerset_skull_mode_old(lua_State* state){
	LUA_RET_BOOL(PvpManager::get()->set_skull_mode_old((pvp_old_t)LuaCore::getNumber<uint32_t>(state, 1)))
}

#pragma endregion
#pragma region BIND_HUNTERACTION
//attack_creature_on_battle(cid)
int luaHunterActionsattack_creature_on_battle(lua_State* state){
	bool retval = HunterActions::get()->attack_creature_on_battle(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_BOOL(retval)
}
//attack_creature_on_coordinates(cid)
int luaHunterActionsattack_creature_on_coordinates(lua_State* state){
	bool retval = HunterActions::get()->attack_creature_on_coordinates(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_BOOL(retval)
		bool attack_creature_on_battle(uint32_t id, int32_t timeout = 300);
}
//attack_creature_by_name(name)
int luaHunterActionsattack_creature_by_name(lua_State* state){
	bool retval = HunterActions::get()->attack_creature_by_name(LuaCore::getString(state, 1));
	LUA_RET_BOOL(retval)
}
//attack_creature_id(cid)
int luaHunterActionsattack_creature_id(lua_State* state){
	bool retval = HunterActions::get()->attack_creature_id(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_BOOL(retval)
}

int luaHunterActionshas_temporary(lua_State* state){
	LUA_RET_BOOL(HunterActions::get()->has_temporary)
}


int luaHunterActionsadd_temporary_target(lua_State* state){
	int argc = lua_gettop(state);

	uint32_t creature_id = 0;
	uint32_t max_time = 0xffffffff;

	if (argc > 0)
		creature_id = LuaCore::getNumber<uint32_t>(state, 1);
	
	if (argc > 1)
		max_time = LuaCore::getNumber<uint32_t>(state, 2);
	
	HunterActions::get()->add_temporary_target(creature_id, max_time);
	return 0;
}

int luaHunterActionsget_temporary_target_id(lua_State* state){
	LUA_RET_INT(HunterActions::get()->get_temporary_target_id());
}


int luaHunterActionsclear_temporary_targets(lua_State* state){
	HunterActions::get()->clear_temporary_targets();
	return 0;
}


int luaHunterActionsget_temporary_tagets(lua_State* state){
	std::vector<TemporaryTarget> targets = HunterActions::get()->get_temporary_tagets();
	lua_newtable(state);
	int index = 1;
	for (auto it : targets){
		lua_newtable(state);
		lua_pushnumber(state, it.creature_id);
		lua_setfield(state, -2, "creature_id");

		lua_pushnumber(state, it.creature_id);
		lua_setfield(state, -2, "max_time");

		lua_pushnumber(state, it.max_time - it.timer.elapsed_milliseconds());
		lua_setfield(state, -2, "timeleft");
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaHunterActionsset_follow_creature(lua_State* state){
	int argc = lua_gettop(state);

	uint32_t creature_id = 0;
	uint32_t max_time = 0xffffffff;

	if (argc > 0)
		creature_id = LuaCore::getNumber<uint32_t>(state, 1);

	if (argc > 1)
		max_time = LuaCore::getNumber<uint32_t>(state, 2);

	HunterActions::get()->set_follow_creature(creature_id, max_time);
		return 0;
}

#pragma endregion
#pragma region BIND_CREATURES
void pushCreatureOnBattle(lua_State* state, CreatureOnBattle* creature){

	lua_newtable(state);
	LuaCore::pushNumber(state, creature->direction);
	lua_setfield(state, -2, "direction");

	LuaCore::pushPosition(state, Position(creature->dist_from_center_x, creature->dist_from_center_y, 0));
	lua_setfield(state, -2, "axis_displacement");

	LuaCore::pushPosition(state, creature->get_coordinate_monster());
	lua_setfield(state, -2, "coordinate");
	
	Coordinate displacement_coord = creature->get_displaced_coordinate();
	if (displacement_coord.is_null()){
		lua_pushnil(state);
	}
	else{
		LuaCore::pushPosition(state, displacement_coord);
	}
	
	lua_setfield(state, -2, "going_step");

	LuaCore::pushNumber(state, creature->id);
	lua_setfield(state, -2, "id");

	LuaCore::pushNumber(state, creature->life);
	lua_setfield(state, -2, "life_percent");

	LuaCore::pushNumber(state, creature->light);
	lua_setfield(state, -2, "light");

	LuaCore::pushNumber(state, creature->light_color);
	lua_setfield(state, -2, "light_color");

	LuaCore::pushNumber(state, creature->mount);
	lua_setfield(state, -2, "mount_id");

	LuaCore::pushString(state, creature->name);
	lua_setfield(state, -2, "name");

	LuaCore::pushNumber(state, creature->skull);
	lua_setfield(state, -2, "skull_type");

	LuaCore::pushNumber(state, creature->type);
	lua_setfield(state, -2, "type");

	LuaCore::pushBoolean(state, creature->visible != 0);
	lua_setfield(state, -2, "visible");

	LuaCore::pushNumber(state, creature->walk_state);
	lua_setfield(state, -2, "walking");
}

void pushCreatureOnBattleVector(lua_State* state, std::vector<CreatureOnBattlePtr> creatures_vector){
	lua_newtable(state);
	int index = 1;
	for (auto it : creatures_vector){
		lua_pushnumber(state, index);
		pushCreatureOnBattle(state, it.get());
		lua_settable(state, -3);
		index++;
	}
}

void pushItems_looting(lua_State* state, std::shared_ptr<ItemsInfoLooted> item){
	lua_newtable(state);

	if (!item){
		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "count");

		LuaCore::pushString(state, "");
		lua_setfield(state, -2, "name");

		LuaCore::pushNumber(state, 00);
		lua_setfield(state, -2, "id");

		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "sellprice");

		LuaCore::pushBoolean(state, false);
		lua_setfield(state, -2, "visible_in_hud");

		return;
	}

	LuaCore::pushNumber(state, item->count);
	lua_setfield(state, -2, "count");

	LuaCore::pushString(state, item->name);
	lua_setfield(state, -2, "name");

	LuaCore::pushNumber(state, item->item_id);
	lua_setfield(state, -2, "id");

	LuaCore::pushNumber(state, item->sell_price);
	lua_setfield(state, -2, "sellprice");

	LuaCore::pushBoolean(state, item->visible_in_hud);
	lua_setfield(state, -2, "visible_in_hud");
}


void pushItems_Used(lua_State* state, std::shared_ptr<ItemsInfoLooted> itemsRule){
	lua_newtable(state);

	if (!itemsRule){
		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "count");

		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "id");

		LuaCore::pushString(state, "");
		lua_setfield(state, -2, "name");

		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "buyprice");

		LuaCore::pushBoolean(state, false);
		lua_setfield(state, -2, "visible_in_hud");

		return;
	}

	std::map<uint32_t, std::pair<uint32_t, uint32_t>>  map_item_used = InfoCore::get()->get_itens_used_map();
	LuaCore::pushNumber(state, map_item_used[itemsRule->item_id].first);
	lua_setfield(state, -2, "count");

	LuaCore::pushNumber(state, itemsRule->item_id);
	lua_setfield(state, -2, "id");

	LuaCore::pushString(state, itemsRule->name);
	lua_setfield(state, -2, "name");

	LuaCore::pushNumber(state, itemsRule->buy_price);
	lua_setfield(state, -2, "buyprice");

	LuaCore::pushBoolean(state, itemsRule->visible_in_hud);
	lua_setfield(state, -2, "visible_in_hud");
}

void pushMonsterKilled(lua_State* state, int killed, std::string name){
	lua_newtable(state);

	LuaCore::pushNumber(state, killed);
	lua_setfield(state, -2, "count");

	LuaCore::pushString(state, name);
	lua_setfield(state, -2, "name");
}

int luamonster_killer_count(lua_State* state){
	LUA_RET_INT(InfoCore::get()->get_monster_killed_map().size());
}
int luais_logged(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->get_is_logged());
}
int luatry_login(lua_State* state){
	LoginManager::get()->TryLogin(LuaCore::getString(state, 1), LuaCore::getString(state, 2), LuaCore::getNumber<uint32_t>(state, 3));
	LUA_RET_NULL();
}

int luamonster_killed_by_name(lua_State* state){
	std::map <std::string, uint32_t> map_item_used = InfoCore::get()->get_monster_killed_map();//InfoCore::get()->get_itens_used_map();

	if (!map_item_used.size())
		LUA_RET_NULL();
	
	for (auto item : map_item_used){
		if (item.first != LuaCore::getString(state, 1))
			continue;
		
		LUA_RET_INT(item.second);
	}

	LUA_RET_NULL();
}

int luaMonsterKilledInfo(lua_State* state){
	std::map <std::string, uint32_t> map_item_used = InfoCore::get()->get_monster_killed_map();//InfoCore::get()->get_itens_used_map();

	lua_newtable(state);

	if (!map_item_used.size()){
		lua_pushinteger(state, 0);
		pushMonsterKilled(state, 0,"");
		lua_settable(state, -3);
		return 1;
	}
	for (auto item : map_item_used){
		lua_pushstring(state, &item.first[0]);
		pushMonsterKilled(state, item.second, item.first);
		lua_settable(state, -3);
	}
	return 1;
}

int luaLootInfo_get_vector_items_used(lua_State* state){
	std::map<uint32_t, std::pair<uint32_t, uint32_t>>  map_item_used = InfoCore::get()->get_itens_used_map();//InfoCore::get()->get_itens_used_map();
	
	lua_newtable(state); 

	if (!map_item_used.size()){
		lua_pushinteger(state, 0);
		pushItems_Used(state, nullptr);
		lua_settable(state, -3);
		return 1;
	}
	for (auto item : map_item_used){
		lua_pushinteger(state, item.first);
		pushItems_Used(state, LooterCore::get()->getRuleItemsLooted(item.first));
		lua_settable(state, -3);
	}
	return 1;
}

int luaLootInfo_get_vector_items(lua_State* state){
	std::map <uint32_t, std::shared_ptr<ItemsInfoLooted>> map_item_used = LooterCore::get()->get_map_items_looted();
	if (!map_item_used.size()){
		lua_pushnil(state);
		return 1;
	}

	lua_newtable(state);
	for (auto item : map_item_used){
		lua_pushnumber(state, item.first);
		pushItems_looting(state, item.second);
		lua_settable(state, -3);
	}
	return 1;
}

int luaCreaturesfind_creature_on_screen(lua_State* state){
	auto creature = BattleList::get()->find_creature_on_screen(LuaCore::getString(state, 1));
	if (!creature)
		lua_pushnil(state);
	else
		pushCreatureOnBattle(state, creature.get());

	return 1;
}

int luaCreaturesfind_creature_by_id(lua_State* state){
	auto creature = BattleList::get()->find_creature_by_id(LuaCore::getNumber<uint32_t>(state, 1));
	if (!creature)
		lua_pushnil(state);
	else
		pushCreatureOnBattle(state, creature.get());

	return 1;
}

int luaCreaturesget_creatures_on_screen(lua_State* state){
	auto creatures = BattleList::get()->get_creatures_on_screen();
	if (!creatures.size())
		lua_pushnil(state);

	lua_newtable(state);
	int index = 1;
	for (auto creature : creatures){
		lua_pushnumber(state, index);
		pushCreatureOnBattle(state, creature.second.get());
		lua_settable(state, -3);
		index++;
	}
	return 1;
}


int luaInfoplayer_kill_on_screen(lua_State* state){
	LUA_RET_BOOL(BattleList::get()->player_kill_on_screen());
}

int luaInfotibiamessages_vector(lua_State* state){
	std::vector<std::string> tibia_messages = InfoCore::get()->get_tibia_messages_vector();
	if (!tibia_messages.size())
		lua_pushnil(state);

	lua_newtable(state);
	int index = 1;
	for (auto message : tibia_messages){
		lua_pushnumber(state, index);
		lua_pushstring(state, message.c_str());
		lua_settable(state, -3);
		index++;
	}
	return 1;
}

int luaCreaturesget_target(lua_State* state){
	auto creature = BattleList::get()->get_target();
	if (!creature)
		lua_pushnil(state);
	else{
		int args = lua_gettop(state);
		if (args == 1){
			if (creature->name == LuaCore::getString(state, 1))
				pushCreatureOnBattle(state, creature.get());
			else
				lua_pushnil(state);			
		}
		else
			pushCreatureOnBattle(state, creature.get());
	}
	return 1;
}
/*
uint32_t get_creature_count_in_radius(std::vector<std::string> creatures_name, uint32_t radius);

std::vector<std::shared_ptr<CreatureOnBattle>> get_creatures_in_radius(std::vector<std::string> creatures_name, uint32_t radius, bool must_be_reachable = false);

*/
int luaCreaturesget_creatures_around(lua_State* state){
	int args_count = lua_gettop(state);
	bool must_be_reachable = false;
	uint32_t consider_near = 1;

	std::vector<std::string> creatures_param_table;
	if (lua_istable(state, 1)){
		creatures_param_table = LuaGetStringVector(state, 1);
	}
	else{
		LUA_RET_INT(0)
	}

	if (args_count > 1)
		consider_near = LuaCore::getNumber<uint32_t>(state, 2);
	
	if (args_count > 2)
		must_be_reachable = LuaCore::getBoolean(state, 3);
	
	LUA_RET_INT(BattleList::get()->get_creature_count_in_radius(creatures_param_table, consider_near, must_be_reachable))
}

int luaPathfinderget_use_diagonal(lua_State* state){
	LUA_RET_BOOL(ConfigPathManager::get()->get_use_diagonal());
}

int luaPathfinderset_use_diagonal(lua_State* state){
	ConfigPathManager::get()->set_use_diagonal(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}

int luaPathfinderwalk_around(lua_State* state){
	int args_count = lua_gettop(state);

	std::vector<Coordinate> vector_coord;

	for (int i = 0; i < args_count; i++){
		if (lua_istable(state, 1))
			vector_coord.push_back(LuaCore::getPosition(state, 1));		
	}

	return 1;
}

int luaCreaturesget_creatures_in_radius(lua_State* state){
	int args_count = lua_gettop(state);
	bool must_be_reachable = false;
	uint32_t consider_near = 1;

	std::vector<std::string> creatures_param_table;
	if (lua_istable(state, 1)){
		creatures_param_table = LuaGetStringVector(state, 1);
	}
	else{
		LUA_RET_INT(0)
	}

	if (args_count > 1)
		consider_near = LuaCore::getNumber<uint32_t>(state, 2);

	if (args_count > 2)
		must_be_reachable = LuaCore::getBoolean(state, 3);

	std::vector<std::shared_ptr<CreatureOnBattle>> creatures =
		BattleList::get()->get_creatures_in_radius(creatures_param_table, consider_near, must_be_reachable);
	int index = 1;
	lua_newtable(state);
	for (auto it : creatures){
		pushCreatureOnBattle(state, it.get());
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaCreaturesget_creatures_at_coordinate(lua_State* state){
	Coordinate coord;
	bool ignore_z = false;
	
	int argc = lua_gettop(state);
	if (argc > 0){
		coord = LuaCore::getPosition(state, 1);
	}
	if (argc > 1)
		ignore_z = lua_toboolean(state, 2) != 0;

	std::map<int, std::shared_ptr<CreatureOnBattle>> _map = BattleList::get()->
		get_creatures_at_coordinate(coord, ignore_z);

	int index = 1;
	lua_newtable(state);
	for (auto it : _map){
		pushCreatureOnBattle(state, it.second.get());
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaCreaturesget_creatures_near_coordinate(lua_State* state){
	Coordinate coord;
	uint32_t sqm_to_consider_near = 1;
	bool ignore_z = false;

	int argc = lua_gettop(state);
	if (argc > 0)
		coord = LuaCore::getPosition(state, 1);
	if (argc > 1)
		sqm_to_consider_near = LuaCore::getNumber<uint32_t>(state, 2);
	if (argc > 2)
		ignore_z = lua_toboolean(state, 3) != 0;

	std::vector<std::shared_ptr<CreatureOnBattle>> _vector = BattleList::get()->
		get_creatures_near_coordinate(coord, sqm_to_consider_near, ignore_z);

	int index = 1;
	lua_newtable(state);
	for (auto it : _vector){
		pushCreatureOnBattle(state, it.get());
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}


int luaCreaturesget_creatures_on_area(lua_State* state){
	Coordinate coord;
	uint32_t sqm_to_consider_near = 1;
	bool ignore_z = false;

	int argc = lua_gettop(state);
	if (argc > 0)
		coord = LuaCore::getPosition(state, 1);
	if (argc > 1)
		sqm_to_consider_near = LuaCore::getNumber<uint32_t>(state, 2);
	if (argc > 2)
		ignore_z = lua_toboolean(state, 3) != 0;

	std::vector<std::shared_ptr<CreatureOnBattle>> _vector = BattleList::get()->
		get_creatures_on_area(coord, sqm_to_consider_near, ignore_z);

	int index = 1;
	lua_newtable(state);
	for (auto it : _vector){
		pushCreatureOnBattle(state, it.get());
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaCreaturesget_creatures_on_area_effect(lua_State* state){
	Coordinate coord;
	area_type_t area_effect = area_type_t::area_type_none;
	int argc = lua_gettop(state);
	if (argc > 0)
		coord = LuaCore::getPosition(state, 1);
	if (argc > 1)
		area_effect = (area_type_t)LuaCore::getNumber<uint32_t>(state, 2);

	std::vector<std::shared_ptr<CreatureOnBattle>>
		creatures = BattleList::get()->
		get_creatures_on_area_effect(coord, (area_type_t)area_effect);

	int index = 1;
	lua_newtable(state);
	for (auto it : creatures){
		pushCreatureOnBattle(state, it.get());
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaCreaturesis_traped_by_monster(lua_State* state){
	LUA_RET_BOOL(BattleList::get()->is_traped_by_monster());
}

int luaCreatureshave_player_on_screen(lua_State* state){
	LUA_RET_BOOL(BattleList::get()->have_other_player())
}


int luaCreatureshave_creature_on_screen(lua_State* state){
	LUA_RET_BOOL(BattleList::get()->have_creature_on_screen())
	return 0;
}
int luaget_creatures_on_screen_count(lua_State* state){
	std::map<int, std::shared_ptr<CreatureOnBattle>> creatures = BattleList::get()->get_creatures_on_screen();
	int count_creature = 0;
	for (auto creature : creatures){
		if (!creature.second->is_monster())
			continue;

		count_creature++;
	}

	LUA_RET_INT(count_creature)
		return 0;
}


int luaCreaturesget_self_creature(lua_State* state){
	CreatureOnBattle* self_creature = BattleList::get()->get_self().get();
	if (!self_creature){
		LUA_RET_NULL();
	}
	pushCreatureOnBattle(state, self_creature);
	return 1;
}

int luaCreatures(lua_State* state){
	bool ignore_z = false;
	if (lua_gettop(state) == 1)
		ignore_z = lua_toboolean(state, 1) != 0;

	LUA_RET_BOOL(BattleList::get()->player_kill_on_screen(ignore_z))
}

int luakeep_distance(lua_State* state){
	Coordinate coord = LuaCore::getPosition(state, 1);
	std::vector<DistanceParamRowBase*> params;
	if (lua_gettop(state) > 1 && lua_istable(state, 2)){
		uint32_t index = lua_gettop(state);

		lua_pushnil(state); // first key
		while (lua_next(state, 2) != 0) { // traverse keys
			if (lua_istable(state, -1)){
				DistanceParamRowBase* new_params = nullptr;
				lua_getfield(state, -1, "param_type");
				distance_param_row_t row_t =
					(distance_param_row_t)LuaCore::getNumber<uint32_t>(state, -1);
				lua_pop(state, 1); // stack restore

				

				switch (row_t)
				{
					case distance_param_creature:
					{
						lua_getfield(state, -1, "name");
						std::string name = lua_tostring(state, -1);
						lua_pop(state, 1); // stack restore
						new_params = (DistanceParamRowBase*) new DistanceParamRowCreature(name, 0);
					}
					break;
					case distance_param_coordinate:
					{
						lua_getfield(state, -1, "position");
						Coordinate position = LuaCore::getPosition(state, -1);
						lua_pop(state, 1); // stack restore
						new_params = (DistanceParamRowBase*) new DistanceParamRowCoordinate(position, 0);
					
					}
					break;
					case distance_param_none:
						continue;
					break;
				default:
					break;
				}
				if (!new_params){
					continue;
				}
				lua_getfield(state, -1, "danger");
				new_params->set_danger(LuaCore::getNumber<uint32_t>(state, -1));
				lua_pop(state, 1); // stack restore
				params.push_back(new_params);
			
			}
			lua_pop(state, 1); // stack restore
		}
	}

	HunterCore::keep_distance(coord,
		&params);
	//cleanup
	for (auto it : params){
		if (it->get_type() == distance_param_row_t::distance_param_creature)
			delete ((DistanceParamRowCreature*)it);
		else if (it->get_type() == distance_param_row_t::distance_param_coordinate)
			delete ((DistanceParamRowCoordinate*)it);
	}
	return 0;
}


int luaHunterCoreexist_creature_to_attack(lua_State* state){
	LUA_RET_BOOL(HunterCore::get()->exist_creature_to_attack());
}

int luaHunterCorepulse(lua_State* state){
	HunterCore::get()->pulse();
	LUA_RET_NULL();
}

int luaHunterCorewait_pulse(lua_State* state){
	uint32_t max_time = UINT32_MAX;
	if (lua_gettop(state) > 0){
		max_time = LuaCore::getNumber<uint32_t>(state, 1);
	}
	LUA_RET_BOOL(HunterCore::get()->wait_pulse(max_time));
}

int luaHunterCorewait_pulse_out(lua_State* state){
	uint32_t max_time = UINT32_MAX;
	if (lua_gettop(state) > 0){
		max_time = LuaCore::getNumber<uint32_t>(state, 1);
	}
	LUA_RET_BOOL(HunterCore::get()->wait_pulse_out(max_time))
}


int luaHunterCorewait_kill_all_complete(lua_State* state){
	uint32_t max_time = UINT32_MAX;
	if (lua_gettop(state) > 0){
		max_time = LuaCore::getNumber<uint32_t>(state, 1);
	}
	LUA_RET_BOOL(HunterCore::get()->wait_kill_all_complete(max_time))
}

int luaHunterCoreis_pulsed(lua_State* state){
	LUA_RET_BOOL(HunterCore::get()->is_pulsed())
}

int luaHunterCoreunset_pulse(lua_State* state){
	HunterCore::get()->unset_pulse();
	LUA_RET_NULL();
}


int luaLooterCoreunset_pulse(lua_State* state){
	LooterCore::get()->unset_pulse();
	LUA_RET_NULL();
}

int luaLooterCoreis_pulsed(lua_State* state){
	LUA_RET_BOOL(LooterCore::get()->is_pulsed())
}

#pragma endregion

void LuaCore::pushContainer(const ItemContainer& container){
	pushContainer((ItemContainer*)&container);
}

#pragma region BIND_STATUS
int lua_status_slow(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_slow())
}
int lua_status_poison(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_poison())
}
int lua_status_fire(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_fire())
}
int lua_status_energy(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_energy())
}
int lua_status_drunk(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_drunk())
}
int lua_status_utamo(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_utamo())
}
int lua_status_haste(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_haste())
}
int lua_status_battle(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_battle())
}
int lua_status_water(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_water())
}
int lua_status_frozen(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_frozen())
}
int lua_status_holly(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_holly())
}
int lua_status_curse(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_curse())
}
int lua_status_streng_up(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_streng_up())
}
int lua_status_battle_red(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_battle_red())
}
int lua_status_pz(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_pz())
}
int lua_status_blood(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->status_blood())
}
#pragma endregion

#pragma region BIND_REPOTERCORE
int lua_repotercore_open_trade(lua_State* state){
	LuaCore::pushBoolean(state,
		RepoterCore::open_trade(std::vector<std::string>()));
	return 1;
}
int lua_repotercore_buy_item_repot_id(lua_State* state){
	LuaCore::pushBoolean(state,
		RepoterCore::buy_item_repot_id(
		LuaCore::getString(state, 1)));
	return 1;
}
int lua_repotercore_total_money_all_repot(lua_State* state){
	LUA_RET_INT(RepoterCore::total_money_all_repot());
}
int lua_repotercore_total_money_repot(lua_State* state){
	LUA_RET_INT(RepoterCore::total_money_repot(
		LuaCore::getString(state, 1)))
}
int lua_repotercore_is_necessary_all_repot(lua_State* state){
	LuaCore::pushBoolean(state,
		RepoterCore::is_necessary_all_repot());
	return 1;
}
int lua_repotercore_is_necessary_repot(lua_State* state){
	LuaCore::pushBoolean(state,
		RepoterCore::is_necessary_repot(
		LuaCore::getString(state, 1)));
	return 1;
}
int lua_repotercore_withdraw_repot(lua_State* state){
	LuaCore::pushBoolean(state,
		RepoterCore::withdraw_repot());
	return 1;
}
int lua_repotercore_sell_empty_vials(lua_State* state){
	LuaCore::pushBoolean(state,
		RepoterCore::sell_empty_vials());
	return 1;
}
#pragma endregion

int lua_actions_goto_nearest_from_creature(lua_State* state){
	LUA_RET_INT(
		Actions::goto_nearest_from_creature(
		LuaCore::getString(state, 1)));
	return 1;
}

int lua_actions_up_ladder(lua_State* state){
	LUA_RET_INT(
		Actions::up_ladder(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int lua_actions_use_lever(lua_State* state){
	uint32_t item_id_false_use = 0;
	uint32_t item_id_true_use = 0;
	Coordinate coord;
	Coordinate offset;
	int args = lua_gettop(state);
	if (!args){
		LUA_RET_INT(action_retval_missing_parameter);
	}
	
	coord = LuaCore::getPosition(state, 1);
	if (args > 1){
		offset = LuaCore::getPosition(state, 2);
	}
	if (args > 2){
		item_id_false_use = LuaCore::getNumber<uint32_t>(state, 3);
	}
	if (args > 3){
		item_id_true_use = LuaCore::getNumber<uint32_t>(state, 4);
	}
	
	LUA_RET_INT(
		Actions::use_lever(
		coord, offset, item_id_false_use, item_id_true_use));
	return 1;
}

int lua_actions_join_in_telepot(lua_State* state){
	LUA_RET_INT(
		Actions::join_in_telepot(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int lua_actions_use_item_id_to_down_or_up(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_id_to_down_or_up(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int lua_actions_check_move_item_container_to_container(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::check_move_item_container_to_container(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getContainerPosition(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::check_move_item_container_to_container(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getContainerPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)
			));
	}
	else if (args == 4){
		LUA_RET_INT(
			Actions::check_move_item_container_to_container(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getContainerPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)
			));
	}
	return 1;
}

int lua_actions_reopen_containers(lua_State* state){
	LUA_RET_INT(Actions::reopen_containers());
	return 1;
}

int lua_drop_empty_potions(lua_State* state){
	LUA_RET_INT(Actions::drop_empty_potions());
	return 1;
}

int lua_actions_refill_amun(lua_State* state){
	uint32_t temp_item_id = 0;
	if (lua_gettop(state) == 1){
		try{
			temp_item_id = boost::lexical_cast<uint32_t>(LuaCore::getNumber<uint32_t>(state, 1));
		}
		catch (...){
			temp_item_id = ItemsManager::get()->getitem_idFromName(LuaCore::getString(state, 1));
		}

		if (temp_item_id == 0)
			temp_item_id = ItemsManager::get()->getitem_idFromName(LuaCore::getString(state, 1));

		if (temp_item_id == 0 || temp_item_id == UINT32_MAX){
			LUA_RET_INT(Actions::refill_weap_amun_process());
			return 1;
		}

		LUA_RET_INT(Actions::refill_weap_amun_process_by_id(temp_item_id));
	}
	else
		LUA_RET_INT(Actions::refill_weap_amun_process());

	return 1;
}

int lua_actions_up_backpack(lua_State* state){
	LUA_RET_INT(
		Actions::up_backpack(
		LuaCore::getString(state, 1)));
	return 1;
}

int lua_actions_open_depot_chest(lua_State* state){
	LUA_RET_INT(
		DepoterCore::open_depot_chest());
	return 1;
}

int lua_actions_open_container_at_coordinate(lua_State* state){
	Coordinate coord = LuaCore::getPosition(state, 1);
	int32_t stack_pos = 0;
	if (lua_gettop(state) > 1){
		stack_pos = LuaCore::getNumber<int32_t>(state, 1);
	}
	LUA_RET_INT(Actions::open_container_at_coordinate(coord, 3000, stack_pos, false, true));
}

int lua_actions_open_main_bp(lua_State* state){
	LUA_RET_INT(Actions::open_main_bp());
}

int lua_actions_put_ring(lua_State* state){ 
	uint32_t equiped_ring_id = Inventory::get()->body_ring();
	uint32_t ring_id = ItemsManager::get()->getitem_idFromName(LuaCore::getString(state, 1));
	
	if (equiped_ring_id == 0 && equiped_ring_id != ring_id && ring_id != UINT32_MAX){
		LUA_RET_BOOL(Inventory::get()->put_item(ring_id, inventory_slot_t::slot_ring, 100))
	}
	else{
		LUA_RET_BOOL(false);
	}
}
int lua_actions_open_container_id(lua_State* state){
	LUA_RET_INT(
		Actions::open_container_id(
		LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}
int lua_actions_use_item(lua_State* state){
	LUA_RET_INT(
		Actions::use_item(
		LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}
int lua_actions_set_follow(lua_State* state){
	Actions::set_follow();
	return 1;
}
int lua_actions_drop_item(lua_State* state){
	Actions::dropitem(LuaCore::getNumber<uint32_t>(state, 1), LuaCore::getPosition(state, 2), LuaCore::getNumber<uint32_t>(state, 3), LuaCore::getBoolean(state, 4));
	return 1;
}
int lua_actions_unset_follow(lua_State* state){
	Actions::unset_follow();
	return 1;
}
int lua_actions_reach_creature(lua_State* state){
	LUA_RET_INT(
		Actions::reach_creature(
		LuaCore::getString(state, 1)));
	return 1;
}
int lua_actions_deposit_items(lua_State* state){
	DepoterCore::deposit_items(LuaCore::getString(state, 1));
	return 1;
}


int lua_actions_use_exani_hur(lua_State* state){
	exani_hur_t map_t = (exani_hur_t)LuaCore::getNumber<uint32_t>(state, 1);
	side_nom_axis_t::side_nom_axis_t side = (side_nom_axis_t::side_nom_axis_t)LuaCore::getNumber<uint32_t>(state, 2);
	LUA_RET_INT(
		Actions::use_exani_hur(map_t,
		LuaCore::getPosition(state, 1), side));
	return 1;
}
int lua_actions_move_creature(lua_State* state){
	LUA_RET_INT(
		Actions::move_creature(
		LuaCore::getPosition(state, 2)));
	return 1;
}
int lua_actions_deposit_all(lua_State* state){
	LUA_RET_INT(Actions::deposit_all());
	return 1;
}
int lua_actions_transfer_money(lua_State* state){
	LUA_RET_INT(
		Actions::transfer_money(
		LuaCore::getString(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}
int lua_actions_deposit_money(lua_State* state){
	LUA_RET_INT(
		Actions::deposit_money(
		LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}
int lua_actions_withdraw_value(lua_State* state){
	LUA_RET_INT(
		Actions::withdraw_value(
		LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}
int lua_actions_take_up_items_from_depot(lua_State* state){
	LUA_RET_INT(
		Actions::take_up_items_from_depot(
		LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2),
		LuaCore::getNumber<uint32_t>(state, 3),
		LuaCore::getNumber<uint32_t>(state, 4)));
	return 1;
}
int lua_actions_play_alert(lua_State* state){
	//AlertsCore::get()->play_sound();
	return 1;
}

int luaActionsmove_item_container_to_coordinate(lua_State* state){

	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_container_to_coordinate(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getPosition(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_container_to_coordinate(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)));
	}
	else if (args == 4){
		LUA_RET_INT(
			Actions::move_item_container_to_coordinate(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)));
	}

	return 1;
}

int luaActionsmove_item_container_to_container(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_container_to_container(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getContainerPosition(state, 2)
			)
			);
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_container_to_container(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getContainerPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)
			)
			);
	}
	else if (args == 4){
		LUA_RET_INT(
			Actions::move_item_container_to_container(
			LuaCore::getContainerPosition(state, 1),
			LuaCore::getContainerPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)
			)
			);
	}

	return 1;
}

int luaActionsmove_item_container_to_slot(lua_State* state){

	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_container_to_slot(
			LuaCore::getContainerPosition(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_container_to_slot(
			LuaCore::getContainerPosition(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)));
	}
	else if (args == 4){
		LUA_RET_INT(
			Actions::move_item_container_to_slot(
			LuaCore::getContainerPosition(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)));
	}

	return 1;
}

int luaActionsmove_item_coordinate_to_container(lua_State* state){

	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_container(
			LuaCore::getPosition(state, 1),
			LuaCore::getContainerPosition(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_container(
			LuaCore::getPosition(state, 1),
			LuaCore::getContainerPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)));
	}
	else if (args == 4){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_container(
			LuaCore::getPosition(state, 1),
			LuaCore::getContainerPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)));
	}

	return 1;
}

int luaActionsmove_item_coordinate_to_slot(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_slot(
			LuaCore::getPosition(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_slot(
			LuaCore::getPosition(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)));
	}
	else if (args == 4){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_slot(
			LuaCore::getPosition(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)));
	}
	return 1;
	//count, check mte
}

int luaActionsmove_item_coordinate_to_coordinate(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_coordinate(
			LuaCore::getPosition(state, 1),
			LuaCore::getPosition(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_coordinate_to_coordinate(
			LuaCore::getPosition(state, 1),
			LuaCore::getPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)));
	}
	else if (args == 4){
		LUA_RET_INT(Actions::move_item_coordinate_to_coordinate(
			LuaCore::getPosition(state, 1),
			LuaCore::getPosition(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			LuaCore::getNumber<uint32_t>(state, 4)));
	}

	return 1;
}

int luaActionsmove_item_slot_to_slot(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(
			Actions::move_item_slot_to_slot(
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2)));
	}
	else if (args == 3){
		LUA_RET_INT(
			Actions::move_item_slot_to_slot(
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1),
			(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3)));
	}
	else if (args == 4){

	}
	LUA_RET_INT(
		Actions::move_item_slot_to_slot(
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1),
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2),
		LuaCore::getNumber<uint32_t>(state, 3),
		LuaCore::getNumber<uint32_t>(state, 4)));

	return 1;
}

int luaActionsmove_item_slot_to_coordinate(lua_State* state){
	LUA_RET_INT(
		Actions::move_item_slot_to_coordinate(
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getPosition(state, 2)));
	return 1;
}

int luaActionsmove_item_slot_to_container(lua_State* state){
	LUA_RET_INT(
		Actions::move_item_slot_to_container(
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getContainerPosition(state, 2)));
	return 1;
}

int luaActionsuse_item_on_inventory(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_on_inventory(
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}

int luaActionsuse_item_on_container(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_on_container(
		LuaCore::getContainerPosition(state, 1)));
}

int luaActionsuse_item_on_coordinate(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_on_coordinate(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsuse_crosshair_item_on_inventory(lua_State* state){
	LUA_RET_INT(
		Actions::use_crosshair_item_on_inventory(
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}

int luaActionsuse_crosshair_item_on_coordinate(lua_State* state){
	LUA_RET_INT(
		Actions::use_crosshair_item_on_coordinate(
		LuaCore::getPosition(state, 1), LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}

int luaActionsuse_crosshair_item_on_container(lua_State* state){
	LUA_RET_INT(
		Actions::use_crosshair_item_on_container(
		LuaCore::getContainerPosition(state, 1)));
	return 1;
}

int luaActionsuse_item_on_to_coodinate(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_on_to_coodinate(
		LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getPosition(state, 2)));
	return 1;
}

int luaActionsuse_item_on_to_invetory(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_on_to_invetory(
		LuaCore::getNumber<uint32_t>(state, 1),
		(inventory_slot_t)LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}

int luaActionsuse_item_on_to_container(lua_State* state){
	LUA_RET_INT(
		Actions::use_item_on_to_container(
		LuaCore::getContainerPosition(state, 1)));
	return 1;
}

int luaActionsgoto_coord(lua_State* state){
	int args = lua_gettop(state);
	if (args == 2){
		LUA_RET_INT(Actions::goto_coord(LuaCore::getPosition(state, 1), LuaCore::getNumber<uint32_t>(state, 2)))
	}
	else{
		LUA_RET_INT(Actions::goto_coord(LuaCore::getPosition(state, 1)));
	}

	return 1;
}

int luaActionsgoto_coord_on_screen(lua_State* state){
	LuaCore::pushNumber(state,
		Actions::goto_coord_on_screen(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionssimple_go_coord_on_screen(lua_State* state){
	LUA_RET_INT(
		Actions::simple_go_coord_on_screen(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsget_coord_near_other(lua_State* state){
	CoordinatePtr pos = Actions::get_coord_near_other(
		LuaCore::getPosition(state, 1));

	if (!pos)
		return 0;

	LuaCore::pushPosition(state,
		*pos);

	return 1;
}

int luaActionsgoto_near_coord(lua_State* state){
	LUA_RET_INT(
		Actions::goto_near_coord(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsopen_hole_with_shovel(lua_State* state){
	LUA_RET_INT(
		Actions::open_hole_with_shovel(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsclear_coordinate(lua_State* state){
	LUA_RET_INT(
		Actions::clean_coordinate(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsopen_hole_with_tool_id(lua_State* state){
	LUA_RET_INT(
		Actions::open_hole_with_tool_id(
		LuaCore::getPosition(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2)));
	return 1;
}

int luaActionsclear_coord_to_loot(lua_State* state){
	LUA_RET_INT(
		Actions::clear_coord_to_loot(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsdropitem(lua_State* state){
	LUA_RET_INT(
		Actions::dropitem(
		LuaCore::getNumber<uint32_t>(state, 1), TibiaProcess::get_default()->character_info->get_self_coordinate()));
	return 1;
}

int luaActionsdestory_field(lua_State* state){
	LUA_RET_INT(
		Actions::destory_field(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionspick_up_item(lua_State* state){
	LUA_RET_INT(
		Actions::pick_up_item(
		LuaCore::getNumber<uint32_t>(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2),
		LuaCore::getNumber<uint32_t>(state, 3)));
	return 1;
}

int luaActionsfish_process(lua_State* state){
	LuaCore::pushNumber(state,
		Actions::fish_process());
	return 1;
}

int luaActionsuse_fishing_rod(lua_State* state){
	LuaCore::pushNumber(state,
		Actions::use_fishing_rod(
		LuaCore::getPosition(state, 1)));
	return 1;
}

int luaActionsexecute_dance(lua_State* state){
	LuaCore::pushNumber(state,
		Actions::execute_dance());
	return 1;
}

int luaActionseat_food(lua_State* state){
	LUA_RET_INT(Actions::eat_food());
	return 1;
}

int luaActionsopen_depot(lua_State* state){
	LUA_RET_INT(DepoterCore::open_depot());
}

int luaActionssays(lua_State* state){
	if (lua_gettop(state) == 2){
		LUA_RET_INT(Actions::says(LuaCore::getString(state, 1), LuaCore::getString(state, 2)));
	}
	else{
		LUA_RET_INT(Actions::says(LuaCore::getString(state, 1)));
	}
	return 1;
}
int luaActionssays_to_npc(lua_State* state){
	if (lua_gettop(state) == 2){
		LUA_RET_INT(Actions::says_to_npc(LuaCore::getString(state, 1), LuaCore::getString(state, 2)));
	}
	else{
		LUA_RET_INT(Actions::says_to_npc(LuaCore::getString(state, 1)));
	}
	return 1;
}

int luaActionsloggout(lua_State* state){
	Actions::loggout();
	return 0;
}

int luaActionsopen_door(lua_State* state){
	if (lua_gettop(state) == 1)
		LuaCore::pushNumber(state, Actions::open_door(LuaCore::getPosition(state, 1), 0));
	else{
		LuaCore::pushNumber(state,
			Actions::open_door(LuaCore::getPosition(state, 1), LuaCore::getNumber<uint32_t>(state, 2)));
	}
	return 1;
}

int luaActionsturn(lua_State* state){
	LUA_RET_INT(
		Actions::turn(
		(side_nom_axis_t::side_nom_axis_t)LuaCore::getNumber<uint32_t>(state, 1)));
}

int luaActionsstep_side(lua_State* state){
	LUA_RET_INT(
		Actions::step_side(
		(side_t::side_t)LuaCore::getNumber<uint32_t>(state, 1)));
	return 1;
}

int luaActionsrefresh_profession_stuff(lua_State* state){

	LuaCore::pushNumber(state,
		Actions::refresh_profession_stuff());
	return 1;
}

int luaSelfammo(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_rope())
}
int luaSelfmount(lua_State* state){
	TibiaProcess::get_default()->character_info->mount();
	LUA_RET_NULL();
}
int luaSelfunmount(lua_State* state){
	TibiaProcess::get_default()->character_info->unmount();
	LUA_RET_NULL();
}


int luaSelfammoamount(lua_State* state){
	LUA_RET_INT(Inventory::get()->ammunation_count())
}

int luaSelfamulet(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_amulet())
}

int luaSelfarmor(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_armor())
}

int luaSelfaxe(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->axe_lvl())
}

int luaSelfaxepc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->axe_pc())
}

int luaSelfback(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_bag())
}

int luaSelfboots(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_boot())
}

int luaSelfcap(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->cap())
}

int luaSelfclub(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->club_lvl())
}

int luaSelfclubpc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->club_pc())
}

int luaSelfdirection(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_direction())
		return 0;
}

int luaSelfdistance(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->distance_lvl())
}

int luaSelfdistancepc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->distance_pc())
}

int luaSelfexp(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->experience())
}

int luaSelffirst(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->first_lvl())
}

int luaSelffirstpc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->first_pc())
}

int luaSelffishing(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->fishing_lvl())
}

int luaSelffishingpc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->fishing_pc())
}

int luaSelfhelmet(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_helmet())
}

int luaSelfhp(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->hp())
}

int luaSelfhppc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->hppc())
}

int luaSelfid(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->character_id())
}

int luaSelflegs(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_leg());
}

int luaSelflevel(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->level())
}

int luaSelfmax_hp(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->max_hp())
}

int luaSelfmax_mp(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->max_mp())
}

int luaSelfmllevel(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->self_ml())
}

int luaSelfmlevelpc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_level_pc())
}

int luaSelfmp(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->mp())
}

int luaSelfmppc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->mppc())
}

int luaSelfname(lua_State* state){
	LUA_RET_STRING(TibiaProcess::get_default()->character_info->get_name_char())
}

int luaSelfofftrain(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->offline_training())
}

int luaSelfoutfit(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_outfit())
		return 0;
}

int luaSelfposx(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_self_coordinate().x)
}

int luaSelfposy(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_self_coordinate().y)
}

int luaSelfposz(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_self_coordinate().z)
}

int luaSelfprofession(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->get_profession())
}
/*

*/
int luaSelfring(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_ring())
}

int luaSelfshield(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_shield())
}

int luaSelfshielding(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->shielding_lvl())
}

int luaSelfshieldingpc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->shielding_pc())
}

int luaSelfsoul(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->soul())
}

int luaSelfstamina(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->stamina())
}

int luaSelfsword(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->sword_lvl())
}

int luaSelfswordpc(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->sword_pc())
}

int luaSelfvisible(lua_State* state){
	LUA_RET_BOOL(TibiaProcess::get_default()->character_info->is_visible())
		return 0;
}

int luaSelfweapon(lua_State* state){
	LUA_RET_INT(Inventory::get()->body_weapon())
}

int luaSelfweaponamount(lua_State* state){
	LUA_RET_INT(Inventory::get()->weapon_count())
}

int luaSelfcoodinate(lua_State* state){
	LUA_RET_COORDINATE(TibiaProcess::get_default()->character_info->get_self_coordinate())
}

int luaSelfposition(lua_State* state){
	LUA_RET_COORDINATE(TibiaProcess::get_default()->character_info->get_self_coordinate())
}

#pragma region BIND_HOTKEYSTATE
/*
HotkeyState{
string text;
bool auto_cast;
int item_id;
(int)hotkey_cast_type cast_type;
}
*/
int luaPushHokeyState(lua_State* state, HotkeyState* hotkeyState){

	lua_newtable(state);

	lua_pushstring(state, &hotkeyState->getText()[0]);
	lua_setfield(state, -2, "text");

	lua_pushboolean(state, hotkeyState->isAutoCast());
	lua_setfield(state, -2, "auto_cast");

	lua_pushnumber(state, hotkeyState->getitem_id());
	lua_setfield(state, -2, "item_id");

	lua_pushnumber(state, hotkeyState->getCastType());
	lua_setfield(state, -2, "cast_type");
	return 1;
}


//HotkeyState = getHotkey(hotkey_t type);
int luaHotkeyManagergetHotkeyState(lua_State* state){
	HotkeyState hotkeyState = HotkeyManager::get()->getHotkey((hotkey_t)lua_tointeger(state, 1));
	return luaPushHokeyState(state, &hotkeyState);
}

int luaHotkeyManagermissing_hotkeys(lua_State* state){
	std::vector<std::string> retval;
	
	auto HotkeyState = SpellManager::get()->missing_hotkeys();
	for (auto hotkey : HotkeyState)
		retval.push_back(hotkey.first);
	
	LuaPushStringVector(state, retval);
	return 1;
}

//get_hotkey_match(text = "", item_id = 0, cast_type = hotkey_cast_any, auto_cast = false)
int luaHotkeyManagerget_hotkey_match(lua_State* state){
	std::string text; uint32_t item_id = 0; hotkey_cast_type_t cast_type = hotkey_cast_any; bool auto_cast = false;
	int top = lua_gettop(state);
	if (top > 3)
		auto_cast = lua_toboolean(state, 4) != 0;
	if (top > 2)
		cast_type = (hotkey_cast_type_t)lua_tointeger(state, 3);
	if (top > 1)
		item_id = (uint32_t)lua_tonumber(state, 2);

	lua_pushnumber(state, HotkeyManager::get()->get_hotkey_match(lua_tostring(state, 1), item_id, cast_type, auto_cast));
	return 1;
}

//table = query_hotkeys(text = "", item_id = 0, cast_type = hotkey_cast_any, check_auto_cast = false, auto_cast = false)
int luaHotkeyManagerquery_hotkeys(lua_State* state){
	std::string text; uint32_t item_id = 0; hotkey_cast_type_t cast_type = hotkey_cast_any; bool check_auto_cast = false, auto_cast = false;
	int top = lua_gettop(state);
	if (top > 4)
		auto_cast = lua_toboolean(state, 5) != 0;
	if (top > 3)
		check_auto_cast = lua_toboolean(state, 4) != 0;
	if (top > 2)
		cast_type = (hotkey_cast_type_t)lua_tointeger(state, 3);
	if (top > 1)
		item_id = lua_tointeger(state, 2);


	auto retval = HotkeyManager::get()->query_hotkeys((char*)lua_tostring(state, 1), item_id, cast_type,
		check_auto_cast, auto_cast);
	lua_newtable(state);
	int index = 1;
	for (auto ret : retval){
		lua_pushnumber(state, index);
		lua_pushnumber(state, (int)ret);
		lua_settable(state, -3);
	}
	return 1;
}

//string = getCurrentHotkeyProfileName();
int luaHotkeyManagergetCurrentHotkeyProfileName(lua_State* state){
	LUA_RET_STRING(HotkeyManager::get()->getCurrentHotkeyProfileName());
}

//bool = useHotkey(hotkey_t);
int luaHotkeyManageruseHotkey(lua_State* state){
	LUA_RET_BOOL(HotkeyManager::get()->useHotkey((hotkey_t)lua_tointeger(state, 1)));
}

//bool = useHotkeyAtLocation(hotkey_t, Position, item_id = UINT32_MAX, Position axis_displacement = nil);
int luaHotkeyManageruseHotkeyAtLocation(lua_State* state){
	Coordinate displacement;
	uint32_t item_id = UINT32_MAX;

	if (lua_gettop(state) > 3)
		displacement = LuaCore::getPosition(state, 4);
	if (lua_gettop(state) > 2)
		item_id = (uint32_t)lua_tonumber(state, 3);
	LUA_RET_BOOL(HotkeyManager::get()->useHotkeyAtLocation((hotkey_t)lua_tointeger(state, 1),
		LuaCore::getPosition(state, 2), item_id, displacement));
}

//bool = use_item_at_location(item_id, Position, Position axis_displacement = nil)
int luaHotkeyManageruseItemAtLocation(lua_State* state){
	Coordinate displacement;
	if (lua_gettop(state) > 2)
		displacement = LuaCore::getPosition(state, 3);
	LUA_RET_BOOL(HotkeyManager::get()->useItemAtLocation((uint32_t)lua_tonumber(state, 1), LuaCore::getPosition(state, 2), displacement));
}

//bool = toggle_equip_item(item_id)
int luaHotkeyManagertoggleEquipItem(lua_State* state){
	LUA_RET_BOOL(HotkeyManager::get()->toggleEquipItem((uint32_t)lua_tonumber(state, 1)));
}
#pragma endregion


#pragma region BIND_TRADE

int luaNpcTradeis_open(lua_State* state){
	LUA_RET_BOOL(NpcTrade::get()->is_open())
}

int luaNpcTraderesize(lua_State* state){
	LUA_RET_BOOL(NpcTrade::get()->resize())
}

int luaNpcTradesell_item(lua_State* state){
	bool res = NpcTrade::get()->sell_item(LuaCore::getitem_id(state, 1), (int)lua_tonumber(state, 2));

	LUA_RET_BOOL(res)
}

int luaNpcTradebuy_item(lua_State* state){
	LUA_RET_BOOL(NpcTrade::get()->buy_item(LuaCore::getitem_id(state, 1), (int)lua_tonumber(state, 2)))
}


#pragma endregion

void luaPushTile(lua_State* state, TilePtr tile){
	//TODO ADD posicao
	LuaCore::pushUserdata<tile_raw>(state, tile->raw_ptr);
	LuaCore::setMetatable(state, -1, "Tile");
}

void luaPushTile(lua_State* state, Tile* tile){
	//TODO ADD posicao
	LuaCore::pushUserdata<tile_raw>(state, tile->raw_ptr);
	LuaCore::setMetatable(state, -1, "Tile");
}

void luaPushTile(lua_State* state, tile_raw* tile){
	//TODO ADD posicao
	LuaCore::pushUserdata<tile_raw>(state, tile);
	LuaCore::setMetatable(state, -1, "Tile");
}



int luaTileget_destination(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	 LuaCore::pushPosition(state, (Tile(ptr).get_destination()));
	 return 1;
}

int luaTileget_required_pos(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LuaCore::pushPosition(state, Tile(ptr).get_required_pos());
	return 1;
}

int luaTileget_thing_count(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_INT(Tile(ptr).get_thing_count());
}

int luaTileget_top_item_minus_index(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_INT(Tile(ptr).get_top_item_minus_index(LuaCore::getNumber<uint32_t>(state, 2)));
}

int luaTilecontains_item_attr(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).contains_item_attr((attr_item_t)LuaCore::getNumber<uint32_t>(state, 2)));
}


int luaTilecan_use_to_down(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).can_use_to_down());
}


int luaTilecan_use_to_up(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).can_use_to_up());
}


int luaTileis_step(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).is_step());
}


int luaTileis_hole(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).is_hole());
}



int luaTilemust_open_with_browse(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).must_open_with_browse());
}



int luaTilecontains_hole_or_step(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).contains_hole_or_step());
}

int luaTilecontains_npc(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).contains_npc());
}

int luaTilecontains_monster(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).contains_monster());
}

int luaTileoverride_unwalkable(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).override_unwalkable());
}

int luaTileoverride_walkable(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).override_walkable());
}

int luaTilecontains_brokable(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).contains_brokable());
}

int luaTileis_teleport(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).is_teleport());
}

int luaTileavoid_when_targeting(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	LUA_RET_BOOL(Tile(ptr).avoid_when_targeting());
}

int luaTilehave_furniture_at_top(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.have_furniture_at_top())
}

int luaTileget_top_item_id(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	int top_count = lua_gettop(state);
	if (!ptr){
		LUA_RET_NULL()
	}

	bool ignore_creatures = true;

	if (lua_gettop(state) > 1 && lua_isboolean(state, 2)){
		ignore_creatures = lua_toboolean(state, 2) != 0;
	}

	LUA_RET_INT(Tile(ptr).get_top_item_id(ignore_creatures));
}

int luaTileget_top_second_item_id(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_INT(t.get_top_second_item_id())
}

int luaTilehave_element_field_at_top(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.have_element_field_at_top())
}

int luaTilehave_element_field(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.have_element_field())
}

int luaTileget_creatures(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.is_item_id())
		lua_newtable(state);
	auto creatures = t.get_creatures();
	int index = 1;
	for (auto creature : creatures){
		lua_pushnumber(state, index);
		pushCreatureOnBattle(state, creature.second.get());
		lua_settable(state, index);
		index++;
	}
	return 1;
}

int luaTileget_players(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.is_item_id())
		lua_newtable(state);
	auto creatures = t.get_players();
	int index = 1;
	for (auto creature : creatures){
		lua_pushnumber(state, index);
		pushCreatureOnBattle(state, creature.second.get());
		lua_settable(state, index);
		index++;
	}
	return 1;
}

int luaTilecontains_player(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_player())
}

int luaTilecontains_furniture(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_furniture())
}

int luaTilecontains_block_path_id(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_block_path_id())
}

int luaTilecontains_free_path_id(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_free_path_id())
}

int luaTilecontains_id(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_id((uint32_t)lua_tonumber(state, 2)))
}

int luaTilecontains_summon(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_summon())
}

int luaTilecontains_depot(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_depot())
}

int luaTilecontains_creature(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);
	LUA_RET_BOOL(t.contains_creature())
}

int luaTilehas_property(lua_State* state){
	tile_raw* ptr = LuaCore::getUserdata<tile_raw>(state, 1);
	if (!ptr){
		LUA_RET_NULL()
	}
	Tile t(ptr);

	uint8_t type = (uint8_t)lua_tonumber(state, 2);
	LUA_RET_BOOL(t.has_thing_attribute((ThingAttr)type))
}

int luaMapget_walkability(lua_State* state){
	LUA_RET_BOOL(gMapMinimapPtr->get_walkability(LuaCore::getPosition(state, 1), map_type_t::map_front_t))
}

int luaMapget_walkability_map(lua_State* state){
	map_type_t map_t = map_type_t::map_front_t;// (map_type_t)LuaCore::getNumber<uint32_t>(state, 2);
	LUA_RET_BOOL(gMapMinimapPtr->get_walkability_map(LuaCore::getPosition(state, 1),
		map_t, TibiaProcess::get_default()->character_info->get_self_coordinate()))
}

int luaMapadd_block_id(lua_State* state){
	gMapMinimapPtr->add_block_id((uint32_t)lua_tonumber(state, 1));
	return 0;
}

int luaMapadd_release_id(lua_State* state){
	gMapMinimapPtr->add_release_id((uint32_t)lua_tonumber(state, 1));
	return 0;
}

int luaMapremove_block_id(lua_State* state){
	gMapMinimapPtr->remove_block_id((uint32_t)lua_tonumber(state, 1));
	return 0;
}

int luaMapremove_release_id(lua_State* state){
	gMapMinimapPtr->remove_release_id((uint32_t)lua_tonumber(state, 1));
	return 0;
}

int luais_coordinate_yellow(lua_State* state){
	LUA_RET_BOOL(gMapMinimapPtr->is_coordinate_yellow(LuaCore::getPosition(state, 1)));
}

//MAP TILES
//get_raw_tile_at(x,y,z)
//get_raw_tile_at(x,y,z, fix_z_dif = true)
//get_raw_tile_at(Position)
int luaMapget_raw_tile_at(lua_State* state){
	bool fix_z_dif = true;
	int args = lua_gettop(state);

	if (args < 3){
		Position p = LuaCore::getPosition(state, 1);

		if (args > 1){
			fix_z_dif = lua_toboolean(state, 2) != 0;
		}

		tile_raw* rtile = gMapTilesPtr->get_raw_tile_at(p.x, p.y, p.z, fix_z_dif);
		if (!rtile){
			LUA_RET_NULL();
		}

		luaPushTile(state, rtile);
	}
	else if (args > 2){
		if (args > 3){
			fix_z_dif = lua_toboolean(state, 4) != 0;
		}
		luaPushTile(state, gMapTilesPtr->get_raw_tile_at((uint32_t)lua_tonumber(state, 1),
			(uint32_t)lua_tonumber(state, 2), (int8_t)lua_tonumber(state, 3), fix_z_dif));
	}

	return 1;
}

int luaMapget_max_visible_level_up(lua_State* state){
	LUA_RET_INT(gMapTilesPtr->get_max_visible_level_up());
}

int luaMapget_max_visible_level_down(lua_State* state){
	LUA_RET_INT(gMapTilesPtr->get_max_visible_level_down());
}

//get_nearest_depot_tile(optional check_reachable = true)
int luaMapget_nearest_depot_tile(lua_State* state){
	if (lua_gettop(state) == 0)
		luaPushTile(state, gMapTilesPtr->get_nearest_depot_tile(lua_toboolean(state, 1) != 0));
	else
		luaPushTile(state, gMapTilesPtr->get_nearest_depot_tile());
	return 1;
}
//get_tile_nearest_coordinate(Position, optional check reachable = true, optional sqm_consider_near = 1)
int luaMapget_tile_nearest_coordinate(lua_State* state){
	Coordinate coord = LuaCore::getPosition(state, 1);
	bool check_reachable = true;
	int sqm_consider_near = 1;
	if (lua_gettop(state) > 1)
		check_reachable = lua_toboolean(state, 2) != 0;
	if (lua_gettop(state) > 2)
		sqm_consider_near = (int)lua_tonumber(state, 3);

	luaPushTile(state, gMapTilesPtr->get_tile_nearest_coordinate(coord, check_reachable, map_front_mini_t, sqm_consider_near));
	return 1;
}
//get_tiles_with_id(item_id)
int luaMapget_tiles_with_id(lua_State* state){
	auto tiles = gMapTilesPtr->get_tiles_with_id((uint32_t)lua_tonumber(state, 1));

	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;
}
//get_tiles_with_top_id(item_id, optional_ignore_player = false)
int luaMapget_tiles_with_top_id(lua_State* state){
	bool ignore_player = false;
	if (lua_gettop(state) > 1)
		ignore_player = lua_toboolean(state, 2) != 0;

	auto tiles = gMapTilesPtr->get_tiles_with_top_id((uint32_t)lua_tonumber(state, 1), ignore_player);

	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;

}
//get_tiles_with_id_near_to(item_id, Coordinate coord, optional max_distance = 1)
int luaMapget_tiles_with_id_near_to(lua_State* state){
	int max_dist = 1;
	if (lua_gettop(state) > 2)
		max_dist = (int)lua_tonumber(state, 3);

	auto tiles = gMapTilesPtr->get_tiles_with_id_near_to((uint32_t)lua_tonumber(state, 1), LuaCore::getPosition(state, 2), max_dist);

	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;
}

//get_depot_tiles()
int luaMapget_depot_tiles(lua_State* state){
	auto tiles = gMapTilesPtr->get_depot_tiles();
	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;
}

int luaMapget_furniture_tiles(lua_State* state){
	auto tiles = gMapTilesPtr->get_furniture_tiles();
	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;
}

int luaMapget_creature_tiles(lua_State* state){
	auto tiles = gMapTilesPtr->get_creature_tiles();
	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;
}

int luaMapget_player_tiles(lua_State* state){
	auto tiles = gMapTilesPtr->get_player_tiles();
	int index = 1;
	lua_newtable(state);
	for (auto tile : tiles){
		lua_pushnumber(state, index);
		luaPushTile(state, tile);
		lua_settable(state, -3);
	}
	return 1;
}

//contains_furniture_at(Position)
int luaMapcontains_furniture_at(lua_State* state){
	Position pos = LuaCore::getPosition(state, 1);
	LUA_RET_BOOL(gMapTilesPtr->contains_furniture_at(LuaCore::getPosition(state, 1)))
}

//find_coordinate_with_top_id(item_id, optional ignore_player)
int luaMapfind_coordinate_with_top_id(lua_State* state){
	bool ignore_player = false;
	if (lua_gettop(state) > 1)
		ignore_player = lua_toboolean(state, 2) != 0;

	LuaCore::pushPosition(state, gMapTilesPtr->
		find_coordinate_with_top_id((uint32_t)lua_tonumber(state, 1), map_front_mini_t, ignore_player));
	return 1;
}

int luaMapfind_coordinate_contains_id(lua_State* state){
	LuaCore::pushPosition(state, gMapTilesPtr->
		find_coordinate_contains_id((uint32_t)lua_tonumber(state, 1), map_front_mini_t));
	return 1;
}

int luaMapcoordinate_is_traped(lua_State* state){
	LUA_RET_BOOL(gMapTilesPtr->coordinate_is_traped(LuaCore::getPosition(state, 1)))
}

int luaMapis_traped_on_screen(lua_State* state){
	LUA_RET_BOOL(gMapTilesPtr->is_traped_on_screen())
}
//contains_furniture_at(optional check_z = false)
int luaMapcheck_sight_line(lua_State* state){
	if (lua_gettop(state) > 0){
		LUA_RET_BOOL(gMapTilesPtr->check_sight_line(LuaCore::getPosition(state, 1), LuaCore::getPosition(state, 2), lua_toboolean(state, 3) != 0))
	}
	else{
		LUA_RET_BOOL(gMapTilesPtr->check_sight_line(LuaCore::getPosition(state, 1), LuaCore::getPosition(state, 2)))
	}
	return 1;
}
int luaMapis_wait_refresh(lua_State* state){
	MapTiles::wait_refresh();
	LUA_RET_NULL();
}
int luaMapis_cordinate_onscreen(lua_State* state){
	LUA_RET_BOOL(MapTiles::is_cordinate_onscreen(LuaCore::getPosition(state, 1)))
}

void luaPushVipPlayer(lua_State* state, VipPlayer* vip){
	lua_newtable(state);
	lua_pushstring(state, &vip->get_name()[0]);
	lua_setfield(state, -2, "name");

	lua_pushstring(state, &vip->get_desc()[0]);
	lua_setfield(state, -2, "description");

	lua_pushboolean(state, vip->notify_at_login);
	lua_setfield(state, -2, "notify_at_login");

	lua_pushnumber(state, vip->type);
	lua_setfield(state, -2, "type");
}

int luaVipListget_vip_by_player_name(lua_State* state){
	if (!lua_gettop(state)){
		lua_pushnil(state);
		return 1;
	}

	auto vip = VipList::get()->get_vip_by_player_name(LuaCore::getString(state, 1));
	if (!vip){
		lua_pushnil(state);
		return 1;
	}
	luaPushVipPlayer(state, vip.get());
	return 1;
}

int luaVipListget_vip_players(lua_State* state){
	auto vips = VipList::get()->get_vip_players();
	lua_newtable(state);
	int index = 1;
	for (auto vip : vips){
		lua_pushnumber(state, index);
		luaPushVipPlayer(state, vip.get());
		lua_settable(state, -3);
		index++;
	}
	return 1;
}

void LuaCore::pushNumber(lua_State* state, int val){
	lua_pushnumber(state, val);
}

void LuaCore::pushBoolean(lua_State* state, bool val){
	lua_pushboolean(state, val ? 1 : 0);
}

void LuaCore::pushString(lua_State*state, std::string val){
	lua_pushstring(state, &val[0]);
}

void LuaCore::pushPosition(lua_State* state, Position& position){
	lua_createtable(state, 0, 3);

	pushNumber(state, position.x);
	lua_setfield(state, -2, "x");

	pushNumber(state, position.y);
	lua_setfield(state, -2, "y");

	pushNumber(state, position.z);
	lua_setfield(state, -2, "z");
}

void LuaCore::pushRect(lua_State* state, Rect rect){
	lua_push_hud_rect(state, HUDRECT(rect.get_x(), rect.get_y(), rect.get_width(), rect.get_height()));
}

void LuaCore::pushLine(lua_State* state, std::pair<Position, Position> line_pair){
	lua_newtable(state);
	LuaCore::pushPosition(state, Position(line_pair.first.x, line_pair.first.y));
	lua_setfield(state, -2, "start");
	LuaCore::pushPosition(state, Position(line_pair.second.x, line_pair.second.y));
	lua_setfield(state, -2, "end");
}


void LuaCore::pushLine(lua_State* state, std::pair<Point, Point> line_pair){
	pushLine(state, std::pair<Position, Position>(Position(line_pair.first), Position(line_pair.second)));
}

void LuaCore::pushNil(lua_State* state){
	lua_pushnil(state);
}

uint32_t LuaCore::getitem_id(lua_State* state, int32_t arg){
	uint32_t id = UINT32_MAX;
	if (lua_isnumber(state, arg))
		id = (uint32_t)lua_tonumber(state, arg);
	else if (lua_isstring(state, arg)){
		std::string item_as_string = lua_tostring(state, arg);
		try{
			id = boost::lexical_cast<uint32_t>(item_as_string);
		}
		catch (...){
			id = ItemsManager::get()->getitem_idFromName(item_as_string);
		}
	}
	return id;
}

std::string LuaCore::getString(lua_State* state, int32_t arg){
	size_t len;
	const char* c_str = lua_tolstring(state, arg, &len);
	if (!c_str || len == 0) {
		return std::string();
	}
	return std::string(c_str, len);
}

Position LuaCore::getPosition(lua_State* state, int32_t arg){
	Position position;
	position.x = (uint32_t)getField<uint32_t>(state, arg, "x");
	position.y = (uint32_t)getField<uint32_t>(state, arg, "y");
	position.z = (int32_t)getField<int32_t>(state, arg, "z");

	lua_pop(state, 3);
	return position;
}

bool LuaCore::getBoolean(lua_State* state, int32_t arg){
	return lua_toboolean(state, arg) != 0;
}

void LuaCore::pushContainer(lua_State* state, ItemContainer* container){
	lua_newtable(state);

	pushNumber(state, container->header_info->bp_item_id);
	lua_setfield(state, -2, "id");

	pushNumber(state, container->header_info->total_slots);
	lua_setfield(state, -2, "total_slots");

	pushNumber(state, container->header_info->used_slots);
	lua_setfield(state, -2, "used_slots");

	lua_pushstring(state, &container->header_info->get_caption()[0]);
	lua_setfield(state, -2, "caption");

	lua_newtable(state);
	lua_pushstring(state, "slots");
	for (uint32_t i = 0; i < container->header_info->used_slots; i++){
		lua_pushnumber(state, i);
		lua_newtable(state);//slot
		lua_pushstring(state, "item_id");//key
		lua_pushnumber(state, container->slots[i].id);
		lua_settable(state, -3);

		lua_pushstring(state, "count");//key
		lua_pushnumber(state, container->slots[i].stack_count);
		lua_settable(state, -3);

		lua_settable(state, -4);
	}
	lua_settable(state, -3);
}

ContainerPosition LuaCore::getContainerPosition(lua_State* state, int32_t arg){
	ContainerPosition container_position;
	container_position.container_index = getField<uint16_t>(state, arg, "container_index");
	container_position.slot_index = getField<uint16_t>(state, arg, "slot_index");

	lua_pop(state, 2);
	return container_position;
}

void LuaCore::setMetatable(lua_State* state, int32_t index, std::string string){
	luaL_getmetatable(state, string.c_str());
	lua_setmetatable(state, index - 1);
}

int luaThreadsleep(lua_State* state){
	Sleep((DWORD)lua_tonumber(state, 1));
	return 0;
}

//bool is_lying_corpse(int id)
int luaItemsManageris_lying_corpse(lua_State* state){
	LUA_RET_BOOL(ItemsManager::get()->is_lying_corpse((uint32_t)lua_tonumber(state, 1)))
}
int luaItemsManagergetitem_idFromName(lua_State* state){
	LUA_RET_INT(ItemsManager::get()->getitem_idFromName((std::string)lua_tostring(state, 1)))
}
int luaItemsManagergetItemNameFromId(lua_State* state){
	LUA_RET_STRING(ItemsManager::get()->getItemNameFromId((uint32_t)lua_tonumber(state, 1)))
}
int lua_ItemsManager_isitem_id(lua_State* state){
	LUA_RET_BOOL(ItemsManager::get()->isitem_id((uint32_t)lua_tonumber(state, 1)))
}
int lua_ItemsManager_isRuneFieldItem(lua_State* state){
	LUA_RET_BOOL(ItemsManager::get()->isRuneFieldItem((uint32_t)lua_tonumber(state, 1)))
}
int lua_ItemsManager_getItemPrice(lua_State* state){
	LUA_RET_INT(ItemsManager::get()->getItemPrice((uint32_t)lua_tonumber(state, 1)))
}
void pushWaypointInfo(lua_State* state, WaypointInfo* info){
	if (!info){
		lua_pushnil(state);
		return;
	}
	lua_newtable(state);
	lua_pushnumber(state, (int)info->get_action());
	lua_setfield(state, -2, "type");

	lua_pushstring(state, &info->get_label()[0]);
	lua_setfield(state, -2, "label");

	LuaCore::pushPosition(state, info->get_destination_coord());
	lua_setfield(state, -2, "position");

	lua_newtable(state);
	auto additional_info_copy = info->getadditionalInfoCopy();
	for (auto additional_info = additional_info_copy.begin(); additional_info != additional_info_copy.end();
		additional_info++){
		lua_pushstring(state, &additional_info->second[0]);
		lua_setfield(state, -2, &additional_info->first[0]);
	}
	lua_setfield(state, -2, "additional_info");
}

void pushWaypointPath(lua_State* state, WaypointPath* path){
	lua_newtable(state);
	lua_pushnumber(state, (int)path);
	lua_setfield(state, -2, "internal_raw_pointer");
	LuaCore::setMetatable(state, -1, "WaypointPath");
}

void pushActionInfo(lua_State* state, actionInfo* info){
	lua_newtable(state);
	LuaCore::pushString(state, info->get_id());
	lua_setfield(state, -2, "id");
	LuaCore::pushString(state, info->get_text());
	lua_setfield(state, -2, "text");
	LuaCore::pushString(state, info->get_type());
	lua_setfield(state, -2, "type");
}

std::shared_ptr<WaypointPath> luaGetWaypointPathFromStack(lua_State* state, int index){
	if (!lua_istable(state, index))
		return nullptr;

	WaypointPath* raw_ptr = (WaypointPath*)LuaCore::getField<uint32_t>(state, index, "internal_raw_pointer");
	if (!raw_ptr)
		return nullptr;

	return WaypointManager::get()->get_waypoint_path_by_raw_pointer(raw_ptr);
}

int luaMetatableWaypointPathgetName(lua_State* state){
	std::shared_ptr<WaypointPath> path = luaGetWaypointPathFromStack(state, -1);
	if (path){
		LUA_RET_STRING(path->getName());
	}
	LUA_RET_NULL()
}

int luaMetatableWaypointPathgetWaypointInfoIdByLabel(lua_State* state){
	std::shared_ptr<WaypointPath> path = luaGetWaypointPathFromStack(state, -1);
	if (path){
		pushWaypointInfo(state, path->getWaypointInfoByLabel(LuaCore::getString(state, 1)).get());
		return 1;
	}
	LUA_RET_NULL()
}

int luaMetatableWaypointPathgetwaypointInfoList(lua_State* state){
	std::shared_ptr<WaypointPath> path = luaGetWaypointPathFromStack(state, -1);
	if (path){
		lua_newtable(state);
		auto list_of_waypoints = path->getwaypointInfoList();
		int index = 1;
		for (auto waypoint_info = list_of_waypoints.begin(); waypoint_info != list_of_waypoints.end(); waypoint_info++){
			pushWaypointInfo(state, waypoint_info->get());
			lua_rawseti(state, -2, index);
			index++;
		}
		return 1;
	}
	LUA_RET_NULL()
}

int luaWaypointManagerenabled(lua_State* state){
	if (lua_gettop(state) == 1){
		WaypointManager::get()->set_enabled(LuaCore::getBoolean(state, 1));
	}
	else {
		LUA_RET_BOOL(WaypointManager::get()->get_enabled());
	}
	return 0;
}

int luaWaypointManagerlastWaypointInfoId(lua_State* state){
	std::shared_ptr<WaypointInfo> info = WaypointManager::get()->getLastWaypointInfo();
	if (!info)
		return 0;
	pushWaypointInfo(state, info.get());
	return 1;
}

int luaWaypointManagerlastWaypointPathId(lua_State* state){
	LUA_RET_INT(WaypointManager::get()->get_lastWaypointPathId());
}

int luaWaypointManagercurrentWaypointPathId(lua_State* state){
	LUA_RET_INT(WaypointManager::get()->get_currentWaypointPathId());
}

int luaWaypointManagergetcurrentWaypointInfoId(lua_State* state){
	LUA_RET_INT(WaypointManager::get()->getcurrentWaypointInfoId());
}

int luaWaypointManagergetCurrentWaypointPath(lua_State* state){
	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getCurrentWaypointPath();
	if (!current_path)
	{
		LUA_RET_NULL();
	}
	pushWaypointPath(state, current_path.get());
	return 1;
}

int luaWaypointManagersetCurrentWaypointPathByLabel(lua_State* state){
	WaypointManager::get()->setCurrentWaypointPathByLabel(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	return 0;
}

int luaWaypointManagersetCurrentWaypointPath(lua_State* state){
	if (lua_gettop(state) == 1){
		WaypointManager::get()->setCurrentWaypointPathByLabel(LuaCore::getString(state, 1), "");
	}
	else{
		WaypointManager::get()->setCurrentWaypointPathByLabel(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	}

	return 0;
}

int luaWaypointManagergetActionInfoList(lua_State* state){
	auto info_list = WaypointManager::get()->getActionInfoList(LuaCore::getNumber<uint32_t>(state, 1));
	lua_newtable(state);
	for (auto info_list_item = info_list.begin(); info_list_item != info_list.end(); info_list_item++){
		pushActionInfo(state, info_list_item->second.get());
		lua_rawseti(state, -2, info_list_item->first);
	}
	return 1;
}

int luaWaypointManagergetLastWaypointPath(lua_State* state){
	auto last = WaypointManager::get()->getLastWaypointPath();
	if (last)
		pushWaypointPath(state, last.get());
	else
		LuaCore::pushNil(state);
	return 1;
}

int luaWaypointManagergetWaypointPath(lua_State* state){
	auto path = WaypointManager::get()->getWaypointPathByLabel(LuaCore::getString(state, 1));
	if (path)
		pushWaypointPath(state, path.get());
	else{
		LUA_RET_NULL()
	}
	return 0;
}
int luaWaypointManagerremoveWaypointPath(lua_State* state){

	WaypointManager::get()->removeWaypointPathByLabel(LuaCore::getString(state, 1));
	return 0;
}
int luaWaypointManagergetwaypointPathList(lua_State* state){
	lua_newtable(state);
	int index = 1;
	auto list_of_paths = WaypointManager::get()->getwaypointPathList();
	for (auto it = list_of_paths.begin(); it != list_of_paths.end(); it++){
		pushWaypointPath(state, it->second.get());
		lua_rawseti(state, -2, index);
		index++;
	}
	return 1;
}

int luaWaypointManageradvanceCurrentWaypointInfo(lua_State* state){
	WaypointManager::get()->advanceCurrentWaypointInfo();
	return 0;
}
int luaWaypointManagergetCurrentWaypointInfo(lua_State* state){
	auto current = WaypointManager::get()->getCurrentWaypointInfo();
	pushWaypointInfo(state, current.get());
	return 1;
}
int luaWaypointManagergetLastWaypointInfo(lua_State* state){
	auto last = WaypointManager::get()->getLastWaypointInfo();
	pushWaypointInfo(state, last.get());
	return 1;
}
int luaWaypointManagergetBeforeWaypointInfo(lua_State* state){
	auto before = WaypointManager::get()->getBeforeWaypointInfo();
	pushWaypointInfo(state, before.get());
	return 1;
}
int luaWaypointManagergetNextWapointInfo(lua_State* state){
	auto next = WaypointManager::get()->getNextWapointInfo();
	pushWaypointInfo(state, next.get());
	return 1;
}

int luaMessageBox(lua_State* state){
	if (lua_gettop(state) > 1){
		MessageBox(0, &(LuaCore::getString(state, 1))[0], &(LuaCore::getString(state, 2))[0], MB_OK);
	}
	else if (lua_gettop(state) == 1){
		MessageBox(0, &(LuaCore::getString(state, 1))[0], "", MB_OK);
	}
	return 0;
}

int luaAddErrorLog(lua_State* state){
	//todo
	return 0;
}

int luaPrintConsole(lua_State* state){	
	std::string temp = "\n PRINT : " + LuaCore::getString(state, 1);
	LuaHandler::get()->add_lua_error_log(temp);
	return 0;
}

int luaNotificationBaloon(lua_State* state){
	std::string caption = "Notification";
	std::string message = "No message";
	uint32_t args = lua_gettop(state);
	if (args == 1){
		message = LuaCore::getString(state, 1);
	}
	else if (args == 2){
		caption = LuaCore::getString(state, 2);
	}
	InfoCore::get()->notify(&message[0], &caption[0]);
	return 0;
}


int luaWaypointManagersetCurrentWaypointInfo(lua_State* state){
	WaypointManager::get()->setCurrentWaypointInfo(LuaCore::getString(state, 1));
	return 0;
}


int luaKeywordManageraddTableDescription(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->addTableDescription(LuaCore::getString(state, 1), LuaCore::getString(state, 2)));
}

int luaKeywordManageraddFunctionDescription(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->addFunctionDescription(LuaCore::getString(state, 1), LuaCore::getString(state, 2)));
}

int luaKeywordManageraddFunctionSuggestion(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->addFunctionSuggestion(LuaCore::getString(state, 1), LuaCore::getString(state, 2)));
}

int luaKeywordManageraddMethodDescription(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->addMethodDescription(LuaCore::getString(state, 1), LuaCore::getString(state, 2), LuaCore::getString(state, 3)));
}

int luaKeywordManageraddMethodSuggestion(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->addMethodSuggestion(LuaCore::getString(state, 1), LuaCore::getString(state, 2), LuaCore::getString(state, 3)));
}

int luaKeywordManagerregisterClass(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->registerClass(LuaCore::getString(state, 1)));
}

int luaKeywordManagerregisterEnumTable(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->registerEnumTable(LuaCore::getString(state, 1)));
}

int luaKeywordManagerregisterTableMethod(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->registerTableMethod(LuaCore::getString(state, 1), LuaCore::getString(state, 2)));
}

int luaKeywordManagerregisterFunction(lua_State* state){
	LUA_RET_BOOL(KeywordManager::get()->registerFunction(LuaCore::getString(state, 1)));
}

int luaKeywordManagersetCurrentParsingLib(lua_State* state){
	KeywordManager::get()->set_lib_parsing(LuaCore::getString(state, 1));
	LUA_RET_NULL()
}

int luaKeywordManageraddTableVariableDescription(lua_State* state){
	KeywordManager::get()->addTableVariableDescription(LuaCore::getString(state, 1), LuaCore::getString(state, 2), LuaCore::getString(state, 3));
	LUA_RET_NULL()
}

int luaKeywordManagerregisterTableVariable(lua_State* state){
	KeywordManager::get()->registerTableVariable(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	LUA_RET_NULL()
}

int luaKeywordManagerregisterTable(lua_State* state){
	KeywordManager::get()->registerTable(LuaCore::getString(state, 1));
	LUA_RET_NULL()
}

int luaKeywordManagerregisterMetatable(lua_State* state){
	KeywordManager::get()->registerMetatable(LuaCore::getString(state, 1));
	LUA_RET_NULL()
}

int luaKeywordManagerregisterVariable(lua_State* state){
	KeywordManager::get()->addVariable(LuaCore::getString(state, 1));
	LUA_RET_NULL()
}

int luaKeywordManageraddVariableDescription(lua_State* state){
	KeywordManager::get()->addVariableDescription(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	LUA_RET_NULL()
}
bool fexists(const std::string& filename) {
	std::ifstream ifile(filename.c_str());
	return (bool)ifile;
}

int luaNeutralManagerloadscript(lua_State* state){
	std::string filename = LuaCore::getString(state, 1);

	NeutralManager::get()->set_bot_state(false);

	Sleep(500);

	NeutralManager::get()->load_script(filename);

	Sleep(500);

	NeutralManager::get()->set_bool_load(true);

	LUA_RET_NULL();
}

int luaNeutralManagerget_health_spells_state(lua_State* state){
	LUA_RET_BOOL(SpellManager::get()->get_health_spells_state());
}
int luaNeutralManagerget_attack_spells_state(lua_State* state){
	LUA_RET_BOOL(SpellManager::get()->get_attack_spells_state());
}
int luaNeutralManagerset_health_spells_state(lua_State* state){
	SpellManager::get()->set_health_spells_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_attack_spells_state(lua_State* state){
	SpellManager::get()->set_attack_spells_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}

int luaNeutralManagerscreenshot(lua_State* state){	
	if (lua_gettop(state) < 1)
		ScreenShoot::get()->take_screen_shot();	
	else
		ScreenShoot::get()->take_screen_shot(LuaCore::getString(state, 1));

	LUA_RET_NULL()
}
int luaNeutralManagerget_alerts_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_alerts_state())
}
int luaNeutralManagerget_info_time_bot_running(lua_State* state){
	LUA_RET_INT(InfoCore::get()->get_info_time_bot_running())
}

int luaNeutralManagerget_bot_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_bot_state())
}

int luaNeutralManagerget_hunter_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_hunter_state())
}

int luaNeutralManagerget_looter_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_looter_state())
}

int luaNeutralManagerget_lua_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_lua_state())
}

int luaNeutralManagerget_pause_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_pause_state())
}

int luaNeutralManagerget_spellcaster_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_spellcaster_state())
}

int luaNeutralManagerget_waypointer_state(lua_State* state){
	LUA_RET_BOOL(NeutralManager::get()->get_waypointer_state())
}

int luaNeutralManagerhide_bot_interface(lua_State* state){
	NeutralManager::get()->hide_bot_interface();
	LUA_RET_NULL()
}
int luaNeutralManagerminimize_bot_interface(lua_State* state){
	NeutralManager::get()->minimize_bot_interface();
	LUA_RET_NULL()
}
int luaNeutralManagerrestore_bot_interface(lua_State* state){
	NeutralManager::get()->restore_bot_interface();
	LUA_RET_NULL()
}
int luaNeutralManagerrequest_close_bot_process(lua_State* state){
	NeutralManager::get()->request_close_bot_process();
	LUA_RET_NULL()
}
int luaNeutralManagerpause(lua_State* state){
	NeutralManager::get()->pause();
	LUA_RET_NULL()
}
int luaNeutralManagerkill_process(lua_State* state){
	TibiaProcess::get_default()->kill();
	LUA_RET_NULL()
}
int luaNeutralManagerkill_bot_process(lua_State* state){
	NeutralManager::get()->kill_bot_process();
	LUA_RET_NULL()
}
int luaNeutralManagerset_alerts_state(lua_State* state){
	NeutralManager::get()->set_alerts_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_bot_state(lua_State* state){
	NeutralManager::get()->set_bot_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_hunter_state(lua_State* state){
	NeutralManager::get()->set_hunter_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_looter_state(lua_State* state){
	NeutralManager::get()->set_looter_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_lua_state(lua_State* state){
	NeutralManager::get()->set_lua_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_pause_state(lua_State* state){
	NeutralManager::get()->set_pause_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_spellcaster_state(lua_State* state){
	NeutralManager::get()->set_spellcaster_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaNeutralManagerset_waypointer_state(lua_State* state){
	NeutralManager::get()->set_waypointer_state(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaHunterManagerneed_operation(lua_State* state){
	LUA_RET_BOOL(HunterCore::get()->need_operate());
}
int luaHunterManagerdisable_group(lua_State* state){
	HunterManager::get()->disable_group(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL()
}
int luaHunterManagerenable_group(lua_State* state){
	HunterManager::get()->enable_group(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL()
}
int luaSpellManagerdisable_spell_by_id(lua_State* state){
	SpellManager::get()->disable_spell_by_id(LuaCore::getString(state, 1));
	LUA_RET_NULL()
}
int luaSpellManagerenable_spell_by_id(lua_State* state){
	SpellManager::get()->enable_spell_by_id(LuaCore::getString(state, 1));
	LUA_RET_NULL()
}

int luaLooterManagerwait_loot_complete(lua_State* state){
	uint32_t timeout = 10000;
	if (lua_gettop(state) > 0 && lua_isnumber(state, 1)){
		timeout = LuaCore::getNumber<uint32_t>(state, 1);
	}
	LUA_RET_BOOL(LooterCore::get()->wait_loot_complete(timeout));
}

int luaLooterManagerwait_pulse_out(lua_State* state){
	uint32_t timeout = 10000;
	if (lua_gettop(state) > 0 && lua_isnumber(state, 1)){
		timeout = LuaCore::getNumber<uint32_t>(state, 1);
	}
	LUA_RET_BOOL(LooterCore::get()->wait_pulse_out(timeout));
}

int luaLooterManagerwait_pulse(lua_State* state){
	uint32_t timeout = 10000;
	if (lua_gettop(state) > 0 && lua_isnumber(state, 1)){
		timeout = LuaCore::getNumber<uint32_t>(state, 1);
	}
	LUA_RET_BOOL(LooterCore::get()->wait_pulse(timeout));
}


int luaLooterManagerpulse(lua_State* state){
	LooterCore::get()->pulse();
	LUA_RET_NULL()
}


int luaLooterManagerneed_operate(lua_State* state){
	LUA_RET_BOOL(LooterCore::get()->need_operate());
}
int luaLooterManagerdisable_loot_group(lua_State* state){
	LooterManager::get()->disable_loot_group(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL()
}
int luaLooterManagerenable_loot_group(lua_State* state){
	LooterManager::get()->enable_loot_group(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL()
}
int luaLooterManagerset_loots_bag_id(lua_State* state){
	LooterManager::get()->set_loots_bag_id(LuaCore::getString(state, 1), LuaCore::getNumber<uint32_t>(state, 2));
	LUA_RET_NULL()
}
int luaLooterManagerget_loots_bag_id(lua_State* state){
	LUA_RET_INT(LooterManager::get()->get_loots_bag_id(LuaCore::getString(state, 1)))
}

int luaLooterManagerset_group_state(lua_State* state){
	LooterManager::get()->set_group_state(LuaCore::getNumber<uint32_t>(state, 1), LuaCore::getBoolean(state, 1));
	LUA_RET_NULL()
}
int luaLooterManagerget_loot_group_state(lua_State* state){
	LUA_RET_BOOL(LooterManager::get()->get_loot_group_state(LuaCore::getNumber<uint32_t>(state, 1)))
}
int luaLooterManagerset_loots_bag_name(lua_State* state){
	LooterManager::get()->set_loots_bag_id(LuaCore::getString(state, 1), LuaCore::getNumber<uint32_t>(state, 2));
	LUA_RET_NULL()
}
int luaLooterManagerget_loots_bag_item_id(lua_State* state){
	LUA_RET_INT(LooterManager::get()->get_loots_bag_item_id(LuaCore::getString(state, 1)))
}
int luaLooterManagerget_item_destination_id(lua_State* state){
	LUA_RET_INT(LooterManager::get()->get_item_destination_id(LuaCore::getNumber<uint32_t>(state, 1)))
}
int luaLooterManagerget_current_filter_type(lua_State* state){
	LUA_RET_INT(LooterManager::get()->get_current_filter_type())
}
int luaLooterManagerget_current_sequence_type(lua_State* state){
	LUA_RET_INT(LooterManager::get()->get_current_sequence_type())
}
int luaLooterManagerset_current_filter_type(lua_State* state){
	LooterManager::get()->set_current_filter_type((loot_filter_t)LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL()
}
int luaLooterManagerset_current_sequence_type(lua_State* state){
	LooterManager::get()->set_current_sequence_type((loot_sequence_t)LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL()
}
int luaLuaManagergetLuaCode(lua_State* state){
	LUA_RET_STRING(LuaBackgroundManager::get()->getLuaCode(LuaCore::getString(state, 1)))
}

int luaLuaManagersetLuaScriptDelay(lua_State* state){
	LuaBackgroundManager::get()->setLuaScriptDelay(LuaCore::getString(state, 1), LuaCore::getNumber<uint32_t>(state, 2));
	LUA_RET_NULL()
}
int lua_travel(lua_State* state){
	Actions::travel(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	LUA_RET_NULL()
}
int lua_open_browse_field(lua_State* state){
	Coordinate coord = LuaCore::getPosition(state, 1);
	LUA_RET_BOOL(FloatingMiniMenu::open_browse_field_at_coordinate(coord))
}

//Actions.open_browse_field(Self.position())


int luaget_lua_script_active(lua_State* state){
	bool temp = LuaBackgroundManager::get()->getLuaScriptActive(LuaCore::getString(state, 1));
	LUA_RET_BOOL(temp);
}

int luaset_lua_script_active(lua_State* state){
	LuaBackgroundManager::get()->updateActive(LuaCore::getString(state, 1), LuaCore::getBoolean(state, 2));
	LUA_RET_NULL()
}

int luaLuaManagersetLuaScriptCode(lua_State* state){
	LuaBackgroundManager::get()->setLuaScriptCode(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	LUA_RET_NULL()
}
int luaLuaManagergetLuaScriptDelay(lua_State* state){
	LUA_RET_INT(LuaBackgroundManager::get()->getLuaScriptDelay(LuaCore::getString(state, 1)))
}
void lua_push_hud_coord(lua_State* state, HUDCOORD coord){
	lua_newtable(state);
	lua_pushnumber(state, coord.X);
	lua_setfield(state, -2, "x");
	lua_pushnumber(state, coord.Y);
	lua_setfield(state, -2, "y");
}

std::shared_ptr<HUDCOORD> lua_get_hud_coord(lua_State* state, uint32_t arg_place){
	std::shared_ptr<HUDCOORD> retval;
	if (lua_istable(state, arg_place)){
		retval = std::shared_ptr<HUDCOORD>(new HUDCOORD);
		retval->X = LuaCore::getField<uint32_t>(state, arg_place, "x");
		retval->Y = LuaCore::getField<uint32_t>(state, arg_place, "y");
	}
	return retval;
}

void lua_push_hud_argb(lua_State* state, HUDARGB color){
	lua_newtable(state);
	lua_pushnumber(state, color.a);
	lua_setfield(state, -2, "a");
	lua_pushnumber(state, color.r);
	lua_setfield(state, -2, "r");
	lua_pushnumber(state, color.g);
	lua_setfield(state, -2, "g");
	lua_pushnumber(state, color.b);
	lua_setfield(state, -2, "b");
}

std::shared_ptr<HUDARGB> lua_get_hud_argb(lua_State* state, uint32_t arg_place){
	std::shared_ptr<HUDARGB> retval;
	if (lua_istable(state, arg_place)){
		retval = std::shared_ptr<HUDARGB>(new HUDARGB);
		retval->a = LuaCore::getField<uint32_t>(state, arg_place, "a");
		retval->r = LuaCore::getField<uint32_t>(state, arg_place, "r");
		retval->g = LuaCore::getField<uint32_t>(state, arg_place, "g");
		retval->b = LuaCore::getField<uint32_t>(state, arg_place, "b");
	}
	return retval;
}

void lua_push_hud_size(lua_State* state, HUDSIZE siz){
	lua_newtable(state);
	lua_pushnumber(state, siz.Height);
	lua_setfield(state, -2, "height");
	lua_pushnumber(state, siz.Width);
	lua_setfield(state, -2, "width");
}

std::shared_ptr<HUDSIZE> lua_get_hud_size(lua_State* state, uint32_t arg_place){
	std::shared_ptr<HUDSIZE> retval;
	if (lua_istable(state, arg_place)){
		retval = std::shared_ptr<HUDSIZE>(new HUDSIZE);
		retval->Height = LuaCore::getField<uint32_t>(state, arg_place, "height");
		retval->Width = LuaCore::getField<uint32_t>(state, arg_place, "width");
	}
	return retval;
}

void lua_push_hud_rect(lua_State* state, HUDRECT rect){
	lua_newtable(state);
	lua_pushnumber(state, rect.X);
	lua_setfield(state, -2, "x");
	lua_pushnumber(state, rect.Y);
	lua_setfield(state, -2, "y");
	lua_pushnumber(state, rect.Height);
	lua_setfield(state, -2, "height"); 
	lua_pushnumber(state, rect.Width);
	lua_setfield(state, -2, "width");
}

std::shared_ptr<HUDRECT> lua_get_hud_rect(lua_State* state, uint32_t arg_place){
	std::shared_ptr<HUDRECT> retval;
	if (lua_istable(state, arg_place)){
		retval = std::shared_ptr<HUDRECT>(new HUDRECT);
		retval->X = LuaCore::getField<uint32_t>(state, arg_place, "x");
		retval->Y = LuaCore::getField<uint32_t>(state, arg_place, "y");
		retval->Height = LuaCore::getField<uint32_t>(state, arg_place, "height");
		retval->Width = LuaCore::getField<uint32_t>(state, arg_place, "width");
	}
	return retval;
}

int HudManagerdraw_circle(lua_State* state){
	auto rect = lua_get_hud_rect(state, 1);
	if (rect)
		HUDManager::get_default()->drawcircle(*rect.get());
	LUA_RET_NULL();
}

int HudManagerdraw_image(lua_State* state){
	int X = LuaCore::getNumber<uint32_t>(state, 2);
	int Y = LuaCore::getNumber<uint32_t>(state, 3);

	int SRCX = LuaCore::getNumber<uint32_t>(state, 4);
	int SRCY = LuaCore::getNumber<uint32_t>(state, 5);

	int SRCW = LuaCore::getNumber<uint32_t>(state, 6);
	int SRCH = LuaCore::getNumber<uint32_t>(state, 7);

	HUDManager::get_default()->drawimage(LuaCore::getString(state, 1), HUDCOORD{ X, Y }, HUDCOORD{ SRCX, SRCY }, HUDSIZE{ SRCW, SRCH });
	LUA_RET_NULL();
}
int HudManagerdrawitem(lua_State* state){
	/*int X = LuaCore::getNumber<uint32_t>(state, 2);
	int Y = LuaCore::getNumber<uint32_t>(state, 3);

	int ZOOMW = LuaCore::getNumber<uint32_t>(state, 4);
	int ZOOMH = LuaCore::getNumber<uint32_t>(state, 5);

	HUDManager::get_default()->drawitem(LuaCore::getString(state, 1), HUDCOORD{ X, Y }, HUDSIZE{ ZOOMW, ZOOMH });*/
	LUA_RET_NULL();
}

int HudManagerdraw_line(lua_State* state){
	std::shared_ptr<HUDCOORD> coord1 = lua_get_hud_coord(state, 1);
	std::shared_ptr<HUDCOORD> coord2 = lua_get_hud_coord(state, 2);
	if (coord1 && coord2)
		HUDManager::get_default()->drawline(*coord1.get(), *coord2.get());
	LUA_RET_NULL();
}

int HudManagermeasurestringWidth(lua_State* state){
	HUDSIZE Size = HUDManager::get_default()->measurestring(LuaCore::getString(state, 1));
	LUA_RET_INT(Size.Width);
}

int HudManagermeasurestringHeight(lua_State* state){
	HUDSIZE Size = HUDManager::get_default()->measurestring(LuaCore::getString(state, 1));
	LUA_RET_INT(Size.Height);
}

int HudManagerdraw_rect(lua_State* state){
	std::shared_ptr<HUDRECT> rect = lua_get_hud_rect(state, 1);
	if (!rect){
		LUA_RET_NULL();
	}
	HUDManager::get_default()->drawrect(*rect.get());
	LUA_RET_NULL();
}

int HudManagerdraw_roundrect(lua_State* state){
	int X = LuaCore::getNumber<uint32_t>(state, 1);
	int Y = LuaCore::getNumber<uint32_t>(state, 2);
	int width = LuaCore::getNumber<uint32_t>(state, 3);
	int height = LuaCore::getNumber<uint32_t>(state, 4);

	HUDManager::get_default()->drawroundrect(HUDRECT{ X, Y, width, height }, LuaCore::getNumber<uint32_t>(state, 5), LuaCore::getNumber<uint32_t>(state, 6));
	LUA_RET_NULL();
}

int HudManagerdraw_text(lua_State* state){
	int X = LuaCore::getNumber<uint32_t>(state, 2);
	int Y = LuaCore::getNumber<uint32_t>(state, 3);

	HUDManager::get_default()->drawtext(LuaCore::getString(state, 1), HUDCOORD{ X, Y });
	LUA_RET_NULL();
}

int HudManageraddgradcolors(lua_State* state){
	//auto argb = lua_get_hud_argb(state, 1);
	//auto rect = lua_get_hud_rect(state, 2);
	//if (!argb || !rect){
	//	return 0;
	//}
	////HUDManager::get_default()->FillRoundRect(*argb, *rect, LuaCore::getNumber<uint32_t>(state, 3),
	////	LuaCore::getBoolean(state, 4));
	LUA_RET_NULL();
}
int HudManagercolor(lua_State* state){
	int A = LuaCore::getNumber<uint32_t>(state, 1);
	int R = LuaCore::getNumber<uint32_t>(state, 2);
	int G = LuaCore::getNumber<uint32_t>(state, 3);
	int B = LuaCore::getNumber<uint32_t>(state, 4);

	lua_push_hud_argb(state, HUDARGB{ A, R, G, B });
	return 1;
}

int HudManagerget_size_client_x(lua_State* state){
	int x = HUDManager::get_default()->get_size_client().Width;
	LUA_RET_INT(x);
}
int HudManagerget_size_client_y(lua_State* state){
	int y = HUDManager::get_default()->get_size_client().Height;
	LUA_RET_INT(y);
}
int HudManagerget_size_game_y(lua_State* state){
	LUA_RET_INT(ClientGeneralInterface::get()->get_game_window_size().y);
}
int HudManagerget_size_game_x(lua_State* state){
	LUA_RET_INT(ClientGeneralInterface::get()->get_game_window_size().x);
}

int HudManagerget_pos_game_y(lua_State* state){
	LUA_RET_INT(ClientGeneralInterface::get()->get_game_window_pos_y());
}

int HudManagerget_pos_game_x(lua_State* state){
	LUA_RET_INT(ClientGeneralInterface::get()->get_game_window_pos_x());
}

int HudManagerfilterinput(lua_State* state){
	HUDManager::get_default()->filterinput(LuaCore::getBoolean(state, 1), LuaCore::getBoolean(state, 2));
	return 1;
}

int HudManagerget_position(lua_State* state){
	//	lua_push_hud_coord(state, HUDManager::get_default()->get_position());
	return 1;
}

int HudManagerset_colorBorder(lua_State* state){
	auto color = lua_get_hud_argb(state, 1);
	if (!color)
		return 0;
	HUDManager::get_default()->setbordercolor(*color);
	LUA_RET_NULL();
}

int HudManagerset_colorBrush(lua_State* state){
	int args = lua_gettop(state);
	std::vector<std::pair< float, HUDARGB >> intporlations_info;

	if (args % 2 == 0){
		for (int index = 1; index <= args; index += 2){
			float numberFloat = LuaCore::getNumber<float>(state, index);
			auto color = lua_get_hud_argb(state, index + 1);
			if (!color)
				continue;

			intporlations_info.push_back(std::make_pair(numberFloat, *color));
		}
		HUDManager::get_default()->addgradcolors(intporlations_info);
	}

	LUA_RET_NULL();
}

int HudManagerset_font(lua_State* state){
	HUDManager::get_default()->setfontname(LuaCore::getString(state, 1));
	LUA_RET_NULL();
}

int HudManagerset_fontBorder(lua_State* state){
	auto color = lua_get_hud_argb(state, 2);
	if (!color)
		return 0;
	HUDManager::get_default()->setfontborder(LuaCore::getNumber<uint32_t>(state, 1), *color);
	LUA_RET_NULL();
}

int HudManagersetbordersize(lua_State* state){
	HUDManager::get_default()->setbordersize(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL();
}

int HudManagerset_fontColor(lua_State* state){
	auto color = lua_get_hud_argb(state, 1);

	if (!color)
		return 0;

	HUDManager::get_default()->setfontcolor(*color);
	LUA_RET_NULL();
}

int HudManagerset_fontsize(lua_State* state){
	HUDManager::get_default()->setfontsize(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL();
}

int HudManagersetposition(lua_State* state){
	auto coord1 = lua_get_hud_coord(state, 1);
	if (coord1)
		HUDManager::get_default()->setposition(*coord1.get());

	LUA_RET_NULL();
}

int HudManagersetfontstyle(lua_State* state){
	auto color = lua_get_hud_argb(state, 4);
	int args = lua_gettop(state);

	if (!color)
		return 0;

	if (args <= 4)
		HUDManager::get_default()->setfontstyle(LuaCore::getString(state, 1),
		LuaCore::getNumber<uint32_t>(state, 2), LuaCore::getNumber<uint32_t>(state, 3),
		*color);

	else if (args >= 5){
		std::shared_ptr<HUDARGB> color2 = lua_get_hud_argb(state, 6);
		if (!color2)
			return 0;
		HUDManager::get_default()->setfontstyle(
			LuaCore::getString(state, 1),
			LuaCore::getNumber<uint32_t>(state, 2),
			LuaCore::getNumber<uint32_t>(state, 3),
			*color,
			LuaCore::getNumber<uint32_t>(state, 5),
			*color2);
	}

	LUA_RET_NULL();
}

int lua_last_balance(lua_State* state){
	LUA_RET_INT(InfoCore::get()->get_last_balance());
}
int lua_get_current_time_total_in_day(lua_State* state){
	LUA_RET_INT(InfoCore::get()->get_current_time_total_in_day());
}
int lua_item_used_count(lua_State* state){
	LUA_RET_INT(InfoCore::get()->get_item_used_count(LuaCore::getNumber<uint32_t>(state, 1)));
}
int lua_exp_hour(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->exp_hour());
}
int lua_exp_to_next_lvl(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->exp_to_next_lvl());
}
int lua_time_to_next_lvl(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->character_info->time_to_next_lvl());
}
int lua_get_ping(lua_State* state){
	LUA_RET_INT(TibiaProcess::get_default()->get_ping());
}
int lua_profit_hour(lua_State* state){
	LUA_RET_INT(InfoCore::get()->profit_hour());
}
int lua_expgained(lua_State* state){
	LUA_RET_INT(InfoCore::get()->expgained());
}
int lua_get_last_medium_ping(lua_State* state){
	LUA_RET_INT(InfoCore::get()->get_last_medium_ping());

}int lua_get_moneyspent(lua_State* state){
	std::map<uint32_t, std::pair<uint32_t, uint32_t>>   mapUsed = InfoCore::get()->get_itens_used_map();
	auto mapItems = LooterCore::get()->get_map_items_looted();
	uint32_t valueEnd = 0;
	for (auto itemName : mapUsed){
		uint32_t item_id = itemName.first;
		
		auto it = mapItems.find(item_id);
		if (it == mapItems.end())
			continue;

		if (!it->second)
			continue;

		auto itemsInfoRule = LooterCore::get()->getRuleItemsLooted(it->second->item_id);
		if (!itemsInfoRule)
			continue;

		uint32_t itemBuyPrice = itemsInfoRule->buy_price;
		valueEnd = valueEnd + (itemName.second.first * itemBuyPrice);
	}

	LUA_RET_INT(valueEnd);
}
int SetupManagerget_text_by_control(lua_State* state){
	std::string controlname = LuaCore::getString(state, 1);
	std::string propertyname = "text";

	std::string value = ControlManager::get()->get_property_find_all(controlname, propertyname);

	LUA_RET_STRING(value);
	return 0;
}

int SetupManagerget_list_by_control(lua_State* state){
	std::string controlname = LuaCore::getString(state, 1);
	std::string propertyname = "text";

	std::vector<std::string> text_vector = ControlManager::get()->get_text_vector_find_all(controlname);
	if (!text_vector.size())
		lua_pushnil(state);

	lua_newtable(state);
	int index = 1;
	for (auto text : text_vector){
		lua_pushnumber(state, index);
		lua_pushstring(state, text.c_str());
		lua_settable(state, -3);
		index++;
	}

	return 1;
}

int SetupManagerset_text_by_control(lua_State* state){
	std::string controlname = LuaCore::getString(state, 1);
	std::string controlText = LuaCore::getString(state,2);
	std::string propertyname = "text";
	
	ControlManager::get()->set_property_find_all(controlname, propertyname, controlText);

	LUA_RET_NULL();
	return 0;
}

void PushSABaseElement(lua_State* state, SABaseElement* element){
	lua_newtable(state);
	lua_pushnumber(state, (int)element);
	lua_setfield(state, -2, "internal_raw_pointer");
	LuaCore::setMetatable(state, -1, "SABaseElement");
}

void luaPushSAAvoidance(lua_State* state, SAAvoidance* avoidance){
	lua_newtable(state);
	
	lua_pushnumber(state, avoidance->decay);
	lua_setfield(state, -2, "decay");

	lua_pushnumber(state, avoidance->range);
	lua_setfield(state, -2, "range");

	lua_pushnumber(state, avoidance->value);
	lua_setfield(state, -2, "value");
}

void luaPushSAWalkDificult(lua_State* state, SAWalkDificult* walk_diff){
	lua_newtable(state);

	lua_pushnumber(state, walk_diff->decay);
	lua_setfield(state, -2, "decay");

	lua_pushnumber(state, walk_diff->range);
	lua_setfield(state, -2, "range");

	lua_pushnumber(state, walk_diff->value);
	lua_setfield(state, -2, "value");
}



SABaseElement* luaGetSABaseElementFromStack(lua_State* state, int index){
	if (!lua_istable(state, index))
		return nullptr;

	SABaseElement* raw_ptr = (SABaseElement*)LuaCore::getField<uint32_t>(state, index, "internal_raw_pointer");
	if (!raw_ptr)
		return nullptr;

	if (SpecialAreaManager::get()->get_root()->exist_raw_pointer(raw_ptr)){
		return raw_ptr;
	}
	return nullptr;
}

int SpecialAreaManagerget_root_area(lua_State* state){
	PushSABaseElement(state, SpecialAreaManager::get()->get_root());
	return 1;
}



//SAWallElement
//SAArea
int SABaseElementset_width(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
			case special_area_element_t::sa_element_area:{
				((SAArea*)element)->width = LuaCore::getNumber<int32_t>(state, 1);
			}
			break;
			case special_area_element_t::sa_element_point:{
				LUA_RET_NULL();
			}
			break;
			case special_area_element_t::sa_element_wall:{
				((SAWallElement*)element)->width = LuaCore::getNumber<int32_t>(state, 1);
			}
			break;
			default:{
				LUA_RET_NULL();
			}
		}
	}
	LUA_RET_NULL();
}
//SAArea
//SAWallElement
int SABaseElementget_width(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LUA_RET_INT(((SAArea*)element)->width);
		}
			break;
		case special_area_element_t::sa_element_point:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LUA_RET_INT(((SAWallElement*)element)->width);
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}
//SAWallElement
//SAArea
//SAPointElement
int SABaseElementget_z(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LUA_RET_INT(((SAArea*)element)->z);
		}
			break;
		case special_area_element_t::sa_element_point:{
			LUA_RET_INT(((SAArea*)element)->z);
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LUA_RET_INT(((SAWallElement*)element)->z);
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}
//SAWallElement
int SABaseElementget_line(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_point:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_wall:{
			SAWallElement* sa_wall_element = ((SAWallElement*)element);
			LuaCore::pushLine(state, std::pair<Point, Point>(sa_wall_element->get_line()));
			return 1;
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}
//SAPointElement
int SABaseElementget_point(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_point:{
			LuaCore::pushPosition(state, Coordinate(((SAPointElement*)element)->get_point(), ((SAPointElement*)element)->z));
			return 1;
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LUA_RET_NULL();
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}

//SAPointElement
int SABaseElementget_walkability_rect(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_point:{
			LuaCore::pushRect(state, ((SAPointElement*)element)->get_walkability_rect());
			return 1;
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LUA_RET_NULL();
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}


//SAPointElement
int SABaseElementget_avoidance_rect(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_point:{
			LuaCore::pushRect(state, ((SAPointElement*)element)->get_avoidance_rect());
			return 1;
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LUA_RET_NULL();
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}

//SAArea
int SABaseElementget_center(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	
	if (element){	
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LuaCore::pushPosition(state, ((SAArea*)element)->get_center());
			return 1;
		}
			break;
		case special_area_element_t::sa_element_point:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LuaCore::pushPosition(state, ((SAArea*)element)->get_center());
			return 1;
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}


int SABaseElementget_rect(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		switch (element->get_type()){
		case special_area_element_t::sa_element_area:{
			LuaCore::pushRect(state, ((SAArea*)element)->get_rect());
			return 1;
		}
			break;
		case special_area_element_t::sa_element_point:{
			LUA_RET_NULL();
		}
			break;
		case special_area_element_t::sa_element_wall:{
			LUA_RET_NULL();
		}
			break;
		default:{
			LUA_RET_NULL();
		}
		}
	}
	LUA_RET_NULL();
}



int SABaseElementget_activation_trigger(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		lua_pushnumber(state, element->get_activation_trigger());
		return 1;
	}
	LUA_RET_NULL();
}

int SABaseElementset_activation_trigger(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element)
		element->
		set_activation_trigger((special_area_activation_t)LuaCore::getNumber<uint32_t>(state, 2));
	return 0;
}

int SABaseElementset_state(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element)
		element->set_state(LuaCore::getBoolean(state, 2));
	return 0;
}

int SABaseElementget_state(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element)
		LUA_RET_BOOL(element->get_state());
	LUA_RET_NULL()
}

int SABaseElementremove_element(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (!element)
		LUA_RET_BOOL(false);

	element->remove_element(element);
	LUA_RET_BOOL(true);
}

int SABaseElementget_childrens_by_type(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	lua_newtable(state);
	if (element){
		int args_count = lua_gettop(state);
		special_area_element_t element_type = special_area_element_t::sa_element_any;
		bool recursive = true;
		if (args_count > 1)
			element_type = (special_area_element_t)LuaCore::getNumber<uint32_t>(state, 2);

		if (args_count > 2)
			recursive = LuaCore::getBoolean(state, 3);

		auto childs = element->get_childrens_by_type(element_type, recursive);
		int index = 1;
		for (auto child : childs){
			PushSABaseElement(state, child);
			lua_rawseti(state , -2, index);
			index++;
		}
	}
	return 1;
}

int SABaseElementget_childrens_by_names(lua_State* state){
	std::vector<std::string> vector_coord;
	if (lua_istable(state, 1)){
		lua_pushnil(state);
		while (lua_next(state, -2)) {
			vector_coord.push_back(LuaCore::getString(state, -1));
			lua_pop(state, 1);
		}
	}
	/*int args_count = lua_gettop(state);

	for (int i = 0; i < args_count; i++){
		if (lua_istable(state, 1))
			vector_coord.push_back(LuaCore::getString(state, 1));
	}*/

	//lua_pushnil(state); // first key
	//index = lua_gettop(state);
	//while (lua_next(state, index)) { // traverse keys
	//	something = lua_tosomething(state, -1); // tonumber for example
	//	results.push_back(something);
	//	lua_pop(state, 1); // stack restore
	//}

	std::vector<std::vector<SABaseElement*>> elements;
	std::vector<SABaseElement*> retval_elements;

	SABaseElement* element_ = SpecialAreaManager::get()->get_root();
	for (auto name : vector_coord)
		elements.push_back(element_->get_childrens_by_name(name));

	for (auto vector_element : elements){
		for (auto element : vector_element){
			retval_elements.push_back(element);
		}
	}
	
	int index = 1;
	for (auto child : retval_elements){
		PushSABaseElement(state, child);
		lua_rawseti(state, -2, index);
		index++;
	}

	return 1;
}
int SABaseElementget_childrens_by_name(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);

	int args_count = lua_gettop(state);
	lua_newtable(state);
	if (element){
		std::string name = LuaCore::getString(state, 2);
		bool recursive = true;
		special_area_element_t element_type = special_area_element_t::sa_element_any;
		if (args_count > 2)
			recursive = LuaCore::getBoolean(state, 3);
		
		if (args_count > 3)
			element_type = (special_area_element_t)LuaCore::getNumber<uint32_t>(state, 4);
	
		auto elements = element->get_childrens_by_name(name, recursive, element_type);
		int index = 1;
		for (auto element : elements){
			PushSABaseElement(state, element);
			lua_rawseti(state, -2, index);
			index++;
		}
	}
	return 1;
}

int SABaseElementget_child_by_name(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);

	int args_count = lua_gettop(state);
	if (lua_gettop(state) < 2){
		LUA_RET_NULL();
	}
	
	std::string name = LuaCore::getString(state, 2);
	bool recursive = true;
	special_area_element_t element_type = special_area_element_t::sa_element_any;

	if (args_count > 2){
		recursive = LuaCore::getBoolean(state, 3);
	}
	if (args_count > 3){
		element_type = (special_area_element_t)LuaCore::getNumber<uint32_t>(state, 4);
	}

	lua_newtable(state);
	if (element){
		auto elements = element->get_childrens_by_name(name, recursive, element_type);
		int index = 1;
		for (auto element : elements){
			PushSABaseElement(state, element);
			lua_rawseti(state, -2, index);
			index++;
		}
			
	}

	return 1;
}

int SABaseElementget_type(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		LUA_RET_INT(element->get_type());
	}
	else{
		LUA_RET_INT(sa_element_none);
	}
}

int SABaseElementget_owner(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (!element){
		LUA_RET_NULL();
	}
	else{
		auto owner = element->get_owner();
		PushSABaseElement(state, owner);
	}
	return 1;
}

int SABaseElementset_name(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		if (!lua_isstring(state, 2)){
			LUA_RET_BOOL(false);
		}
		element->set_name(LuaCore::getString(state, 2));
		LUA_RET_BOOL(true);
	}
	else{
		LUA_RET_BOOL(false);
	}
}

int SABaseElementget_name(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	std::string name = "";
	if (element)
		name = element->get_name();
	LuaCore::pushString(state, name);
	return 1;
}

int SABaseElementset_block_walk(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){
		element->set_block_walk(LuaCore::getBoolean(state, 2));
	}
	return 0;
}

int SABaseElementget_block_walk(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	bool retval = false;
	if (element){
		retval = element->get_block_walk();
	}
	LUA_RET_BOOL(retval);
}

int SABaseElementget_walk_dificult(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (!element){
		luaPushSAWalkDificult(state, element->get_walk_dificult());
		return 1;
	}
	LUA_RET_NULL();
}

int SABaseElementget_avoidance(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	if (element){		
		luaPushSAAvoidance(state, element->get_avoidance());
		return 1;
	}
	LUA_RET_NULL();
}

int SABaseElementget_childrens(lua_State* state){
	SABaseElement* element = luaGetSABaseElementFromStack(state, 1);
	lua_newtable(state);
	if (element){
		std::vector<SABaseElement*> list_items = element->get_childrens();
		int index = 1;
		for (SABaseElement* child : list_items){
			PushSABaseElement(state, child);
			lua_rawseti(state, -2, index);
			index++;
		}
	}
	return 1;
}



int SetupManagerget_state_by_control(lua_State* state){
	std::string controlname = LuaCore::getString(state, 1);
	std::string propertyname = "state";

	std::string value = ControlManager::get()->get_property_find_all(controlname, propertyname);

	bool booltemp = false;

	if (string_util::lower(value) == "true")
		booltemp = true;

	LUA_RET_BOOL(booltemp);
}

int SetupManagerget_value_by_control(lua_State* state){
	std::string controlname = LuaCore::getString(state, 1);
	std::string propertyname = "value";

	std::string value = ControlManager::get()->get_property_find_all(controlname, propertyname);

	int temp_int = 0;
	try{
		temp_int = boost::lexical_cast<uint32_t>(value);
	}
	catch (...){
		temp_int = 0;
	}
	LUA_RET_INT(temp_int);
}


int luaSelfget_displacement(lua_State* state){
	LUA_RET_COORDINATE(TibiaProcess::get_default()->character_info->get_displacement());
}


int luaSelfget_going_step(lua_State* state){
	Coordinate going = TibiaProcess::get_default()->character_info->get_going_step();
	if (going.is_null()){
		LUA_RET_NULL();
	}
	LUA_RET_COORDINATE( going);
}



int luaTestMethod(lua_State* state){
	HighLevelVirtualInput::get_default()->click_left(Point(LuaCore::getNumber<int32_t>(state, 1), LuaCore::getNumber<int32_t>(state, 2)));
	return 0;
}



int luaItemsManagerget_items_as_pick(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_pick());
	return 1;
}

int luaItemsManagerget_items_as_rope(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_rope());
	return 1;
}

int luaItemsManagerget_items_as_shovel(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_shovel());
	return 1;
}

int luaItemsManagerget_items_as_take_skin(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_take_skin());
	return 1;
}

int luaItemsManagerget_items_as_machete(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_machete());
	return 1;
}

int luaItemsManagerget_items_as_food(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_food());
	return 1;
}

int luaItemsManagerget_items_as_sickle(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_sickle());
	return 1;
}

int luaItemsManagerget_items_as_scythe(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_scythe());
	return 1;
}

int luaItemsManagerget_items_as_crowbar(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_crowbar());
	return 1;
}

int luaItemsManagerget_items_as_spoon(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_spoon());
	return 1;
}

int luaItemsManagerget_items_as_knife(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_knife());
	return 1;
}

int luaItemsManagerget_items_as_kitchen(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_kitchen());
	return 1;
}

int luaItemsManagerget_items_as_empty_potions(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_empty_potions());
	return 1;
}

int luaItemsManagerget_items_as_fields_runes(lua_State* state){
	LuaPushIntVector(state,ItemsManager::get()->get_items_as_fields_runes());
	return 1;
}


int luaRepoteris_necesary_all_repot(lua_State* state){
	LUA_RET_BOOL(RepoterCore::is_necessary_all_repot());
}

int luaRepoteris_necesary_repot_id(lua_State* state){
	LUA_RET_BOOL(RepoterCore::is_necessary_repot(LuaCore::getString(state, 1)));
}

int luaDepoteris_necesary_some_depot(lua_State* state){
	LUA_RET_BOOL(DepoterManager::get()->checkNecessaryDepot());
}

int luaDepoterin_necesary_depot_id(lua_State* state){
	LUA_RET_BOOL(DepoterManager::get()->checkNecessaryDepot(LuaCore::getString(state, 1)));
}

int luaBotManagerKillTibiaClient(lua_State* state){
	NeutralManager::get()->kill_tibia_client();
	return 0;
}

int Utilget_axis_min_dist(lua_State* state){
	int args = lua_gettop(state);
	int dist = 0;
	
	if (args >= 2){
		Coordinate coord_one = LuaCore::getPosition(state, 1);
		Coordinate coord_two = LuaCore::getPosition(state, 2);

		dist = coord_one.get_axis_min_dist(coord_two);		
	}
	else{
		Coordinate coord_one = TibiaProcess::get_default()->character_info->get_mixed_coordinate();
		Coordinate coord_two = LuaCore::getPosition(state, 1);

		dist = coord_one.get_axis_min_dist(coord_two);
	}

	LUA_RET_INT(dist);
}

int Waypointeris_location(lua_State* state){
	int args = lua_gettop(state);

	uint32_t dist = 1;
	if (args > 0 && lua_istable(state, 1)){
		if (args > 1){
			dist = LuaCore::getNumber<uint32_t>(state, 2);
		}
		auto pos = LuaCore::getPosition(state, 1);
		if (pos.is_null()){
			LUA_RET_BOOL(false);
		}
		LUA_RET_BOOL(
			WaypointManager::get()->is_location(pos.x, pos.y, pos.z, dist)
			);
	}
	else{
		if (args > 0){
			dist = LuaCore::getNumber<uint32_t>(state, 1);
		}
		LUA_RET_BOOL(
			WaypointManager::get()->is_location(dist)
			);
	}

	LUA_RET_NULL();
}

int luaBattleListwait_creature_out_of_range(lua_State* state){
	int args = lua_gettop(state);
	if (args < 3){
		LUA_RET_INT(-1);
	}
	uint32_t creature_id = LuaCore::getNumber<uint32_t>(state, 1);
	int min = LuaCore::getNumber<uint32_t>(state, 2);
	int max = LuaCore::getNumber<uint32_t>(state, 3);
	uint32_t timeout = 2000;
	bool consider_displacement = true;
	if (args > 3)
		timeout = LuaCore::getNumber<uint32_t>(state, 4);
	if (args > 4)
		consider_displacement = LuaCore::getBoolean(state, 5);

	LUA_RET_INT(
		BattleList::get()->wait_creature_out_of_range(creature_id, min, max, timeout,
		consider_displacement)
		);
}

int luaPathfinderget_consider_near_yellow_coords(lua_State* state){
	LUA_RET_INT(ConfigPathManager::get()->get_consider_near_yellow_coords());
}


int luaPathfinderget_time_to_wait(lua_State* state){
	LUA_RET_INT( ConfigPathManager::get()->get_time_to_wait());
}


int luaPathfinderset_consider_near_yellow_coords(lua_State* state){
	ConfigPathManager::get()->set_consider_near_yellow_coords(LuaCore::getNumber<uint32_t>(state, 1));
	return 0;
}

int luaPathfinderset_time_to_wait(lua_State* state){
	ConfigPathManager::get()->set_time_to_wait(LuaCore::getNumber<uint32_t>(state, 1));
	return 0;
}

int luaLootercheck_cap(lua_State* state){	
	LUA_RET_BOOL(LooterManager::get()->get_min_cap() > 
		TibiaProcess::get_default()->character_info->cap());
}

int luaLooterget_stop_loot_on_minimun_cap(lua_State* state){
	LUA_RET_BOOL(LooterManager::get()->get_min_cap_to_loot());
}

int luaLooterset_stop_loot_on_minimun_cap(lua_State* state){
	LooterManager::get()->set_min_cap_to_loot(LuaCore::getBoolean(state, 1));
	return 0;
}

int luaHunterManagerpause_to_time(lua_State* state){
	uint32_t time = UINT32_MAX;

	int args = lua_gettop(state);
	if (args == 1)
		time = LuaCore::getNumber<uint32_t>(state, 1);

	HunterCore::get()->set_time_to_pause(time);
	LUA_RET_NULL();
}
int luaHunterManagerunpause_to_time(lua_State* state){
	HunterCore::get()->set_time_to_pause(0);
	LUA_RET_NULL();
}

int luaLooterManagerpause_to_time(lua_State* state){
	uint32_t time = UINT32_MAX;

	int args = lua_gettop(state);
	if (args == 1)
		time = LuaCore::getNumber<uint32_t>(state, 1);

	LooterCore::get()->set_time_to_pause(time);
	LUA_RET_NULL();
}
int luaLooterManagerunpause_to_time(lua_State* state){
	LooterCore::get()->set_time_to_pause(0);
	LUA_RET_NULL();
}

int luaWaypointManagerpause_to_time(lua_State* state){
	uint32_t time = UINT32_MAX;
	
	int args = lua_gettop(state);
	if (args == 1)
		time = LuaCore::getNumber<uint32_t>(state, 1);
	
	WaypointerCore::get()->set_time_to_pause(time);
	LUA_RET_NULL();
}
int luaWaypointManagerunpause_to_time(lua_State* state){
	WaypointerCore::get()->set_time_to_pause(0);
	LUA_RET_NULL();
}



int luaNavigation_get_last_time_recv(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->time_last_update.elapsed_milliseconds());

	LUA_RET_NULL();
}
int luaNavigation_exp_to_next_lvl(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->exp_to_next_lvl)

	LUA_RET_NULL();
}
int luaNavigation_experience(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);
 
	LUA_RET_INT(player_info->experience)

	LUA_RET_NULL();
}
int luaNavigation_time_to_next_lvl(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->time_to_next_lvl)

	LUA_RET_NULL();
}
int luaNavigation_get_name_char(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_STRING(player_info->get_name_char)

	LUA_RET_NULL();
}
int luaNavigation_self_level(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_level)

	LUA_RET_NULL();
}
int luaNavigation_self_levelpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_levelpc)

	LUA_RET_NULL();
}
int luaNavigation_self_ml(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_ml)

	LUA_RET_NULL();
}
int luaNavigation_self_mlpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_mlpc)

	LUA_RET_NULL();
}
int luaNavigation_self_first(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_first)

	LUA_RET_NULL();
}
int luaNavigation_self_firstpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_firstpc)

	LUA_RET_NULL();
}
int luaNavigation_self_club(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_club)

	LUA_RET_NULL();
}
int luaNavigation_self_clubpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_clubpc)

	LUA_RET_NULL();
}
int luaNavigation_self_sword(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_sword)

	LUA_RET_NULL();
}
int luaNavigation_self_swordpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_swordpc)

	LUA_RET_NULL();
}
int luaNavigation_self_axe(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_axe)

	LUA_RET_NULL();
}
int luaNavigation_self_axepc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_axepc)

	LUA_RET_NULL();
}
int luaNavigation_self_distance(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_distance)

	LUA_RET_NULL();
}
int luaNavigation_self_distancepc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_distancepc)

	LUA_RET_NULL();
}
int luaNavigation_self_shielding(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_shielding)

	LUA_RET_NULL();
}
int luaNavigation_self_shieldingpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_shieldingpc)

	LUA_RET_NULL();
}
int luaNavigation_self_fishing(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_fishing)

	LUA_RET_NULL();
}
int luaNavigation_self_fishingpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_fishingpc)

	LUA_RET_NULL();
}
int luaNavigation_get_self_coordinate(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);
	Coordinate self_coordiante = player_info->get_self_coordinate;

	LuaCore::pushPosition(state, self_coordiante);

	LUA_RET_NULL();
}
int luaNavigation_self_maxhp(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_maxhp)

	LUA_RET_NULL();
}
int luaNavigation_self_maxmp(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_maxmp)

	LUA_RET_NULL();
}
int luaNavigation_current_hp(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->current_hp)

	LUA_RET_NULL();
}
int luaNavigation_current_hppc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->current_hppc)

	LUA_RET_NULL();
}
int luaNavigation_current_mp(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->current_mp)

	LUA_RET_NULL();
}
int luaNavigation_current_mmpc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->current_mppc)

	LUA_RET_NULL();
}
int luaNavigation_self_cap(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_cap)

	LUA_RET_NULL();
}
int luaNavigation_self_soul(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_soul)

	LUA_RET_NULL();
}
int luaNavigation_self_stamina(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_stamina)

	LUA_RET_NULL();
}
int luaNavigation_self_off_training(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_off_training)

	LUA_RET_NULL();
}
int luaNavigation_self_character_id(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->self_character_id)

	LUA_RET_NULL();
}
int luaNavigation_target_red(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_INT(player_info->target_red)

	LUA_RET_NULL();
}
int luaNavigation_is_follow_mode(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->is_follow_mode)

	LUA_RET_NULL();
}
int luaNavigation_is_walking(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->is_walking)

	LUA_RET_NULL();
}
int luaNavigation_satus_water(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->satus_water)

	LUA_RET_NULL();
}
int luaNavigation_status_holly(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_holly)

	LUA_RET_NULL();
}
int luaNavigation_status_frozen(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_frozen)

	LUA_RET_NULL();
}
int luaNavigation_status_curse(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_curse)

	LUA_RET_NULL();
}
int luaNavigation_status_battle_red(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_battle_red)

	LUA_RET_NULL();
}
int luaNavigation_status_streng_up(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_streng_up)

	LUA_RET_NULL();
}
int luaNavigation_status_pz(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_pz)

	LUA_RET_NULL();
}
int luaNavigation_status_blood(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_blood)

	LUA_RET_NULL();
}
int luaNavigation_status_poison(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_poison)

	LUA_RET_NULL();
}
int luaNavigation_status_fire(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_fire)

	LUA_RET_NULL();
}
int luaNavigation_status_energy(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_energy)

	LUA_RET_NULL();
}
int luaNavigation_status_drunk(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_drunk)

	LUA_RET_NULL();
}
int luaNavigation_status_slow(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_slow)

	LUA_RET_NULL();
}
int luaNavigation_status_utamo(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_utamo)

	LUA_RET_NULL();
}
int luaNavigation_status_haste(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_haste)

	LUA_RET_NULL();
}
int luaNavigation_status_battle(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInfoPtr player_info = NavigationManager::get()->get_rule_player_info(name_char);

	LUA_RET_BOOL(player_info->status_battle)

	LUA_RET_NULL();
}



int luaNavigation_body_helmet(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_helmet)

	LUA_RET_NULL();
}
int luaNavigation_body_amulet(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_amulet)

		LUA_RET_NULL();
}
int luaNavigation_body_bag(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_bag)

		LUA_RET_NULL();
}
int luaNavigation_body_armor(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_armor)

		LUA_RET_NULL();
}
int luaNavigation_body_weapon(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_weapon)

		LUA_RET_NULL();
}
int luaNavigation_body_shield(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_shield)

		LUA_RET_NULL();
}
int luaNavigation_body_leg(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_leg)

		LUA_RET_NULL();
}
int luaNavigation_body_boot(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_boot)

		LUA_RET_NULL();
}
int luaNavigation_body_ring(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_ring)

		LUA_RET_NULL();
}
int luaNavigation_body_rope(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->body_rope)

		LUA_RET_NULL();
}
int luaNavigation_ammunation_count(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->ammunation_count)

		LUA_RET_NULL();
}
int luaNavigation_weapon_count(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_INT(player_info->weapon_count)

		LUA_RET_NULL();
}
int luaNavigation_maximize(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_BOOL(player_info->maximize)

		LUA_RET_NULL();
}
int luaNavigation_minimize(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerInventoryPtr player_info = NavigationManager::get()->get_rule_player_inventory(name_char);

	LUA_RET_BOOL(player_info->minimize)

		LUA_RET_NULL();
}



int luaNavigation_creatures_on_screen(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_INT(player_info->creatures_on_screen)

		LUA_RET_NULL();
}
int luaNavigation_current_creature_name(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_STRING(player_info->current_creature_name)

		LUA_RET_NULL();
}
int luaNavigation_current_creature_coordinate(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);
	Coordinate current_creature_coordinate = player_info->current_creature_coordinate;

	LuaCore::pushPosition(state, current_creature_coordinate);
	
	LUA_RET_NULL();
}
int luaNavigation_current_creature_is_monster(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_BOOL(player_info->current_creature_is_monster);

	LUA_RET_NULL();
}
int luaNavigation_current_creature_is_npc(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_BOOL(player_info->current_creature_is_npc);
	
	LUA_RET_NULL();
}
int luaNavigation_current_creature_is_player(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_BOOL(player_info->current_creature_is_player);

	LUA_RET_NULL();
}
int luaNavigation_have_player_on_screen(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_BOOL(player_info->have_player_on_screen);

	LUA_RET_NULL();
}
int luaNavigation_have_creature_on_screen(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_BOOL(player_info->have_creature_on_screen);

	LUA_RET_NULL();
}
int luaNavigation_is_trapped_by_creatures(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerCreaturesPtr player_info = NavigationManager::get()->get_rule_player_creatures(name_char);

	LUA_RET_BOOL(player_info->is_trapped_by_creatures);

	LUA_RET_NULL();
}



void pushItems_Info(lua_State* state, std::shared_ptr<ContainerInfo> itemsRule){
	lua_newtable(state);

	if (!itemsRule){
		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "slots_used");

		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "slots_free");

		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "slots_total");

		LuaCore::pushNumber(state, 0);
		lua_setfield(state, -2, "container_id");

		LuaCore::pushString(state, "");
		lua_setfield(state, -2, "container_name");

		LuaCore::pushBoolean(state, false);
		lua_setfield(state, -2, "is_full");

		LuaCore::pushBoolean(state, false);
		lua_setfield(state, -2, "has_next");

		LuaCore::pushBoolean(state, false);
		lua_setfield(state, -2, "is_minimized");

		return;
	}

	LuaCore::pushNumber(state, itemsRule->slots_used);
	lua_setfield(state, -2, "slots_used");

	LuaCore::pushNumber(state, itemsRule->slots_free);
	lua_setfield(state, -2, "slots_free");

	LuaCore::pushNumber(state, itemsRule->slots_total);
	lua_setfield(state, -2, "slots_total");

	LuaCore::pushNumber(state, itemsRule->container_id);
	lua_setfield(state, -2, "container_id");

	LuaCore::pushString(state, itemsRule->container_name);
	lua_setfield(state, -2, "container_name");

	LuaCore::pushBoolean(state, itemsRule->is_full);
	lua_setfield(state, -2, "is_full");

	LuaCore::pushBoolean(state, itemsRule->has_next);
	lua_setfield(state, -2, "has_next");

	LuaCore::pushBoolean(state, itemsRule->is_minimized);
	lua_setfield(state, -2, "is_minimized");
}

int luaNavigation_waypoint_action(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerWaypointerPtr player_info = NavigationManager::get()->get_rule_player_waypointer(name_char);

	if (!player_info){
		LUA_RET_NULL();
	}

	LUA_RET_INT((int)player_info->current_waypoint_action);

	LUA_RET_NULL();
}
int luaNavigation_waypoint_path(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerWaypointerPtr player_info = NavigationManager::get()->get_rule_player_waypointer(name_char);

	if (!player_info){
		LUA_RET_NULL();
	}

	LUA_RET_STRING(player_info->current_waypoint_path);
	
	LUA_RET_NULL();
}
int luaNavigation_waypoint_coordinate(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerWaypointerPtr player_info = NavigationManager::get()->get_rule_player_waypointer(name_char);

	if (!player_info){
		LUA_RET_NULL();
	}

	LuaCore::pushPosition(state, player_info->current_waypoint_coordinate);

	LUA_RET_NULL();
}
int luaNavigation_waypoint_label(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerWaypointerPtr player_info = NavigationManager::get()->get_rule_player_waypointer(name_char);

	if (!player_info){
		LUA_RET_NULL();
	}

	LUA_RET_STRING(player_info->current_waypoint_label)

	LUA_RET_NULL();
}
int luaNavigation_count_money_total(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerContainersPtr player_info = NavigationManager::get()->get_rule_player_containers(name_char);

	LUA_RET_INT(player_info->count_money_total);

	LUA_RET_NULL();
}
int luaNavigation_count_container_open(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerContainersPtr player_info = NavigationManager::get()->get_rule_player_containers(name_char);

	LUA_RET_INT(player_info->count_container_open);

	LUA_RET_NULL();
}
int luaNavigation_container_infos(lua_State* state){
	std::string name_char = LuaCore::getString(state, 1);
	NaviPlayerContainersPtr player_info = NavigationManager::get()->get_rule_player_containers(name_char);

	lua_newtable(state);

	if (!player_info->container_infos.size()){
		lua_pushinteger(state, 0);
		pushItems_Used(state, nullptr);
		lua_settable(state, -3);
		return 1;
	}
	else{
		for (auto container : player_info->container_infos){
			lua_pushinteger(state, container->container_id);
			pushItems_Info(state, container);
			lua_settable(state, -3);
		}
		return 1;
	}
	LUA_RET_NULL();
}




int luaNavigation_close(lua_State* state){
	NavigationManager::get()->close();
	LUA_RET_NULL();
}
int luaNavigation_get_simples_string(lua_State* state){
	LUA_RET_STRING(NavigationManager::get()->get_simple_string());
}
int luaNavigation_connect_to_server(lua_State* state){
	NavigationManager::get()->connect_to_server(LuaCore::getString(state, 1), LuaCore::getNumber<uint32_t>(state, 2), LuaCore::getString(state, 3), LuaCore::getString(state, 4));
	LUA_RET_NULL();
}
int luaNavigation_send_message(lua_State* state){
	NavigationManager::get()->send_player_message(LuaCore::getString(state, 1), LuaCore::getString(state, 2));
	LUA_RET_NULL();
}
int luaNavigation_send_player_info(lua_State* state){
	SendInfoManager::get()->set_is_send_player_info(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL();
}
int luaNavigation_send_player_inventory(lua_State* state){
	SendInfoManager::get()->set_is_send_player_inventory(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL();
}
int luaNavigation_send_player_containers(lua_State* state){
	SendInfoManager::get()->set_is_send_player_containers(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL();
}
int luaNavigation_send_player_creatures(lua_State* state){
	SendInfoManager::get()->set_is_send_player_creatures(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL();
}
int luaNavigation_send_player_waypointer(lua_State* state){
	SendInfoManager::get()->set_is_send_player_waypointer(LuaCore::getBoolean(state, 1));
	LUA_RET_NULL();
}

int luaNavigation_delay_work(lua_State* state){
	SendInfoManager::get()->set_work_delay(LuaCore::getNumber<uint32_t>(state, 1));
	LUA_RET_NULL();
}

void LuaCore::BindLua(){
		KeywordManager::get()->set_lib_parsing("CoreLib");

		luaRegisterEnumTable("distance_param_row_t");
		registerEnumIn("distance_param_row_t", distance_param_creature)
		registerEnumIn("distance_param_row_t", distance_param_coordinate)
		registerEnumIn("distance_param_row_t", distance_param_none)


		luaRegisterEnumTable("lure_state_t");
		registerEnumIn("lure_state_t", lure_state_none)
		registerEnumIn("lure_state_t", lure_state_luring)
		registerEnumIn("lure_state_t", lure_state_reached)
		registerEnumIn("lure_state_t", lure_state_not_reachable)


		luaRegisterEnumTable("map_type_t");
		registerEnumIn("map_type_t", map_back_t)
		registerEnumIn("map_type_t", map_front_t)
		registerEnumIn("map_type_t", map_back_mini_t)
		registerEnumIn("map_type_t", map_front_mini_t)
		registerEnumIn("map_type_t", map_total_t)


		luaRegisterEnumTable("pathfinder_retval");
		registerEnumIn("pathfinder_retval", pathfinder_not_reachable)
		registerEnumIn("pathfinder_retval", pathfinder_reached)
		registerEnumIn("pathfinder_retval", pathfinder_going)
		registerEnumIn("pathfinder_retval", pathfinder_found)


		luaRegisterEnumTable("pathfinder_state");
		registerEnumIn("pathfinder_state", pathfinder_walk_none)
		registerEnumIn("pathfinder_state", pathfinder_walk_request_start)
		registerEnumIn("pathfinder_state", pathfinder_walk_going)
		registerEnumIn("pathfinder_state", pathfinder_walk_not_reachable)
		registerEnumIn("pathfinder_state", pathfinder_walk_overriden)
		registerEnumIn("pathfinder_state", pathfinder_walk_canceled)
		registerEnumIn("pathfinder_state", pathfinder_walk_reached)
		registerEnumIn("pathfinder_state", pathfinder_walk_condition_has_to_stop)
		registerEnumIn("pathfinder_state", pathfinder_walk_killed)
		registerEnumIn("pathfinder_state", pathfinder_state_none)


		luaRegisterEnumTable("thing_attribute");
		registerEnumInNotAuto("thing_attribute", "animate_always", ThingAttr::ThingAttrAnimateAlways)
		registerEnumInNotAuto("thing_attribute", "block_project_tile", ThingAttr::ThingAttrBlockProjectile)
		registerEnumInNotAuto("thing_attribute", "chargeable", ThingAttr::ThingAttrChargeable)
		registerEnumInNotAuto("thing_attribute", "cloth", ThingAttr::ThingAttrCloth)
		registerEnumInNotAuto("thing_attribute", "container", ThingAttr::ThingAttrContainer)
		registerEnumInNotAuto("thing_attribute", "displacement", ThingAttr::ThingAttrDisplacement)
		registerEnumInNotAuto("thing_attribute", "dont_hide", ThingAttr::ThingAttrDontHide)
		registerEnumInNotAuto("thing_attribute", "elevation", ThingAttr::ThingAttrElevation)
		registerEnumInNotAuto("thing_attribute", "floor_floor_change", ThingAttr::ThingAttrFloorChange)
		registerEnumInNotAuto("thing_attribute", "fluid_fluid_container", ThingAttr::ThingAttrFluidContainer)
		registerEnumInNotAuto("thing_attribute", "force_force_use", ThingAttr::ThingAttrForceUse)
		registerEnumInNotAuto("thing_attribute", "full_ground", ThingAttr::ThingAttrFullGround)
		registerEnumInNotAuto("thing_attribute", "ground", ThingAttr::ThingAttrGround)
		registerEnumInNotAuto("thing_attribute", "ground_border", ThingAttr::ThingAttrGroundBorder)
		registerEnumInNotAuto("thing_attribute", "hangable", ThingAttr::ThingAttrHangable)
		registerEnumInNotAuto("thing_attribute", "hook_east", ThingAttr::ThingAttrHookEast)
		registerEnumInNotAuto("thing_attribute", "hook_south", ThingAttr::ThingAttrHookSouth)
		registerEnumInNotAuto("thing_attribute", "lens_help", ThingAttr::ThingAttrLensHelp)
		registerEnumInNotAuto("thing_attribute", "light", ThingAttr::ThingAttrLight)
		registerEnumInNotAuto("thing_attribute", "look", ThingAttr::ThingAttrLook)
		registerEnumInNotAuto("thing_attribute", "lying_corpse", ThingAttr::ThingAttrLyingCorpse)
		registerEnumInNotAuto("thing_attribute", "market", ThingAttr::ThingAttrMarket)
		registerEnumInNotAuto("thing_attribute", "minimap_color", ThingAttr::ThingAttrMinimapColor)
		registerEnumInNotAuto("thing_attribute", "multi_use", ThingAttr::ThingAttrMultiUse)
		registerEnumInNotAuto("thing_attribute", "no_move_animation", ThingAttr::ThingAttrNoMoveAnimation)
		registerEnumInNotAuto("thing_attribute", "not_moveable", ThingAttr::ThingAttrNotMoveable)
		registerEnumInNotAuto("thing_attribute", "not_pathable", ThingAttr::ThingAttrNotPathable)
		registerEnumInNotAuto("thing_attribute", "not_pre_walkable", ThingAttr::ThingAttrNotPreWalkable)
		registerEnumInNotAuto("thing_attribute", "not_walkable", ThingAttr::ThingAttrNotWalkable)
		registerEnumInNotAuto("thing_attribute", "on_bottom", ThingAttr::ThingAttrOnBottom)
		registerEnumInNotAuto("thing_attribute", "on_top", ThingAttr::ThingAttrOnTop)
		registerEnumInNotAuto("thing_attribute", "opacity", ThingAttr::ThingAttrOpacity)
		registerEnumInNotAuto("thing_attribute", "pickupable", ThingAttr::ThingAttrPickupable)
		registerEnumInNotAuto("thing_attribute", "rotateable", ThingAttr::ThingAttrRotateable)
		registerEnumInNotAuto("thing_attribute", "splash", ThingAttr::ThingAttrSplash)
		registerEnumInNotAuto("thing_attribute", "stackable", ThingAttr::ThingAttrStackable)
		registerEnumInNotAuto("thing_attribute", "translucent", ThingAttr::ThingAttrTranslucent)
		registerEnumInNotAuto("thing_attribute", "usable", ThingAttr::ThingAttrUsable)
		registerEnumInNotAuto("thing_attribute", "writable", ThingAttr::ThingAttrWritable)
		registerEnumInNotAuto("thing_attribute", "writable_once", ThingAttr::ThingAttrWritableOnce)
		registerEnumInNotAuto("thing_attribute", "last_attr", ThingAttr::ThingLastAttr)


		luaRegisterEnumTable("vip_type_t");
		registerEnumIn("vip_type_t", vip_type_none)
		registerEnumIn("vip_type_t", vip_type_heart)
		registerEnumIn("vip_type_t", vip_type_skull)
		registerEnumIn("vip_type_t", vip_type_lightning)
		registerEnumIn("vip_type_t", vip_type_target)
		registerEnumIn("vip_type_t", vip_type_start)
		registerEnumIn("vip_type_t", vip_type_element)
		registerEnumIn("vip_type_t", vip_type_green_symbol)
		registerEnumIn("vip_type_t", vip_type_x)
		registerEnumIn("vip_type_t", vip_type_money)
		registerEnumIn("vip_type_t", vip_type_cross)


		luaRegisterEnumTable("hotkey_t");
		registerEnumIn("hotkey_t", hotkey_f1)
		registerEnumIn("hotkey_t", hotkey_f2)
		registerEnumIn("hotkey_t", hotkey_f3)
		registerEnumIn("hotkey_t", hotkey_f4)
		registerEnumIn("hotkey_t", hotkey_f5)
		registerEnumIn("hotkey_t", hotkey_f6)
		registerEnumIn("hotkey_t", hotkey_f7)
		registerEnumIn("hotkey_t", hotkey_f8)
		registerEnumIn("hotkey_t", hotkey_f9)
		registerEnumIn("hotkey_t", hotkey_f10)
		registerEnumIn("hotkey_t", hotkey_f11)
		registerEnumIn("hotkey_t", hotkey_f12)
		registerEnumIn("hotkey_t", hotkey_f1_shift)
		registerEnumIn("hotkey_t", hotkey_f2_shift)
		registerEnumIn("hotkey_t", hotkey_f3_shift)
		registerEnumIn("hotkey_t", hotkey_f4_shift)
		registerEnumIn("hotkey_t", hotkey_f5_shift)
		registerEnumIn("hotkey_t", hotkey_f6_shift)
		registerEnumIn("hotkey_t", hotkey_f7_shift)
		registerEnumIn("hotkey_t", hotkey_f8_shift)
		registerEnumIn("hotkey_t", hotkey_f9_shift)
		registerEnumIn("hotkey_t", hotkey_f10_shift)
		registerEnumIn("hotkey_t", hotkey_f11_shift)
		registerEnumIn("hotkey_t", hotkey_f12_shift)
		registerEnumIn("hotkey_t", hotkey_f1_ctrl)
		registerEnumIn("hotkey_t", hotkey_f2_ctrl)
		registerEnumIn("hotkey_t", hotkey_f3_ctrl)
		registerEnumIn("hotkey_t", hotkey_f4_ctrl)
		registerEnumIn("hotkey_t", hotkey_f5_ctrl)
		registerEnumIn("hotkey_t", hotkey_f6_ctrl)
		registerEnumIn("hotkey_t", hotkey_f7_ctrl)
		registerEnumIn("hotkey_t", hotkey_f8_ctrl)
		registerEnumIn("hotkey_t", hotkey_f9_ctrl)
		registerEnumIn("hotkey_t", hotkey_f10_ctrl)
		registerEnumIn("hotkey_t", hotkey_f11_ctrl)
		registerEnumIn("hotkey_t", hotkey_f12_ctrl)
		registerEnumIn("hotkey_t", hotkey_t_count)
		registerEnumIn("hotkey_t", hotkey_any)
		registerEnumIn("hotkey_t", hotkey_none)


		luaRegisterEnumTable("hotkey_cast_type");
		registerEnumIn("hotkey_cast_type", hotkey_cast_type_with_crosshairs)
		registerEnumIn("hotkey_cast_type", hotkey_cast_type_on_target)
		registerEnumIn("hotkey_cast_type", hotkey_cast_type_on_self)
		registerEnumIn("hotkey_cast_type", hotkey_cast_type_toggle_equip)
		registerEnumIn("hotkey_cast_type", hotkey_cast_any)


		luaRegisterEnumTable("use_spell_type");
		registerEnumIn("use_spell_type", use_spell_fast)
		registerEnumIn("use_spell_type", use_spell_by_mounster_count)
		registerEnumIn("use_spell_type", use_spell_by_safety)


		luaRegisterEnumTable("pathfinder_retval");
		registerEnumIn("pathfinder_retval", pathfinder_not_reachable)
		registerEnumIn("pathfinder_retval", pathfinder_reached)
		registerEnumIn("pathfinder_retval", pathfinder_going)
		registerEnumIn("pathfinder_retval", pathfinder_found)


		luaRegisterEnumTable("pathfinder_line_t");
		registerEnumIn("pathfinder_line_t", pathfinder_line_1)
		registerEnumIn("pathfinder_line_t", pathfinder_line_2)
		registerEnumIn("pathfinder_line_t", pathfinder_line_3)


		//	luaRegisterEnumTable("status");
		//	registerEnumIn("status", poison)
		//	registerEnumIn("status", fire)
		//	registerEnumIn("status", energy)
		//	registerEnumIn("status", drunk)
		//	registerEnumIn("status", utamo)
		//	registerEnumIn("status", slow)
		//	registerEnumIn("status", haste)
		//	registerEnumIn("status", battle)
		//	registerEnumIn("status", water)
		//	registerEnumIn("status", frozen)
		//	registerEnumIn("status", holly)
		//	registerEnumIn("status", curse)
		//	registerEnumIn("status", streng_up)
		//	registerEnumIn("status", battle_red)
		//	registerEnumIn("status", pz)
		//	registerEnumIn("status", blood)


		luaRegisterEnumTable("inventory_slot_t");
		registerEnumIn("inventory_slot_t", slot_amulet)
		registerEnumIn("inventory_slot_t", slot_armor)
		registerEnumIn("inventory_slot_t", slot_bag)
		registerEnumIn("inventory_slot_t", slot_boots)
		registerEnumIn("inventory_slot_t", slot_helmet)
		registerEnumIn("inventory_slot_t", slot_leg)
		registerEnumIn("inventory_slot_t", slot_none)
		registerEnumIn("inventory_slot_t", slot_ring)
		registerEnumIn("inventory_slot_t", slot_rope)
		registerEnumIn("inventory_slot_t", slot_shield)
		registerEnumIn("inventory_slot_t", slot_weapon)


	{
		using namespace side_t;
		luaRegisterEnumTable("side_t");
		registerEnumIn("side_t", side_north)
		registerEnumIn("side_t", side_north_east)
		registerEnumIn("side_t", side_east)
		registerEnumIn("side_t", side_south_east)
		registerEnumIn("side_t", side_south)
		registerEnumIn("side_t", side_south_west)
		registerEnumIn("side_t", side_west)
		registerEnumIn("side_t", side_north_west)
	}
	{
		using namespace side_nom_axis_t;
		luaRegisterEnumTable("side_nom_axis_t");
		registerEnumIn("side_nom_axis_t", side_north)
		registerEnumIn("side_nom_axis_t", side_east)
		registerEnumIn("side_nom_axis_t", side_south)
		registerEnumIn("side_nom_axis_t", side_west)
	}

		luaRegisterEnumTable("hotkey_cast_type_t");
		registerEnumIn("hotkey_cast_type_t", hotkey_cast_type_with_crosshairs)
		registerEnumIn("hotkey_cast_type_t", hotkey_cast_type_on_target)
		registerEnumIn("hotkey_cast_type_t", hotkey_cast_type_on_self)
		registerEnumIn("hotkey_cast_type_t", hotkey_cast_type_toggle_equip)
		registerEnumIn("hotkey_cast_type_t", hotkey_cast_any)

		luaRegisterEnumTable("pvp_old_t");
		registerEnumIn("pvp_old_t", pvp_enabled)
		registerEnumIn("pvp_old_t", pvp_disabled)

		luaRegisterEnumTable("pvp_new_t");
		registerEnumIn("pvp_new_t", pvp_new_1)
		registerEnumIn("pvp_new_t", pvp_new_2)
		registerEnumIn("pvp_new_t", pvp_new_3)
		registerEnumIn("pvp_new_t", pvp_new_4)

		luaRegisterEnumTable("attack_mode_t");
		registerEnumIn("attack_mode_t", attack_mode_offensive)
		registerEnumIn("attack_mode_t", attack_mode_balanced)
		registerEnumIn("attack_mode_t", attack_mode_defensive)

		luaRegisterEnumTable("loot_sequence_t");
		registerEnumIn("loot_sequence_t", loot_sequence_inteligent)
		registerEnumIn("loot_sequence_t", loot_sequence_instant)
		registerEnumIn("loot_sequence_t", loot_sequence_after_kill_all)
		registerEnumIn("loot_sequence_t", loot_sequence_t_total)
		registerEnumIn("loot_sequence_t", loot_sequence_t_total)
		registerEnumIn("loot_sequence_t", loot_sequence_t_none)

		luaRegisterEnumTable("loot_filter_t");
		registerEnumIn("loot_filter_t", loot_filter_t_from_target)
		registerEnumIn("loot_filter_t", loot_filter_t_from_all)
		registerEnumIn("loot_filter_t", loot_filter_t_from_all_on_hunter)
		registerEnumIn("loot_filter_t", loot_filter_t_total)
		registerEnumIn("loot_filter_t", loot_filter_t_none)

		luaRegisterEnumTable("loot_item_priority_t");
		registerEnumIn("loot_item_priority_t", loot_item_priority_min)
		registerEnumIn("loot_item_priority_t", loot_item_priority_medium)
		registerEnumIn("loot_item_priority_t", loot_item_priority_max)
		registerEnumIn("loot_item_priority_t", loot_item_priority_total)
		registerEnumIn("loot_item_priority_t", loot_item_priority_none)

		//luaRegisterEnumTable("loot_item_new_condition_t");
		//registerEnumIn("loot_item_new_condition_t", loot_item_condition_none)
		//registerEnumIn("loot_item_new_condition_t", drop_if_destination_full)
		//registerEnumIn("loot_item_new_condition_t", drop_if_cap_bellow)
		//registerEnumIn("loot_item_new_condition_t", drop_if_have_more_than)
		//registerEnumIn("loot_item_new_condition_t", dont_loot_if_have_more_than)
		//registerEnumIn("loot_item_new_condition_t", dont_loot_if_cap_bellow_than)
		//registerEnumIn("loot_item_new_condition_t", drop)
		//registerEnumIn("loot_item_new_condition_t", drop_from_own_backpack_if_have_more_than)
		//registerEnumIn("loot_item_new_condition_t", drop_from_own_backpack_if_cap_bellow_than)
		//registerEnumIn("loot_item_new_condition_t", loot_from_the_floor_while_cap_more_than)
		//registerEnumIn("loot_item_new_condition_t", loot_item_new_condition_total)

		luaRegisterEnumTable("exani_hur_t");
		registerEnumIn("exani_hur_t", exani_hur_up)
		registerEnumIn("exani_hur_t", exani_hur_down)

		luaRegisterEnumTable("alert_t");
		registerEnumIn("alert_t", alert_none)
		registerEnumIn("alert_t", alert_player_on_screen)
		registerEnumIn("alert_t", alert_player_attack)
		registerEnumIn("alert_t", alert_disconnect)
		registerEnumIn("alert_t", alert_player_kill)
		registerEnumIn("alert_t", alert_cap)
		registerEnumIn("alert_t", alert_total)


		luaRegisterEnumTable("action_retval_t");
		registerEnumIn("action_retval_t", action_retval_fail)
		registerEnumIn("action_retval_t", action_retval_trying)
		registerEnumIn("action_retval_t", action_retval_timeout)
		registerEnumIn("action_retval_t", action_retval_not_near)
		registerEnumIn("action_retval_t", action_retval_total)
		registerEnumIn("action_retval_t", action_retval_none)
		registerEnumIn("action_retval_t", action_retval_success)

		luaRegisterEnumTable("spell_type_t");
		registerEnumIn("spell_type_t", spell_type_healing)
		registerEnumIn("spell_type_t", spell_type_on_target)
		registerEnumIn("spell_type_t", spell_type_area)
		registerEnumIn("spell_type_t", spell_type_rune_on_target)
		registerEnumIn("spell_type_t", spell_type_rune_area)

		luaRegisterEnumTable("spell_condition_t");
		registerEnumIn("spell_condition_t", OnlyUse)
		registerEnumIn("spell_condition_t", OnlyWithPlayerOnScreen)
		registerEnumIn("spell_condition_t", OnlyWithoutPlayerOnScreen)

		luaRegisterEnumTable("pathfinder_walkability_t");
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_not_walkable)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_walkable)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_player)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_monster)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_summon)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_npc)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_furniture)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_rune_field)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_timed_not_walkable)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_near_depot_walkable)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_near_depot_unwalkable)
		registerEnumIn("pathfinder_walkability_t", pathinder_walkability_undefined)


		luaRegisterEnumTable("creature_type");
		registerEnumIn("creature_type", creature_type_player)
		registerEnumIn("creature_type", creature_type_monster)
		registerEnumIn("creature_type", creature_type_npc)
		registerEnumIn("creature_type", creature_type_our_summon)
		registerEnumIn("creature_type", creature_type_other_summon)

		luaRegisterEnumTable("skull_type");
		registerEnumIn("skull_type", skull_yellow)
		registerEnumIn("skull_type", skull_green)
		registerEnumIn("skull_type", skull_white)
		registerEnumIn("skull_type", skull_red)
		registerEnumIn("skull_type", skull_black)
		registerEnumIn("skull_type", skull_orange)



	enum special_area_element_t{
		sa_element_area,
		sa_element_point,
		sa_element_wall,
		sa_element_total,
		sa_element_any,
		sa_element_none
	};

	enum special_area_activation_t{
		sa_activation_inside_area,
		sa_activation_on_lured,
		sa_activation_on_target,
		sa_activation_on_hunter_creature,
		sa_activation_on_creature,
		sa_activation_on_player,
		sa_activation_on_lured_reached,
		sa_activation_manual_lua
	};


	
#pragma region BIND_STATUS
	registerTable("Status");
	registerMethod("Status", "slow", lua_status_slow);
	registerMethod("Status", "poison", lua_status_poison);
	registerMethod("Status", "fire", lua_status_fire);
	registerMethod("Status", "energy", lua_status_energy);
	registerMethod("Status", "drunk", lua_status_drunk);
	registerMethod("Status", "utamo", lua_status_utamo);
	registerMethod("Status", "haste", lua_status_haste);
	registerMethod("Status", "battle", lua_status_battle);
	registerMethod("Status", "water", lua_status_water);
	registerMethod("Status", "frozen", lua_status_frozen);
	registerMethod("Status", "holly", lua_status_holly);
	registerMethod("Status", "curse", lua_status_curse);
	registerMethod("Status", "streng_up", lua_status_streng_up);
	registerMethod("Status", "battle_red", lua_status_battle_red);
	registerMethod("Status", "pz", lua_status_pz);
	registerMethod("Status", "blood", lua_status_blood);
#pragma endregion 

	registerTable("ItemsManager");
	registerMethod("ItemsManager", "is_lying_corpse", luaItemsManageris_lying_corpse);
	registerMethod("ItemsManager", "get_item_id_from_name", luaItemsManagergetitem_idFromName);
	registerMethod("ItemsManager", "get_item_name_from_id", luaItemsManagergetItemNameFromId);
	registerMethod("ItemsManager", "is_item_id", lua_ItemsManager_isitem_id);
	registerMethod("ItemsManager", "is_rune_field_item", lua_ItemsManager_isRuneFieldItem);
	registerMethod("ItemsManager", "get_item_price", lua_ItemsManager_getItemPrice);
	registerMethod("ItemsManager", "get_items_as_pick", luaItemsManagerget_items_as_pick);
	registerMethod("ItemsManager", "get_items_as_rope", luaItemsManagerget_items_as_rope);
	registerMethod("ItemsManager", "get_items_as_shovel", luaItemsManagerget_items_as_shovel);
	registerMethod("ItemsManager", "get_items_as_take_skin", luaItemsManagerget_items_as_take_skin);
	registerMethod("ItemsManager", "get_items_as_machete", luaItemsManagerget_items_as_machete);
	registerMethod("ItemsManager", "get_items_as_food", luaItemsManagerget_items_as_food);
	registerMethod("ItemsManager", "get_items_as_sickle", luaItemsManagerget_items_as_sickle);
	registerMethod("ItemsManager", "get_items_as_scythe", luaItemsManagerget_items_as_scythe);
	registerMethod("ItemsManager", "get_items_as_crowbar", luaItemsManagerget_items_as_crowbar);
	registerMethod("ItemsManager", "get_items_as_spoon", luaItemsManagerget_items_as_spoon);
	registerMethod("ItemsManager", "get_items_as_knife", luaItemsManagerget_items_as_knife);
	registerMethod("ItemsManager", "get_items_as_kitchen", luaItemsManagerget_items_as_kitchen);
	registerMethod("ItemsManager", "get_items_as_empty_potions", luaItemsManagerget_items_as_empty_potions);
	registerMethod("ItemsManager", "get_items_as_fields_runes", luaItemsManagerget_items_as_fields_runes);

#pragma region BIND_REPOTERCORE

	registerTable("Repoter");
	registerMethod("Repoter", "open_trade", lua_repotercore_open_trade);
	registerMethod("Repoter", "buy_item_repot_id", lua_repotercore_buy_item_repot_id);
	registerMethod("Repoter", "total_money_all_repot", lua_repotercore_total_money_all_repot);
	registerMethod("Repoter", "total_money_repot", lua_repotercore_total_money_repot);
	registerMethod("Repoter", "is_necessary_all_repot", lua_repotercore_is_necessary_all_repot);
	registerMethod("Repoter", "is_necessary_repot", lua_repotercore_is_necessary_repot);
	registerMethod("Repoter", "withdraw_repot", lua_repotercore_withdraw_repot);
	registerMethod("Repoter", "sell_empty_vials", lua_repotercore_sell_empty_vials);
	registerMethod("Repoter", "is_necesary_all_repot", luaRepoteris_necesary_all_repot);
	registerMethod("Repoter", "is_necesary_repot_id", luaRepoteris_necesary_repot_id);

	registerTable("Depoter");
	registerMethod("Depoter", "is_necesary_some_depot", luaDepoteris_necesary_some_depot);
	registerMethod("Depoter", "is_necesary_depot_id", luaDepoterin_necesary_depot_id);

#pragma endregion

	registerTable("Actions");
	registerMethod("Actions", "move_item_container_to_coordinate", luaActionsmove_item_container_to_coordinate);
	registerMethod("Actions", "open_browse_field", lua_open_browse_field);
	registerMethod("Actions", "travel", lua_travel);
	registerMethod("Actions", "drop_empty_potions", lua_drop_empty_potions);
	registerMethod("Actions", "refill_amun", lua_actions_refill_amun);
	registerMethod("Actions", "move_item_container_to_container", luaActionsmove_item_container_to_container);
	registerMethod("Actions", "move_item_container_to_slot", luaActionsmove_item_container_to_slot);
	registerMethod("Actions", "move_item_coordinate_to_container", luaActionsmove_item_coordinate_to_container);
	registerMethod("Actions", "move_item_coordinate_to_slot", luaActionsmove_item_coordinate_to_slot);
	registerMethod("Actions", "move_item_coordinate_to_coordinate", luaActionsmove_item_coordinate_to_coordinate);
	registerMethod("Actions", "move_item_slot_to_slot", luaActionsmove_item_slot_to_slot);
	registerMethod("Actions", "move_item_slot_to_coordinate", luaActionsmove_item_slot_to_coordinate);
	registerMethod("Actions", "move_item_slot_to_container", luaActionsmove_item_slot_to_container);
	registerMethod("Actions", "use_item_on_inventory", luaActionsuse_item_on_inventory);
	registerMethod("Actions", "use_item_on_container", luaActionsuse_item_on_container);
	registerMethod("Actions", "use_item_on_coordinate", luaActionsuse_item_on_coordinate);
	registerMethod("Actions", "use_crosshair_item_on_inventory", luaActionsuse_crosshair_item_on_inventory);
	registerMethod("Actions", "use_crosshair_item_on_coordinate", luaActionsuse_crosshair_item_on_coordinate);
	registerMethod("Actions", "use_crosshair_item_on_container", luaActionsuse_crosshair_item_on_container);
	registerMethod("Actions", "goto_coord", luaActionsgoto_coord);
	registerMethod("Actions", "goto_coord_on_screen", luaActionsgoto_coord_on_screen);
	registerMethod("Actions", "simple_go_coord_on_screen", luaActionssimple_go_coord_on_screen);
	registerMethod("Actions", "get_coord_near_other", luaActionsget_coord_near_other);
	registerMethod("Actions", "goto_near_coord", luaActionsgoto_near_coord);
	registerMethod("Actions", "open_hole_with_shovel", luaActionsopen_hole_with_shovel);
	registerMethod("Actions", "clear_coordinate", luaActionsclear_coordinate);
	registerMethod("Actions", "open_hole_with_tool_id", luaActionsopen_hole_with_tool_id);
	registerMethod("Actions", "clear_coord_to_loot", luaActionsclear_coord_to_loot);
	registerMethod("Actions", "dropitem", luaActionsdropitem);
	registerMethod("Actions", "destroy_field", luaActionsdestory_field);
	registerMethod("Actions", "pick_up_item", luaActionspick_up_item);
	registerMethod("Actions", "fish_process", luaActionsfish_process);
	registerMethod("Actions", "use_fishing_rod", luaActionsuse_fishing_rod);
	registerMethod("Actions", "execute_dance", luaActionsexecute_dance);
	registerMethod("Actions", "eat_food", luaActionseat_food);
	registerMethod("Actions", "open_depot", luaActionsopen_depot);
	registerMethod("Actions", "says_to_npc", luaActionssays_to_npc);
	registerMethod("Actions", "says", luaActionssays); 
	registerMethod("Actions", "loggout", luaActionsloggout);
	registerMethod("Actions", "open_door", luaActionsopen_door);
	registerMethod("Actions", "turn", luaActionsturn);
	registerMethod("Actions", "step_side", luaActionsstep_side);
	registerMethod("Actions", "goto_nearest_from_creature", lua_actions_goto_nearest_from_creature);
	registerMethod("Actions", "up_ladder", lua_actions_up_ladder);
	registerMethod("Actions", "use_lever", lua_actions_use_lever);
	registerMethod("Actions", "join_in_telepot", lua_actions_join_in_telepot);
	registerMethod("Actions", "use_item_id_to_down_or_up", lua_actions_use_item_id_to_down_or_up);
	registerMethod("Actions", "check_move_item_container_to_container", lua_actions_check_move_item_container_to_container);
	registerMethod("Actions", "reopen_containers", lua_actions_reopen_containers);
	registerMethod("Actions", "up_backpack", lua_actions_up_backpack);
	registerMethod("Actions", "open_depot_chest", lua_actions_open_depot_chest);
	registerMethod("Actions", "open_container_at_coordinate", lua_actions_open_container_at_coordinate);
	registerMethod("Actions", "open_main_bp", lua_actions_open_main_bp);
	registerMethod("Actions", "put_ring", lua_actions_put_ring);
	registerMethod("Actions", "open_container_id", lua_actions_open_container_id);
	registerMethod("Actions", "use_item", lua_actions_use_item);
	registerMethod("Actions", "set_follow", lua_actions_set_follow);
	registerMethod("Actions", "drop_item", lua_actions_drop_item);
	registerMethod("Actions", "unset_follow", lua_actions_unset_follow);
	registerMethod("Actions", "use_exani_hur", lua_actions_use_exani_hur);
	registerMethod("Actions", "reach_creature", lua_actions_reach_creature);
	registerMethod("Actions", "deposit_items", lua_actions_deposit_items);
	registerMethod("Actions", "move_creature", lua_actions_move_creature);
	registerMethod("Actions", "deposit_all", lua_actions_deposit_all);
	registerMethod("Actions", "transfer_money", lua_actions_transfer_money);
	registerMethod("Actions", "deposit_money", lua_actions_deposit_money);
	registerMethod("Actions", "withdraw_value", lua_actions_withdraw_value);
	registerMethod("Actions", "play_alert", lua_actions_play_alert);
	registerMethod("Actions", "take_up_items_from_depot", lua_actions_take_up_items_from_depot);

	
	registerTable("Self");
	registerMethod("Self", "ammo", luaSelfammo); 
	registerMethod("Self", "mount", luaSelfmount);
	registerMethod("Self", "unmount", luaSelfunmount);
	registerMethod("Self", "ammoamount", luaSelfammoamount);
	registerMethod("Self", "amulet", luaSelfamulet);
	registerMethod("Self", "armor", luaSelfarmor);
	registerMethod("Self", "axe", luaSelfaxe);
	registerMethod("Self", "axepc", luaSelfaxepc);
	registerMethod("Self", "back", luaSelfback);
	registerMethod("Self", "boots", luaSelfboots);
	registerMethod("Self", "cap", luaSelfcap);
	registerMethod("Self", "club", luaSelfclub);
	registerMethod("Self", "clubpc", luaSelfclubpc);
	registerMethod("Self", "direction", luaSelfdirection);
	registerMethod("Self", "distance", luaSelfdistance);
	registerMethod("Self", "distancepc", luaSelfdistancepc);
	registerMethod("Self", "exp", luaSelfexp);
	registerMethod("Self", "first", luaSelffirst);
	registerMethod("Self", "firstpc", luaSelffirstpc);
	registerMethod("Self", "fishing", luaSelffishing);
	registerMethod("Self", "fishingpc", luaSelffishingpc);
	registerMethod("Self", "helmet", luaSelfhelmet);
	registerMethod("Self", "hp", luaSelfhp);
	registerMethod("Self", "hppc", luaSelfhppc);
	registerMethod("Self", "id", luaSelfid);
	registerMethod("Self", "legs", luaSelflegs);
	registerMethod("Self", "level", luaSelflevel);
	registerMethod("Self", "max_hp", luaSelfmax_hp);
	registerMethod("Self", "max_mp", luaSelfmax_mp);
	registerMethod("Self", "mllevel", luaSelfmllevel);
	registerMethod("Self", "mlevelpc", luaSelfmlevelpc);
	registerMethod("Self", "mp", luaSelfmp);
	registerMethod("Self", "mppc", luaSelfmppc);
	registerMethod("Self", "name", luaSelfname);
	registerMethod("Self", "offtrain", luaSelfofftrain);
	registerMethod("Self", "outfit", luaSelfoutfit);
	registerMethod("Self", "posx", luaSelfposx);
	registerMethod("Self", "posy", luaSelfposy);
	registerMethod("Self", "posz", luaSelfposz);
	registerMethod("Self", "profession", luaSelfprofession);
	registerMethod("Self", "ring", luaSelfring);
	registerMethod("Self", "shield", luaSelfshield);
	registerMethod("Self", "shielding", luaSelfshielding);
	registerMethod("Self", "shieldingpc", luaSelfshieldingpc);
	registerMethod("Self", "soul", luaSelfsoul);
	registerMethod("Self", "stamina", luaSelfstamina);
	registerMethod("Self", "sword", luaSelfsword);
	registerMethod("Self", "swordpc", luaSelfswordpc);
	registerMethod("Self", "visible", luaSelfvisible);
	registerMethod("Self", "weapon", luaSelfweapon);
	registerMethod("Self", "weaponamount", luaSelfweaponamount);
	registerMethod("Self", "coordinate", luaSelfcoodinate);
	registerMethod("Self", "position", luaSelfposition);
	registerMethod("Self", "get_displacement", luaSelfget_displacement);
	registerMethod("Self", "get_going_step", luaSelfget_going_step);
	
	registerTable("Thread");
	registerMethod("Thread", "sleep", luaThreadsleep);

	registerTable("TibiaHotkeys");
	registerMethod("TibiaHotkeys", "missing_hotkeys", luaHotkeyManagermissing_hotkeys);
	registerMethod("TibiaHotkeys", "get_hotkey_match", luaHotkeyManagerget_hotkey_match);
	registerMethod("TibiaHotkeys", "query_hotkeys", luaHotkeyManagerquery_hotkeys);
	registerMethod("TibiaHotkeys", "get_hotkey", luaHotkeyManagergetHotkeyState);
	registerMethod("TibiaHotkeys", "get_current_hotkey_profile_name", luaHotkeyManagergetCurrentHotkeyProfileName);
	registerMethod("TibiaHotkeys", "use_hotkey", luaHotkeyManageruseHotkey);
	registerMethod("TibiaHotkeys", "use_hotkey_at_location", luaHotkeyManageruseHotkeyAtLocation);
	registerMethod("TibiaHotkeys", "use_item_at_location", luaHotkeyManageruseItemAtLocation);
	registerMethod("TibiaHotkeys", "toggle_equip_item", luaHotkeyManagertoggleEquipItem);

	registerTable("NpcTrade");
	registerMethod("NpcTrade", "is_open", luaNpcTradeis_open);
	registerMethod("NpcTrade", "resize", luaNpcTraderesize);
	registerMethod("NpcTrade", "sell_item", luaNpcTradesell_item);
	registerMethod("NpcTrade", "buy_item", luaNpcTradebuy_item);

	registerTable("Pathfinder");
	registerMethod("Pathfinder", "get_use_diagonal", luaPathfinderget_use_diagonal);
	registerMethod("Pathfinder", "set_use_diagonal", luaPathfinderset_use_diagonal);
	registerMethod("Pathfinder", "walk_around", luaPathfinderwalk_around);
	registerMethod("Pathfinder", "is_reachable", luaPathfinderis_reachable);
	registerMethod("Pathfinder", "set_go_to", luaPathfinderset_go_to);
	registerMethod("Pathfinder", "walk_near_to", luaPathfinderwalk_near_to);
	registerMethod("Pathfinder", "cancel_all", luaPathfindercancel_all);
	registerMethod("Pathfinder", "step_to_coordinate", luaPathfinderstep_to_coordinate);
	registerMethod("Pathfinder", "get_consider_near_yellow_coords", luaPathfinderget_consider_near_yellow_coords);
	registerMethod("Pathfinder", "get_time_to_wait_near_yellow_coords", luaPathfinderget_time_to_wait);
	registerMethod("Pathfinder", "set_consider_near_yellow_coords", luaPathfinderset_consider_near_yellow_coords);
	registerMethod("Pathfinder", "set_time_to_wait_near_yellow_coords", luaPathfinderset_time_to_wait);
	
	registerTable("Inventory");
	registerMethod("Inventory", "ammunation_count", luaInventoryammunation_count);
	registerMethod("Inventory", "amulet", luaInventorybody_amulet);
	registerMethod("Inventory", "armor", luaInventorybody_armor);
	registerMethod("Inventory", "bag", luaInventorybody_bag);
	registerMethod("Inventory", "boots", luaInventorybody_boot);
	registerMethod("Inventory", "helmet", luaInventorybody_helmet);
	registerMethod("Inventory", "legs", luaInventorybody_leg);
	registerMethod("Inventory", "ring", luaInventorybody_ring);
	registerMethod("Inventory", "rope", luaInventorybody_rope);
	registerMethod("Inventory", "shield", luaInventorybody_shield);
	registerMethod("Inventory", "weapon", luaInventorybody_weapon);
	registerMethod("Inventory", "is_item_equiped", luaInventoryis_item_equiped);
	registerMethod("Inventory", "is_maximized", luaInventoryis_maximized);
	registerMethod("Inventory", "maximize", luaInventory_maximize);
	registerMethod("Inventory", "minimize", luaInventory_minimize);
	registerMethod("Inventory", "weapon_count", luaInventoryweapon_count);
	registerMethod("Inventory", "put_item", luaInventoryput_item);
	registerMethod("Inventory", "unequip_slot", luaInventoryunequip_slot);
	registerMethod("Inventory", "look_at_inventory", luaInventorylook_at_inventory);
	registerMethod("Inventory", "item_count", luaInventoryitem_count);
	
	registerTable("SpellsManager");
	registerMethod("SpellsManager", "can_cast", luacan_cast);
	registerMethod("SpellsManager", "need_operate", luaSpellsManagerneed_operate);

	registerTable("Interface");
	registerMethod("Interface", "open_battle", luaopen_battle);
	registerMethod("Interface", "close_battle", luaclose_battle);
	registerMethod("Interface", "is_battle_container_open", luaget_battle_container);

	registerTable("Containers");
	registerMethod("Containers", "get_container_by_caption", luaContainersget_container_by_caption);
	registerMethod("Containers", "get_container_by_id", luaContainersget_container_by_id);
	registerMethod("Containers", "get_depot_chest_container", luaContainersget_depot_chest_container);
	registerMethod("Containers", "get_depot_locker_container", luaContainersget_depot_locker_container);
	registerMethod("Containers", "get_browse_field", luaContainersget_browse_field);
	registerMethod("Containers", "get_total_money", luaContainersget_total_money);
	registerMethod("Containers", "get_browse_fields", luaContainerget_browse_fields);
	registerMethod("Containers", "query_container_positions_where", luaContainerget_container_positions_where);
	registerMethod("Containers", "get_item_count", luaContainersget_item_count);
	registerMethod("Containers", "get_containers_open", lua_containers_get_containers_open);
	registerMethod("Containers", "get_used_slots", lua_containers_get_used_slots);
	registerMethod("Containers", "find_item_slot", lua_containers_find_item_slot);
	registerMethod("Containers", "get_slot_count", lua_containers_get_slot_count);
	registerMethod("Containers", "is_open", lua_containers_is_open);
	registerMethod("Containers", "get_slot_id", lua_containers_get_slot_id);
	registerMethod("Containers", "get_food_count", luaContainerget_food_count);
	registerMethod("Containers", "close_all_creature_containers", luaContainerclose_all_creature_containers);
	registerMethod("Containers", "close_all_containers", luaContainerclose_all_containers);
	registerMethod("Containers", "close_all_browse_fields", luaContainerclose_all_browse_fields);
	registerMethod("Containers", "close_containers_by_index", luaContainerclose_containers_by_index);
	registerMethod("Containers", "get_all_containers_id_opened", luaContainerget_all_containers_id_opened);
	registerMethod("Containers", "get_all_items_is_in_open_containers", luaContainerget_all_items_is_in_open_containers);
	registerMethod("Containers", "wait_browse_field_close", luaContainerwait_browse_field_close);
	registerMethod("Containers", "wait_container_id_close", luaContainerwait_container_browse_field);
	registerMethod("Containers", "wait_container_id_close", luaContainerwait_container_id_close);
	registerMethod("Containers", "wait_container_id_open", luaContainerwait_container_id_open);
	registerMethod("Containers", "wait_container_index_close", luaContainerwait_container_index_close);
	registerMethod("Containers", "wait_depot_locker_open", luaContainerwait_depot_locker_open);
	registerMethod("Containers", "wait_open_container_count_change", luaContainerwait_open_container_count_change);
	registerMethod("Containers", "wait_open_trade", luaContainerwait_open_trade);

	registerTable("PvpManager");
	registerMethod("PvpManager", "get_attack_mode", luaPvpManagerget_attack_mode);
	registerMethod("PvpManager", "get_skull_mode_new", luaPvpManagerget_skull_mode_new);
	registerMethod("PvpManager", "get_skull_mode_old", luaPvpManagerget_skull_mode_old);
	registerMethod("PvpManager", "set_balanced_attack_mode", luaPvpManagerset_balanced_attack_mode);
	registerMethod("PvpManager", "set_defensive_attack_mode", luaPvpManagerset_defensive_attack_mode);
	registerMethod("PvpManager", "set_offensive_attack_mode", luaPvpManagerset_offensive_attack_mode);
	registerMethod("PvpManager", "set_skull_mode_new", luaPvpManagerset_skull_mode_new);
	registerMethod("PvpManager", "set_skull_mode_old", luaPvpManagerset_skull_mode_old);

	registerTable("HunterActions");
	registerMethod("HunterActions", "attack_creature_on_battle", luaHunterActionsattack_creature_on_battle);
	registerMethod("HunterActions", "attack_creature_on_coordinates", luaHunterActionsattack_creature_on_coordinates);
	registerMethod("HunterActions", "attack_creature_by_name", luaHunterActionsattack_creature_by_name);
	registerMethod("HunterActions", "attack_creature_id", luaHunterActionsattack_creature_id);
	registerMethod("HunterActions", "wait_creature_out_of_range", luaBattleListwait_creature_out_of_range);
	registerMethod("HunterActions", "keep_distance", luakeep_distance);
	registerMethod("HunterActions", "exist_creature_to_attack", luaHunterCoreexist_creature_to_attack);

	registerTable("NaviPlayerInfo");
	registerMethod("NaviPlayerInfo", "get_last_time_recv", luaNavigation_get_last_time_recv);
	registerMethod("NaviPlayerInfo", "exp_to_next_lvl", luaNavigation_exp_to_next_lvl);
	registerMethod("NaviPlayerInfo", "experience", luaNavigation_experience);
	registerMethod("NaviPlayerInfo", "time_to_next_lvl", luaNavigation_time_to_next_lvl);
	registerMethod("NaviPlayerInfo", "get_name_char", luaNavigation_get_name_char);
	registerMethod("NaviPlayerInfo", "self_level", luaNavigation_self_level);
	registerMethod("NaviPlayerInfo", "self_levelpc", luaNavigation_self_levelpc);
	registerMethod("NaviPlayerInfo", "self_ml", luaNavigation_self_ml);
	registerMethod("NaviPlayerInfo", "self_mlpc", luaNavigation_self_mlpc);
	registerMethod("NaviPlayerInfo", "self_first", luaNavigation_self_first);
	registerMethod("NaviPlayerInfo", "self_firstpc", luaNavigation_self_firstpc);
	registerMethod("NaviPlayerInfo", "self_club", luaNavigation_self_club);
	registerMethod("NaviPlayerInfo", "self_clubpc", luaNavigation_self_clubpc);
	registerMethod("NaviPlayerInfo", "self_sword", luaNavigation_self_sword);
	registerMethod("NaviPlayerInfo", "self_swordpc", luaNavigation_self_swordpc);
	registerMethod("NaviPlayerInfo", "self_axe", luaNavigation_self_axe);
	registerMethod("NaviPlayerInfo", "self_axepc", luaNavigation_self_axepc);
	registerMethod("NaviPlayerInfo", "self_distance", luaNavigation_self_distance);
	registerMethod("NaviPlayerInfo", "self_distancepc", luaNavigation_self_distancepc);
	registerMethod("NaviPlayerInfo", "self_shielding", luaNavigation_self_shielding);
	registerMethod("NaviPlayerInfo", "self_shieldingpc", luaNavigation_self_shieldingpc);
	registerMethod("NaviPlayerInfo", "self_fishing", luaNavigation_self_fishing);
	registerMethod("NaviPlayerInfo", "self_fishingpc", luaNavigation_self_fishingpc);
	registerMethod("NaviPlayerInfo", "get_self_coordinate", luaNavigation_get_self_coordinate);
	registerMethod("NaviPlayerInfo", "self_maxhp", luaNavigation_self_maxhp);
	registerMethod("NaviPlayerInfo", "self_maxmp", luaNavigation_self_maxmp);
	registerMethod("NaviPlayerInfo", "current_hp", luaNavigation_current_hp);
	registerMethod("NaviPlayerInfo", "current_hppc", luaNavigation_current_hppc);
	registerMethod("NaviPlayerInfo", "current_mp", luaNavigation_current_mp);
	registerMethod("NaviPlayerInfo", "current_mmpc", luaNavigation_current_mmpc);
	registerMethod("NaviPlayerInfo", "self_cap", luaNavigation_self_cap);
	registerMethod("NaviPlayerInfo", "self_soul", luaNavigation_self_soul);
	registerMethod("NaviPlayerInfo", "self_stamina", luaNavigation_self_stamina);
	registerMethod("NaviPlayerInfo", "self_off_training", luaNavigation_self_off_training);
	registerMethod("NaviPlayerInfo", "self_character_id", luaNavigation_self_character_id);
	registerMethod("NaviPlayerInfo", "target_red", luaNavigation_target_red);
	registerMethod("NaviPlayerInfo", "is_follow_mode", luaNavigation_is_follow_mode);
	registerMethod("NaviPlayerInfo", "is_walking", luaNavigation_is_walking);
	registerMethod("NaviPlayerInfo", "satus_water", luaNavigation_satus_water);
	registerMethod("NaviPlayerInfo", "status_holly", luaNavigation_status_holly);
	registerMethod("NaviPlayerInfo", "status_frozen", luaNavigation_status_frozen);
	registerMethod("NaviPlayerInfo", "status_curse", luaNavigation_status_curse);
	registerMethod("NaviPlayerInfo", "status_battle_red", luaNavigation_status_battle_red);
	registerMethod("NaviPlayerInfo", "status_streng_up", luaNavigation_status_streng_up);
	registerMethod("NaviPlayerInfo", "status_pz", luaNavigation_status_pz);
	registerMethod("NaviPlayerInfo", "status_blood", luaNavigation_status_blood);
	registerMethod("NaviPlayerInfo", "status_poison", luaNavigation_status_poison);
	registerMethod("NaviPlayerInfo", "status_fire", luaNavigation_status_fire);
	registerMethod("NaviPlayerInfo", "status_energy", luaNavigation_status_energy);
	registerMethod("NaviPlayerInfo", "status_drunk", luaNavigation_status_drunk);
	registerMethod("NaviPlayerInfo", "status_slow", luaNavigation_status_slow);
	registerMethod("NaviPlayerInfo", "status_utamo", luaNavigation_status_utamo);
	registerMethod("NaviPlayerInfo", "status_haste", luaNavigation_status_haste);
	registerMethod("NaviPlayerInfo", "status_battle", luaNavigation_status_battle);

	registerTable("NaviPlayerInventory");
	registerMethod("NaviPlayerInventory", "body_helmet", luaNavigation_body_helmet);
	registerMethod("NaviPlayerInventory", "body_amulet", luaNavigation_body_amulet);
	registerMethod("NaviPlayerInventory", "body_bag", luaNavigation_body_bag);
	registerMethod("NaviPlayerInventory", "body_armor", luaNavigation_body_armor);
	registerMethod("NaviPlayerInventory", "body_shield", luaNavigation_body_shield);
	registerMethod("NaviPlayerInventory", "body_weapon", luaNavigation_body_weapon);
	registerMethod("NaviPlayerInventory", "body_leg", luaNavigation_body_leg);
	registerMethod("NaviPlayerInventory", "body_boot", luaNavigation_body_boot);
	registerMethod("NaviPlayerInventory", "body_ring", luaNavigation_body_ring);
	registerMethod("NaviPlayerInventory", "body_rope", luaNavigation_body_rope);
	registerMethod("NaviPlayerInventory", "ammunation_count", luaNavigation_ammunation_count);
	registerMethod("NaviPlayerInventory", "weapon_count", luaNavigation_weapon_count);
	registerMethod("NaviPlayerInventory", "maximize", luaNavigation_maximize);
	registerMethod("NaviPlayerInventory", "minimize", luaNavigation_minimize);

	registerTable("NaviPlayerCreatures");
	registerMethod("NaviPlayerCreatures", "creatures_on_screen", luaNavigation_creatures_on_screen);
	registerMethod("NaviPlayerCreatures", "current_creature_name", luaNavigation_current_creature_name);
	registerMethod("NaviPlayerCreatures", "current_creature_coordinate", luaNavigation_current_creature_coordinate);
	registerMethod("NaviPlayerCreatures", "current_creature_is_monster", luaNavigation_current_creature_is_monster);
	registerMethod("NaviPlayerCreatures", "current_creature_is_npc", luaNavigation_current_creature_is_npc);
	registerMethod("NaviPlayerCreatures", "current_creature_is_player", luaNavigation_current_creature_is_player);
	registerMethod("NaviPlayerCreatures", "have_player_on_screen", luaNavigation_have_player_on_screen);
	registerMethod("NaviPlayerCreatures", "have_creature_on_screen", luaNavigation_have_creature_on_screen);
	registerMethod("NaviPlayerCreatures", "is_trapped_by_creatures", luaNavigation_is_trapped_by_creatures);

	registerTable("NaviPlayerContainers");
	registerMethod("NaviPlayerContainers", "count_money_total", luaNavigation_count_money_total);
	registerMethod("NaviPlayerContainers", "count_container_open", luaNavigation_count_container_open);
	registerMethod("NaviPlayerContainers", "container_infos", luaNavigation_container_infos);

	registerTable("NaviPlayerWaypointer");
	registerMethod("NaviPlayerWaypointer", "waypoint_coordinate", luaNavigation_waypoint_coordinate);
	registerMethod("NaviPlayerWaypointer", "waypoint_label", luaNavigation_waypoint_label);
	registerMethod("NaviPlayerWaypointer", "waypoint_path", luaNavigation_waypoint_path);
	registerMethod("NaviPlayerWaypointer", "waypoint_action", luaNavigation_waypoint_action);

	registerTable("NavigationManager");
	registerMethod("NavigationManager", "close", luaNavigation_close);
	registerMethod("NavigationManager", "get_simples_string", luaNavigation_get_simples_string);
	registerMethod("NavigationManager", "connect_to_server", luaNavigation_connect_to_server);
	registerMethod("NavigationManager", "send_message", luaNavigation_send_message);
	registerMethod("NavigationManager", "enable_send_player_info", luaNavigation_send_player_info);
	registerMethod("NavigationManager", "enable_send_player_inventory", luaNavigation_send_player_inventory);
	registerMethod("NavigationManager", "enable_send_player_containers", luaNavigation_send_player_containers);
	registerMethod("NavigationManager", "enable_send_player_creatures", luaNavigation_send_player_creatures);
	registerMethod("NavigationManager", "delay_work", luaNavigation_delay_work);
	registerMethod("NavigationManager", "enable_send_player_waypointer", luaNavigation_send_player_waypointer);

	registerTable("Creatures");
	registerMethod("Creatures", "get_creatures_at_coordinate", luaCreaturesget_creatures_at_coordinate);
	registerMethod("Creatures", "get_creatures_around", luaCreaturesget_creatures_around);
	registerMethod("Creatures", "get_creatures_around_count", luaCreaturesget_creatures_around);
	registerMethod("Creatures", "get_creatures_around_table", luaCreaturesget_creatures_in_radius);
	
	registerMethod("Creatures", "get_creatures_near_coordinate", luaCreaturesget_creatures_near_coordinate);
	registerMethod("Creatures", "find_creature_on_screen", luaCreaturesfind_creature_on_screen);
	registerMethod("Creatures", "find_creature_by_id", luaCreaturesfind_creature_by_id);
	registerMethod("Creatures", "get_creatures_on_screen", luaCreaturesget_creatures_on_screen);
	registerMethod("Creatures", "get_target", luaCreaturesget_target);

	registerMethod("Creatures", "get_creatures_on_area", luaCreaturesget_creatures_on_area);
	registerMethod("Creatures", "get_creatures_on_area_effect", luaCreaturesget_creatures_on_area_effect);
	registerMethod("Creatures", "is_traped_by_monster", luaCreaturesis_traped_by_monster);
	registerMethod("Creatures", "have_player_on_screen", luaCreatureshave_player_on_screen);
	registerMethod("Creatures", "get_self_creature", luaCreaturesget_self_creature);
	registerMethod("Creatures", "have_creature_on_screen", luaCreatureshave_creature_on_screen);
	registerMethod("Creatures", "get_monster_on_screen_count", luaget_creatures_on_screen_count);

	
	registerClass("Tile", std::string());
	registerMethod("Tile", "have_furniture_at_top", luaTilehave_furniture_at_top);
	registerMethod("Tile", "get_top_item_id", luaTileget_top_item_id);
	registerMethod("Tile", "get_top_second_item_id", luaTileget_top_second_item_id);
	registerMethod("Tile", "have_element_field_at_top", luaTilehave_element_field_at_top);
	registerMethod("Tile", "have_element_field", luaTilehave_element_field);
	registerMethod("Tile", "get_creatures", luaTileget_creatures);
	registerMethod("Tile", "get_players", luaTileget_players);
	registerMethod("Tile", "contains_player", luaTilecontains_player);
	registerMethod("Tile", "contains_furniture", luaTilecontains_furniture);
	registerMethod("Tile", "contains_block_path_id", luaTilecontains_block_path_id);
	registerMethod("Tile", "contains_free_path_id", luaTilecontains_free_path_id);
	registerMethod("Tile", "contains_id", luaTilecontains_id);
	registerMethod("Tile", "contains_summon", luaTilecontains_summon);
	registerMethod("Tile", "contains_depot", luaTilecontains_depot);
	registerMethod("Tile", "contains_creature", luaTilecontains_creature);
	registerMethod("Tile", "has_property", luaTilehas_property);
	registerMethod("Tile", "get_thing_count", luaTileget_thing_count);
	registerMethod("Tile", "get_destination", luaTileget_destination);
	registerMethod("Tile", "get_required_pos", luaTileget_required_pos);
	registerMethod("Tile", "get_thing_count", luaTileget_thing_count);
	registerMethod("Tile", "get_top_item_minus_index", luaTileget_top_item_minus_index);
	registerMethod("Tile", "contains_item_attr", luaTilecontains_item_attr);
	registerMethod("Tile", "can_use_to_down", luaTilecan_use_to_down);
	registerMethod("Tile", "can_use_to_up", luaTilecan_use_to_up);
	registerMethod("Tile", "is_step", luaTileis_step);
	registerMethod("Tile", "is_hole", luaTileis_hole);
	registerMethod("Tile", "must_open_with_browse", luaTilemust_open_with_browse);
	registerMethod("Tile", "contains_hole_or_step", luaTilecontains_hole_or_step);
	registerMethod("Tile", "contains_npc", luaTilecontains_npc);
	registerMethod("Tile", "contains_monster", luaTilecontains_monster);
	registerMethod("Tile", "override_unwalkable", luaTileoverride_unwalkable);
	registerMethod("Tile", "override_walkable", luaTileoverride_walkable);
	registerMethod("Tile", "contains_brokable", luaTilecontains_brokable);
	registerMethod("Tile", "is_teleport", luaTileis_teleport);
	registerMethod("Tile", "avoid_when_targeting", luaTileavoid_when_targeting);


	registerTable("Map");
	registerMethod("Map", "get_walkability", luaMapget_walkability);
	registerMethod("Map", "get_walkability_map", luaMapget_walkability_map);
	registerMethod("Map", "add_block_id", luaMapadd_block_id);
	registerMethod("Map", "add_release_id", luaMapadd_release_id);
	registerMethod("Map", "remove_block_id", luaMapremove_block_id);
	registerMethod("Map", "remove_release_id", luaMapremove_release_id);
	registerMethod("Map", "get_nearest_depot_tile", luaMapget_nearest_depot_tile);
	registerMethod("Map", "get_raw_tile_at", luaMapget_raw_tile_at);
	registerMethod("Map", "get_tile_nearest_coordinate", luaMapget_tile_nearest_coordinate);
	registerMethod("Map", "get_tiles_with_id", luaMapget_tiles_with_id);
	registerMethod("Map", "get_tiles_with_top_id", luaMapget_tiles_with_top_id);
	registerMethod("Map", "get_tiles_with_id_near_to", luaMapget_tiles_with_id_near_to);
	registerMethod("Map", "get_furniture_tiles", luaMapget_furniture_tiles);
	registerMethod("Map", "get_creature_tiles", luaMapget_creature_tiles);
	registerMethod("Map", "get_player_tiles", luaMapget_player_tiles);
	registerMethod("Map", "contains_furniture_at", luaMapcontains_furniture_at);
	registerMethod("Map", "find_coordinate_with_top_id", luaMapfind_coordinate_with_top_id);
	registerMethod("Map", "find_coordinate_contains_id", luaMapfind_coordinate_contains_id);
	registerMethod("Map", "coordinate_is_traped", luaMapcoordinate_is_traped);
	registerMethod("Map", "is_traped_on_screen", luaMapis_traped_on_screen);
	registerMethod("Map", "check_sight_line", luaMapcheck_sight_line);
	registerMethod("Map", "is_coordinate_onscreen", luaMapis_cordinate_onscreen);
	registerMethod("Map", "wait_refresh", luaMapis_wait_refresh);
	
	registerMethod("Map", "get_max_visible_level_up", luaMapget_max_visible_level_up);
	registerMethod("Map", "get_max_visible_level_down", luaMapget_max_visible_level_down);

	registerTable("VipList");
	registerMethod("VipList", "get_vip_by_player_name", luaVipListget_vip_by_player_name);
	registerMethod("VipList", "get_vip_players", luaVipListget_vip_players);

	registerClass("WaypointPath", std::string());
	registerMethod("WaypointPath", "get_name", luaMetatableWaypointPathgetName);
	registerMethod("WaypointPath", "get_waypoint_info_by_label", luaMetatableWaypointPathgetWaypointInfoIdByLabel);
	registerMethod("WaypointPath", "get_waypoint_info_table", luaMetatableWaypointPathgetwaypointInfoList);

	registerTable("Util");
	registerMethod("Util", "get_axis_min_dist", Utilget_axis_min_dist);

	registerClass("SpecialAreas", std::string());
	registerMethod("SpecialAreas", "get_root_area", SpecialAreaManagerget_root_area);
	registerMethod("SpecialAreas", "get_axis_min_dist", Utilget_axis_min_dist);

	registerClass("SABaseElement", std::string());
	registerMethod("SABaseElement", "get_activation_trigger", SABaseElementget_activation_trigger);
	registerMethod("SABaseElement", "set_activation_trigger", SABaseElementset_activation_trigger);
	registerMethod("SABaseElement", "set_state", SABaseElementset_state);
	registerMethod("SABaseElement", "get_state", SABaseElementget_state);
	registerMethod("SABaseElement", "remove_element", SABaseElementremove_element);
	registerMethod("SABaseElement", "get_childrens_by_type", SABaseElementget_childrens_by_type);
	registerMethod("SABaseElement", "get_childrens_by_name", SABaseElementget_childrens_by_name);
	registerMethod("SABaseElement", "get_childrens_by_names", SABaseElementget_childrens_by_names);
	registerMethod("SABaseElement", "get_child_by_name", SABaseElementget_child_by_name);
	registerMethod("SABaseElement", "get_type", SABaseElementget_type);
	registerMethod("SABaseElement", "get_owner", SABaseElementget_owner);
	registerMethod("SABaseElement", "set_name", SABaseElementset_name);
	registerMethod("SABaseElement", "get_name", SABaseElementget_name);
	registerMethod("SABaseElement", "set_block_walk", SABaseElementset_block_walk);
	registerMethod("SABaseElement", "get_block_walk", SABaseElementget_block_walk);
	registerMethod("SABaseElement", "get_walk_dificult", SABaseElementget_walk_dificult);
	registerMethod("SABaseElement", "get_avoidance", SABaseElementget_avoidance);
	registerMethod("SABaseElement", "get_childrens", SABaseElementget_childrens);
	registerMethod("SABaseElement", "set_width", SABaseElementset_width);
	registerMethod("SABaseElement", "get_width", SABaseElementget_width);
	registerMethod("SABaseElement", "get_z", SABaseElementget_z);
	registerMethod("SABaseElement", "get_line", SABaseElementget_line);
	registerMethod("SABaseElement", "get_point", SABaseElementget_point);
	registerMethod("SABaseElement", "get_point", SABaseElementget_point);
	registerMethod("SABaseElement", "get_walkability_rect", SABaseElementget_walkability_rect);
	registerMethod("SABaseElement", "get_avoidance_rect", SABaseElementget_avoidance_rect);
	registerMethod("SABaseElement", "get_center", SABaseElementget_center);
	registerMethod("SABaseElement", "get_rect", SABaseElementget_rect); 

#pragma region BIND_WAYPOINT_MANAGER
	
	registerTable("WaypointManager");
	registerMethod("WaypointManager", "unpause", luaWaypointManagerunpause_to_time);
	registerMethod("WaypointManager", "pause", luaWaypointManagerpause_to_time);
	registerMethod("WaypointManager", "get_enabled_state", luaWaypointManagerenabled);
	registerMethod("WaypointManager", "set_enabled_state", luaWaypointManagerenabled);
	registerMethod("WaypointManager", "get_last_waypoint_info_id", luaWaypointManagerlastWaypointInfoId);
	registerMethod("WaypointManager", "get_last_waypoint_path_id", luaWaypointManagerlastWaypointPathId);
	registerMethod("WaypointManager", "get_current_path", luaWaypointManagergetCurrentWaypointPath);
	registerMethod("WaypointManager", "get_current_waypoint_path", luaWaypointManagercurrentWaypointPathId);
	registerMethod("WaypointManager", "get_current_waypoint_info_id", luaWaypointManagergetcurrentWaypointInfoId);
	registerMethod("WaypointManager", "set_current_waypoint_path_by_label", luaWaypointManagersetCurrentWaypointPathByLabel);
	registerMethod("WaypointManager", "set_current_waypoint_path", luaWaypointManagersetCurrentWaypointPath);

	registerMethod("WaypointManager", "get_actions_table", luaWaypointManagergetActionInfoList);
	registerMethod("WaypointManager", "get_last_waypoint_path", luaWaypointManagergetLastWaypointPath);
	registerMethod("WaypointManager", "get_waypoint_path_by_label", luaWaypointManagergetWaypointPath);
	registerMethod("WaypointManager", "remove_waypoint_path_by_label", luaWaypointManagerremoveWaypointPath);
	registerMethod("WaypointManager", "get_waypoint_path_table", luaWaypointManagergetwaypointPathList);
	registerMethod("WaypointManager", "advance_current_waypoint", luaWaypointManageradvanceCurrentWaypointInfo);
	registerMethod("WaypointManager", "get_current_waypoint_info", luaWaypointManagergetCurrentWaypointInfo);
	registerMethod("WaypointManager", "get_last_waypoint_info", luaWaypointManagergetLastWaypointInfo);
	registerMethod("WaypointManager", "get_before_waypoint_info", luaWaypointManagergetBeforeWaypointInfo);
	registerMethod("WaypointManager", "get_next_waypoint_info", luaWaypointManagergetNextWapointInfo);
	registerMethod("WaypointManager", "set_current_waypoint_by_label", luaWaypointManagersetCurrentWaypointInfo);
	registerMethod("WaypointManager", "is_location", Waypointeris_location);


#pragma endregion

	registerTable("Notify");
	registerMethod("Notify", "message_box", luaMessageBox);
	registerMethod("Notify", "add_error_log", luaAddErrorLog);
	registerMethod("Notify", "print_console", luaPrintConsole);
	registerMethod("Notify", "notify_baloon", luaNotificationBaloon);

	registerTable("Login");
	registerMethod("Login", "is_logged", luais_logged);
	registerMethod("Login", "is_logged", luais_logged);
	registerMethod("Login", "try_login", luatry_login);

	registerTable("Info");
	registerMethod("Info", "player_kill_on_screen", luaInfoplayer_kill_on_screen);
	registerMethod("Info", "tibia_messages_vector", luaInfotibiamessages_vector);
	registerMethod("Info", "monster_killed_by_name", luamonster_killed_by_name);
	registerMethod("Info", "monster_killed", luaMonsterKilledInfo);
	registerMethod("Info", "monster_killer_count", luamonster_killer_count);
	registerMethod("Info", "items_used", luaLootInfo_get_vector_items_used);
	registerMethod("Info", "items_looted", luaLootInfo_get_vector_items);
	registerMethod("Info", "last_balance", lua_last_balance);
	registerMethod("Info", "get_current_time_total_in_day", lua_get_current_time_total_in_day);
	registerMethod("Info", "item_used_count", lua_item_used_count);
	registerMethod("Info", "exp_hour", lua_exp_hour);
	registerMethod("Info", "exp_to_next_lvl", lua_exp_to_next_lvl);
	registerMethod("Info", "time_to_next_lvl", lua_time_to_next_lvl);
	registerMethod("Info", "get_ping", lua_get_ping);
	registerMethod("Info", "money_spent", lua_get_moneyspent);
	registerMethod("Info", "get_last_average_ping", lua_get_last_medium_ping);
	registerMethod("Info", "exp_gained", lua_expgained);
	registerMethod("Info", "profit_hour", lua_profit_hour);

	registerTable("LuaLibrary");
	registerMethod("LuaLibrary", "register_table", luaKeywordManagerregisterTable);
	registerMethod("LuaLibrary", "register_metatable", luaKeywordManagerregisterMetatable);
	registerMethod("LuaLibrary", "register_class", luaKeywordManagerregisterClass);
	registerMethod("LuaLibrary", "register_enum_table", luaKeywordManagerregisterEnumTable);
	registerMethod("LuaLibrary", "register_method", luaKeywordManagerregisterTableMethod);
	registerMethod("LuaLibrary", "register_function", luaKeywordManagerregisterFunction);
	registerMethod("LuaLibrary", "set_current_parsing_lib", luaKeywordManagersetCurrentParsingLib);
	registerMethod("LuaLibrary", "add_table_variable_description", luaKeywordManageraddTableVariableDescription);
	registerMethod("LuaLibrary", "add_table_variable", luaKeywordManagerregisterTableVariable);
	registerMethod("LuaLibrary", "add_variable", luaKeywordManagerregisterVariable);
	registerMethod("LuaLibrary", "add_variable_description", luaKeywordManageraddVariableDescription);
	registerMethod("LuaLibrary", "add_table_description", luaKeywordManageraddTableDescription);
	registerMethod("LuaLibrary", "add_function_description", luaKeywordManageraddFunctionDescription);
	registerMethod("LuaLibrary", "add_function_suggestion", luaKeywordManageraddFunctionSuggestion);
	registerMethod("LuaLibrary", "add_method_description", luaKeywordManageraddMethodDescription);
	registerMethod("LuaLibrary", "add_method_suggestion", luaKeywordManageraddMethodSuggestion);

	registerTable("BotManager");
	registerMethod("BotManager", "get_health_spells_state", luaNeutralManagerget_health_spells_state);
	registerMethod("BotManager", "get_attack_spells_state", luaNeutralManagerget_attack_spells_state);
	registerMethod("BotManager", "set_health_spells_state", luaNeutralManagerset_health_spells_state);
	registerMethod("BotManager", "set_attack_spells_state", luaNeutralManagerset_attack_spells_state);
	registerMethod("BotManager", "screenshot", luaNeutralManagerscreenshot);
	registerMethod("BotManager", "load_script", luaNeutralManagerloadscript);
	registerMethod("BotManager", "get_alerts_state", luaNeutralManagerget_alerts_state);
	registerMethod("BotManager", "get_bot_state", luaNeutralManagerget_bot_state);
	registerMethod("BotManager", "get_hunter_state", luaNeutralManagerget_hunter_state);
	registerMethod("BotManager", "get_looter_state", luaNeutralManagerget_looter_state);
	registerMethod("BotManager", "get_lua_state", luaNeutralManagerget_lua_state);
	registerMethod("BotManager", "get_pause_state", luaNeutralManagerget_pause_state);
	registerMethod("BotManager", "get_spellcaster_state", luaNeutralManagerget_spellcaster_state);
	registerMethod("BotManager", "get_info_time_bot_running", luaNeutralManagerget_info_time_bot_running);
	registerMethod("BotManager", "get_waypointer_state", luaNeutralManagerget_waypointer_state);
	registerMethod("BotManager", "hide_bot_interface", luaNeutralManagerhide_bot_interface);
	registerMethod("BotManager", "minimize_bot_interface", luaNeutralManagerminimize_bot_interface);
	registerMethod("BotManager", "restore_bot_interface", luaNeutralManagerrestore_bot_interface);
	registerMethod("BotManager", "request_close_bot_process", luaNeutralManagerrequest_close_bot_process);
	registerMethod("BotManager", "pause", luaNeutralManagerpause); 
	registerMethod("BotManager", "kill_tibia_process", luaNeutralManagerkill_process);
	registerMethod("BotManager", "kill_bot_process", luaNeutralManagerkill_bot_process);
	registerMethod("BotManager", "set_alerts_state", luaNeutralManagerset_alerts_state);
	registerMethod("BotManager", "set_bot_state", luaNeutralManagerset_bot_state);
	registerMethod("BotManager", "set_hunter_state", luaNeutralManagerset_hunter_state);
	registerMethod("BotManager", "set_looter_state", luaNeutralManagerset_looter_state);
	registerMethod("BotManager", "set_lua_state", luaNeutralManagerset_lua_state);
	registerMethod("BotManager", "set_pause_state", luaNeutralManagerset_pause_state);
	registerMethod("BotManager", "set_spellcaster_state", luaNeutralManagerset_spellcaster_state);
	registerMethod("BotManager", "set_waypointer_state", luaNeutralManagerset_waypointer_state);
	registerMethod("BotManager", "kill_tibia_process", luaBotManagerKillTibiaClient);

	registerTable("HunterManager");
	registerMethod("HunterManager", "unpause", luaHunterManagerunpause_to_time);
	registerMethod("HunterManager", "pause", luaHunterManagerpause_to_time);
	registerMethod("HunterManager", "need_operate", luaHunterManagerneed_operation);
	registerMethod("HunterManager", "disable_group", luaHunterManagerdisable_group);
	registerMethod("HunterManager", "enable_group", luaHunterManagerenable_group);
	registerMethod("HunterManager", "has_temporary", luaHunterActionshas_temporary);
	registerMethod("HunterManager", "add_temporary_target", luaHunterActionsadd_temporary_target);
	registerMethod("HunterManager", "get_temporary_target_id", luaHunterActionsget_temporary_target_id);
	registerMethod("HunterManager", "clear_temporary_targets", luaHunterActionsclear_temporary_targets);
	registerMethod("HunterManager", "pulse", luaHunterCorepulse);
	registerMethod("HunterManager", "wait_pulse", luaHunterCorewait_pulse);
	registerMethod("HunterManager", "wait_pulse_out", luaHunterCorewait_pulse_out);
	registerMethod("HunterManager", "wait_kill_all_complete", luaHunterCorewait_kill_all_complete);
	registerMethod("HunterManager", "is_pulsed", luaHunterCoreis_pulsed);
	registerMethod("HunterManager", "unset_pulse", luaHunterCoreunset_pulse);

	registerTable("SpellManager");
	registerMethod("SpellManager", "disable_spell_by_id", luaSpellManagerdisable_spell_by_id);
	registerMethod("SpellManager", "enable_spell_by_id", luaSpellManagerenable_spell_by_id);

	registerTable("LooterManager");
	registerMethod("LooterManager", "unpause", luaLooterManagerunpause_to_time);
	registerMethod("LooterManager", "pause", luaLooterManagerpause_to_time);
	registerMethod("LooterManager", "need_operate", luaLooterManagerneed_operate);
	registerMethod("LooterManager", "disable_loot_group", luaLooterManagerdisable_loot_group);
	registerMethod("LooterManager", "enable_loot_group", luaLooterManagerenable_loot_group);
	registerMethod("LooterManager", "set_loots_bag_id", luaLooterManagerset_loots_bag_id);
	registerMethod("LooterManager", "get_loots_bag_id", luaLooterManagerget_loots_bag_id);
	registerMethod("LooterManager", "enable_loot_group", luaLooterManagerenable_loot_group);
	registerMethod("LooterManager", "set_group_state", luaLooterManagerset_group_state);
	registerMethod("LooterManager", "get_loot_group_state", luaLooterManagerget_loot_group_state);
	registerMethod("LooterManager", "set_loots_bag_name", luaLooterManagerset_loots_bag_name);
	registerMethod("LooterManager", "get_loots_bag_item_id", luaLooterManagerget_loots_bag_item_id);
	registerMethod("LooterManager", "get_item_destination_id", luaLooterManagerget_item_destination_id);
	registerMethod("LooterManager", "get_current_filter_type", luaLooterManagerget_current_filter_type);
	registerMethod("LooterManager", "get_current_sequence_type", luaLooterManagerget_current_sequence_type);
	registerMethod("LooterManager", "set_current_filter_type", luaLooterManagerset_current_filter_type);
	registerMethod("LooterManager", "set_current_sequence_type", luaLooterManagerset_current_sequence_type);
	registerMethod("LooterManager", "check_cap", luaLootercheck_cap);
	registerMethod("LooterManager", "get_stop_loot_on_minimun_cap", luaLooterget_stop_loot_on_minimun_cap);
	registerMethod("LooterManager", "wait_loot_complete", luaLooterManagerwait_loot_complete);
	registerMethod("LooterManager", "wait_pulse_out", luaLooterManagerwait_pulse_out);
	registerMethod("LooterManager", "wait_pulse", luaLooterManagerwait_pulse);
	registerMethod("LooterManager", "pulse", luaLooterManagerpulse);
	registerMethod("LooterManager", "is_pulsed", luaLooterCoreis_pulsed);
	registerMethod("LooterManager", "unset_pulse", luaLooterCoreunset_pulse);


	registerTable("LuaManager");
	registerMethod("LuaManager", "get_lua_script_code", luaLuaManagergetLuaCode);
	registerMethod("LuaManager", "set_lua_script_delay", luaLuaManagersetLuaScriptDelay);
	registerMethod("LuaManager", "set_lua_script_code", luaLuaManagersetLuaScriptCode);
	registerMethod("LuaManager", "get_lua_script_delay", luaLuaManagergetLuaScriptDelay);
	registerMethod("LuaManager", "set_lua_script_active", luaset_lua_script_active);
	registerMethod("LuaManager", "get_lua_script_active", luaget_lua_script_active);

	registerTable("HudManager");
	registerMethod("HudManager", "add_grad_colors", HudManagerset_colorBrush);
	registerMethod("HudManager", "measure_string_width", HudManagermeasurestringWidth);
	registerMethod("HudManager", "measure_string_height", HudManagermeasurestringHeight);
	registerMethod("HudManager", "color", HudManagercolor);
	registerMethod("HudManager", "draw_circle", HudManagerdraw_circle);
	registerMethod("HudManager", "draw_item", HudManagerdrawitem);
	registerMethod("HudManager", "draw_image", HudManagerdraw_image);
	registerMethod("HudManager", "draw_line", HudManagerdraw_line);
	registerMethod("HudManager", "draw_rect", HudManagerdraw_rect);
	registerMethod("HudManager", "draw_round_rect", HudManagerdraw_roundrect);
	registerMethod("HudManager", "draw_text", HudManagerdraw_text);
	registerMethod("HudManager", "set_border_color", HudManagerset_colorBorder);
	registerMethod("HudManager", "set_font_border", HudManagerset_fontBorder);
	registerMethod("HudManager", "set_font_color", HudManagerset_fontColor);
	registerMethod("HudManager", "set_border_size", HudManagersetbordersize);
	registerMethod("HudManager", "set_font_name", HudManagerset_font);
	registerMethod("HudManager", "set_font_size", HudManagerset_fontsize);
	registerMethod("HudManager", "set_font_style", HudManagersetfontstyle);
	registerMethod("HudManager", "get_client_width", HudManagerget_size_client_x);
	registerMethod("HudManager", "get_client_height", HudManagerget_size_client_y);
	registerMethod("HudManager", "get_game_height", HudManagerget_size_game_y);
	registerMethod("HudManager", "get_game_width", HudManagerget_size_game_x);
	registerMethod("HudManager", "set_position", HudManagersetposition);
	registerMethod("HudManager", "get_pos_game_y", HudManagerget_pos_game_y);
	registerMethod("HudManager", "get_pos_game_x", HudManagerget_pos_game_x);
	registerMethod("HudManager", "filter_input ", HudManagerfilterinput);

	registerTable("SetupManager"); 
	registerMethod("SetupManager", "set_text_by_control", SetupManagerset_text_by_control);
	registerMethod("SetupManager", "get_text_by_control", SetupManagerget_text_by_control);
	registerMethod("SetupManager", "get_list_by_control", SetupManagerget_list_by_control);
	registerMethod("SetupManager", "get_state_by_control", SetupManagerget_state_by_control);
	registerMethod("SetupManager", "get_value_by_control", SetupManagerget_value_by_control);
	
	registerTable("Test");
	registerMethod("Test", "test", luaTestMethod);
	
}