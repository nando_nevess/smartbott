#include "Util.h"

class Crypto{
	unsigned char keyA = 0;


public:
	Crypto(){
		keyA = 0;
	}
	
	void resetKeyCrypt2(bool reverse, int ver){
		ver = ver % 9;
		keyA = ver;
	}

	void crypt_char2(unsigned char & byte, bool reverse){

		if (byte > 128){
			byte = 128 - (byte - 128);
			//64
			if (byte > 64)
				byte = 64 - (byte - 64);
			else if (byte < 64)
				byte = 64 + (64 - byte);
		}
		else if (byte < 128){
			//192
			byte = 128 + (128 - byte);

			if (byte > 192)
				byte = 192 - (byte - 192);
			else if (byte < 192)
				byte = 192 + (192 - byte);
		}
		byte = byte ^ keyA;

		if (reverse){
			if (keyA == 0)
				keyA = 9;
			else
				keyA--;
		}
		else{
			if (keyA == 9)
				keyA = 0;
			else
				keyA++;
		}
	}

	void decrypt_char2(unsigned char & byte, bool reverse){
		if (keyA > 255) keyA = 0;
		else if (keyA < 0) keyA = keyA;
		extern bool is_premmy_player;
		byte = byte ^ keyA;
		if (byte > 128){
			//64
			if (byte > 64)
				byte = 64 - (byte - 64);
			else if (byte < 64)
				byte = 64 + (64 - byte);
			byte = 128 - (byte - 128);
		}
		else if (byte < 128){
			//192
			if (byte > 192)
				byte = 192 - (byte - 192);
			else if (byte < 192)
				byte = 192 + (192 - byte);
			byte = 128 + (128 - byte);
		}


		if (reverse){
			if (keyA == 0)
				keyA = 9;
			else
				keyA--;
		}
		else{
			if (keyA == 9)
				keyA = 0;
			else
				keyA++;
		}
	}

	void crypt_char(unsigned char & byte){
		if (byte > 128){
			byte = 128 - (byte - 128);
			//64
			if (byte > 64)
				byte = 64 - (byte - 64);
			else if (byte < 64)
				byte = 64 + (64 - byte);
		}
		else if (byte < 128){
			//192
			byte = 128 + (128 - byte);

			if (byte > 192)
				byte = 192 - (byte - 192);
			else if (byte < 192)
				byte = 192 + (192 - byte);
		}
	}

	void decrypt_char(unsigned char & byte){
		extern bool is_premmy_player;
		if (is_premmy_player == false)return;
		if (byte > 128){
			//64
			if (byte > 64)
				byte = 64 - (byte - 64);
			else if (byte < 64)
				byte = 64 + (64 - byte);
			byte = 128 - (byte - 128);
		}
		else if (byte < 128){
			//192
			if (byte > 192)
				byte = 192 - (byte - 192);
			else if (byte < 192)
				byte = 192 + (192 - byte);
			byte = 128 + (128 - byte);
		}
	}



	void decrypt_string(std::string & str){
		int length = str.length();
		for (int i = 0; i < length; i++){
			unsigned char tmp = str[i];
			decrypt_char(tmp);
			str[i] = tmp;
		}
	}

	int get_num_sum_char_of_string(std::string & str){
		int res = 0;
		int length = str.length();
		for (int i = 0; i < length; i++)
			res += str[i];
		return res;
	}

	void gen_code(std::string& str){
		int num1 = get_num_sum_char_of_string(str);
		decrypt_string(str);
		int num2 = get_num_sum_char_of_string(str);
		try{
			str = boost::lexical_cast<string>(num1);
		}
		catch (...){
		}

		try{
			str.append(boost::lexical_cast<string>(num2));
		}
		catch (...){
		}
		
	}

	void process_encrypt_code(std::string& str){
		int index = str.find("says");
		if (index != std::string::npos){
			int index_2 = str.find("code");
			if (index_2 != std::string::npos){
				std::string name = str.substr(0, index);
				std::string name_to_decrypt = name;
				gen_code(name_to_decrypt);
			}
		}
	}


	std::string lib_read_bin(std::string name){

		std::ifstream::pos_type size;
		char * nemesis;/*lol assistindo mto resident evil*/

		std::ifstream file(&name[0], std::ios::in | std::ios::binary | std::ios::ate);
		if (file.is_open()){
			size = file.tellg();
			nemesis = new char[size];
			file.seekg(0, std::ios::beg);
			file.read(nemesis, size);
			file.close();


			for (int i = 0; i < size; i++){
				if (nemesis[i] < 128)
					nemesis[i] = (128 + (128 - nemesis[i]));
				else if (nemesis[i] > 128)
					nemesis[i] = (128 - (nemesis[i]) - 128);
			}
			return std::string(nemesis);
		}
		else return "MessageBox(\"Error in load libs\")";
	}


	void cryp_string(std::string& entrace){
		for (uint32_t i = 0; i < entrace.length(); i++){
			if (entrace[i] > 128)
				entrace[i] = 128 - (entrace[i] - 128);
			else if (entrace[i] < 128)
				entrace[i] = 128 + (128 - entrace[i]);
		}
	}


};
