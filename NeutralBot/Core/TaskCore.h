#include <boost\asio.hpp>


class BaseTask{
	NEUTRAL_TASK_ID id;
	boost::asio::io_service& _service;
public:
	BaseTask(boost::asio::io_service& service) :_service(service){
	}

	virtual void start(){
		throw new std::string("exception BaseTask method start called with task id:" + std::to_string((long long)id);
	}
	virtual bool need_stop(){
		throw new std::string("exception BaseTask method need_stop called with task id:" + std::to_string((long long)id);
	}
};

class AutoMountTask: public BaseTask{
};

class AntiIdleTask : public BaseTask{
};

class AutoFishTask : public BaseTask{
};

class RefillAmmunationTask : public BaseTask{
};

class MultiClientTask : public BaseTask{
};

class TreinerTask : public BaseTask{
};


class HelperCore{
	boost::asio::io_service thread_controller;

public:
	HelperCore(){

	}
	void initialize_async_timers(){
		async_refill_weapon_ammun_check();
	}

	void run(){
		initialize_async_timers();
			thread_controller.run();
	}

	static HelperCore* get(){
		static HelperCore* mHelperCore = nullptr;
		if (!mHelperCore)
			mHelperCore = new HelperCore;
		return mHelperCore;
	}

	void async_refill_weapon_ammun_check();
};

