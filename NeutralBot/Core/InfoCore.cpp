#include "InfoCore.h"
#include "Actions.h"
#include <ShellAPI.h>

uint64_t WarningMessage::get_last_refresh_timeout(){
	mtx_access.lock();
	uint32_t retval = last_update.elapsed_milliseconds();
	mtx_access.unlock();
	return retval;
}

void WarningMessage::refresh_timeout(){
	mtx_access.lock();
	last_update.reset();
	count++;
	mtx_access.unlock();
}

std::string WarningMessage::get_text(){
	std::string retval;
	mtx_access.lock();
	retval = text;
	mtx_access.unlock();
	return retval;
}

void WarningMessage::set_text(std::string new_text){
	mtx_access.lock();
	text = new_text;
	mtx_access.unlock();
}

warning_message_type WarningMessage::get_message_type_from_text(std::string& text){
	if (string_util::string_compare("You are not the owner.", text, true)){
		return warning_message_type::warning_message_you_are_not_the_owner;
	}
	else if (string_util::string_compare("Sorry, not possible.", text, true)){
		return warning_message_type::warning_message_sorry_not_possible;
	}
	else if (string_util::string_compare("First go Upstairs.", text, true)){
		return warning_message_type::warning_message_first_go_upstairs;
	}
	else if (string_util::string_compare("You cannot move this object.", text, true)){
		return warning_message_type::warning_message_you_cannot_move_this_object;
	}
	else if (string_util::string_compare("You cannot use this object.", text, true)){
		return warning_message_type::warning_message_you_cannot_use;
	}
	else if (string_util::string_compare("There is no way.", text, true)){
		return warning_message_type::warning_message_there_is_no_way;
	}
	else if (string_util::string_compare("There is not enough room.", text, true)){
		return warning_message_type::warning_message_there_is_not_enough_room;
	}
	else if (string_util::string_compare("First go downstairs.", text, true)){
		return warning_message_type::warning_message_you_may_first_go_downstairs;
	}
	else if (string_util::string_compare("You may not attack this person.", text, true)){
		return warning_message_type::warning_message_you_may_no_attack_this_person;
	}
	else if (string_util::string_compare("This object is too heavy for you to carry.", text, true)){
		return warning_message_type::warning_message_is_too_heavy_to_carry;
	}
	return warning_message_type::warning_message_total_type;
}


std::map<uint32_t, std::pair<uint32_t, uint32_t>> InfoCore::get_itens_used_map(){
	lock_guard _m_lock(mtx_access);
	return itensusedmap;
}

std::map <std::string, uint32_t> InfoCore::get_monster_killed_map(){
	lock_guard _m_lock(mtx_access);
	return monsterkilledmap;
}

InfoCore::InfoCore(){
	mtx_access.lock();
	time.reset(); 
	Messages::get()->add_server_log_callbacks(std::bind(&InfoCore::refresh_item_buy_and_sell/*function q executa qnd chega message nova*/, this, std::placeholders::_1));
	Messages::get()->add_server_log_callbacks(std::bind(&InfoCore::refresh_items_used/*function q executa qnd chega message nova*/, this, std::placeholders::_1));
	Messages::get()->add_tibia_message_callbacks(std::bind(&InfoCore::refresh_last_balance_messages /*function q executa qnd chega message nova*/, this, std::placeholders::_1));
	Messages::get()->add_server_log_callbacks(std::bind(&InfoCore::refresh_player_attacking_you/*function q executa qnd chega message nova*/, this, std::placeholders::_1));
	Messages::get()->add_server_log_callbacks(std::bind(&InfoCore::refresh_monster_killed/*function q executa qnd chega message nova*/, this, std::placeholders::_1));
	Messages::get()->add_action_message_callbacks(std::bind(&InfoCore::parse_action_message/*function q executa qnd chega message nova*/, this, std::placeholders::_1));
	Messages::get()->add_server_log_callbacks(std::bind(&InfoCore::parse_player_attack_string /*function q executa qnd chega message nova*/, this, std::placeholders::_1));

	int total_types = (int)warning_message_type::warning_message_total_type + 1;
	for (int i = 0; i < total_types; i++){
		warnings_messages_information.resize(total_types);
	}
	mtx_access.unlock();
}

void InfoCore::parse_action_message(std::string& message_in){
	warning_message_type type = WarningMessage::get_message_type_from_text(message_in);
	if (type == warning_message_type::warning_message_total_type)
		return;
	
	lock_guard _m_lock(mtx_access);
	warnings_messages_information[type].get_last_refresh_timeout();
	warnings_messages_information[type].refresh_timeout();

}

WarningMessage* InfoCore::get_warning_message_info(warning_message_type type){
	lock_guard _m_lock(mtx_access);
	return &warnings_messages_information[type];
}

uint32_t InfoCore::get_item_used_count(uint32_t itemname){
	lock_guard _m_lock(mtx_access);
	auto it = itensusedmap.find(itemname);
	if (it == itensusedmap.end()){
		return itensusedmap[itemname].first;
	}
	return 0;
}

uint32_t InfoCore::get_last_balance(){
	return last_balance;
}

void InfoCore::run_att_info(){
	while (true){
		Sleep(1* (60 * 1000));
		lock_guard _m_lock(mtx_access);
		int expHour = exp_hour();
		int profitHour = profit_hour();
		
		if (vector_profit_hour.size() >= 200)
			vector_profit_hour.erase(vector_profit_hour.begin());

		if (vector_exp_hour.size() >= 200)
			vector_exp_hour.erase(vector_exp_hour.begin());

		vector_exp_hour.push_back(expHour);
		vector_profit_hour.push_back(profitHour);
	}
}

void InfoCore::refresh_player_attacking_you(std::string& messagee){
	lock_guard _lock(mtx_access);

	if (string_util::trim(messagee) == "")
		return;

	const std::string message = messagee;
	std::regex rgx("due to an attack by (.*)\\.");
	std::smatch match;

	if (std::regex_search(message.begin(), message.end(), match, rgx)){
		if (players_attack.find(match[1]) != players_attack.end())
			players_attack[match[1]].reset();
		else
			players_attack[match[1]];
	}
}

bool InfoCore::is_player_attack(){
	lock_guard _lock(mtx_access);

	if (players_attack.empty())
		return false;

	for (auto it = players_attack.begin(); it != players_attack.end();it++){
		if (it->second.elapsed_milliseconds() < 3000)
			return true;		
		else
			players_attack.erase(it);

	}

	return false;
}

void InfoCore::add_item_on_used(uint32_t itemname, uint32_t qty){
	mtx_access.lock();
	auto it = itensusedmap.find(itemname);
	if (it == itensusedmap.end()){
		itensusedmap[itemname].first = 1;
	}
	else{
		if (it->second.second != qty || qty == 0){
			it->second.second = qty;			
			it->second.first++;
		}
	}
	mtx_access.unlock();
}

bool InfoCore::go_to_first_go_upstairs(Coordinate coord){
	auto warning_message = InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_first_go_upstairs);
	if (warning_message->get_last_refresh_timeout() >= 2000)
		return false;

	if(!Actions::goto_coord_on_screen(coord, 0, true))
		return false;

	return true;
}

void InfoCore::add_item_on_used_bolt(uint32_t itemname, uint32_t qty){
	lock_guard _m_lock(mtx_access);
	auto it = itensusedmap.find(itemname);
	if (it == itensusedmap.end())
		itensusedmap[itemname].first = 1;
	else
		it->second.first += qty;
}

void InfoCore::update_last_balance(uint32_t value){
	if (value != 0)
		last_balance = value;
}

int InfoCore::get_info_time_bot_running(){
	lock_guard _m_lock(mtx_access);
	return time.elapsed_milliseconds();
}

void InfoCore::add_ping_in_vector(int ping){
	lock_guard _m_lock(mtx_access);
	if (ping_vector.size() <= 30)
		ping_vector.push_back(ping);
	else{
		ping_vector.push_back(ping);
		ping_vector.erase(ping_vector.begin());
	}
}

int InfoCore::get_last_medium_ping(){
	add_ping_in_vector(TibiaProcess::get_default()->get_ping());
	int medium = 1;
	lock_guard _m_lock(mtx_access);
	for (auto ping : ping_vector)
		medium += ping;

	if (medium <= 0 || ping_vector.size() <= 0)
		return 0;

	return medium / ping_vector.size();
}

int InfoCore::get_current_time_total_in_day(){
	_SYSTEMTIME Time;
	GetSystemTime(&Time);
	return (Time.wHour * 60 * 60 + Time.wMinute * 60 + Time.wSecond) * 1000 + Time.wMilliseconds;
}

void InfoCore::add_monster_killed(std::string monstername){
	lock_guard _m_lock(mtx_access);
	auto it = monsterkilledmap.find(monstername);
	if (it == monsterkilledmap.end())
		monsterkilledmap[monstername] = 1;
	else
		it->second = it->second + 1;
}

void InfoCore::refresh_items_used(std::string& message_in){
	if (string_util::trim(message_in) == "")
		return;

	const std::string message = message_in;
	std::regex rgx("Using one of(.*)\\.\\.\\.");
	//21:15 Using one of 518 mana potions...
	std::smatch match;

	if (std::regex_search(message.begin(), message.end(), match, rgx)){
		const std::string str = match[1];
		std::string str_temp = match[1];
		std::regex regexitemname("([a-z].+)");
		std::smatch match_in;

		if (std::regex_search(str.begin(), str.end(), match_in, regexitemname)){
			const std::string str = match_in[1];
			std::string str_new = match_in[1];
			std::regex vowel_re("[^0-9]");

			if (str_new[str.length() - 1] == 's')
				str_new.pop_back();

			std::string temp = std::regex_replace(str_temp, vowel_re, "");
			int count = std::atoi(temp.c_str());

			add_item_on_used(ItemsManager::get()->getitem_idFromName(str_new), count);
		}

	}
	std::regex rgxx("Using the last(.*)\\.\\.\\.");
	if (std::regex_search(message.begin(), message.end(), match, rgxx)){
		std::string str_item = match[1];
		add_item_on_used(ItemsManager::get()->getitem_idFromName(str_item));
	}
}

void InfoCore::refresh_item_buy_and_sell(std::string& messagee){
}

void InfoCore::refresh_monster_killed(std::string& messagee){
	if (string_util::trim(messagee) == "")
		return;

	const std::string message = messagee;
	std::regex rgx("Loot of an (.*):");
	std::regex rgx2("Loot of a (.*):");
	std::regex rgx3("Loot of (.*):");
	std::smatch match;

	if (std::regex_search(message.begin(), message.end(), match, rgx))
		add_monster_killed(std::string(match[1]));
	else if (std::regex_search(message.begin(), message.end(), match, rgx2))
		add_monster_killed(std::string(match[1]));
	else if (std::regex_search(message.begin(), message.end(), match, rgx3))
		add_monster_killed(std::string(match[1]));
}

void InfoCore::refresh_last_balance_messages(std::string& message_in){
	if (string_util::trim(message_in) == "")
		return;

	const std::string message = message_in;
	std::regex rgx("Your account balance is (.*) gold");
	std::regex rgx2("Your account balance is now (.*) gold.");
	std::smatch match;

	if (std::regex_search(message.begin(), message.end(), match, rgx2))
		update_last_balance(std::stoi(match[1]));
	else if (std::regex_search(message.begin(), message.end(), match, rgx))
		update_last_balance(std::stoi(match[1]));
}

void InfoCore::weapon_and_ammunation_wasted(){
	int this_ammo = Inventory::get()->ammunation_count();
	int this_weapon = Inventory::get()->weapon_count();
	if (this_ammo > 1){
		int dif = last_ammunation_ammount - this_ammo;
		if (dif < 3 && dif > 0)
			add_item_on_used_bolt(Inventory::get()->body_rope(), dif);
		last_ammunation_ammount = this_ammo;
	}
	if (this_weapon > 1){
		int dif = last_weapon_ammount - this_weapon;
		if (dif < 3 && dif > 0)
			add_item_on_used_bolt(Inventory::get()->body_weapon(), dif);
		last_weapon_ammount = this_weapon;
	}
}

InfoCore* InfoCore::get(){
	static InfoCore* mInfoCore = nullptr;
	if (!mInfoCore)
		mInfoCore = new InfoCore;
	return mInfoCore;
}

std::vector<std::string> InfoCore::get_players_attacking(){
	std::vector<std::string> retval;
	lock_guard scope_locked(mtx_access);
	for (auto it : players_attacking)
		retval.push_back(it.first);
	return retval;
}

void InfoCore::parse_player_attack_string(std::string& message_in){
	if (string_util::trim(message_in) == "")
		return;

	if (message_in.find("attack by a") != std::string::npos || message_in.find("attack by an") != std::string::npos)
		return;

	const std::string message = message_in;
	
	std::regex rgx;
	if (message_in.find("hitpoints") != std::string::npos){
		rgx = std::regex("You lose (.*) hitpoints due to an attack by (.*)");
	}
	else{
		rgx = std::regex("You lose (.*) hitpoint due to an attack by (.*)");
	}
	
	std::smatch matches;
	if (std::regex_search(message.begin(), message.end(), matches, rgx)){
		if (matches.size() == 2){
			std::string player_name = matches[1].str();
			//check size
			if (player_name.size() > 1){
				//remove last point
				if (*player_name.rbegin() == '.')
					player_name.erase(player_name.rbegin().base());
				lock_guard scope_locked(mtx_access);
				players_attacking[player_name].reset();
			}
		}
	}
}


BOOL SetMessageDuration(DWORD seconds, UINT flags){
	return SystemParametersInfo(0x2017,
		0, IntToPtr(seconds), flags);
}

void notify_ballon(int id, char* message, char*title, int timeout){
	extern bool IsVistaOrLater();
	if (IsVistaOrLater())
		SetMessageDuration(timeout, 0);
	NOTIFYICONDATA nid;
	memset(&nid, 0, sizeof(NOTIFYICONDATA));
	nid.cbSize = sizeof(NOTIFYICONDATA);
	nid.hWnd = 0;
	nid.uID = id;
	nid.uFlags = NIF_INFO;
	//memcpy(nid.szInfo, message, 50);

	for (int i = 0; i < 255; i++){
		nid.szInfo[i] = message[i];
		if (!message[i])
			break;
	}

	for (int i = 0; i < 63; i++){
		nid.szInfoTitle[i] = title[i];
		if (!title[i])
			break;
	}


	nid.dwInfoFlags = NIIF_INFO;
	nid.uTimeout = timeout;

	Shell_NotifyIcon(NIM_DELETE, &nid);
	Shell_NotifyIcon(NIM_ADD, &nid);
}

void InfoCore::notify(std::string message_in, std::string title, uint32_t timeout_seconds){
	notify_ballon(rand(), &message_in[0], &title[0], timeout_seconds);
}


