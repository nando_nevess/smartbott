#pragma once
#include "Util.h"
#include <functional>
#include "Util.h"
#include "..\CoreBase.h"
#include "Time.h"

enum NEUTRAL_STATUS{
	STATUS_PAUSED,
	STATUS_RUNNING
};

enum NEUTRAL_CORE_ID{
	CORE_WAYPOINTER,
	CORE_HUNTER,
	CORE_SPELLCASTER,
	CORE_LOOTER,
	CORE_GENERAL,
	CORE_LUA,
	CORE_INPUT,
	CORE_ALERTS,
	CORE_TOTAL
};

enum NEUTRAL_TASK_ID{
	TASK_AUTOMOUNT,
	TASK_ANTIIDLE,
	TASK_AUTOFISH,
	TASK_REFILL_AMMUNATION,
	TASK_MULTI_CLIENT,
	TASK_TREINER,
	TASK_TOTAL
};

enum NEUTRAL_CORE_TASK_STATE{
	DISABLED,
	ENABLED
};

#define TIBIA_CLIENT_VERSION 1081

class NeutralManager : public CoreBase{
	HWND MainWindowHandle;
	bool active_auto_save = false;
	neutral_mutex mtx_access;
	NEUTRAL_STATUS status = STATUS_RUNNING;

	std::function<void(bool paused)> pause_callback;
	std::vector<NEUTRAL_CORE_TASK_STATE > task_states;
	std::vector<NEUTRAL_CORE_TASK_STATE > core_states;
	std::map<NEUTRAL_CORE_ID, CoreBase*> core_pointers;
	std::map<NEUTRAL_TASK_ID, CoreBase*> task_pointers;
	bool bool_load;
	void delayed_init();

	void run_cores();

	void init();
	
	static std::vector<std::function<void()>> on_close_callbacks;

	void on_close();

public:
	bool get_bool_load();

	TimeChronometer time_load;

	void disable_all();

	NeutralManager();

	void set_bool_load(bool in);

	void clear();

	void run_auto_save();
	
	bool dirExists(const std::string& dirName_in);
	
	void set_main_window_handle(HWND Hwindow);

	std::string save_temp_name();

	std::vector<std::pair<std::string/*filename*/, std::time_t/*file date*/>> files_in_path(std::string file_path);

	void remove_file_temp_old();

	HWND get_main_window_handle();

	void set_core_states(NEUTRAL_CORE_ID key, NEUTRAL_CORE_TASK_STATE value);

	NEUTRAL_CORE_TASK_STATE get_core_states(NEUTRAL_CORE_ID key);

	void set_task_states(NEUTRAL_TASK_ID key, NEUTRAL_CORE_TASK_STATE value);

	NEUTRAL_CORE_TASK_STATE get_task_states(NEUTRAL_TASK_ID key);

	GET_SET(NEUTRAL_STATUS, status)

	bool load_default_config(std::string file_name);

	bool save_items_config(std::string file_name);
	bool load_items_config(std::string file_name);

	void set_pause_callback(std::function<void(bool paused)> callback);

	uint32_t getTibiaVersion();

	bool save_script(std::string file_name);

	bool save_default_config(std::string file_name);

	bool load_script(std::string file_name);

	bool check_default_config();

	Json::Value CoreStatesToJson();

	void ParseCoreStates(Json::Value& json_core_states);

	void start();

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value neutralMgr);

	static void add_callback_on_close(std::function<void()> callback);

	void close();

	void set_pause_state(bool);

	bool get_pause_state();
			
	bool is_paused();

	void pause(){
		set_pause_state(true);
	}

	bool get_bot_state();

	void set_bot_state(bool);

	bool get_looter_state();

	bool get_hunter_state();

	bool get_spellcaster_state();

	bool get_waypointer_state();

	bool get_lua_state();

	bool get_alerts_state();

	void set_looter_state(bool);

	void set_hunter_state(bool);

	void set_spellcaster_state(bool);

	void set_waypointer_state(bool);

	void set_lua_state(bool);

	void set_alerts_state(bool);

	void show_bot_interface();

	void hide_bot_interface();

	void minimize_bot_interface();

	void restore_bot_interface();

	void request_close_bot_process();

	void kill_bot_process();

	void kill_tibia_client();

	static NeutralManager* get();
};
