#include "DepoterCore.h"
#include "Actions.h"
#include "ItemsManager.h"
#include "Pathfinder.h"
#include "..\\string_util.h"
#include "..\\LooterManager.h"

int DepoterCore::lastDestinationBackpackIndex = 0;

void DepoterCore::request_deposit_all_items(){
	bool if_still_have = false;

	ContainerManager::get()->maximize_all_container();

	do{
		bool use_new = DepoterManager::get()->get_use_old_style();
		std::map<std::string, std::shared_ptr<DepotContainer>> myMap = DepoterManager::get()->getMapDepotContainer();
		for (auto depoter : myMap){
			if (use_new)
				deposit_items_new(depoter.first);
			else
				deposit_items(depoter.first);
		}

		if_still_have = false;
		std::vector<uint32_t> looterContainer = LooterManager::get()->get_all_containers_id();
		for (auto container_id : looterContainer){
			auto container = ContainerManager::get()->get_container_by_id(container_id);
			if (!container)
				continue;

			uint32_t old_body = container->header_info->body_address;

			ItemContainerPtr container_in;
			TimeChronometer time;
			while (time.elapsed_milliseconds() < 2000){
				container = ContainerManager::get()->get_container_by_id(container_id);
				if (!container)
					continue;

				TibiaProcess::get_default()->wait_ping_delay(1.5);

				container->up_container();

				TibiaProcess::get_default()->wait_ping_delay(1.5);

				container = ContainerManager::get()->get_container_by_id(container_id);
				if (!container)
					continue;

				uint32_t current_body = container->header_info->body_address;
				
				if (old_body != current_body){
					if_still_have = true;
					break;
				}
			}
		}

	} while (if_still_have);
}

void DepoterCore::request_deposit(std::string DepotContainerId){
	bool if_still_have = false;
	bool use_new = DepoterManager::get()->get_use_old_style();

	ContainerManager::get()->maximize_all_container();

	do{
		std::vector<uint32_t> looterContainer = LooterManager::get()->get_all_containers_id();
		std::map<uint32_t, ItemContainerPtr> containers_open = ContainerManager::get()->get_containers();

		if (use_new)
			deposit_items_new(DepotContainerId);
		else
			deposit_items(DepotContainerId);
		
		if_still_have = false;

		for (auto container : containers_open){
			bool for_continue = false;

			for (auto container_id : looterContainer)
			if (container_id == container.second->get_id())
				for_continue = true;

			if (!for_continue)
				continue;

			TibiaProcess::get_default()->wait_ping_delay(1.5);

			if (Actions::get()->up_backpack(container.second->get_caption()))
				if_still_have = true;

			TibiaProcess::get_default()->wait_ping_delay(1.5);
		}
	} while (if_still_have);
}

action_retval_t DepoterCore::deposit_items_new(std::string DepotContainerId){
	std::shared_ptr<DepotContainer> DepotContainer = DepoterManager::get()->getDepotContainer(DepotContainerId);
	if (!DepotContainer)
		return action_retval_t::action_retval_missing_parameter;

	int depotBoxId = DepotContainer->get_Containeritem_id();
	ItemContainerPtr DepotChest = ContainerManager::get()->get_depot_chest_container();
	if (!DepotChest) {
		open_depot_chest();

		TibiaProcess::get_default()->wait_ping_delay(1.0);

		DepotChest = ContainerManager::get()->get_depot_chest_container();

		if (!DepotChest)
			return action_retval_t::action_retval_missing_depot_chest;
	}
		
	if (move_items(DepotChest, DepotContainer))
		return action_retval_t::action_retval_success;

	return action_retval_t::action_retval_fail;
}

action_retval_t DepoterCore::deposit_items(std::string DepotContainerId){
	std::shared_ptr<DepotContainer> DepotContainer = DepoterManager::get()->getDepotContainer(DepotContainerId);
	if (!DepotContainer)
		return action_retval_t::action_retval_missing_parameter;
	
	uint32_t containerId = DepoterManager::get()->get_item_id();
	ItemContainerPtr MainDpBackpack = ContainerManager::get()->get_container_by_id(containerId);

	if (!MainDpBackpack) {
		uint32_t depotBoxId = DepoterManager::get()->get_DepotBoxId();
		
		if (string_util::lower(DepoterManager::get()->get_DepotBoxName()) == "depot chest")
			depotBoxId = 3502;
				
		open_depot_box(depotBoxId);

		int try_count = 0;
		while (try_count < Actions::Max_Count) {
			TibiaProcess::get_default()->wait_ping_delay(1.0);

			ContainerPosition item_on_backpack = ContainerManager::get()->find_item(containerId);
			if (item_on_backpack.is_null()){
				try_count++;
				continue;
			}

			Actions::use_item_on_container(item_on_backpack);
			ContainerManager::get()->wait_container_id_open(containerId);

			MainDpBackpack = ContainerManager::get()->get_container_by_id(containerId);
			if (!MainDpBackpack){
				try_count++;
				continue;
			}

			try_count++;
		}
	}

	if (!MainDpBackpack)
		return action_retval_t::action_retval_missing_parameter;

	if (move_items(MainDpBackpack, DepotContainer))
		return action_retval_t::action_retval_success;

	return action_retval_t::action_retval_fail;
}

action_retval_t DepoterCore::move_items(ItemContainerPtr mainBackpack, std::shared_ptr<DepotContainer> depotContainer){
	uint32_t ItemsDeposited = 0;
	std::map<std::string, std::shared_ptr<DepotItem>> myMap = depotContainer->getMapDepoItens();
	for (auto item : myMap) {
		int destinyBackpackId = depotContainer->get_item_id();

		if (destinyBackpackId == 0)
			destinyBackpackId = ItemsManager::get()->getitem_idFromName(depotContainer->get_ContainerName());

		uint32_t itemCount = ContainerManager::get()->get_item_count(item.second->get_item_id());
		if (itemCount <= 0){
			ItemsDeposited++;
			continue;
		}

		ContainerPosition itemContainer = ContainerManager::get()->find_item(item.second->get_item_id());

		if (itemContainer.is_null())
			continue;

		action_retval_t result_try_move = action_retval_t::action_retval_fail;
		if (DepoterManager::get()->get_use_old_style())
			result_try_move = try_to_move_item(mainBackpack, destinyBackpackId, item.second);
		else
			result_try_move = try_to_move_item(mainBackpack, destinyBackpackId, item.second, true);

		if (result_try_move == action_retval_success){
			ItemsDeposited++;
			continue;
		}
	}

	if (ItemsDeposited >= myMap.size())
		return action_retval_t::action_retval_success;

	return action_retval_t::action_retval_trying;
}

action_retval_t DepoterCore::try_to_move_item(ItemContainerPtr mainBackpack, int destinyBackpackId, std::shared_ptr<DepotItem> item,bool use_last) {
	uint32_t item_id = item->get_item_id();
	uint32_t itemCount = 0;

	int loopTryingCount = 0;
	while (true) {
		Sleep(50);

		if (!mainBackpack)
			return action_retval_t::action_retval_missing_parameter;

		int destinyBackpackSlotIndex = UINT32_MAX;

		if (use_last)
			destinyBackpackSlotIndex = mainBackpack->find_item_slot(destinyBackpackId, lastDestinationBackpackIndex);
		else
			destinyBackpackSlotIndex = mainBackpack->find_item_slot(destinyBackpackId);

		if (destinyBackpackSlotIndex == UINT32_MAX)
			return action_retval_t::action_retval_container_not_found;

		ContainerPosition destinationbackpack(mainBackpack->get_container_index(), destinyBackpackSlotIndex);

		ContainerPosition itemContainerPosition = ContainerManager::get()->find_item(item_id);
		if (itemContainerPosition.is_null())
			return action_retval_t::action_retval_container_not_found;

		ItemContainerPtr itemContainer = ContainerManager::get()->get_container_by_index(itemContainerPosition.container_index);
		if (!itemContainer)
			return action_retval_t::action_retval_container_not_found;
	
		ItemContainerPtr mainBackpack_current = ContainerManager::get()->get_container_by_id(mainBackpack->get_id());
		if (!mainBackpack_current)
			return action_retval_t::action_retval_missing_depot_chest;
		
		uint32_t currentItemCount = itemContainer->get_item_count_by_id(item_id);
		if (currentItemCount == 0 || currentItemCount <= item->get_minqty())
			return action_retval_t::action_retval_success;

		if (currentItemCount == itemCount)
			loopTryingCount++;

		if (loopTryingCount == 2) {
			lastDestinationBackpackIndex++;
			loopTryingCount = 0;
		}

		int virtualItemCount = currentItemCount - item->get_minqty();
		if (virtualItemCount <= 0)
			return action_retval_t::action_retval_success;

		if (item->get_minqty() > 0)
			itemCount = virtualItemCount;
		else
			itemCount = currentItemCount;
		
		TibiaProcess::get_default()->wait_ping_delay(0.5);
		Actions::move_item_container_to_container(itemContainerPosition, destinationbackpack, itemCount, item_id);
	}
}

TilePtr DepoterCore::search_depot_chest(){
	TilePtr depot = gMapTilesPtr->get_nearest_depot_tile(true, map_front_mini_t);

	if (depot == nullptr)
		return nullptr;

	return depot;
}

action_retval_t DepoterCore::go_to_depot_chest() {
	TibiaProcess::get_default()->character_info->wait_stop_waking();

	TibiaProcess::get_default()->wait_ping_delay(1.0);

	TilePtr depot = search_depot_chest();

	if (!depot)
		return action_retval_t::action_retval_fail;

	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (depot->coordinate.get_axis_max_dist(current_coord) == 1)
		return action_retval_t::action_retval_success;	

	Coordinate nearest_depot_coord = Pathfinder::get()->get_nearest_coordinate_from_another(depot->coordinate);
	if (nearest_depot_coord.is_null())
		return action_retval_t::action_retval_fail;

	int try_count = 0;
	while (try_count < Actions::Max_Count) {
		if (Actions::goto_coord_on_screen(nearest_depot_coord, 0, true))
			return action_retval_t::action_retval_success;

		try_count++;
	}

	return action_retval_t::action_retval_trying;
}

action_retval_t DepoterCore::open_depot() {
	Inventory::get()->minimize();

	ItemContainerPtr depot_chest = ContainerManager::get()->get_depot_chest_container();
	if (depot_chest) {
		depot_chest->up_container();
		return action_retval_t::action_retval_success;
	}

	action_retval_t depot_reached_retval = DepoterCore::go_to_depot_chest();

	if (depot_reached_retval != action_retval_t::action_retval_success)
		return depot_reached_retval;

	TilePtr depot_chest_position = gMapTilesPtr->get_nearest_tile(gMapTilesPtr->get_depot_chest(), false, map_front_mini_t);

	if (!depot_chest_position)
		return action_retval_t::action_retval_fail;

	int try_count = 0;
	while (try_count < Actions::Max_Count) {
		BaseActions::use_item_on_coordinate(depot_chest_position->coordinate);
		if (ContainerManager::get()->wait_depot_locker_open())
			return action_retval_t::action_retval_success;

		try_count++;
	}

	return action_retval_t::action_retval_fail;
}

action_retval_t DepoterCore::open_depot_box(uint32_t depotboxid){
	ItemContainerPtr depot_box = ContainerManager::get()->get_depot_box_container(depotboxid);
	if (depot_box)
		return action_retval_t::action_retval_success;

	int try_count = 0;
	while (try_count < Actions::Max_Count) {
		Sleep(10);

		if (open_depot_chest() == action_retval_t::action_retval_success)
			break;

		try_count++;
	}

	ItemContainerPtr depot_chest = ContainerManager::get()->get_depot_chest_container();

	if (!depot_chest)
		return action_retval_t::action_retval_container_not_found;
	
	depot_box = ContainerManager::get()->get_depot_box_container(depotboxid);

	if (depot_box)
		return action_retval_t::action_retval_success;

	ContainerPosition depot_chest_position = ContainerManager::get()->find_item(depotboxid, depot_chest->get_id());

	BaseActions::use_item_on_container(depot_chest_position, depotboxid);

	return ContainerManager::get()->wait_container_id_open(depot_chest->get_id()) ? action_retval_t::action_retval_success : action_retval_t::action_retval_trying;
}

action_retval_t DepoterCore::open_depot_chest() {
	ItemContainerPtr depot_chest = ContainerManager::get()->get_depot_chest_container();
	if (depot_chest)
		return  action_retval_t::action_retval_success;

	action_retval_t open_ret = DepoterCore::go_to_depot_chest();
	if (open_ret != action_retval_t::action_retval_success)
		return open_ret;

	ItemContainerPtr depot_locker = ContainerManager::get()->get_depot_locker_container();
	if (!depot_locker) {
		if (open_depot()) {
			if (!ContainerManager::get()->wait_depot_locker_open())
				return action_retval_t::action_retval_trying;

			depot_locker = ContainerManager::get()->get_depot_locker_container();
		}
	}

	depot_chest = ContainerManager::get()->get_depot_chest_container();

	if (!depot_locker)
		return action_retval_t::action_retval_trying;

	if (!depot_chest) {
		int deposit_chest_id = 3502;
		ContainerPosition depot_chest_position = ContainerManager::get()->find_item(deposit_chest_id, depot_locker->get_id());
		BaseActions::use_item_on_container(depot_chest_position, deposit_chest_id);
		return ContainerManager::get()->wait_container_id_open(3502) ? action_retval_t::action_retval_success : action_retval_t::action_retval_trying;
	}

	return action_retval_t::action_retval_trying;
}