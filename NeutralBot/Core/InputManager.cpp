#include "InputEvent.h"
#include "InputManager.h"
#include <thread>
#include "..\\GeneralManager.h"
#include "..\\DevelopmentManager.h"

HHOOK	kbdhook = nullptr;
HHOOK	mousehook = nullptr;

LRESULT InputManager::handlekeys(int code, WPARAM wp, LPARAM lp){
	InputManager::get()->add_keyboard_event_code(code, wp, lp);
	return CallNextHookEx(kbdhook, code, wp, lp);
}

LRESULT InputManager::handlemouse(int code, WPARAM wp, LPARAM lp){
	InputManager::get()->add_mouse_event_code(code, wp, lp);
	return CallNextHookEx(mousehook, code, wp, lp);
}

void InputManager::check_thread(){
	MONITOR_THREAD(__FUNCTION__);
	kbdhook = SetWindowsHookEx(WH_KEYBOARD_LL, (HOOKPROC)InputManager::handlekeys, NULL, NULL);
	mousehook = SetWindowsHookEx(WH_MOUSE_LL, (HOOKPROC)InputManager::handlemouse, NULL, NULL);

	MSG	msg;
	while (true) {
		Sleep(5);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!GetMessage(&msg, NULL, 0, 0))
			continue;

		TranslateMessage(&msg);
		DispatchMessage(&msg);
	}
}


void InputManager::process_input_thread(){
	MONITOR_THREAD(__FUNCTION__);
	//TODO VERIFICAR CONSUMO DE CPU
	while (true){
		Sleep(5);

		if (!has_action)
			continue;

		while (has_action){
			Sleep(5);

			if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
				continue;
			
			mtx_access.lock();
			std::shared_ptr<InputBaseEvent> _event = nullptr;
			auto first = inputs.begin();
			if (first != inputs.end()){
				_event = *first;
				inputs.erase(first);
			}
			else{
				mtx_access.unlock();
				has_action = false;
				continue;
			}
				

			mtx_access.unlock();
			mtx_invoke.lock();
			for (auto callback : callbacks)
				(callback.second)(_event.get());
			mtx_invoke.unlock();
		}
	}
}


void InputManager::handle_event(std::shared_ptr<InputBaseEvent> input){
	mtx_access.lock();
	inputs.push_back(input);
	has_action = true;
	mtx_access.unlock();
}

InputManager::InputManager(){
	if (GeneralManager::get()->get_dev_mode())
		return;

	std::thread([&](){ 
		Sleep(1000);//initialization delay
		std::thread(std::bind(&InputManager::check_thread, this)).detach();
		std::thread(std::bind(&InputManager::process_input_thread, this)).detach();
		std::thread(std::bind(&InputManager::processor_thread, this)).detach();
	}).detach();
	
}

int InputManager::add_callback(uint32_t id, std::function<void(InputBaseEvent* input_event)> callback){
	mtx_invoke.lock();
	if (!id){
		int new_index = 1000;
		while (callbacks.find(new_index) != callbacks.end())
			new_index++;
		id = new_index;
	}
	if (!callback){
		auto it = callbacks.find(id);
		if (it != callbacks.end())
			callbacks.erase(it);
	}
	else
		callbacks[id] = callback;
	mtx_invoke.unlock();
	return id;
}

void InputManager::remove_callback(uint32_t id){
	mtx_invoke.lock();
	auto it = callbacks.find(id);
	if (it != callbacks.end())
		callbacks.erase(it);
	mtx_invoke.unlock();
}


void InputManager::add_mouse_event_code(int code, WPARAM wp, LPARAM lp){
	events_mutex.lock();
	events.push_back(InputEventParams(true, code, wp, lp));
	has_event = true;
	events_mutex.unlock();
}


void InputManager::add_keyboard_event_code(int code, WPARAM wp, LPARAM lp){
	events_mutex.lock();
	events.push_back(InputEventParams(false, code, wp, lp));
	has_event = true;
	events_mutex.unlock();
}

InputEventParams InputManager::get_event(){
	InputEventParams retval;
	events_mutex.lock();
	auto& begin = events.begin();
	retval = *begin;
	events.erase(begin);
	if (!events.size())
		has_event = false;
	
	events_mutex.unlock();
	return retval;
}

void InputManager::processor_thread(){
	MONITOR_THREAD(__FUNCTION__);

	while (true){
		Sleep(5);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;
		
		while (!has_event)
			Sleep(5);

		InputEventParams _event = get_event();
		if (_event.mouse)
			process_mouse(_event);
		else
			process_keyboard(_event);
	}
}

void InputManager::process_mouse(InputEventParams& _event){
	MouseInput* current_input = new MouseInput;
	current_input->ctrl_pressed = GetAsyncKeyState(VK_CONTROL) != 0;
	current_input->shift_pressed = GetAsyncKeyState(VK_SHIFT) != 0;
	current_input->alt_pressed = GetAsyncKeyState(VK_MENU) != 0;


	current_input->capslock_state = (GetKeyState(VK_CAPITAL) != 0);
	current_input->insert_state = (GetKeyState(VK_INSERT) != 0);//TODO
	current_input->numlock_state = (GetKeyState(VK_NUMLOCK) != 0);
	current_input->scrolllock_state = (GetKeyState(VK_SCROLL) != 0);
	current_input->event_type = mouse_event_t::mouse_event_none;
	if (HC_ACTION == _event.code){

		MSLLHOOKSTRUCT* mouse_struct = (MSLLHOOKSTRUCT*)&_event.mouse_struct;
		if (_event.wp == WM_MOUSEWHEEL || _event.wp == 0x020E){
			current_input->wheel_delta = HIWORD(mouse_struct->mouseData);
			current_input->event_type = mouse_event_t::mouse_event_wheel;
		}
		else{
			if (_event.wp == WM_LBUTTONDOWN || _event.wp == WM_LBUTTONUP || _event.wp == WM_LBUTTONDBLCLK){
				current_input->button_code = mouse_button_t::mouse_button_left;
			}
			if (_event.wp == WM_RBUTTONDOWN || _event.wp == WM_RBUTTONUP || _event.wp == WM_RBUTTONDBLCLK){
				current_input->button_code = mouse_button_t::mouse_button_rigth;
			}
			if (_event.wp == WM_MBUTTONDOWN || _event.wp == WM_MBUTTONUP || _event.wp == WM_MBUTTONDBLCLK){
				current_input->button_code = mouse_button_t::mouse_button_middle;
			}
			if (_event.wp == WM_XBUTTONDOWN || _event.wp == WM_XBUTTONUP || _event.wp == WM_XBUTTONDBLCLK || _event.wp == WM_NCXBUTTONDOWN || _event.wp == WM_NCXBUTTONUP || _event.wp == WM_NCXBUTTONDBLCLK){
				if (!HIWORD(mouse_struct->mouseData))
					current_input->button_code = mouse_button_t::mouse_button_additional_1;
				else
					current_input->button_code = mouse_button_t::mouse_button_additional_2;
			}

			if (_event.wp == WM_MOUSEMOVE){
				current_input->event_type = mouse_event_t::mouse_event_move;
				current_input->button_code = mouse_button_t::mouse_button_none;
			}

			if (current_input->button_code != mouse_button_t::mouse_button_none){
				if (WM_LBUTTONDOWN == _event.wp || WM_RBUTTONDOWN == _event.wp || WM_MBUTTONDOWN == _event.wp || WM_XBUTTONDOWN == _event.wp || WM_NCXBUTTONDOWN == _event.wp){
					current_input->event_type = mouse_event_t::mouse_event_button_down;
				}
				else if (WM_LBUTTONUP == _event.wp || WM_RBUTTONUP == _event.wp || WM_MBUTTONUP == _event.wp || WM_XBUTTONUP == _event.wp || WM_NCXBUTTONUP == _event.wp){
					current_input->event_type = mouse_event_t::mouse_event_button_up;
				}
				else if (WM_LBUTTONDBLCLK == _event.wp || WM_RBUTTONDBLCLK == _event.wp || WM_MBUTTONDBLCLK == _event.wp || WM_NCXBUTTONDBLCLK == _event.wp || WM_XBUTTONDBLCLK == _event.wp){
					current_input->event_type = mouse_event_t::mouse_event_double_click;
				}
			}
		}

		current_input->x = mouse_struct->pt.x;
		current_input->y = mouse_struct->pt.y;
	}
	InputManager::get()->handle_event(std::shared_ptr<InputBaseEvent>(current_input));
}

void InputManager::process_keyboard(InputEventParams& _event){
	KeyboardInput* current_input = new KeyboardInput;

	current_input->ctrl_pressed = GetAsyncKeyState(VK_CONTROL) != 0;
	current_input->shift_pressed = GetAsyncKeyState(VK_SHIFT) != 0;
	current_input->alt_pressed = GetAsyncKeyState(VK_MENU) != 0;
	current_input->capslock_state = (GetKeyState(VK_CAPITAL) != 0);
	current_input->insert_state = (GetKeyState(VK_INSERT) != 0);//TODO
	current_input->numlock_state = (GetKeyState(VK_NUMLOCK) != 0);
	current_input->scrolllock_state = (GetKeyState(VK_SCROLL) != 0);


	if (_event.code == HC_ACTION && (_event.wp == WM_SYSKEYDOWN || _event.wp == WM_KEYDOWN || _event.wp == WM_KEYUP || _event.wp == WM_SYSKEYUP)) {
		if (_event.wp == WM_SYSKEYDOWN || _event.wp == WM_KEYDOWN)
			current_input->event_type = (keyboard_event_t::keyboard_key_down);
		else
			current_input->event_type = (keyboard_event_t::keyboard_key_up);
		static bool capslock = false;
		static bool shift = false;
		char tmp[0xFF] = { 0 };
		std::string str;
		DWORD msg = 1;
		KBDLLHOOKSTRUCT& st_hook = _event.keyboard_struct;
		bool printable;
		current_input->button_code = (uint32_t)st_hook.vkCode;
		current_input->character = (char)st_hook.vkCode;


		msg += (st_hook.scanCode << 16);
		msg += (st_hook.flags << 24);
		GetKeyNameText(msg, tmp, 0xFF);
		str = std::string(tmp);

		printable = (str.length() <= 1) ? true : false;

		if (!printable) {
			if (str == "CAPSLOCK")
				capslock = true;
			else if (str == "SHIFT")
				shift = true;

			if (str == "ENTER") {
				//str = "\n";
				//	printable = true;
			}
			else if (str == "SPACE") {
				//str = " ";
				//	printable = true;
			}
			else if (str == "TAB") {
				//str = "\t";
				//	printable = true;
			}
			else {
				//str = ("[" + str + "]");
			}
		}


		shift = current_input->shift_pressed;
		capslock = current_input->capslock_state;

		//if (printable) {
		if (printable && shift == capslock) { /* Lowercase */
			for (size_t i = 0; i < str.length(); ++i)
				str[i] = tolower(str[i]);
		}
		else { /* Uppercase */
			for (size_t i = 0; i < str.length(); ++i) {
				if (str[i] >= 'A' && str[i] <= 'Z') {
					str[i] = toupper(str[i]);
				}
			}
		}

		shift = false;
		//}
		current_input->key_string = str;
	}
	InputManager::get()->handle_event(std::shared_ptr<InputBaseEvent>(current_input));

}




std::shared_ptr<InputHolder> InputManager::wait_input(uint32_t timeout, 
	input_match_params& match){
	InputHolder* my_input = nullptr;
	neutral_mutex local_mtx_access;
	TimeChronometer local_timer;
	std::function<void(InputBaseEvent* input_event)> local_callback = [&](InputBaseEvent* input_event){
		
		local_mtx_access.lock();
		if (match.match(input_event))
			if (!my_input)
				my_input = new InputHolder(input_event);

		local_mtx_access.unlock();
	};

	uint32_t mCallbackId = add_callback(0, local_callback);
	while (!my_input && (uint32_t)local_timer.elapsed_milliseconds() < timeout)
		Sleep(1);

	remove_callback(mCallbackId);
	if (my_input)
		return std::shared_ptr<InputHolder>(my_input);
	return std::shared_ptr<InputHolder>();
}
