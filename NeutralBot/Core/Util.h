#pragma once

#include <boost\asio.hpp>

#pragma region WINDOWS_OS_HEADERS
#include <Windows.h>
#include <psapi.h>
#include <tlhelp32.h>
#include <shlobj.h> 
#include <memory>
#include <shlobj.h>
#pragma comment(lib, "shell32.lib")
#pragma endregion

#pragma region STD_INCLUDES
#include <stdint.h>
#include <string>
#include <stdio.h> 
#include <map>
#include <vector>
#include <fstream>
#include <iostream>
#include <sstream>
#pragma endregion

#pragma region BOOST_INCLUDES
//#include <boost\thread.hpp>
#include <boost\chrono.hpp>
#include <boost\lexical_cast.hpp>
#include <boost\any.hpp>
#include <boost\shared_ptr.hpp>
#include <boost\bind.hpp>
//#include <boost\thread\lock_guard.hpp>
#pragma endregion

#pragma region NEUTRAL_INCLUDES 
#include "..\string_util.h"
#include "constants.h"
#include <json\json.h>
#pragma endregion

#pragma region NEUTRAL_MACROS
#define GET_SET_MT(type,var)\
	void set_##var(type value){\
	lock_guard _lock(mtx_access);\
	this->var = value;\
		}\
	type get_##var(){\
	lock_guard _lock(mtx_access);\
	return this->var;\
			}\

#define GET_SET(type,var)\
	void set_##var(type value){\
	this->var = value;\
				}\
	type get_##var(){\
	return this->var;\
}

#define GETTER(type,var)\
type get_##var(){\
	return this->var;\
}

#define SETTER(type,var)\
void set_##var(type value){\
	this->var = value;\
}

#define GET_SET_AT(keytype,type,var)\
	void set_##var( keytype key,type value){\
	this->var[key] = value;\
								}\
	type get_##var( keytype key){\
	return this->var[key];\
}
/*
	multithreaded macro uses, need variable declared neutral_mutex mtx_access
*/
#define GET_SET_AT_MT(keytype,type,var)\
	void set_##var( keytype key,type value){\
lock_guard _lock(mtx_access); \
	this->var[key] = value;\
																}\
	type get_##var( keytype key){\
	lock_guard _lock(mtx_access);\
	return this->var[key];\
}\


#define GET_SET_MTX_ACCESS(type,var)\
	void set_##var(type value){\
	\
	mtx_access.lock()\
	this->var = value; mtx_access.unlock()\
		}\
	type get_##var(){\
	\
	mtx_access.lock()\
	type temp = this->var; mtx_access.unlock()\
	return temp; \
		}
#define _read_int(address, use_base)\
	TibiaProcess::get_default()->read_int(address, use_base)

#define _get_address(type)\
	AddressManager::get()->getAddress(type)

#define DEFAULT_GET_SET GET_SET
#define NEUTRAL_MT \
neutral_mutex mtx_access;
//#ifdef DEV_MODE
#define FUNCTION_MEASURE_TIME_START(id) TimeChronometer function_measure_##id;
#define FUNCTION_MEASURE_TIME_ELAPSED(id) \
	//std::cout << "\n " << __FUNCTION__ << " : "  << __LINE__ << " elapsed " << function_measure_##id.elapsed_milliseconds() << " ms";

#define FUNCTION_MEASURE_TIME_ELAPSED_RESET(id) \
	//std::cout << "\n " << __FUNCTION__ << " : "  << __LINE__ << " elapsed " << function_measure_##id.elapsed_milliseconds() << " ms"; function_measure_##id.reset();
#define FUNCTION_MEASURE_TIME_END(id) \
	//std::cout << "\n " << __FUNCTION__ << " : "  << __LINE__ << " finish elapsed " << function_measure_##id.elapsed_milliseconds() << " ms";

#define DEFAULT_DIR_SETTINGS_PATH std::string(GetDocummentsDir() + "\\SmartSettings")
#define DEFAULT_HOTKEY_FILE_PATH std::string(DEFAULT_DIR_SETTINGS_PATH + "\\hotkeys.json")

//#else
//empty macros
//#define FUNCTION_MEASURE_TIME_START(id)
//#define FUNCTION_MEASURE_TIME_ELAPSED(id)
//#define FUNCTION_MEASURE_TIME_ELAPSED_RESET(id)
//#define FUNCTION_MEASURE_TIME_END(id)
//#endif

#define FUNCTION_MEASURE_TIME_START(id) TimeChronometer function_measure_##id;
#define FUNCTION_MEASURE_TIME_ELAPSED(id) \
	//std::cout << "\n " << __FUNCTION__ << " : "  << __LINE__ << " elapsed " << function_measure_##id.elapsed_milliseconds() << " ms";

#define FUNCTION_MEASURE_TIME_ELAPSED_RESET(id) \
	//std::cout << "\n " << __FUNCTION__ << " : "  << __LINE__ << " elapsed " << function_measure_##id.elapsed_milliseconds() << " ms"; function_measure_##id.reset();
#define FUNCTION_MEASURE_TIME_END(id) \
	//std::cout << "\n " << __FUNCTION__ << " : "  << __LINE__ << " finish elapsed " << function_measure_##id.elapsed_milliseconds() << " ms";


#pragma endregion


class neutral_mutex {
public: neutral_mutex() {
		::InitializeCriticalSection(&this->critical_section);
	}
public: ~neutral_mutex() {
		::DeleteCriticalSection(&this->critical_section);
	}
	public: void lock() {
		::EnterCriticalSection(&this->critical_section);
	}
	public: bool trylock() {
		return (::TryEnterCriticalSection(&this->critical_section) == TRUE);
	}
	public: void unlock() {
		::LeaveCriticalSection(&this->critical_section);
	}
	 CRITICAL_SECTION critical_section;
};

class lock_guard {
public: lock_guard(neutral_mutex &lockable) :
		lockable(lockable) {
		lockable.lock();
	}
	public: ~lock_guard() {
		lockable.unlock();
	}
 neutral_mutex &lockable;
};



class Point;

class Rect{
	int32_t x;
	int32_t y;
	int32_t width;
	int32_t height;
public:
	Rect();

	Rect(int32_t _x, int32_t _y, int32_t _width, int32_t _height);

	uint32_t get_x();

	uint32_t get_y();

	uint32_t get_width();

	uint32_t get_height();

	void set_x(uint32_t new_value);

	void set_y(uint32_t new_value);

	void set_width(uint32_t new_value);

	void set_height(uint32_t new_value);

	void normalize();

	bool is_null();
	Point get_center();
	bool is_valid();
	bool Contains(Point& point);
	Rect operator+ (Point point);
};

class Point{
public:
	Point(){ x = 0; y = 0; }
	Point(int32_t _x, int32_t _y){ x = _x; y = _y; }	
	int32_t x, y;

	Point(Rect rect);
	bool is_null();
	Point operator+(Point other);
};

class Coordinate{
public:
	Coordinate();
	Coordinate(COORD coord);
	Coordinate(int32_t _x, int32_t _y, int8_t _z);
	Coordinate(int32_t _x, int32_t _y);
	Coordinate(Point p);
	Coordinate(Point p, int32_t z);
	Coordinate(std::string coord_as_string);


	int32_t x, y;
	int8_t z;//can be negative

	bool is_null();
	bool is_x_null();
	bool is_y_null();
	bool is_z_null();

	int32_t get_abs_x();
	int32_t get_abs_y();
	int32_t get_abs_z();

	void update_if_null_member(Coordinate _new);

	Coordinate get_abs_coord();
	Coordinate operator -(Coordinate other);
	Coordinate operator +(Coordinate other);
	std::shared_ptr<Coordinate> get_as_pointer();

	static Coordinate* get();

	bool operator !=(Coordinate other);
	bool operator ==(Coordinate other);
	bool is_in_near_other(Coordinate other, int x_dist = 7, int y_dist = 5, int z_dist = 0, bool ignore_z = false);

	uint32_t get_axis_max_dist(Coordinate other);
	uint32_t get_axis_min_dist(Coordinate other);
	uint32_t get_total_distance(Coordinate other);
	uint32_t get_greatest_axis_value(){
		return (x > y ? x : y);
	}
	Point to_point();
	
	template<int_fast32_t deltax, int_fast32_t deltay>
	inline static bool areInRange(const Coordinate& p1, const Coordinate& p2) {
		return Coordinate::getDistanceX(p1, p2) <= deltax && Coordinate::getDistanceY(p1, p2) <= deltay;
	}

	template<int_fast32_t deltax, int_fast32_t deltay, int_fast16_t deltaz>
	inline static bool areInRange(const Coordinate& p1, const Coordinate& p2) {
		return Coordinate::getDistanceX(p1, p2) <= deltax && Coordinate::getDistanceY(p1, p2) <= deltay && Coordinate::getDistanceZ(p1, p2) <= deltaz;
	}

	inline static int_fast32_t getOffsetX(const Coordinate& p1, const Coordinate& p2) {
		return p1.getX() - p2.getX();
	}
	inline static int_fast32_t getOffsetY(const Coordinate& p1, const Coordinate& p2) {
		return p1.getY() - p2.getY();
	}
	inline static int_fast16_t getOffsetZ(const Coordinate& p1, const Coordinate& p2) {
		return p1.getZ() - p2.getZ();
	}

	inline static int32_t getDistanceX(const Coordinate& p1, const Coordinate& p2) {
		return std::abs(Coordinate::getOffsetX(p1, p2));
	}
	inline static int32_t getDistanceY(const Coordinate& p1, const Coordinate& p2) {
		return std::abs(Coordinate::getOffsetY(p1, p2));
	}
	inline static int16_t getDistanceZ(const Coordinate& p1, const Coordinate& p2) {
		return std::abs(Coordinate::getOffsetZ(p1, p2));
	}

	inline int_fast32_t getX() const { return x; }
	inline int_fast32_t getY() const { return y; }
	inline int_fast16_t getZ() const { return z; }

	std::string ToString(){
		return ("x:" + std::to_string(x) + ", y:" + std::to_string(y) + ", z:" + std::to_string(z));
	}
};

class Range{
	bool _is_nill;
	int32_t min_x, min_y, min_z;
	int32_t max_x, max_y, max_z;

	void update_is_nill();
public:
	Range(int32_t _min_x = INT32_MAX, int32_t _min_y = INT32_MAX, int32_t _min_z = INT32_MAX,
		int32_t _max_x = INT32_MAX, int32_t _max_y = INT32_MAX, int32_t _max_z = INT32_MAX);

	Range(Rect& rect);

	bool is_null();

	void set_min_x(int32_t x);
	int32_t get_min_x();
	void set_min_y(int32_t y);
	int32_t get_min_y();
	void set_min_z(int32_t z);
	int32_t get_min_z();

	void set_max_x(int32_t x);
	int32_t get_max_x();
	void set_max_y(int32_t y);
	int32_t get_max_y();
	void set_max_z(int32_t x);
	int32_t get_max_z();

	bool is_null_range_z();
	bool is_null_range_x();
	bool is_null_range_y();


	bool is_in_range(Point pt);
	bool is_in_range(Coordinate coord);
};

typedef std::shared_ptr<Range> RangePtr;
typedef std::shared_ptr<Coordinate> CoordinatePtr;
typedef Coordinate Position;
typedef std::shared_ptr<Position> PositionPtr;

Json::Value loadJsonFile(std::string file);
bool saveJsonFile(std::string file, Json::Value json);

std::pair<char*, uint32_t> load_file_data(std::string path);

std::map<std::string, std::pair<char*, uint32_t>> get_unziped_files(std::string file);

std::string GetDocummentsDir();

namespace Util{
	template <class T>
	inline bool is_is_vector(std::vector<T>& vector, T& value){
		return std::find(vector.begin(), vector.end(), value) != vector.end();
	}
	template <class T>
	inline bool contains_vector(std::vector<T>& to_search, std::vector<T>& target){
		int size = to_search.size();
		for (int i = 0; i < size; i++){
			if (std::find(target.begin(), target.end(), to_search[i]) == target.end()){
				return false;
			}
		}
		return true;
	}

	uint32_t generateHash(void *data, size_t len);

	bool rect_intersect_rect(Rect& rect_parent, Rect& look_for);
	bool line_intersect_line(Point& l1p1, Point& l1p2, Point& l2p1, Point& l2p2);
	bool line_intersect_rect(Point& l1p1, Point& l1p2, Rect& rect);
	bool rect_intersect_circle(Rect& rect_parent, Point& point, uint32_t radius);
	bool rect_contains_point(Rect& rect_parent, Point& point);
	bool circle_contains_point(Rect& rect, Point& point);
};