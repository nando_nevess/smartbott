#pragma once
#include "Util.h"
#include "HunterCore.h"

#pragma pack(push,1)
struct tile_stack_raw{
	uint32_t idk;
	uint32_t slot_count_or_cid;
	uint32_t item_id;
	uint32_t idk2[5];
};

struct tile_raw {
	uint32_t stack_count;
	uint32_t indexes[10];
	tile_stack_raw raw_stacks[10];
	uint32_t end_UINT32_MAX;
	tile_stack_raw* get_raw_item_at_index(int index){
		for (uint32_t i = 0; i < stack_count; i++)
			if (indexes[i] == index)
				return &raw_stacks[i];
		return nullptr;
	}
};
#pragma pack(pop)

class Tile{
	
public:
	tile_raw* raw_ptr;
	Coordinate coordinate;

	Tile(tile_raw* tile_raw_ptr, Coordinate coord = Coordinate());

	bool have_furniture_at_top();

	uint32_t get_top_item_id(bool ignore_creatures = false);
	
	uint32_t get_top_item_minus_index(uint32_t rindex);

	uint32_t get_top_second_item_id();

	bool have_element_field_at_top();

	bool have_element_field();

	std::shared_ptr<ConfigPathProperty> have_custom_item_cost();	

	bool is_item_id();

	std::map<int, CreatureOnBattlePtr> get_monsters();

	std::map<int, CreatureOnBattlePtr> get_npcs();

	std::map<int, CreatureOnBattlePtr> get_summons();

	std::map<int, CreatureOnBattlePtr> get_creatures();

	std::map<int, CreatureOnBattlePtr> get_players();

	bool contains_player();

	bool contains_furniture();

	bool contains_block_path_id();

	bool contains_free_path_id();

	bool contains_id(uint32_t item_id);

	bool contains_monster();

	bool contains_summon();

	bool contains_npc();

	bool contains_depot();

	bool contains_creature();
	
	bool has_thing_attribute(ThingAttr attr);

	bool contains_hole_or_step();

	uint32_t get_thing_count();

	CustomIdAttributes* find_attribute_details_type(attr_item_t attr_type);

	bool contains_item_attr(attr_item_t attr_type);

	bool is_hole();

	bool is_step();

	bool can_use_to_up();

	bool can_use_to_down();

	bool must_open_with_browse();

	bool avoid_when_targeting();

	bool is_teleport();

	bool contains_brokable();
	
	bool override_walkable();

	bool override_unwalkable();

	Coordinate get_destination();

	Coordinate get_required_pos();
};

typedef std::shared_ptr<Tile> TilePtr;

