#pragma once

#include "Util.h"
#include <vector>
#include <map>
#include <string>
#include "AddressManager.h"

#define PING_PROMEDY_COUNT 10

//declaration usage in TibiaProcess
class CharacterInfo;



class TibiaProcess{
	TibiaProcess();

	bool is_logged;
	uint32_t current_tibia_version = 0;
	uint32_t pid;
	uint32_t last_pid;
	uint32_t ping_promedy = 100;
	uint32_t window_handler;
	HANDLE process_handler;
	bool disable_hud;
	std::vector<uint32_t> last_pings;
	neutral_mutex mtx_ping_vector;
	std::vector < std::function<void(Coordinate&/*last_coord*/, Coordinate&/*new_coord*/)> >
		on_change_coordinate_callbacks;
public:

	void refresh_name();

	uint32_t get_current_tibia_version();

	void set_current_tibia_version(uint32_t version);

	void add_on_change_coordinate_callback(
		std::function < void(Coordinate&/*last_coord*/, Coordinate&/*new_coord*/) >
		callback_function);

	void on_change_coordinate(Coordinate& old_coord, Coordinate& new_coord);

	void set_disable_hud(bool temp);

	bool get_disable_hud();

	void kill_connect();

	void restore_window();

	void minimize_window();

	void hide_window();

	void show_window();

	int base_address;

	std::shared_ptr<CharacterInfo> character_info;

	bool attached;

	HWND get_window_handler();

	bool get_is_logged();

	void set_pid(uint32_t pid, int counts = 0);

	uint32_t get_pid();

	bool is_process_running();

	void update_process_handler();

	void update_base_address();

	std::string get_name_char();

	static std::shared_ptr<TibiaProcess> default_TibiaProcess;
	static std::shared_ptr<TibiaProcess> get_default();

	//Factory
	static std::shared_ptr<TibiaProcess> requestTibiaProcessByPid(uint32_t pid);

	//client memory utilitty
	uint32_t read_memory_block(uint32_t address, void * out_ptr, int length, std::vector<uint32_t>& offsets = std::vector<uint32_t>(), bool use_base = true, bool read_last = true);

	uint32_t read_memory_block(uint32_t address, void * out_ptr, int length, bool use_base);

	uint32_t read_int(uint32_t address, std::vector<uint32_t> offsets = std::vector<uint32_t>(), bool use_base = true, bool read_last = true);

	uint32_t read_int(uint32_t address, bool use_base);

	std::string read_string(uint32_t address, bool use_base);

	unsigned char read_byte(uint32_t address, std::vector<uint32_t> offsets = std::vector<uint32_t>(), bool use_base = true);

	unsigned char read_byte(uint32_t address, bool use_base);

	std::string read_string(uint32_t address, std::vector<uint32_t> offsets = std::vector<uint32_t>(), bool use_base = true);

	void write_memory_block(uint32_t remote_address, void* in_ptr, uint32_t length);

	void write_byte(uint32_t address, unsigned char value);

	void write_string(uint32_t address, std::string value);

	void write_int(uint32_t address, uint32_t value);

	uint32_t get_id_to_move();

	uint32_t get_id_to_use();

	uint32_t get_ping();

	uint32_t get_ping_promedy();

	uint32_t get_action_delay_promedy_time(uint32_t action_count);

	void wait_ping_delay(float multiplier = 2.0);

	uint32_t get_action_wait_delay(float multiplier = 2.0);

	void kill();

};

class CharacterInfo{
	TibiaProcess* owner;

public:
	std::string name;
	void set_name_char(std::string in);
	CharacterInfo();

	~CharacterInfo(){
	}

	GET_SET(TibiaProcess*, owner);

	uint32_t get_id_to_move();

	std::string read_world();

	bool is_visible();

	uint32_t get_profession();

	uint32_t get_outfit();

	uint32_t get_direction();

	uint32_t get_level_pc();

	std::string get_name_char();


#pragma region STATUS

	bool status_water();

	bool status_holly();

	bool status_frozen();

	bool status_curse();

	bool status_streng_up();

	bool status_battle_red();

	bool status_pz();

	bool status_blood();

	bool status_poison();

	bool status_fire();

	bool status_energy();

	bool status_drunk();

	bool status_utamo();

	bool status_slow();

	bool status_haste();

	bool status_battle();

	uint32_t self_ml();

	uint32_t self_mlpc();

	uint32_t exp_to_lvl(uint32_t next);

	uint32_t exp_to_next_lvl();

#pragma endregion

	uint32_t time_to_next_lvl();

#pragma region SKILL_PERCENT
	uint32_t first_pc();

	uint32_t club_pc();

	uint32_t sword_pc();

	uint32_t axe_pc();

	uint32_t distance_pc();

	uint32_t shielding_pc();

	uint32_t fishing_pc();
#pragma endregion

#pragma region SKILL_LEVEL
	uint32_t first_lvl();

	uint32_t club_lvl();

	uint32_t sword_lvl();

	uint32_t axe_lvl();

	uint32_t distance_lvl();

	uint32_t shielding_lvl();

	uint32_t fishing_lvl();

	uint32_t exp_hour();

#pragma endregion

	bool status(unsigned char entrace, int index);

	uint32_t get_x();

	uint32_t get_window_handler();

	uint32_t get_y();

	int get_z();

	Coordinate get_self_coordinate();

	Coordinate get_mixed_coordinate();

	uint32_t level();

	uint32_t experience();

	uint32_t max_hp();

	uint32_t max_mp();

	uint32_t hppc();

	uint32_t mppc();

	uint32_t hp();

	uint32_t mp();

	uint32_t soul();

	uint32_t cap();

	uint32_t stamina();

	uint32_t offline_training();

	uint32_t character_id();

	uint32_t target_red();

	bool is_follow_mode();

	bool is_walking();

	Coordinate get_displacement();

	Coordinate get_going_step();

	bool wait_stop_waking(uint32_t timeout = 2000);

	void mount();

	void unmount();

	void tryloggout();

	Json::Value parse_class_to_json(){
		Json::Value playerInfo;

		playerInfo["exp_to_next_lvl"] = exp_to_next_lvl();
		playerInfo["experience"] = experience();
		playerInfo["time_to_next_lvl"] = time_to_next_lvl();
		playerInfo["get_name_char"] = get_name_char();
		playerInfo["level"] = level();
		playerInfo["get_level_pc"] = get_level_pc();
		playerInfo["self_ml"] = self_ml();
		playerInfo["self_mlpc"] = self_mlpc();
		playerInfo["first_lvl"] = first_lvl();
		playerInfo["first_pc"] = first_pc();
		playerInfo["club_lvl"] = club_lvl();
		playerInfo["club_pc"] = club_pc();
		playerInfo["sword_lvl"] = sword_lvl();
		playerInfo["sword_pc"] = sword_pc();
		playerInfo["axe_lvl"] = axe_lvl();
		playerInfo["axe_pc"] = axe_pc();
		playerInfo["distance_lvl"] = distance_lvl();
		playerInfo["distance_pc"] = distance_pc();
		playerInfo["shielding_lvl"] = shielding_lvl();
		playerInfo["shielding_pc"] = shielding_pc();
		playerInfo["fishing_lvl"] = fishing_lvl();
		playerInfo["fishing_pc"] = fishing_pc();
		playerInfo["get_self_coordinate_x"] = get_self_coordinate().x;
		playerInfo["get_self_coordinate_y"] = get_self_coordinate().y;
		playerInfo["get_self_coordinate_z"] = get_self_coordinate().z;
		playerInfo["max_hp"] = max_hp();
		playerInfo["max_mp"] = max_mp();
		playerInfo["hp"] = hp();
		playerInfo["hppc"] = hppc();
		playerInfo["mp"] = mp();
		playerInfo["mppc"] = mppc();
		playerInfo["cap"] = cap();
		playerInfo["soul"] = soul();
		playerInfo["stamina"] = stamina();
		playerInfo["offline_training"] = offline_training();
		playerInfo["character_id"] = character_id();
		playerInfo["target_red"] = target_red();
		playerInfo["is_follow_mode"] = is_follow_mode();
		playerInfo["is_walking"] = is_walking();
		playerInfo["status_water"] = status_water();
		playerInfo["status_holly"] = status_holly();
		playerInfo["status_frozen"] = status_frozen();
		playerInfo["status_curse"] = status_curse();
		playerInfo["status_battle_red"] = target_red();
		playerInfo["status_streng_up"] = status_streng_up();
		playerInfo["status_pz"] = status_pz();
		playerInfo["status_blood"] = status_blood();
		playerInfo["status_poison"] = status_poison();
		playerInfo["status_fire"] = status_fire();
		playerInfo["status_energy"] = status_energy();
		playerInfo["status_drunk"] = status_drunk();
		playerInfo["status_slow"] = status_slow();
		playerInfo["status_utamo"] = status_utamo();
		playerInfo["status_haste"] = status_haste();
		playerInfo["status_battle"] = status_battle();

		return playerInfo;
	}
};

