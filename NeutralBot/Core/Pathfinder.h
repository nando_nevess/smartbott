#pragma once

#include "Util.h"
#include "TibiaProcess.h"
#include "Map.h"
#include <mutex>
#include <condition_variable>

//
//sqm
#define MAX_DISTANCE_TO_GO_PATHFINDER 100

class PathfinderConditionVariable{
public:
	PathfinderConditionVariable(){
		waiting = false;
		alive = true;
	}

	void set_state(pathfinder_state state, bool kill){
		std::unique_lock<std::mutex> lock(mtx);
		if (!alive)
			return;
		alive = false;
		current_state = state;
		waiting = false;
		cb.notify_all();
	}

	pathfinder_state wait_state(){
		std::unique_lock<std::mutex> lock(mtx);
		if (!alive)
			return current_state;
		waiting = true;
		cb.wait(lock);
		return current_state;
	}

	void kill(){
		std::unique_lock<std::mutex> lock(mtx);
		cb.notify_all();
		alive = false;
	}

	bool is_alive(){
		std::unique_lock<std::mutex> lock(mtx);
		return alive;
	}

	pathfinder_state get_current_state(){
		return current_state;
	}

	bool waiting;
	bool alive;
	std::mutex mtx;
	std::condition_variable cb;

	pathfinder_state current_state;
};
typedef std::shared_ptr<PathfinderConditionVariable> PathfinderConditionVariablePtr;

class CurrentPathfinderGoState{
public:
	CurrentPathfinderGoState(){
		consider_target_walkable = false;
		state = pathfinder_walk_none;
		/*
		TODO VERIFICAR COMO RESETAR STD::FUNCTION
		*/

		map_type = map_front_t;
		finalized = false;
	}

	uint32_t try_count;
	uint32_t try_delay;
	uint32_t near_distance; // specifies distance that is near
	map_type_t map_type;
	Coordinate current_destination;
	pathfinder_state state;
	PathfinderConditionVariablePtr condition_callback;
	std::function<bool()> condition_function;
	std::function<bool()> force_arrow;
	uint32_t arrow_delay;

	bool finalized;
	bool consider_target_walkable;
	void fire_callback(pathfinder_state state, bool finalize = true){
		static neutral_mutex mtx_access;
		lock_guard _lock(mtx_access);

		if (!finalized && condition_callback){
			condition_callback->set_state(state, finalize);
			if (finalize){
				finalized = true;
				condition_callback->kill();
				condition_callback = 0;
			}
		}
	}

};

class TibiaAutoWalkCoordinate{
	neutral_mutex mtx_access;
	TibiaAutoWalkCoordinate();
	bool state;
	uint32_t x;
	uint32_t y;
	int8_t z;
	boost::chrono::steady_clock::time_point last_set_state;
public:

	bool get_state();
	void set_to_go(Coordinate coord, bool state = true, int32_t timeout = 1000);
	void set_state(bool state);

	static TibiaAutoWalkCoordinate* get(){
		static TibiaAutoWalkCoordinate* mTibiaAutoWalkCoordinate = 0;
		if (!mTibiaAutoWalkCoordinate)
			mTibiaAutoWalkCoordinate = new TibiaAutoWalkCoordinate;
		return mTibiaAutoWalkCoordinate;
	}
};

struct TemporaryCoordinate{
	Coordinate coord;
	TimeChronometer registered_time;
	uint32_t timeout;

	void reset();

	bool has_expired();
};



class Pathfinder{
	uint32_t pathfinder_thread;
	TimeChronometer time;
	Pathfinder();
	std::map<pathfinder_line_t, Coordinate> pathfinder_lines_coordinates;
	std::shared_ptr<CurrentPathfinderGoState> current_go_state;

	neutral_mutex mtx_access;
	std::vector<TemporaryCoordinate> temporary_blocked_coordinates;

	void remove_temporary_blocked_coord(Coordinate& coord);

	void check_remove_expired_temporary_coordinates();

	void add_temporary_blocked_coord(Coordinate& coord);

	void verifier_going_steps();

	bool has_blocked_coord = false;
	neutral_mutex temporary_coords_lock;

	bool must_ignore_mouse_clicks();
public:

	bool has_blocked_coordinates();

	std::vector<Coordinate> get_blocked_coordinates();


	std::function<bool(Coordinate*)> callback_reachable_only_cache_map;
	bool state_tibia_auto_walk;
	Coordinate coordinate_tibia_auto_walk;

	GET_SET_MT(std::shared_ptr<CurrentPathfinderGoState>, current_go_state);

	bool walk_through_players;
	void set_coordinate_walkability(Coordinate coord, bool _walkable, map_type_t map = map_front_t, int32_t timeout = UINT32_MAX);

	bool is_reachable(Coordinate coord, map_type_t map_type = map_front_t, bool consider_target_reachable = true);

	pathfinder_retval walk_near_to(Coordinate coord_target, map_type_t map_type = map_front_t, uint32_t distance_near = 1);

	void set_go_to(Coordinate coord_target, PathfinderConditionVariablePtr condition_callback,
		uint32_t distance_near = 0, map_type_t map_type = map_front_t /*auto try use map 1 if not possible try map 2*/
		, std::function<bool()> condition_function = std::function<bool()>(), bool consider_target_walkable = false,
		uint32_t try_count = 3, uint32_t try_delay = 50, std::function<bool()> force_arrow = std::function<bool()>(), uint32_t arrow_delay = 10);
	
	pathfinder_state set_to_go_while(Coordinate coord, uint32_t consider_near = 1, map_type_t map_type = map_front_t,
		pathfinder_state condition_keep = pathfinder_walk_going, std::function<bool()> condition_function = std::function<bool()>(),
		bool consider_target_walkable = false, uint32_t try_count = 3, uint32_t try_delay = 100,
		std::function<bool()> force_arrow = std::function<bool()>(), uint32_t arrow_delay = 0);
	
	pathfinder_retval go_with_hotkey(Coordinate target_coord, bool consider_target_walkable = false, bool force_rearch = true,
		map_type_t first_try_map = map_front_t, map_type_t second_try_map = map_front_clear_t, bool use_diagonal = false);


	void path_walker();

	void cancel_all();

	static int cost_from_points(int x, int y, int x2, int y2){
		int cost_x = abs(x - x2);
		int cost_y = abs(y - y2);
		return (cost_x + cost_y);
	}

	static bool tibia_coord_to_path_coord(int &x, int &y, Coordinate& current_coordinate){
		x = 128 - (current_coordinate.x - x);
		y = 128 - (current_coordinate.y - y);
		return ((x >= 0 && x < 256) && (y >= 0 && y < 256));
	}

	static bool tibia_coord_to_path_coord(Coordinate& in_out, Coordinate& current_coordinate){
		return tibia_coord_to_path_coord(in_out.x, in_out.y, current_coordinate);
	}

	Coordinate get_nearest_coordinate_from_another(Coordinate target, map_type_t map_type = map_front_mini_t,
		bool reachable_from_us = true, bool reachable_from_dest = true,
		uint32_t range = 1, bool most_near_from_center = true, bool ingore_own_target = false);

	bool wait_reachability(Coordinate target, int32_t timeout, map_type_t map_type = map_front_mini_t,
		uint32_t check_delay_ms = 10, bool consider_target_reachable = true);

	bool is_shotable(Coordinate pos);

	static Pathfinder* get(){
		static Pathfinder* mPathfinder;
		if (!mPathfinder)
			mPathfinder = new Pathfinder;
		return mPathfinder;
	}
};

/*
QUANDO DER TEMPO MUDAR PRA DENTRO DA CLASSE E DEIXAR 100% orientado a objeto
*/
#pragma region PATHFINDER_VARIABLES

//Declare constants
#define numberPeople map_total_t
#define notfinished 0
#define notStarted 0
#define pixelsPerFrame 1
#define startingX 128
#define startingY 128
#define found 1
#define nonexistent 2
#define mapWidth 256
#define mapHeight 256
#define tileSize 1

#pragma endregion
extern uint32_t null_cost;
class PathFinderCore{
	neutral_mutex mtx_access[map_type_t::map_total_t];
	int read_path_x(map_type_t map_type, int pathLocation);
	int read_path_y(map_type_t map_type, int pathLocation);
	void read_path(int x, int y, map_type_t map_type);
	int find_path(map_type_t map_type, int x, int y, bool find_reverse_path = true,
		bool consider_target_coordinate_walkable = true, bool use_diagonal = false);
public:
	PathFinderCore();
	~PathFinderCore();

	bool is_reachable(Coordinate& coord, map_type_t map_type = map_front_t, bool consider_target_rearchable = true, bool get_cost = false, uint32_t& cost = null_cost);

	CoordinatePtr find_path_way(Coordinate& coord, map_type_t map_type = map_front_t, bool consider_target_walkable = false, uint32_t& cost = null_cost, bool use_diagonal = false);

	static PathFinderCore* get();
};