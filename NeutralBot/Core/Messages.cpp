#pragma once
#include "Messages.h"
#include <thread>
#include "..\\DevelopmentManager.h"
#include "Neutral.h"
#include "InfoCore.h"

void Messages::run_async(){

}


//look at some itens
void Messages::on_server_log_message(std::string& message){
	for (auto it = server_log_callbacks.begin(); it != server_log_callbacks.end(); it++)
		(it->second)(message);
}
//messages que agente fala
void Messages::on_tibia_message(std::string& message){
	for (auto it = tibia_messsage_callbacks.begin(); it != tibia_messsage_callbacks.end(); it++){
		(it->second)(message);

		InfoCore::get()->add_tibia_messages_vector_message(message);
	}
}

//sorry not possible
void Messages::on_action_message(std::string& message){
	for (auto it = action_message_callbacks.begin(); it != action_message_callbacks.end(); it++)
		(it->second)(message);
}



void Messages::refresh_all(){
	LOG_TIME_INIT
	refresh_header();
	LOG_TIME_SET_RESET	
	refresh_message_not_possible();
	LOG_TIME_SET_RESET
	refresh_tibia_message();
	LOG_TIME_SET_RESET
	refresh_server_log();
	LOG_TIME_SET_RESET
}

void Messages::refresh_server_log(){
	static uint32_t last_server_message_index = 0;
	//TODO nao esta correto.
	uint32_t version = TibiaProcess::get_default()->get_current_tibia_version();
	if (version >= 1094){
		uint32_t new_index = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_MESSAGE_SERVER), std::vector<uint32_t>{ 0x14 + 8});
		if (last_server_message_index == new_index)
			return;

		last_server_message_index = new_index;
		mtx_access.lock();
		on_server_log_message(
			TibiaProcess::get_default()->read_string(
			AddressManager::get()->getAddress(ADDRESS_MESSAGE_SERVER), { 0x14, 0x48, 0x4 })
			);
		mtx_access.unlock();
	}
	else if (version >= 1094){
		uint32_t new_index = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_MESSAGE_SERVER), std::vector<uint32_t>{ 0x14 + 8});
		if (last_server_message_index == new_index)
			return;

		last_server_message_index = new_index;
		mtx_access.lock();
		on_server_log_message(
			TibiaProcess::get_default()->read_string(
			AddressManager::get()->getAddress(ADDRESS_MESSAGE_SERVER), { 0x14, 0x48, 0x4 })
			);
		mtx_access.unlock();
	}
	else if (version >= 1093){
		uint32_t new_index = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_MESSAGE_SERVER), std::vector<uint32_t>{ 0x14 });
		if (last_server_message_index == new_index)
			return;

		last_server_message_index = new_index;
		mtx_access.lock();
		on_server_log_message(
			TibiaProcess::get_default()->read_string(
			AddressManager::get()->getAddress(ADDRESS_MESSAGE_SERVER), { 0x14, 0x48, 0xc })
			);
		mtx_access.unlock();
	}
	
}

void Messages::refresh_tibia_message(){
	static uint32_t now_last_id = 0;
	static uint32_t last_id = 0;

	uint32_t total_messages = 0;
	if (TibiaProcess::get_default()->get_current_tibia_version() >= 1093){
		total_messages = TibiaProcess::get_default()->read_int(
			AddressManager::get()->getAddress(ADDRESS_MESSAGE_NOT_POSSIBLE) - 4);
	}
	else{
		total_messages = header.total_messages;
	}

	if (last_id >total_messages){
		last_id = total_messages;
		now_last_id = total_messages;
	}

	uint32_t address = AddressManager::get()->getAddress(ADDRESS_MESSAGE_PLAYER) - 32;
	TibiaProcess::get_default()->read_memory_block(address, tibia_messages, sizeof(tibia_messages));

	for (int i = 0; i < 9; i++){
		tibia_message* message_info = &tibia_messages[i];

		if (message_info->sequence_number >= total_messages)
			continue;

		if (now_last_id < message_info->sequence_number)
			now_last_id = message_info->sequence_number;

		if (last_id >= message_info->sequence_number)
			continue;
		std::string result = "";
		std::string separador = " ";
		bool error = false;
		for (int y = 0; y < message_info->next; y++){
			int offset = y * 40;
			if (offset >= 600){
				error = true;
				break;
			}
			result += &(message_info->text[offset]) + separador;
		}
		if (!error){
			mtx_access.lock();
			on_tibia_message(result);
			mtx_access.unlock();
		}
	}
	last_id = now_last_id;
}

void Messages::refresh_header(){
	//587DD0
	int address = AddressManager::get()->getAddress(ADDRESS_MESSAGE_NOT_POSSIBLE) - 24;
	TibiaProcess::get_default()->read_memory_block(address, &header, sizeof(log_message), true);
}

void Messages::refresh_message_not_possible(){
	mtx_access.lock();
	static int32_t last_time_message = 0;
	static std::string last_message = "";
	//64

	int time_left = 0;

	if (TibiaProcess::get_default()->get_current_tibia_version() >= 1096){
		time_left = TibiaProcess::get_default()->read_int(
			AddressManager::get()->getAddress(ADDRESS_MESSAGE_NOT_POSSIBLE) - 8);
	}
	else if (TibiaProcess::get_default()->get_current_tibia_version() >= 1093){
		time_left = TibiaProcess::get_default()->read_int(
			AddressManager::get()->getAddress(ADDRESS_MESSAGE_NOT_POSSIBLE) - 100);
	}
	else{
		time_left = header.time_left * 100;
	}

	//100
	
	if (time_left > last_time_message){
		last_message = header.text;
		on_action_message(last_message);
	}
	else{
		std::string temp = header.text;
		if (temp != last_message){
			last_message = temp;
			on_action_message(last_message);
		}
	}
	last_time_message = time_left;
	mtx_access.unlock();
}

int Messages::add_server_log_callbacks(std::function<void(std::string)> functionMessage){
	mtx_access.lock();
	int retval = callback_id++;
	server_log_callbacks.push_back(std::pair<int, std::function<void(std::string)>>(retval, functionMessage));
	mtx_access.unlock();
	return retval;
}

int Messages::add_tibia_message_callbacks(std::function<void(std::string)> functionMessage){
	mtx_access.lock();
	int retval = callback_id++;
	tibia_messsage_callbacks.push_back(std::pair<int, std::function<void(std::string)>>(retval, functionMessage));
	mtx_access.unlock();
	return retval;
}

int Messages::add_action_message_callbacks(std::function<void(std::string)> functionMessage){
	mtx_access.lock();
	int retval = callback_id++;
	action_message_callbacks.push_back(std::pair<int, std::function<void(std::string)>>(retval, functionMessage));
	mtx_access.unlock();
	return retval;
}

void Messages::refresher(){
	while (true){
		Sleep(100);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;
		
		if (!NeutralManager::get()->get_bot_state())
			continue;

		refresh_all();
	}
}

void Messages::init(){
	mMessages = new Messages();
	std::thread([&](){ 
		MONITOR_THREAD("Messages::refresher");
		mMessages->refresher();
	}).detach();
}

void Messages::remove_action_message_callback(int id){
	mtx_access.lock();
	auto it = std::find_if(action_message_callbacks.begin(), action_message_callbacks.end(), [&](std::pair<int, std::function<void(std::string)>> info){
		return id == info.first;
	} );
	if (it != action_message_callbacks.end()){
		action_message_callbacks.erase(it);
	}
	mtx_access.unlock();
}

void Messages::remove_server_message_callback(int id){
	mtx_access.lock();
	auto it = std::find_if(server_log_callbacks.begin(), server_log_callbacks.end(), [&](std::pair<int, std::function<void(std::string)>> info){
		return id == info.first;
	});
	if (it != server_log_callbacks.end()){
		server_log_callbacks.erase(it);
	}
	mtx_access.unlock();
}

void Messages::remove_tibia_message_callback(int id){
	mtx_access.lock();
	auto it = std::find_if(tibia_messsage_callbacks.begin(), tibia_messsage_callbacks.end(), [&](std::pair<int, std::function<void(std::string)>> info){
		return id == info.first;
	});
	if (it != tibia_messsage_callbacks.end()){
		tibia_messsage_callbacks.erase(it);
	}
	mtx_access.unlock();
	return;
}

std::string Messages::get_tibia_message_where_contains(std::string str_to_find, uint32_t timeout, bool ignore_case){
	TimeChronometer timer;
	std::string message = "";
	bool received = false;

	std::function<void(std::string)> callback = [&](std::string new_message) -> void{
		if (ignore_case){
			std::string old_str = new_message;
			if (string_util::lower(old_str).find(string_util::lower(str_to_find)) != std::string::npos){
				message = new_message;
				received = true;
			}
		}
		else{
			if (new_message.find(str_to_find)){
				message = new_message;
				received = true;
			}
		}
	};

	int id = Messages::get()->add_tibia_message_callbacks(callback);

	while ((uint32_t)timer.elapsed_milliseconds() < timeout){

		if (received)
			break;

		Sleep(5);
	}

	Messages::get()->remove_tibia_message_callback(id);
	return message;
}

std::string Messages::get_server_message_where_contains(std::string str_to_find, uint32_t timeout, bool ignore_case){
	TimeChronometer timer;
	std::string message = "";
	bool received = false;

	std::function<void(std::string)> callback = [&](std::string new_message)-> void{
		if (ignore_case){
			std::string old_str = new_message;
			std::string temp = str_to_find;
			if (string_util::lower(old_str).find(string_util::lower(str_to_find)) != std::string::npos){
				message = new_message;
				received = true;
			}
		}
		else{
			if (new_message.find(str_to_find)){
				message = new_message;
				received = true;
			}
		}
	};
	int id = Messages::get()->add_server_log_callbacks(callback);
	while ((uint32_t)timer.elapsed_milliseconds() < timeout){

		if (received)
			break;

		Sleep(5);
	}
	Messages::get()->remove_server_message_callback(id);
	return message;
}

std::string Messages::get_action_message_where_contains(std::string str_to_find, uint32_t timeout, bool ignore_case){
	TimeChronometer timer;
	std::string message = "";
	bool received = false;

	std::function<void(std::string)> callback = [&](std::string new_message)-> void{
		if (ignore_case){
			std::string old_str = new_message;
			if (string_util::lower(old_str).find(string_util::lower(str_to_find)) != std::string::npos){
				message = new_message;
				received = true;
			}
		}
		else{
			if (new_message.find(str_to_find)){
				message = new_message;
				received = true;
			}
		}
	};
	int id = Messages::get()->add_action_message_callbacks(callback);
	while ((uint32_t)timer.elapsed_milliseconds() < timeout){

		if (received)
			break;

		Sleep(5);
	}

	Messages::get()->remove_action_message_callback(id);
	return message;
}

Messages* Messages::mMessages = nullptr;
