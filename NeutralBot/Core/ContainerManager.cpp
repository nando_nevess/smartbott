#pragma once
#include "ContainerManager.h"
#include "Util.h"
#include "ItemsManager.h"
#include "Interface.h"
#include "..\\DepoterManager.h"
#include "TibiaProcess.h"
#include "LooterCore.h"
#include "..\\RepoterManager.h"
#include "NpcTrade.h"
#include <thread>
#include "Time.h"
#include "Input.h"
#include "..\\DevelopmentManager.h"
#include "Actions.h"
#include "..\\LooterManager.h"

#define MAX_CONTAINER 16

#pragma region OFFSETS
#define offset_bp_id 0xc
#define offset_address_container 0x4C
#define offset_total_slot 64
#define offset_occuped_slot 68
#define offset_between_slots 0x20
#pragma endregion

void ContainerManager::update(){
	swap_containers->clear();
	actualize_all_bp();
	lock_guard _lock(mtx_access);
	std::map < uint32_t/*key index*/, ItemContainerPtr >* temp = containers;
	containers = swap_containers;
	swap_containers = temp;
}


uint32_t ItemContainer::find_item_slot(uint32_t item_id){

	for (uint32_t i = 0; i < header_info->used_slots; i++){
		if (slots[i].id == item_id)
			return i;
	}
	return UINT32_MAX;
}

std::vector<uint32_t> ItemContainer::find_items_slots(std::vector<uint32_t> items){
	std::vector<uint32_t> item_indexes;
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		if (Util::is_is_vector<uint32_t>(items, slots[i].id))
			item_indexes.push_back(i);
	}
	return item_indexes;
}

uint32_t ItemContainer::find_item_slot(uint32_t item_id, uint32_t lastIndex){
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		if (slots[i].id == item_id && ((int)i >= (int)lastIndex))
			return i;
	}
	return UINT32_MAX;
}

bool ItemContainer::up_container(){
	auto container_interface = get_this_interface();

	if (!container_interface)
		return false;

	return container_interface->up();
}

bool ItemContainer::close(){
	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < 500){
		auto this_interface = get_this_interface();
		if (this_interface){
			this_interface->close();
			Sleep(50);
		}
		else
			return true;
	}
	return false;
}

void ItemContainer::minimize(){
	auto this_interface = get_this_interface();
	if (!this_interface)
		return;
	
	if (!this_interface->is_minimized())
		this_interface->minimize();
}

bool ItemContainer::is_minimized(){
	auto this_interface = get_this_interface();
	if (this_interface)
		return this_interface->is_minimized();

	return false;
}


void ItemContainer::maximize(){
	auto this_interface = get_this_interface();
	if (!this_interface)
		return;

	if (this_interface->is_minimized())
		this_interface->maximize();
}

bool ItemContainer::set_slot_row(uint32_t slot_index, int32_t timeout){
	auto input = HighLevelVirtualInput::get_default();

	TimeChronometer chronometer;
	while (chronometer.elapsed_milliseconds() < timeout){
		Rect slot_rect = get_slot_rect_client(slot_index);
		if (!slot_rect.is_null() && slot_rect.get_height() >= 4)
			return true;

		uint32_t row = (slot_index) / 4;

		std::pair<int32_t, int32_t> height_range = { row * size_of_slot_y, row * size_of_slot_y + pixel_maximun_to_hide };

		auto this_interface = get_this_interface();
		if (!this_interface)
			return false;

		int hidden_pixel = this_interface->get_pixel_hidden_slots(is_browse_field());


		if ((int)height_range.first >= hidden_pixel - 15){
			int display_area = (int)hidden_pixel + (this_interface->heigth - 16) - 2;
			if (is_browse_field())
				display_area -= 24;


			if ((height_range.first) <= (display_area - 15))
				return true;
		}


		int count_of_scroll = (hidden_pixel - height_range.first) / size_of_eachscroll;

		auto window_dimension = Interface->get_client_dimension();
		auto this_location = this_interface->get_real_location();
		if (this_location.is_null())
			return false;
		if (count_of_scroll > 0) {
			if (!input->can_use_input())
				continue;

			input->mouse_scroll_up(window_dimension.x - pixel_offset_bp_places_x, this_location.y + 30, count_of_scroll);
		}
		else if (count_of_scroll) {
			if (!input->can_use_input())
				continue;

			input->mouse_scroll_down(window_dimension.x - pixel_offset_bp_places_x, this_location.y + 30, abs(count_of_scroll));
		}
		Sleep(50);
	}
	return false;
}
Rect ItemContainer::get_rect_client(){
	auto this_interface = get_this_interface();
	if (!this_interface)
		return Rect();
	return this_interface->get_rect_on_client_view();
}


Rect ItemContainer::get_slot_rect_client(uint32_t slot_index){
	uint32_t row = (slot_index) / 4;
	uint32_t row_index = slot_index % 4;
	struct { int32_t min; int32_t max; } height_range;
	height_range = {
		row * size_of_slot_y + 3,
		row * size_of_slot_y + size_of_slot_y - 1
	};

	auto this_interface = get_this_interface();
	if (!this_interface)
		return Rect();

	int hidden_pixel = this_interface->get_pixel_hidden_slots(is_browse_field());
	int display_area = (this_interface->heigth - (15 + 5));
	if (is_browse_field())
		display_area -= 24;

	if ((int)height_range.min < hidden_pixel - (size_of_slot_y - 5))
		return Rect();
	else if ((int)height_range.min > hidden_pixel + display_area)
		return Rect();

	Point this_coordinate = this_interface->get_real_location();
	if (this_coordinate.is_null())
		return Rect();


	Rect retval;
	retval.set_x(this_coordinate.x + row_index * size_of_slot_y + 10 /*border left*/ + 1);
	retval.set_width(size_of_slot_y - 4);

	this_coordinate.y += 16;//backpack header
	if (hidden_pixel > height_range.min)
		retval.set_y( this_coordinate.y );
	else
		retval.set_y( this_coordinate.y + (height_range.min - hidden_pixel) );


	if (height_range.max >
		(display_area + hidden_pixel)){
		retval.set_height( (display_area + hidden_pixel) - height_range.min );
	}
	else{
		int this_slot_visible = height_range.max - hidden_pixel;
		if (this_slot_visible < (height_range.max - height_range.min)){
			retval.set_height( height_range.max - hidden_pixel );
			//			retval.height -= 4;
		}
		else
			retval.set_height((height_range.max - height_range.min));
	}

	return retval;
}

bool ItemContainer::look_at_index(uint32_t slot_index, int32_t timeout){

	TimeChronometer timer;
	set_slot_row(slot_index, timeout);
	if (timer.elapsed_milliseconds() > timeout)
		return false;

	auto input = HighLevelVirtualInput::get_default();

	while (timer.elapsed_milliseconds() < timeout){
		auto current_visible_point = get_slot_rect_client(slot_index);
		if (!current_visible_point.is_null()){
			if (!input->can_use_input())
				continue;

			return input->look_pixel(Point((current_visible_point.get_x() + current_visible_point.get_width()) / 2,
				(current_visible_point.get_y() + current_visible_point.get_height()) / 2));
		}
		if (!set_slot_row(slot_index))
			return false;

		Sleep(50);
	}
	return false;
}

std::string tibia_container_header::get_caption(){
	if (len < 16)
		return std::string(caption);
	else
		return TibiaProcess::get_default()
		->read_string(*(uint32_t*)caption, false);
}

std::shared_ptr<RightPaneContainer> ItemContainer::get_this_interface(){
	if (!Interface)
		return nullptr;

	return Interface->get_container_by_type((right_pane_child_t)(right_pane_container_1 + container_index));
}

std::vector<uint32_t> ItemContainer::get_all_items_id(){
	std::vector<uint32_t> retval;
	if (header_info && slots){
		uint32_t total_used = header_info->used_slots;
		for (uint32_t i = 0; i < total_used; i++)
			retval.push_back(slots[i].id);
	}
	return retval;
}


uint32_t ItemContainer::get_reverse_lying_corpse_index(int index){
	if (!header_info)
		return UINT32_MAX;

	int total = header_info->used_slots;
	int r_index = 0;
	for (int i = total - 1; i >= 0; i--){
		if (ItemsManager::get()->is_lying_corpse(slots[i].id)){
			r_index--;
		}
		if (r_index == index)
			return i;
	}

	return UINT32_MAX;
}

bool ItemContainer::resize(){
	auto container_interface = get_this_interface();
	if (!container_interface)
		return false;
	return container_interface->resize(is_browse_field());
}

ItemContainer::ItemContainer(uint32_t index){
	container_index = index;
	header_info = 0;
	slots = 0;
}

ItemContainer::~ItemContainer(){
	if (header_info)
		delete header_info;

	if (slots)
		delete[]slots;
}

std::vector<uint32_t> ItemContainer::get_visible_container_indexes(){
	std::vector<uint32_t> retval;
	//TODO
	return retval;
}


ContainerPosition ItemContainer::get_random_container_slot(){
	/*
	TODO randomize
	auto visible_indexes = get_visible_container_indexes();
	std::vector<uint32_t> indexes_visibles_and_emptys()
	for (auto it : visible_indexes){
	if (it > header_info->used_slots)
	}
	*/
	if (header_info->total_slots == header_info->used_slots)
		return ContainerPosition();
	return ContainerPosition(container_index, header_info->total_slots - 1);
}

bool ItemContainer::is_browse_field(){
	if (2853 == header_info->bp_item_id || 12902 == header_info->bp_item_id||
		(header_info->bp_item_id >= 22797 && header_info->bp_item_id <= 22813))
		if (header_info->total_slots != 8)
			return true;

	return false;
}

uint32_t ItemContainer::get_item_count_by_id(uint32_t id){
	uint32_t retval = 0;
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		if (slots[i].id == id)
		if (!slots[i].stack_count)
			retval++;
		else
			retval += slots[i].stack_count;
	}
	return retval;
}

bool ItemContainer::is_full(){
	return header_info->used_slots == header_info->total_slots;
}

ContainerPositionPtr ItemContainer::get_container_position_to_add_item(){
	if (is_full())
		return 0;
	auto retval = ContainerPositionPtr(new ContainerPosition);
	retval->container_index = container_index;
	retval->slot_index = header_info->total_slots - 1;
	return retval;
}

ContainerManager::ContainerManager(){
	swap_containers = new std::map < uint32_t/*key index*/, ItemContainerPtr >;
	containers = new std::map < uint32_t/*key index*/, ItemContainerPtr >;
	(new std::thread(&ContainerManager::refresh_thread , this))->detach();
}

std::shared_ptr<FunctionCallbackAuto> ContainerManager::set_auto_critical_refresh(){
	add_critical_refresh_count();
	return std::shared_ptr<FunctionCallbackAuto>(new FunctionCallbackAuto(
		[&](){
		this->sub_critical_refresh_count();
	}
	));
}

bool ContainerManager::is_required_critical_refresh(){
	return critical_refresh_count != 0;
}

void ContainerManager::add_critical_refresh_count(){
	mtx_critical_refresh.lock();
	critical_refresh_count++;
	mtx_critical_refresh.unlock();
}

void ContainerManager::sub_critical_refresh_count(){
	mtx_critical_refresh.lock();
	critical_refresh_count--;
	mtx_critical_refresh.unlock();
}

std::vector<uint32_t> ContainerManager::get_all_items_is_in_open_containers(){
	std::vector<uint32_t> items_id;
	mtx_access.lock();
	for (auto container = containers->begin(); container != containers->end(); container++){
		std::vector<uint32_t> items = container->second.get()->get_all_items_id();
		items_id.insert(items_id.end(), items.begin(), items.end());
	}
	mtx_access.unlock();
	return items_id;
}

std::vector<uint32_t> ContainerManager::get_all_containers_id_opened(){
	std::vector<uint32_t> containers_id;
	mtx_access.lock();
	for (auto container = containers->begin(); container != containers->end(); container++)
		containers_id.push_back(container->second.get()->get_id());
	mtx_access.unlock();
	return containers_id;
}

bool ContainerManager::wait_money_change(uint32_t timeout){
	auto critical_handler = set_auto_critical_refresh();
	uint32_t current_value = get_total_money();

	return TimeChronometer::wait_condition(
		[&](){
		return current_value != get_total_money();
	}
	);
}

int ContainerManager::get_containers_open(){
	return get_containers().size();
}

void ContainerManager::minimize_all_container(){
	mtx_access.lock();
	for (auto container = containers->begin(); container != containers->end(); container++){
		if (!container->second)
			continue;

		container->second->minimize();
	}
	mtx_access.unlock();
}
void ContainerManager::maximize_all_container(){
	mtx_access.lock();
	for (auto container = containers->begin(); container != containers->end(); container++){
		if (!container->second)
			continue;

		container->second->maximize();
	}
	mtx_access.unlock();
}


void ContainerManager::refresh_thread(){
	MONITOR_THREAD(__FUNCTION__);
	TimeChronometer timer;

	while (true) {
		Sleep(5);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;

		update();
		timer.reset();

		uint32_t elapsed;

		while ((elapsed = timer.elapsed_milliseconds()) < 50){
			if (elapsed > 5 && this->is_required_critical_refresh()){
				break;
			}
			Sleep(1);
		}
		refresh_count++;
	}
}

std::map<uint32_t, ItemContainerPtr> ContainerManager::get_containers(){
	lock_guard _lock(mtx_access);
	return *containers;
}

std::map<uint32_t, ItemContainerPtr> ContainerManager::get_containers_not_in(std::vector<uint32_t>& containers_id){
	std::map<uint32_t, ItemContainerPtr> containers_copy = get_containers();
	for (auto it = containers_copy.begin(); it != containers_copy.end();){
		if (std::find(containers_id.begin(), containers_id.end(), it->second->get_id()) == containers_id.end()){
			it++;
		}
		else{
			it = containers_copy.erase(it);
		}
	}
	return containers_copy;
}

std::map<uint32_t, ItemContainerPtr> ContainerManager::get_containers_in(std::vector<uint32_t>& containers_id){
	std::map<uint32_t, ItemContainerPtr> containers_copy = get_containers();
	for (auto it = containers_copy.begin(); it != containers_copy.end();){
		if (std::find(containers_id.begin(), containers_id.end(), it->second->get_id()) != containers_id.end()){
			it++;
		}
		else{
			it = containers_copy.erase(it);
		}
	}
	return containers_copy;
}

uint32_t ContainerManager::get_item_count_on_container_index(uint32_t item_id, uint32_t index){
	lock_guard _lock(mtx_access);
	auto it = containers->find(index);
	if (it == containers->end())
		return 0;
	return it->second->get_item_count_by_id(item_id);
}

std::vector<ItemContainerPtr> ContainerManager::get_browse_fields(){
	lock_guard _lock(mtx_access);
	std::vector<ItemContainerPtr> retval;
	for (auto it = containers->begin(); it != containers->end(); it++){
		if (it->second->is_browse_field())
			retval.push_back(it->second);
	}
	return retval;
}

ItemContainerPtr ContainerManager::get_browse_field(){
	lock_guard _lock(mtx_access);
	for (auto it = containers->begin(); it != containers->end(); it++){
		if (it->second->is_browse_field())
			return it->second;
	}
	return nullptr;
}

ItemContainerPtr ContainerManager::get_container_by_id(uint32_t id){
	lock_guard _lock(mtx_access);
	for (auto it = containers->begin(); it != containers->end(); it++){
		if (it->second->get_id() == id){
			return it->second;
		}
	}
	return 0;
}

std::vector<ItemContainerPtr> ContainerManager::get_containers_by_id(uint32_t id){
	lock_guard _lock(mtx_access);
	std::vector<ItemContainerPtr> containers_retval;
	for (auto it = containers->begin(); it != containers->end(); it++)
		if (it->second->get_id() == id){
			containers_retval.push_back(it->second);
		}
	return containers_retval;
}

std::vector<ItemContainerPtr> ContainerManager::get_containers_by_ids(std::vector<uint32_t>& ids){
	lock_guard _lock(mtx_access);
	std::vector<ItemContainerPtr> containers_retval;
	for (auto it = containers->begin(); it != containers->end(); it++)
		if (std::find(ids.begin(), ids.end(), it->second->get_id()) != ids.end()){
			containers_retval.push_back(it->second);
		}
	return containers_retval;
}

ItemContainerPtr ContainerManager::get_container_by_index(uint32_t container_index){
	lock_guard _lock(mtx_access);
	auto res = containers->find(container_index);
	if (res == containers->end())
		return 0;
	return res->second;
}

ItemContainerPtr ContainerManager::get_container_by_interface_index(uint32_t cointainer_index){
	mtx_access.lock();
	std::map<uint32_t/*key index*/, ItemContainerPtr>  res = *containers;
	mtx_access.unlock();
	/*
	for (auto container : res){
	container.second->in
	}*/
	return nullptr;
}

ItemContainerPtr ContainerManager::get_container_by_caption(std::string caption){
	std::transform(caption.begin(), caption.end(), caption.begin(), ::tolower);
	lock_guard _lock(mtx_access);
	for (auto it = containers->begin(); it != containers->end(); it++)
		if (_stricmp(&it->second->get_caption()[0] ,&caption[0]) == 0)
			return it->second;

	return 0;
}

ItemContainerPtr ContainerManager::get_locker_container(){
	std::vector<uint32_t> depot_tiles_ids = ItemsManager::get()->get_depot_ids();
	lock_guard _lock(mtx_access);
	for (auto depot_id = depot_tiles_ids.begin(); depot_id != depot_tiles_ids.end(); depot_id++)
	for (auto it = containers->begin(); it != containers->end(); it++)
	if (it->second->get_id() == *depot_id)
		return it->second;

	return 0;
}

ItemContainerPtr ContainerManager::get_depot_chest_container(){
	int depot_chest_id = 3502;
	ItemContainerPtr container = ContainerManager::get_container_by_id(depot_chest_id);

	if (container)
		return container;

	return 0;
}

ItemContainerPtr ContainerManager::get_depot_box_container(uint32_t item_id){
	int depot_box_id = item_id;
	ItemContainerPtr container = ContainerManager::get_container_by_id(depot_box_id);

	if (container)
		return container;

	return 0;
}

bool ContainerManager::is_container_id_completely_full(uint32_t id){
	auto it = get_container_by_id(id);
	if (it)
		return it->is_full();
	return true;
}

bool ContainerManager::is_container_index_completely_full(uint32_t index){
	lock_guard _lock(mtx_access);
	auto it = containers->find(index);
	if (it == containers->end())
		return true;
	return it->second->is_full();
}

bool ContainerManager::is_some_container_completely_full(){
	lock_guard _lock(mtx_access);
	for (auto it = containers->begin(); it != containers->end(); it++){
		if (it->second->is_full())
			return true;
	}
	return false;
}

ContainerPositionPtr ContainerManager::find_item_in_vector(std::vector<uint32_t> item_ids){
	ContainerPositionPtr retval(new ContainerPosition);
	ContainerPosition pos;
	for (auto item_id : item_ids){
		pos = find_item(item_id);
		if (!pos.is_null()){
			*retval = pos;
			break;
		}
	}
	return retval;
}

int ContainerManager::find_item_id_from_vector(std::vector<uint32_t> item_ids){
	for (auto item_id : item_ids) {
		if (!find_item(item_id).is_null())
			return item_id;
	}
	return 0;
}

ContainerPositionPtr ContainerManager::find_pick_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_pick());
}

ContainerPositionPtr ContainerManager::find_rope_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_rope());
}

ContainerPositionPtr ContainerManager::find_shovel_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_shovel());
}

ContainerPositionPtr ContainerManager::find_machete_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_machete());
}

ContainerPositionPtr ContainerManager::find_kitchen_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_kitchen());
}

ContainerPositionPtr ContainerManager::find_knife_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_knife());
}

ContainerPositionPtr ContainerManager::find_spoon_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_spoon());
}

ContainerPositionPtr ContainerManager::find_croowbar_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_crowbar());
}

ContainerPositionPtr ContainerManager::find_scythe_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_scythe());
}

ContainerPositionPtr ContainerManager::find_sickle_item(){
	return find_item_in_vector(ItemsManager::get()->get_items_as_sickle());
}

bool ContainerManager::is_container_index_full(uint32_t index){
	//TODO
	return false;
}

uint32_t ContainerManager::get_total_money(){
	int count_money = get_item_count(3031) + 10000 * get_item_count(3043) + 100 * get_item_count(3035);
	return count_money;
}

/*
retrieves container index, and slot index of found item
return false if not found item on opened containers
*/
ContainerPosition ContainerManager::find_item(uint32_t item_id, uint32_t container_id){
	lock_guard _lock(mtx_access);
	if (container_id != UINT32_MAX){
		/*
		find by bp id
		*/
		for (auto it = containers->begin(); it != containers->end(); it++){
			if (it->second->get_id() != container_id)
				continue;

			uint32_t slot_index_found = it->second->find_item_slot(item_id);
			if (slot_index_found != UINT32_MAX)
				return ContainerPosition(it->first, slot_index_found);
		}
	}
	/*
	ignore bp id
	*/
	for (auto it = containers->begin(); it != containers->end(); it++){
		uint32_t slot_index_found = it->second->find_item_slot(item_id);
		if (slot_index_found != UINT32_MAX)
			return ContainerPosition(it->first, slot_index_found);
	}
	return ContainerPosition();
}

uint32_t ContainerManager::get_item_count(uint32_t item_id){
	lock_guard _lock(mtx_access);
	uint32_t item_count = 0;
	for (auto it = containers->begin(); it != containers->end(); it++)
		item_count += it->second->get_item_count_by_id(item_id);
	return item_count;
}

ContainerManager* ContainerManager::get(){
	static ContainerManager* mContainerManager = nullptr;
	if (!mContainerManager){
		mContainerManager = new ContainerManager();
	}

	return mContainerManager;
}

void ContainerManager::actualize_bp_info(uint32_t pointer, uint32_t container_num){
	if (!pointer)
		return;

	auto container_it = swap_containers->find(container_num);
	if (container_it != swap_containers->end())
		return;

	if (container_num > 100)
		return;
	
	ItemContainerPtr container = ItemContainerPtr(new ItemContainer(container_num));
	container->header_info = new tibia_container_header;

	if (TibiaProcess::get_default()->read_memory_block(pointer, container->header_info, sizeof(tibia_container_header), false)
		!= sizeof(tibia_container_header)){
		return;
	}

	if (!container->header_info->used_slots){
		swap_containers->insert(std::pair<uint32_t, ItemContainerPtr>(container_num, container));
		return;
	}

	if (container->header_info->used_slots > 200)
		return;
			
	tibia_slot* slots = new tibia_slot[container->header_info->used_slots];
	if (!slots){
		return;
	}
		
	int to_read = sizeof(tibia_slot)* container->header_info->used_slots;

	if (TibiaProcess::get_default()->read_memory_block(container->header_info->body_address, slots, to_read, false)
		!= to_read){
		delete slots;
		container->slots = 0;
		return;
	}

	container->slots = slots;
	container->memory = pointer;
	swap_containers->insert(std::pair<uint32_t, ItemContainerPtr>(container_num, container));
}


void ContainerManager::SearchContainerInThree(uint32_t address, uint32_t dept_level, std::vector<uint32_t>* found_addresses){
	if (dept_level > 15){
		return;
	}
	dept_level++;
	found_addresses->push_back(address);

	uint32_t info_address = _read_int(address + 0x14, false);
	uint32_t bp_num = _read_int(address + 0x10, false);
	actualize_bp_info(info_address, bp_num);
	uint32_t addresses[3];
	TibiaProcess::get_default()->read_memory_block(address, &addresses, 3 * sizeof(uint32_t), false);
	for (int i = 0; i < 3; i++){
		if (std::find(found_addresses->begin(), found_addresses->end(), addresses[i]) == found_addresses->end()){
			SearchContainerInThree(addresses[i], dept_level, found_addresses);
		}
	}
	dept_level--;
}

void ContainerManager::actualize_all_bp(){
	
	uint32_t base_pointer_address = TibiaProcess::get_default()->read_int(AddressManager::get()->getAddress(ADDRESS_CONTAINER_BASE_NEW));
	base_pointer_address = TibiaProcess::get_default()->read_int(base_pointer_address + 4, false);
	base_pointer_address = TibiaProcess::get_default()->read_int(base_pointer_address, false);
	
	std::vector<uint32_t> addresses;
	SearchContainerInThree(base_pointer_address, 0, &addresses);

	auto interface_containers = Interface->get_containers();
	for (auto interface_container : interface_containers){
		int index = interface_container->type - 0x40;
		if (index < 0)
			continue;

		auto container = swap_containers->find(index);
		if (container != swap_containers->end())
			container->second->is_open = true;
	}
	//refresh caption
	for (auto it = swap_containers->begin(); it != swap_containers->end(); it++){
		it->second->set_caption(it->second->header_info->get_caption());
	}
}

bool ContainerManager::wait_open_trade(int32_t timeout){
	auto auto_critical_handler = set_auto_critical_refresh();
	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < timeout){
		if (NpcTrade::get()->is_open())
			return true;
		Sleep(5);
	}

	return false;
}

int32_t ContainerManager::wait_open_container_count_change(int32_t timeout,
	std::function<bool()> or_callback){
	auto auto_critical_handler = set_auto_critical_refresh();
	if (!timeout)
		timeout = TibiaProcess::get_default()->get_action_delay_promedy_time(3);
	int32_t current_size = ContainerManager::get()->get_containers().size();
	TimeChronometer chronometer;
	while (chronometer.elapsed_milliseconds() < timeout){
		if (or_callback && or_callback()){
			return 1;
		}
		int32_t diff = (int32_t)ContainerManager::get()->get_containers().size() - current_size;
		if (diff)
			return diff;		

		Sleep(20);
	}
	return 0;
}

bool ContainerManager::wait_container_id_close(uint32_t container_id, int32_t timeout){
	TimeChronometer chronometer;
	auto auto_critical_handler = set_auto_critical_refresh();
	while (chronometer.elapsed_milliseconds() < timeout){
		if (!ContainerManager::get()->get_container_by_id(container_id))
			return true;
		Sleep(5);
	}
	return false;
}

bool ContainerManager::wait_container_index_close(uint32_t container_index, int32_t timeout){
	TimeChronometer chronometer;
	auto auto_critical_handler = set_auto_critical_refresh();
	while (chronometer.elapsed_milliseconds() < timeout){
		if (!ContainerManager::get()->get_container_by_index(container_index))
			return true;
		Sleep(5);
	}
	return false;
}

bool ContainerManager::wait_browse_field_close(int32_t timeout){
	TimeChronometer chronometer;
	auto auto_critical_handler = set_auto_critical_refresh();
	while (chronometer.elapsed_milliseconds() < timeout){
		if (!get_browse_field())
			return true;
		Sleep(5);
	}
	return false;
}

bool ItemContainer::is_creature_corpses(){
	return ItemsManager::get()->is_lying_corpse(get_id());
}


bool ContainerManager::close_containers_by_index(uint32_t index) {
	auto container = get_container_by_index(index);
	if (!container)
		return false;
	return container->close();
}


bool ContainerManager::close_all_containers() {
	auto containers_list = get_containers();
	for (auto container = containers_list.rbegin(); container != containers_list.rend(); container++) {
		for (int count = 0; count < 3; count++) {
			int containerId = container->second->get_id();
			container->second->close();

			if (!get_container_by_id(containerId))
				break;
		}
	}
	return true;
}

bool ContainerManager::close_all_browse_fields(){
	//	TimeChronometer timer;
	//	while (timer.elapsed_milliseconds() < 1000){
	auto containers_copy = get_containers();
	auto browse_field = get_browse_field();
	if (!browse_field)
		return true;
	browse_field->close();
	//	}
	return false;
}
bool ContainerManager::close_all_creature_containers(){
	TimeChronometer timer;
	while (timer.elapsed_milliseconds() < 1000){
		auto containers_copy = get_containers();
		bool some_close = false;
		for (auto container : containers_copy)
		if (container.second->is_creature_corpses())
			container.second->close();
		if (!some_close)
			return true;
	}
	return false;
}

bool ContainerManager::resize_all_containers(){
	TimeChronometer timer;
	auto containers_copy = get_containers();
	for (auto container : containers_copy)
		container.second->resize();

	return true;
}

bool ContainerManager::resize_all_browse_fields(){
	TimeChronometer timer;
	auto containers_copy = get_containers();
	for (auto container : containers_copy)
	if (container.second->is_browse_field())
		container.second->resize();
	return true;
}

bool ContainerManager::resize_all_creatures_containers(){
	TimeChronometer timer;
	auto containers_copy = get_containers();
	for (auto container : containers_copy)
	if (container.second->is_creature_corpses())
		container.second->resize();
	return true;
}

bool ContainerManager::wait_container_id_open(uint32_t container_id, int32_t timeout, std::function<bool()> or_callback){
	TimeChronometer timer;
	auto auto_critical_handler = set_auto_critical_refresh();
	while (timer.elapsed_milliseconds() < timeout)
		if (get_container_by_id(container_id) || (or_callback && or_callback()))
			return true;

	return false;
}

bool ContainerManager::wait_container_browse_field(int32_t timeout, std::function<bool()> or_callback){
	TimeChronometer timer;
	auto auto_critical_handler = set_auto_critical_refresh();
	while (timer.elapsed_milliseconds() < timeout)
		if (get_browse_field() || (or_callback && or_callback()))
			return true;
	return false;
}

bool ContainerManager::wait_depot_locker_open(int32_t timeout, std::function<bool()> or_callback){
	TimeChronometer timer;
	auto auto_critical_handler = set_auto_critical_refresh();
	while (timer.elapsed_milliseconds() < timeout)
		if (get_depot_locker_container() || (or_callback && or_callback()))
			return true;
	return false;
}

std::vector<uint32_t> ContainerManager::get_open_container_indexes(){
	std::vector<uint32_t> retval;
	mtx_access.lock();
	for (auto begin = containers->begin(); begin != containers->end(); begin++){
		retval.push_back(begin->first);
	}
	mtx_access.unlock();
	return retval;
}

ItemContainerPtr ContainerManager::get_depot_locker_container(){
	std::vector<uint32_t> depot_tiles_ids = ItemsManager::get()->get_depot_ids();
	for (auto depot_id = depot_tiles_ids.begin(); depot_id != depot_tiles_ids.end(); depot_id++) {
		ItemContainerPtr container = ContainerManager::get_container_by_id(*depot_id);
		if (container)
			return container;
	}
	return nullptr;
}

bool ContainerManager::compare_containers(std::map<uint32_t, ItemContainerPtr>& containers/*get ref*/, bool compare_items){
	//lets copy to avoid mtx_lock
	std::map<uint32_t, ItemContainerPtr> cache = get_containers();
	for (auto container_it : containers){
		auto cache_container = cache.find(container_it.first);
		if (cache_container == cache.end())
			return false;
		if (container_it.second->equals(cache_container->second.get(), compare_items))
			return false;
	}
	return true;
}

bool ItemContainer::wait_change_content(int32_t timeout){
	TimeChronometer timer;
	//� ASSIM MSM Q FUNCIONA A FUN��O???
	//SEGURAR O CACHE NAO DEVERIA SER ANTES DO LOOP?	
	auto auto_critical_handler = ContainerManager::get()->set_auto_critical_refresh();
	while (timer.elapsed_milliseconds() < timeout){
		auto cache_container = ContainerManager::get()->get_container_by_index(this->get_container_index());

		if (!cache_container)
			continue;

		if (!cache_container->equals(this, true))
			return true;
		Sleep(5);
	}
	return false;
}

bool ItemContainer::compare_slots(tibia_slot* other_slots, uint32_t other_count){
	for (int slot_index = 0; slot_index != other_count; slot_index++){
		if (other_slots[slot_index] != slots[slot_index])
			return false;
	}
	return true;
}

action_retval_t ItemContainer::open_next(uint32_t timeout){
	TimeChronometer timer;
	while ((uint32_t)timer.elapsed_milliseconds() < timeout && get_this_interface()){
		uint32_t next_slot_index = find_item_slot(get_id());
		if (next_slot_index == UINT32_MAX)
			return action_retval_t::action_retval_container_not_found;

		if (BaseActions::use_item_on_to_container(ContainerPosition(get_container_index(), next_slot_index), get_id())){
			Sleep(TibiaProcess::get_default()->get_action_wait_delay());
			return action_retval_t::action_retval_success;
		}
	}
	return action_retval_t::action_retval_timeout;
}

bool ItemContainer::equals(ItemContainer* other, bool compare_content){
	if (header_info->body_address != other->header_info->body_address)
		return false;

	if (header_info->bp_item_id != other->header_info->bp_item_id)
		return false;

	if (header_info->total_slots != other->header_info->total_slots)
		return false;

	if (header_info->used_slots != other->header_info->used_slots)
		return false;

	if (compare_content){
		if (slots){
			//TODO - melhorar 
			/*if (this->compare_slots(other->slots, header_info->used_slots))
				return false;*/
			if (*slots != *other->slots)
				return false;
		}
	}
	return true;
}

uint32_t ContainerManager::get_food_count(){
	uint32_t retval = 0;

	mtx_access.lock();
	std::map<uint32_t/*key index*/, ItemContainerPtr> containers_copy = *containers;
	mtx_access.unlock();

	for (auto container : containers_copy){
		retval += container.second.get()->get_food_count();
	}
	return retval;
}

bool ContainerManager::reorganize_container(){
	if (!RepoterManager::get()->get_reorganize_containers())
		return false;

	std::map<uint32_t, ItemContainerPtr> containers_open = get_containers();
	if (!containers_open.size())
		return false;

	for (auto container : containers_open){
		std::vector<uint32_t> items_ids = container.second->get_all_items_id();
		if (!items_ids.size())
			continue;

		for (auto item_id : items_ids){
			LooterContainerPtr container_destiny_id = LooterManager::get()->get_container_owns_by_item_id(item_id);
			if (!container_destiny_id)
				continue;

			ItemContainerPtr destinyContainer = get_container_by_id(container_destiny_id->get_container_id());
			if (!destinyContainer)
				continue;

			ItemContainerPtr sourceContainer = ContainerManager::get()->get_container_by_id(container.second->get_id());
			if (!sourceContainer)
				continue;

			if (container_destiny_id->get_container_id() == container.second->get_id())
				continue;

			ContainerPosition sourceBackpack(container.second->get_container_index(), container.second->find_item_slot(item_id));
			ContainerPosition destinyBackpack(destinyContainer->get_container_index(), destinyContainer->get_max_slots() - 1);

			if (sourceBackpack.is_null() || destinyBackpack.is_null())
				continue;
			
			uint32_t itemCount = container.second->get_item_count_by_id(item_id);
			
			TimeChronometer time;
			while (time.elapsed_milliseconds() <= 3000){

				if (destinyContainer->is_full())
					destinyContainer->open_next();

				destinyContainer = ContainerManager::get()->get_container_by_id(destinyContainer->get_id());
				if (!destinyContainer)
					break;

				sourceContainer = ContainerManager::get()->get_container_by_id(container.second->get_id());
				if (!sourceContainer)
					break;

				sourceBackpack = ContainerPosition(sourceContainer->get_container_index(), sourceContainer->find_item_slot(item_id));
				if (sourceBackpack.is_null())
					break;

				uint32_t itemCountMove = sourceContainer->get_item_count_by_id(item_id);
				if (itemCountMove <= 0)
					break;

				if (destinyContainer->is_full())
					break;

				Actions::move_item_container_to_container(sourceBackpack, destinyBackpack, itemCountMove, item_id);
				
				TibiaProcess::get_default()->wait_ping_delay(1.0);

				LooterCore::get()->refresh_items_in_map_looted();			
			}
		}
	}
	return true;
}

std::vector<ContainerPosition> ContainerManager::get_container_positions_where(uint32_t container_id, uint32_t item_id, uint8_t count_min, uint8_t count_max){
	std::vector<ContainerPosition> retval;

	mtx_access.lock();
	std::map<uint32_t/*key index*/, ItemContainerPtr> containers_copy = *containers;
	mtx_access.unlock();

	for (auto container : containers_copy){
		if (container_id == 0xffffffff || container_id == 0 || container.second.get()->get_id() == container_id){
			std::vector<uint32_t> indexes = container.second.get()->get_slot_indexes_where(item_id, count_min, count_max);
			for (auto index : indexes){
				retval.push_back(ContainerPosition(container.first, index));
			}
		}
	}
	return retval;
}


std::vector<uint32_t> ItemContainer::get_slot_indexes_where(uint32_t item_id, uint8_t min_count, uint8_t max_count){
	std::vector<uint32_t> retval;
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		if (item_id == 0xffffffff || slots[i].id == item_id){
			if ((min_count == 0 || min_count <= slots[i].stack_count) && (max_count == 100 || max_count >= slots[i].stack_count)){
				retval.push_back(i);
			}
		}
	}
	return retval;
}



uint32_t ItemContainer::get_food_count(){
	uint32_t retval = 0;
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		if (!ItemsManager::get()->is_food_item_id(slots[i].id))
			continue;
		retval += slots[i].stack_count;
	}
	return retval;
}
uint32_t ItemContainer::get_container_index(){
	return container_index;
}
std::string ItemContainer::get_caption(){
	return caption;
}

void ItemContainer::set_caption(std::string caption){
	this->caption = caption;
}
uint32_t ItemContainer::get_id(){
	if (!header_info)
		return UINT32_MAX;
	return header_info->bp_item_id;
}

uint32_t ItemContainer::get_slot_count(uint32_t index){
	if (header_info->used_slots)
		return slots[index].stack_count;
	return UINT32_MAX;
}

uint32_t ItemContainer::get_slot_id(uint32_t index){
	if (index < header_info->used_slots)
		return slots[index].id;
	return UINT32_MAX;
}

uint32_t ItemContainer::get_max_slots(){
	return header_info->total_slots;
}

uint32_t ItemContainer::get_not_container_slot(){
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		std::shared_ptr<ThingType> item_t = ItemsManager::get()->getItemTypeById(slots[i].id);
		if (!item_t)
			return i;

		if (!item_t->m_attribs.has(ThingAttr::ThingAttrContainer))
			return i;
	}
	return UINT32_MAX;
}

std::vector<uint32_t> ItemContainer::get_visible_slot_indexes(){
	//TODO MELHORAR ALGORITMO TA MTO PESADO
	std::vector<uint32_t> retval;
	for (uint32_t i = 0; i < header_info->used_slots; i++){
		auto current_visible_point = get_slot_rect_client(i);
		if (!current_visible_point.is_null()){
			retval.push_back(i);
		}
	}
	return retval;
}

uint32_t ItemContainer::get_slot_index_to_move_item(uint32_t item_id){
	if (is_full()){
		if (item_id == UINT32_MAX)
			return UINT32_MAX;

		std::vector<uint32_t> slots_now = this->get_slot_indexes_where(item_id, 0, 99);
		if (!slots_now.size())
			return UINT32_MAX;
		

		std::vector<uint32_t> visible_indexes = get_visible_slot_indexes();
		for (auto it : visible_indexes){
			std::shared_ptr<ThingType> item_t = ItemsManager::get()->getItemTypeById(slots[it].id);
			if (!item_t)
				return it;

			if (!item_t->m_attribs.has(ThingAttr::ThingAttrContainer))
				return it;
		}
		return get_not_container_slot();
	}
	else
		return get_max_slots() - 1;
}

uint32_t ItemContainer::get_used_slots(){
	return header_info->used_slots;
}

tibia_slot ItemContainer::get_slot_at(uint32_t index){
	if (!header_info->used_slots)
		return tibia_slot();
	return slots[index];
}

bool ItemContainer::advance_page(){
	return false;
	//IMPLEMENT
}

bool ItemContainer::back_page(){
	return false;
	//IMPLEMENT
}


uint32_t ItemContainer::current_page_index(){
	MessageBox(0,&("method " + std::string(__FUNCTION__) + " not implemented")[0],0,MB_OK);
	return 0;
	//IMPLEMENT
}

bool ItemContainer::has_next(){
	uint32_t current_id = get_id();
	if (current_id == UINT32_MAX)
		return false;
	return find_item_slot(current_id) != UINT32_MAX;
}
