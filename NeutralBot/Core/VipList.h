
#include "Util.h"
#include "TibiaProcess.h"

#pragma region VIP_LIST_OFFSETS

#define OFFSET_VIP_NAME		0x18
#define OFFSET_VIP_TYPE		0x14
#define OFFSET_VIP_NOTIFY	0x15
#define OFFSET_VIP_TEXT		0x34

#pragma endregion
#pragma pack(push,1)
struct VipPlayer{
	char idk_buffer[12];
	unsigned char type;
	unsigned char notify_at_login;
	uint16_t idk;
	uint32_t idk2;
	uint32_t idk3;
	uint32_t address_name;
	char idk_buffer2[16];
	uint32_t name_len;
	uint32_t address_desc;
	
	std::string get_name();
	std::string get_desc();
};

#pragma pack(pop)



class VipList{
	neutral_mutex mtx_access;

	std::vector<std::shared_ptr<VipPlayer>>* mVipPlayers;
	std::vector<std::shared_ptr<VipPlayer>>* mVipPlayers_swap;

	void swap();
	void update();

public:
	VipList();

	std::shared_ptr<VipPlayer> get_vip_by_player_name(std::string& name);

	std::vector<std::shared_ptr<VipPlayer>> get_vip_players();

	static VipList* get(){
		static VipList* mVipList = nullptr;
		if (!mVipList)
			mVipList = new VipList;
		return mVipList;
	}
};


