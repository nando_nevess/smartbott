#include <stdint.h>
#include <string>
#include <vector>
#include "Core\\Util.h"

enum special_area_element_t{
	sa_element_area,
	sa_element_point,
	sa_element_wall, 
	sa_element_total, 
	sa_element_any, 
	sa_element_none 
};

enum special_area_activation_t{ 
	sa_activation_inside_area,
	sa_activation_on_target,
	sa_activation_on_hunter_creature,
	sa_activation_on_creature,
	sa_activation_on_player,
	sa_activation_on_lured_reached, 
	sa_activation_on_luring,
	sa_activation_on_lure_not_reachable,
	sa_activation_manual_lua,
	special_area_activation_total
};
enum special_area_event_t{
	special_area_event_none,
	special_area_event_enter_area,
	special_area_event_leave_area,
	special_area_event_total
};

#pragma pack(push,1)
struct SAWalkDificult{
	uint32_t range;
	uint32_t value;
	float decay;
	Json::Value ClassToJson();

	static SAWalkDificult* JsonToClass(Json::Value& value);
};

struct SAAvoidance{
	uint32_t range;
	uint32_t value;
	float decay;
	Json::Value ClassToJson();

	static SAAvoidance* JsonToClass(Json::Value& value);
};

struct SAActivationTrigger{
	special_area_activation_t activation_trigger;

public:
	DEFAULT_GET_SET(special_area_activation_t, activation_trigger);

};

struct SAEvent{
	bool last_inside_area = false;
	special_area_event_t event_type = (special_area_event_t)0;
	std::string event_code = "";

public:
	SAEvent(){
		event_type = (special_area_event_t)0;
		event_code = "";
	}

	DEFAULT_GET_SET(special_area_event_t,event_type);

	void set_event_code(std::string in){
		event_code = in;
	}
	std::string get_event_code(){
		std::string temp = "none";
		try{
			temp = event_code;
		}
		catch (...){
			temp = "";
		}
		return temp;
	}
};

class SABaseElement{
protected:
	neutral_mutex mtx_access;
	special_area_element_t element_t;
	special_area_activation_t activation_trigger;
	std::string name;
	std::vector<SABaseElement*> childrens;
	SABaseElement* owner;

	bool block_walk;
	void on_delete();
	SAAvoidance* avoidance_ptr;
	SAWalkDificult* walk_dificult_ptr;
	bool state = true;
	bool has_event = false;
	bool bool_is_in_area = false;

	std::vector<std::shared_ptr<SAActivationTrigger>> vector_activation_trigger;
	std::vector<std::shared_ptr<SAEvent>> vector_event;

public:

	std::shared_ptr<SAActivationTrigger> requset_new_activation_trigger(){
		std::shared_ptr<SAActivationTrigger> newTrigger = std::shared_ptr<SAActivationTrigger>(new SAActivationTrigger);
		newTrigger->activation_trigger = (special_area_activation_t)0;

		vector_activation_trigger.push_back(newTrigger);
		return newTrigger;
	}
	void creat_events(){
		std::shared_ptr<SAEvent> newTriggerNone = std::shared_ptr<SAEvent>(new SAEvent);
		newTriggerNone->event_code = "none";
		newTriggerNone->event_type = special_area_event_t::special_area_event_none;

		std::shared_ptr<SAEvent> newTrigger = std::shared_ptr<SAEvent>(new SAEvent);
		newTrigger->event_code = "none";
		newTrigger->event_type = special_area_event_t::special_area_event_enter_area;

		std::shared_ptr<SAEvent> newTrigger2 = std::shared_ptr<SAEvent>(new SAEvent);
		newTrigger2->event_code = "none";
		newTrigger2->event_type = special_area_event_t::special_area_event_leave_area;

		vector_event.push_back(newTriggerNone);
		vector_event.push_back(newTrigger);
		vector_event.push_back(newTrigger2);
	}

	std::shared_ptr<SAActivationTrigger> get_rule_activation_trigger(int index){
		if (index > static_cast<int32_t>(vector_activation_trigger.size()) - 1)
			return nullptr;

		return vector_activation_trigger[index];
	}
	void remove_activation_trigger(int index){
		vector_activation_trigger.erase(vector_activation_trigger.begin() + index);
	}
	std::shared_ptr<SAEvent> get_rule_event(int index){
		if (index > static_cast<int32_t>(vector_event.size()) - 1)
			return nullptr;

		return vector_event[index];
	}

	std::vector<std::shared_ptr<SAActivationTrigger>> get_vector_activation_trigger(){
		return vector_activation_trigger;
	}
	std::vector<std::shared_ptr<SAEvent>> get_vector_event(){
		return vector_event;
	}




	SABaseElement(special_area_element_t type, SABaseElement* parent = nullptr);

	~SABaseElement();

	SAAvoidance* get_avoidance();

	SAWalkDificult* get_walk_dificult();

	special_area_activation_t get_activation_trigger();

	void set_activation_trigger(special_area_activation_t trigger_type);

	void set_state(bool _state);

	bool get_state();

	bool exist_raw_pointer(SABaseElement* element);

	std::vector<SABaseElement*> get_childrens();

	bool get_block_walk();

	Coordinate get_center();

	void set_block_walk(bool block);

	std::string get_name();

	void set_name(std::string name);

	void set_owner(SABaseElement* onwer);

	SABaseElement* get_owner();

	bool is_area();

	bool is_wall();

	bool is_point();

	special_area_element_t get_type();

	void set_type(special_area_element_t type);

	SABaseElement* create_child(special_area_element_t type);

	bool is_type_of(special_area_element_t element_t);

	SABaseElement* get_child_by_name(std::string name, bool recursive = false, special_area_element_t element_t = sa_element_any);

	std::vector<SABaseElement*> get_childrens_by_name(std::string name, bool recursive = false, special_area_element_t element_t = sa_element_any);

	SABaseElement* get_childrens_by_raw_pointer(SABaseElement* pointer, bool recursive = false);

	std::vector<SABaseElement*> get_childrens_by_type(special_area_element_t _element_t, bool recursive = false);

	SABaseElement* remove_element(SABaseElement* element_ptr, bool delete_after_remove = true);

	Json::Value ClassToJson();

	bool JsonToClass(Json::Value& value);

	bool is_in_area(uint32_t x, uint32_t y, int16_t z);

	void check_coord_change(Coordinate& coord);

	std::string get_on_enter_area_event();

	std::string get_on_out_area_event();

	void on_enter_area();

	void on_out_area();

	std::string* event_out_str_ptr = nullptr;
	std::string* event_enter_str_ptr = nullptr;

	void clear();
};


class SAArea : public SABaseElement{
public:
	SAArea(SABaseElement* owner);
	int32_t start_x, start_y, z;
	int32_t width = 1;
	int32_t end_x, end_y;
	void normalize_negative();

	Rect get_rect();
};

class SAPointElement : public SABaseElement{
public:
	SAPointElement(SABaseElement* owner);

	uint32_t x, y, z;
	Rect get_avoidance_rect();
	Rect get_walkability_rect();
	Point get_point();
};

class SAWallElement : public SABaseElement{
public:
	SAWallElement(SABaseElement* owner);

	void set_width(int32_t value);

	int32_t get_width();

	int32_t start_x, start_y, width, z;
	int32_t end_x, end_y;

	Rect get_rect();

	std::pair<Point, Point> get_line();
};

class SpecialAreaManager{
	SpecialAreaManager();
	SAArea* root_area;

public:

	bool exist_raw_pointer(SABaseElement* element);

	SAArea* get_root();

	static SpecialAreaManager* get();
	
};

#pragma pack(pop)