#pragma once
#include "Core\Util.h"
#include "Core\Time.h"

class NaviPlayerInfo{
public:
	TimeChronometer time_last_update;

	uint32_t* mtx_access;

	uint32_t exp_to_next_lvl;
	uint32_t experience;
	uint32_t time_to_next_lvl;

	std::string get_name_char;

	uint32_t self_level;
	uint32_t self_levelpc;
	uint32_t self_ml;
	uint32_t self_mlpc;
	uint32_t self_first;
	uint32_t self_firstpc;
	uint32_t self_club;
	uint32_t self_clubpc;
	uint32_t self_sword;
	uint32_t self_swordpc;
	uint32_t self_axe;
	uint32_t self_axepc;
	uint32_t self_distance;
	uint32_t self_distancepc;
	uint32_t self_shielding;
	uint32_t self_shieldingpc;
	uint32_t self_fishing;
	uint32_t self_fishingpc;

	Coordinate get_self_coordinate;

	uint32_t self_maxhp;
	uint32_t self_maxmp;
	uint32_t current_hp;
	uint32_t current_hppc;
	uint32_t current_mp;
	uint32_t current_mppc;
	uint32_t self_cap;
	uint32_t self_soul;
	uint32_t self_stamina;
	uint32_t self_off_training;
	uint32_t self_character_id;
	uint32_t target_red;

	bool is_follow_mode;
	bool is_walking;

	bool satus_water;
	bool status_holly;
	bool status_frozen;
	bool status_curse;
	bool status_battle_red;
	bool status_streng_up;
	bool status_pz;
	bool status_blood;
	bool status_poison;
	bool status_fire;
	bool status_energy;
	bool status_drunk;
	bool status_slow;
	bool status_utamo;
	bool status_haste;
	bool status_battle;

	NaviPlayerInfo();

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonValue);
};

typedef std::shared_ptr<NaviPlayerInfo> NaviPlayerInfoPtr;