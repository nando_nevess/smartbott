#include "HotkeyManager.h"
#include "HotkeysManager.h"
#include "Core\InputManager.h"


using namespace NeutralBot;

HotkeyManager::HotkeyManager(void)
{
	notifications_enabled = true;

	InitializeComponent();

	System::Collections::Generic::List<Telerik::WinControls::UI::RadListDataItem^>^ items =
		gcnew System::Collections::Generic::List < Telerik::WinControls::UI::RadListDataItem^ >();

	items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListCtrl->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListCtrl->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListCtrl->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListScrollLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListScrollLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListScrollLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListShift->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListShift->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListShift->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListCapsLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListCapsLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListCapsLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListAlt->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListAlt->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListAlt->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListInsert->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListInsert->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListInsert->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	radDropDownListNumLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("unpressed"));
	radDropDownListNumLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("pressed"));
	radDropDownListNumLock->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem("any"));

	LoadInterfaceData();
	
	this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
	NeutralBot::FastUIPanelController::get()->install_controller(this);
}

System::Void HotkeyManager::radListViewHotkeys_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {	
	if (radListViewHotkeys->SelectedIndex == -1 || !radListViewHotkeys->SelectedItem || !radListViewHotkeys->SelectedItem["Id"])
		return;
	
	uint32_t hk_id = (uint32_t)radListViewHotkeys->SelectedItem["Id"];
	std::shared_ptr<Hotkey> hotkey = HotkeysManager::get()->get_hotkey_by_id(hk_id);
	
	if (hotkey){
		notifications_enabled = false;
		loadHotkey(hotkey);
		radTextBoxName->Text = gcnew String(&hotkey->name[0]);
		notifications_enabled = true;
	}

}

void HotkeyManager::updateCurrentHotkey(){
	InputWait^ mInput = gcnew InputWait();

	mInput->ShowDialog();
	if (mInput->input_retval){
		int last_input = radListViewHotkeys->SelectedIndex;
		if (last_input != -1){
			std::shared_ptr<Hotkey> hotkey = getSelectedHotkey();
			if (hotkey){
				radListViewHotkeys->SelectedIndex = -1;
				hotkey->assign(mInput->input_retval);
				radListViewHotkeys->SelectedIndex = last_input;
				radListViewHotkeys->SelectedItem["Hotkey"] = gcnew String(&hotkey->name[0]);
				radTextBoxName->Text = gcnew String(&hotkey->name[0]);
				SaveAll(nullptr);
			}
		}
	}
}

void HotkeyManager::addInterfaceHotkey(Hotkey* hk){
	Telerik::WinControls::UI::ListViewDataItem^ new_item = gcnew Telerik::WinControls::UI::ListViewDataItem();
	radListViewHotkeys->Items->Add(new_item);

	new_item["Id"] = (uint32_t)hk->id;
	new_item["Hotkey"] = gcnew String(&hk->name[0]);
	radTextBoxName->Text = gcnew String(&hk->name[0]);
	radListViewHotkeys->SelectedIndex = -1;
	radListViewHotkeys->SelectedIndex = radListViewHotkeys->Items->Count - 1;
}

void HotkeyManager::setEditMode(){
	this->radPanelEdit->Visible = true;
}

void HotkeyManager::setInputMode(){
	this->radPanelEdit->Visible = false;
}

System::Void HotkeyManager::radButtonNewHotkey_Click(System::Object^  sender, System::EventArgs^  e) {
	InputWait^ mInput = gcnew InputWait();
	mInput->ShowDialog();
	if (mInput->input_retval){
		auto new_hk = HotkeysManager::get()->new_hotkey();
		new_hk->assign(mInput->input_retval);
		addInterfaceHotkey(new_hk.get());
		SaveAll(nullptr);
	}
}

System::Void HotkeyManager::radButtonRemoveHotkey_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!radListViewHotkeys->SelectedItem)
		return;

	uint32_t hk_id = (uint32_t)radListViewHotkeys->SelectedItem["Id"];
	auto hk = getSelectedHotkey();

	if (!hk)
		return;

	radListViewHotkeys->Items->Remove(radListViewHotkeys->SelectedItem);
	
	HotkeysManager::get()->remove_hotkey(hk_id);
}

System::Void HotkeyManager::radButtonUpdateHotkey_Click(System::Object^  sender, System::EventArgs^  e) {
	updateCurrentHotkey();
}

std::shared_ptr<Hotkey> HotkeyManager::getSelectedHotkey(){
	if (!radListViewHotkeys->SelectedItem)
		return nullptr;

	uint32_t hk_id = (uint32_t)radListViewHotkeys->SelectedItem["Id"];
	auto temp = HotkeysManager::get()->get_hotkey_by_id(hk_id);
	return temp;
}

System::Void HotkeyManager::radDropDownListCtrl_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();
	if (!selected)
		return;
	selected->ctrl = (hotkey_value_t)radDropDownListCtrl->SelectedIndex;
}
System::Void HotkeyManager::radDropDownListShift_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();
	if (!selected)
		return;
	selected->shift = (hotkey_value_t)radDropDownListShift->SelectedIndex;
}
System::Void HotkeyManager::radDropDownListAlt_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();
	if (!selected)
		return;
	selected->alt = (hotkey_value_t)radDropDownListAlt->SelectedIndex;
}
System::Void HotkeyManager::radDropDownListNumLock_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();
	if (!selected)
		return;
	selected->numlock = (hotkey_value_t)radDropDownListNumLock->SelectedIndex;
}
System::Void HotkeyManager::radDropDownListInsert_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();
	if (!selected)
		return;
	selected->insert = (hotkey_value_t)radDropDownListInsert->SelectedIndex;
}
System::Void HotkeyManager::radDropDownListCapsLock_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();
	if (!selected)
		return;
	selected->capslock = (hotkey_value_t)radDropDownListCapsLock->SelectedIndex;
}
System::Void HotkeyManager::radDropDownListScrollLock_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (!notifications_enabled)
		return;

	auto selected = getSelectedHotkey();
	if (!selected)
		return;

	selected->scrolllock = (hotkey_value_t)radDropDownListScrollLock->SelectedIndex;
}
System::Void HotkeyManager::radCheckBoxState_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	if (!notifications_enabled)
		return;
	auto selected = getSelectedHotkey();

	if (!selected)
		return;

	auto temp = radCheckBoxState->Checked;

	selected->state = temp;
}

void HotkeyManager::loadHotkey(std::shared_ptr<Hotkey> hotkey){
	radDropDownListCtrl->SelectedIndex = (int)hotkey->ctrl;
	radDropDownListScrollLock->SelectedIndex = (int)hotkey->scrolllock;
	radDropDownListShift->SelectedIndex = (int)hotkey->shift;
	radDropDownListCapsLock->SelectedIndex = (int)hotkey->capslock;
	radDropDownListAlt->SelectedIndex = (int)hotkey->alt;
	radDropDownListInsert->SelectedIndex = (int)hotkey->insert;
	radDropDownListNumLock->SelectedIndex = (int)hotkey->numlock;
	radCheckBoxState->Checked = hotkey->state;
	radTextBoxKeys->Text = (gcnew String(&hotkey->button_as_string[0]))->ToUpper();
}

System::Void HotkeyManager::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	auto current_hk = getSelectedHotkey();

	if (!current_hk)
		return;


	LuaBackground^ lua_background = nullptr;
	if (current_hk->events.begin() == current_hk->events.end())
		lua_background = gcnew LuaBackground("");
	else
		lua_background = gcnew LuaBackground(gcnew String(&(*current_hk->events.begin())[0]));
	
	HotkeysManager::get()->set_edit_script(true);
	lua_background->ShowDialog();
	HotkeysManager::get()->set_edit_script(false);
	current_hk->events.clear();
	current_hk->events.push_back(managed_util::fromSS(lua_background->fastColoredTextBox1->Text));
	SaveAll(nullptr);
}

System::Void HotkeyManager::radTextBoxName_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	auto hk = getSelectedHotkey();

	if (!hk)
		return;

	hk->name = managed_util::fromSS(radTextBoxName->Text);
	radListViewHotkeys->SelectedItem["Hotkey"] = gcnew String(&hk->name[0]);
}

void HotkeyManager::LoadAll(String^ path){
	if (path)
		HotkeysManager::get()->Load(managed_util::fromSS(path));
	else
		HotkeysManager::get()->Load();
	
	LoadInterfaceData();
}

void HotkeyManager::LoadInterfaceData(){
	auto hotkeys = HotkeysManager::get()->get_hotkeys();
	for (auto hotkey : hotkeys){
		addInterfaceHotkey(hotkey.get());
	}
}

void HotkeyManager::SaveAll(String^ path){
	if (path)
		HotkeysManager::get()->Save(managed_util::fromSS(path));
	else
		HotkeysManager::get()->Save();
}

void HotkeyManager::SetDefault(){
	radListViewHotkeys->Items->Clear();
	HotkeysManager::get()->reset_default();
	LoadAll(nullptr);
}

System::Void HotkeyManager::radMenuItemExport_Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::SaveFileDialog fileDialog;
	fileDialog.Filter = "Json Object|*.json";
	fileDialog.Title = "Export Hotkeys";

	System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();

	if (result == System::Windows::Forms::DialogResult::Cancel)
		return;

	String^ path = fileDialog.FileName;
	SaveAll(path);
}
System::Void HotkeyManager::radMenuItemImport_Click(System::Object^  sender, System::EventArgs^  e) {

	System::Windows::Forms::OpenFileDialog fileDialog;
	fileDialog.Filter = "Json Object|*.json";
	fileDialog.Title = "Inport Hotkeys";

	System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();
	if (result == System::Windows::Forms::DialogResult::Cancel){
		return;
	}

	String^ path = fileDialog.FileName;
	LoadAll(path);
	SaveAll(nullptr);
}
System::Void HotkeyManager::radListViewHotkeys_ItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
	SaveAll(nullptr);
}