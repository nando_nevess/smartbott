#include "ResizeFormClass.h"

using namespace NeutralBot;

ModeInterfaceWindows::ModeInterfaceWindows(){
	

	this->Shown += gcnew System::EventHandler(this, &ModeInterfaceWindows::ThisShown);
}


System::Void ModeInterfaceWindows::ThisShown(System::Object^  sender, System::EventArgs^  e) {

	size_default = System::Drawing::Size(0, 0);

	this->Activated += gcnew System::EventHandler(this, &ModeInterfaceWindows::Form_Activated);
	this->Deactivate += gcnew System::EventHandler(this, &ModeInterfaceWindows::Form_Deactivate);


	size_default = this->Size;
	if (size_resized.Width == 0 && size_resized.Height == 0){
		//size_resized = size_default;
		size_resized = System::Drawing::Size(110, this->Size.Height);
	}
	Telerik::WinControls::UI::RadForm::PerformLayout();
}

System::Void ModeInterfaceWindows::Form_Activated(System::Object^  sender, System::EventArgs^  e){
	std::cout << "\n MOUSE ENTER 2";
	update_mode_sizes();
	bot_interface_mode type = GeneralManager::get()->get_interface_mode();
	if (type == bot_interface_mode::bot_interface_mode_default
		|| type == bot_interface_mode::bot_interface_mode_always_on_top){
		if (size_default != this->Size)
			this->Size = size_default;
		return;
	}
	else{
		if (size_resized != this->Size)
			this->Size = size_default;
	}
}

System::Void ModeInterfaceWindows::Form_Deactivate(System::Object^  sender, System::EventArgs^  e){
	update_mode_sizes();
	bot_interface_mode type = GeneralManager::get()->get_interface_mode();
	if (type == bot_interface_mode::bot_interface_mode_default
		|| type == bot_interface_mode::bot_interface_mode_always_on_top){
		if (size_default != this->Size)
			this->Size = size_default;
		return;
	}
	else{
		if (size_resized != this->Size)
			this->Size = size_resized;
	}
}
	
void ModeInterfaceWindows::update_mode_sizes(){
	bot_interface_mode type = GeneralManager::get()->get_interface_mode();
	switch (type){
	case bot_interface_mode::bot_interface_mode_always_on_top:{
		this->TopMost = true;
	}
		break;
	case bot_interface_mode::bot_interface_mode_always_plus_resize:{
		this->TopMost = true;
	}
		break;
	case bot_interface_mode::bot_interface_mode_default:{
		this->TopMost = false;
	}
		break;
	}
}