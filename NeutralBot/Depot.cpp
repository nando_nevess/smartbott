#include "Depot.h"

using namespace Neutral;

System::Void Depot::loadAll(){
	std::map<std::string, std::shared_ptr<DepotContainer>> map = DepoterManager::get()->getMapDepotContainer();
	for (auto it = map.begin(); it != map.end(); it++){
		waypointPageAddNewPage(it->first);
		loadItemOnView(it->first);
	}
	check_use_alternative_mode->Checked = DepoterManager::get()->get_use_old_style();
}
System::Void Depot::waypointPageAddNewPage(std::string PageNameId){
	Telerik::WinControls::UI::RadPageViewPage^ radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
	Telerik::WinControls::UI::RadListView^ radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
	Telerik::WinControls::UI::RadDropDownList^ dropDownList = (gcnew Telerik::WinControls::UI::RadDropDownList());
	Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0", L"Item"));
	
	// box_namebp
	radPageViewPage1->Controls->Add(dropDownList);
	dropDownList->Location = System::Drawing::Point(0, 0);
	dropDownList->Name = "box_" + gcnew String(PageNameId.c_str());
	dropDownList->Dock = System::Windows::Forms::DockStyle::Top;
	dropDownList->TabIndex = 1;

	dropDownList->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
	dropDownList->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Depot::box_namebpSelectedIndexChanged);
	dropDownList->DropDownListElement->ItemHeight = 20;
	dropDownList->DropDownListElement->DropDownHeight = 30 * 8;

	std::vector<std::string> arraycontainer = ItemsManager::get()->get_containers();

	dropDownList->BeginUpdate();
	for (auto container : arraycontainer){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = gcnew String(container.c_str());
		item->Image = managed_util::get_item_image(gcnew String(container.c_str()), 20);
		dropDownList->Items->Add(item);
	}
	dropDownList->EndUpdate();

	// radPageViewPage1
	radPageViewPage1->Controls->Add(radListView1);
	radPageViewPage1->ItemSize = System::Drawing::SizeF(39, 28);
	radPageViewPage1->Location = System::Drawing::Point(10, 37);
	radPageViewPage1->Name = "Page_" + gcnew String(PageNameId.c_str());
	radPageViewPage1->Size = System::Drawing::Size(773, 392);
	radPageViewPage1->Text = gcnew String(PageNameId.c_str());

	// radListView1
	listViewDetailColumn7->HeaderText = L"Item";
	listViewDetailColumn7->Width = 282;
	listViewDetailColumn7->MaxWidth = 282;
	listViewDetailColumn7->MinWidth = 282;
	radListView1->AutoScroll = false;
	radListView1->AllowEdit = false;
	radListView1->ShowGridLines = true;
	radListView1->Columns->AddRange(gcnew cli::array<Telerik::WinControls::UI::ListViewDetailColumn^>(1) { listViewDetailColumn7 });
	radListView1->ItemSpacing = -1;
	radListView1->Location = System::Drawing::Point(0, 25);
	radListView1->Name = "list_" + gcnew String(PageNameId.c_str());
	radListView1->Size = System::Drawing::Size(284, 225);
	radListView1->TabIndex = 0;
	radListView1->Text = L"";
	radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
	radListView1->ListViewElement->ItemSize = System::Drawing::Size(radListView1->ListViewElement->ItemSize.Width, 25);
	radListView1->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Depot::ItemListViewItemRemoving);
	radListView1->SelectedItemChanged += gcnew System::EventHandler(this, &Depot::ListView1ItemChanged);
	
	PageDepot->Controls->Add(radPageViewPage1);
}
System::Void Depot::addItemOnView(String^ ContainerName, String^ idBp, String^ Item, int maxqty,int minqty){
	Telerik::WinControls::UI::RadListView^ radListView = getItemListView();

	if (!radListView)
		return;

	std::shared_ptr<DepotContainer> Itens = DepoterManager::get()->getDepotContainer(managed_util::fromSS(idBp));
	std::string idItem = Itens->addDepotItem(managed_util::fromSS(Item), maxqty, minqty);
	array<String^>^ columnArrayItems = { Item, Convert::ToString(maxqty), Convert::ToString(minqty) };

	Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(idItem.c_str()), columnArrayItems);
	newItem->Image = managed_util::get_item_image(Item, 20);
	radListView->Items->Add(newItem);
	radListView->SelectedIndex = radListView->Items->Count - 1;
}
System::Void Depot::loadItemOnView(std::string id_) {
	array<Control^>^ getRadListView = Controls->Find("list_" + gcnew String(&id_[0]), true);
	array<Control^>^ getRadDropDownBox = Controls->Find("box_" + gcnew String(&id_[0]), true);

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	Telerik::WinControls::UI::RadDropDownList^ RadDropDownBox = (Telerik::WinControls::UI::RadDropDownList^) getRadDropDownBox[0];

	std::map<std::string, std::shared_ptr<DepotContainer>> mapDepotContainer = DepoterManager::get()->getMapDepotContainer();
	std::map<std::string, std::shared_ptr<DepotItem>> mapDepotItem = mapDepotContainer[id_]->getMapDepoItens();

	box_mainbpdp->Text = gcnew String(DepoterManager::get()->get_ContainerName().c_str());
	pictureBoxMainDepotBp->Image = managed_util::get_item_image(box_mainbpdp->Text, pictureBoxMainDepotBp->Height);
	RadDropDownBox->Text = gcnew String(mapDepotContainer[id_]->get_ContainerName().c_str());
	pictureBoxMainDepotBp->Image = managed_util::get_item_image(box_mainbpdp->Text, pictureBoxMainDepotBp->Height);

	box_chest->Text = gcnew String(DepoterManager::get()->get_DepotBoxName().c_str());

	radListView->BeginUpdate();
	for (auto i = mapDepotItem.begin(); i != mapDepotItem.end(); i++) {
		array<String^>^ columnArrayItems = { gcnew String(i->second->get_itemName().c_str()), Convert::ToString(i->second->get_maxqty()) };
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(i->first.c_str()), columnArrayItems);
		newItem->Image = managed_util::get_item_image(gcnew String(i->second->get_itemName().c_str()), 20);
		radListView->Items->AddRange(gcnew array<Telerik::WinControls::UI::ListViewDataItem^>(1) { newItem });
	}
    radListView->EndUpdate();
}
Telerik::WinControls::UI::RadListView^ Depot::getItemListView(){
	auto selected_page = this->PageDepot->SelectedPage;
	if (!selected_page)
		return nullptr;

	array<Control^>^ getRadListView = selected_page->Controls->Find("list_" + PageDepot->SelectedPage->Text, true);
	if (!getRadListView->Length)
		return nullptr;

	return (Telerik::WinControls::UI::RadListView^) getRadListView[0];
}
Telerik::WinControls::UI::RadDropDownList^ Depot::getBpCurrentDropDownList(){
	if (!PageDepot)
		return nullptr;

	if (!PageDepot->SelectedPage)
		return nullptr;
	array<Control^>^ getRadDownList = PageDepot->SelectedPage->Controls->Find("box_" + PageDepot->SelectedPage->Text, true);

	if (!getRadDownList->Length)
		return nullptr;
	
	return (Telerik::WinControls::UI::RadDropDownList^)getRadDownList[0];
}
System::Void Depot::ItemListViewItemRemoving(System::Object^ sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	Telerik::WinControls::UI::RadListView^ radListView = getItemListView();
	if (!radListView)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;
	auto mapDepotContainer = DepoterManager::get()->getMapDepotContainer();
	if (!itemRow)
		return;

	mapDepotContainer[managed_util::fromSS(PageDepot->SelectedPage->Text)]->removeDepotItem(managed_util::fromSS(itemRow->Text));
}
System::Void Depot::PageDepot_PageRemoved(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e) {
	DepoterManager::get()->removeDepotContainer(managed_util::fromSS(e->Page->Text));
	close_button();
	update_alternative_mode();

	auto control_rule = ControlManager::get()->getControlInfoRule("DepoterGroup");
	if (!control_rule)
		return;

	control_rule->removeControl(managed_util::fromSS(e->Page->Text));
	control_rule->removeControl(managed_util::fromSS(e->Page->Text) + "label");
}
System::Void Depot::PageDepot_NewPageRequested(System::Object^ sender, System::EventArgs^  e) {
	waypointPageAddNewPage(DepoterManager::get()->requestNewDepotContainer());
	update_alternative_mode();
	close_button();
}
System::Void Depot::Depot_Load(System::Object^ sender, System::EventArgs^  e) {
	std::vector<std::string> arraycontainer = ItemsManager::get()->get_containers();
	box_mainbpdp->BeginUpdate();
	for (auto item : arraycontainer)
		box_mainbpdp->Items->Add(gcnew String(item.c_str()));
	box_mainbpdp->EndUpdate();
	
	std::vector<std::string> arrayitems = ItemsManager::get()->get_itens();
	box_itemname->BeginUpdate();
	for (auto item : arrayitems)
		box_itemname->Items->Add(gcnew String(item.c_str()));
	box_itemname->EndUpdate();
	
	loadAll();

	update_idiom();
	update_alternative_mode();

	radGroupBox2->Enabled = false;

	PageDepot->ViewElement->AllowEdit = true;
	PageDepot->ViewElement->EditorInitialized += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEditorEventArgs^>(this, &Depot::Page_EditorInitialized);
	
	close_button();
}
System::Void Depot::bt_add_Click(System::Object^ sender, System::EventArgs^  e) {
	if (!PageDepot->SelectedPage)
		return;

	Telerik::WinControls::UI::RadDropDownList^ radDownList = getBpCurrentDropDownList();

	String^ item = "new";
	int maxty = 0;
	int minqty = 0;

	if (item == String::Empty)
		return;

	if (!PageDepot->SelectedPage)
		return;

	addItemOnView(radDownList->Text, PageDepot->SelectedPage->Text, item, maxty, minqty);
}
System::Void Depot::bt_delete_Click(System::Object^ sender, System::EventArgs^  e) {
	if (!PageDepot->SelectedPage)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = getItemListView();
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;
	auto mapDepotContainer = DepoterManager::get()->getMapDepotContainer();
	if (!itemRow)
		return;

	radListView->Items->Remove(itemRow);
	mapDepotContainer[managed_util::fromSS(PageDepot->SelectedPage->Text)]->removeDepotItem(managed_util::fromSS(itemRow->Text));
}
System::Void Depot::box_namebpSelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadDropDownList^ radDownList = getBpCurrentDropDownList();

	if (radDownList->Text == "")
		return;

	std::shared_ptr<DepotContainer> srd_bp = DepoterManager::get()->getDepotContainer(managed_util::fromSS(PageDepot->SelectedPage->Text));
	
	if (check_use_alternative_mode->Checked)
		srd_bp->set_Containeritem_id(22797 + (int)e->Position);
	else
		srd_bp->set_ContainerName(managed_util::fromSS(radDownList->Text));

	ControlManager::get()->changePropertyDepoter(managed_util::fromSS(radDownList->Text), managed_util::fromSS(PageDepot->SelectedPage->Text), "text");
}
System::Void Depot::Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e){
	this->PageDepot->SelectedPage->TextChanged += gcnew System::EventHandler(this, &Depot::PageRename_TextChanged);
	text = e->Value->ToString();
}
System::Void Depot::PageRename_TextChanged(System::Object^ sender, System::EventArgs^ e){
	String^ newValue = PageDepot->SelectedPage->Text;
	String^ oldValue = text;

	if (newValue == "" || oldValue == "")
		return;

	if (newValue == oldValue)
		return;

	int count = 0;
	for each(auto items in PageDepot->Pages){
		if (items->Text == newValue){
			if (count == 0)
				count++;
			else{
				PageDepot->SelectedPage->Text = oldValue;
				return;
			}
		}
	}

	std::string newName = string_util::lower( managed_util::to_std_string(newValue));
	std::string oldName = string_util::lower(managed_util::to_std_string(oldValue));

	if (!DepoterManager::get()->changeDepoId(newName, oldName)){
		PageDepot->SelectedPage->Text = oldValue;
		return;
	}

	text = newValue;

	array<Control^>^ getRadListView = PageDepot->SelectedPage->Controls->Find("list_" + oldValue, true);

	if (getRadListView->Length == 0)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
	radListView->Name = "list_" + newValue;

	ControlManager::get()->changePropertyLooter(newName, oldName, "name");
	ControlManager::get()->changePropertyLooter(newName, oldName + "label", "text");
	ControlManager::get()->changePropertyLooter(newName + "label", oldName + "label", "name");
}
System::Void Depot::ListView1ItemChanged(System::Object^ sender, System::EventArgs^  e) {
	Telerik::WinControls::UI::RadListViewElement^ list = (Telerik::WinControls::UI::RadListViewElement^)sender;
	updateSelectedData();
}

System::Void Depot::updateSelectedData(){
	if (!PageDepot->SelectedPage)
		return;

	Telerik::WinControls::UI::RadListView^ radlist = getItemListView();
	Telerik::WinControls::UI::RadDropDownList^ boxlist = getBpCurrentDropDownList();

	if (!radlist->SelectedItem){

		if(radGroupBox2->Enabled)
			radGroupBox2->Enabled = false;

		return;
	}

	if (!radGroupBox2->Enabled)
		radGroupBox2->Enabled = true;

	std::shared_ptr<DepotContainer> mapId = DepoterManager::get()->getDepotContainer(managed_util::fromSS(PageDepot->SelectedPage->Text));
	auto mapItens = mapId->getMapDepoItens();

	std::string idMap = managed_util::fromSS(radlist->SelectedItem->Text);

	String^ ItemName = gcnew String(mapItens[idMap]->get_itemName().c_str());
	uint32_t MaxQty = mapItens[idMap]->get_maxqty();
	uint32_t MinQty = mapItens[idMap]->get_minqty();

	String^ Box = Box = gcnew String(mapId->get_ContainerName().c_str());
	if (!check_use_alternative_mode->Checked){
		if (boxlist)
			boxlist->Text = Box;
	}
	else{
		if (boxlist)
			boxlist->SelectedIndex = mapId->get_depot_box_index();
	}

	disable_item_update = true;
	box_itemname->Text = ItemName;
	box_maxqty->Value = MaxQty;
	box_minqty->Value = MinQty;
	
	disable_item_update = false;
}

System::Void Depot::box_itemname_SelectedIndexChanged(System::Object^ sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e){
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

	if (!radListview)
		return;

	if (!radListview->SelectedItem)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ radItem = (Telerik::WinControls::UI::ListViewDataItem^)radListview->SelectedItem;
	radListview->SelectedItem[0] = box_itemname->Text;
	
	radListview->SelectedItem->Image = managed_util::get_item_image(gcnew String(box_itemname->Text), 20);
	pictureBoxItemName->Image = managed_util::get_item_image(gcnew String(box_itemname->Text), pictureBoxItemName->Height);
	std::shared_ptr<DepotContainer> srd_bp = DepoterManager::get()->getDepotContainer(managed_util::fromSS(PageDepot->SelectedPage->Text));
	srd_bp->getDepotItemById(managed_util::fromSS(radListview->SelectedItem->Text))->set_itemName(managed_util::fromSS(box_itemname->Text));
}
System::Void Depot::box_maxqty_ValueChanged(System::Object^ sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

	if (!radListview)
		return;

	if (!radListview->SelectedItem)
		return;

	std::shared_ptr<DepotContainer> srd_bp = DepoterManager::get()->getDepotContainer(managed_util::fromSS(PageDepot->SelectedPage->Text));
	srd_bp->getDepotItemById(managed_util::fromSS(radListview->SelectedItem->Text))->set_maxqty(Convert::ToInt32(box_maxqty->Value));
}
System::Void Depot::box_minqty_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	Telerik::WinControls::UI::RadListView^ radListview = getItemListView();

	if (!radListview)
		return;

	if (!radListview->SelectedItem)
		return;

	std::shared_ptr<DepotContainer> srd_bp = DepoterManager::get()->getDepotContainer(managed_util::fromSS(PageDepot->SelectedPage->Text));
	srd_bp->getDepotItemById(managed_util::fromSS(radListview->SelectedItem->Text))->set_minqty(Convert::ToInt32(box_minqty->Value));
}
System::Void Depot::box_mainbpdp_SelectedIndexChanged(System::Object^ sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	auto selectedItem = this->box_mainbpdp->SelectedItem;

	if (!selectedItem)
		return;

	DepoterManager::get()->set_ContainerName(marshal_as<std::string>(selectedItem->Text->ToString()));
	pictureBoxMainDepotBp->Image = managed_util::get_item_image(selectedItem->Text->ToString(), pictureBoxMainDepotBp->Height);
}
System::Void Depot::box_chest_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	auto selectedItem = this->box_chest->SelectedItem;

	if (!selectedItem)
		return;

	DepoterManager::get()->set_DepotBox(marshal_as<std::string>(selectedItem->Text->ToString()), e->Position);
	pictureBoxDepotBox->Image = managed_util::get_item_image(selectedItem->Text->ToString(), pictureBoxDepotBox->Height);
}
System::Void Depot::bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::SaveFileDialog fileDialog;
	fileDialog.Filter = "Depoter Script|*.Depot";
	fileDialog.Title = "Save Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	if (!save_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Saved", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved", "Save/Load");
}
System::Void Depot::bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::OpenFileDialog fileDialog;
	fileDialog.Filter = "Depoter Script|*.Depot";
	fileDialog.Title = "Open Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	PageDepot->Pages->Clear();
	DepoterManager::get()->clear();
	box_mainbpdp->Text = "";

	if (!load_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

	loadAll();
}
System::Void Depot::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	NeutralBot::AutoGenerateLoot^ tempForm = gcnew NeutralBot::AutoGenerateLoot("Depoter");
	tempForm->ShowDialog();
	bool check_exist = true;
	if (tempForm->comfirmed){
		for each(String^ item in tempForm->selectedItems){
			check_exist = false;
			if (item == String::Empty)
				continue;

			if (!managed_util::check_exist_item(item->ToLower())){
				continue;
			}


			for each(auto list_item in getItemListView()->Items){
				if (list_item->Text->ToLower() == item->ToLower()){
					check_exist = true;
					break;
				}
			}

			if (check_exist)
				continue;



			Telerik::WinControls::UI::RadDropDownList^ radDownList = getBpCurrentDropDownList();


			int maxty = 0;
			int minqty = 0;

			if (item == String::Empty)
				return;

			if (!PageDepot->SelectedPage)
				return;


			addItemOnView(item, PageDepot->SelectedPage->Text, item, maxty, minqty);
		}
	}
}

bool Depot::save_script(std::string file_name){
	Json::Value file;
	file["version"] = 0100;

	file["DepoterManager"] = DepoterManager::get()->parse_class_to_json();

	return saveJsonFile(file_name, file);
}

bool Depot::load_script(std::string file_name){
	Json::Value file = loadJsonFile(file_name);
	if (file.isNull())
		return false;

	DepoterManager::get()->parse_json_to_class(file["DepoterManager"]);
	return true;
}

