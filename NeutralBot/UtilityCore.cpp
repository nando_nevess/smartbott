#pragma once
#include "UtilityCore.h"
#include "WaypointManager.h"
#include <thread>
#include "Core\TibiaProcess.h"
#include "Core\Screenshoot.h"
#include "Core\LooterCore.h"
#include "Core\Actions.h"
#include "Core\SpellcasterCore.h"

UtilityCore::UtilityCore(){
	creat_new_map();
	std::thread([&](){run_thread(); }).detach(); 
}

void UtilityCore::clear(){
	map_scriptInfo.clear();
	creat_new_map();
}

void UtilityCore::creat_new_map(){
	for (int i = 0; i < utility_t::utility_total; i++){
		std::shared_ptr<ScriptInfo> newScriptInfo = std::shared_ptr<ScriptInfo>(new ScriptInfo);
		std::string keyScriptInfo = get_script_utility((utility_t)i);
		newScriptInfo->script_type = (utility_t)i;
		
		if ((utility_t)i == utility_auto_sio)
			newScriptInfo->set_delay(500);

		map_scriptInfo[keyScriptInfo] = newScriptInfo;
	}
}

std::shared_ptr<ScriptInfo> UtilityCore::get_rule_scriptInfo(std::string key){
	return map_scriptInfo[key];
}

std::string UtilityCore::get_script_utility(utility_t type){
	switch (type){
	case utility_none:
		return "utility_none";
		break;
	case utility_eat_food:
		return "utility_eat_food";
		break;
	case utility_anti_afk:
		return "utility_anti_afk";
		break;
	case utility_drop_vial:
		return "utility_drop_vial";
		break;
	case utility_auto_fishing:
		return "utility_auto_fishing";
		break;
	case utility_refil_amun:
		return "utility_refil_amun";
		break;
	case utility_screenshot:
		return "utility_screenshot";
		break;
	case utility_rune_maker:
		return "utility_rune_maker";
		break;
	case utility_anti_paralyze:
		return "utility_anti_paralyze";
		break;
	case utility_auto_mana_shield:
		return "utility_auto_mana_shield";
		break;
	case utility_auto_ring:
		return "utility_auto_ring";
		break;
	case utility_auto_haste:
		return "utility_auto_haste";
		break;
	case utility_anti_poison:
		return "utility_anti_poison";
		break;
	case utility_total:
		return "utility_total";
		break;	
	case utility_auto_sio:
		return "utility_auto_sio";
		break;
	case utility_auto_utura:
		return "utility_auto_utura";
		break;
	case utility_soft_change:
		return "utility_soft_change";
		break; 
	case utility_auto_mount:
		return "utility_auto_mount";
		break;		
	default:
		return "";
		break;
	}
}

void UtilityCore::run_thread(){
	while (true){
		Sleep(50);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;
		
		if (!TibiaProcess::get_default()->get_is_logged())
			continue;

		if (!map_scriptInfo.size())
			continue;

		for (auto script_info : map_scriptInfo){
			if (!script_info.second)
				continue;

			if (!script_info.second->active)
				continue;

			int delay_to_wait = script_info.second->delay;

			if (script_info.second->script_type == utility_eat_food)
				delay_to_wait = delay_to_wait * 1000;
			
			if (script_info.second->time.elapsed_milliseconds() <= delay_to_wait)
				continue;
			
			execute_script(script_info.second->script_type,script_info.second.get());

			script_info.second->time.reset();
		}
	}
}

void UtilityCore::execute_script(utility_t type, ScriptInfo* info){
	switch (type){
	case utility_eat_food:
		execute_eat_food(info);
		break;
	case utility_anti_afk:
		execute_anti_afk(info);
		break;
	case utility_drop_vial:
		execute_drop_vial(info);
		break;
	case utility_auto_fishing:
		execute_auto_fishing(info);
		break;
	case utility_refil_amun:
		execute_refil_amun(info);
		break;
	case utility_screenshot:
		execute_screenshot(info);
		break;
	case utility_rune_maker:
		execute_rune_maker(info);
		break;
	case utility_anti_paralyze:
		execute_anti_paralyze(info);
		break;
	case utility_auto_mana_shield:
		execute_auto_mana_shield(info);
		break;
	case utility_auto_ring:
		execute_auto_ring(info);
		break;
	case utility_auto_haste:
		execute_auto_haste(info);
		break;
	case utility_anti_poison:
		execute_anti_poison(info);
		break;
	case utility_auto_sio:
		execute_auto_sio(info);
		break;
	case utility_auto_utura:
		execute_auto_utura(info);
		break;
	case utility_soft_change:
		execute_soft_change(info);
		break;
	case utility_auto_mount:
		execute_auto_mount(info);
		break;
	default:
		break;
	}
}

void UtilityCore::execute_auto_mount(ScriptInfo* info) {
	TibiaProcess::get_default()->character_info->mount();
}

void UtilityCore::execute_auto_sio(ScriptInfo* info){
	auto myMap = info->map_friend;
	for (auto creatureMap : myMap){
		std::shared_ptr<CreatureOnBattle> last_creature;
		std::vector<std::shared_ptr<CreatureOnBattle>> creatures;
		
		if (TibiaProcess::get_default()->character_info->hppc() < info->get_mana())
			if (SpellCasterCore::get()->can_cast_spell("exura sio"))
				Actions::says("exura sio \"" + TibiaProcess::get_default()->get_name_char() + " \"");

		std::shared_ptr<CreatureOnBattle> creature = BattleList::get()->find_creature_on_screen(creatureMap.first);
		if (!creature)
			continue;

		Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		if (creature->pos_z != current_coord.getZ())
			continue;

		if (TibiaProcess::get_default()->character_info->mp() < 140)
			continue;

		if (!SpellCasterCore::get()->can_cast_spell("exura sio"))
			continue;

		if (creature->life > creatureMap.second)
			continue;

		if (myMap.size() > 1)
			creatures.push_back(creature);	

		if (!last_creature)
			last_creature = creature;

		for (auto character : creatures)
			if (character->life < last_creature->life)
				last_creature = creature;		

		std::string name = last_creature->name;
		Actions::says("exura sio \"" + name);
	}
}

void UtilityCore::execute_auto_haste(ScriptInfo* info) {
	std::string spell = info->get_value_string();
	uint32_t current_mana = TibiaProcess::get_default()->character_info->mp();
	bool status_haste = TibiaProcess::get_default()->character_info->status_haste();
	bool can_cast = SpellCasterCore::get()->can_cast_spell(spell);

	if (!status_haste && current_mana > info->get_mana() && can_cast)
		Actions::says(spell);
}

void UtilityCore::execute_anti_poison(ScriptInfo* info) {
	std::string spell = info->get_value_string();
	uint32_t current_mana = TibiaProcess::get_default()->character_info->mp();
	bool status_poison = TibiaProcess::get_default()->character_info->status_poison();
	bool can_cast = SpellCasterCore::get()->can_cast_spell(spell);

	if (status_poison && current_mana > info->get_mana() && can_cast)
		Actions::says(spell);
}

void UtilityCore::execute_soft_change(ScriptInfo* info){
	if (LooterCore::get()->need_operate() || HunterCore::get()->need_operate())
		return;

	uint32_t old_boots = ItemsManager::get()->getitem_idFromName(info->get_value_string());
	uint32_t soft_used_boots = 3549; //Id da soft usada
	uint32_t soft_new_boots = 6529;//id da soft nova

	uint32_t soft_used_count = ContainerManager::get()->get_item_count(soft_used_boots);
	uint32_t soft_new_count = ContainerManager::get()->get_item_count(soft_new_boots);

	uint32_t boot_equiped = Inventory::get()->body_boot();
	if (info->get_mana() <= TibiaProcess::get_default()->character_info->mppc()){
		if (boot_equiped == soft_used_boots)
			return;

		if (soft_used_count <= 0 && soft_new_count <= 0)
			return;
		
		if (soft_new_count > 0)
			Inventory::get()->put_item(soft_new_boots, inventory_slot_t::slot_boots, 1);
		else if (soft_used_count > 0)
			Inventory::get()->put_item(soft_used_boots, inventory_slot_t::slot_boots, 1);
	}
	else if(info->get_hp() > TibiaProcess::get_default()->character_info->mppc()) {
		if (boot_equiped == old_boots)
			return;

		if (boot_equiped != soft_used_boots && boot_equiped != 0)
			return;

		if (ContainerManager::get()->get_item_count(old_boots) > 0)
			Inventory::get()->put_item(old_boots, inventory_slot_t::slot_boots, 1);		
		else
			Inventory::get()->unequip_slot(inventory_slot_t::slot_boots, Inventory::get()->body_bag());
	}
}

void UtilityCore::execute_auto_ring(ScriptInfo* info){
	uint32_t ring_id = ItemsManager::get()->getitem_idFromName(info->get_value_string());
	if (ring_id <= 0 || ring_id == UINT32_MAX)
		return;

	uint32_t itemCount = ContainerManager::get()->get_item_count(ring_id);
	if (itemCount <= 0)
		return;

	uint32_t ring_equiped = Inventory::get()->get_item_id_by_slot_type(slot_ring);
	if (ring_equiped != 0 && ring_equiped != UINT32_MAX)
		return;

	Inventory::get()->put_item(ring_id, inventory_slot_t::slot_ring, 1);
}

void UtilityCore::execute_auto_utura(ScriptInfo* info){
	std::string spell = info->get_value_string();
	uint32_t current_mana = TibiaProcess::get_default()->character_info->mp();
	uint32_t current_hp = TibiaProcess::get_default()->character_info->hppc();
	bool status_up = TibiaProcess::get_default()->character_info->status_streng_up();
	bool can_cast = SpellCasterCore::get()->can_cast_spell(spell);

	if (!status_up && current_mana > info->get_mana() && can_cast 
		&& current_hp < info->get_hp())
		Actions::says(spell);
}

void UtilityCore::execute_auto_mana_shield(ScriptInfo* info) {
	std::string spell = info->get_value_string();
	uint32_t current_mana = TibiaProcess::get_default()->character_info->mp();
	bool status_utamo = TibiaProcess::get_default()->character_info->status_utamo();
	bool can_cast = SpellCasterCore::get()->can_cast_spell(spell);

	if (!status_utamo && current_mana > info->get_mana() && can_cast)
		Actions::says(spell);
}

void UtilityCore::execute_anti_paralyze(ScriptInfo* info) {
	std::string spell = info->get_value_string();
	uint32_t current_mana = TibiaProcess::get_default()->character_info->mp();
	bool status_slow = TibiaProcess::get_default()->character_info->status_slow();
	bool can_cast = SpellCasterCore::get()->can_cast_spell(spell);

	if (status_slow && current_mana > info->get_mana() && can_cast)
		Actions::says(spell);
}

void UtilityCore::execute_rune_maker(ScriptInfo* info){
	std::string spell = info->get_value_string();
	uint32_t current_mana = TibiaProcess::get_default()->character_info->mp();
	uint32_t current_soul = TibiaProcess::get_default()->character_info->soul();
	bool can_cast = SpellCasterCore::get()->can_cast_spell(spell);

	if (current_soul > info->get_soul() && current_mana > info->get_mana() && can_cast)
		Actions::says(spell);
}

void UtilityCore::execute_screenshot(ScriptInfo* info){
	if ((current_level - TibiaProcess::get_default()->character_info->level() == 1)){
		ScreenShoot::get()->take_screen_shot();
		current_level = TibiaProcess::get_default()->character_info->level();
	}

	if (TibiaProcess::get_default()->character_info->hp() < 50)
		ScreenShoot::get()->take_screen_shot();	
}

void UtilityCore::execute_refil_amun(ScriptInfo* info) {
	auto current_waypoint = WaypointManager::get()->getCurrentWaypointInfo();
	if (current_waypoint){
		switch (current_waypoint->get_action()){
		case waypoint_lua_script:
			return;
			break;
		case waypoint_hole_or_step:
			return;
			break;
		case waypoint_up_ladder:
			return;
			break;
		case waypoint_reopen_containers:
			return;
			break;
		case waypoint_close_containers:
			return;
			break;
		case waypoint_repot_here_by_id:
			return;
			break;
		case waypoint_deposit_items_by_depot_id:
			return;
			break;
		case waypoint_deposit_items_all_depot:
			return;
			break;
		case waypoint_repot_from_depot_by_id:
			return;
			break;
		case waypoint_sell_items_by_sell_id:
			return;
			break;
		case waypoint_sell_all_items:
			return;
			break;
		case waypoint_deposit_money:
			return;
			break;
		case waypoint_withdraw_all_repots_if_fail_goto_label:
			return;
			break;
		case waypoint_rope:
			return;
			break;
		case waypoint_shovel:
			return;
			break;
		case waypoint_pick:
			return;
			break;
		case waypoint_machete:
			return;
			break;
		case waypoint_scythe:
			return;
			break;
		case waypoint_destroy_field:
			return;
			break;
		case waypoint_tool_id:
			return;
			break;
		case waypoint_use:
			return;
			break;
		case waypoint_use_item:
			return;
			break;
		case waypoint_use_lever:
			return;
			break;
		case waypoint_open_door:
			return;
			break;
		case waypoint_remove_item:
			return;
			break;
		case waypoint_teleport_else_go_to_label:
			return;
			break;
		case waypoint_use_item_id_to_down_or_up:
			return;
			break;
		case waypoint_withdraw_value:
			return;
			break;
		}
	}

	action_retval_t temp;

	uint32_t item_id = ItemsManager::get()->getitem_idFromName(info->get_value_string());
	if (item_id == UINT32_MAX || item_id <= 0)
		temp = Actions::refill_weap_amun_process([&]()->bool{if (LooterCore::get()->need_operate() || HunterCore::get()->need_operate())return true; return false; });
	else
		temp = Actions::refill_weap_amun_process_by_id(item_id, [&]()->bool{if (LooterCore::get()->need_operate() || HunterCore::get()->need_operate())return true; return false; });

	if (temp == action_retval_t::action_retval_success)
		Inventory::get()->minimize();
}

void UtilityCore::execute_auto_fishing(ScriptInfo* info){
	if (LooterCore::get()->need_operate() || HunterCore::get()->need_operate())
		return;

	Actions::fish_process();
}

void UtilityCore::execute_drop_vial(ScriptInfo* info) {
	auto current_waypoint = WaypointManager::get()->getCurrentWaypointInfo();
	if (!current_waypoint)
		return;

	switch (current_waypoint->get_action()){
	case waypoint_lua_script:
		return;
		break;
	case waypoint_hole_or_step:
		return;
		break;
	case waypoint_up_ladder:
		return;
		break;
	case waypoint_reopen_containers:
		return;
		break;
	case waypoint_close_containers:
		return;
		break;
	case waypoint_repot_here_by_id:
		return;
		break;
	case waypoint_deposit_items_by_depot_id:
		return;
		break;
	case waypoint_deposit_items_all_depot:
		return;
		break;
	case waypoint_repot_from_depot_by_id:
		return;
		break;
	case waypoint_sell_items_by_sell_id:
		return;
		break;
	case waypoint_sell_all_items:
		return;
		break;
	case waypoint_deposit_money:
		return;
		break;
	case waypoint_withdraw_all_repots_if_fail_goto_label:
		return;
		break;
	case waypoint_rope:
		return;
		break;
	case waypoint_shovel:
		return;
		break;
	case waypoint_pick:
		return;
		break;
	case waypoint_machete:
		return;
		break;
	case waypoint_scythe:
		return;
		break;
	case waypoint_destroy_field:
		return;
		break;
	case waypoint_tool_id:
		return;
		break;
	case waypoint_use:
		return;
		break;
	case waypoint_use_item:
		return;
		break;
	case waypoint_use_lever:
		return;
		break;
	case waypoint_open_door:
		return;
		break;
	case waypoint_remove_item:
		return;
		break;
	case waypoint_teleport_else_go_to_label:
		return;
		break;
	case waypoint_use_item_id_to_down_or_up:
		return;
		break;
	case waypoint_withdraw_value:
		return;
		break;
	}

	if (LooterCore::get()->need_operate())
		return;

	Actions::drop_empty_potions();
}

void UtilityCore::execute_anti_afk(ScriptInfo* info) {
	if (LooterCore::get()->need_operate() || HunterCore::get()->need_operate())
		return;

	Actions::execute_dance();
}

void UtilityCore::execute_eat_food(ScriptInfo* info) {
	Actions::eat_food(UINT32_MAX,info->get_value_string());
}

std::map<std::string, std::shared_ptr<ScriptInfo>> UtilityCore::get_map_scriptInfo(){
	return map_scriptInfo;
}