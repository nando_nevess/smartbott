#pragma once 
#include "PlayerInfo.h"
#include "Core\TibiaProcess.h"
#include "Core\Inventory.h"
#include "Core\HunterCore.h"
#include <mutex>

Json::Value NaviPlayerInfo::parse_class_to_json(){
	return TibiaProcess::get_default()->character_info->parse_class_to_json();
}
void NaviPlayerInfo::parse_json_to_class(Json::Value jsonValue){
	((std::mutex*)mtx_access)->lock();
	exp_to_next_lvl = jsonValue["exp_to_next_lvl"].asInt();
	experience = jsonValue["experience"].asInt();
	time_to_next_lvl = jsonValue["time_to_next_lvl"].asInt();

	get_name_char = jsonValue["get_name_char"].asString();

	self_level = jsonValue["level"].asInt();
	self_levelpc = jsonValue["get_level_pc"].asInt();
	self_ml = jsonValue["self_ml"].asInt();
	self_mlpc = jsonValue["self_mlpc"].asInt();
	self_first = jsonValue["first_lvl"].asInt();
	self_firstpc = jsonValue["first_pc"].asInt();
	self_club = jsonValue["club_lvl"].asInt();
	self_clubpc = jsonValue["club_pc"].asInt();
	self_sword = jsonValue["sword_lvl"].asInt();
	self_swordpc = jsonValue["sword_pc"].asInt();
	self_axe = jsonValue["axe_lvl"].asInt();
	self_axepc = jsonValue["axe_pc"].asInt();
	self_distance = jsonValue["distance_lvl"].asInt();
	self_distancepc = jsonValue["distance_pc"].asInt();

	self_shielding = jsonValue["shielding_lvl"].asInt();
	self_shieldingpc = jsonValue["shielding_pc"].asInt();
	self_fishing = jsonValue["fishing_lvl"].asInt();
	self_fishingpc = jsonValue["fishing_pc"].asInt();

	get_self_coordinate = Coordinate(jsonValue["get_self_coordinate_x"].asInt(), jsonValue["get_self_coordinate_y"].asInt(), jsonValue["get_self_coordinate_z"].asInt());

	self_maxhp = jsonValue["max_hp"].asInt();
	self_maxmp = jsonValue["max_mp"].asInt();
	current_hp = jsonValue["hp"].asInt();
	current_hppc = jsonValue["hppc"].asInt();
	current_mp = jsonValue["mp"].asInt();
	current_mppc = jsonValue["mppc"].asInt();
	self_cap = jsonValue["cap"].asInt();
	self_soul = jsonValue["soul"].asInt();
	self_stamina = jsonValue["stamina"].asInt();
	self_off_training = jsonValue["offline_training"].asInt();
	self_character_id = jsonValue["character_id"].asInt();
	target_red = jsonValue["target_red"].asInt();

	is_follow_mode = jsonValue["is_follow_mode"].asBool();
	is_walking = jsonValue["is_walking"].asBool();

	satus_water = jsonValue["status_water"].asBool();
	status_holly = jsonValue["status_holly"].asBool();
	status_frozen = jsonValue["status_frozen"].asBool();
	status_curse = jsonValue["status_curse"].asBool();
	status_battle_red = jsonValue["status_battle_red"].asBool();
	status_streng_up = jsonValue["status_streng_up"].asBool();
	status_pz = jsonValue["status_pz"].asBool();
	status_blood = jsonValue["status_blood"].asBool();
	status_poison = jsonValue["status_poison"].asBool();
	status_fire = jsonValue["status_fire"].asBool();
	status_energy = jsonValue["status_energy"].asBool();
	status_drunk = jsonValue["status_drunk"].asBool();
	status_slow = jsonValue["status_slow"].asBool();
	status_utamo = jsonValue["status_utamo"].asBool();
	status_haste = jsonValue["status_haste"].asBool();
	status_battle = jsonValue["status_battle"].asBool();
	((std::mutex*)mtx_access)->unlock();
}
NaviPlayerInfo::NaviPlayerInfo(){
	mtx_access = (uint32_t*)new std::mutex();
}