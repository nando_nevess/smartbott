#pragma once
#include "Core\Tile.h"
#include "Core\ItemsManager.h"
#include "ConfigPathManager.h"

Tile::Tile(tile_raw* tile_raw_ptr, Coordinate coord){
	raw_ptr = tile_raw_ptr;
	coordinate = coord;
} 

bool Tile::have_furniture_at_top(){
	return ItemsManager::get()->is_furniture_id(get_top_item_id());
}
//TODO: Andrei Revisar
uint32_t Tile::get_top_item_id(bool ignore_creatures){
	if (ignore_creatures){
		for (int i = 0; i < 9; i++){
			uint32_t temp = get_top_item_minus_index(i);
			if (temp != 99)
				return temp;
		}
	}
	else
		return get_top_item_minus_index(0);
	return UINT32_MAX;
}

uint32_t Tile::get_top_item_minus_index(uint32_t rindex){
	auto temp = this;

	if (!raw_ptr)
		return 0;

	if (rindex > 9)
		return UINT32_MAX;

	uint32_t index = std::min(((uint32_t)raw_ptr->stack_count - 1) - rindex, (uint32_t)9);
	auto raw_item = raw_ptr->get_raw_item_at_index(index);
	if (!raw_item)
		return UINT32_MAX;
	return raw_item->item_id;
}

uint32_t Tile::get_top_second_item_id(){
	return get_top_item_minus_index(1);
}

bool Tile::have_element_field_at_top(){
	return ItemsManager::get()->isRuneFieldItem(get_top_item_id());
}

bool Tile::have_element_field(){
	if (!raw_ptr)
		return false;

	uint32_t count = std::min(raw_ptr->stack_count, (uint32_t) 10);
	for (int i = count; i > 0; i--) 
		if (ItemsManager::get()->isRuneFieldItem(raw_ptr->raw_stacks[i - 1].item_id))
			return true;	

	return false;
}

std::shared_ptr<ConfigPathProperty> Tile::have_custom_item_cost(){
	if (!raw_ptr)
		return false;

	uint32_t count = std::min(raw_ptr->stack_count, (uint32_t)10);
	for (int i = count; i > 0; i--){
		int item_id = raw_ptr->raw_stacks[i - 1].item_id;
		std::shared_ptr<ConfigPathProperty> path_property = ItemsManager::get()->get_path_property_by_id(item_id);
		if (!path_property)
			continue;

		return path_property;
	}
	return nullptr;
}

bool Tile::is_item_id(){
	return ItemsManager::get()->isitem_id(get_top_item_id());
}

std::map<int, CreatureOnBattlePtr> Tile::get_monsters(){
	auto creatures = get_creatures();
	for (auto it = creatures.begin(); it != creatures.end();){
		if (!it->second->is_monster()){
			it = creatures.erase(it);
		}
		else
			it++;
	}
	return creatures;
}

std::map<int, CreatureOnBattlePtr> Tile::get_npcs(){
	auto creatures = get_creatures();
	for (auto it = creatures.begin(); it != creatures.end();){
		if (!it->second->is_npc()){
			it = creatures.erase(it);
		}
		else
			it++;
	}
	return creatures;
}

std::map<int, CreatureOnBattlePtr> Tile::get_summons(){
	auto creatures = get_creatures();
	for (auto it = creatures.begin(); it != creatures.end();){
		if (!it->second->is_summon()){
			it = creatures.erase(it);
		}
		else
			it++;
	}
	return creatures;
}


std::map<int, CreatureOnBattlePtr> Tile::get_creatures(){
	std::map<int, CreatureOnBattlePtr> retval;
	if (!contains_id(99))
		return retval;
	std::map<int, CreatureOnBattlePtr> creatures = BattleList::get()->get_creatures_at_coordinate(coordinate);
	for (auto creature : creatures)
		retval[creature.first] = creature.second;
	return retval;
}

std::map<int, CreatureOnBattlePtr> Tile::get_players(){
	std::map<int, CreatureOnBattlePtr> retval;
	if (!contains_id(99))
		return retval;
	std::map<int, CreatureOnBattlePtr> creatures = BattleList::get()->get_creatures_at_coordinate(coordinate);
	for (auto creature : creatures)
		if (creature.second->is_player())
			retval[creature.first] = creature.second;
	return retval;
}

bool Tile::contains_player(){
	if (!contains_creature())
		return false;
	std::map<int, CreatureOnBattlePtr> creatures = BattleList::get()->get_creatures_at_coordinate(coordinate);
	for (auto creature : creatures)
		if (creature.second->is_player())
			return true;
	return false;
}

bool Tile::contains_furniture(){
	if (!raw_ptr)
		return false;
	uint32_t count = std::min((uint32_t)raw_ptr->stack_count, (uint32_t)10);
	for (int i = count; i > 0; i--){
		if (ItemsManager::get()->
			is_furniture_id(raw_ptr->raw_stacks[ i - 1 ].item_id))
			return true;
	}
	return false;
}

bool Tile::contains_block_path_id(){
	//TODO
	return false;
}

bool Tile::contains_free_path_id(){
	//TODO
	return false;
}

bool Tile::contains_id(uint32_t item_id){
	if (!raw_ptr)
		return false;
	uint32_t count = std::min((uint32_t)raw_ptr->stack_count, (uint32_t)10);
	for (int i = count; i > 0; i--){
		if (item_id == raw_ptr->raw_stacks[i - 1].item_id)
			return true;
	}
	return false;
}

bool Tile::contains_monster(){
	std::map<int, CreatureOnBattlePtr> creatures = BattleList::get()->get_creatures_at_coordinate(coordinate);
	for (auto creature : creatures)
		if (creature.second->is_monster())
			return true;
	return false;
}

bool Tile::contains_summon(){
	std::map<int, CreatureOnBattlePtr> creatures = BattleList::get()->get_creatures_at_coordinate(coordinate);
	for (auto creature : creatures)
		if (creature.second->is_summon())
			return true;
	return false;
}

bool Tile::contains_npc(){
	//TODO
	return false;
}

bool Tile::contains_depot(){
	std::vector<uint32_t> depots_ids = ItemsManager::get()->get_depot_ids();
	for (auto depot_id : depots_ids){
		if (contains_id(depot_id))
			return true;
	}
	return false;
}

bool Tile::contains_creature(){
	return contains_id(99);
}

bool Tile::contains_hole_or_step(){
	if (!raw_ptr)
		return false;
	uint32_t count = std::min((uint32_t)raw_ptr->stack_count, (uint32_t)9);
	for (int i = count; i > 0; i--){
		CustomIdDetails* details = CustomIdAttributesManager::get()->get_item_attributes(raw_ptr->raw_stacks[i - 1].item_id);
		if (details){
			if (details->contains_attribute(attr_item_t::attr_item_step_up) || details->contains_attribute(attr_item_t::attr_item_step_down))
				return true;
		}
	}
	return false;
}

bool Tile::has_thing_attribute(ThingAttr attr){
	if (!raw_ptr)
		return false;

	uint32_t count = std::min((uint32_t) raw_ptr->stack_count, (uint32_t) 9);
	for (int i = count; i > 0; i--){
		auto type = ItemsManager::get()->getItemTypeById(raw_ptr->raw_stacks[i - 1].item_id);
		if (type)
			if (type->m_attribs.has(attr))
			  return true;
	}
	return false;
}

uint32_t Tile::get_thing_count(){
	if (!raw_ptr)
		return 0;
	return raw_ptr->stack_count;
}




CustomIdAttributes* Tile::find_attribute_details_type(attr_item_t attr_type){
	for (int stack_pos = 0; stack_pos < (int)raw_ptr->stack_count; stack_pos++){
		auto item_struct = raw_ptr->get_raw_item_at_index(stack_pos);
		CustomIdDetails* attr = CustomIdAttributesManager::get()->get_item_attributes(item_struct->item_id);
		if (!attr)
			continue;
		CustomIdAttributes* attr_id = attr->get_attribute(attr_type);
		if (attr_id)
			return attr_id;
	}
	return nullptr;
}


bool Tile::contains_item_attr(attr_item_t attr_type){
	for (int stack_pos = 0; stack_pos < (int)raw_ptr->stack_count; stack_pos++){
		auto item_struct = raw_ptr->get_raw_item_at_index(stack_pos);
		if (!item_struct)
			continue;

		CustomIdDetails* attr = CustomIdAttributesManager::get()->get_item_attributes(item_struct->item_id);
		if (!attr)
			continue;

		return attr->contains_attribute(attr_type);
	}
	return false;
}

bool Tile::is_hole(){
	return contains_item_attr(attr_item_t::attr_item_step_down);
}

bool Tile::is_step(){
	return contains_item_attr(attr_item_t::attr_item_step_up);
}

bool Tile::can_use_to_up(){
	return contains_item_attr(attr_item_t::attr_item_use_to_up);
}

bool Tile::can_use_to_down(){
	return contains_item_attr(attr_item_t::attr_item_use_to_down);
}

bool Tile::must_open_with_browse(){
	return contains_item_attr(attr_item_t::attr_item_force_loot_browse);
}

bool Tile::avoid_when_targeting(){
	return contains_item_attr(attr_item_t::attr_item_avoid_when_targeting);
}

bool Tile::is_teleport(){
	return contains_item_attr(attr_item_t::attr_item_use_to_teleport);
}

bool Tile::contains_brokable(){
	return contains_item_attr(attr_item_t::attr_item_can_broke);
}

bool Tile::override_walkable(){
	return contains_item_attr(attr_item_t::attr_item_override_walkable);
}

bool Tile::override_unwalkable(){
	return contains_item_attr(attr_item_t::attr_item_override_unwalkable);
}
Coordinate Tile::get_destination(){
	if (!is_teleport())
		return Coordinate();

	CustomIdAttributes* attr = find_attribute_details_type(attr_item_use_to_teleport);
	if (attr){
		std::string destination_string = attr->get_additional(attr_additional_t::attr_additional_destination_coord);
		return Coordinate(destination_string);
	}
	return Coordinate();
}

Coordinate Tile::get_required_pos(){
	if (!is_teleport() && !can_use_to_up() && !can_use_to_down())
		return Coordinate();

	CustomIdAttributes* attr = find_attribute_details_type(attr_item_use_to_teleport);
	if (attr){
		std::string destination_string = attr->get_additional(attr_additional_t::attr_additional_required_coord);
		return Coordinate(destination_string);
	}
	return Coordinate();
}