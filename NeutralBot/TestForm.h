#pragma once

#include "Core\Process.h"
#include "Core\InfoCore.h"
#include "Core\ItemsManager.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class TestForm : public System::Windows::Forms::Form{
	public:
		TestForm(void){
			InitializeComponent();
		}

	protected:
		~TestForm(){
			if (components)
				delete components;			
		}
	protected:
	private: Telerik::WinControls::UI::RadListView^  tibiaClientsList;
	private: Telerik::WinControls::UI::RadButton^  bt_update;
	private: Telerik::WinControls::UI::RadLabel^  label_level;
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::Windows::Forms::Button^  button2;

	private: System::ComponentModel::IContainer^  components;
	private:

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->label_level = (gcnew Telerik::WinControls::UI::RadLabel());
			this->bt_update = (gcnew Telerik::WinControls::UI::RadButton());
			this->tibiaClientsList = (gcnew Telerik::WinControls::UI::RadListView());
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->button2 = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_level))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_update))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tibiaClientsList))->BeginInit();
			this->SuspendLayout();
			// 
			// label_level
			// 
			this->label_level->Location = System::Drawing::Point(126, 12);
			this->label_level->Name = L"label_level";
			this->label_level->Size = System::Drawing::Size(38, 18);
			this->label_level->TabIndex = 1;
			this->label_level->Text = L"level : ";
			// 
			// bt_update
			// 
			this->bt_update->Location = System::Drawing::Point(126, 36);
			this->bt_update->Name = L"bt_update";
			this->bt_update->Size = System::Drawing::Size(110, 24);
			this->bt_update->TabIndex = 0;
			this->bt_update->Text = L"update";
			this->bt_update->Click += gcnew System::EventHandler(this, &TestForm::radButton1_Click);
			// 
			// tibiaClientsList
			// 
			this->tibiaClientsList->Dock = System::Windows::Forms::DockStyle::Left;
			this->tibiaClientsList->Location = System::Drawing::Point(0, 0);
			this->tibiaClientsList->Name = L"tibiaClientsList";
			this->tibiaClientsList->Size = System::Drawing::Size(120, 130);
			this->tibiaClientsList->TabIndex = 1;
			this->tibiaClientsList->Text = L"radListView1";
			this->tibiaClientsList->ItemMouseDoubleClick += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &TestForm::tibiaClientsList_ItemMouseDoubleClick);
			// 
			// button1
			// 
			this->button1->Location = System::Drawing::Point(126, 66);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(110, 23);
			this->button1->TabIndex = 2;
			this->button1->Text = L"button1";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &TestForm::button1_Click);
			// 
			// timer1
			// 
			this->timer1->Tick += gcnew System::EventHandler(this, &TestForm::timer1_Tick);
			// 
			// button2
			// 
			this->button2->Location = System::Drawing::Point(161, 95);
			this->button2->Name = L"button2";
			this->button2->Size = System::Drawing::Size(75, 23);
			this->button2->TabIndex = 3;
			this->button2->Text = L"button2";
			this->button2->UseVisualStyleBackColor = true;
			this->button2->Click += gcnew System::EventHandler(this, &TestForm::button2_Click);
			// 
			// TestForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(247, 130);
			this->Controls->Add(this->button2);
			this->Controls->Add(this->button1);
			this->Controls->Add(this->tibiaClientsList);
			this->Controls->Add(this->bt_update);
			this->Controls->Add(this->label_level);
			this->Name = L"TestForm";
			this->Text = L"Test";
			this->TopMost = true;
			this->Load += gcnew System::EventHandler(this, &TestForm::TestForm_Load);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_level))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_update))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tibiaClientsList))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

	private: System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
				 tibiaClientsList->Items->Clear();
				 Process::get()->RefresherTibiaClients();
				 auto processes = Process::get()->GetProcessesMap();
				 for (auto process : processes)
					 tibiaClientsList->Items->Add(process.second.pid + "|" + gcnew String(&process.second.player_name[0]));
	}
	private: System::Void tibiaClientsList_ItemMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
				 TibiaProcess::get_default()->set_pid(Convert::ToInt32(e->Item->Text->Split('|')[0]));
				 label_level->Text = "Level : " + TibiaProcess::get_default()->character_info->level();
				 timer1->Enabled = true;
	}
private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
			 InfoCore::get()->is_player_attack();
			/* tibia_message message;
			 Messages::get()->refresh_server_log();
			 Messages::get()->refresh_tibia_message();*/
			 //Messages::get()->on_new_message(message);
			 auto processes = InfoCore::get()->get_itens_used_map();
			 for (auto process : processes)
			  std::cout << process.first << " // " << process.second << "\n";
}
private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			 Messages::get()->refresh_server_log();
			 Messages::get()->refresh_tibia_message();

			 //std::cout << "\n" << InfoCore::get()->monsterkilledmap[];
			 //auto processes = InfoCore::get()->get_monster_killed_map();
			 //for (auto process : processes)
				// //tibiaClientsList->Items->Add(gcnew String(&process.first[0]) + "|" + Convert::ToString(process.second));
				// std::cout << process.first << " // " << process.second << "\n";
}
private: System::Void button2_Click(System::Object^  sender, System::EventArgs^  e) {
}
private: System::Void TestForm_Load(System::Object^  sender, System::EventArgs^  e) {
}
};
}
