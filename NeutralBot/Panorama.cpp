#pragma once
#include "Panorama.h"
#include "SelectedCharacter.h"
#include "RestoreScript.h"

using namespace NeutralBot;

System::Void Panorama::menu_restore_Click(System::Object^  sender, System::EventArgs^  e) {
	NeutralBot::RestoreScript::ShowUnique();
}

System::Void Panorama::menu_client_Click(System::Object^  sender, System::EventArgs^  e) {
	/*closeAllOtherForms();

	this->Hide();

	NeutralBot::FastUIPanelController::get()->on_resize_option_change(false);
	NeutralBot::SelectedCharacter::ShowUnique(0);

	if (GeneralManager::get()->get_interface_auto_resize())
		NeutralBot::FastUIPanelController::get()->on_resize_option_change(true);	

	this->Show();*/
}

System::Void Panorama::pingUpdater_Tick(System::Object^  sender, System::EventArgs^  e) {
	if (NeutralBot::SelectedCharacter::unique)
		return;

	if (!TibiaProcess::get_default()->is_process_running())
		menu_client_Click(nullptr, nullptr);

	this->Text = Convert::ToString(gcnew String(TibiaProcess::get_default()->character_info->name.c_str()) + " - Ping " + TibiaProcess::get_default()->get_ping() + " - BETA Version");

	if (TibiaProcess::get_default()->character_info->stamina() <= 0)
		return;

	label_check_hour->Text = Convert::ToString(String::Format("{0:0,0}", (int)TibiaProcess::get_default()->character_info->exp_hour()));
	label_balance->Text = Convert::ToString(String::Format("{0:0,0}", (int)InfoCore::get()->get_last_balance()));
	label_stamina->Text = Convert::ToString(ToTime((float)((float)TibiaProcess::get_default()->character_info->stamina() / 60.0f)));		
	label_profit_hour->Text = Convert::ToString(InfoCore::get()->profit_hour());

	if (NeutralManager::get()->get_bool_load()){
		closeAllOtherForms();
		managed_util::ManagedControlsHook::get()->updateButtonsByVars();
		NeutralManager::get()->set_bool_load(false);
	}
}