#pragma once
#include "SellCore.h"

uint32_t SellCore::get_count_sell(){
	const std::string message = Messages::get_server_message_where_contains("Sold");

	std::regex rgx("Sold (.*?)x (.*?) for (.*?) gold");
	std::smatch match;

	if (std::regex_search(message.begin(), message.end(), match, rgx)){
		std::string countItemSellString = std::string(match[1]);
		std::string itemName = std::string(match[2]);
		std::string valueItemSellString = std::string(match[3]);

		int countItemSell = std::atoi(countItemSellString.c_str());
		int valueItemSell = std::atoi(valueItemSellString.c_str());
		int item_id = ItemsManager::get()->getitem_idFromName(itemName);

		std::shared_ptr<ItemsInfoLooted> itemInfoLootedRule = LooterCore::get()->getRuleItemsLooted(item_id);
		if (itemInfoLootedRule)
		if (itemInfoLootedRule->buy_price <= 0)
			itemInfoLootedRule->buy_price = valueItemSell / countItemSell;

		return countItemSell;
	}
	return 0;
}

bool SellCore::open_trade(){
	Inventory::get()->minimize();
	for (int trying_loop = 0; trying_loop <= 3; trying_loop++){

		Actions::get()->says("Hi", "NPCs");
		TibiaProcess::get_default()->wait_ping_delay(4);
		Actions::get()->says("Trade", "NPCs");

		if (ContainerManager::get()->wait_open_trade(500))
			return true;
	}
	return false;
}

bool SellCore::sell_items(std::string sell_item_id){
	Actions::reopen_containers();

	if (!NpcTrade::get()->is_open())
		RepoterCore::open_trade(std::vector<std::string>());

	auto sellItems = SellManager::get()->getSellItems(sell_item_id);
	if (!sellItems)
		return false;

	int container_id = sellItems->get_backpack_loot_id();
	
	Actions::get()->open_container_while_hast_next(container_id);

	ItemContainerPtr container = ContainerManager::get()->get_container_by_id(container_id);
	if (!container)
		return false;

	TimeChronometer time2;
	do{
		auto map_items = sellItems->get_mapItemStruc();
		if (map_items.size() <= 0)
			return false;

		for (auto item : map_items) {
			Sleep(10);

			int item_id = ItemsManager::get()->getitem_idFromName(item.second->get_item_name());
			if (item_id == 0 || item_id == UINT32_MAX)
				continue;

			TimeChronometer time;
			int itemSell = 0;
			int count = 0;
			int itemToSell = item.second->get_item_qty();
			while (time.elapsed_milliseconds() <= 3000){
				container = ContainerManager::get()->get_container_by_id(container_id);
				if (!container)
					break;

				TibiaProcess::get_default()->wait_ping_delay(1.0);

				int itemCurrentCount = container->get_item_count_by_id(item_id);
				if (itemCurrentCount <= 0)
					break;

				if (itemSell >= itemToSell && itemToSell > 0)
					break;

				if (itemToSell <= 0)
					itemToSell = itemCurrentCount;

				NpcTrade::get()->sell_item(item_id, itemToSell);

				TibiaProcess::get_default()->wait_ping_delay(2.0);

				int current_item_sell = get_count_sell();
				if (current_item_sell > 0 && current_item_sell < 100 && itemToSell <= 0)
					break;
				else if (current_item_sell == 100){
					itemSell += current_item_sell;
					continue;
				}

				itemSell += current_item_sell;
				count++;
			}
		}
		
		container = ContainerManager::get()->get_container_by_id(container_id);
		if (!container)
			break;

		if (!container->up_container())
			break;

		TibiaProcess::get_default()->wait_ping_delay(2.5);

	} while (time2.elapsed_milliseconds() <= 20000);
	return false;
}