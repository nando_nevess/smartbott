#include <iostream>
#include <fstream>
#include <string>

class Logger{
public:
	static void add_error_log(std::string file, std::string error_str);
	static Logger* get(){
		static Logger* mLogger = nullptr;
		if (!mLogger)
			mLogger = new Logger;
		return mLogger;
	}
};


