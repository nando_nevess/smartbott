#pragma once
#include "FormEditor.h"
#include "LooterManager.h"
#include "SpellsInfo.h"
#include "RepoterManager.h"
#include "DepoterManager.h"
#include "LuaThread.h"
#include "SpellManager.h"
#include "LuaBackground.h"
#include "ManagedUtil.h"
#include "LanguageManager.h"
#include "Core\ItemsManager.h"
#include "Core\Util.h"

using namespace NeutralBot;

void FormEditor::creatContextMenu(){
	ItemGroup = gcnew Telerik::WinControls::UI::RadMenuItem("Group");
	ItemCheckBox = gcnew Telerik::WinControls::UI::RadMenuItem("CheckBox");
	ItemButton = gcnew Telerik::WinControls::UI::RadMenuItem("Button");
	ItemTextBox = gcnew Telerik::WinControls::UI::RadMenuItem("TextBox");
	ItemLabel = gcnew Telerik::WinControls::UI::RadMenuItem("Label");
	ItemCombo = gcnew Telerik::WinControls::UI::RadMenuItem("ComboBox");
	ItemComboBoxItem = gcnew Telerik::WinControls::UI::RadMenuItem("ItemList");
	ItemComboBoxbp = gcnew Telerik::WinControls::UI::RadMenuItem("BackpackList");
	ItemSpinEditor = gcnew Telerik::WinControls::UI::RadMenuItem("SpinEditor");
	ItemEdit = gcnew Telerik::WinControls::UI::RadMenuItem("Edit");
	ItemLooterGroup = gcnew Telerik::WinControls::UI::RadMenuItem("LooterGroup");
	ItemRepoterGroup = gcnew Telerik::WinControls::UI::RadMenuItem("RepoterGroup");
	ItemDepoterGroup = gcnew Telerik::WinControls::UI::RadMenuItem("DepoterGroup");
	ItemSpellsAttackGroup = gcnew Telerik::WinControls::UI::RadMenuItem("SpellsAttackGroup");
	ItemSpellsHealthGroup = gcnew Telerik::WinControls::UI::RadMenuItem("SpellsHealthGroup");

	ItemSpellsAttackGroup->Click += gcnew System::EventHandler(this, &FormEditor::MenuItemSpellsAttackGroup_Click);
	ItemSpellsHealthGroup->Click += gcnew System::EventHandler(this, &FormEditor::MenuItemSpellsHealthGroup_Click);
	ItemRepoterGroup->Click += gcnew System::EventHandler(this, &FormEditor::MenuItemRepoterGroup_Click);
	ItemDepoterGroup->Click += gcnew System::EventHandler(this, &FormEditor::MenuItemDepoterGroup_Click);
	ItemLooterGroup->Click += gcnew System::EventHandler(this, &FormEditor::MenuItemLooterGroup_Click);
	ItemCheckBox->Click += gcnew System::EventHandler(this, &FormEditor::MenuCheckBox_Click);
	ItemGroup->Click += gcnew System::EventHandler(this, &FormEditor::MenuGroup_Click);
	ItemButton->Click += gcnew System::EventHandler(this, &FormEditor::MenuButton_Click);
	ItemTextBox->Click += gcnew System::EventHandler(this, &FormEditor::MenuTextBox_Click);
	ItemLabel->Click += gcnew System::EventHandler(this, &FormEditor::MenuLabel_Click);
	ItemCombo->Click += gcnew System::EventHandler(this, &FormEditor::MenuCombo_Click);
	ItemComboBoxItem->Click += gcnew System::EventHandler(this, &FormEditor::MenuComboBoxItem_Click);
	ItemComboBoxbp->Click += gcnew System::EventHandler(this, &FormEditor::MenuComboBoxBp_Click);
	ItemSpinEditor->Click += gcnew System::EventHandler(this, &FormEditor::MenuSpinEditor_Click);
	ItemEdit->Click += gcnew System::EventHandler(this, &FormEditor::MenuEdit_Click);
}

System::Void FormEditor::MenuControls_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
	if (!TreeViewControls->SelectedNode){
		e->Cancel = true;
		return;
	}

	if (TreeViewControls->SelectedNode->Text == "LooterGroup"){
		e->Cancel = true;
		return;
	}

	if (TreeViewControls->SelectedNode->Text == "DepoterGroup"){
		e->Cancel = true;
		return;
	}

	if (TreeViewControls->SelectedNode->Text == "SpellsAttackGroup"){
		e->Cancel = true;
		return;
	}

	if (TreeViewControls->SelectedNode->Text == "SpellsHealthGroup"){
		e->Cancel = true;
		return;
	}

	if (TreeViewControls->SelectedNode->Text == "RepoterGroup"){
		e->Cancel = true;
		return;
	}

	if (TreeViewControls->SelectedNode->Tag == "group"){
		MenuControls->Items->Clear();
		MenuControls->Items->Add(ItemButton);
		MenuControls->Items->Add(ItemTextBox);
		MenuControls->Items->Add(ItemLabel);
		MenuControls->Items->Add(ItemCombo);
		MenuControls->Items->Add(ItemComboBoxItem);
		MenuControls->Items->Add(ItemComboBoxbp);
		MenuControls->Items->Add(ItemSpinEditor);
		MenuControls->Items->Add(ItemCheckBox);
		return;
	}

	else if (TreeViewControls->SelectedNode->Text == "Controls"){
		MenuControls->Items->Clear();
		MenuControls->Items->Add(ItemGroup);
		MenuControls->Items->Add(ItemButton);
		MenuControls->Items->Add(ItemTextBox);
		MenuControls->Items->Add(ItemLabel);
		MenuControls->Items->Add(ItemCombo);
		MenuControls->Items->Add(ItemComboBoxItem);
		MenuControls->Items->Add(ItemComboBoxbp);
		MenuControls->Items->Add(ItemSpinEditor);
		MenuControls->Items->Add(ItemCheckBox);

		if (!ControlManager::get()->checkExistControl("LooterGroup"))
			MenuControls->Items->Add(ItemLooterGroup);

		if (!ControlManager::get()->checkExistControl("RepoterGroup"))
			MenuControls->Items->Add(ItemRepoterGroup);

		if (!ControlManager::get()->checkExistControl("SpellsAttackGroup"))
			MenuControls->Items->Add(ItemSpellsAttackGroup);

		if (!ControlManager::get()->checkExistControl("SpellsHealthGroup"))
			MenuControls->Items->Add(ItemSpellsHealthGroup);

		if (!ControlManager::get()->checkExistControl("DepoterGroup"))
			MenuControls->Items->Add(ItemDepoterGroup);

		return;
	}

	else if (TreeViewControls->SelectedNode->Tag == "button"){
		MenuControls->Items->Clear();
		MenuControls->Items->Add(ItemEdit);
		return;
	}

	if (TreeViewControls->SelectedNode->Text != "Controls" || TreeViewControls->SelectedNode->Tag != "group"){
		e->Cancel = true;
		return;
	}
}

void FormEditor::updateLooterGroup(){
	Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control("LooterGroup");
	if (!groupControl)
		return;

	int label_positionX = 23;
	int positionX = 40;
	int endPositionX = 40 * groupControl->Controls->Count;

	groupControl->Size = System::Drawing::Size(160, endPositionX);

	for each (auto control in groupControl->Controls){
		if (Telerik::WinControls::UI::RadLabel::typeid == control->GetType()){
			Telerik::WinControls::UI::RadLabel^ label_control = ((Telerik::WinControls::UI::RadLabel^)control);

			label_control->Location = System::Drawing::Point(10, label_positionX);
			label_positionX = label_positionX + 45;
			continue;
		}

		if (Telerik::WinControls::UI::RadDropDownList::typeid != control->GetType())
			continue;

		Telerik::WinControls::UI::RadDropDownList^ list_control = ((Telerik::WinControls::UI::RadDropDownList^)control);

		for each(auto item in list_control->Items){
			if (item->Text->Trim()->ToLower() != list_control->Text->Trim()->ToLower())
				continue;

			list_control->SelectedItem = item;
		}		

		list_control->SetBounds(10, positionX, list_control->Width, list_control->Height);
		positionX = positionX + 46;
	}

	groupControl->Size = System::Drawing::Size(160, positionX - 16);
}

void FormEditor::updateRepoterGroup(){	
	Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control("RepoterGroup");
	if (!groupControl)
		return;

	int control_positionX = 155;
	int control_positionY = 23;
	int control_positionY_spin = 23;
	int endPositionX = 40 * groupControl->Controls->Count;

	groupControl->Size = System::Drawing::Size(1000, 800);

	int count_index = 0;
	String^ control_main = "";

	for each (auto control in groupControl->Controls){
		if (Telerik::WinControls::UI::RadSpinEditor::typeid == control->GetType()){
			Telerik::WinControls::UI::RadSpinEditor^ control_in = ((Telerik::WinControls::UI::RadSpinEditor^)control);
			
			if (control_in->Name->IndexOf("min") >= 0)
				control_in->Location = System::Drawing::Point(155, control_positionY_spin);			
			else
				control_in->Location = System::Drawing::Point(202, control_positionY_spin);
		
			count_index++;

			if (count_index >= 2){
				control_positionY_spin = control_positionY_spin + 25;
				count_index = 0;
			}

			control_in->Size = System::Drawing::Size(42, 22);
		}

		if (Telerik::WinControls::UI::RadDropDownList::typeid == control->GetType()){
			Telerik::WinControls::UI::RadDropDownList^ control_in = ((Telerik::WinControls::UI::RadDropDownList^)control);
			
			control_in->Location = System::Drawing::Point(10, control_positionY);
			control_positionY = control_positionY + 25;		
			control_in->Size = System::Drawing::Size(140, 22);

			for each(auto item in control_in->Items){
				if (item->Text->Trim()->ToLower() != control_in->Text->Trim()->ToLower())
					continue;

				control_in->SelectedItem = item;
			}
		}
	}

	groupControl->Size = System::Drawing::Size(253, control_positionY + 5);

}

void FormEditor::updateDepoterGroup(){
	Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control("DepoterGroup");
	if (!groupControl)
		return;

	int label_positionX = 23;
	int positionX = 40;
	int endPositionX = 40 * groupControl->Controls->Count;

	groupControl->Size = System::Drawing::Size(160, endPositionX);

	for each (auto control in groupControl->Controls){
		if (Telerik::WinControls::UI::RadLabel::typeid == control->GetType()){
			Telerik::WinControls::UI::RadLabel^ label_control = ((Telerik::WinControls::UI::RadLabel^)control);

			label_control->Location = System::Drawing::Point(10, label_positionX);
			label_positionX = label_positionX + 45;
			continue;
		}

		if (Telerik::WinControls::UI::RadDropDownList::typeid != control->GetType())
			continue;

		Telerik::WinControls::UI::RadDropDownList^ list_control = ((Telerik::WinControls::UI::RadDropDownList^)control);

		for each(auto item in list_control->Items){
			if (item->Text->Trim()->ToLower() != list_control->Text->Trim()->ToLower())
				continue;

			list_control->SelectedItem = item;
		}

		list_control->SetBounds(10, positionX, list_control->Width, list_control->Height);
		positionX = positionX + 46;
	}

	groupControl->Size = System::Drawing::Size(160, positionX - 16);
}

void FormEditor::updateSpellsAttackGroup(){
	Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control("SpellsAttackGroup");
	if (!groupControl)
		return;

	int label_positionX = 23;
	int positionX = 17;
	int endPositionX = 40 * groupControl->Controls->Count;

	groupControl->Size = System::Drawing::Size(150, endPositionX);

	for each (auto control in groupControl->Controls){
		if (Telerik::WinControls::UI::RadCheckBox::typeid != control->GetType())
			continue;

		Telerik::WinControls::UI::RadCheckBox^ list_control = ((Telerik::WinControls::UI::RadCheckBox^)control);

		list_control->SetBounds(10, positionX, list_control->Width, list_control->Height);
		positionX = positionX + 20;
	}

	
	std::map<int, std::shared_ptr<SpellAttack>>& myMap = SpellManager::get()->getAttackList();
	for (auto container : myMap){
		String^ spells_id = managed_util::fromString(std::to_string(container.first) + "Attack");
		auto controlRule_ = getControlChieldRule("SpellsAttackGroup", spells_id);
		if (!controlRule_)
			continue;

		controlRule_->set_property("state", container.second->get_checked() ? "true" : "false");
	}

	groupControl->Size = System::Drawing::Size(160, positionX + 5);
}

void FormEditor::updateSpellsHealthGroup(){
	Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control("SpellsHealthGroup");
	if (!groupControl)
		return;

	int label_positionX = 23;
	int positionX = 17;
	int endPositionX = 40 * groupControl->Controls->Count;

	groupControl->Size = System::Drawing::Size(150, endPositionX);

	for each (auto control in groupControl->Controls){
		if (Telerik::WinControls::UI::RadCheckBox::typeid != control->GetType())
			continue;

		Telerik::WinControls::UI::RadCheckBox^ list_control = ((Telerik::WinControls::UI::RadCheckBox^)control);

		list_control->SetBounds(10, positionX, list_control->Width, list_control->Height);
		positionX = positionX + 20;
	}

	std::map<int, std::shared_ptr<SpellHealth>>& myMap = SpellManager::get()->getHealthList();
	for (auto container : myMap){
		String^ spells_id = managed_util::fromString(std::to_string(container.first) + "Health");
		auto controlRule_ = getControlChieldRule("SpellsHealthGroup", spells_id);
		if (!controlRule_)
			continue;

		controlRule_->set_property("state", container.second->get_checked() ? "true" : "false");
	}

	groupControl->Size = System::Drawing::Size(160, positionX + 5);
}

void FormEditor::creatEvents(){
	TreeViewControls->NodeRemoving += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewCancelEventHandler(this, &FormEditor::TreeViewControls_NodeRemoving);
	TreeViewControls->Editing += gcnew Telerik::WinControls::UI::TreeNodeEditingEventHandler(this, &FormEditor::TreeViewControls_Editing);
	TreeViewControls->ValueChanging += gcnew Telerik::WinControls::UI::TreeNodeValueChangingEventHandler(this, &FormEditor::TreeViewControls_ValueChanging);
	TreeViewControls->NodeMouseDoubleClick += gcnew Telerik::WinControls::UI::RadTreeView::TreeViewEventHandler(this, &FormEditor::TreeViewControls_NodeMouseDoubleClick);
	TreeViewControls->SelectedNodeChanged += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewEventHandler(this, &FormEditor::TreeViewControls_SelectedNodeChanged);
}

bool FormEditor::changeChieldId(bool group, String^ groupKey, String^ controlKey, String^ newControlKey){
	if (group){
		auto controlRule = ControlManager::get()->getControlInfoRule(managed_util::fromSS(groupKey));

		if (!controlRule)
			return false;

		return controlRule->changeId(managed_util::fromSS(newControlKey), managed_util::fromSS(controlKey));
	}
	return ControlManager::get()->changeId(managed_util::fromSS(newControlKey), managed_util::fromSS(controlKey));
}

System::Void FormEditor::TreeViewControls_ValueChanging(System::Object^  sender, Telerik::WinControls::UI::TreeNodeValueChangingEventArgs^  e) {
	if (e->Node->Text == TreeViewControls->TopNode->Text){
		e->Cancel = true;
		return;
	}

	if (e->Node->Text == "LooterGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Text == "RepoterGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Text == "DepoterGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Text == "SpellsAttackGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Text == "SpellsHealthGroup"){
		e->Cancel = true;
		return;
	}

	if (!e->Node)
		return;

	Control^ control = get_control(oldValue);

	if (!control)
		return;

	for each(auto items in e->Node->Parent->Nodes)
	if (e->NewValue->ToString() == items->Text)
		e->Cancel = true;

	if (Telerik::WinControls::UI::RadButton::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadButton^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId((e->Node->Parent->Tag == "group"), e->Node->Parent->Text, oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
	else if (Telerik::WinControls::UI::RadGroupBox::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadGroupBox^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId(false, "", oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
	else if (Telerik::WinControls::UI::RadTextBox::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadTextBox^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId((e->Node->Parent->Tag == "group"), e->Node->Parent->Text, oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
	else if (Telerik::WinControls::UI::RadSpinEditor::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadSpinEditor^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId((e->Node->Parent->Tag == "group"), e->Node->Parent->Text, oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
	else if (Telerik::WinControls::UI::RadDropDownList::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadDropDownList^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId((e->Node->Parent->Tag == "group"), e->Node->Parent->Text, oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
	else if (Telerik::WinControls::UI::RadLabel::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadLabel^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId((e->Node->Parent->Tag == "group"), e->Node->Parent->Text, oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
	else if (Telerik::WinControls::UI::RadCheckBox::typeid == control->GetType()){
		((Telerik::WinControls::UI::RadCheckBox^)control)->Name = e->NewValue->ToString();
		if (!changeChieldId((e->Node->Parent->Tag == "group"), e->Node->Parent->Text, oldValue, e->NewValue->ToString()))
			e->Cancel = true;
	}
}
System::Void FormEditor::TreeViewControls_Editing(System::Object^  sender, Telerik::WinControls::UI::TreeNodeEditingEventArgs^  e) {
	if (e->Node->Text != TreeViewControls->TopNode->Text)
		oldValue = e->Node->Text;

	if (!e->Node->Parent)
		return;

	if (e->Node->Text == "LooterGroup" || e->Node->Parent->Text == "LooterGroup"){
		oldValue = e->Node->Text;
		e->Cancel = true;
	}

	if (e->Node->Text == "RepoterGroup" || e->Node->Parent->Text == "RepoterGroup"){
		oldValue = e->Node->Text;
		e->Cancel = true;
	}

	if (e->Node->Text == "DepoterGroup" || e->Node->Parent->Text == "DepoterGroup"){
		oldValue = e->Node->Text;
		e->Cancel = true;
	}

	if (e->Node->Text == "SpellsAttackGroup" || e->Node->Parent->Text == "SpellsAttackGroup"){
		oldValue = e->Node->Text;
		e->Cancel = true;
	}

	if (e->Node->Text == "SpellsHealthGroup" || e->Node->Parent->Text == "SpellsHealthGroup"){
		oldValue = e->Node->Text;
		e->Cancel = true;
	}
}
System::Void FormEditor::TreeViewControls_NodeRemoving(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewCancelEventArgs^  e) {
	if (dragInProgress){
		e->Cancel = true;
		return;
	}

	if (e->Node->Text == TreeViewControls->TopNode->Text){
		e->Cancel = true;
		return;
	}

	if (!e->Node->Parent){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text == "LooterGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text == "RepoterGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text == "DepoterGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text == "SpellsAttackGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text == "SpellsHealthGroup"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text != TreeViewControls->TopNode->Text){
		auto parentControl = get_control(e->Node->Parent->Text);

		if (!parentControl){
			e->Cancel = true;
			return;
		}

		array<Control^>^ controlChield = parentControl->Controls->Find(e->Node->Text, true);

		parentControl->Controls->Remove(controlChield[0]);

		auto controlRule = getControlRule(e->Node->Parent->Text);

		if (!controlRule)
			return;

		controlRule->removeControl(managed_util::fromSS(e->Node->Text));
		return;
	}

	radPanel3->Controls->Remove(get_control(e->Node->Text));
	ControlManager::get()->removeControl(managed_util::fromSS(e->Node->Text));
}
System::Void FormEditor::TreeViewControls_NodeMouseDoubleClick(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
	if (disable_update2)
		return;

	if (e->Node->Text == "LooterGroup")
		return;

	if (e->Node->Text == "RepoterGroup")
		return;

	if (e->Node->Text == "DepoterGroup")
		return;

	if (e->Node->Text == "SpellsAttackGroup")
		return;

	if (e->Node->Text == "SpellsHealthGroup")
		return;

	if (e->Node->Text != TreeViewControls->TopNode->Text)
		e->Node->BeginEdit();
}
System::Void FormEditor::TreeViewControls_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
	if (disable_update2)
		return;

	if (!edit_mode)
		return;

	if (!e->Node)
		return;

	System::Object^ object = (System::Object^)get_control(e->Node->Text);

	if (!object)
		return;

	radPropertyGrid1->SelectedObject = object;
	filterProperty(radPropertyGrid1->Items);
}

Telerik::WinControls::UI::RadButton^ FormEditor::createButton(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size){
	Telerik::WinControls::UI::RadButton^ ControlButton = (gcnew Telerik::WinControls::UI::RadButton());

	ControlButton->Location = point;
	ControlButton->Name = name;
	ControlButton->Size = size;
	ControlButton->TabIndex = 0;
	ControlButton->Text = text;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode();

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "button";

	if (group)
		TreeViewControls->Nodes[TreeViewControls->TopNode->Text]->Nodes[group->Name]->Nodes->AddRange(NewNode);
	else
		TreeViewControls->TopNode->Nodes->AddRange(NewNode);

	ControlButton->Click += gcnew System::EventHandler(this, &FormEditor::EventClick);
	ControlButton->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlButton->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlButton->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlButton->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlButton->Resize += gcnew System::EventHandler(this, &FormEditor::EventResize);
	ControlButton->TextChanged += gcnew System::EventHandler(this, &FormEditor::Event_TextChanged);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlButton;
	filterProperty(radPropertyGrid1->Items);

	return ControlButton;
}
Telerik::WinControls::UI::RadGroupBox^ FormEditor::createGroup(String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size){
	Telerik::WinControls::UI::RadGroupBox^ ControlGroupBox = (gcnew Telerik::WinControls::UI::RadGroupBox());

	ControlGroupBox->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
	ControlGroupBox->HeaderText = text;
	ControlGroupBox->Location = point;
	ControlGroupBox->Name = name;
	ControlGroupBox->Size = size;
	ControlGroupBox->TabIndex = 0;
	ControlGroupBox->Text = text;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode();

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "group";

	ControlGroupBox->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlGroupBox->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlGroupBox->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlGroupBox->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlGroupBox->Resize += gcnew System::EventHandler(this, &FormEditor::EventResize);
	ControlGroupBox->TextChanged += gcnew System::EventHandler(this, &FormEditor::Event_TextChanged);

	TreeViewControls->TopNode->Nodes->AddRange(NewNode);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlGroupBox;
	filterProperty(radPropertyGrid1->Items);

	return ControlGroupBox;
}
Telerik::WinControls::UI::RadTextBox^ FormEditor::createTextBox(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size){
	Telerik::WinControls::UI::RadTextBox^ ControlText = gcnew Telerik::WinControls::UI::RadTextBox();

	ControlText->Location = point;
	ControlText->Name = name;
	ControlText->Text = text;
	ControlText->Size = size;
	ControlText->TabIndex = 0;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode();

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "text";

	if (group)
		TreeViewControls->Nodes[TreeViewControls->TopNode->Text]->Nodes[group->Name]->Nodes->AddRange(NewNode);
	else
		TreeViewControls->TopNode->Nodes->AddRange(NewNode);

	ControlText->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlText->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlText->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlText->TextChanged += gcnew System::EventHandler(this, &FormEditor::Event_TextChanged);
	ControlText->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlText->Resize += gcnew System::EventHandler(this, &FormEditor::EventResize);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlText;
	filterProperty(radPropertyGrid1->Items);

	return ControlText;
}
Telerik::WinControls::UI::RadCheckBox^ FormEditor::createCheckBox(Control^ group, bool state, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size){
	Telerik::WinControls::UI::RadCheckBox^ ControlCheck = gcnew Telerik::WinControls::UI::RadCheckBox();

	ControlCheck->Location = point;
	ControlCheck->Name = name;
	ControlCheck->Text = text;
	ControlCheck->Size = size;
	ControlCheck->Checked = state;
	ControlCheck->TabIndex = 0;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode();

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "check";

	if (group){
		TreeViewControls->Nodes[TreeViewControls->TopNode->Text]->Nodes[group->Name]->Nodes->AddRange(NewNode);
		
		if (group->Name == "SpellsAttackGroup" || group->Name == "SpellsHealthGroup")
			ControlCheck->CheckStateChanged += gcnew System::EventHandler(this, &FormEditor::Spells_StateChanged);
	}
	else
		TreeViewControls->TopNode->Nodes->AddRange(NewNode);


	ControlCheck->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlCheck->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlCheck->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlCheck->TextChanged += gcnew System::EventHandler(this, &FormEditor::Event_TextChanged);
	ControlCheck->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlCheck->CheckStateChanged += gcnew System::EventHandler(this, &FormEditor::EventCheckStateChanged);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlCheck;
	filterProperty(radPropertyGrid1->Items);

	return ControlCheck;
}
Telerik::WinControls::UI::RadSpinEditor^ FormEditor::createSpinEditor(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size, double max, double min, int value){
	Telerik::WinControls::UI::RadSpinEditor^ ControlSpin = gcnew Telerik::WinControls::UI::RadSpinEditor();

	ControlSpin->Location = point;
	ControlSpin->Name = name;
	ControlSpin->Size = size;
	ControlSpin->TabIndex = 0;
	ControlSpin->Maximum = Convert::ToInt32(max);
	ControlSpin->Minimum = Convert::ToInt32(min);
	ControlSpin->Value = value;
	ControlSpin->TabStop = false;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode();

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "spin";

	if (group){
		TreeViewControls->Nodes[TreeViewControls->TopNode->Text]->Nodes[group->Name]->Nodes->AddRange(NewNode);
		if (group->Name == "RepoterGroup")
			ControlSpin->ValueChanged += gcnew System::EventHandler(this, &FormEditor::Repoter_ValueChanged);
	}
	else
		TreeViewControls->TopNode->Nodes->AddRange(NewNode);


	ControlSpin->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlSpin->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlSpin->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlSpin->ValueChanged += gcnew System::EventHandler(this, &FormEditor::EventValueChanged);
	ControlSpin->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlSpin->Resize += gcnew System::EventHandler(this, &FormEditor::EventResize);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlSpin;
	filterProperty(radPropertyGrid1->Items);

	return ControlSpin;
}
Telerik::WinControls::UI::RadDropDownList^ FormEditor::createComboBox(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size, array<String^>^ items){
	Telerik::WinControls::UI::RadDropDownList^ ControlCombo = gcnew Telerik::WinControls::UI::RadDropDownList();

	ControlCombo->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
	ControlCombo->Location = point;
	ControlCombo->Name = name;
	ControlCombo->Size = size;
	ControlCombo->TabIndex = 0;

	if (items->Length > 0){
		ControlCombo->DropDownListElement->ItemHeight = 20;
		ControlCombo->DropDownListElement->DropDownHeight = 30 * 8;
		ControlCombo->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
	}

	ControlCombo->BeginUpdate();
	for each (auto itemName in items){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = (String^)itemName;
		item->Image = managed_util::get_item_image((String^)itemName, 18);
		ControlCombo->Items->Add(item);
	}
	ControlCombo->EndUpdate();

	ControlCombo->Text = text;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode(name);

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "combobox";

	if (group)
		TreeViewControls->Nodes[TreeViewControls->TopNode->Text]->Nodes[group->Name]->Nodes->AddRange(NewNode);
	else
		TreeViewControls->TopNode->Nodes->AddRange(NewNode);
	
	if (group){
		if (group->Name == "LooterGroup")
			ControlCombo->TextChanged += gcnew System::EventHandler(this, &FormEditor::Looter_TextChanged);

		if (group->Name == "DepoterGroup")
			ControlCombo->TextChanged += gcnew System::EventHandler(this, &FormEditor::Depoter_TextChanged);

		if (group->Name == "RepoterGroup")
			ControlCombo->TextChanged += gcnew System::EventHandler(this, &FormEditor::Repoter_TextChanged);
	}

	ControlCombo->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlCombo->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlCombo->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlCombo->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &FormEditor::EventSelectedIndexChanged);
	ControlCombo->TextChanged += gcnew System::EventHandler(this, &FormEditor::Event_TextChanged);
	ControlCombo->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlCombo->Resize += gcnew System::EventHandler(this, &FormEditor::EventResize);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlCombo;
	filterProperty(radPropertyGrid1->Items);

	return ControlCombo;
}
Telerik::WinControls::UI::RadLabel^ FormEditor::createLabel(Control^ group, String^ name, String^ text, System::Drawing::Point point, System::Drawing::Size size){
	Telerik::WinControls::UI::RadLabel^ ControlLabel = gcnew Telerik::WinControls::UI::RadLabel();

	ControlLabel->Location = point;
	ControlLabel->Name = name;
	ControlLabel->Size = size;
	ControlLabel->Text = text;
	ControlLabel->TabIndex = 0;

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode();

	NewNode->Name = name;
	NewNode->Text = name;
	NewNode->Tag = "label";

	if (group)
		TreeViewControls->Nodes[TreeViewControls->TopNode->Text]->Nodes[group->Name]->Nodes->AddRange(NewNode);
	else
		TreeViewControls->TopNode->Nodes->AddRange(NewNode);

	ControlLabel->TextChanged += gcnew System::EventHandler(this, &FormEditor::Event_Label_TextChanged);
	ControlLabel->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseDown);
	ControlLabel->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseUp);
	ControlLabel->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &FormEditor::EventMouseMove);
	ControlLabel->LocationChanged += gcnew System::EventHandler(this, &FormEditor::EventLocationChanged);
	ControlLabel->Resize += gcnew System::EventHandler(this, &FormEditor::EventResize);

	TreeViewControls->SelectedNode = NewNode;
	radPropertyGrid1->SelectedObject = (System::Object^)ControlLabel;
	filterProperty(radPropertyGrid1->Items);

	return ControlLabel;
}

void  FormEditor::setPropertyButton(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadButton^ control){
	if (groupKey != ""){
		auto controlRule = getControlChieldRule(groupKey, controlKey);

		if (!controlRule)
			return;

		controlRule->set_property("text", managed_util::fromSS(controlKey));
		controlRule->set_property("name", managed_util::fromSS(controlKey));
		controlRule->set_property("type", "button");
		controlRule->set_property("script", "none");
		controlRule->set_property("sizeW", std::to_string(control->Size.Width));
		controlRule->set_property("sizeH", std::to_string(control->Size.Height));
		controlRule->set_property("locationX", std::to_string(control->Location.X));
		controlRule->set_property("locationY", std::to_string(control->Location.Y));
		return;
	}
	auto controlRule_ = getControlRule(controlKey);

	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("type", "button");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}
void  FormEditor::setPropertyTextBox(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadTextBox^ control){
	if (groupKey != ""){
		auto controlRule = getControlChieldRule(groupKey, controlKey);

		if (!controlRule)
			return;

		controlRule->set_property("text", managed_util::fromSS(controlKey));
		controlRule->set_property("name", managed_util::fromSS(controlKey));
		controlRule->set_property("type", "text");
		controlRule->set_property("sizeW", std::to_string(control->Size.Width));
		controlRule->set_property("sizeH", std::to_string(control->Size.Height));
		controlRule->set_property("locationX", std::to_string(control->Location.X));
		controlRule->set_property("locationY", std::to_string(control->Location.Y));
		return;
	}
	auto controlRule_ = getControlRule(controlKey);

	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("type", "text");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}
void  FormEditor::setPropertyCheckBox(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadCheckBox^ control){
	if (groupKey != ""){
		auto controlRule = getControlChieldRule(groupKey, controlKey);
		if (!controlRule)
			return;

		controlRule->set_property("text", managed_util::fromSS(controlKey));
		controlRule->set_property("name", managed_util::fromSS(controlKey));
		controlRule->set_property("state", managed_util::fromSS(control->Checked.ToString()));
		controlRule->set_property("type", "check");
		controlRule->set_property("sizeW", std::to_string(control->Size.Width));
		controlRule->set_property("sizeH", std::to_string(control->Size.Height));
		controlRule->set_property("locationX", std::to_string(control->Location.X));
		controlRule->set_property("locationY", std::to_string(control->Location.Y));

		return;
	}
	auto controlRule_ = getControlRule(controlKey);

	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("state", managed_util::fromSS(control->Checked.ToString()));
	controlRule_->set_property("type", "check");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}
void  FormEditor::setPropertySpinEditor(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadSpinEditor^ control){
	if (groupKey != ""){
		auto controlRule = getControlChieldRule(groupKey, controlKey);

		if (!controlRule)
			return;

		controlRule->set_property("text", managed_util::fromSS(controlKey));
		controlRule->set_property("name", managed_util::fromSS(controlKey));
		controlRule->set_property("type", "spin");
		controlRule->set_property("sizeW", std::to_string(control->Size.Width));
		controlRule->set_property("sizeH", std::to_string(control->Size.Height));
		controlRule->set_property("maxvalue", managed_util::fromSS(control->Maximum.ToString()));
		controlRule->set_property("minvalue", managed_util::fromSS(control->Minimum.ToString()));
		controlRule->set_property("value", managed_util::fromSS(control->Value.ToString()));
		controlRule->set_property("locationX", std::to_string(control->Location.X));
		controlRule->set_property("locationY", std::to_string(control->Location.Y));
		return;
	}
	auto controlRule_ = getControlRule(controlKey);

	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("type", "spin");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("maxvalue", managed_util::fromSS(control->Maximum.ToString()));
	controlRule_->set_property("minvalue", managed_util::fromSS(control->Minimum.ToString()));
	controlRule_->set_property("value", managed_util::fromSS(control->Value.ToString()));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}
void  FormEditor::setPropertyComboBox(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadDropDownList^ control){
	if (groupKey != ""){
		auto controlRule = getControlChieldRule(groupKey, controlKey);

		if (!controlRule)
			return;

		controlRule->set_property("text", managed_util::fromSS(controlKey));
		controlRule->set_property("name", managed_util::fromSS(controlKey));
		controlRule->set_property("type", "combobox");
		controlRule->set_property("sizeW", std::to_string(control->Size.Width));
		controlRule->set_property("sizeH", std::to_string(control->Size.Height));
		controlRule->set_property("locationX", std::to_string(control->Location.X));
		controlRule->set_property("locationY", std::to_string(control->Location.Y));
		return;
	}
	auto controlRule_ = getControlRule(controlKey);

	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("type", "combobox");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}
void  FormEditor::setPropertyLabel(String^ groupKey, String^ controlKey, Telerik::WinControls::UI::RadLabel^ control){
	if (groupKey != ""){
		auto controlRule = getControlChieldRule(groupKey, controlKey);
		if (!controlRule)
			return;

		controlRule->set_property("text", managed_util::fromSS(controlKey));
		controlRule->set_property("name", managed_util::fromSS(controlKey));
		controlRule->set_property("type", "label");
		controlRule->set_property("sizeW", std::to_string(control->Size.Width));
		controlRule->set_property("sizeH", std::to_string(control->Size.Height));
		controlRule->set_property("locationX", std::to_string(control->Location.X));
		controlRule->set_property("locationY", std::to_string(control->Location.Y));
		return;
	}

	auto controlRule_ = getControlRule(controlKey);
	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("type", "label");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}
void  FormEditor::setPropertyGroup(String^ controlKey, Telerik::WinControls::UI::RadGroupBox^ control){
	auto controlRule_ = getControlRule(controlKey);

	if (!controlRule_)
		return;

	controlRule_->set_property("text", managed_util::fromSS(controlKey));
	controlRule_->set_property("name", managed_util::fromSS(controlKey));
	controlRule_->set_property("type", "group");
	controlRule_->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule_->set_property("sizeH", std::to_string(control->Size.Height));
	controlRule_->set_property("locationX", std::to_string(control->Location.X));
	controlRule_->set_property("locationY", std::to_string(control->Location.Y));
}

void  FormEditor::addGroupInForm(){
	if (!TreeViewControls->SelectedNode && !TreeViewControls->SelectedNode->Parent)
		return;

	if (!TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Text != "Controls")
		return;

	std::string controlId = ControlManager::get()->requestNewGroupId();

	auto controlInfo = createGroup(gcnew String(controlId.c_str()), gcnew String(controlId.c_str()), System::Drawing::Point(10, 10), System::Drawing::Size(300, 100));

	radPanel3->Controls->Add(controlInfo);

	setPropertyGroup(gcnew String(controlId.c_str()), controlInfo);
}
void  FormEditor::addLoooterGroupInForm(){
	if (!TreeViewControls->SelectedNode && !TreeViewControls->SelectedNode->Parent)
		return;

	if (!TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Text != "Controls")
		return;

	if (ControlManager::get()->checkExistControl("LooterGroup"))
		return;

	std::string controlId = ControlManager::get()->requestNewById("LooterGroup");
	String^ group_id = gcnew String(controlId.c_str());

	Telerik::WinControls::UI::RadGroupBox^ controlInfo = createGroup(group_id, group_id,
		System::Drawing::Point(10, 10), System::Drawing::Size(300, 100));

	radPanel3->Controls->Add(controlInfo);
	setPropertyGroup(group_id, controlInfo);
	std::shared_ptr<ControlInfo> controlInfoRule = getControlRule(group_id);
	if (!controlInfoRule)
		return;

	std::map<std::string, std::shared_ptr<LooterContainer>> myMap = LooterManager::get()->containers;
	for (auto container : myMap){
		String^ looter_id = gcnew String(container.first.c_str());
		String^ container_name = gcnew String(container.second->get_container_name().c_str());
		String^ label_control_id = gcnew String(controlInfoRule->requestNewById(container.first + "label").c_str());
		String^ combo_control_id = gcnew String(controlInfoRule->requestNewById(container.first).c_str());

		Telerik::WinControls::UI::RadLabel^ control_label = createLabel(controlInfo, label_control_id, looter_id,
			System::Drawing::Point(10, 10), System::Drawing::Size(55, 18));

		Telerik::WinControls::UI::RadDropDownList^ control_combo = createComboBox(controlInfo, combo_control_id, container_name,
			System::Drawing::Point(10, 30), System::Drawing::Size(140, 23), managed_util::convert_vector_to_array(ItemsManager::get()->get_containers()));

		controlInfo->Controls->Add(control_combo);
		controlInfo->Controls->Add(control_label);

		setPropertyLabel(controlInfo->Name, label_control_id, control_label);
		setPropertyComboBox(controlInfo->Name, combo_control_id, control_combo);
		
		control_label->Text = looter_id;
		control_combo->Text = container_name;

		control_label->Name = label_control_id;
		control_combo->Name = combo_control_id;

		auto controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(combo_control_id));
		if (controlRule_){
			controlRule_->set_vectorItem(ItemsManager::get()->get_containers());
			controlRule_->set_property("name", managed_util::fromSS(combo_control_id));
			controlRule_->set_property("text", managed_util::fromSS(container_name));
		}

		auto controlRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(label_control_id));
		if (controlRule){
			controlRule->set_property("name", managed_util::fromSS(label_control_id));
			controlRule->set_property("text", managed_util::fromSS(looter_id));
		}
	}
}
void  FormEditor::addDepoterGroupInForm(){
	if (!TreeViewControls->SelectedNode && !TreeViewControls->SelectedNode->Parent)
		return;

	if (!TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Text != "Controls")
		return;

	if (ControlManager::get()->checkExistControl("DepoterGroup"))
		return;

	std::string controlId = ControlManager::get()->requestNewById("DepoterGroup");
	String^ group_id = gcnew String(controlId.c_str());

	Telerik::WinControls::UI::RadGroupBox^ controlInfo = createGroup(group_id, group_id,
		System::Drawing::Point(10, 10), System::Drawing::Size(300, 100));

	radPanel3->Controls->Add(controlInfo);
	setPropertyGroup(group_id, controlInfo);

	std::shared_ptr<ControlInfo> controlInfoRule = getControlRule(group_id);
	if (!controlInfoRule)
		return;

	std::map<std::string, std::shared_ptr<DepotContainer>> myMap = DepoterManager::get()->getMapDepotContainer();
	for (auto container : myMap){
		String^ looter_id = gcnew String(container.first.c_str());
		String^ container_name = gcnew String(container.second->get_ContainerName().c_str());
		String^ label_control_id = gcnew String(controlInfoRule->requestNewById(container.first + "label").c_str());
		String^ combo_control_id = gcnew String(controlInfoRule->requestNewById(container.first).c_str());

		Telerik::WinControls::UI::RadLabel^ control_label = createLabel(controlInfo, label_control_id, looter_id,
			System::Drawing::Point(10, 10), System::Drawing::Size(55, 18));

		Telerik::WinControls::UI::RadDropDownList^ control_combo = createComboBox(controlInfo, combo_control_id, container_name,
			System::Drawing::Point(10, 30), System::Drawing::Size(140, 20), managed_util::convert_vector_to_array(ItemsManager::get()->get_depot_boxs()));

		controlInfo->Controls->Add(control_combo);
		controlInfo->Controls->Add(control_label);

		setPropertyLabel(controlInfo->Name, label_control_id, control_label);
		setPropertyComboBox(controlInfo->Name, combo_control_id, control_combo);

		control_label->Text = looter_id;
		control_combo->Text = container_name;

		control_label->Name = label_control_id;
		control_combo->Name = combo_control_id;

		auto controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(combo_control_id));
		if (controlRule_){
			controlRule_->set_vectorItem(ItemsManager::get()->get_depot_boxs());
			controlRule_->set_property("name", managed_util::fromSS(combo_control_id));
			controlRule_->set_property("text", managed_util::fromSS(container_name));
		}

		auto controlRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(label_control_id));
		if (controlRule){
			controlRule->set_property("name", managed_util::fromSS(label_control_id));
			controlRule->set_property("text", managed_util::fromSS(looter_id));
		}
	}
}
void FormEditor::addSpellsAttackGroupInForm(){
	if (!TreeViewControls->SelectedNode && !TreeViewControls->SelectedNode->Parent)
		return;

	if (!TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Text != "Controls")
		return;

	if (ControlManager::get()->checkExistControl("SpellsAttackGroup"))
		return;

	std::string controlId = ControlManager::get()->requestNewById("SpellsAttackGroup");
	String^ group_id = gcnew String(controlId.c_str());

	Telerik::WinControls::UI::RadGroupBox^ controlInfo = createGroup(group_id, group_id,
		System::Drawing::Point(10, 10), System::Drawing::Size(300, 100));

	radPanel3->Controls->Add(controlInfo);
	setPropertyGroup(group_id, controlInfo);

	std::shared_ptr<ControlInfo> controlInfoRule = getControlRule(group_id);
	if (!controlInfoRule)
		return;

	std::map<int, std::shared_ptr<SpellAttack>>& myMap = SpellManager::get()->getAttackList();
	for (auto container : myMap){
		String^ spells_id = managed_util::fromString(controlInfoRule->requestNewById(std::to_string(container.first) + "Attack"));
		String^ spells_text = managed_util::fromString(container.second->get_visual_name());

		Telerik::WinControls::UI::RadCheckBox^ control_combo = createCheckBox(controlInfo, container.second->get_checked(),
			spells_id, spells_text,	System::Drawing::Point(10, 30), System::Drawing::Size(140, 20));

		controlInfo->Controls->Add(control_combo);

		setPropertyCheckBox(controlInfo->Name, spells_id, control_combo);

		control_combo->Text = spells_text;
		control_combo->Name = spells_id;

		std::shared_ptr<ControlInfoChield> controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(spells_id));
		if (controlRule_){
			controlRule_->set_property("state", container.second->get_checked() ? "true" : "false");
			controlRule_->set_property("name", managed_util::fromSS(spells_id));
			controlRule_->set_property("text", managed_util::fromSS(spells_text));
		}
	}
}
void FormEditor::addSpellsHealthGroupInForm(){
	if (!TreeViewControls->SelectedNode && !TreeViewControls->SelectedNode->Parent)
		return;

	if (!TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Text != "Controls")
		return;

	if (ControlManager::get()->checkExistControl("SpellsHealthGroup"))
		return;

	std::string controlId = ControlManager::get()->requestNewById("SpellsHealthGroup");
	String^ group_id = gcnew String(controlId.c_str());

	Telerik::WinControls::UI::RadGroupBox^ controlInfo = createGroup(group_id, group_id,
		System::Drawing::Point(10, 10), System::Drawing::Size(300, 100));

	radPanel3->Controls->Add(controlInfo);
	setPropertyGroup(group_id, controlInfo);

	std::shared_ptr<ControlInfo> controlInfoRule = getControlRule(group_id);
	if (!controlInfoRule)
		return;

	std::map<int, std::shared_ptr<SpellHealth>>& myMap = SpellManager::get()->getHealthList();
	for (auto container : myMap){
		String^ spells_id = managed_util::fromString(controlInfoRule->requestNewById(std::to_string(container.first) + "Health"));
		String^ spells_text = managed_util::fromString(container.second->get_visual_name());

		Telerik::WinControls::UI::RadCheckBox^ control_combo = createCheckBox(controlInfo, container.second->get_checked(),
			spells_id, spells_text, System::Drawing::Point(10, 30), System::Drawing::Size(140, 20));

		controlInfo->Controls->Add(control_combo);

		setPropertyCheckBox(controlInfo->Name, spells_id, control_combo);

		control_combo->Text = spells_text;
		control_combo->Name = spells_id;

		std::shared_ptr<ControlInfoChield> controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(spells_id));
		if (controlRule_){
			controlRule_->set_property("state", container.second->get_checked() ? "true" : "false");
			controlRule_->set_property("name", managed_util::fromSS(spells_id));
			controlRule_->set_property("text", managed_util::fromSS(spells_text));
		}
	}
}
void  FormEditor::addRepoterGroupInForm(){
	if (!TreeViewControls->SelectedNode && !TreeViewControls->SelectedNode->Parent)
		return;

	if (!TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Text != "Controls")
		return;

	if (ControlManager::get()->checkExistControl("RepoterGroup"))
		return;

	String^ group_id = gcnew String(ControlManager::get()->requestNewById("RepoterGroup").c_str());

	Telerik::WinControls::UI::RadGroupBox^ controlInfo = createGroup(group_id, group_id,
		System::Drawing::Point(10, 10), System::Drawing::Size(500, 500));

	radPanel3->Controls->Add(controlInfo);
	setPropertyGroup(group_id, controlInfo);

	std::shared_ptr<ControlInfo> controlInfoRule = getControlRule(group_id);
	if (!controlInfoRule)
		return;

	std::map<std::string, std::shared_ptr<RepotId>>  myRepotMap = RepoterManager::get()->getMapRepotId();
	for (auto Repot : myRepotMap){
		String^ repotId = gcnew String(Repot.first.c_str());
		std::map<std::string, std::shared_ptr<RepotItens>> myRepotItemsMap = Repot.second->getMapRepotItens();
		
		for (auto Item : myRepotItemsMap){
			String^ combo_control_id = gcnew String(controlInfoRule->requestNewById(Repot.first + Item.first + "combo").c_str());
			String^ spinmin_control_id = gcnew String(controlInfoRule->requestNewById(Repot.first + Item.first + "spinmin").c_str());
			String^ spinmax_control_id = gcnew String(controlInfoRule->requestNewById(Repot.first + Item.first + "spinmax").c_str());
			String^ item_name = gcnew String(Item.second->get_itemName().c_str());

			Telerik::WinControls::UI::RadSpinEditor^ control_spin_min = createSpinEditor(controlInfo, spinmin_control_id, item_name,
				System::Drawing::Point(60, 30), System::Drawing::Size(42, 22),99999,0,Item.second->min);

			Telerik::WinControls::UI::RadSpinEditor^ control_spin_max = createSpinEditor(controlInfo, spinmax_control_id, item_name,
				System::Drawing::Point(100, 30), System::Drawing::Size(42, 22), 99999, 0, Item.second->max);

			Telerik::WinControls::UI::RadDropDownList^ control_combo = createComboBox(controlInfo, combo_control_id, item_name,
				System::Drawing::Point(10, 30), System::Drawing::Size(140, 22), managed_util::convert_vector_to_array(ItemsManager::get()->get_itens()));

			controlInfo->Controls->Add(control_combo);
			controlInfo->Controls->Add(control_spin_min);
			controlInfo->Controls->Add(control_spin_max);

			setPropertyComboBox(controlInfo->Name, combo_control_id, control_combo);
			setPropertySpinEditor(controlInfo->Name, spinmin_control_id, control_spin_min);
			setPropertySpinEditor(controlInfo->Name, spinmax_control_id, control_spin_max);

			control_spin_min->Name = spinmin_control_id;
			control_spin_max->Name = spinmax_control_id;
			control_combo->Name = combo_control_id;

			control_spin_min->Text = Convert::ToString(Item.second->min);
			control_spin_max->Text = Convert::ToString(Item.second->max);
			control_combo->Text = item_name;


			auto control_comboRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(combo_control_id));
			if (control_comboRule){
				control_comboRule->set_vectorItem(ItemsManager::get()->get_itens());
				control_comboRule->set_property("name", managed_util::fromSS(combo_control_id));
				control_comboRule->set_property("text", managed_util::fromSS(item_name));
			}

			auto control_spin_minRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(spinmin_control_id));
			if (control_spin_minRule){
				control_spin_minRule->set_property("name", managed_util::fromSS(spinmin_control_id));
				control_spin_minRule->set_property("value", managed_util::fromSS(Convert::ToString(Item.second->min)));
			}

			auto control_spin_maxRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(spinmax_control_id));
			if (control_spin_maxRule){
				control_spin_maxRule->set_property("name", managed_util::fromSS(spinmax_control_id));
				control_spin_maxRule->set_property("value", managed_util::fromSS(Convert::ToString(Item.second->max)));
			}
		}
	}

}
void  FormEditor::addButtonInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;

		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);
		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());

		if (!groupControl)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createButton(groupControl, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(75, 23));

		groupControl->Controls->Add(controlInfo);

		setPropertyButton(temp_string, ControlId, controlInfo);
		return;
	}
	String^ controlId = gcnew String(ControlManager::get()->request_new_id().c_str());
	auto controlInfo_ = createButton(nullptr, controlId, controlId, System::Drawing::Point(10, 10), System::Drawing::Size(75, 23));

	radPanel3->Controls->Add(controlInfo_);
	setPropertyButton("", controlId, controlInfo_);
}
void  FormEditor::addTextBoxInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;

		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);
		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());

		if (!groupControl)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createTextBox(groupControl, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20));

		groupControl->Controls->Add(controlInfo);
		setPropertyTextBox(temp_string, ControlId, controlInfo);
		return;
	}

	String^ ControlId_ = gcnew String(ControlManager::get()->request_new_id().c_str());
	auto controlInfo_ = createTextBox(nullptr, ControlId_, ControlId_, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20));

	radPanel3->Controls->Add(controlInfo_);
	setPropertyTextBox("", ControlId_, controlInfo_);
}
void  FormEditor::addCheckBoxInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;

		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);
		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());

		if (!groupControl)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createCheckBox(groupControl, false, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(91, 18));

		groupControl->Controls->Add(controlInfo);
		setPropertyCheckBox(temp_string, ControlId, controlInfo);
		return;
	}

	String^ ControlId_ = gcnew String(ControlManager::get()->request_new_id().c_str());
	auto controlInfo_ = createCheckBox(nullptr, false, ControlId_, ControlId_, System::Drawing::Point(10, 10), System::Drawing::Size(91, 18));

	radPanel3->Controls->Add(controlInfo_);
	setPropertyCheckBox("", ControlId_, controlInfo_);
}
void  FormEditor::addSpinEditorInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;

		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);
		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());

		if (!groupControl)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createSpinEditor(groupControl, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20), 100, 0, 0);

		groupControl->Controls->Add(controlInfo);
		setPropertySpinEditor(temp_string, ControlId, controlInfo);

		return;
	}
	String^ ControlId_ = gcnew String(ControlManager::get()->request_new_id().c_str());
	auto controlInfo_ = createSpinEditor(nullptr, ControlId_, ControlId_, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20), 100, 0, 0);

	radPanel3->Controls->Add(controlInfo_);
	setPropertySpinEditor("", ControlId_, controlInfo_);
}
void  FormEditor::addComboBoxInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;

		array<String^>^ itens{};
		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);
		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());

		if (!groupControl)
			return;

		auto ControlRule = getControlChieldRule(TreeViewControls->SelectedNode->Text, ControlId);

		if (!ControlRule)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createComboBox(groupControl, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20), itens);

		groupControl->Controls->Add(controlInfo);
		setPropertyComboBox(temp_string, ControlId, controlInfo);
		return;
	}
	String^ ControlId_ = gcnew String(ControlManager::get()->request_new_id().c_str());
	array<String^>^ itens{};

	auto controlInfo_ = createComboBox(nullptr, ControlId_, ControlId_, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20), itens);

	radPanel3->Controls->Add(controlInfo_);
	setPropertyComboBox("", ControlId_, controlInfo_);
}
void  FormEditor::addComboBoxItemInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;


		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);
		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());

		if (!groupControl)
			return;

		auto ControlRule = getControlChieldRule(TreeViewControls->SelectedNode->Text, ControlId);

		if (!ControlRule)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createComboBox(groupControl, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20),
			managed_util::convert_vector_to_array(ItemsManager::get()->get_itens()));

		groupControl->Controls->Add(controlInfo);
		ControlRule->set_vectorItem(ItemsManager::get()->get_itens());
		setPropertyComboBox(temp_string, ControlId, controlInfo);

		return;
	}
	String^ ControlId_ = gcnew String(ControlManager::get()->request_new_id().c_str());

	auto control_Rule_ = ControlManager::get()->getControlInfoRule(managed_util::fromSS(ControlId_));

	if (!control_Rule_)
		return;

	auto controlInfo_ = createComboBox(nullptr, ControlId_, ControlId_, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20),
		managed_util::convert_vector_to_array(ItemsManager::get()->get_itens()));

	radPanel3->Controls->Add(controlInfo_);
	control_Rule_->set_vectorItem(ItemsManager::get()->get_itens());
	setPropertyComboBox("", ControlId_, controlInfo_);
}
void  FormEditor::addComboBoxBpInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		if (!controlInfoRule)
			return;

		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());
		auto controlRuleInfoChield = getControlChieldRule(TreeViewControls->SelectedNode->Text, ControlId);
		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);

		if (!controlRuleInfoChield && !groupControl)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createComboBox(groupControl, ControlId, ControlId,
			System::Drawing::Point(10, 10), System::Drawing::Size(100, 20), managed_util::convert_vector_to_array(ItemsManager::get()->get_containers()));

		groupControl->Controls->Add(controlInfo);
		controlRuleInfoChield->set_vectorItem(ItemsManager::get()->get_containers());
		setPropertyComboBox(temp_string, ControlId, controlInfo);

		return;
	}
	String^ controlId = gcnew String(ControlManager::get()->request_new_id().c_str());
	auto controlInfoRule = getControlRule(controlId);

	if (!controlInfoRule)
		return;

	auto systemControl = createComboBox(nullptr, controlId, controlId, System::Drawing::Point(10, 10), System::Drawing::Size(100, 20),
		managed_util::convert_vector_to_array(ItemsManager::get()->get_containers()));

	radPanel3->Controls->Add(systemControl);
	controlInfoRule->set_vectorItem(ItemsManager::get()->get_containers());
	setPropertyComboBox("", controlId, systemControl);

}
void  FormEditor::addLabelInForm(){
	if (!TreeViewControls->TopNode && !TreeViewControls->SelectedNode)
		return;

	if (TreeViewControls->SelectedNode->Tag != "group" && TreeViewControls->SelectedNode->Text != TreeViewControls->TopNode->Text)
		return;

	if (TreeViewControls->SelectedNode->Tag == "group"){
		auto controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);
		if (!controlInfoRule)
			return;

		String^ ControlId = gcnew String(controlInfoRule->request_new_id().c_str());
		Telerik::WinControls::UI::RadGroupBox^ groupControl = (Telerik::WinControls::UI::RadGroupBox^)get_control(TreeViewControls->SelectedNode->Text);

		if (!groupControl)
			return;

		auto temp_string = TreeViewControls->SelectedNode->Text;

		auto controlInfo = createLabel(groupControl, ControlId, ControlId, System::Drawing::Point(10, 10), System::Drawing::Size(55, 18));

		groupControl->Controls->Add(controlInfo);
		setPropertyLabel(temp_string, ControlId, controlInfo);
		return;
	}

	String^ controlId = gcnew String(ControlManager::get()->request_new_id().c_str());
	auto controlInfo_ = createLabel(nullptr, controlId, controlId, System::Drawing::Point(10, 10), System::Drawing::Size(55, 18));

	radPanel3->Controls->Add(controlInfo_);
	setPropertyLabel("", controlId, controlInfo_);
}

void  FormEditor::openLuaEditor(String^ groupKey, String^ controlKey){
	if (groupKey != ""){
		std::string current_script = getControlChieldRule(groupKey, controlKey)->get_property("script");

		NeutralBot::LuaBackground^ mluaEditor = gcnew NeutralBot::LuaBackground(gcnew String(&current_script[0]));

		mluaEditor->hide_left_pane();
		mluaEditor->ShowDialog();

		getControlChieldRule(groupKey, controlKey)->set_property("script", managed_util::fromSS(mluaEditor->fastColoredTextBox1->Text));
		return;
	}
	auto controlRule_ = getControlRule(controlKey);
	std::string current_script = controlRule_->get_property("script");

	NeutralBot::LuaBackground^ mluaEditor = gcnew NeutralBot::LuaBackground(gcnew String(&current_script[0]));

	mluaEditor->hide_left_pane();
	mluaEditor->ShowDialog();

	controlRule_->set_property("script", managed_util::fromSS(mluaEditor->fastColoredTextBox1->Text));
}

System::Void  FormEditor::MenuGroup_Click(Object^ sender, EventArgs^ e){
	addGroupInForm();
}
System::Void  FormEditor::MenuEdit_Click(Object^ sender, EventArgs^ e){
	if (TreeViewControls->SelectedNode->Tag != "button")
		return;

	if (TreeViewControls->SelectedNode->Parent->Tag == "group")
		openLuaEditor(TreeViewControls->SelectedNode->Parent->Text, TreeViewControls->SelectedNode->Text);
	else
		openLuaEditor("", TreeViewControls->SelectedNode->Text);
}
System::Void  FormEditor::MenuButton_Click(Object^ sender, EventArgs^ e){
	addButtonInForm();
}
System::Void  FormEditor::MenuItemLooterGroup_Click(Object^ sender, EventArgs^ e){
	addLoooterGroupInForm();
	updateLooterGroup();
}
System::Void  FormEditor::MenuItemRepoterGroup_Click(Object^ sender, EventArgs^ e){
	addRepoterGroupInForm();
	updateRepoterGroup();
}
System::Void FormEditor::MenuItemDepoterGroup_Click(Object^ sender, EventArgs^ e){
	addDepoterGroupInForm();
	updateDepoterGroup();
}
System::Void FormEditor::MenuItemSpellsAttackGroup_Click(Object^ sender, EventArgs^ e){
	addSpellsAttackGroupInForm();
	updateSpellsAttackGroup();
}
System::Void FormEditor::MenuItemSpellsHealthGroup_Click(Object^ sender, EventArgs^ e){
	addSpellsHealthGroupInForm();
	updateSpellsHealthGroup();
}
System::Void  FormEditor::MenuCheckBox_Click(Object^ sender, EventArgs^ e){
	addCheckBoxInForm();
}
System::Void  FormEditor::MenuTextBox_Click(Object^ sender, EventArgs^ e){
	addTextBoxInForm();
}
System::Void  FormEditor::MenuLabel_Click(Object^ sender, EventArgs^ e){
	addLabelInForm();
}
System::Void  FormEditor::MenuCombo_Click(Object^ sender, EventArgs^ e){
	addComboBoxInForm();
}
System::Void  FormEditor::MenuComboBoxItem_Click(Object^ sender, EventArgs^ e){
	addComboBoxItemInForm();
}
System::Void  FormEditor::MenuComboBoxBp_Click(Object^ sender, EventArgs^ e){
	addComboBoxBpInForm();
}
System::Void  FormEditor::MenuSpinEditor_Click(Object^ sender, EventArgs^ e){
	addSpinEditorInForm();
}

Control^  FormEditor::get_control(String^ controlName){
	array<Control^>^ get_control = radPanel3->Controls->Find(controlName, true);

	if (!get_control->Length)
		return nullptr;

	return get_control[0];
}

System::Void  FormEditor::radMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!edit_mode){
		edit_mode = true;
		move_mode = true;
		bt_move->Text = L"Move Mode On";
		radPanel1->Enabled = true;
		radPanel4->Enabled = true;
		radPanel1->Size = System::Drawing::Size(200, radPanel1->Size.Height);
		radPanel4->Size = System::Drawing::Size(200, radPanel4->Size.Height);
		this->Size = System::Drawing::Size(this->Size.Width + 400, this->Size.Height);
		bt_move->Visibility = Telerik::WinControls::ElementVisibility::Visible;
		return;
	}

	edit_mode = false;
	move_mode = false;
	bt_move->Text = L"Move Mode Off";
	radPanel1->Enabled = false;
	radPanel4->Enabled = false;
	radPanel1->Size = System::Drawing::Size(0, radPanel1->Size.Height);
	radPanel4->Size = System::Drawing::Size(0, radPanel4->Size.Height);
	this->Size = System::Drawing::Size(this->Size.Width - 400, this->Size.Height);
	bt_move->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
}
System::Void  FormEditor::FormEditor_Load(System::Object^  sender, System::EventArgs^  e) {
	creatEvents();
	loadControlInForm();

	bt_move->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	radPanel1->Enabled = false;
	radPanel4->Enabled = false;
	radPanel1->Size = System::Drawing::Size(0, radPanel1->Size.Height);
	radPanel4->Size = System::Drawing::Size(0, radPanel4->Size.Height);
	update_idiom();
	temp_disable = false;

	disable_update = true;
	updateDepoterGroup();
	updateRepoterGroup();
	updateLooterGroup();
	updateSpellsAttackGroup();
	updateSpellsHealthGroup();
	disable_update = false;
}

void  FormEditor::filterProperty(Telerik::WinControls::UI::PropertyGridItemCollection^ Items){
	for each(auto item in radPropertyGrid1->Items){
		if (item->Name == "Text" || item->Name == "Location" || item->Name == "Size" || item->Name == "Items"
			|| item->Name == "Value" || item->Name == "Maximum" || item->Name == "Minimum")
			continue;

		item->Visible = false;
	}
}

std::shared_ptr<ControlInfoChield>  FormEditor::getControlChieldRule(String^ groupKey, String^ controlKey){
	auto controlRule = ControlManager::get()->getControlInfoRule(managed_util::fromSS(groupKey));
	if (!controlRule)
		return nullptr;

	auto controlRuleChield = controlRule->getControlInfoChieldRule(managed_util::fromSS(controlKey));
	if (!controlRuleChield)
		return nullptr;

	return controlRuleChield;
}
std::shared_ptr<ControlInfo>  FormEditor::getControlRule(String^ controlKey){
	auto controlRule = ControlManager::get()->getControlInfoRule(managed_util::fromSS(controlKey));

	if (!controlRule)
		return nullptr;

	return controlRule;
}

System::Void  FormEditor::EventClick(System::Object^  sender, System::EventArgs^  e) {
	if (edit_mode)
		return;

	auto control = (System::Windows::Forms::Control^)sender;

	if (!TreeViewControls->SelectedNode || !TreeViewControls->SelectedNode->Parent)
		return;

	if (TreeViewControls->SelectedNode->Parent->Tag == "group"){
		std::shared_ptr<ControlInfoChield> controlInfoChieldRule = getControlChieldRule(TreeViewControls->SelectedNode->Parent->Text, TreeViewControls->SelectedNode->Text);

		std::string current_script = controlInfoChieldRule->get_property("script");
		LuaThread::get()->simple_execute(current_script, managed_util::fromSS(control->Name));
		return;
	}

	if (TreeViewControls->SelectedNode->Tag == "button"){
		std::shared_ptr<ControlInfo> controlInfoRule = getControlRule(TreeViewControls->SelectedNode->Text);

		std::string current_script = controlInfoRule->get_property("script");
		LuaThread::get()->simple_execute(current_script, managed_util::fromSS(control->Name));

		return;
	}
}
System::Void  FormEditor::EventMouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (e->Button == System::Windows::Forms::MouseButtons::Right)
		return;

	if (!edit_mode)
		return;

	if (!move_mode)
		return;

	if (dragInProgress)
		return;

	dragInProgress = true;
	pressed = true;
	MouseDownX = e->X;
	MouseDownY = e->Y;
	return;
}
System::Void  FormEditor::EventMouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (!edit_mode)
		return;

	if (!move_mode)
		return;

	if (e->Button == System::Windows::Forms::MouseButtons::Left)
		dragInProgress = false;

	EventLocationChanged(sender, e);
	pressed = false;
	return;
}
System::Void  FormEditor::EventMouseMove(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
	if (!edit_mode)
		return;

	if (!move_mode)
		return;

	if (!dragInProgress)
		return;

	Control^ controlThis = (Control^)sender;

	System::Drawing::Point^ temp = gcnew System::Drawing::Point();
	temp->X = controlThis->Location.X + (e->X - MouseDownX);
	temp->Y = controlThis->Location.Y + (e->Y - MouseDownY);
	controlThis->Location = System::Drawing::Point(temp->X, temp->Y);
	return;
}
System::Void  FormEditor::EventSelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	Telerik::WinControls::UI::RadDropDownList ^ control = (Telerik::WinControls::UI::RadDropDownList^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
		std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);

		if (!controlChiedlRule)
			return;

		controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));
		return;
	}
	std::shared_ptr<ControlInfo> control_Rule = getControlRule(control->Name);

	if (!control_Rule)
		return;

	control_Rule->set_property("text", managed_util::fromSS(control->Text));
}
System::Void  FormEditor::EventValueChanged(System::Object^  sender, System::EventArgs^  e) {
	Telerik::WinControls::UI::RadSpinEditor^ control = (Telerik::WinControls::UI::RadSpinEditor^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
		std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);

		if (!controlChiedlRule)
			return;

		controlChiedlRule->set_property("value", managed_util::fromSS(control->Text));
		return;
	}

	std::shared_ptr<ControlInfo> control_Rule = getControlRule(control->Name);

	if (!control_Rule)
		return;

	control_Rule->set_property("value", managed_util::fromSS(control->Text));
}
System::Void  FormEditor::EventLocationChanged(System::Object^  sender, System::EventArgs^  e) {
	if (dragInProgress)
		return;

	Control^ control = (Control^)sender;

	if (radPanel3->Size.Width < control->Size.Width)
		return;

	if (radPanel3->Size.Height < control->Size.Height)
		return;


	if (control->Location.X < 0)
		control->Location = System::Drawing::Point(0, control->Location.Y);


	if (control->Location.Y < 0)
		control->Location = System::Drawing::Point(control->Location.X, 0);


	if (control->Location.X > radPanel3->Size.Width)
		control->Location = System::Drawing::Point((control->Parent->Size.Width - 10), control->Location.Y);

	if (control->Location.Y > (control->Parent->Size.Height - control->Size.Height))
		control->Location = System::Drawing::Point(control->Location.X, (control->Parent->Size.Height - 10));


	if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
		auto controlChieldRule = getControlChieldRule(control->Parent->Name, control->Name);

		if (!controlChieldRule)
			return;

		controlChieldRule->set_property("locationX", std::to_string(control->Location.X));
		controlChieldRule->set_property("locationY", std::to_string(control->Location.Y));
		return;
	}
	std::shared_ptr<ControlInfo> controlRule = getControlRule(control->Name);

	if (!controlRule)
		return;

	controlRule->set_property("locationX", std::to_string(control->Location.X));
	controlRule->set_property("locationY", std::to_string(control->Location.Y));

	radPanel3->Invalidate();
}
System::Void  FormEditor::EventResize(System::Object^  sender, System::EventArgs^  e) {
	if (disable_update)
		return;

	Control^ control = (Control^)sender;

	if (control->Parent){
		if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
			std::shared_ptr<ControlInfoChield> controlChieldRule = getControlChieldRule(control->Parent->Name, control->Name);

			if (!controlChieldRule)
				return;

			controlChieldRule->set_property("sizeW", std::to_string(control->Size.Width));
			controlChieldRule->set_property("sizeH", std::to_string(control->Size.Height));
			return;
		}
	}
	std::shared_ptr<ControlInfo> controlRule = getControlRule(control->Name);

	if (!controlRule)
		return;

	controlRule->set_property("sizeW", std::to_string(control->Size.Width));
	controlRule->set_property("sizeH", std::to_string(control->Size.Height));

	radPanel3->Invalidate();
}
System::Void  FormEditor::EventCheckStateChanged(System::Object^  sender, System::EventArgs^  e) {
	auto control_ = (System::Windows::Forms::Control^)sender;

	Telerik::WinControls::UI::RadCheckBox^ control = (Telerik::WinControls::UI::RadCheckBox^)control_;

	if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
		std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control_->Name);

		if (!controlChiedlRule)
			return;

		controlChiedlRule->set_property("state", managed_util::fromSS(control->Checked.ToString()));
		return;
	}
	std::shared_ptr<ControlInfo> control_Rule = getControlRule(control_->Name);

	if (!control_Rule)
		return;

	control_Rule->set_property("state", managed_util::fromSS(control->Checked.ToString()));
}
System::Void  FormEditor::Event_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	Control^ control = (Control^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
		std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);

		if (!controlChiedlRule)
			return;

		controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));
		return;
	}

	std::shared_ptr<ControlInfo> control_Rule = getControlRule(control->Name);

	if (!control_Rule)
		return;

	control_Rule->set_property("text", managed_util::fromSS(control->Text));
}
System::Void FormEditor::Repoter_ValueChanged(System::Object^  sender, System::EventArgs^  e){
	Control^ control = (Control^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid != control->Parent->GetType())
		return;

	std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);
	if (!controlChiedlRule)
		return;

	std::string repoter_id = managed_util::fromSS((String^)control->Name);

	Regex^ regex = gcnew Regex("(.*)Item");
	Match^ match = regex->Match(managed_util::fromString(repoter_id));
	if (match->Success)
		repoter_id = managed_util::fromSS(match->Value->Replace("Item", ""));

	controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));

	if (control->Name->IndexOf("spinmin") >= 0){
		String^ item_id = control->Name->Replace(managed_util::fromString(repoter_id), "")->Replace("spinmin", "");
		
		std::shared_ptr<RepotId> repoterIdRule = RepoterManager::get()->getRepoIdRule(repoter_id);
		if (!repoterIdRule)
			return;
		
		std::shared_ptr<RepotItens> itemRule = repoterIdRule->getRepotItensRule(managed_util::fromSS(item_id));
		if (!itemRule)
			return;

		itemRule->set_min(Convert::ToInt32(control->Text));		
	}
	else{
		String^ item_id = control->Name->Replace(managed_util::fromString(repoter_id), "")->Replace("spinmax", "");

		std::shared_ptr<RepotId> repoterIdRule = RepoterManager::get()->getRepoIdRule(repoter_id);
		if (!repoterIdRule)
			return;

		std::shared_ptr<RepotItens> itemRule = repoterIdRule->getRepotItensRule(managed_util::fromSS(item_id));
		if (!itemRule)
			return;

		itemRule->set_max(Convert::ToInt32(control->Text));
	}
	return;
}
System::Void  FormEditor::Repoter_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	Control^ control = (Control^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid != control->Parent->GetType())
		return;

	std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);
	if (!controlChiedlRule)
		return;

	std::string repoter_id = managed_util::fromSS((String^)control->Name);

	Regex^ regex = gcnew Regex("(.*)Item");
	Match^ match = regex->Match(managed_util::fromString(repoter_id));
	if (match->Success)
		repoter_id = managed_util::fromSS(match->Value->Replace("Item", ""));
		
	controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));

	String^ item_id = control->Name->Replace(managed_util::fromString(repoter_id), "")->Replace("combo", "");

	std::shared_ptr<RepotId> repoterIdRule = RepoterManager::get()->getRepoIdRule(repoter_id);
	if (repoterIdRule){
		std::shared_ptr<RepotItens> itemRule = repoterIdRule->getRepotItensRule(managed_util::fromSS(item_id));
		if (itemRule)
			itemRule->set_item_name(managed_util::fromSS(control->Text));
	}
	return;
}
System::Void FormEditor::Depoter_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	Control^ control = (Control^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid != control->Parent->GetType())
		return;

	std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);
	if (!controlChiedlRule)
		return;

	std::string looter_id = managed_util::fromSS((String^)control->Name);

	controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));

	std::shared_ptr<DepotContainer> looter_container = DepoterManager::get()->getDepotContainer(looter_id);
	if (looter_container)
		looter_container->set_ContainerName(managed_util::fromSS(control->Text));

	return;
}
System::Void FormEditor::Looter_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	Control^ control = (Control^)sender;

	if (Telerik::WinControls::UI::RadGroupBox::typeid != control->Parent->GetType())
		return;

	std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);
	if (!controlChiedlRule)
		return;

	std::string looter_id = managed_util::fromSS((String^)control->Name);

	controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));

	LooterContainerPtr looter_container = LooterManager::get()->get_looter_container_by_id(looter_id);
	if (looter_container)
		looter_container->set_container_name(managed_util::fromSS(control->Text));

	return;
}
System::Void FormEditor::Spells_StateChanged(System::Object^  sender, System::EventArgs^  e) {
	auto control_ = (System::Windows::Forms::Control^)sender;

	Telerik::WinControls::UI::RadCheckBox^ control = (Telerik::WinControls::UI::RadCheckBox^)control_;
	if (Telerik::WinControls::UI::RadGroupBox::typeid != control->Parent->GetType())
		return;
	
	std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control_->Name);	
	if (!controlChiedlRule)
		return;

	if (control->Parent->Text == "SpellsAttackGroup"){
		String^ name = control->Name->Replace("Attack", "");
		std::shared_ptr<SpellAttack> spellAttack = SpellManager::get()->getAttackSpellInListById(Convert::ToInt32(name));
		if (!spellAttack)
			return;

		spellAttack->set_checked(control->Checked);
	}
	else{
		String^ name = control->Name->Replace("Health", "");
		std::shared_ptr<SpellHealth> spellHealth = SpellManager::get()->getHealthSpellInListById(Convert::ToInt32(name));
		if (!spellHealth)
			return;

		spellHealth->set_checked(control->Checked);
	}

	controlChiedlRule->set_property("state", managed_util::fromSS(control->Checked.ToString()));
	return;
}

void FormEditor::loadControlInForm(){
	disable_update = true;
	auto mapControls_ = ControlManager::get()->getMap_();
	auto mapControls = ControlManager::get()->getMap();

	auto tempW = mapControls_["sizeW"];
	auto tempH = mapControls_["sizeH"];

	this->Size = System::Drawing::Size(boost::lexical_cast<int>(mapControls_["sizeW"]), boost::lexical_cast<int>(mapControls_["sizeH"]));

	for (auto it : mapControls){
		auto mapControlInfo = it.second->getMapInfo();
		auto mapControlChield = it.second->getMapChield();

		if (mapControlInfo["type"] == "button"){
			auto controlInfo = createButton(nullptr, gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])));


			radPanel3->Controls->Add(controlInfo);
			disable_update = false;
		}
		else if (mapControlInfo["type"] == "text"){
			auto controlInfo = createTextBox(nullptr, gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])));


			radPanel3->Controls->Add(controlInfo);
			disable_update = false;
		}
		else if (mapControlInfo["type"] == "check"){
			auto controlInfo = createCheckBox(nullptr, (string_util::lower(mapControlInfo["state"]) == "true"), gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])));

			radPanel3->Controls->Add(controlInfo);
			disable_update = false;
		}
		else if (mapControlInfo["type"] == "spin"){
			std::string maxvalue = mapControlInfo["maxvalue"];
			std::replace(maxvalue.begin(), maxvalue.end(), ',', '.');


			std::string minvalue = mapControlInfo["minvalue"];
			std::replace(minvalue.begin(), minvalue.end(), ',', '.');

			auto controlInfo = createSpinEditor(nullptr, gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])),
				boost::lexical_cast<double>(maxvalue), boost::lexical_cast<double>(minvalue), boost::lexical_cast<int>(mapControlInfo["value"]));


			radPanel3->Controls->Add(controlInfo);
			disable_update = false;
		}
		else if (mapControlInfo["type"] == "combobox"){
			auto controlInfo = createComboBox(nullptr, gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])), managed_util::convert_vector_to_array(it.second->vectorItemComboBox()));


			radPanel3->Controls->Add(controlInfo);
			disable_update = false;
		}
		else if (mapControlInfo["type"] == "label"){
			auto controlInfo = createLabel(nullptr, gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])));


			radPanel3->Controls->Add(controlInfo);
			disable_update = false;
		}

		else if (mapControlInfo["type"] == "group"){
			auto controlInfo_ = createGroup(gcnew String(it.first.c_str()), gcnew String(mapControlInfo["text"].c_str()),
				System::Drawing::Point(boost::lexical_cast<int>(mapControlInfo["locationX"]), boost::lexical_cast<int>(mapControlInfo["locationY"])),
				System::Drawing::Size(boost::lexical_cast<int>(mapControlInfo["sizeW"]), boost::lexical_cast<int>(mapControlInfo["sizeH"])));


			radPanel3->Controls->Add(controlInfo_);

			for (auto controlChield : mapControlChield){
				auto mapControlChieldInfo = controlChield.second->getMapInfo();

				if (mapControlChieldInfo["type"] == "button"){
					auto controlInfo = createButton((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())), gcnew String(controlChield.first.c_str()), gcnew String(mapControlChieldInfo["text"].c_str()),
						System::Drawing::Point(boost::lexical_cast<int>(mapControlChieldInfo["locationX"]), boost::lexical_cast<int>(mapControlChieldInfo["locationY"])),
						System::Drawing::Size(boost::lexical_cast<int>(mapControlChieldInfo["sizeW"]), boost::lexical_cast<int>(mapControlChieldInfo["sizeH"])));


					get_control(gcnew String(it.first.c_str()))->Controls->Add(controlInfo);
					disable_update = false;
				}
				else if (mapControlChieldInfo["type"] == "check"){
					auto controlInfo = createCheckBox((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())), (string_util::lower(mapControlChieldInfo["state"]) == "true"), gcnew String(controlChield.first.c_str()), gcnew String(mapControlChieldInfo["text"].c_str()),
						System::Drawing::Point(boost::lexical_cast<int>(mapControlChieldInfo["locationX"]), boost::lexical_cast<int>(mapControlChieldInfo["locationY"])),
						System::Drawing::Size(boost::lexical_cast<int>(mapControlChieldInfo["sizeW"]), boost::lexical_cast<int>(mapControlChieldInfo["sizeH"])));


					((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())))->Controls->Add(controlInfo);
					disable_update = false;
				}
				else if (mapControlChieldInfo["type"] == "text"){
					auto controlInfo = createTextBox((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())), gcnew String(controlChield.first.c_str()), gcnew String(mapControlChieldInfo["text"].c_str()),
						System::Drawing::Point(boost::lexical_cast<int>(mapControlChieldInfo["locationX"]), boost::lexical_cast<int>(mapControlChieldInfo["locationY"])),
						System::Drawing::Size(boost::lexical_cast<int>(mapControlChieldInfo["sizeW"]), boost::lexical_cast<int>(mapControlChieldInfo["sizeH"])));


					((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())))->Controls->Add(controlInfo);
					disable_update = false;
				}
				else if (mapControlChieldInfo["type"] == "spin"){
					std::string maxvalue = mapControlChieldInfo["maxvalue"];
					std::replace(maxvalue.begin(), maxvalue.end(), ',', '.');


					std::string minvalue = mapControlChieldInfo["minvalue"];
					std::replace(minvalue.begin(), minvalue.end(), ',', '.');


					auto controlInfo = createSpinEditor((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())), gcnew String(controlChield.first.c_str()), gcnew String(mapControlChieldInfo["text"].c_str()),
						System::Drawing::Point(boost::lexical_cast<int>(mapControlChieldInfo["locationX"]), boost::lexical_cast<int>(mapControlChieldInfo["locationY"])),
						System::Drawing::Size(boost::lexical_cast<int>(mapControlChieldInfo["sizeW"]), boost::lexical_cast<int>(mapControlChieldInfo["sizeH"])),
						boost::lexical_cast<double>(maxvalue), boost::lexical_cast<double>(minvalue), boost::lexical_cast<int>(mapControlChieldInfo["value"]));


					((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())))->Controls->Add(controlInfo);
					disable_update = false;
				}
				else if (mapControlChieldInfo["type"] == "combobox"){
					auto controlInfo = createComboBox((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())), gcnew String(controlChield.first.c_str()), gcnew String(mapControlChieldInfo["text"].c_str()),
						System::Drawing::Point(boost::lexical_cast<int>(mapControlChieldInfo["locationX"]), boost::lexical_cast<int>(mapControlChieldInfo["locationY"])),
						System::Drawing::Size(boost::lexical_cast<int>(mapControlChieldInfo["sizeW"]), boost::lexical_cast<int>(mapControlChieldInfo["sizeH"])), managed_util::convert_vector_to_array(controlChield.second->vectorItemComboBox()));


					((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())))->Controls->Add(controlInfo);
					disable_update = false;
				}
				else if (mapControlChieldInfo["type"] == "label"){
					auto controlInfo = createLabel((Telerik::WinControls::UI::RadGroupBox^)get_control(gcnew String(it.first.c_str())), gcnew String(controlChield.first.c_str()), gcnew String(mapControlChieldInfo["text"].c_str()),
						System::Drawing::Point(boost::lexical_cast<int>(mapControlChieldInfo["locationX"]), boost::lexical_cast<int>(mapControlChieldInfo["locationY"])),
						System::Drawing::Size(boost::lexical_cast<int>(mapControlChieldInfo["sizeW"]), boost::lexical_cast<int>(mapControlChieldInfo["sizeH"])));


					get_control(gcnew String(it.first.c_str()))->Controls->Add(controlInfo);
					disable_update = false;
				}
			}
		}
	}
}

bool  FormEditor::save_script(std::string file_name){
	Json::Value file;
	file["version"] = 0100;

	file["ControlManager"] = ControlManager::get()->parse_class_to_json();

	return saveJsonFile(file_name, file);
}
bool  FormEditor::load_script(std::string file_name){
	Json::Value file = loadJsonFile(file_name);
	if (file.isNull())
		return false;

	ControlManager::get()->parse_json_to_class(file["ControlManager"]);
	return true;
}

System::Void  FormEditor::buttonSave_Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::SaveFileDialog fileDialog;
	fileDialog.Filter = "Setup|*.Setup";
	fileDialog.Title = "Save Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	if (!save_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Saved", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved", "Save/Load");
}
System::Void  FormEditor::buttonLoad_Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::OpenFileDialog fileDialog;
	fileDialog.Filter = "Setup|*.Setup";
	fileDialog.Title = "Open Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	radPanel3->Controls->Clear();

	for each(auto node in TreeViewControls->Nodes)
		node->Nodes->Clear();

	ControlManager::get()->clear();

	if (!load_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

	loadControlInForm();
}

System::Void  FormEditor::FormEditor_Resize(System::Object^  sender, System::EventArgs^  e) {
	if (temp_disable)
		return;

	Control^ control = (Control^)sender;

	ControlManager::get()->set_property("sizeW", std::to_string(this->Size.Width));
	ControlManager::get()->set_property("sizeH", std::to_string(this->Size.Height));
}

System::Void  FormEditor::bt_move_Click(System::Object^  sender, System::EventArgs^  e) {
	if (move_mode){
		move_mode = false;
		bt_move->Text = L"Move Mode Off";
	}
	else if (!move_mode){
		move_mode = true;
		bt_move->Text = L"Move Mode On";
	}
}
System::Void  FormEditor::Event_Label_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	Control^ control = (Control^)sender;

	if (Telerik::WinControls::UI::RadLabel::typeid != control->GetType())
		return;

	if (Telerik::WinControls::UI::RadGroupBox::typeid == control->Parent->GetType()){
		std::shared_ptr<ControlInfoChield> controlChiedlRule = getControlChieldRule(control->Parent->Name, control->Name);

		if (!controlChiedlRule)
			return;

		controlChiedlRule->set_property("text", managed_util::fromSS(control->Text));
		return;
	}

	std::shared_ptr<ControlInfo> control_Rule = getControlRule(control->Name);

	if (!control_Rule)
		return;

	control_Rule->set_property("text", managed_util::fromSS(control->Text));

}

void  FormEditor::update_idiom(){
	buttonSave->Text = gcnew String(&GET_TR(managed_util::fromSS(buttonSave->Text))[0]);
	buttonLoad->Text = gcnew String(&GET_TR(managed_util::fromSS(buttonLoad->Text))[0]);
	radMenuItem1->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem1->Text))[0]);
	bt_move->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_move->Text))[0]);
}

System::Void  FormEditor::radPropertyGrid1_Edited(System::Object^  sender, Telerik::WinControls::UI::PropertyGridItemEditedEventArgs^  e) {
	disable_update2 = false;
	Telerik::WinControls::UI::PropertyGridItem^ Item = (Telerik::WinControls::UI::PropertyGridItem^) e->Item;

	if (!TreeViewControls->SelectedNode->Parent)
		return;

	if (TreeViewControls->SelectedNode->Parent->Tag == "group"){
		std::shared_ptr<ControlInfoChield> controlChieldRule = getControlChieldRule(TreeViewControls->SelectedNode->Parent->Text, TreeViewControls->SelectedNode->Text);

		if (!controlChieldRule)
			return;

		if (Item->Name == "Maximum")
			controlChieldRule->set_property("maxvalue", managed_util::fromSS(Item->Value->ToString()));

		else if (Item->Name == "Minimum")
			controlChieldRule->set_property("minvalue", managed_util::fromSS(Item->Value->ToString()));

		else if (Item->Name == "Text")
			controlChieldRule->set_property("text", managed_util::fromSS(Item->Value->ToString()));

		else if (Item->Name == "Items") {
			Telerik::WinControls::UI::RadListDataItemCollection^ list = (Telerik::WinControls::UI::RadListDataItemCollection^)Item->Value;

			controlChieldRule->clear_itemCombox();

			for (int index = 0; index < list->Count; index++)
				controlChieldRule->add_itemComboBox(managed_util::fromSS(list[index]->Text));
		}
		return;
	}


	std::shared_ptr<ControlInfo> controlRule = getControlRule(TreeViewControls->SelectedNode->Text);

	if (!controlRule)
		return;

	if (Item->Name == "Maximum")
		controlRule->set_property("maxvalue", managed_util::fromSS(Item->Value->ToString()));

	else if (Item->Name == "Minimum")
		controlRule->set_property("minvalue", managed_util::fromSS(Item->Value->ToString()));

	else if (Item->Name == "Text")
		controlRule->set_property("text", managed_util::fromSS(Item->Value->ToString()));

	else if (Item->Name == "Items") {
		Telerik::WinControls::UI::RadListDataItemCollection^ list = (Telerik::WinControls::UI::RadListDataItemCollection^) Item->Value;

		controlRule->clear_itemCombox_();

		for (int index = 0; index < list->Count; index++)
			controlRule->add_itemComboBox_(managed_util::fromSS(list[index]->Text));
	}
}
System::Void  FormEditor::radPropertyGrid1_Editing(System::Object^  sender, Telerik::WinControls::UI::PropertyGridItemEditingEventArgs^  e) {
	Telerik::WinControls::UI::PropertyGridItem^ Item = (Telerik::WinControls::UI::PropertyGridItem^) e->Item;
	OldValue_Grid = Convert::ToString(Item->Value);
}
System::Void  FormEditor::radPropertyGrid1_PropertyValidating(System::Object^  sender, Telerik::WinControls::UI::PropertyValidatingEventArgs^  e) {
	disable_update2 = true;
	Telerik::WinControls::UI::PropertyGridItem^ Item = (Telerik::WinControls::UI::PropertyGridItem^) e->Item;
	if (e->Item->Name != "Location" && e->Item->Name != "Size" && e->Item->Name != "Maximum" && e->Item->Name != "Minimum" && e->Item->Name != "Value" &&  e->Item->Name != "Text"){
		e->Cancel = true;
		//Item->Value = OldValue_Grid;
		return;
	}

	String^ newValue = e->NewValue->ToString();

	if (e->Item->Name == "Size" || e->Item->Name == "Location"){
		if (!newValue->Contains(";")){
			e->Cancel = true;
			//Item->Value = OldValue_Grid;
			return;
		}
		String^ newValue_ = newValue->Replace(" ", "");

		if (!Regex::IsMatch(newValue_, "([0-9]+(;)[0-9]+)")){
			e->Cancel = true;
			//Item->Value = OldValue_Grid;
			return;
		}
		else  if (Regex::IsMatch(newValue_, "([a-z\\-]+)")){
			e->Cancel = true;
			//Item->Value = OldValue_Grid;
			return;
		}
		return;
	}

	String^ newValue_ = newValue->Replace(" ", "");
	newValue_ = newValue_->Replace(",", "");

	if (e->Item->Name == "Location" && e->Item->Name == "Size" && e->Item->Name == "Maximum" && e->Item->Name == "Minimum" && e->Item->Name == "Value"){
		if (!Regex::IsMatch(newValue_, "([0-9])")){
			e->Cancel = true;
			//Item->Value = OldValue_Grid;
			return;
		}
	}
}