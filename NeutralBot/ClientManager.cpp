#pragma once 
#include "ClientManager.h"
#include "NavigationManager.h"

void Client::check_send_data() {
	if (!_my_socket)
		return;

	if (is_sending || !send_buffer.size() || !_my_socket->is_open())
		return;

	is_sending = true;

	std::shared_ptr<std::string> _buffer(new std::string(send_buffer.data(), send_buffer.size()));

	//send_buffer.clear();

	_my_socket->async_send(boost::asio::buffer(_buffer->data(), _buffer->size()),
		boost::bind(&Client::on_send, shared_from_this(), _my_socket, 
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred, _buffer));
}

void Client::set_connect(std::string ip, uint16_t port) {	
	if (!connection_error && current_server_ip == ip && current_server_port == port)
		return;
	
	mtx.lock();
	current_server_ip = ip;
	current_server_port = port;
	is_close = false;	

	if (_my_socket && _my_socket->is_open())
		_my_socket->close();

	mtx.unlock();

	connect(true);
}

void Client::on_connect(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder, const boost::system::error_code& error) {
	is_connecting = false;

	if (error) {
		std::cout << "\n error" << error << " message" << error.message();
		if (socket_holder == _my_socket){
			connection_error = true;
			connect(true);
		}
		return;
	}

	mtx.lock();
	if (current_server_ip != socket_holder->remote_endpoint().address().to_string() ||
		socket_holder->remote_endpoint().port() != current_server_port){
		if (socket_holder == _my_socket){
			connection_error = true;
			connect(true);
		}
		mtx.unlock();
		return;
	}
	mtx.unlock();

	NavigationManager::get()->frist_packet();
	check_set_recv();
}

void Client::check_set_recv() {
	if (is_recving || !_my_socket->is_open())
		return;

	is_recving = true;

	_my_socket->async_receive(boost::asio::buffer(recv_buffer, sizeof(recv_buffer)),
		boost::bind(&Client::on_recv, shared_from_this(), _my_socket,
		boost::asio::placeholders::error, boost::asio::placeholders::bytes_transferred));
}

void Client::add_send_data(std::shared_ptr<std::string> data) {
	if (is_sending)
		send_buffer.clear();

	if (send_buffer.size())
		send_buffer.clear();

	send_buffer.append(data->data(), data->size());
	check_send_data();
}

void Client::close() {
	if (is_close)
		return;

	if (_my_socket && _my_socket->is_open())
		_my_socket->close();
}

void Client::on_recv(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder,
	const boost::system::error_code& error, const size_t bytes_transferred) {

	is_recving = false;

	if (error)  {
		std::cout << "\n ERROR " << error.message();

		if (_my_socket != socket_holder)
			return;
					
		connection_error = true;
		connect(true);
		return;
	}
	
	NavigationManager::get()->process_message(recv_buffer, bytes_transferred);

	check_set_recv();
}

void Client::connect(bool lock){
	if (is_connecting)
		return;

	if (is_close)
		return;

	if (lock)
		mtx.lock();

	std::string server_ip = current_server_ip;
	uint16_t server_port = current_server_port;
	
	try{
		std::cout << "\n Chamando connect novamente";
		if (!_my_socket){
			_my_socket = std::shared_ptr<boost::asio::ip::tcp::socket>(new boost::asio::ip::tcp::socket(_io_service));
		}
		else{
			if (_my_socket->is_open())
				_my_socket->close();

			_my_socket = std::shared_ptr<boost::asio::ip::tcp::socket>(
				new boost::asio::ip::tcp::socket(_io_service));
		}
		
		boost::asio::ip::address_v4::from_string(server_ip);
	}
	catch (std::exception& ex){
		std::cout << "\n EX " << ex.what();
		if (lock)
			mtx.unlock();
		return;
	}

	is_connecting = true;
	connection_error = false;

	_my_socket->async_connect(boost::asio::ip::tcp::endpoint(
		boost::asio::ip::address_v4::from_string(server_ip), server_port),
		boost::bind(&Client::on_connect, shared_from_this(), _my_socket, boost::asio::placeholders::error));
	
	if (lock)
		mtx.unlock();
}

void Client::on_send(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder,
	const boost::system::error_code& error, const size_t bytes_transferred, std::shared_ptr<std::string> _data_holder) {
	is_sending = false;

	send_buffer.clear();

	if (error) {
		std::cout << "\n ERROR " << error.message();
		
		if (_my_socket != socket_holder)
			return;
		
		connection_error = true;
		connect(true);
		return;
	}

	check_send_data();
}