#pragma once
#include "LuaBackgroundManager.h"
#include "Core\Util.h"
#include "Core\LuaCore.h"

#pragma pack(push,1)
class ThreadHandler : public std::enable_shared_from_this<ThreadHandler>{
	std::shared_ptr<LuaBackgroundCodes> current_script;
	uint32_t thread_ptr = 0;
	bool is_alive = false;
	bool is_used = false;
	LuaCore* luaCore = nullptr;

	std::shared_ptr<LuaCore> shared_luaCore;
	neutral_mutex mtx_access;
public:
	ThreadHandler();
	~ThreadHandler();
	void start_loop();
	std::shared_ptr<ThreadHandler> getPtr();
	bool get_is_used();
	bool get_is_alive();
	std::shared_ptr<LuaBackgroundCodes> get_current_script();
	
	void execute_script(std::string code, std::string name);
	void remove_current_script();
	bool set_current_script(std::shared_ptr<LuaBackgroundCodes> script);
	bool need_execute();
};
#pragma pack(pop)


