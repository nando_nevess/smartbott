//#ifdef DEV_MODE

#include "DevelopmentManager.h"

#include <winternl.h>
#include <TlHelp32.h>
#include <Windows.h>
#include <WinBase.h>


std::map<std::string/*mainidentifier*/, std::map<std::string/*subidentifier*/, uint64_t/*value*/>> DevelopmentManager::get_time_costs(){
	lock_guard _lock(mtx_access);
	return time_costs;
}

std::map<uint32_t/*threadid*/, std::string/*name*/> DevelopmentManager::get_threads(){
	lock_guard _lock(mtx_access);
	return threads;
}

void DevelopmentManager::set_id_time(std::string main_id, std::string sub_id, uint64_t value){
	lock_guard _lock(mtx_access);
	time_costs[main_id][sub_id] = value;
}

uint64_t DevelopmentManager::get_id_time(std::string main_id, std::string sub_id){
	lock_guard _lock(mtx_access);
	return time_costs[main_id][sub_id];
} 

DevelopmentManager* DevelopmentManager::get(){
	static DevelopmentManager* mDevelopmentManager = nullptr;

	if (!mDevelopmentManager)
		mDevelopmentManager = new DevelopmentManager;

	return mDevelopmentManager;
}

std::shared_ptr<DevelopmentManager::ThreadAutoRelease> DevelopmentManager::registerThread(uint32_t threadId, std::string name){
	lock_guard _lock(mtx_access);
	threads[threadId] = name;
	return std::shared_ptr<ThreadAutoRelease>(new ThreadAutoRelease(threadId));
}

bool DevelopmentManager::thread_is_on(uint32_t threadId, std::string name){
	return true;
	//return true;
	if (name == "BotManager::auto_save"){
		return false;
	}
	if (name == "LooterCore::start"){
		return false;
	}
	if (name == "LastLootControl::check_loot_position"){
		return false;
	}
	if (name == "LooterCore::run_check_looted"){
		return false;
	}
	if (name == "Messages::refresher"){
		return false;
	}
	
	if (name == "BotManager::auto_saveBotManager::init"){
		return false;
	}
	if (name == "AlertsCore::refresh"){
		return false;
	}
	/*if (name == "Messages::refresher"){
		return false;
	}*/
	/*if (name == "TakeSkinCore::run"){
		return false;
	}
	*/

	if (name == "MapMinimap::MapMinimap"){
		return false;
	}
	if (name == "LuaThread::refresh"){
		return false;
	}
	if (name == "TakeSkinCore::run"){
		return false;
	}
	if (name == "HunterCore::runHunterCore::refresher"){
		return false;
	}
	if (name == "HunterCore::path_runner_thread"){
		return false;
	}
	if (name == "HunterCore::runHunterCore::refresher"){
		return false;
	}
	if (name == "SpellCasterCore::executor"){
		return false;
	}
	if (name == "BattleList::refresher"){
		return false;
	}
	
	if (name == "HunterCore::run"){
		return false;
	}

	if (name == "HunterCore::refresher"){
		return false;
	}

	if (name == "HunterCore::run_refresher_service"){
		return false;
	}



	if (name == "HunterCore::path_runner_thread"){
		return false;
	}
	if (name == "ContainerManager::refresh_thread"){
		return false;
	}
	/*if (name == "GifManager::items_load"){
		return false;
	}*/
	/*if (name == "BotManager::init"){
	return false;
	}*/
	/*if (name == "GifManager::items_load"){
	return false;
	}*/
	/*
	if (name == "WaypointerCore::run"){
		return false; 
		}*/


	/*if (name == "Pathfinder::path_walker"){
		return false;
	}
	*/
	/*



	DevelopmentManager::thread_is_on|BotManager::init
	DevelopmentManager::thread_is_on|ThreadHandler::{ctor}::<lambda_6f71
	DevelopmentManager::thread_is_on|GifManager::items_load
	DevelopmentManager::thread_is_on|WaypointerCore::run
	DevelopmentManager::thread_is_on|Pathfinder::path_walker
	

	*/
	/*if (name == "GifManager::items_load"){
		return false;
	}*/
	/*if (name == "MapMinimap::MapMinimap"){
		return false;
	}

	
/*	NAME BotManager::init
		NAME BotManager::auto_save
		NAME LooterCore::start
		NAME LastLootControl::check_loot_position
		NAME LooterCore::run_check_looted
		NAME Messages::refresherADDED radToggleButtonSpellcaster*/

	return true;
}

void DevelopmentManager::registerThreadNotAutoDelete(uint32_t threadId, std::string name){
	lock_guard _lock(mtx_access);
	threads[threadId] = name;
}

void DevelopmentManager::unregisterThread(uint32_t threadId){
	lock_guard _lock(mtx_access);
	auto found_it = threads.find(threadId);
	if (found_it != threads.end()){
		threads.erase(found_it);
	}
}

DevelopmentMethodTimeCost::DevelopmentMethodTimeCost(std::string _main_id){
	main_id = _main_id;
}

DevelopmentMethodTimeCost::~DevelopmentMethodTimeCost(){
	log_main_time();
}

void DevelopmentMethodTimeCost::log_main_time(){
	DevelopmentManager::get()->set_id_time(main_id, main_id, start_time.elapsed_milliseconds());
}


void DevelopmentMethodTimeCost::reset_sub_time(){
	sub_start_time.reset();
}

void DevelopmentMethodTimeCost::reset_main_time(){
	start_time.reset();
}

void DevelopmentMethodTimeCost::log_sub_time(std::string log_id, bool reset){
	DevelopmentManager::get()->set_id_time(main_id, log_id, sub_start_time.elapsed_milliseconds());
	if (reset)
		reset_sub_time();
}

namespace DevelopmentUtil{
	uint64_t get_thread_delta(uint32_t thread_id){
		HANDLE thread_handler = OpenThread(
			THREAD_QUERY_INFORMATION, false, thread_id);

		FILETIME creationTime;
		FILETIME exitTime;
		FILETIME kernelTime;
		FILETIME userTime;


		if (thread_handler){
			int result = GetThreadTimes(thread_handler, &creationTime, &exitTime, &kernelTime, &userTime);
			CloseHandle(thread_handler);
			return *(uint64_t*)&kernelTime
				+ *(uint64_t*)&userTime;
		}
		return 0;
	}


	void suspend_thread(uint32_t tid){
		HANDLE thread_handler = OpenThread(
			THREAD_SUSPEND_RESUME, false, tid);
		if (!thread_handler)
			return;
		SuspendThread(thread_handler);
		CloseHandle(thread_handler);
	}

	void resume_thread(uint32_t tid){
		HANDLE thread_handler = OpenThread(
			THREAD_SUSPEND_RESUME, false, tid);
		if (!thread_handler)
			return;
		ResumeThread(thread_handler);
		CloseHandle(thread_handler);
	}
}


void register_in_monitor(char* thread_name){
	DevelopmentManager::get()->registerThreadNotAutoDelete(GetCurrentThreadId(), thread_name);
}
//#endif