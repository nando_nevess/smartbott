#include "XMLSpellManager.h"

XMLSpellManager gXMLSpellManager;

XMLSpellManager::XMLSpellManager()
{
	LoadSpells();
}

void XMLSpellManager::LoadSpells()
{
	TiXmlDocument doc(".\\conf\\defaults\\spell_info.xml");
	if (doc.LoadFile()) {
		TiXmlElement* element = (TiXmlElement*)doc.FirstChild("ListSpells");

		TiXmlElement* elementSpell = (TiXmlElement*)element->FirstChild("Spell");

		while (elementSpell) {
			XMLSpellInfo spellInfo;

			std::string name = elementSpell->FirstChild("Name")->ToElement()->GetText();
			spellInfo.set_name(name);

			std::string cast = elementSpell->FirstChild("Cast")->ToElement()->GetText();
			spellInfo.set_cast(cast);

			std::string mana = elementSpell->FirstChild("Mana")->ToElement()->GetText();
			spellInfo.set_mana(std::stoi(mana));

			std::string level = elementSpell->FirstChild("Level")->ToElement()->GetText();
			spellInfo.set_level(std::stoi(level));
			
			std::string magic = elementSpell->FirstChild("Magic")->ToElement()->GetText();
			spellInfo.set_magicLevel(std::stoi(magic));
			
			std::string premium = elementSpell->FirstChild("Premium")->ToElement()->GetText();
			spellInfo.set_premium(XMLconvertStringToBoolean(premium));

			std::string category = elementSpell->FirstChild("Category")->ToElement()->GetText();
			spellInfo.set_category(XMLconvertStringToInt(category));

			std::string type = elementSpell->FirstChild("Type")->ToElement()->GetText();
			spellInfo.set_type(XMLconvertStringToInt(type));

			std::string soul = elementSpell->FirstChild("Soul")->ToElement()->GetText();
			spellInfo.set_soul(std::stoi(soul));

			std::string range = elementSpell->FirstChild("Range")->ToElement()->GetText();
			spellInfo.set_range(std::stoi(range));

			std::string cooldownCategory = elementSpell->FirstChild("CoolDown1")->ToElement()->GetText();
			spellInfo.set_cooldownCategory(std::stoi(cooldownCategory));

			std::string cooldownSpell = elementSpell->FirstChild("CoolDown2")->ToElement()->GetText();
			spellInfo.set_cooldownSpell(std::stoi(cooldownSpell));

			std::string cooldownId = elementSpell->FirstChild("CoolDownID")->ToElement()->GetText();
			spellInfo.set_cooldownId(std::stoi(cooldownId));

			if (elementSpell->FirstChild("min_hp")){
				std::string min_hp = elementSpell->FirstChild("min_hp")->ToElement()->GetText();
				spellInfo.set_min_hp(std::stoi(min_hp));
			}else
				spellInfo.set_min_hp(0);

			if (elementSpell->FirstChild("max_hp")){
				std::string max_hp = elementSpell->FirstChild("max_hp")->ToElement()->GetText();
				spellInfo.set_max_hp(std::stoi(max_hp));
			}else				
				spellInfo.set_max_hp(100);
			
			spellsInfoList.push_back(spellInfo);
			elementSpell = elementSpell->NextSiblingElement();
		}
	}
}

bool XMLSpellManager::XMLconvertStringToBoolean(std::string string)
{
	if (string == "true")
		return true;
	else if (string == "false")
		return false;

	return false;
}

int XMLSpellManager::XMLconvertStringToInt(std::string string)
{
	if (string == "Attack")
		return 0;
	else if (string == "Healing")
		return 1;
	else if (string == "Support")
		return 2;
	else if (string == "Special")
		return 3;
	else if (string == "Missile")
		return 0;
	else if (string == "Area")
		return 1;
	else if (string == "Instant")
		return 2;
	else if (string == "Strike")
		return 3;

	return 0;
}

int XMLSpellManager::getSpellCooldownId(std::string name){
	for (auto spellInfo : spellsInfoList) 
		if (string_util::string_compare(spellInfo.get_cast(), name, true))
			return spellInfo.get_cooldownId();
	return 0;
}

std::vector<XMLSpellInfo> XMLSpellManager::getspellsInfoList(){
	return spellsInfoList;
}