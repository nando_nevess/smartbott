#include <Windows.h>


class NotificationManager{
	static void notify_baloon(char* caption, char* text, int time_ms);
	static void print_console(char* text);
	static void add_error_log(char* error_group, char* error_text);
	static bool show_message_box(char* caption, char* text);
};