#pragma once
#include "PlayerInfo.h"


class NaviPlayerCreatures{
public:

	uint32_t* mtx_access;

	TimeChronometer time_last_update;

	uint32_t creatures_on_screen;
	std::string current_creature_name;

	Coordinate current_creature_coordinate;

	bool current_creature_is_monster;
	bool current_creature_is_npc;
	bool current_creature_is_player;
	bool have_player_on_screen;
	bool have_creature_on_screen;
	bool is_trapped_by_creatures;

	NaviPlayerCreatures();
	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonValue);
};

typedef std::shared_ptr<NaviPlayerCreatures> NaviPlayerCreaturesPtr;