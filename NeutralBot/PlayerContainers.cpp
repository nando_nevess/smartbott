#pragma once
#include "PlayerContainers.h"
#include "Core\ContainerManager.h"
#include <mutex>

Json::Value NaviPlayerContainers::parse_class_to_json(){
	return ContainerManager::get()->parse_class_to_json();
}
NaviPlayerContainers::NaviPlayerContainers(){
	mtx_access = (uint32_t*)new std::mutex();
}
void NaviPlayerContainers::parse_json_to_class(Json::Value jsonValue){
	((std::mutex*)mtx_access)->lock();
	count_money_total = jsonValue["count_money_total"].asInt();
	count_container_open = jsonValue["count_container_open"].asInt();

	if (!jsonValue["containersInfo"].empty() || !jsonValue["containersInfo"].isNull()){
		Json::Value containersInfo = jsonValue["containersInfo"];

		for (auto containerInfo : containersInfo){
			std::shared_ptr<ContainerInfo> container = std::shared_ptr<ContainerInfo>(new ContainerInfo());

			container->slots_used = containerInfo["slots_used"].asInt();
			container->slots_free = containerInfo["slots_free"].asInt();
			container->slots_total = containerInfo["slots_total"].asInt();
			container->container_id = containerInfo["container_id"].asInt();
			container->container_name = containerInfo["container_name"].asString();
			container->is_full = containerInfo["is_full"].asBool();
			container->has_next = containerInfo["has_next"].asBool();
			container->is_minimized = containerInfo["is_minimized"].asBool();


			if (!jsonValue["containerItemInfoItems"].empty() || !jsonValue["containerItemInfoItems"].isNull()){
				Json::Value itemsInfo = containerInfo["containerItemInfoItems"];

				for (auto itemInfo : itemsInfo){
					uint32_t item_id = itemInfo["item_id"].asInt();
					uint32_t item_count = itemInfo["item_count"].asInt();

					container->list_item_and_count.push_back(std::make_pair(item_id, item_count));
				}
			}
		}
	}
	((std::mutex*)mtx_access)->unlock();
}