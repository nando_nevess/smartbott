#pragma once

public enum class spell_typeManaged{
	HealingSpells,
	AttackSpellsTarget,
	AttackSpellsArea,
	RuneSpellsTarget,
	RuneSpellsArea,
};

public enum class SpellUsePercentManaged{
	True,
	False,
};

public enum class Spellhealth_type{
	Life,
	Mana,
};


public enum class SpellConditionManaged{
	OnlyUse,
	OnlyWithPlayerOnScreen,
	OnlyWithoutPlayerOnScreen,
};

