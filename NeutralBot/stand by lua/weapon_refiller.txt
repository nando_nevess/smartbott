weapon = "Spear"  -- if you want you can change this by another name or id
weapon_minimun_qty = 7 -- is the minimun quantity when happens the change

if is_secure_move() then
	if Self.weaponamount() <= weapon_minimun_qty then
		putweapon(weapon)
	end
end