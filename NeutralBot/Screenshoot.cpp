#include "Core\Screenshoot.h"

void ScreenShoot::CreateScreenshot(LPCWSTR filename) {
	int x1 = 0;
	int y1 = 0;
	int x2 = GetSystemMetrics(SM_CXSCREEN);
	int y2 = GetSystemMetrics(SM_CYSCREEN);
	ScreenCapture(x1, y1, x2 - x1, y2 - y1, filename);
}
inline int ScreenShoot::GetFilePointer(HANDLE FileHandle){
	return SetFilePointer(FileHandle, 0, 0, FILE_CURRENT);
}
bool ScreenShoot::SaveBMPFile(LPCWSTR filename, HBITMAP bitmap, HDC bitmapDC, int width, int height){

	bool Success = 0;
	HDC SurfDC = NULL;
	HBITMAP OffscrBmp = NULL;
	HDC OffscrDC = NULL;
	LPBITMAPINFO lpbi = NULL;
	LPVOID lpvBits = NULL;
	HANDLE BmpFile = INVALID_HANDLE_VALUE;
	BITMAPFILEHEADER bmfh;
	if ((OffscrBmp = CreateCompatibleBitmap(bitmapDC, width, height)) == NULL)
		return 0;
	if ((OffscrDC = CreateCompatibleDC(bitmapDC)) == NULL)
		return 0;
	HBITMAP OldBmp = (HBITMAP)SelectObject(OffscrDC, OffscrBmp);
	BitBlt(OffscrDC, 0, 0, width, height, bitmapDC, 0, 0, SRCCOPY);
	if ((lpbi = (LPBITMAPINFO)(new char[sizeof(BITMAPINFOHEADER)+256 * sizeof(RGBQUAD)])) == NULL)
		return 0;
	ZeroMemory(&lpbi->bmiHeader, sizeof(BITMAPINFOHEADER));
	lpbi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	SelectObject(OffscrDC, OldBmp);


	GetDIBits(OffscrDC, OffscrBmp, 0, height, NULL, lpbi, DIB_RGB_COLORS); //<- Problem here
	lpvBits = new char[lpbi->bmiHeader.biSizeImage];
	GetDIBits(OffscrDC, OffscrBmp, 0, height, lpvBits, lpbi, DIB_RGB_COLORS); //<- And here

	if ((BmpFile = CreateFile((LPCSTR)filename,
		GENERIC_WRITE,
		0, NULL,
		CREATE_ALWAYS,
		FILE_ATTRIBUTE_NORMAL,
		NULL)) == INVALID_HANDLE_VALUE)
		return 0;
	DWORD Written;
	bmfh.bfType = 19778;
	bmfh.bfReserved1 = bmfh.bfReserved2 = 0;
	if (!WriteFile(BmpFile, &bmfh, sizeof(bmfh), &Written, NULL))
		return 0;
	if (Written < sizeof(bmfh))
		return 0;
	if (!WriteFile(BmpFile, &lpbi->bmiHeader, sizeof(BITMAPINFOHEADER), &Written, NULL))
		return 0;
	if (Written < sizeof(BITMAPINFOHEADER))
		return 0;
	int PalEntries;
	if (lpbi->bmiHeader.biCompression == BI_BITFIELDS)
		PalEntries = 3;
	else PalEntries = (lpbi->bmiHeader.biBitCount <= 8) ?
		(int)(1 << lpbi->bmiHeader.biBitCount) : 0;
	if (lpbi->bmiHeader.biClrUsed)
		PalEntries = lpbi->bmiHeader.biClrUsed;
	if (PalEntries){
		if (!WriteFile(BmpFile, &lpbi->bmiColors, PalEntries * sizeof(RGBQUAD), &Written, NULL))
			return 0;
		if (Written < PalEntries * sizeof(RGBQUAD))
			return 0;
	}
	bmfh.bfOffBits = GetFilePointer(BmpFile);
	if (!WriteFile(BmpFile, lpvBits, lpbi->bmiHeader.biSizeImage, &Written, NULL))
		return 0;
	if (Written < lpbi->bmiHeader.biSizeImage)
		return 0;
	bmfh.bfSize = GetFilePointer(BmpFile);
	SetFilePointer(BmpFile, 0, 0, FILE_BEGIN);
	if (!WriteFile(BmpFile, &bmfh, sizeof(bmfh), &Written, NULL))
	{
		CloseHandle(BmpFile);
		return 0;
	}
	if (Written < sizeof(bmfh))
	{
		CloseHandle(BmpFile);
		return 0;
	}
	CloseHandle(BmpFile);
	return 1;
}
bool ScreenShoot::ScreenCapture(int x, int y, int width, int height, LPCWSTR filename){
	HDC hDc = GetDC((HWND)TibiaProcess::get_default()->get_window_handler());
	HBITMAP hBmp = CreateCompatibleBitmap(GetDC(0), width, height);
	SelectObject(hDc, hBmp);
	BitBlt(hDc, 0, 0, width, height, GetDC(0), x, y, SRCCOPY);
	bool ret = SaveBMPFile(filename, hBmp, hDc, width, height);
	DeleteObject(hBmp);
	return ret;
}
void ScreenShoot::take_screen_shot(std::string path ){
	ClientGeneralInterface::get()->set_maximized();
	Sleep(200);
	if (path != ""){
		CreateScreenshot(LPCWSTR(&path[0]));
		return;
	}

	_SYSTEMTIME Time;
	GetSystemTime(&Time);

	std::string temp = TibiaProcess::get_default()->character_info->get_name_char();
	std::string date = temp;
	date.append(std::to_string((long long)Time.wDay));
	date.append("_");
	date.append(std::to_string((long long)Time.wMonth));
	date.append("_");
	date.append(std::to_string((long long)Time.wYear));
	date.append("---");
	int hour = Time.wHour;
	if (hour < 10)
		date.append("0");

	date.append(std::to_string((long long)hour));
	date.append(".");
	int minute = Time.wMinute;
	if (minute < 10)
		date.append("0");

	date.append(std::to_string((long long)minute));
	date.append(".bmp");
	
	CreateScreenshot(LPCWSTR(&date[0]));
}