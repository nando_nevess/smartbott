#pragma once 
#include "PlayerInfo.h"
#include "WaypointManager.h"

class NaviPlayerWaypointer{
public:
	TimeChronometer time_last_update;

	uint32_t* mtx_access;

	Coordinate current_waypoint_coordinate;
	std::string current_waypoint_label;
	std::string current_waypoint_path;
	waypoint_t current_waypoint_action;

	NaviPlayerWaypointer();
	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value jsonValue);
};

typedef std::shared_ptr<NaviPlayerWaypointer> NaviPlayerWaypointerPtr;
