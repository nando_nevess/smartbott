#pragma once
#include "GeneralManager.h"
#include "DepoterManager.h"
#include "ManagedUtil.h"
#include "Core\ItemsManager.h"
#include "LanguageManager.h"
#include "ControlManager2.h"
#include <msclr\marshal_cppstd.h>
#include "Core\Actions.h"
#include "GifManager.h"
#include "AutoGenerateLoot.h"

namespace Neutral {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Text;
	using namespace msclr::interop;

	public ref class Depot : public Telerik::WinControls::UI::RadForm{
	public:
		Depot(void){
			InitializeComponent();

			this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}
		static void CloseUnique(){
			if (unique)unique->Close();
		}
	protected:
		~Depot(){
			unique = nullptr;
			if (components)
				delete components;
		}

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	protected:
		Telerik::WinControls::UI::RadGroupBox^  radGroupBox4;
		Telerik::WinControls::UI::RadDropDownList^  box_chest;
		Telerik::WinControls::UI::RadMenu^  radMenu1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_save_;
		Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_load_;
		Telerik::WinControls::UI::RadPanel^  radPanel1;

		Telerik::WinControls::UI::RadPageView^  PageDepot;
		System::Windows::Forms::PictureBox^  pictureBoxMainDepotBp;
		System::Windows::Forms::PictureBox^  pictureBoxItemName;
		System::Windows::Forms::PictureBox^  pictureBoxDepotBox;
	public: Telerik::WinControls::UI::RadButton^  radButton1;

	public:
	public:
	public:



	private: System::Windows::Forms::ImageList^  containerImageList;
	private: Telerik::WinControls::UI::RadCheckBox^  check_use_alternative_mode;
	public: Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
	private:

	private:
	private:
	protected:
	protected: static Depot^ unique;

	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;	
				if (unique == nullptr)
					unique = gcnew Depot();

				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew Depot();
	}
	public: static void ReloadForm(){
				unique->Depot_Load(nullptr, nullptr);
	}
			Telerik::WinControls::UI::RadLabel^  radLabel2;
			Telerik::WinControls::UI::RadButton^  bt_add;
			Telerik::WinControls::UI::RadButton^  bt_delete;
			Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
			Telerik::WinControls::UI::RadDropDownList^  box_mainbpdp;
			Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
			Telerik::WinControls::UI::RadDropDownList^  box_itemname;
			Telerik::WinControls::UI::RadSpinEditor^  box_maxqty;
			Telerik::WinControls::UI::RadSpinEditor^  box_minqty;
			Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: System::ComponentModel::IContainer^  components;
	public:


#pragma region Windows Form Designer generated code
			void InitializeComponent(void)
			{
				this->components = (gcnew System::ComponentModel::Container());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem5 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem6 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem7 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem8 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem9 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem10 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem11 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem12 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem13 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem14 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem15 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem16 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem17 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem18 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bt_add = (gcnew Telerik::WinControls::UI::RadButton());
				this->bt_delete = (gcnew Telerik::WinControls::UI::RadButton());
				this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->pictureBoxMainDepotBp = (gcnew System::Windows::Forms::PictureBox());
				this->box_mainbpdp = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->pictureBoxItemName = (gcnew System::Windows::Forms::PictureBox());
				this->box_minqty = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->box_itemname = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->box_maxqty = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->radGroupBox4 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->pictureBoxDepotBox = (gcnew System::Windows::Forms::PictureBox());
				this->box_chest = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
				this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
				this->PageDepot = (gcnew Telerik::WinControls::UI::RadPageView());
				this->containerImageList = (gcnew System::Windows::Forms::ImageList(this->components));
				this->check_use_alternative_mode = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				this->radGroupBox1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxMainDepotBp))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_mainbpdp))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
				this->radGroupBox2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxItemName))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_minqty))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_itemname))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_maxqty))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->BeginInit();
				this->radGroupBox4->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDepotBox))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_chest))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
				this->radPanel1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageDepot))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_use_alternative_mode))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
				this->radGroupBox3->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				this->SuspendLayout();
				// 
				// radLabel2
				// 
				this->radLabel2->Location = System::Drawing::Point(177, 39);
				this->radLabel2->Name = L"radLabel2";
				this->radLabel2->Size = System::Drawing::Size(51, 18);
				this->radLabel2->TabIndex = 6;
				this->radLabel2->Text = L"Max Qty.";
				this->radLabel2->ThemeName = L"ControlDefault";
				// 
				// bt_add
				// 
				this->bt_add->Location = System::Drawing::Point(5, 7);
				this->bt_add->Name = L"bt_add";
				this->bt_add->Size = System::Drawing::Size(132, 24);
				this->bt_add->TabIndex = 9;
				this->bt_add->Text = L"New";
				this->bt_add->ThemeName = L"ControlDefault";
				this->bt_add->Click += gcnew System::EventHandler(this, &Depot::bt_add_Click);
				// 
				// bt_delete
				// 
				this->bt_delete->Location = System::Drawing::Point(148, 7);
				this->bt_delete->Name = L"bt_delete";
				this->bt_delete->Size = System::Drawing::Size(132, 24);
				this->bt_delete->TabIndex = 10;
				this->bt_delete->Text = L"Remove";
				this->bt_delete->ThemeName = L"ControlDefault";
				this->bt_delete->Click += gcnew System::EventHandler(this, &Depot::bt_delete_Click);
				// 
				// radGroupBox1
				// 
				this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox1->Controls->Add(this->pictureBoxMainDepotBp);
				this->radGroupBox1->Controls->Add(this->box_mainbpdp);
				this->radGroupBox1->HeaderText = L"";
				this->radGroupBox1->Location = System::Drawing::Point(308, 84);
				this->radGroupBox1->Name = L"radGroupBox1";
				this->radGroupBox1->Size = System::Drawing::Size(285, 65);
				this->radGroupBox1->TabIndex = 11;
				this->radGroupBox1->ThemeName = L"ControlDefault";
				// 
				// pictureBoxMainDepotBp
				// 
				this->pictureBoxMainDepotBp->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->pictureBoxMainDepotBp->Location = System::Drawing::Point(10, 7);
				this->pictureBoxMainDepotBp->Name = L"pictureBoxMainDepotBp";
				this->pictureBoxMainDepotBp->Size = System::Drawing::Size(52, 52);
				this->pictureBoxMainDepotBp->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				this->pictureBoxMainDepotBp->TabIndex = 8;
				this->pictureBoxMainDepotBp->TabStop = false;
				// 
				// box_mainbpdp
				// 
				this->box_mainbpdp->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				this->box_mainbpdp->Location = System::Drawing::Point(69, 23);
				this->box_mainbpdp->Name = L"box_mainbpdp";
				this->box_mainbpdp->NullText = L"Main Backpack On Depot";
				this->box_mainbpdp->Size = System::Drawing::Size(206, 20);
				this->box_mainbpdp->TabIndex = 6;
				this->box_mainbpdp->ThemeName = L"ControlDefault";
				this->box_mainbpdp->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Depot::box_mainbpdp_SelectedIndexChanged);
				// 
				// radGroupBox2
				// 
				this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox2->Controls->Add(this->pictureBoxItemName);
				this->radGroupBox2->Controls->Add(this->box_minqty);
				this->radGroupBox2->Controls->Add(this->box_itemname);
				this->radGroupBox2->Controls->Add(this->radLabel4);
				this->radGroupBox2->Controls->Add(this->box_maxqty);
				this->radGroupBox2->Controls->Add(this->radLabel2);
				this->radGroupBox2->HeaderText = L"";
				this->radGroupBox2->Location = System::Drawing::Point(308, 152);
				this->radGroupBox2->Name = L"radGroupBox2";
				this->radGroupBox2->Size = System::Drawing::Size(285, 65);
				this->radGroupBox2->TabIndex = 12;
				this->radGroupBox2->ThemeName = L"ControlDefault";
				// 
				// pictureBoxItemName
				// 
				this->pictureBoxItemName->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->pictureBoxItemName->Location = System::Drawing::Point(10, 7);
				this->pictureBoxItemName->Name = L"pictureBoxItemName";
				this->pictureBoxItemName->Size = System::Drawing::Size(52, 52);
				this->pictureBoxItemName->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				this->pictureBoxItemName->TabIndex = 14;
				this->pictureBoxItemName->TabStop = false;
				// 
				// box_minqty
				// 
				this->box_minqty->Location = System::Drawing::Point(124, 38);
				this->box_minqty->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
				this->box_minqty->Name = L"box_minqty";
				this->box_minqty->Size = System::Drawing::Size(51, 20);
				this->box_minqty->TabIndex = 13;
				this->box_minqty->TabStop = false;
				this->box_minqty->ThemeName = L"ControlDefault";
				this->box_minqty->ValueChanged += gcnew System::EventHandler(this, &Depot::box_minqty_ValueChanged);
				// 
				// box_itemname
				// 
				this->box_itemname->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				this->box_itemname->Location = System::Drawing::Point(74, 9);
				this->box_itemname->Name = L"box_itemname";
				this->box_itemname->NullText = L"Item Name";
				this->box_itemname->Size = System::Drawing::Size(205, 20);
				this->box_itemname->TabIndex = 13;
				this->box_itemname->ThemeName = L"ControlDefault";
				this->box_itemname->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Depot::box_itemname_SelectedIndexChanged);
				// 
				// radLabel4
				// 
				this->radLabel4->Location = System::Drawing::Point(75, 39);
				this->radLabel4->Name = L"radLabel4";
				this->radLabel4->Size = System::Drawing::Size(49, 18);
				this->radLabel4->TabIndex = 12;
				this->radLabel4->Text = L"Min Qty.";
				this->radLabel4->ThemeName = L"ControlDefault";
				// 
				// box_maxqty
				// 
				this->box_maxqty->Location = System::Drawing::Point(228, 38);
				this->box_maxqty->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
				this->box_maxqty->Name = L"box_maxqty";
				this->box_maxqty->Size = System::Drawing::Size(51, 20);
				this->box_maxqty->TabIndex = 11;
				this->box_maxqty->TabStop = false;
				this->box_maxqty->ThemeName = L"ControlDefault";
				this->box_maxqty->ValueChanged += gcnew System::EventHandler(this, &Depot::box_maxqty_ValueChanged);
				// 
				// radGroupBox4
				// 
				this->radGroupBox4->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox4->Controls->Add(this->pictureBoxDepotBox);
				this->radGroupBox4->Controls->Add(this->box_chest);
				this->radGroupBox4->HeaderText = L"";
				this->radGroupBox4->Location = System::Drawing::Point(308, 17);
				this->radGroupBox4->Name = L"radGroupBox4";
				this->radGroupBox4->Size = System::Drawing::Size(285, 65);
				this->radGroupBox4->TabIndex = 12;
				this->radGroupBox4->ThemeName = L"ControlDefault";
				// 
				// pictureBoxDepotBox
				// 
				this->pictureBoxDepotBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->pictureBoxDepotBox->Location = System::Drawing::Point(10, 7);
				this->pictureBoxDepotBox->Name = L"pictureBoxDepotBox";
				this->pictureBoxDepotBox->Size = System::Drawing::Size(52, 52);
				this->pictureBoxDepotBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				this->pictureBoxDepotBox->TabIndex = 7;
				this->pictureBoxDepotBox->TabStop = false;
				// 
				// box_chest
				// 
				this->box_chest->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				radListDataItem1->Text = L"Depot Chest";
				radListDataItem2->Text = L"Depot Box I";
				radListDataItem3->Text = L"Depot Box II";
				radListDataItem4->Text = L"Depot Box III";
				radListDataItem5->Text = L"Depot Box IV";
				radListDataItem6->Text = L"Depot Box V";
				radListDataItem7->Text = L"Depot Box VI";
				radListDataItem8->Text = L"Depot Box VII";
				radListDataItem9->Text = L"Depot Box VIII";
				radListDataItem10->Text = L"Depot Box IX";
				radListDataItem11->Text = L"Depot Box X";
				radListDataItem12->Text = L"Depot Box XI";
				radListDataItem13->Text = L"Depot Box XII";
				radListDataItem14->Text = L"Depot Box XIII";
				radListDataItem15->Text = L"Depot Box XIV";
				radListDataItem16->Text = L"Depot Box XV";
				radListDataItem17->Text = L"Depot Box XVI";
				radListDataItem18->Text = L"Depot Box XVII";
				this->box_chest->Items->Add(radListDataItem1);
				this->box_chest->Items->Add(radListDataItem2);
				this->box_chest->Items->Add(radListDataItem3);
				this->box_chest->Items->Add(radListDataItem4);
				this->box_chest->Items->Add(radListDataItem5);
				this->box_chest->Items->Add(radListDataItem6);
				this->box_chest->Items->Add(radListDataItem7);
				this->box_chest->Items->Add(radListDataItem8);
				this->box_chest->Items->Add(radListDataItem9);
				this->box_chest->Items->Add(radListDataItem10);
				this->box_chest->Items->Add(radListDataItem11);
				this->box_chest->Items->Add(radListDataItem12);
				this->box_chest->Items->Add(radListDataItem13);
				this->box_chest->Items->Add(radListDataItem14);
				this->box_chest->Items->Add(radListDataItem15);
				this->box_chest->Items->Add(radListDataItem16);
				this->box_chest->Items->Add(radListDataItem17);
				this->box_chest->Items->Add(radListDataItem18);
				this->box_chest->Location = System::Drawing::Point(69, 22);
				this->box_chest->Name = L"box_chest";
				this->box_chest->NullText = L"Depot Box";
				this->box_chest->Size = System::Drawing::Size(206, 20);
				this->box_chest->TabIndex = 6;
				this->box_chest->ThemeName = L"ControlDefault";
				this->box_chest->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Depot::box_chest_SelectedIndexChanged);
				// 
				// radMenu1
				// 
				this->radMenu1->Cursor = System::Windows::Forms::Cursors::Default;
				this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
				this->radMenu1->Location = System::Drawing::Point(0, 0);
				this->radMenu1->Name = L"radMenu1";
				this->radMenu1->Size = System::Drawing::Size(596, 20);
				this->radMenu1->TabIndex = 13;
				this->radMenu1->Text = L"radMenu1";
				// 
				// radMenuItem1
				// 
				this->radMenuItem1->AccessibleDescription = L"Menu";
				this->radMenuItem1->AccessibleName = L"Menu";
				this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
					this->bt_save_, this->radMenuSeparatorItem1,
						this->bt_load_
				});
				this->radMenuItem1->Name = L"radMenuItem1";
				this->radMenuItem1->Text = L"Menu";
				// 
				// bt_save_
				// 
				this->bt_save_->AccessibleDescription = L"Save";
				this->bt_save_->AccessibleName = L"Save";
				this->bt_save_->Name = L"bt_save_";
				this->bt_save_->Text = L"Save";
				this->bt_save_->Click += gcnew System::EventHandler(this, &Depot::bt_save__Click);
				// 
				// radMenuSeparatorItem1
				// 
				this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
				this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
				this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
				this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
				// 
				// bt_load_
				// 
				this->bt_load_->AccessibleDescription = L"Load";
				this->bt_load_->AccessibleName = L"Load";
				this->bt_load_->Name = L"bt_load_";
				this->bt_load_->Text = L"Load";
				this->bt_load_->Click += gcnew System::EventHandler(this, &Depot::bt_load__Click);
				// 
				// radPanel1
				// 
				this->radPanel1->Controls->Add(this->radMenu1);
				this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
				this->radPanel1->Location = System::Drawing::Point(0, 0);
				this->radPanel1->Name = L"radPanel1";
				this->radPanel1->Size = System::Drawing::Size(596, 19);
				this->radPanel1->TabIndex = 14;
				this->radPanel1->Text = L"radPanel1";
				// 
				// radButton1
				// 
				this->radButton1->Location = System::Drawing::Point(5, 41);
				this->radButton1->Name = L"radButton1";
				this->radButton1->Size = System::Drawing::Size(275, 24);
				this->radButton1->TabIndex = 10;
				this->radButton1->Text = L"Auto Generate Depot List";
				this->radButton1->ThemeName = L"ControlDefault";
				this->radButton1->Click += gcnew System::EventHandler(this, &Depot::radButton1_Click);
				// 
				// PageDepot
				// 
				this->PageDepot->Location = System::Drawing::Point(0, 18);
				this->PageDepot->Name = L"PageDepot";
				this->PageDepot->PageBackColor = System::Drawing::Color::Transparent;
				this->PageDepot->Size = System::Drawing::Size(308, 302);
				this->PageDepot->TabIndex = 0;
				this->PageDepot->Text = L"PageDepot";
				this->PageDepot->ThemeName = L"ControlDefault";
				this->PageDepot->NewPageRequested += gcnew System::EventHandler(this, &Depot::PageDepot_NewPageRequested);
				this->PageDepot->PageRemoved += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEventArgs^ >(this, &Depot::PageDepot_PageRemoved);
				this->PageDepot->SelectedPageChanged += gcnew System::EventHandler(this, &Depot::PageDepot_SelectedPageChanged);
				this->PageDepot->TextChanged += gcnew System::EventHandler(this, &Depot::PageRename_TextChanged);
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageDepot->GetChildAt(0)))->NewItemVisibility = Telerik::WinControls::UI::StripViewNewItemVisibility::End;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageDepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageDepot->GetChildAt(0)))->ItemFitMode = Telerik::WinControls::UI::StripViewItemFitMode::None;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageDepot->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
				// 
				// containerImageList
				// 
				this->containerImageList->ColorDepth = System::Windows::Forms::ColorDepth::Depth8Bit;
				this->containerImageList->ImageSize = System::Drawing::Size(16, 16);
				this->containerImageList->TransparentColor = System::Drawing::Color::Transparent;
				// 
				// check_use_alternative_mode
				// 
				this->check_use_alternative_mode->Location = System::Drawing::Point(5, 72);
				this->check_use_alternative_mode->Name = L"check_use_alternative_mode";
				this->check_use_alternative_mode->Size = System::Drawing::Size(147, 18);
				this->check_use_alternative_mode->TabIndex = 13;
				this->check_use_alternative_mode->Text = L"Use New Style Depot Box";
				this->check_use_alternative_mode->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Depot::check_use_alternative_mode_ToggleStateChanged);
				// 
				// radGroupBox3
				// 
				this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox3->Controls->Add(this->bt_add);
				this->radGroupBox3->Controls->Add(this->check_use_alternative_mode);
				this->radGroupBox3->Controls->Add(this->bt_delete);
				this->radGroupBox3->Controls->Add(this->radButton1);
				this->radGroupBox3->HeaderText = L"";
				this->radGroupBox3->Location = System::Drawing::Point(308, 218);
				this->radGroupBox3->Name = L"radGroupBox3";
				this->radGroupBox3->Size = System::Drawing::Size(285, 95);
				this->radGroupBox3->TabIndex = 15;
				this->radGroupBox3->ThemeName = L"ControlDefault";
				// 
				// Depot
				// 
				this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				this->ClientSize = System::Drawing::Size(596, 316);
				this->Controls->Add(this->radGroupBox4);
				this->Controls->Add(this->radGroupBox3);
				this->Controls->Add(this->radPanel1);
				this->Controls->Add(this->PageDepot);
				this->Controls->Add(this->radGroupBox2);
				this->Controls->Add(this->radGroupBox1);
				this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				this->MaximizeBox = false;
				this->Name = L"Depot";
				// 
				// 
				// 
				this->RootElement->ApplyShapeToControl = true;
				this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				this->Text = L"Depot";
				this->ThemeName = L"ControlDefault";
				this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Depot::Depot_FormClosing);
				this->Load += gcnew System::EventHandler(this, &Depot::Depot_Load);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				this->radGroupBox1->ResumeLayout(false);
				this->radGroupBox1->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxMainDepotBp))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_mainbpdp))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
				this->radGroupBox2->ResumeLayout(false);
				this->radGroupBox2->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxItemName))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_minqty))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_itemname))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_maxqty))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->EndInit();
				this->radGroupBox4->ResumeLayout(false);
				this->radGroupBox4->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDepotBox))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_chest))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
				this->radPanel1->ResumeLayout(false);
				this->radPanel1->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageDepot))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_use_alternative_mode))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
				this->radGroupBox3->ResumeLayout(false);
				this->radGroupBox3->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				this->ResumeLayout(false);

			}
#pragma endregion

			bool disable_item_update;

			String^ text;
			System::Void loadAll();
			System::Void updateSelectedData();

			System::Void waypointPageAddNewPage(std::string PageNameId);
			System::Void addItemOnView(String^ ContainerName, String^ idBp, String^ Item, int maxqty, int minqty);
			System::Void loadItemOnView(std::string id_);

			Telerik::WinControls::UI::RadListView^ getItemListView();
			Telerik::WinControls::UI::RadDropDownList^ getBpCurrentDropDownList();
			System::Void ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
			System::Void PageDepot_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e);
			System::Void PageDepot_NewPageRequested(System::Object^  sender, System::EventArgs^  e);
			System::Void Depot_Load(System::Object^  sender, System::EventArgs^  e);
			System::Void bt_add_Click(System::Object^  sender, System::EventArgs^  e);
			System::Void bt_delete_Click(System::Object^  sender, System::EventArgs^  e);
			System::Void box_namebpSelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e);
			System::Void PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e);
			System::Void ListView1ItemChanged(System::Object^  sender, System::EventArgs^  e);
			System::Void box_itemname_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void box_maxqty_ValueChanged(System::Object^  sender, System::EventArgs^  e);
			System::Void box_mainbpdp_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void box_minqty_ValueChanged(System::Object^  sender, System::EventArgs^  e);
			bool save_script(std::string file_name);
			bool load_script(std::string file_name);

			void close_button(){
				if (PageDepot->Pages->Count <= 1)
					(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageDepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
				else
					(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageDepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
			}

			void add_in_setup(std::string id){
				String^ looter_id = gcnew String(id.c_str());
				String^ container_name = "none";

				std::shared_ptr<ControlInfo> controlInfoRule = ControlManager::get()->getControlInfoRule("DepoterGroup");
				if (!controlInfoRule)
					return;

				String^ label_control_id = gcnew String(controlInfoRule->requestNewById(id + "label").c_str());
				String^ combo_control_id = gcnew String(controlInfoRule->requestNewById(id).c_str());

				auto controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(combo_control_id));
				if (controlRule_){
					controlRule_->set_vectorItem(ItemsManager::get()->get_containers());
					controlRule_->set_property("name", managed_util::fromSS(looter_id));
					controlRule_->set_property("text", managed_util::fromSS(container_name));

					controlRule_->set_property("type", "combobox");
					controlRule_->set_property("sizeW", std::to_string(140));
					controlRule_->set_property("sizeH", std::to_string(20));
					controlRule_->set_property("locationX", std::to_string(10));
					controlRule_->set_property("locationY", std::to_string(60));
				}

				auto controlRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(label_control_id));
				if (controlRule){
					controlRule->set_property("text", managed_util::fromSS(looter_id));
					controlRule->set_property("name", managed_util::fromSS(label_control_id));
					controlRule->set_property("type", "label");
					controlRule->set_property("sizeW", std::to_string(55));
					controlRule->set_property("sizeH", std::to_string(18));
					controlRule->set_property("locationX", std::to_string(10));
					controlRule->set_property("locationY", std::to_string(50));
				}
			}
			System::Void box_chest_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e);
			System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void PageDepot_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
				 auto listview = getItemListView();
				 if (!listview)
					 return;

				 if (listview->Items->Count <= 0)
					 radGroupBox2->Enabled = false;
				 else
					 radGroupBox2->Enabled = true;

				 update_alternative_mode();
	}

			 void update_idiom(){
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 bt_add->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_add->Text))[0]);
				 bt_delete->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete->Text))[0]);
				 radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
				 check_use_alternative_mode->Text = gcnew String(&GET_TR(managed_util::fromSS(check_use_alternative_mode->Text))[0]);
				 bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
				 bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);
				 radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);
				 PageDepot->Text = gcnew String(&GET_TR(managed_util::fromSS(PageDepot->Text))[0]);
			 }

			 void update_alternative_mode(){
				 disable_item_update = true;

				 Telerik::WinControls::UI::RadDropDownList^ box_item = getBpCurrentDropDownList();				
			
				 if (check_use_alternative_mode->Checked){
					 if (!box_item)
						 return;

					 box_item->BeginUpdate();

					 box_item->Items->Clear();

					 box_item->Items->Add("Depot Box I");
					 box_item->Items->Add("Depot Box II");
					 box_item->Items->Add("Depot Box III");
					 box_item->Items->Add("Depot Box IV");
					 box_item->Items->Add("Depot Box V");
					 box_item->Items->Add("Depot Box VI");
					 box_item->Items->Add("Depot Box VII");
					 box_item->Items->Add("Depot Box VIII");
					 box_item->Items->Add("Depot Box IX");
					 box_item->Items->Add("Depot Box X");
					 box_item->Items->Add("Depot Box XI");
					 box_item->Items->Add("Depot Box XII");
					 box_item->Items->Add("Depot Box XIII");
					 box_item->Items->Add("Depot Box XIV");
					 box_item->Items->Add("Depot Box XV");
					 box_item->Items->Add("Depot Box XVI");
					 box_item->Items->Add("Depot Box XVII");

					 box_item->EndUpdate();
				 }
				 else{
					 std::vector<std::string> arrayitems = ItemsManager::get()->get_containers();
						
					 if (!box_item)
						 return;

					 box_item->BeginUpdate();
					 box_item->Items->Clear();

					 for (auto item : arrayitems)
						 box_item->Items->Add(gcnew String(item.c_str()));
					
					 box_item->EndUpdate();
				 }

				 std::shared_ptr<DepotContainer> mapId = DepoterManager::get()->getDepotContainer(managed_util::fromSS(PageDepot->SelectedPage->Text));

				 String^ Box = Box = gcnew String(mapId->get_ContainerName().c_str());
				 if (!check_use_alternative_mode->Checked)
					 box_item->Text = Box;
				 else
					 box_item->SelectedIndex = mapId->get_depot_box_index();


				 disable_item_update = false;
			 }

	private: System::Void check_use_alternative_mode_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 update_alternative_mode();
				 DepoterManager::get()->set_use_old_style(check_use_alternative_mode->IsChecked);

				 if (check_use_alternative_mode->IsChecked){
					 this->radGroupBox2->Location = System::Drawing::Point(308, 50);
					 this->radGroupBox3->Location = System::Drawing::Point(308, 117);
					 radGroupBox1->Visible = false;
					 radGroupBox4->Visible = false;
				 }
				 else{
					 this->radGroupBox3->Location = System::Drawing::Point(308, 219);
					 this->radGroupBox2->Location = System::Drawing::Point(308, 152);
					 radGroupBox1->Visible = true;
					 radGroupBox4->Visible = true;
				 }
	}
	private: System::Void Depot_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (GeneralManager::get()->get_practice_mode()){
					 this->Hide();
					 e->Cancel = true;
					 return;
				 }
	}
};
}
