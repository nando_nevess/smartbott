#pragma once
#include "Core\ItemsManager.h"
#include "Logger.h"
#include <boost\any.hpp>
#include <algorithm>
#include <iostream>
#include "Core\Time.h"
#include "xml.h"

uint8_t m_realSize;
int m_exactSize;
uint8_t m_layers;
uint8_t m_numPatternX;
uint8_t m_numPatternY;
uint8_t m_numPatternZ;
uint8_t m_animationPhases;
Size m_size;
Point m_displacement;

void ThingType::unserialize(uint16_t clientId, ThingCategory category, std::shared_ptr< FileReader> fin){
	m_null = false;
	m_id = clientId;
	m_category = category;

	int count = 0, attr = -1;
	bool done = false;
	for (int i = 0; i < ThingLastAttr; ++i) {
		count++;
		attr = fin->get<uint8_t>();
		if (attr == ThingLastAttr) {
			done = true;
			break;
		}
			if (attr == 16)
				attr = ThingAttrNoMoveAnimation;
			else if (attr > 16)
				attr -= 1;

		switch (attr) {
		case ThingAttrDisplacement: {
		
				m_displacement.x = fin->get<uint16_t>();
				m_displacement.y = fin->get<uint16_t>();
		
			m_attribs.set(attr, true);
			break;
		}
		case ThingAttrLight: { 
			Light light;
			light.intensity = (uint8_t)fin->get<uint16_t>();
			light.color = (uint8_t)fin->get<uint16_t>();
			m_attribs.set(attr, light);
			break;
		}
		case ThingAttrMarket: {
			MarketData market;
			market.category = fin->get<uint16_t>();
			market.tradeAs = fin->get<uint16_t>();
			market.showAs = fin->get<uint16_t>();
			market.name = fin->getString();
			market.restrictVocation = fin->get<uint16_t>();
			market.requiredLevel = fin->get<uint16_t>();
			m_attribs.set(attr, market);
			break;
		}
		case ThingAttrElevation: {
			/*m_elevation =*/ fin->get<uint16_t>();
			//m_attribs.set(attr, m_elevation);
			break;
		}
		case ThingAttrUsable:
		case ThingAttrGround:
		case ThingAttrWritable:
		case ThingAttrWritableOnce:
		case ThingAttrMinimapColor:
		case ThingAttrCloth:
		case ThingAttrLensHelp:
			m_attribs.set(attr, fin->get<uint16_t>());
			break;

		case ThingAttrGroundBorder:
		case ThingAttrOnBottom:
		case ThingAttrOnTop:
		case ThingAttrContainer:
		case ThingAttrStackable:
		case ThingAttrForceUse:
		case ThingAttrMultiUse:
		case ThingAttrFluidContainer:
		case ThingAttrSplash:
		case ThingAttrNotWalkable:
		case ThingAttrNotMoveable:
		case ThingAttrBlockProjectile:
		case ThingAttrNotPathable:
		case ThingAttrPickupable:
		case ThingAttrHangable:
		case ThingAttrHookSouth:
		case ThingAttrHookEast:
		case ThingAttrRotateable:
		case ThingAttrDontHide:
		case ThingAttrTranslucent:
		
		case ThingAttrAnimateAlways:
		case ThingAttrFullGround:
		case ThingAttrLook:
		case ThingAttrNoMoveAnimation:
		// additional
		case ThingAttrOpacity:
		case ThingAttrNotPreWalkable:

		case ThingAttrFloorChange:
		case ThingAttrChargeable:
			m_attribs.set(attr, true);
			break;
		case ThingAttrLyingCorpse:
			m_attribs.set(attr, true);
			break;
		default:
			//throw("error");
			break;
		};
	}

	if (!done)
		throw("corrupt data (id: %d, category: %d, count: %d, lastAttr: %d)");
		

	bool hasFrameGroups = category == ThingCategoryCreature;
	uint8_t groupCount = hasFrameGroups ? fin->get<uint8_t>() : 1;

	for (int i = 0; i < groupCount; ++i) {
		uint8_t frameGroupType = FrameGroupDefault;
		if (hasFrameGroups)
			frameGroupType = fin->get<uint8_t>();

		uint8_t width = fin->get<uint8_t>();
		uint8_t height = fin->get<uint8_t>();
		m_size = Size(width, height);
		if (width > 1 || height > 1) {
			m_realSize = fin->get<uint8_t>();
			m_exactSize = std::min<int>(m_realSize, std::max<int>(width * 32, height * 32));
		}
		else
			m_exactSize = 32;

		m_layers = fin->get<uint8_t>();
		m_numPatternX = fin->get<uint8_t>();
		m_numPatternY = fin->get<uint8_t>();

			m_numPatternZ = fin->get<uint8_t>();
		
		m_animationPhases = fin->get<uint8_t>();

		if (m_animationPhases > 1) {
			//m_animationPhases = animationPhases;
			/*m_async = */fin->get<uint8_t>() /*== 0*/;
			/*m_loopCount = */fin->get<uint32_t>();
			/*m_startPhase = */fin->get<uint8_t>();

			for (int i = 0; i < m_animationPhases; ++i) {
				int minimum = fin->get<uint32_t>();
				int maximum = fin->get<uint32_t>();
				//m_phaseDurations.push_back(std::make_tuple(minimum, maximum));
			}

		//	m_phase = getStartPhase();

			//assert(m_animationPhases == (int)m_phaseDurations.size());
			//assert(m_startPhase >= -1 && m_startPhase < m_animationPhases);

			//m_animator = AnimatorPtr(new Animator);
			//m_animator->unserialize(m_animationPhases, fin);
		}

		int totalSprites = m_size.area() * m_layers * m_numPatternX * m_numPatternY * m_numPatternZ * m_animationPhases;

		if (totalSprites > 4096)
			throw("a thing type has more than 4096 sprites");

		//m_spritesIndex.resize(totalSprites);
		for (int i = 0; i < totalSprites; i++)
			fin->get<uint32_t>();
			//m_spritesIndex[i] = g_game.getFeature(Otc::GameSpritesU32) ? fin->getU32() : fin->getU16();
	}

	/*m_textures.resize(m_animationPhases);
	m_texturesFramesRects.resize(m_animationPhases);
	m_texturesFramesOriginRects.resize(m_animationPhases);
	m_texturesFramesOffsets.resize(m_animationPhases);*/
}
TibiaFileParser::TibiaFileParser(){
	m_thingTypesStringKey = std::shared_ptr<stringmap<ThingTypePtr>>(new stringmap<ThingTypePtr>);
	m_nullThingType = ThingTypePtr(new ThingType);
}

bool TibiaFileParser::LoadDat(std::string file){
	 
	static neutral_mutex mtx_access;
	mtx_access.lock();
	 
	bool m_datLoaded = false;
	 
	uint32_t m_datSignature = 0;
	 
	uint32_t m_contentRevision = 0;
	 
	try {
		 
		std::shared_ptr< FileReader> fin(new FileReader(file));
		 
		m_datSignature = fin->get<uint32_t>();
		 
		m_contentRevision = static_cast<uint16_t>(m_datSignature);
		 

		for (int category = 0; category < ThingLastCategory; ++category) {
			int count = fin->get<uint16_t>() + 1;
			m_thingTypes[category].clear();
			m_thingTypes[category].resize(count, m_nullThingType);
		}
		for (int category = 0; category < ThingLastCategory; ++category) {
			uint16_t firstId = 1;
			if (category == ThingCategoryItem)
				firstId = 100;

			for (uint16_t id = firstId; id < m_thingTypes[category].size(); ++id) {
				ThingTypePtr type(new ThingType);
				type->unserialize(id, (ThingCategory)category, fin);
				m_thingTypes[category][id] = type;
			}
		}
		 
		m_datLoaded = true;
		mtx_access.unlock();
		return true;
	}
	catch (std::exception& e) {
		 
		Logger::get()->add_error_log("ItemManagerErrors", e.what());
		 
		mtx_access.unlock();
		 
		return false;
	}
	 
	mtx_access.unlock();	
	 
	return false;
}

TibiaFileParser* TibiaFileParser::get(){
	static TibiaFileParser* mTibiaFileParser = nullptr;
	if (!mTibiaFileParser)
		mTibiaFileParser = new TibiaFileParser;
	
	return mTibiaFileParser;
}

bool ItemsManager::isRuneFieldItem(uint32_t id){
	auto find_item_id = std::find(items_as_fields_runes.begin(), items_as_fields_runes.end(), id);
	if (find_item_id == items_as_fields_runes.end())
		return false;

	return true;
}

std::shared_ptr<ConfigPathProperty> ItemsManager::get_path_property_by_id(uint32_t id){
	auto myVector = ConfigPathManager::get()->get_vector_path_property();	
	for (auto tile_item : myVector){
		if (tile_item->get_path_id() != id)
			continue;

		return tile_item;
	}
	return nullptr;
}

void ItemsManager::load_custom(){
	items_as_pick.clear();
	items_as_rope.clear();
	items_as_shovel.clear();
	items_as_machete.clear();
	items_as_kitchen.clear();
	items_as_knife.clear();
	items_as_spoon.clear();
	items_as_crowbar.clear();
	items_as_scythe.clear();
	items_as_sickle.clear();
	items_as_furniture.clear();
	items_as_take_skin.clear();
	items_as_food.clear();
	items_as_empty_potions.clear();
	items_as_fields_runes.clear();
	items_as_container.clear();

	//Todo: Revisar Itens ID
	//Pick Itens Id
	{
		items_as_pick.push_back(3456);//Pick
	}
	//Rope Itens Id
	{
		items_as_rope.push_back(3003);//Rope
		items_as_rope.push_back(646);//Elvenhair Rope
	}
	//Shovel Itens Id
	{
		items_as_shovel.push_back(5710);//Light Shovel
		items_as_shovel.push_back(3457);//Shovel
	}
	//Machet Itens Id
	{
		items_as_machete.push_back(3330);//Heavy Machete
		items_as_machete.push_back(3308);//Machete

	}
	//Kitchen Itens Id
	{
		items_as_kitchen.push_back(3464);//Baking Tray
		items_as_kitchen.push_back(2902);//Bowl
		items_as_kitchen.push_back(3474);//Bowl(Pirate, green)
		items_as_kitchen.push_back(3475);//Bowl(Pirate, white)
		items_as_kitchen.push_back(3476);//Bowl(Pirate, yellow)
		items_as_kitchen.push_back(3471);//Cleaver
		items_as_kitchen.push_back(2881);//Cup
		items_as_kitchen.push_back(2883);//Cup(Pirate)
		items_as_kitchen.push_back(3467);//Fork
		items_as_kitchen.push_back(2882);//Jug
		items_as_kitchen.push_back(3479);//Jug(Pirate, Blue)
		items_as_kitchen.push_back(3478);//Jug(Pirate, Gold)
		items_as_kitchen.push_back(3480);//Jug(Pirate, Green)
		items_as_kitchen.push_back(3477);//Jug(Pirate, Silver)
		items_as_kitchen.push_back(3469);//Kitchen Knife
		items_as_kitchen.push_back(2880);//Mug
		items_as_kitchen.push_back(2884);//Mug(Pirate)
		items_as_kitchen.push_back(3472);//Oven Spatula
		items_as_kitchen.push_back(3466);//Pan
		items_as_kitchen.push_back(2902);//Plate
		items_as_kitchen.push_back(3473);//Rolling Pin
		items_as_kitchen.push_back(3468);//Spoon
		items_as_kitchen.push_back(3470);//Wooden Spoon
	}
	//Knife Itens Id
	{
		items_as_knife.push_back(3469);//Kitchen Knife
		items_as_knife.push_back(3291);//Knife
		items_as_knife.push_back(5908);//Obsidian Knife
	}
	//Spoon Itens Id
	{
		items_as_spoon.push_back(3468);//Spoon
		items_as_spoon.push_back(3470);//Wooden Spoon
	}
	//Crowbar Itens Id
	{
		items_as_crowbar.push_back(3304);//Crowbar
	}
	//Scythe Itens Id
	{
		items_as_scythe.push_back(3453);//Scythe
	}
	//Sickle Itens Id
	{
		items_as_sickle.push_back(3293);//Sickle
		items_as_sickle.push_back(3306);//Golden Sickle
	}
	//Furniture Itens Id
	{
		items_as_furniture.push_back(9063);//Crystal Pedestal (Red)
		items_as_furniture.push_back(9064);//Crystal Pedestal (Blue)
		items_as_furniture.push_back(9065);//Crystal Pedestal (Cyan)
		items_as_furniture.push_back(9066);//Crystal Pedestal (Green)
	}
	//TAKE SKIN
	{
		items_as_take_skin.push_back(3483);
		items_as_take_skin.push_back(5908);
		items_as_take_skin.push_back(5942);
	}
	//Food Itens Id
	{
		items_as_food.push_back(17821);//Rat Chesse
		items_as_food.push_back(21146);//glooth Steak 
		items_as_food.push_back(11460);//Aubergine 
		items_as_food.push_back(8020);//Baking Tray (With dough) 
		items_as_food.push_back(3587);//Banana 
		items_as_food.push_back(6574);//Bar of Chocolate 
		items_as_food.push_back(3588);//Blueberry 
		items_as_food.push_back(8017);//Beetroot 
		items_as_food.push_back(3600);//Bread 
		items_as_food.push_back(11461);//Broccoli 
		items_as_food.push_back(3602);//Brown Bread 
		items_as_food.push_back(3725);//Brown Mushroom 
		items_as_food.push_back(8197);//Bulb of Garlic 
		items_as_food.push_back(10328);//Bunch of Ripe Rice 
		items_as_food.push_back(6277);//Cake 
		items_as_food.push_back(6569);//Candy 
		items_as_food.push_back(3599);//Candy Cane 
		items_as_food.push_back(3595);//Carrot 
		items_as_food.push_back(3250);//Carrot of Doom 
		items_as_food.push_back(11462);//Cauliflower 
		items_as_food.push_back(3607);//Cheese 
		items_as_food.push_back(3590);//Cherry 
		items_as_food.push_back(8019);//Chocolate Cake 
		items_as_food.push_back(3589);//Coconut 
		items_as_food.push_back(11584);//Coconut Shrimp Bake 
		items_as_food.push_back(6543);//Coloured Egg (Blue) 
		items_as_food.push_back(6544);//Coloured Egg (Green) 
		items_as_food.push_back(6545);//Coloured Egg (Purple) 
		items_as_food.push_back(6542);//Coloured Egg (Red) 
		items_as_food.push_back(6541);//Coloured Egg (Yellow) 
		items_as_food.push_back(3598);//Cookie 
		items_as_food.push_back(3597);//Corncob 
		items_as_food.push_back(6393);//Creamcake 
		items_as_food.push_back(10219);//Crocodile Steak 
		items_as_food.push_back(8014);//Cucumber 
		items_as_food.push_back(3728);//Dark Mushroom 
		items_as_food.push_back(6278);//Decorated Cake 
		items_as_food.push_back(11587);//Demonic Candy Ball 
		items_as_food.push_back(3583);//Dragon Ham 
		items_as_food.push_back(11682);//Dragonfruit 
		items_as_food.push_back(11681);//Ectoplasmic Sushi 
		items_as_food.push_back(3606);//Egg 
		items_as_food.push_back(3731);//Fire Mushroom 
		items_as_food.push_back(3578);//Fish 
		items_as_food.push_back(6500);//Gingerbreadman 
		items_as_food.push_back(3592);//Grapes 
		items_as_food.push_back(3732);//Green Mushroom 
		items_as_food.push_back(7159);//Green Perch 
		items_as_food.push_back(3582);//Ham 
		items_as_food.push_back(7377);//Ice Cream Cone (Blue-Barbarian) 
		items_as_food.push_back(7375);//Ice Cream Cone (Chilly Cherry) 
		items_as_food.push_back(7372);//Ice Cream Cone (Crispy Chocolate Chips) 
		items_as_food.push_back(7373);//Ice Cream Cone (Venorean Dream) 
		items_as_food.push_back(8016);//Jalape�o Pepper 
		items_as_food.push_back(8013);//Lemon 
		items_as_food.push_back(6276);//Lump of Cake Dough 
		items_as_food.push_back(8018);//Lump of Chocolate Dough 
		items_as_food.push_back(3604);//Lump of Dough 
		items_as_food.push_back(5096);//Mango 
		items_as_food.push_back(901);//Marlin 
		items_as_food.push_back(3577);//Meat 
		items_as_food.push_back(3593);//Melon 
		items_as_food.push_back(3580);//Northern Pike 
		items_as_food.push_back(8015);//Onion 
		items_as_food.push_back(3586);//Orange 
		items_as_food.push_back(3726);//Orange Mushroom 
		items_as_food.push_back(6279);//Party Cake 
		items_as_food.push_back(841);//Peanut 
		items_as_food.push_back(3584);//Pear 
		items_as_food.push_back(11683);//Peas 
		items_as_food.push_back(11459);//Pineapple 
		items_as_food.push_back(8011);//Plum 
		items_as_food.push_back(11586);//Pot of Blackjack 
		items_as_food.push_back(8010);//Potato 
		items_as_food.push_back(3594);//Pumpkin 
		items_as_food.push_back(7158);//Rainbow Trout 
		items_as_food.push_back(8012);//Raspberry 
		items_as_food.push_back(3585);//Red Apple 
		items_as_food.push_back(3724);//Red Mushroom 
		items_as_food.push_back(10329);//Rice Ball 
		items_as_food.push_back(3601);//Roll 
		items_as_food.push_back(3579);//Salmon 
		items_as_food.push_back(3581);//Shrimp 
		items_as_food.push_back(3730);//Some Mushrooms 
		items_as_food.push_back(3729);//Some Mushrooms (Brown) 
		items_as_food.push_back(3591);//Strawberry 
		items_as_food.push_back(11588);//Sweet Mangonaise Elixir 
		items_as_food.push_back(10453);//Terramite Eggs 
		items_as_food.push_back(3596);//Tomato 
		items_as_food.push_back(5678);//Tortoise Egg 
		items_as_food.push_back(6125);//Tortoise Egg from Nargor 
		items_as_food.push_back(6392);//Valentine's Cake 
		items_as_food.push_back(836);//Walnut 
		items_as_food.push_back(3723);//White Mushroom 
		items_as_food.push_back(3727);//Wood Mushrooms 
		items_as_food.push_back(8177);//Yummy Gummy Worm 
		items_as_food.push_back(17457);//FOOD DE DOWNPORT algo la egg
	}
	//Empty Potions Itens Id
	{
		items_as_empty_potions.push_back(284);//Empty Potion Flask (Large) 
		items_as_empty_potions.push_back(283);//Empty Potion Flask (Medium) 
		items_as_empty_potions.push_back(285);//Empty Potion Flask (Small)
	}
	//Runes Field
	{
		items_as_fields_runes.push_back(2118);//Fire Field Rune ( 1 SQM )
		items_as_fields_runes.push_back(2119);//Fire Field Rune ( 1 SQM )
		items_as_fields_runes.push_back(2124);
		items_as_fields_runes.push_back(2123);
		items_as_fields_runes.push_back(2125);
		items_as_fields_runes.push_back(2120);
		items_as_fields_runes.push_back(105);
		items_as_fields_runes.push_back(2121);
		items_as_fields_runes.push_back(2127);
		items_as_fields_runes.push_back(2122);
		items_as_fields_runes.push_back(2126);
		items_as_fields_runes.push_back(2130);
		items_as_fields_runes.push_back(2131);
		items_as_fields_runes.push_back(2132);
		items_as_fields_runes.push_back(2133);
		items_as_fields_runes.push_back(2134);
		items_as_fields_runes.push_back(2135);
		items_as_fields_runes.push_back(2136);
	}
	{
		items_as_container.push_back(18339);
		items_as_container.push_back(22797);
		items_as_container.push_back(22797 + 1);
		items_as_container.push_back(22797 + 2);
		items_as_container.push_back(22797 + 3);
		items_as_container.push_back(22797 + 4);
		items_as_container.push_back(22797 + 5);
		items_as_container.push_back(22797 + 6);
		items_as_container.push_back(22797 + 7);
		items_as_container.push_back(22797 + 8);
		items_as_container.push_back(22797 + 9);
		items_as_container.push_back(22797 + 10);
		items_as_container.push_back(22797 + 11);
		items_as_container.push_back(22797 + 12);
		items_as_container.push_back(22797 + 13);
		items_as_container.push_back(22797 + 14);
		items_as_container.push_back(22797 + 15);
		items_as_container.push_back(22797 + 16);
	}

	addItem(22797 + 0, "Depot Box I", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 1, "Depot Box II", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 2, "Depot Box III", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 3, "Depot Box IV", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 4, "Depot Box V", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 5, "Depot Box VI", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 6, "Depot Box VII", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 7, "Depot Box VIII", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 8, "Depot Box IX", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 9, "Depot Box X", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 10, "Depot Box XI", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 11, "Depot Box XII", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 12, "Depot Box XIII", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 13, "Depot Box XIV", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 14, "Depot Box XV", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 15, "Depot Box XVI", { ThingAttr::ThingAttrContainer });
	addItem(22797 + 16, "Depot Box XVII", { ThingAttr::ThingAttrContainer });

	 
	addItem(8895, "rusty armor (semi rare)");
	addItem(3012, "wolf tooth chain (cyc)");
	addItem(18339, "zaoan chess box");
	addItem(21470, "simple arrow");
	addItem(20356, "fireproof horn");
	addItem(3031, "gold coin"); 
	addItem(3035, "platinum coin");
	addItem(3043, "crystal coin");

	//

	addItem(282, "giant shimmering pearl (brown)");
	addItem(281, "giant shimmering pearl (green)");

	addItem(14753, "dung ball (rare)");
	addItem(14225, "dung ball (temp)");

	addItem(3502, "depot chest", { ThingAttr::ThingAttrContainer });
	addItem(12902, "your inbox", { ThingAttr::ThingAttrContainer });
	addItem(12902, "inbox", { ThingAttr::ThingAttrContainer });
	addItem(3500, "locker", { ThingAttr::ThingAttrContainer });
	addItem(3499, "locker", { ThingAttr::ThingAttrContainer });
	addItem(3498, "locker", { ThingAttr::ThingAttrContainer });
	addItem(3497, "locker", { ThingAttr::ThingAttrContainer });
	addItem(283, "empty potion flask strong");
	addItem(284, "empty potion flask great");
	addItem(285, "empty potion flask");
	addItem(3175, "stone shower rune");
	addItem(21352, "lightest missile rune");

	
	dat_parser->sort();
	dat_parser->updateStringMap();
}
		
bool ItemsManager::load(std::string path){
	dat_parser = TibiaFileParser::get();	
	if (!dat_parser->LoadDat(path))
		return false;
	load_custom();
	return true;
}
		
ItemsManager::ItemsManager(){
	load("data\\additional\\Tibia.dat");
}

std::vector<uint32_t> ItemsManager::get_depot_box_ids(){
	std::vector<uint32_t> retval;

	for (auto item_id : items_as_container)
		retval.push_back(item_id);

	return retval;
}

void TibiaFileParser::sort(){
	static std::function<bool(const ThingTypePtr& a1, const ThingTypePtr& a2)> sort_comparer_function
		= [](const ThingTypePtr& a1, const ThingTypePtr& a2){ return a1->m_id < a2->m_id; };

	for (int i = 0; i < ThingLastCategory; i++){
		std::sort(m_thingTypes[i].begin(), m_thingTypes[i].end(), sort_comparer_function);
	}
	std::sort(m_thingTypesCustom.begin(), m_thingTypesCustom.end(), sort_comparer_function);
	std::ofstream off("iff");
	for (auto it : m_thingTypes[ThingCategory::ThingCategoryItem]){
		off << "\n ID " << it->m_id << it->get_item_name();
	}	
	off.close();
}

void TibiaFileParser::updateStringMap(){
	m_thingTypesStringKey->clear();
	for (auto it : m_thingTypes[ThingCategory::ThingCategoryItem]){
		std::string name = it->get_item_name();

		if (!name.empty())
			m_thingTypesStringKey->set(name, it);
	}
}

class CompareAThingTypePtr{

public:
	bool operator()(const uint32_t in_id, const ThingTypePtr& item_ptr){
		return 1;
	}
	bool operator()(const ThingTypePtr& item_ptr, const uint32_t in_id){
		return in_id > item_ptr->m_id;
	}
};

ThingTypePtr binary_find(std::vector<ThingTypePtr>::iterator& begin,
	std::vector<ThingTypePtr>::iterator& end, uint32_t id){

	uint32_t sz = end - begin;
	if (sz == 0)
		return nullptr;

	uint32_t mid = sz / 2;

	if (sz == 1){
		if (begin->get()->m_id == id)
			return *begin;
		return nullptr;
	}
	else{
		
		auto mid_value = begin + mid;
		uint32_t mid_id = mid_value->get()->m_id;
		if (mid_id == id){
			return *mid_value;
		}
		else if (id > mid_id){
			return binary_find(mid_value, end, id);
		}
		else {
			return binary_find(begin, mid_value, id);
		}
	}
	return nullptr;
}

ThingTypePtr ItemsManager::getItemTypeById(uint32_t id){
	auto& vec = dat_parser->m_thingTypes[ThingCategoryItem];
	return binary_find(vec.begin(), vec.end(), id);
}

ThingTypePtr ItemsManager::getItemTypeByName(std::string name){
	return dat_parser->m_thingTypesStringKey->find(name);
}

std::vector<ThingTypePtr> ItemsManager::getItemsByAttributes(std::vector<ThingAttr> types){
	std::vector<ThingTypePtr> retval;
	auto item_vector = ItemsManager::get()->dat_parser->m_thingTypes[ThingCategoryItem];
	for (auto item : item_vector)
		for (auto type : types){
		uint8_t _type = type;
		if (item->m_attribs.has(_type)){
			item_vector.push_back(item);
			break;
		}
	}
	return retval;
}

std::vector<ThingTypePtr> ItemsManager::getItemsByAttribute(ThingAttr type){
	std::vector<ThingTypePtr> retval;
	auto item_vector = ItemsManager::get()->dat_parser->m_thingTypes[ThingCategoryItem];
	for (auto item : item_vector)
		if (item->m_attribs.has(type))
			retval.push_back(item);
	return retval;
}

bool ItemsManager::isitem_id(uint32_t id){
	return getItemTypeById(id) != 0;
}

int ItemsManager::getItemPrice(uint32_t id){
	auto it = getItemTypeById(id);
	return 0;
}

int ItemsManager::getitem_idFromName(std::string name){
	auto item = getItemTypeByName(name);
	if (item)
		return item->m_id;
	return UINT32_MAX;
}

bool ItemsManager::is_item(uint32_t id){
	auto it = getItemTypeById(id);
	if (it){
		if (it->m_attribs.has(ThingAttrMarket))
		if (!it->m_attribs.get<MarketData>(ThingAttrMarket).name.empty())
			return true;
	}
	return false;
}

std::string ItemsManager::getItemNameFromId(uint32_t id){
	auto it = getItemTypeById(id);
	if (it){
		if (it->m_attribs.has(ThingAttrMarket))
			if (!it->m_attribs.get<MarketData>(ThingAttrMarket).name.empty())
				return it->m_attribs.get<MarketData>(ThingAttrMarket).name;
	}
	return std::to_string(id);
}

void ItemsManager::addItem(int id, std::string name, std::vector<ThingAttr> attributes){
	auto item_type = getItemTypeById(id);
	if (item_type){
		if (item_type->m_attribs.has(ThingAttrMarket)){
			//update
			MarketData market = item_type->m_attribs.get<MarketData>(ThingAttrMarket);
			market.name = name;
			item_type->m_attribs.set(ThingAttrMarket, market);
		}
		else{
			MarketData market;
			market.name = name;
			item_type->m_attribs.set(ThingAttrMarket, market);
		}
	}
	else{
		ThingTypePtr temp(new ThingType);
		temp->m_id = id;

		MarketData market;
		market.name = name;
		temp->m_attribs.set(ThingAttrMarket, market);
		dat_parser->m_thingTypes[ThingCategoryItem].push_back(temp);
	}
}

std::vector<uint32_t> ItemsManager::get_depot_ids(){
	std::vector<uint32_t> ids = { 3497, 3498, 3499, 3500 };
	return ids;
}

std::vector<uint32_t> ItemsManager::get_depot_tiles_ids(){
	std::vector<uint32_t> ids = { 10145 };
	return ids;
}

std::vector<std::string> ItemsManager::get_containers(){
	std::vector<std::string> retval;

	auto items = getItemsByAttribute(ThingAttr::ThingAttrContainer);
	for (auto item : items){
		std::string name = item->get_item_name();
		name = string_util::lower(name);
		
		if (!name.empty()){
			if (name.find("bag") != std::string::npos ||
				name.find("backpack") != std::string::npos ||
				name.find("Depot") != std::string::npos ||
				name.find("chess box") != std::string::npos){
				retval.push_back(name);
			}
		}
	}
		

	return retval;
}

std::vector<std::string> ItemsManager::get_itens(){
	std::vector<std::string> retval;
	auto items = dat_parser->m_thingTypes[ThingCategoryItem];

	for (auto item : items){
		std::string item_in = item->get_item_name();

		std::transform(item_in.begin(), item_in.end(), item_in.begin(), ::tolower);

		if (item_in != "")
			retval.push_back(item_in);		
	}
	return retval;
}

std::vector<std::string> ItemsManager::get_potions(){
	std::vector<std::string> names = 
	{ "Health Potion", "Strong Health Potion", "Great Health Potion",
	"Ultimate Health Potion", "Great Spirit Potion", "Mana Potion",
	"Strong Mana Potion", "Great Mana Potion", "Great Spirit Potion", "Small Health Potion" };
	return names;
}

std::vector<std::string> ItemsManager::get_runes(){
	std::vector<std::string> names = 
	{ "Avalanche Rune", "Destroy Field Rune", "Energy Bomb Rune", 
	"Energy Field Rune", "Energy Wall Rune", "Explosion Rune",
	"Fire Bomb Rune", "Fire Field Rune", "Fire Wall Rune", "Fireball Rune",
	"Great Fireball Rune", "Heavy Magic Missile Rune", "Holy Missile Rune", 
	"Icicle Rune", "Light Magic Missile Rune", "Light Stone Shower Rune", 
	"Lightest Magic Missile Rune", "Lightest Missile Rune", "Magic Wall Rune",
	"Paralyse Rune", "Poison Bomb Rune", "Poison Field Rune",
	"Poison Wall Rune", "Soulfire Rune", "Stalagmite Rune",
	"Stone Shower Rune", "Sudden Death Rune", "Thunderstorm Rune",
	"Ultimate Healing Rune", "Wild Growth Rune" };
	return names;
}

std::vector<std::string> ItemsManager::get_ammunitions(){
	std::vector<std::string> names = 
	{ "Arrow", "Burst Arrow", "Crystalline Arrow", "Earth Arrow",
	"Envenomed Arrow", "Flaming Arrow", "Flash Arrow", "Onyx Arrow",
	"Poison Arrow", "Shiver Arrow", "Simple Arrow", "Sniper Arrow", 
	"Tarsal Arrow", "Bolt", "Drill Bolt", "Infernal Bolt", "Piercing Bolt",
	"Power Bolt", "Prismatic Bolt", "Vortex Bolt", "Assassin Star",
	"Enchanted Spear", "Glooth Spear", "Hunting Spear", "Royal Spear", 
	"Small Stone", "Spear", "Throwing Star", "Throwing Star" };
	return names;
}

ItemsManager* ItemsManager::get(){
	static neutral_mutex mtx_access;
	static ItemsManager* mItemsManager;
	if (!mItemsManager){
		mtx_access.lock();
		if (!mItemsManager){
			mItemsManager = new ItemsManager;
		}
		mtx_access.unlock();
	}
	return mItemsManager;
}

bool ItemsManager::is_furniture_id(uint32_t item_id){
	return std::find(items_as_furniture.begin(), items_as_furniture.end(), item_id) != items_as_furniture.end();
}

bool ItemsManager::is_pick_item(uint32_t item_id){
	return std::find(items_as_pick.begin(), items_as_pick.end(), item_id) != items_as_pick.end();
}

bool ItemsManager::is_rope_item(uint32_t item_id){
	return std::find(items_as_rope.begin(), items_as_rope.end(), item_id) != items_as_rope.end();
}

bool ItemsManager::is_shovel_item(uint32_t item_id){
	return std::find(items_as_shovel.begin(), items_as_shovel.end(), item_id) != items_as_shovel.end();
}

bool ItemsManager::is_machete_item(uint32_t item_id){
	return std::find(items_as_machete.begin(), items_as_machete.end(), item_id) != items_as_machete.end();
}

bool ItemsManager::is_kitchen_item(uint32_t item_id){
	return std::find(items_as_kitchen.begin(), items_as_kitchen.end(), item_id) != items_as_kitchen.end();
}

bool ItemsManager::is_knife_item(uint32_t item_id){
	return std::find(items_as_knife.begin(), items_as_knife.end(), item_id) != items_as_knife.end();
}

bool ItemsManager::is_spoon_item(uint32_t item_id){
	return std::find(items_as_spoon.begin(), items_as_spoon.end(), item_id) != items_as_spoon.end();
}

bool ItemsManager::is_croowbar_item(uint32_t item_id){
	return std::find(items_as_crowbar.begin(), items_as_crowbar.end(), item_id) != items_as_crowbar.end();
}

bool ItemsManager::is_scythe_item(uint32_t item_id){
	return std::find(items_as_scythe.begin(), items_as_scythe.end(), item_id) != items_as_scythe.end();
}

bool ItemsManager::is_sickle_item(uint32_t item_id){
	return std::find(items_as_sickle.begin(), items_as_sickle.end(), item_id) != items_as_sickle.end();
}

bool ItemsManager::is_lying_corpse(uint32_t item_id){
	auto item_type = getItemTypeById(item_id);
	if (!item_type)
		return false;

	return item_type->m_attribs.has(ThingAttrLyingCorpse);
}

attr_item_t CustomIdAttributesManager::attr_item_type_from_string(char* str){
	if (strcmp(str, "avoid_when_targeting") == 0){
		return attr_item_t::attr_item_avoid_when_targeting;
	}
	else if (strcmp(str, "can_broke") == 0){
		return attr_item_t::attr_item_can_broke;
	}
	else if (strcmp(str, "can_move") == 0){
		return attr_item_t::attr_item_can_move;
	}
	else if (strcmp(str, "force_loot_browse") == 0){
		return attr_item_t::attr_item_force_loot_browse;
	}
	else if (strcmp(str, "override_walkable") == 0){
		return attr_item_t::attr_item_override_walkable;
	}
	else if (strcmp(str, "override_unwalkable") == 0){
		return attr_item_t::attr_item_override_unwalkable;
	}
	else if (strcmp(str, "step_down") == 0){
		return attr_item_t::attr_item_step_down;
	}
	else if (strcmp(str, "step_up") == 0){
		return attr_item_t::attr_item_step_up;
	}
	else if (strcmp(str, "use_to_down") == 0){
		return attr_item_t::attr_item_use_to_down;
	}
	else if (strcmp(str, "use_to_teleport") == 0){
		return attr_item_t::attr_item_use_to_teleport;
	}
	else if (strcmp(str, "use_to_up") == 0){
		return attr_item_t::attr_item_use_to_up;
	}
	return attr_item_none;
}

attr_additional_t CustomIdAttributesManager::attr_additional_from_string(char* str){

	if (strcmp(str, "destination_coord") == 0){
		return attr_additional_t::attr_additional_destination_coord;
	}
	else if (strcmp(str, "key") == 0){
		return attr_additional_t::attr_additional_key;
	}
	else if (strcmp(str, "required_coord") == 0){
		return attr_additional_t::attr_additional_required_coord;
	}
	else
		return attr_additional_t::attr_additional_none;
}

void CustomIdAttributesManager::loadFile(){
	std::string file_name = ".\\conf\\defaults\\items_info.xml";
	TiXmlDocument doc;
	bool state = doc.LoadFile(&file_name[0]);
	if (!state){
		MessageBox(0, &("Fail to load file items_info.xml with error :" + std::string(doc.ErrorDesc())
			+ " at " + std::to_string(doc.ErrorRow()) + ":" + std::to_string(doc.ErrorCol())
			)[0]  , "error", MB_OK);
		return;
	}
	
	auto items = doc.FirstChildElement();
	if (items){
		auto item = items->FirstChildElement();
		while (item){
			const char* attr = item->Attribute("id");
			if (!attr)
				continue;
			
			CustomIdDetails* new_item_attr = new CustomIdDetails;

			new_item_attr->id = boost::lexical_cast<uint32_t>(attr);
			attributes_id[new_item_attr->id] = new_item_attr;
			auto attributes_elements = item->FirstChildElement();
			while (attributes_elements){
				const char* key = attributes_elements->Attribute("key");
				if (!key){
					attributes_elements = attributes_elements->NextSiblingElement();
					continue;
				}
			
				attr_item_t type = attr_item_type_from_string((char*)key);
				if (type == attr_item_t::attr_item_none){
					attributes_elements = attributes_elements->NextSiblingElement();
					continue;
				}
				new_item_attr->add_attribute(type);
				TiXmlAttribute* attribute = attributes_elements->FirstAttribute();
				while (attribute){
					const char* attr_name = attribute->Name();
					if (!attr_name){
						attribute = attribute->Next();
						continue;
					}
					
					attr_additional_t key_t = attr_additional_from_string((char*)attr_name);
					if (key_t != attr_additional_t::attr_additional_none){
						new_item_attr->add_attribute_optional(type, key_t, attribute->Value());
					}
					attribute = attribute->Next();
				}
				attributes_elements = attributes_elements->NextSiblingElement();
			}

			item = item->NextSiblingElement();
		}
	}
}

void CustomIdDetails::add_attribute(attr_item_t attr){
	get_attribute(attr, true);
}

void CustomIdDetails::add_attribute_optional(attr_item_t type, attr_additional_t additional, std::string value){
	auto attr_class = get_attribute(type, true);
	attr_class->set_additional(additional, value);
}

CustomIdAttributes* CustomIdDetails::get_attribute(attr_item_t type, bool create){
	for (auto it = attributes.begin(); it != attributes.end(); it++){
		if ((*it)->get_type() == type){
			return *it;
		}
	}
	if (create){
		CustomIdAttributes* new_attr = new CustomIdAttributes(type);
		attributes.push_back(new_attr);
		return new_attr;
	}
	return nullptr;
}

bool CustomIdDetails::contains_attribute(attr_item_t attr){
	return get_attribute(attr) != 0;
}

/*
<Item>
<Name>Depot Chest</Name>
<Id>3502</Id>
<Type>Containers</Type>
</Item>
*/
