#pragma once
#include "SpellInfo.h"

uint32_t SpellBase::get_min_hp(){
	return min_hp;
}

void SpellBase::set_min_hp(uint32_t value){
	min_hp = value;
}

void SpellBase::set_max_hp(uint32_t value){
	max_hp = value;
}

uint32_t SpellBase::get_min_mp(){
	return min_mp;
}

uint32_t SpellBase::get_max_hp(){
	return max_hp;
}

void SpellBase::set_min_mp(uint32_t value){
	min_mp = value;
}

uint32_t SpellBase::get_max_mp(){
	return max_mp;
}

void SpellBase::set_no_use_with_loot(bool value){
	no_use_with_loot = value;
}

void SpellBase::set_use_percent_mp(bool value){
	use_percent_mp = value;
}

void SpellBase::set_max_mp(uint32_t value){
	max_mp = value;
}

uint32_t SpellBase::get_order(){
	return order;
}

void SpellBase::set_order(uint32_t value){
	order = value;
}

std::string SpellBase::get_visual_name(){
	return visual_name;
}

void SpellBase::set_visual_name(std::string value){
	visual_name = value;
}

uint32_t SpellBase::get_required_value_point(){
	return required_value_point;
}

void SpellBase::set_required_value_point(uint32_t value){
	required_value_point = value;
}

bool SpellBase::get_use_percent_mp(){
	return use_percent_mp;
}

spell_type_t SpellBase::get_spell_type(){
	return spell_type;
}

void SpellBase::set_spell_type(spell_type_t value){
	spell_type = value;
}

bool SpellBase::get_state(){
	return state;
}

void SpellBase::set_state(bool value){
	state = value;
}

bool SpellBase::get_no_use_with_hunt(){
	return no_use_with_hunt;
}

void SpellBase::set_no_use_with_hunt(bool value){
	no_use_with_hunt = value;
}

bool SpellBase::get_checked(){
	return checked;
}

void SpellBase::set_checked(bool value){
	checked = value;
}

bool SpellBase::get_no_use_with_loot(){
	return no_use_with_loot;
}

void SpellBase::set_use_percent_hp(bool value){
	use_percent_hp = value;
}

bool SpellBase::get_use_percent_hp(){
	return use_percent_hp;
}

uint32_t SpellBase::get_item_id(){
	return item_id;
}

void SpellBase::set_item_id(uint32_t value){
	item_id = value;
}


SpellHealth::SpellHealth() {
}

uint32_t SpellBase::add_vector_condition(std::string value, spells_conditions_t type, spell_actions_t type_action){
	vector_condition.push_back(std::shared_ptr<SpellCondition>(new SpellCondition(type, type_action, value)));
	return vector_condition.size() - 1;
}
std::shared_ptr<SpellCondition> SpellBase::get_vector_condition_rule(uint32_t index){
	return vector_condition[index];
}

void SpellBase::set_cooldown(int newcooldown){
	cooldown = newcooldown;
}

int SpellBase::get_cooldown(){
	if (cooldown == 0)
		return 0;

	return cooldown;
}
SpellCondition::SpellCondition(spells_conditions_t type, spell_actions_t type_action, std::string value){
	condition_type = type;
	action_type = type_action;
	set_value(value);
}

void SpellCondition::set_value(std::string val){
	str_value = val;
	int_value = 0;
	try{
		int_value = boost::lexical_cast<uint32_t>(str_value);
	}
	catch (...){}
}

SpellAttack::SpellAttack(bool checked, spell_type_t spell_type, int spell_condition, int order, std::string visual_cast_value, int min_hp,
	int min_mp, int cooldown, int maximum_sqm, int monster_in_area_min, int monster_in_area_max,
	int monster_qty_list_min, int monster_qty_list_max){
	set_spell_type(spell_type);
	set_spell_condition(spell_condition);
	set_order(order);
	set_checked(checked);
	set_visual_cast_value(visual_cast_value);
	set_min_hp(min_hp);
	set_min_mp(min_mp);
	set_cooldown(cooldown);
	set_maximum_sqm(maximum_sqm);
	set_monster_in_area_min(monster_in_area_min);
	set_monster_in_area_max(monster_in_area_max);
	set_monster_qty_list_min(monster_qty_list_min);
	set_monster_qty_list_max(monster_qty_list_max);
}

int SpellAttack::request_new_id(){
	int id = 0;
	while (monsterList.find(id) != monsterList.end())
		id++;

	monsterList[id] = std::shared_ptr<MonsterInfo>(new MonsterInfo);
	monsterList[id]->set_max_hp(100);
	monsterList[id]->set_min_hp(0);
	monsterList[id]->set_id(id);
	monsterList[id]->set_name("new monster");
	return id;
}

std::shared_ptr<MonsterInfo> SpellAttack::get_monster_rule(int key){
	auto it = monsterList.find(key);
	if (it == monsterList.end())
		return nullptr;

	return monsterList[key];
}

bool SpellAttack::remove_id(int key){
	auto i = monsterList.find(key);
	if (i == monsterList.end())
		return false;

	monsterList.erase(i);
	return true;
}

std::string SpellCondition::get_value(){
	return str_value;
}

uint32_t SpellCondition::get_integer_value(){
	return int_value;
}

SpellHealth::SpellHealth(bool checked, uint32_t health_type, uint32_t order, uint32_t min_hp,
	uint32_t max_hp, uint32_t min_mp, uint32_t max_mp, std::string visual_cast_value, uint32_t cooldown) {
	set_health_type(health_type);
	set_order(order); 
	set_min_hp(min_hp);
	set_max_hp(max_hp);
	set_min_mp(min_mp);
	set_max_mp(max_hp);
	set_visual_name(visual_cast_value);
	set_checked(checked);
	set_cooldown(cooldown);
}

Json::Value SpellHealth::parse_class_to_json(){
	Json::Value spellInfo;

	spellInfo["base"] = SpellBase::parse_class_to_json();
	spellInfo["health_type"] = health_type;
	return spellInfo;
}

void SpellHealth::parse_json_to_class(Json::Value spellInfo){
	if (!spellInfo["healthType"].empty() || !spellInfo["healthType"].isNull())
		health_type = spellInfo["healthType"].asInt();
	else if (!spellInfo["health_type"].empty() || !spellInfo["health_type"].isNull())
		health_type = spellInfo["health_type"].asInt();

	if (!spellInfo["base"].empty() || !spellInfo["base"].isNull())
		SpellBase::parse_json_to_class(spellInfo["base"]);
}

Json::Value SpellAttack::parse_class_to_json(){
	Json::Value spellInfo;
	spellInfo["base"] = SpellBase::parse_class_to_json();
	if (!spellInfo["maximumSqm"].empty() || !spellInfo["maximumSqm"].isNull())
		spellInfo["maximumSqm"] = get_maximum_sqm();
	else
		spellInfo["maximum_sqm"] = get_maximum_sqm();

	if (!spellInfo["monsterInAreaMax"].empty() || !spellInfo["monsterInAreaMax"].isNull())
		spellInfo["monsterInAreaMax"] = get_maximum_sqm();
	else
		spellInfo["monster_in_area_max"] = get_monster_in_area_max();

	if (!spellInfo["monsterInAreaMin"].empty() || !spellInfo["monsterInAreaMin"].isNull())
		spellInfo["monsterInAreaMin"] = get_maximum_sqm();
	else
		spellInfo["monster_in_area_min"] = get_monster_in_area_min();

	if (!spellInfo["monsterQtyListMax"].empty() || !spellInfo["monsterQtyListMax"].isNull())
		spellInfo["monsterQtyListMax"] = get_maximum_sqm();
	else
		spellInfo["monster_qty_list_max"] = get_monster_qty_list_max();

	if (!spellInfo["monsterQtyListMin"].empty() || !spellInfo["monsterQtyListMin"].isNull())
		spellInfo["monsterQtyListMin"] = get_maximum_sqm();
	else
		spellInfo["monster_qty_list_min"] = get_monster_qty_list_min();

	if (!spellInfo["spellCondition"].empty() || !spellInfo["spellCondition"].isNull())
		spellInfo["spellCondition"] = get_maximum_sqm();
	else
		spellInfo["spell_condition"] = get_spell_condition();

	if (!spellInfo["anyMonster"].empty() || !spellInfo["anyMonster"].isNull())
		spellInfo["anyMonster"] = get_maximum_sqm();
	else
		spellInfo["any_monster"] = get_any_monster(); 


	Json::Value& creatures = spellInfo["creatures"];
	for (auto creature : monsterList){
		Json::Value json_creature;

		json_creature["max_hp"] = creature.second->get_max_hp();
		json_creature["min_hp"] = creature.second->get_min_hp();

		json_creature["name"] = creature.second->get_name();

		creatures[std::to_string(creature.first)] = json_creature;
	}
	return spellInfo;
}


void SpellAttack::parse_json_to_class(Json::Value spellInfo){
	if (!spellInfo["base"].empty() || !spellInfo["base"].isNull())
	SpellBase::parse_json_to_class(spellInfo["base"]);

	if (!spellInfo["maximumSqm"].empty() || !spellInfo["maximumSqm"].isNull())
		set_maximum_sqm(spellInfo["maximumSqm"].asInt());
	else if (!spellInfo["maximum_sqm"].empty() || !spellInfo["maximum_sqm"].isNull())
		set_maximum_sqm(spellInfo["maximum_sqm"].asInt());

	if (!spellInfo["monsterInAreaMax"].empty() || !spellInfo["monsterInAreaMax"].isNull())
		set_monster_in_area_max(spellInfo["monsterInAreaMax"].asInt());
	else if (!spellInfo["monster_in_area_max"].empty() || !spellInfo["monster_in_area_max"].isNull())
		set_monster_in_area_max(spellInfo["monster_in_area_max"].asInt());

	if (!spellInfo["monsterInAreaMin"].empty() || !spellInfo["monsterInAreaMin"].isNull())
		set_monster_in_area_min(spellInfo["monsterInAreaMin"].asInt());
	else if (!spellInfo["monster_in_area_min"].empty() || !spellInfo["monster_in_area_min"].isNull())
		set_monster_in_area_min(spellInfo["monster_in_area_min"].asInt());

	if (!spellInfo["monsterQtyListMax"].empty() || !spellInfo["monsterQtyListMax"].isNull())
		set_monster_qty_list_max(spellInfo["monsterQtyListMax"].asInt());
	else if (!spellInfo["monster_qty_list_max"].empty() || !spellInfo["monster_qty_list_max"].isNull())
		set_monster_qty_list_max(spellInfo["monster_qty_list_max"].asInt());

	if (!spellInfo["monsterQtyListMin"].empty() || !spellInfo["monsterQtyListMin"].isNull())
		set_monster_qty_list_min(spellInfo["monsterQtyListMin"].asInt());
	else if (!spellInfo["monster_qty_list_min"].empty() || !spellInfo["monster_qty_list_min"].isNull())
		set_monster_qty_list_min(spellInfo["monster_qty_list_min"].asInt());

	if (!spellInfo["anyMonster"].empty() || !spellInfo["anyMonster"].isNull())
		set_any_monster(spellInfo["anyMonster"].asBool());
	else if (!spellInfo["any_monster"].empty() || !spellInfo["any_monster"].isNull())
		set_any_monster(spellInfo["any_monster"].asBool());
	
	if (!spellInfo["spellCondition"].empty() || !spellInfo["spellCondition"].isNull())
		set_spell_condition(spellInfo["spellCondition"].asInt());
	else if (!spellInfo["spell_condition"].empty() || !spellInfo["spell_condition"].isNull())
		set_spell_condition(spellInfo["spell_condition"].asInt());

	if (!spellInfo["creatures"].empty() || !spellInfo["creatures"].isNull()){
		Json::Value& json_creatures = spellInfo["creatures"];
		Json::Value::Members members = json_creatures.getMemberNames();
		for (std::string member : members){
			Json::Value json_creature = json_creatures[member];
			std::shared_ptr<MonsterInfo> monster(new MonsterInfo);

			if (!json_creature["max_hp"].empty() || !json_creature["max_hp"].isNull())
				monster->set_max_hp(json_creature["max_hp"].asInt());
			else
				monster->set_max_hp(100);

			if (!json_creature["min_hp"].empty() || !json_creature["min_hp"].isNull())
				monster->set_min_hp(json_creature["min_hp"].asInt());
			else
				monster->set_min_hp(0);

			monster->set_name(json_creature["name"].asString());
			monsterList[atoi(&member[0])] = monster;
		}
	}
}


void SpellAttack::add_monster_in_list_by_index(int index, std::shared_ptr<MonsterInfo> monsterName) {
	monsterList[index] = monsterName;
}

bool SpellAttack::contains_monster_in_list(std::string& monsterName, int creature_hp) {
	for (auto monster : monsterList) { 
		if (!monster.second)
			continue;

		if (monster.second->get_min_hp() > creature_hp || monster.second->get_max_hp() < creature_hp)
			continue;

		if (string_util::string_compare(monster.second->get_name(), monsterName, true))
			return true;
	}
	return false;
}

std::map<uint32_t, std::shared_ptr<MonsterInfo>>& SpellAttack::get_monster_list() {
	return monsterList;
}

void SpellBase::set_visual_cast_value(std::string& value){
	visual_cast_value = value;
	//check if string is a number
	try{
		std::string trimmed_value = string_util::trim(value);
		item_id = boost::lexical_cast<uint32_t>(trimmed_value);
		return;
	}
	catch (...){
	}
	
	uint32_t searchitem_id = ItemsManager::get()->getitem_idFromName(visual_cast_value);
	if (searchitem_id != UINT32_MAX)
		item_id = searchitem_id;
	else
		item_id = 0;
}

std::string SpellBase::get_visual_cast_value(){
	return visual_cast_value;
}

Json::Value SpellBase::parse_class_to_json(){
	Json::Value spellInfo;

	if (!this)
		return spellInfo;

	spellInfo["cooldown"] = get_cooldown();
	spellInfo["n_visual_name"] = get_visual_name();
	spellInfo["checked"] = get_checked();
	spellInfo["item_id"] = get_item_id();
	spellInfo["max_hp"] = get_max_hp();
	spellInfo["max_mp"] = get_max_mp();
	spellInfo["min_hp"] = get_min_hp();
	spellInfo["min_mp"] = get_min_mp();
	spellInfo["order"] = get_order();
	spellInfo["get_no_use_with_loot"] = get_no_use_with_loot();
	spellInfo["get_no_use_with_hunt"] = get_no_use_with_hunt();
	spellInfo["use_percent_hp"] = get_use_percent_hp();
	spellInfo["use_percent_mp"] = get_use_percent_mp();
	spellInfo["n_visual_cast_value"] = get_visual_cast_value();
	spellInfo["spell_type"] = get_spell_type();

	Json::Value vector_conditions_json;
	for (auto repot : vector_condition){
		Json::Value monsterListItem;

		monsterListItem["action_type"] = (int)repot->action_type;
		monsterListItem["condition_type"] = (int)repot->condition_type;
		monsterListItem["str_value"] = repot->str_value;

		vector_conditions_json.append(monsterListItem);
	}

	spellInfo["vector_conditions_json"] = vector_conditions_json;
	return spellInfo;
}

void SpellBase::parse_json_to_class(Json::Value spellInfo){
	vector_condition.clear();

	if (!spellInfo["vector_conditions_json"].isNull() && !spellInfo["vector_conditions_json"].empty()){
		Json::Value vector_conditions_json = spellInfo["vector_conditions_json"];

		for (auto member : vector_conditions_json){
			spell_actions_t action_type_temp = (spell_actions_t)0;
			if (!member["action_type"].empty() || !member["action_type"].isNull())
				action_type_temp = (spell_actions_t)member["action_type"].asInt();

			spells_conditions_t condition_type_temp = (spells_conditions_t)0;
			if (!member["condition_type"].empty() || !member["condition_type"].isNull())
				condition_type_temp = (spells_conditions_t)member["condition_type"].asInt();

			std::string str_temp = "";
			if (!member["str_value"].empty() || !member["str_value"].isNull())
				str_temp = member["str_value"].asString();

			std::shared_ptr<SpellCondition> newItem = std::shared_ptr<SpellCondition>(new SpellCondition(condition_type_temp,action_type_temp, str_temp));

			vector_condition.push_back(newItem);
		}
	}

	if (!spellInfo["cooldown"].empty() || !spellInfo["cooldown"].isNull())
		set_cooldown(spellInfo["cooldown"].asInt());
	
	if (!spellInfo["get_no_use_with_loot"].empty() || !spellInfo["get_no_use_with_loot"].isNull())
		set_no_use_with_loot(spellInfo["get_no_use_with_loot"].asBool());

	if (!spellInfo["get_no_use_with_hunt"].empty() || !spellInfo["get_no_use_with_hunt"].isNull())
		set_no_use_with_hunt(spellInfo["get_no_use_with_hunt"].asBool());

	if (!spellInfo["visual_cast_value"].empty() || !spellInfo["visual_cast_value"].isNull())
		set_visual_name((std::string&)spellInfo["visual_cast_value"].asString());
	else if (!spellInfo["n_visual_name"].empty() || !spellInfo["n_visual_name"].isNull())
		set_visual_name((std::string&)spellInfo["n_visual_name"].asString());
	
	if (!spellInfo["visualCastValue"].empty() || !spellInfo["visualCastValue"].isNull())
		set_visual_cast_value((std::string&)spellInfo["visualCastValue"].asString());
	else if (!spellInfo["n_visual_cast_value"].empty() || !spellInfo["n_visual_cast_value"].isNull())
		set_visual_cast_value((std::string&)spellInfo["n_visual_cast_value"].asString()); 

	if (!spellInfo["itemId"].empty() || !spellInfo["itemId"].isNull())
		set_item_id(spellInfo["itemId"].asInt());
	else if (!spellInfo["item_id"].empty() || !spellInfo["item_id"].isNull())
		set_item_id(spellInfo["item_id"].asInt());


	if (!spellInfo["maxHp"].empty() || !spellInfo["maxHp"].isNull())
		set_max_hp(spellInfo["maxHp"].asInt());
	else if (!spellInfo["max_hp"].empty() || !spellInfo["max_hp"].isNull())
		set_max_hp(spellInfo["max_hp"].asInt());


	if (!spellInfo["minHp"].empty() || !spellInfo["minHp"].isNull())
		set_min_hp(spellInfo["minHp"].asInt());
	else if (!spellInfo["min_hp"].empty() || !spellInfo["min_hp"].isNull())
		set_min_hp(spellInfo["min_hp"].asInt());
	
	if (!spellInfo["checked"].empty() || !spellInfo["checked"].isNull())
		set_checked(spellInfo["checked"].asBool());

	if (!spellInfo["minMp"].empty() || !spellInfo["minMp"].isNull())
		set_min_mp(spellInfo["minMp"].asInt());
	else if (!spellInfo["min_mp"].empty() || !spellInfo["min_mp"].isNull())
		set_min_mp(spellInfo["min_mp"].asInt());

	if (!spellInfo["maxMp"].empty() || !spellInfo["maxMp"].isNull())
		set_max_mp(spellInfo["maxMp"].asInt());
	else if (!spellInfo["max_mp"].empty() || !spellInfo["max_mp"].isNull())
		set_max_mp(spellInfo["max_mp"].asInt());


	if (!spellInfo["order"].empty() || !spellInfo["order"].isNull())
		set_order(spellInfo["order"].asInt());

	if (!spellInfo["usePercentHp"].empty() || !spellInfo["usePercentHp"].isNull())
		set_use_percent_hp(spellInfo["usePercentHp"].asBool());
	else if (!spellInfo["use_percent_hp"].empty() || !spellInfo["use_percent_hp"].isNull())
		set_use_percent_hp(spellInfo["use_percent_hp"].asBool());
	
	if (!spellInfo["usePercentMp"].empty() || !spellInfo["usePercentMp"].isNull())
		set_use_percent_mp(spellInfo["usePercentMp"].asBool());
	else if (!spellInfo["use_percent_mp"].empty() || !spellInfo["use_percent_mp"].isNull())
		set_use_percent_mp(spellInfo["use_percent_mp"].asBool());


	if (!spellInfo["spellType"].empty() || !spellInfo["spellType"].isNull())
		set_spell_type((spell_type_t)spellInfo["spellType"].asInt());
	else if (!spellInfo["spell_type"].empty() || !spellInfo["spell_type"].isNull())
		set_spell_type((spell_type_t)spellInfo["spell_type"].asInt());
}
