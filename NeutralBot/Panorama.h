#pragma once
#include "Tools.h"
#include "HUDManager.h"
#include "ManagedUtil.h"
#include "ConfigPath.h"
#include "Core\ItemsManager.h"
#include "Sell.h"
#include "ItemsInfo.h"
#include "Depot.h"
#include "Core\Hotkey.h"
#include "SmartBot Console.h"
#include "Alerts.h"
#include "Looter.h"
#include "Hunter.h" 
#include "Repot.h"
#include "PauseForm.h"
#include "Waypoint.h"
#include "Spells.h"
#include "Settings.h"
#include "Setup.h"
#include "Core\Util.h"
#include "Core\Login.h"
#include "Core\Neutral.h"
#include "ManagedUtil.h" 
#include "AdvancedRepoter.h"
#include "LuaBackground.h"
#include "Core\Process.h"
#include "MapWall.h"
#include "LooterStatistics.h"
#include "hudform.h"
#include "DevelopmentForm.h"
#include "HotkeyManager.h"
#include "FormEditor.h"
#include "SpecialAreaEditor.h"
#include "General Settings.h"

namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Timers;
	using namespace System::Threading;

	public ref class Panorama : public Telerik::WinControls::UI::RadForm{

	public:	Panorama(){
				load_general_config();
				NeutralManager::get();
				this->Hide();

				InitializeComponent();
				loadtheme(gcnew String(GeneralManager::get()->get_themaName().c_str()));
				LanguageManager::get()->set_current_lang(GeneralManager::get()->get_currentLang());
				
				update_idiom();

				if (GeneralManager::get()->get_practice_mode())
					openAllOtherForms();

				//menu_client_Click(nullptr, nullptr);

				notifyIcon->Icon = gcnew System::Drawing::Icon("img\\smartboticon.ico");
				this->Icon = gcnew System::Drawing::Icon("img\\smartboticon.ico");

				LoadThemes();
				load_recent_scripts();
				disable_and_enable_buttons();

				radToggleButtonGeneral->Text = "General";
				radToggleButtonSpellcaster->Text = "Spellcaster";
				radToggleButtonHunter->Text = "Hunter";
				radToggleButtonLooter->Text = "Looter";
				radToggleButtonAlerts->Text = "Alerts";
				radToggleButtonLua->Text = "Lua";
				radToggleButtonWaypointer->Text = "Waypointer";
				radToggleButtonSpellHeal->Text = "Spell:Heal";
				radToggleButtonSpellAttack->Text = "Spell:Attack";
				NeutralBot::FastUIPanelController::get()->install_controller(this);
				PauseForm::ShowUnique();

				if (GeneralManager::get()->get_dev_mode()){
					radMenuItem6->Visibility = Telerik::WinControls::ElementVisibility::Visible;

					/*AllocConsole();
					FILE *pFile;
					freopen_s(&pFile, "CONIN$", "r", stdin);
					freopen_s(&pFile, "CONOUT$", "w", stdout);
					freopen_s(&pFile, "CONOUT$", "w", stderr);*/
				}
	}
	protected: ~Panorama()	{
				   unique = nullptr;
				   if (components)
					   delete components;
	}

			   void disable_and_enable_buttons(){
				   managed_util::setToggleCheckButtonStyle(radToggleButtonSpellcaster);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonHunter);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonLooter);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonAlerts);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonLua);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonWaypointer);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonSpellHeal);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonSpellAttack);
				   managed_util::setToggleCheckButtonStyle(radToggleButtonGeneral);
			   }

			   void AddTheme(String^ themeString){
				   Telerik::WinControls::UI::RadMenuHeaderItem^  radMenuItem = gcnew Telerik::WinControls::UI::RadMenuHeaderItem;
				   radMenuItem->AccessibleDescription = themeString;
				   radMenuItem->AccessibleName = themeString;
				   radMenuItem->Name = themeString;
				   radMenuItem->Text = themeString;
				   radMenuItem->Click += gcnew System::EventHandler(this, &Panorama::radMenuHeaderItem1_Click);
				   this->radMenuThemes->Items->Add(radMenuItem);
			   }

			   void LoadThemes(){
				   AddTheme("Office2013LightTheme");
				   AddTheme("Office2013DarkTheme");
				   AddTheme("Office2010SilverTheme");
				   AddTheme("Office2010BlackTheme");
				   AddTheme("Office2007SilverTheme");
				   AddTheme("Office2007BlackTheme");
				   AddTheme("HighContrastBlackTheme");
				   AddTheme("BreezeTheme");
				   AddTheme("DesertTheme");
				   AddTheme("AquaTheme");
				   AddTheme("Windows8Theme");
				   AddTheme("Windows7Theme");
				   AddTheme("VisualStudio2012LightTheme");
				   AddTheme("VisualStudio2012DarkTheme");
				   AddTheme("TelerikMetroTouchTheme");
				   AddTheme("TelerikMetroBlueTheme");
				   AddTheme("TelerikMetroTheme");

			   }

	public:
		Telerik::WinControls::UI::RadPanorama^  radPanorama1;
		Telerik::WinControls::UI::RadTileElement^  radTileDepoter;
		Telerik::WinControls::UI::RadTileElement^  radTileAlerts;
		Telerik::WinControls::UI::RadTileElement^  radTileLooter;
		Telerik::WinControls::UI::RadTileElement^  radTileHunter;
		Telerik::WinControls::UI::RadTileElement^  radTileRepoter;
		Telerik::WinControls::UI::RadTileElement^  radTileContainerRepoter;
		Telerik::WinControls::UI::RadTileElement^  radTileWaypointer;
		Telerik::WinControls::UI::RadTileElement^  radTileSpellcaster;
		Telerik::WinControls::UI::RadTileElement^  radTileElement2;
		Telerik::WinControls::UI::RadTileElement^  radTileLuaBackground;

	protected:
		Telerik::WinControls::UI::TileGroupElement^  panoramaGroupSetup;
		Telerik::WinControls::UI::RadTileElement^  radTileElement1;
		Telerik::WinControls::UI::RadPanorama^  radPanorama2;
		System::Windows::Forms::NotifyIcon^  notifyIcon;
		System::Windows::Forms::ContextMenuStrip^  contextMenuStrip1;
		System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem1;
		System::Windows::Forms::ToolStripSeparator^  toolStripSeparator1;
		System::Windows::Forms::ToolStripMenuItem^  toolStripMenuItem2;
		Telerik::WinControls::UI::RadMenuItem^  bt_save;
		Telerik::WinControls::UI::RadMenuItem^  bt_load;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem2;
		Telerik::WinControls::UI::RadMenu^  radMenu2;
		Telerik::WinControls::UI::RadLabel^  pingLabel;
		System::Windows::Forms::Timer^  pingUpdater;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_save_;
		Telerik::WinControls::UI::RadMenuItem^  bt_load_;
		Telerik::WinControls::UI::RadMenuItem^  radMenuThemes;

		Telerik::WinControls::UI::RadPanel^  radPanel1;
		Telerik::WinControls::UI::RadPanel^  radPanel2;
		Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem4;
		Telerik::WinControls::UI::RadTileElement^  radTileSetup;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonLua;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonAlerts;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonLooter;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonHunter;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonWaypointer;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonSpellcaster;
		Telerik::WinControls::UI::RadPanel^  radPanel5;
		Telerik::WinControls::UI::RadPanel^  radPanel4;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonGeneral;
		Telerik::WinControls::UI::RadTileElement^  radTileSpecialArea;
	private: Telerik::WinControls::UI::RadTileElement^  radTileHotkeys;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem5;




	private: Telerik::WinControls::UI::RadTileElement^  radTileTools;
	private: Telerik::WinControls::UI::RadPanel^  radPanel7;
	private: Telerik::WinControls::UI::RadPanel^  radPanelHud;

	private: Telerik::WinControls::UI::RadMenuItem^  menu_client;

	private: Telerik::WinControls::UI::RadTileElement^  radTileUtility;
	private: Telerik::WinControls::UI::RadMenuItem^  recent_scripts;
	private: Telerik::WinControls::UI::RadTileElement^  radTileSell;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_settings;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_language;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_eng;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_pt;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem3;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_config_path;
	private: Telerik::WinControls::UI::RadLabel^  label_stamina;
	private: Telerik::WinControls::UI::RadLabel^  label_balance;
	private: Telerik::WinControls::UI::RadLabel^  label_profit_hour;
	private: Telerik::WinControls::UI::RadLabel^  label_check_hour;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: System::Windows::Forms::PictureBox^  pictureBox3;
	private: System::Windows::Forms::PictureBox^  pictureBox2;
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: Telerik::WinControls::UI::RadMenuItem^  menu_restore;
	private: Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem2;
	private: Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem3;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem7;
	private: Telerik::WinControls::UI::RadMenu^  radMenu3;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_clear;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem6;
	protected: Telerik::WinControls::UI::RadToggleButton^  radToggleButtonSpellAttack;
	private:

	protected: Telerik::WinControls::UI::RadToggleButton^  radToggleButtonSpellHeal;
	private: Telerik::WinControls::UI::RadMenuItem^  MenuItemMissingHotkey;
private: Telerik::WinControls::UI::RadMenuItem^  MenuItemSpanish;
	protected:
	private:





	protected:
	private: System::ComponentModel::IContainer^  components;
	protected:
#pragma region Windows Form Designer generated codeg
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Panorama::typeid));
			this->radPanorama1 = (gcnew Telerik::WinControls::UI::RadPanorama());
			this->panoramaGroupSetup = (gcnew Telerik::WinControls::UI::TileGroupElement());
			this->radTileWaypointer = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileSpellcaster = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileLooter = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileHunter = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileDepoter = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileAlerts = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileRepoter = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileContainerRepoter = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileLuaBackground = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileSetup = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileSpecialArea = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileHotkeys = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileTools = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileUtility = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileSell = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileElement2 = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radTileElement1 = (gcnew Telerik::WinControls::UI::RadTileElement());
			this->radPanorama2 = (gcnew Telerik::WinControls::UI::RadPanorama());
			this->radToggleButtonSpellAttack = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radToggleButtonSpellHeal = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
			this->label_balance = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
			this->label_profit_hour = (gcnew Telerik::WinControls::UI::RadLabel());
			this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
			this->label_check_hour = (gcnew Telerik::WinControls::UI::RadLabel());
			this->pictureBox2 = (gcnew System::Windows::Forms::PictureBox());
			this->label_stamina = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->radToggleButtonLua = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->pingLabel = (gcnew Telerik::WinControls::UI::RadLabel());
			this->radToggleButtonAlerts = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radToggleButtonLooter = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radToggleButtonHunter = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radToggleButtonWaypointer = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radToggleButtonSpellcaster = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->notifyIcon = (gcnew System::Windows::Forms::NotifyIcon(this->components));
			this->contextMenuStrip1 = (gcnew System::Windows::Forms::ContextMenuStrip(this->components));
			this->toolStripMenuItem1 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->toolStripSeparator1 = (gcnew System::Windows::Forms::ToolStripSeparator());
			this->toolStripMenuItem2 = (gcnew System::Windows::Forms::ToolStripMenuItem());
			this->pingUpdater = (gcnew System::Windows::Forms::Timer(this->components));
			this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->menu_client = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuSeparatorItem2 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
			this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->menu_restore = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->recent_scripts = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
			this->radMenuItem3 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_config_path = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->MenuItemMissingHotkey = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuItem5 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_language = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_eng = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_pt = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->MenuItemSpanish = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuThemes = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_clear = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radMenuSeparatorItem3 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
			this->radMenuItem7 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->bt_settings = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel5 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radMenu3 = (gcnew Telerik::WinControls::UI::RadMenu());
			this->radMenuItem6 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radToggleButtonGeneral = (gcnew Telerik::WinControls::UI::RadToggleButton());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel7 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanelHud = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radMenuItem4 = (gcnew Telerik::WinControls::UI::RadMenuItem());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanorama1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanorama2))->BeginInit();
			this->radPanorama2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellAttack))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellHeal))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
			this->radGroupBox1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_balance))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_profit_hour))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_check_hour))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_stamina))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLua))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pingLabel))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonAlerts))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLooter))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonHunter))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonWaypointer))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellcaster))->BeginInit();
			this->contextMenuStrip1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->BeginInit();
			this->radPanel5->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu3))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
			this->radPanel4->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonGeneral))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel7))->BeginInit();
			this->radPanel7->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelHud))->BeginInit();
			this->radPanelHud->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// radPanorama1
			// 
			this->radPanorama1->AllowDragDrop = false;
			this->radPanorama1->AutoScroll = true;
			this->radPanorama1->CellSize = System::Drawing::Size(80, 80);
			this->radPanorama1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanorama1->EnableZooming = false;
			this->radPanorama1->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radPanorama1->Groups->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->panoramaGroupSetup });
			this->radPanorama1->Location = System::Drawing::Point(0, 0);
			this->radPanorama1->MouseWheelBehavior = Telerik::WinControls::UI::PanoramaMouseWheelBehavior::Scroll;
			this->radPanorama1->Name = L"radPanorama1";
			// 
			// 
			// 
			this->radPanorama1->RootElement->Alignment = System::Drawing::ContentAlignment::TopLeft;
			this->radPanorama1->RowsCount = 4;
			this->radPanorama1->ShowGroups = true;
			this->radPanorama1->Size = System::Drawing::Size(568, 354);
			this->radPanorama1->TabIndex = 0;
			this->radPanorama1->Text = L"radPanorama1";
			this->radPanorama1->ThemeName = L"ControlDefault";
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radPanorama1->GetChildAt(0)->GetChildAt(1)))->ScaleSize = System::Drawing::Size(0,
				0);
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radPanorama1->GetChildAt(0)->GetChildAt(1)))->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radPanorama1->GetChildAt(0)->GetChildAt(1)))->RightToLeft = true;
			// 
			// panoramaGroupSetup
			// 
			this->panoramaGroupSetup->AccessibleDescription = L"Setup";
			this->panoramaGroupSetup->AccessibleName = L"Setup";
			this->panoramaGroupSetup->AutoSize = true;
			this->panoramaGroupSetup->BorderBottomWidth = 1;
			this->panoramaGroupSetup->BorderBoxStyle = Telerik::WinControls::BorderBoxStyle::SingleBorder;
			this->panoramaGroupSetup->BorderDashStyle = System::Drawing::Drawing2D::DashStyle::Solid;
			this->panoramaGroupSetup->BorderLeftWidth = 1;
			this->panoramaGroupSetup->BorderRightWidth = 1;
			this->panoramaGroupSetup->BorderTopWidth = 1;
			this->panoramaGroupSetup->CellSize = System::Drawing::Size(73, 73);
			this->panoramaGroupSetup->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(15) {
				this->radTileWaypointer,
					this->radTileSpellcaster, this->radTileLooter, this->radTileHunter, this->radTileDepoter, this->radTileAlerts, this->radTileRepoter,
					this->radTileContainerRepoter, this->radTileLuaBackground, this->radTileSetup, this->radTileSpecialArea, this->radTileHotkeys,
					this->radTileTools, this->radTileUtility, this->radTileSell
			});
			this->panoramaGroupSetup->Name = L"panoramaGroupSetup";
			this->panoramaGroupSetup->RowsCount = 7;
			this->panoramaGroupSetup->Text = L"";
			// 
			// radTileWaypointer
			// 
			this->radTileWaypointer->AccessibleDescription = L"radTileWaypointer";
			this->radTileWaypointer->AccessibleName = L"radTileWaypointer";
			this->radTileWaypointer->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->radTileWaypointer->Name = L"radTileWaypointer";
			this->radTileWaypointer->Text = L"Waypoint";
			this->radTileWaypointer->Click += gcnew System::EventHandler(this, &Panorama::radTileWaypointer_Click);
			// 
			// radTileSpellcaster
			// 
			this->radTileSpellcaster->AccessibleDescription = L"radTileSpellcaster";
			this->radTileSpellcaster->AccessibleName = L"radTileSpellcaster";
			this->radTileSpellcaster->Column = 1;
			this->radTileSpellcaster->DrawImage = false;
			this->radTileSpellcaster->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->radTileSpellcaster->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radTileSpellcaster.Image")));
			this->radTileSpellcaster->ImageAlignment = System::Drawing::ContentAlignment::BottomCenter;
			this->radTileSpellcaster->Name = L"radTileSpellcaster";
			this->radTileSpellcaster->Text = L"<html><p>Spells</p></html>";
			this->radTileSpellcaster->TextAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radTileSpellcaster->TextImageRelation = System::Windows::Forms::TextImageRelation::Overlay;
			this->radTileSpellcaster->Click += gcnew System::EventHandler(this, &Panorama::radTileSpellCaster_Click);
			// 
			// radTileLooter
			// 
			this->radTileLooter->AccessibleDescription = L"radTileLooter";
			this->radTileLooter->AccessibleName = L"radTileLooter";
			this->radTileLooter->Column = 2;
			this->radTileLooter->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radTileLooter->Name = L"radTileLooter";
			this->radTileLooter->Text = L"Looter";
			this->radTileLooter->Click += gcnew System::EventHandler(this, &Panorama::radTileLoot_Click);
			// 
			// radTileHunter
			// 
			this->radTileHunter->AccessibleDescription = L"radTileHunter";
			this->radTileHunter->AccessibleName = L"radTileHunter";
			this->radTileHunter->Column = 3;
			this->radTileHunter->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radTileHunter->Name = L"radTileHunter";
			this->radTileHunter->StretchVertically = true;
			this->radTileHunter->Text = L"Hunter";
			this->radTileHunter->Click += gcnew System::EventHandler(this, &Panorama::radTileHunter_Click);
			// 
			// radTileDepoter
			// 
			this->radTileDepoter->AccessibleDescription = L"radTileDepoter";
			this->radTileDepoter->AccessibleName = L"radTileDepoter";
			this->radTileDepoter->Column = 4;
			this->radTileDepoter->DisabledTextRenderingHint = System::Drawing::Text::TextRenderingHint::SystemDefault;
			this->radTileDepoter->DrawImage = false;
			this->radTileDepoter->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radTileDepoter->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"radTileDepoter.Image")));
			this->radTileDepoter->ImageAlignment = System::Drawing::ContentAlignment::BottomCenter;
			this->radTileDepoter->Name = L"radTileDepoter";
			this->radTileDepoter->Text = L"Depot";
			this->radTileDepoter->TextAlignment = System::Drawing::ContentAlignment::MiddleCenter;
			this->radTileDepoter->TextImageRelation = System::Windows::Forms::TextImageRelation::Overlay;
			this->radTileDepoter->TextRenderingHint = System::Drawing::Text::TextRenderingHint::SystemDefault;
			this->radTileDepoter->Click += gcnew System::EventHandler(this, &Panorama::radTileDepoter_Click);
			// 
			// radTileAlerts
			// 
			this->radTileAlerts->AccessibleDescription = L"radTileAlerts";
			this->radTileAlerts->AccessibleName = L"radTileAlerts";
			this->radTileAlerts->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radTileAlerts->Name = L"radTileAlerts";
			this->radTileAlerts->Row = 2;
			this->radTileAlerts->Text = L"Alerts";
			this->radTileAlerts->Click += gcnew System::EventHandler(this, &Panorama::radTileAlerts_Click);
			// 
			// radTileRepoter
			// 
			this->radTileRepoter->AccessibleDescription = L"radTileRepoter";
			this->radTileRepoter->AccessibleName = L"radTileRepoter";
			this->radTileRepoter->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radTileRepoter->Name = L"radTileRepoter";
			this->radTileRepoter->Row = 1;
			this->radTileRepoter->Text = L"Repoter";
			this->radTileRepoter->Click += gcnew System::EventHandler(this, &Panorama::radTileRepoter_Click);
			// 
			// radTileContainerRepoter
			// 
			this->radTileContainerRepoter->AccessibleDescription = L"radTileContainerRepoter";
			this->radTileContainerRepoter->AccessibleName = L"radTileContainerRepoter";
			this->radTileContainerRepoter->Column = 1;
			this->radTileContainerRepoter->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->radTileContainerRepoter->Name = L"radTileContainerRepoter";
			this->radTileContainerRepoter->Row = 1;
			this->radTileContainerRepoter->Text = L"<html><p>Container</p><p>Repoter</p></html>";
			this->radTileContainerRepoter->Click += gcnew System::EventHandler(this, &Panorama::radTileContainerRepoter_Click);
			// 
			// radTileLuaBackground
			// 
			this->radTileLuaBackground->AccessibleDescription = L"radTileLuaBackground";
			this->radTileLuaBackground->AccessibleName = L"radTileLuaBackground";
			this->radTileLuaBackground->Column = 2;
			this->radTileLuaBackground->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular,
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(0)));
			this->radTileLuaBackground->Name = L"radTileLuaBackground";
			this->radTileLuaBackground->Row = 1;
			this->radTileLuaBackground->Text = L"<html><p>Lua</p><p>Scripts</p></html>";
			this->radTileLuaBackground->Click += gcnew System::EventHandler(this, &Panorama::radTileLuaBackground_Click);
			// 
			// radTileSetup
			// 
			this->radTileSetup->AccessibleDescription = L"radTileSetup";
			this->radTileSetup->AccessibleName = L"radTileSetup";
			this->radTileSetup->Column = 3;
			this->radTileSetup->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radTileSetup->Name = L"radTileSetup";
			this->radTileSetup->Row = 1;
			this->radTileSetup->Text = L"Setup";
			this->radTileSetup->Click += gcnew System::EventHandler(this, &Panorama::radTileSetup_Click);
			// 
			// radTileSpecialArea
			// 
			this->radTileSpecialArea->AccessibleDescription = L"Special Areas";
			this->radTileSpecialArea->AccessibleName = L"Special Areas";
			this->radTileSpecialArea->Column = 1;
			this->radTileSpecialArea->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F));
			this->radTileSpecialArea->Name = L"radTileSpecialArea";
			this->radTileSpecialArea->Row = 2;
			this->radTileSpecialArea->Text = L"<html><p>Special</p><p>Areas</p></html>";
			this->radTileSpecialArea->Click += gcnew System::EventHandler(this, &Panorama::radTileSpecialArea_Click);
			// 
			// radTileHotkeys
			// 
			this->radTileHotkeys->AccessibleDescription = L"radTileHotkeys";
			this->radTileHotkeys->AccessibleName = L"radTileHotkeys";
			this->radTileHotkeys->Column = 4;
			this->radTileHotkeys->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F));
			this->radTileHotkeys->Name = L"radTileHotkeys";
			this->radTileHotkeys->Row = 1;
			this->radTileHotkeys->Text = L"<html><p>Config</p><p>Path</p></html>";
			this->radTileHotkeys->Click += gcnew System::EventHandler(this, &Panorama::radTileHotkeys_Click);
			// 
			// radTileTools
			// 
			this->radTileTools->AccessibleDescription = L"radTileTools";
			this->radTileTools->AccessibleName = L"radTileTools";
			this->radTileTools->Column = 2;
			this->radTileTools->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F));
			this->radTileTools->Name = L"radTileTools";
			this->radTileTools->Row = 2;
			this->radTileTools->Text = L"Tools";
			this->radTileTools->Click += gcnew System::EventHandler(this, &Panorama::radTileTools_Click);
			// 
			// radTileUtility
			// 
			this->radTileUtility->AccessibleDescription = L"radTileUtility";
			this->radTileUtility->AccessibleName = L"radTileUtility";
			this->radTileUtility->BorderBottomWidth = 1;
			this->radTileUtility->Column = 3;
			this->radTileUtility->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F));
			this->radTileUtility->Name = L"radTileUtility";
			this->radTileUtility->Row = 2;
			this->radTileUtility->Text = L"Utility";
			this->radTileUtility->Click += gcnew System::EventHandler(this, &Panorama::radTileUtility_Click);
			// 
			// radTileSell
			// 
			this->radTileSell->AccessibleDescription = L"radTileSell";
			this->radTileSell->AccessibleName = L"radTileSell";
			this->radTileSell->Column = 4;
			this->radTileSell->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F));
			this->radTileSell->Name = L"radTileSell";
			this->radTileSell->Row = 2;
			this->radTileSell->Text = L"<html><p>Sell</p><p>Items</p></html>";
			this->radTileSell->Click += gcnew System::EventHandler(this, &Panorama::radTileSell_Click);
			// 
			// radTileElement2
			// 
			this->radTileElement2->Name = L"radTileElement2";
			// 
			// radTileElement1
			// 
			this->radTileElement1->AccessibleDescription = L"radTileElement1";
			this->radTileElement1->AccessibleName = L"radTileElement1";
			this->radTileElement1->Name = L"radTileElement1";
			this->radTileElement1->Text = L"radTileElement1";
			// 
			// radPanorama2
			// 
			this->radPanorama2->AllowDragDrop = false;
			this->radPanorama2->AutoScroll = true;
			this->radPanorama2->BackColor = System::Drawing::Color::Transparent;
			this->radPanorama2->CellSize = System::Drawing::Size(80, 80);
			this->radPanorama2->Controls->Add(this->radToggleButtonSpellAttack);
			this->radPanorama2->Controls->Add(this->radToggleButtonSpellHeal);
			this->radPanorama2->Controls->Add(this->radGroupBox1);
			this->radPanorama2->Controls->Add(this->radToggleButtonLua);
			this->radPanorama2->Controls->Add(this->pingLabel);
			this->radPanorama2->Controls->Add(this->radToggleButtonAlerts);
			this->radPanorama2->Controls->Add(this->radToggleButtonLooter);
			this->radPanorama2->Controls->Add(this->radToggleButtonHunter);
			this->radPanorama2->Controls->Add(this->radToggleButtonWaypointer);
			this->radPanorama2->Controls->Add(this->radToggleButtonSpellcaster);
			this->radPanorama2->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanorama2->EnableZooming = false;
			this->radPanorama2->Font = (gcnew System::Drawing::Font(L"Microsoft Sans Serif", 9.75F, System::Drawing::FontStyle::Regular, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radPanorama2->Location = System::Drawing::Point(0, 0);
			this->radPanorama2->MouseWheelBehavior = Telerik::WinControls::UI::PanoramaMouseWheelBehavior::Scroll;
			this->radPanorama2->Name = L"radPanorama2";
			// 
			// 
			// 
			this->radPanorama2->RootElement->Alignment = System::Drawing::ContentAlignment::TopLeft;
			this->radPanorama2->RootElement->AutoSize = false;
			this->radPanorama2->RowsCount = 4;
			this->radPanorama2->ShowGroups = true;
			this->radPanorama2->Size = System::Drawing::Size(311, 354);
			this->radPanorama2->TabIndex = 0;
			this->radPanorama2->ThemeName = L"ControlDefault";
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radPanorama2->GetChildAt(0)->GetChildAt(1)))->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			(cli::safe_cast<Telerik::WinControls::Primitives::ImagePrimitive^>(this->radPanorama2->GetChildAt(0)->GetChildAt(1)))->RightToLeft = true;
			// 
			// radToggleButtonSpellAttack
			// 
			this->radToggleButtonSpellAttack->Location = System::Drawing::Point(5, 75);
			this->radToggleButtonSpellAttack->Name = L"radToggleButtonSpellAttack";
			this->radToggleButtonSpellAttack->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonSpellAttack->TabIndex = 17;
			this->radToggleButtonSpellAttack->Text = L"Spell:Attack";
			// 
			// radToggleButtonSpellHeal
			// 
			this->radToggleButtonSpellHeal->Location = System::Drawing::Point(5, 52);
			this->radToggleButtonSpellHeal->Name = L"radToggleButtonSpellHeal";
			this->radToggleButtonSpellHeal->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonSpellHeal->TabIndex = 16;
			this->radToggleButtonSpellHeal->Text = L"Spell:Heal";
			// 
			// radGroupBox1
			// 
			this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
			this->radGroupBox1->Controls->Add(this->label_balance);
			this->radGroupBox1->Controls->Add(this->radLabel3);
			this->radGroupBox1->Controls->Add(this->radLabel1);
			this->radGroupBox1->Controls->Add(this->radLabel2);
			this->radGroupBox1->Controls->Add(this->pictureBox4);
			this->radGroupBox1->Controls->Add(this->label_profit_hour);
			this->radGroupBox1->Controls->Add(this->pictureBox3);
			this->radGroupBox1->Controls->Add(this->label_check_hour);
			this->radGroupBox1->Controls->Add(this->pictureBox2);
			this->radGroupBox1->Controls->Add(this->label_stamina);
			this->radGroupBox1->Controls->Add(this->radLabel4);
			this->radGroupBox1->Controls->Add(this->pictureBox1);
			this->radGroupBox1->HeaderText = L"";
			this->radGroupBox1->Location = System::Drawing::Point(1, 196);
			this->radGroupBox1->Name = L"radGroupBox1";
			this->radGroupBox1->Size = System::Drawing::Size(131, 72);
			this->radGroupBox1->TabIndex = 15;
			this->radGroupBox1->Visible = false;
			// 
			// label_balance
			// 
			this->label_balance->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_balance->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label_balance->Location = System::Drawing::Point(72, 34);
			this->label_balance->Name = L"label_balance";
			this->label_balance->Size = System::Drawing::Size(13, 18);
			this->label_balance->TabIndex = 9;
			this->label_balance->Text = L"0";
			// 
			// radLabel3
			// 
			this->radLabel3->BackColor = System::Drawing::Color::Transparent;
			this->radLabel3->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radLabel3->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->radLabel3->Location = System::Drawing::Point(16, 35);
			this->radLabel3->Name = L"radLabel3";
			this->radLabel3->Size = System::Drawing::Size(57, 18);
			this->radLabel3->TabIndex = 5;
			this->radLabel3->Text = L"BALANCE";
			// 
			// radLabel1
			// 
			this->radLabel1->BackColor = System::Drawing::Color::Transparent;
			this->radLabel1->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radLabel1->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->radLabel1->Location = System::Drawing::Point(15, 3);
			this->radLabel1->Name = L"radLabel1";
			this->radLabel1->Size = System::Drawing::Size(40, 18);
			this->radLabel1->TabIndex = 3;
			this->radLabel1->Text = L"EXP/H";
			// 
			// radLabel2
			// 
			this->radLabel2->BackColor = System::Drawing::Color::Transparent;
			this->radLabel2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radLabel2->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->radLabel2->Location = System::Drawing::Point(15, 19);
			this->radLabel2->Name = L"radLabel2";
			this->radLabel2->Size = System::Drawing::Size(59, 18);
			this->radLabel2->TabIndex = 4;
			this->radLabel2->Text = L"PROFIT/H";
			// 
			// pictureBox4
			// 
			this->pictureBox4->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox4.Image")));
			this->pictureBox4->Location = System::Drawing::Point(5, 54);
			this->pictureBox4->Name = L"pictureBox4";
			this->pictureBox4->Size = System::Drawing::Size(10, 10);
			this->pictureBox4->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox4->TabIndex = 14;
			this->pictureBox4->TabStop = false;
			// 
			// label_profit_hour
			// 
			this->label_profit_hour->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_profit_hour->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label_profit_hour->Location = System::Drawing::Point(72, 18);
			this->label_profit_hour->Name = L"label_profit_hour";
			this->label_profit_hour->Size = System::Drawing::Size(13, 18);
			this->label_profit_hour->TabIndex = 8;
			this->label_profit_hour->Text = L"0";
			// 
			// pictureBox3
			// 
			this->pictureBox3->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox3.Image")));
			this->pictureBox3->Location = System::Drawing::Point(5, 7);
			this->pictureBox3->Name = L"pictureBox3";
			this->pictureBox3->Size = System::Drawing::Size(10, 10);
			this->pictureBox3->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox3->TabIndex = 13;
			this->pictureBox3->TabStop = false;
			// 
			// label_check_hour
			// 
			this->label_check_hour->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_check_hour->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label_check_hour->Location = System::Drawing::Point(54, 2);
			this->label_check_hour->Name = L"label_check_hour";
			this->label_check_hour->Size = System::Drawing::Size(13, 18);
			this->label_check_hour->TabIndex = 7;
			this->label_check_hour->Text = L"0";
			// 
			// pictureBox2
			// 
			this->pictureBox2->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox2.Image")));
			this->pictureBox2->Location = System::Drawing::Point(5, 23);
			this->pictureBox2->Name = L"pictureBox2";
			this->pictureBox2->Size = System::Drawing::Size(10, 10);
			this->pictureBox2->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox2->TabIndex = 12;
			this->pictureBox2->TabStop = false;
			// 
			// label_stamina
			// 
			this->label_stamina->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->label_stamina->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(192)), static_cast<System::Int32>(static_cast<System::Byte>(0)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->label_stamina->Location = System::Drawing::Point(72, 50);
			this->label_stamina->Name = L"label_stamina";
			this->label_stamina->Size = System::Drawing::Size(13, 18);
			this->label_stamina->TabIndex = 10;
			this->label_stamina->Text = L"0";
			// 
			// radLabel4
			// 
			this->radLabel4->BackColor = System::Drawing::Color::Transparent;
			this->radLabel4->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->radLabel4->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(64)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(64)));
			this->radLabel4->Location = System::Drawing::Point(16, 50);
			this->radLabel4->Name = L"radLabel4";
			this->radLabel4->Size = System::Drawing::Size(58, 18);
			this->radLabel4->TabIndex = 6;
			this->radLabel4->Text = L"STAMINA";
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(5, 39);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(10, 10);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
			this->pictureBox1->TabIndex = 11;
			this->pictureBox1->TabStop = false;
			// 
			// radToggleButtonLua
			// 
			this->radToggleButtonLua->Location = System::Drawing::Point(5, 167);
			this->radToggleButtonLua->Name = L"radToggleButtonLua";
			this->radToggleButtonLua->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonLua->TabIndex = 2;
			this->radToggleButtonLua->Text = L"Lua";
			// 
			// pingLabel
			// 
			this->pingLabel->Location = System::Drawing::Point(0, 0);
			this->pingLabel->Name = L"pingLabel";
			this->pingLabel->Size = System::Drawing::Size(2, 2);
			this->pingLabel->TabIndex = 0;
			// 
			// radToggleButtonAlerts
			// 
			this->radToggleButtonAlerts->Location = System::Drawing::Point(5, 144);
			this->radToggleButtonAlerts->Name = L"radToggleButtonAlerts";
			this->radToggleButtonAlerts->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonAlerts->TabIndex = 1;
			this->radToggleButtonAlerts->Text = L"Alerts";
			// 
			// radToggleButtonLooter
			// 
			this->radToggleButtonLooter->Location = System::Drawing::Point(5, 121);
			this->radToggleButtonLooter->Name = L"radToggleButtonLooter";
			this->radToggleButtonLooter->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonLooter->TabIndex = 1;
			this->radToggleButtonLooter->Text = L"Looter";
			// 
			// radToggleButtonHunter
			// 
			this->radToggleButtonHunter->Location = System::Drawing::Point(5, 98);
			this->radToggleButtonHunter->Name = L"radToggleButtonHunter";
			this->radToggleButtonHunter->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonHunter->TabIndex = 1;
			this->radToggleButtonHunter->Text = L"Hunter";
			// 
			// radToggleButtonWaypointer
			// 
			this->radToggleButtonWaypointer->Location = System::Drawing::Point(5, 6);
			this->radToggleButtonWaypointer->Name = L"radToggleButtonWaypointer";
			this->radToggleButtonWaypointer->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonWaypointer->TabIndex = 0;
			this->radToggleButtonWaypointer->Text = L"Waypointer";
			// 
			// radToggleButtonSpellcaster
			// 
			this->radToggleButtonSpellcaster->Location = System::Drawing::Point(5, 29);
			this->radToggleButtonSpellcaster->Name = L"radToggleButtonSpellcaster";
			this->radToggleButtonSpellcaster->Size = System::Drawing::Size(126, 20);
			this->radToggleButtonSpellcaster->TabIndex = 1;
			this->radToggleButtonSpellcaster->Text = L"Spellcaster";
			this->radToggleButtonSpellcaster->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Panorama::radToggleButtonSpellcaster_ToggleStateChanged);
			// 
			// notifyIcon
			// 
			this->notifyIcon->BalloonTipIcon = System::Windows::Forms::ToolTipIcon::Info;
			this->notifyIcon->BalloonTipText = L"Seu sistema est� aqui";
			this->notifyIcon->BalloonTipTitle = L"Smart Bot";
			this->notifyIcon->ContextMenuStrip = this->contextMenuStrip1;
			this->notifyIcon->Text = L"Smart Bot";
			this->notifyIcon->MouseDoubleClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Panorama::notifyIconicon_MouseDoubleClick);
			// 
			// contextMenuStrip1
			// 
			this->contextMenuStrip1->Items->AddRange(gcnew cli::array< System::Windows::Forms::ToolStripItem^  >(3) {
				this->toolStripMenuItem1,
					this->toolStripSeparator1, this->toolStripMenuItem2
			});
			this->contextMenuStrip1->Name = L"contextMenuStrip1";
			this->contextMenuStrip1->Size = System::Drawing::Size(104, 54);
			// 
			// toolStripMenuItem1
			// 
			this->toolStripMenuItem1->Name = L"toolStripMenuItem1";
			this->toolStripMenuItem1->Size = System::Drawing::Size(103, 22);
			this->toolStripMenuItem1->Text = L"Show";
			this->toolStripMenuItem1->Click += gcnew System::EventHandler(this, &Panorama::toolStripMenuItem1_Click);
			// 
			// toolStripSeparator1
			// 
			this->toolStripSeparator1->Name = L"toolStripSeparator1";
			this->toolStripSeparator1->Size = System::Drawing::Size(100, 6);
			// 
			// toolStripMenuItem2
			// 
			this->toolStripMenuItem2->Name = L"toolStripMenuItem2";
			this->toolStripMenuItem2->Size = System::Drawing::Size(103, 22);
			this->toolStripMenuItem2->Text = L"Close";
			this->toolStripMenuItem2->Click += gcnew System::EventHandler(this, &Panorama::toolStripMenuItem2_Click);
			// 
			// pingUpdater
			// 
			this->pingUpdater->Enabled = true;
			this->pingUpdater->Tick += gcnew System::EventHandler(this, &Panorama::pingUpdater_Tick);
			// 
			// radMenuItem1
			// 
			this->radMenuItem1->AccessibleDescription = L"Menu";
			this->radMenuItem1->AccessibleName = L"Menu";
			this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(16) {
				this->menu_client, this->radMenuSeparatorItem2,
					this->bt_load_, this->bt_save_, this->menu_restore, this->recent_scripts, this->radMenuSeparatorItem1, this->radMenuItem3, this->bt_config_path,
					this->MenuItemMissingHotkey, this->radMenuItem5, this->bt_language, this->radMenuThemes, this->bt_clear, this->radMenuSeparatorItem3,
					this->radMenuItem7
			});
			this->radMenuItem1->Name = L"radMenuItem1";
			this->radMenuItem1->Text = L"Menu";
			// 
			// menu_client
			// 
			this->menu_client->AccessibleDescription = L"New Client";
			this->menu_client->AccessibleName = L"New Client";
			this->menu_client->Name = L"menu_client";
			this->menu_client->Text = L"Selected Client";
			this->menu_client->Click += gcnew System::EventHandler(this, &Panorama::menu_client_Click);
			// 
			// radMenuSeparatorItem2
			// 
			this->radMenuSeparatorItem2->AccessibleDescription = L"radMenuSeparatorItem2";
			this->radMenuSeparatorItem2->AccessibleName = L"radMenuSeparatorItem2";
			this->radMenuSeparatorItem2->Name = L"radMenuSeparatorItem2";
			this->radMenuSeparatorItem2->Text = L"radMenuSeparatorItem2";
			// 
			// bt_load_
			// 
			this->bt_load_->AccessibleDescription = L"radMenuItem4";
			this->bt_load_->AccessibleName = L"radMenuItem4";
			this->bt_load_->Name = L"bt_load_";
			this->bt_load_->Text = L"Load";
			this->bt_load_->Click += gcnew System::EventHandler(this, &Panorama::bt_load_Click);
			// 
			// bt_save_
			// 
			this->bt_save_->AccessibleDescription = L"radMenuItem3";
			this->bt_save_->AccessibleName = L"radMenuItem3";
			this->bt_save_->Name = L"bt_save_";
			this->bt_save_->Text = L"Save";
			this->bt_save_->Click += gcnew System::EventHandler(this, &Panorama::bt_save_Click);
			// 
			// menu_restore
			// 
			this->menu_restore->AccessibleDescription = L"Restore Last Script";
			this->menu_restore->AccessibleName = L"Restore Last Script";
			this->menu_restore->Name = L"menu_restore";
			this->menu_restore->Text = L"Restore Last Script";
			this->menu_restore->Click += gcnew System::EventHandler(this, &Panorama::menu_restore_Click);
			// 
			// recent_scripts
			// 
			this->recent_scripts->AccessibleDescription = L"Recent Scripts";
			this->recent_scripts->AccessibleName = L"Recent Scripts";
			this->recent_scripts->Name = L"recent_scripts";
			this->recent_scripts->Text = L"Recent Scripts";
			this->recent_scripts->Click += gcnew System::EventHandler(this, &Panorama::recent_scripts_Click);
			// 
			// radMenuSeparatorItem1
			// 
			this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
			this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
			// 
			// radMenuItem3
			// 
			this->radMenuItem3->AccessibleDescription = L"Bot General Settings.";
			this->radMenuItem3->AccessibleName = L"Bot General Settings.";
			this->radMenuItem3->Name = L"radMenuItem3";
			this->radMenuItem3->Text = L"Bot General Settings.";
			this->radMenuItem3->Click += gcnew System::EventHandler(this, &Panorama::radMenuItem3_Click);
			// 
			// bt_config_path
			// 
			this->bt_config_path->AccessibleDescription = L"Config Path";
			this->bt_config_path->AccessibleName = L"Config Path";
			this->bt_config_path->Name = L"bt_config_path";
			this->bt_config_path->Text = L"Hotkeys";
			this->bt_config_path->Click += gcnew System::EventHandler(this, &Panorama::bt_config_path_Click);
			// 
			// MenuItemMissingHotkey
			// 
			this->MenuItemMissingHotkey->AccessibleDescription = L"Missing Hotkeys";
			this->MenuItemMissingHotkey->AccessibleName = L"Missing Hotkeys";
			this->MenuItemMissingHotkey->Name = L"MenuItemMissingHotkey";
			this->MenuItemMissingHotkey->Text = L"Missing Hotkeys";
			this->MenuItemMissingHotkey->Click += gcnew System::EventHandler(this, &Panorama::MenuItemMissingHotkey_Click);
			// 
			// radMenuItem5
			// 
			this->radMenuItem5->AccessibleDescription = L"lootstatic";
			this->radMenuItem5->AccessibleName = L"lootstatic";
			this->radMenuItem5->Name = L"radMenuItem5";
			this->radMenuItem5->Text = L"Loot Statistics";
			this->radMenuItem5->Click += gcnew System::EventHandler(this, &Panorama::radMenuItem5_Click_1);
			// 
			// bt_language
			// 
			this->bt_language->AccessibleDescription = L"Language";
			this->bt_language->AccessibleName = L"Language";
			this->bt_language->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) { this->bt_eng, this->bt_pt, this->MenuItemSpanish });
			this->bt_language->Name = L"bt_language";
			this->bt_language->Text = L"Language";
			// 
			// bt_eng
			// 
			this->bt_eng->AccessibleDescription = L"English";
			this->bt_eng->AccessibleName = L"English";
			this->bt_eng->Name = L"bt_eng";
			this->bt_eng->Text = L"English";
			this->bt_eng->Click += gcnew System::EventHandler(this, &Panorama::bt_eng_Click);
			// 
			// bt_pt
			// 
			this->bt_pt->AccessibleDescription = L"Portugu�s";
			this->bt_pt->AccessibleName = L"Portugu�s";
			this->bt_pt->Name = L"bt_pt";
			this->bt_pt->Text = L"Portugu�s";
			this->bt_pt->Click += gcnew System::EventHandler(this, &Panorama::bt_pt_Click);
			// 
			// MenuItemSpanish
			// 
			this->MenuItemSpanish->AccessibleDescription = L"Spanish";
			this->MenuItemSpanish->AccessibleName = L"Spanish";
			this->MenuItemSpanish->Name = L"MenuItemSpanish";
			this->MenuItemSpanish->Text = L"Spanish";
			this->MenuItemSpanish->Click += gcnew System::EventHandler(this, &Panorama::MenuItemSpanish_Click);
			// 
			// radMenuThemes
			// 
			this->radMenuThemes->AccessibleDescription = L"radMenuThemes";
			this->radMenuThemes->AccessibleName = L"radMenuThemes";
			this->radMenuThemes->Name = L"radMenuThemes";
			this->radMenuThemes->Text = L"Themes";
			// 
			// bt_clear
			// 
			this->bt_clear->AccessibleDescription = L"bt_clear";
			this->bt_clear->AccessibleName = L"bt_clear";
			this->bt_clear->Name = L"bt_clear";
			this->bt_clear->Text = L"Clear";
			this->bt_clear->Click += gcnew System::EventHandler(this, &Panorama::bt_clear_Click);
			// 
			// radMenuSeparatorItem3
			// 
			this->radMenuSeparatorItem3->AccessibleDescription = L"radMenuSeparatorItem3";
			this->radMenuSeparatorItem3->AccessibleName = L"radMenuSeparatorItem3";
			this->radMenuSeparatorItem3->Name = L"radMenuSeparatorItem3";
			this->radMenuSeparatorItem3->Text = L"radMenuSeparatorItem3";
			// 
			// radMenuItem7
			// 
			this->radMenuItem7->AccessibleDescription = L"bt_exit";
			this->radMenuItem7->AccessibleName = L"bt_exit";
			this->radMenuItem7->Name = L"radMenuItem7";
			this->radMenuItem7->Text = L"Exit";
			this->radMenuItem7->Click += gcnew System::EventHandler(this, &Panorama::radMenuItem7_Click);
			// 
			// bt_settings
			// 
			this->bt_settings->AccessibleDescription = L"Settings";
			this->bt_settings->AccessibleName = L"Settings";
			this->bt_settings->Name = L"bt_settings";
			this->bt_settings->Text = L"Settings";
			this->bt_settings->Click += gcnew System::EventHandler(this, &Panorama::bt_settings_Click);
			// 
			// radPanel1
			// 
			this->radPanel1->AutoSize = true;
			this->radPanel1->Controls->Add(this->radPanel5);
			this->radPanel1->Controls->Add(this->radPanel4);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel1->Location = System::Drawing::Point(0, 0);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(500, 22);
			this->radPanel1->TabIndex = 2;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radPanel5
			// 
			this->radPanel5->Controls->Add(this->radMenu3);
			this->radPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel5->Location = System::Drawing::Point(0, 0);
			this->radPanel5->Name = L"radPanel5";
			this->radPanel5->Size = System::Drawing::Size(380, 22);
			this->radPanel5->TabIndex = 4;
			this->radPanel5->Text = L"radPanel5";
			// 
			// radMenu3
			// 
			this->radMenu3->BackColor = System::Drawing::Color::Transparent;
			this->radMenu3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radMenu3->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
				this->radMenuItem1, this->radMenuItem1,
					this->radMenuItem6
			});
			this->radMenu3->Location = System::Drawing::Point(0, 0);
			this->radMenu3->Name = L"radMenu3";
			this->radMenu3->Size = System::Drawing::Size(380, 20);
			this->radMenu3->TabIndex = 1;
			this->radMenu3->Text = L"radMenu3";
			// 
			// radMenuItem6
			// 
			this->radMenuItem6->AccessibleDescription = L"dev";
			this->radMenuItem6->AccessibleName = L"dev";
			this->radMenuItem6->Name = L"radMenuItem6";
			this->radMenuItem6->Text = L"dev";
			this->radMenuItem6->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
			this->radMenuItem6->Click += gcnew System::EventHandler(this, &Panorama::radMenuItem6_Click_1);
			// 
			// radPanel4
			// 
			this->radPanel4->Controls->Add(this->radToggleButtonGeneral);
			this->radPanel4->Dock = System::Windows::Forms::DockStyle::Right;
			this->radPanel4->Location = System::Drawing::Point(380, 0);
			this->radPanel4->Name = L"radPanel4";
			this->radPanel4->Size = System::Drawing::Size(120, 22);
			this->radPanel4->TabIndex = 3;
			this->radPanel4->Text = L"radPanel4";
			// 
			// radToggleButtonGeneral
			// 
			this->radToggleButtonGeneral->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radToggleButtonGeneral->Location = System::Drawing::Point(0, 0);
			this->radToggleButtonGeneral->Name = L"radToggleButtonGeneral";
			this->radToggleButtonGeneral->Size = System::Drawing::Size(120, 22);
			this->radToggleButtonGeneral->TabIndex = 2;
			this->radToggleButtonGeneral->Text = L"Disabled";
			this->radToggleButtonGeneral->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Panorama::radToggleButtonGeneral_ToggleStateChanged);
			// 
			// radPanel2
			// 
			this->radPanel2->Controls->Add(this->radPanel7);
			this->radPanel2->Controls->Add(this->radPanelHud);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel2->Location = System::Drawing::Point(0, 22);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(500, 354);
			this->radPanel2->TabIndex = 3;
			// 
			// radPanel7
			// 
			this->radPanel7->Controls->Add(this->radPanorama2);
			this->radPanel7->Location = System::Drawing::Point(365, 0);
			this->radPanel7->Name = L"radPanel7";
			this->radPanel7->Size = System::Drawing::Size(311, 354);
			this->radPanel7->TabIndex = 6;
			// 
			// radPanelHud
			// 
			this->radPanelHud->BackColor = System::Drawing::SystemColors::Control;
			this->radPanelHud->Controls->Add(this->radPanorama1);
			this->radPanelHud->Dock = System::Windows::Forms::DockStyle::Left;
			this->radPanelHud->Location = System::Drawing::Point(0, 0);
			this->radPanelHud->Name = L"radPanelHud";
			this->radPanelHud->Size = System::Drawing::Size(568, 354);
			this->radPanelHud->TabIndex = 5;
			// 
			// radMenuItem4
			// 
			this->radMenuItem4->Name = L"radMenuItem4";
			this->radMenuItem4->Text = L"";
			// 
			// Panorama
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::LightSteelBlue;
			this->ClientSize = System::Drawing::Size(500, 241);
			this->Controls->Add(this->radPanel2);
			this->Controls->Add(this->radPanel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedDialog;
			this->MaximizeBox = false;
			this->Name = L"Panorama";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Text = L"Bot";
			this->ThemeName = L"ControlDefault";
			this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Panorama::Panorama_FormClosing);
			this->Resize += gcnew System::EventHandler(this, &Panorama::Panorama_Resize);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanorama1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanorama2))->EndInit();
			this->radPanorama2->ResumeLayout(false);
			this->radPanorama2->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellAttack))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellHeal))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
			this->radGroupBox1->ResumeLayout(false);
			this->radGroupBox1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_balance))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_profit_hour))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_check_hour))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox2))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->label_stamina))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLua))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pingLabel))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonAlerts))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLooter))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonHunter))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonWaypointer))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonSpellcaster))->EndInit();
			this->contextMenuStrip1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->EndInit();
			this->radPanel5->ResumeLayout(false);
			this->radPanel5->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu3))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
			this->radPanel4->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonGeneral))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel7))->EndInit();
			this->radPanel7->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelHud))->EndInit();
			this->radPanelHud->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion

		static Panorama^ unique;

	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Panorama();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Panorama();
	}
			static void CloseUnique(){
				if (unique)unique->Close();
			}

			bool disable_update;

	public: static void closeAllOtherForms(){
				NeutralBot::SpecialAreaEditor::CloseUnique();
				NeutralBot::Waypoint::CloseUnique();
				NeutralBot::Tools::CloseUnique();
				Neutral::Spells::CloseUnique();
				Neutral::Looter::CloseUnique();
				NeutralBot::Hunter::CloseUnique();
				Neutral::Depot::CloseUnique();
				Neutral::Alerts::CloseUnique();
				Neutral::Repot::CloseUnique();
				NeutralBot::FormEditor::CloseUnique();
				Neutral::AdvancedRepoter::CloseUnique();
				NeutralBot::LuaBackground::CloseUnique();
				NeutralBot::ItemsInfo::CloseUnique();
	}
	public: static void realoadAllOtherForms(){
				NeutralBot::SpecialAreaEditor::ReloadForm();
				NeutralBot::Waypoint::ReloadForm();
				Neutral::Spells::ReloadForm();
				Neutral::Looter::ReloadForm();
				NeutralBot::Hunter::ReloadForm();
				Neutral::Depot::ReloadForm();
				Neutral::Alerts::ReloadForm();
				Neutral::Repot::ReloadForm();
				NeutralBot::FormEditor::ReloadForm();
				Neutral::AdvancedRepoter::ReloadForm();
				NeutralBot::LuaBackground::ReloadForm();
				NeutralBot::ItemsInfo::ReloadForm();
	}
	public: static void openAllOtherForms(){
				NeutralBot::SpecialAreaEditor::CreatUnique();
				NeutralBot::Waypoint::CreatUnique();
				NeutralBot::Tools::CreatUnique();
				Neutral::Spells::CreatUnique();
				Neutral::Looter::CreatUnique();
				NeutralBot::Hunter::CreatUnique();
				Neutral::Depot::CreatUnique();
				Neutral::Alerts::CreatUnique();
				Neutral::Repot::CreatUnique();
				NeutralBot::FormEditor::CreatUnique();
				Neutral::AdvancedRepoter::CreatUnique();
				NeutralBot::LuaBackground::CreatUnique();
				NeutralBot::ItemsInfo::CreatUnique();
	}

			System::Void toolStripMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
				if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente fechar o sistema? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::Yes)
					NeutralManager::get()->close();
			}
			System::Void toolStripMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {
				this->Show();
				this->WindowState = FormWindowState::Normal;
			}

			System::Void radTileSpecialArea_Click(System::Object^  sender, System::EventArgs^  e) {
				NeutralBot::SpecialAreaEditor::ShowUnique();
			}
			System::Void radTileWaypointer_Click(System::Object^  sender, System::EventArgs^  e) {
				NeutralBot::Waypoint::ShowUnique();
			}
			System::Void radTileSpellCaster_Click(System::Object^  sender, System::EventArgs^  e) {
				Neutral::Spells::ShowUnique();
			}
			System::Void radTileLoot_Click(System::Object^  sender, System::EventArgs^  e) {
				Neutral::Looter::ShowUnique();
			}
			System::Void radTileHunter_Click(System::Object^  sender, System::EventArgs^  e) {
				NeutralBot::Hunter::ShowUnique();
			}
			System::Void radTileDepoter_Click(System::Object^  sender, System::EventArgs^  e) {
				Neutral::Depot::ShowUnique();
			}
			System::Void radTileAlerts_Click(System::Object^  sender, System::EventArgs^  e) {
				Neutral::Alerts::ShowUnique();
			}
			System::Void radTileRepoter_Click(System::Object^  sender, System::EventArgs^  e) {
				Neutral::Repot::ShowUnique();
			}
			System::Void radTileSetup_Click(System::Object^  sender, System::EventArgs^  e) {
				NeutralBot::FormEditor::ShowUnique();
			}
			System::Void radTileContainerRepoter_Click(System::Object^  sender, System::EventArgs^  e) {
				Neutral::AdvancedRepoter::ShowUnique();
			}
			System::Void radTileLuaBackground_Click(System::Object^  sender, System::EventArgs^  e) {
				NeutralBot::LuaBackground::ShowUnique(nullptr);
			}
			System::Void notifyIconicon_MouseDoubleClick(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
				this->Show();
				this->WindowState = FormWindowState::Normal;
			}
			System::Void Panorama_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente fechar o sistema? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::No){
					e->Cancel = true;
					return;
				}
				NeutralManager::get()->close();
			}
			System::Void bt_save_Click(System::Object^  sender, System::EventArgs^  e) {
				System::Windows::Forms::SaveFileDialog fileDialog;
				fileDialog.Filter = "Json Object|*.json";
				fileDialog.Title = "Save Script";

				System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();

				if (result == System::Windows::Forms::DialogResult::Cancel)
					return;

				String^ path = fileDialog.FileName;
				if (!NeutralManager::get()->save_script(managed_util::fromSS(path)))
					Telerik::WinControls::RadMessageBox::Show(this, "Error Saved.", "Save/Load");
				else
					Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved.", "Save/Load");
			}
			System::Void bt_load_Click(System::Object^  sender, System::EventArgs^  e) {
				System::Windows::Forms::OpenFileDialog fileDialog;
				fileDialog.Filter = "Json Object|*.json";
				fileDialog.Title = "Open Script";

				System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();
				if (result == System::Windows::Forms::DialogResult::Cancel)
					return;

				String^ path = fileDialog.FileName;

				if (!NeutralManager::get()->load_script(managed_util::fromSS(path)))
					Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded.", "Save/Load");
				else{
					closeAllOtherForms();
					Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded.", "Save/Load");
				}

				radToggleButtonGeneral->Text = "General";
				radToggleButtonSpellcaster->Text = "Spellcaster";
				radToggleButtonHunter->Text = "Hunter";
				radToggleButtonLooter->Text = "Looter";
				radToggleButtonAlerts->Text = "Alerts";
				radToggleButtonLua->Text = "Lua";
				radToggleButtonWaypointer->Text = "Waypointer";
				radToggleButtonSpellHeal->Text = "Spell:Heal";
				radToggleButtonSpellAttack->Text = "Spell:Attack";
				managed_util::ManagedControlsHook::get()->updateButtonsByVars();
				GeneralManager::get()->add_recent_scripts(managed_util::fromSS(path));
			}

			void loadtheme(String^ themeText){
				Telerik::WinControls::RadThemeComponentBase^ theme;

				if (themeText == "Office2013LightTheme")
					theme = gcnew Telerik::WinControls::Themes::Office2013LightTheme();
				else if (themeText == "Office2013DarkTheme")
					theme = gcnew Telerik::WinControls::Themes::Office2013DarkTheme();
				else if (themeText == "Office2010SilverTheme")
					theme = gcnew Telerik::WinControls::Themes::Office2010SilverTheme();
				else if (themeText == "Office2010BlackTheme")
					theme = gcnew Telerik::WinControls::Themes::Office2010BlackTheme();
				else if (themeText == "Office2007SilverTheme")
					theme = gcnew Telerik::WinControls::Themes::Office2007SilverTheme();
				else if (themeText == "Office2007BlackTheme")
					theme = gcnew Telerik::WinControls::Themes::Office2007BlackTheme();
				else if (themeText == "HighContrastBlackTheme")
					theme = gcnew Telerik::WinControls::Themes::HighContrastBlackTheme();
				else if (themeText == "BreezeTheme")
					theme = gcnew Telerik::WinControls::Themes::BreezeTheme();
				else if (themeText == "DesertTheme")
					theme = gcnew Telerik::WinControls::Themes::DesertTheme();
				else if (themeText == "AquaTheme")
					theme = gcnew Telerik::WinControls::Themes::AquaTheme();
				else if (themeText == "Windows8Theme"){
					theme = gcnew Telerik::WinControls::Themes::Windows8Theme();
				}
				else if (themeText == "Windows7Theme"){
					theme = gcnew Telerik::WinControls::Themes::Windows7Theme();
				}
				else if (themeText == "VisualStudio2012LightTheme"){
					theme = gcnew Telerik::WinControls::Themes::VisualStudio2012LightTheme();
				}
				else if (themeText == "VisualStudio2012DarkTheme"){
					theme = gcnew Telerik::WinControls::Themes::VisualStudio2012DarkTheme();
				}
				else if (themeText == "TelerikMetroTouchTheme"){
					theme = gcnew Telerik::WinControls::Themes::TelerikMetroTouchTheme();
				}
				else if (themeText == "TelerikMetroBlueTheme"){
					theme = gcnew Telerik::WinControls::Themes::TelerikMetroBlueTheme();
				}
				else if (themeText == "TelerikMetroTheme"){

					theme = gcnew Telerik::WinControls::Themes::TelerikMetroTheme();
				}

				if (theme)
					Telerik::WinControls::ThemeResolutionService::ApplicationThemeName = theme->ThemeName;

			}

			System::Void radMenuHeaderItem1_Click(System::Object^  sender, System::EventArgs^  e) {
				Telerik::WinControls::UI::RadMenuHeaderItem^ current_sender = (Telerik::WinControls::UI::RadMenuHeaderItem^)sender;
				String^ themeText = current_sender->Text;
				Telerik::WinControls::RadThemeComponentBase^ theme;

				if (themeText == "Office2013LightTheme"){
					theme = gcnew Telerik::WinControls::Themes::Office2013LightTheme();
				}
				else if (themeText == "Office2013DarkTheme"){
					theme = gcnew Telerik::WinControls::Themes::Office2013DarkTheme();
				}
				else if (themeText == "Office2010SilverTheme"){
					theme = gcnew Telerik::WinControls::Themes::Office2010SilverTheme();
				}
				else if (themeText == "Office2010BlackTheme"){
					theme = gcnew Telerik::WinControls::Themes::Office2010BlackTheme();
				}
				else if (themeText == "Office2007SilverTheme"){
					theme = gcnew Telerik::WinControls::Themes::Office2007SilverTheme();
				}
				else if (themeText == "Office2007BlackTheme"){
					theme = gcnew Telerik::WinControls::Themes::Office2007BlackTheme();
				}
				else if (themeText == "HighContrastBlackTheme"){
					theme = gcnew Telerik::WinControls::Themes::HighContrastBlackTheme();
				}
				else if (themeText == "BreezeTheme"){
					theme = gcnew Telerik::WinControls::Themes::BreezeTheme();
				}
				else if (themeText == "DesertTheme"){
					theme = gcnew Telerik::WinControls::Themes::DesertTheme();
				}
				else if (themeText == "AquaTheme"){
					theme = gcnew Telerik::WinControls::Themes::AquaTheme();
				}
				else if (themeText == "Windows8Theme"){
					theme = gcnew Telerik::WinControls::Themes::Windows8Theme();
				}
				else if (themeText == "Windows7Theme"){
					theme = gcnew Telerik::WinControls::Themes::Windows7Theme();
				}
				else if (themeText == "VisualStudio2012LightTheme"){
					theme = gcnew Telerik::WinControls::Themes::VisualStudio2012LightTheme();
				}
				else if (themeText == "VisualStudio2012DarkTheme"){
					theme = gcnew Telerik::WinControls::Themes::VisualStudio2012DarkTheme();
				}
				else if (themeText == "TelerikMetroTouchTheme"){
					theme = gcnew Telerik::WinControls::Themes::TelerikMetroTouchTheme();
				}
				else if (themeText == "TelerikMetroBlueTheme"){
					theme = gcnew Telerik::WinControls::Themes::TelerikMetroBlueTheme();
				}
				else if (themeText == "TelerikMetroTheme"){

					theme = gcnew Telerik::WinControls::Themes::TelerikMetroTheme();
				}
				if (theme){
					GeneralManager::get()->set_themaName(managed_util::fromSS(themeText));
					Telerik::WinControls::ThemeResolutionService::ApplicationThemeName = theme->ThemeName;
				}
			}

			System::Void radMenuItem5_Click(System::Object^  sender, System::EventArgs^  e) {
				(gcnew NeutralBot::MapWall)->Show();
			}

#ifdef DEV_MODE
#endif
			System::Void pingUpdater_Tick(System::Object^  sender, System::EventArgs^  e);

	private: System::Void radTileHotkeys_Click(System::Object^  sender, System::EventArgs^  e) {
				 NeutralBot::ConfigPath::ShowUnique();
	}
	private: System::Void radMenuItem6_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew NeutralBot::DevelopmentForm)->Show();
	}
	private: System::Void radMenuItem5_Click_1(System::Object^  sender, System::EventArgs^  e) {
				 Neutral::LooterStatistics::ShowUnique();
	}
			 void load_general_config(){
				 NeutralManager::get()->load_default_config(DEFAULT_DIR_SETTINGS_PATH + "\\default.json");
				 NeutralManager::get()->load_items_config(DEFAULT_DIR_SETTINGS_PATH + "\\Items.json");
			 }


			 void update_idiom(){
				 recent_scripts->Text = gcnew String(&GET_TR(managed_util::fromSS(recent_scripts->Text))[0]);
				 MenuItemMissingHotkey->Text = gcnew String(&GET_TR(managed_util::fromSS(MenuItemMissingHotkey->Text))[0]);
				 menu_restore->Text = gcnew String(&GET_TR(managed_util::fromSS(menu_restore->Text))[0]);
				 bt_settings->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_settings->Text))[0]);
				 bt_language->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_language->Text))[0]);
				 radMenuThemes->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuThemes->Text))[0]);
				 bt_clear->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_clear->Text))[0]);
				 radMenuItem7->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem7->Text))[0]);
				 pingLabel->Text = gcnew String(&GET_TR(managed_util::fromSS(pingLabel->Text))[0]);
				 toolStripMenuItem1->Text = gcnew String(&GET_TR(managed_util::fromSS(toolStripMenuItem1->Text))[0]);
				 toolStripMenuItem2->Text = gcnew String(&GET_TR(managed_util::fromSS(toolStripMenuItem2->Text))[0]);
				 bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
				 bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);
				 menu_client->Text = gcnew String(&GET_TR(managed_util::fromSS(menu_client->Text))[0]);
			 }
			 bool top_most;

	private: System::Void menu_client_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void radTileTools_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew Tools)->ShowUnique();
	}
	private: System::Void radTileUtility_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew Useful)->ShowUnique();
	}
	private: System::Void radTileSell_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew Sell)->ShowUnique();
	}
	private: System::Void recent_scripts_Click(System::Object^  sender, System::EventArgs^  e) {
				 load_recent_scripts();
	}
	private: System::Void Menu_recent_script_Click(System::Object^  sender, System::EventArgs^  e) {
				 Telerik::WinControls::UI::RadMenuItem^ menu_recent_script = (Telerik::WinControls::UI::RadMenuItem^)sender;
				 if (!NeutralManager::get()->load_script(managed_util::fromSS(menu_recent_script->Name))){
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded.", "Save/Load");
					 GeneralManager::get()->remove_recent_scritps(managed_util::fromSS(menu_recent_script->Name));
				 }
				 else{
					 closeAllOtherForms();
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded.", "Save/Load");
				 }

				 radToggleButtonGeneral->Text = "General";
				 radToggleButtonSpellcaster->Text = "Spellcaster";
				 radToggleButtonHunter->Text = "Hunter";
				 radToggleButtonLooter->Text = "Looter";
				 radToggleButtonAlerts->Text = "Alerts";
				 radToggleButtonLua->Text = "Lua";
				 radToggleButtonWaypointer->Text = "Waypointer";
				 radToggleButtonSpellHeal->Text = "Spell:Heal";
				 radToggleButtonSpellAttack->Text = "Spell:Attack";
				 managed_util::ManagedControlsHook::get()->updateButtonsByVars();

	}

			 void load_recent_scripts(){
				 this->recent_scripts->Items->Clear();
				 std::vector<std::string> recent_scripts_vector = GeneralManager::get()->get_recent_scripts();
				 for (auto item : recent_scripts_vector){
					 String^ fileName = System::IO::Path::GetFileName(gcnew String(item.c_str()));
					 Telerik::WinControls::UI::RadMenuItem^  Menu_recent_script = (gcnew Telerik::WinControls::UI::RadMenuItem());
					 Menu_recent_script->Name = gcnew String(item.c_str());
					 Menu_recent_script->Text = fileName;
					 Menu_recent_script->Click += gcnew System::EventHandler(this, &Panorama::Menu_recent_script_Click);
					 this->recent_scripts->Items->Add(Menu_recent_script);
				 }
			 }

	private: System::Void bt_settings_Click(System::Object^  sender, System::EventArgs^  e) {
				 NeutralBot::Settings::ShowUnique();
	}
	private: System::Void bt_eng_Click(System::Object^  sender, System::EventArgs^  e) {
				 LanguageManager::get()->set_current_lang("eng");
				 GeneralManager::get()->set_currentLang("eng");
				 update_idiom();
	}
	private: System::Void bt_pt_Click(System::Object^  sender, System::EventArgs^  e) {
				 LanguageManager::get()->set_current_lang("pt");
				 GeneralManager::get()->set_currentLang("pt");
				 update_idiom();
	}
	private: System::Void radMenuItem3_Click(System::Object^  sender, System::EventArgs^  e) {
				 GeneralSettings::ShowUnique();
	}
	private: System::Void bt_config_path_Click(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew HotkeyManager)->ShowUnique();
	}

			 bool print_missings_hotkeys(){
				 if (!TibiaProcess::get_default()->get_is_logged())
					 return false;

				 auto missing_hk = SpellManager::get()->missing_hotkeys();

				 if (!missing_hk.size())
					 return false;

				 std::string missing_kh_text = "";

				 for (auto hk_text : missing_hk)
					 missing_kh_text += "\n" + hk_text.first + " - type: " + GET_TR("spell_type_t_" + std::to_string(hk_text.second));

				 Telerik::WinControls::RadMessageBox::Show(this, "The hotkey of \n " + gcnew String(missing_kh_text.c_str()) + "\n\nwas not gounded in your tibia hotkeys.\nConfigure it before to start", "WARNING", MessageBoxButtons::OKCancel, Telerik::WinControls::RadMessageIcon::Exclamation);
				 return true;
			 }

	private: System::Void radToggleButtonSpellcaster_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (TibiaProcess::get_default()->get_is_logged())
					 show_missing_hk();
	}
	private: System::Void radToggleButtonGeneral_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (TibiaProcess::get_default()->get_is_logged())
					 show_missing_hk();
	}

			 void show_missing_hk(){
				 std::vector<std::pair<std::string, spell_type_t>> missing_hk = SpellManager::get()->missing_hotkeys();

				 std::string missing_kh_text = "";

				 for (auto hk_text : missing_hk)
					 missing_kh_text += "\n" + hk_text.first;

				 if (missing_hk.size())
					 InfoCore::get()->notify("The Missing hotkey: " + missing_kh_text, "Notification", 3);
			 }
			 String^ ToTime(float hours)
			 {
				 auto span = TimeSpan::FromHours(hours);
				 return String::Format("{0}:{1:00}",
					 (int)span.TotalHours, span.Minutes, span.Seconds);
			 }

	private: System::Void menu_restore_Click(System::Object^  sender, System::EventArgs^  e);

	private: System::Void radMenuItem7_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente fechar o sistema? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::Yes){
					 closeAllOtherForms();

					 NeutralManager::get()->close();
				 }
	}

	private: System::Void Panorama_Resize(System::Object^  sender, System::EventArgs^  e) {
				 if (FormWindowState::Minimized == this->WindowState){
					 notifyIcon->Visible = true;
					 notifyIcon->Text = gcnew String(TibiaProcess::get_default()->get_name_char().c_str());
					 notifyIcon->ShowBalloonTip(500);
					 this->Hide();
				 }
	}
	private: System::Void radMenuItem6_Click_1(System::Object^  sender, System::EventArgs^  e) {
				 (gcnew DevelopmentForm)->Show();
	}
	private: System::Void bt_clear_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente fechar o sistema? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::Yes)
					 NeutralManager::get()->clear();

				 managed_util::ManagedControlsHook::get()->updateButtonsByVars();
	}
	private: System::Void MenuItemMissingHotkey_Click(System::Object^  sender, System::EventArgs^  e) {
				 print_missings_hotkeys();
	}
	private: System::Void MenuItemSpanish_Click(System::Object^  sender, System::EventArgs^  e) {
				 LanguageManager::get()->set_current_lang("spanish");
				 GeneralManager::get()->set_currentLang("spanish");
				 update_idiom();
	}
};
}