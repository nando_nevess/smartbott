#pragma once
#include "MonsterManager.h"
#include "HunterManager.h"
#include <iostream>
#include <Windows.h>
#include <memory>
#include "LanguageManager.h"
#include "ManagedUtil.h"
#include "GifManager.h"
#include "GeneralManager.h"
#include "LuaBackground.h"
#include <vcclr.h>
#include "AutoGenerateLoot.h"

namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Telerik::WinControls::UI;

	public ref class Hunter : public Telerik::WinControls::UI::RadForm{

		void load_weapons_drop_down_list(){
			auto items_std = ItemsManager::get()->get_itens();
			array<String^>^ items = gcnew array<String^>(items_std.size());
			int index = 0;
			for (auto it : items_std){
				items[index] = gcnew String(&it[0]);
				index++;
			}
			drop_down_list_target_weapon->BeginUpdate();
			drop_down_list_default_weapon->BeginUpdate();

			drop_down_list_target_weapon->Items->Add("none");
			drop_down_list_default_weapon->Items->Add("none");

			drop_down_list_target_weapon->Items->AddRange(items);
			drop_down_list_default_weapon->Items->AddRange(items);

			drop_down_list_target_weapon->EndUpdate();
			drop_down_list_default_weapon->EndUpdate();
		}

	public:	Hunter(void){
				InitializeComponent();
				update_idiom();
				update_idiom_listbox();

				enabled_updates = true;

				load_weapons_drop_down_list();

				managed_util::setToggleCheckButtonStyle(radToggleButtonHunter);
				this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");

				panel_state = false;

				check_custom_runner->Checked = HunterManager::get()->get_has_keep_distance_hook();
				radPanelContent->Enabled = !radToggleButtonHunter->IsChecked;
				spin_min_point->Value = (int)HunterManager::get()->get_min_points();
				check_wait_pulse->Checked = HunterManager::get()->get_wait_pulse();
				check_target_any_monster->Checked = HunterManager::get()->get_any_monster();

				radPageView2->SelectedPage = radPageView2->Pages[0];
				
				std::string str_item_name = ItemsManager::get()->getItemNameFromId(HunterManager::get()->get_weapon());
				try{
					uint32_t id = boost::lexical_cast<uint32_t>(str_item_name);
					if (id == 0 || id == UINT32_MAX)
						drop_down_list_default_weapon->SelectedIndex = 0;					
					else
						drop_down_list_default_weapon->Text = gcnew String(&str_item_name[0]);					
				}
				catch (...){
					drop_down_list_default_weapon->Text = gcnew String(&str_item_name[0]);
				}

				enabled_updates = false;

				NeutralBot::FastUIPanelController::get()->install_controller(this);
	}

			static void CloseUnique(){
				if (unique)
					unique->Close();
			}
	protected:	~Hunter(){
					managed_util::unsetToggleCheckButtonStyle(radToggleButtonHunter);
					unique = nullptr;
					if (components)
						delete components;
	}
	public: static void HideUnique(){
				if (unique)unique->Hide();
	}

			Telerik::WinControls::UI::RadDropDownList^  txname_monster;
			System::Windows::Forms::PictureBox^  gifPictureBox;
			Telerik::WinControls::UI::RadPanel^  radPanel2;
			Telerik::WinControls::UI::RadPanel^  radPanel5;

			Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
			Telerik::WinControls::UI::RadMenuItem^  bt_save_;
			Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
			Telerik::WinControls::UI::RadMenuItem^  bt_load_;
			Telerik::WinControls::UI::RadPanel^  radPanel4;
			Telerik::WinControls::UI::RadToggleButton^  radToggleButtonHunter;
			Telerik::WinControls::UI::RadPanel^  radPanel3;
	protected: Telerik::WinControls::UI::RadPanel^  radPanelContent;
			   System::Windows::Forms::ImageList^  creaturesImageList;
			   System::Windows::Forms::Label^  label6;
			   Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
			   Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
			   Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
			   Telerik::WinControls::UI::RadButton^  btnew_monster;
			   Telerik::WinControls::UI::RadLabel^  radLabel2;
			   Telerik::WinControls::UI::RadLabel^  radLabel6;
			   Telerik::WinControls::UI::RadLabel^  radLabel5;
			   Telerik::WinControls::UI::RadLabel^  radLabel4;
			   Telerik::WinControls::UI::RadLabel^  radLabel3;
			   Telerik::WinControls::UI::RadLabel^  radLabel8;
			   Telerik::WinControls::UI::RadLabel^  radLabel7;
			   Telerik::WinControls::UI::RadListView^  ListMonster;
			   Telerik::WinControls::UI::RadSpinEditor^  boxmax_hp_monster;
			   Telerik::WinControls::UI::RadSpinEditor^  boxmin_hp_monster;
			   Telerik::WinControls::UI::RadSpinEditor^  boxdistance_monster;
			   Telerik::WinControls::UI::RadDropDownList^  boxshooter_monster;
			   Telerik::WinControls::UI::RadDropDownList^  boxloot_monster;
			   Telerik::WinControls::UI::RadDropDownList^  boxdiagonal_monster;
			   Telerik::WinControls::UI::RadSpinEditor^  trackdranger_monster;
			   Telerik::WinControls::UI::RadSpinEditor^  trackorder_monster;
			   Telerik::WinControls::UI::RadButton^  btdelete;
			   Telerik::WinControls::UI::RadDropDownList^  dropDownGroup;
	private: Telerik::WinControls::UI::RadContextMenu^  radContextMenu1;
	private: Telerik::WinControls::UI::RadContextMenuManager^  radContextMenuManager1;






	private: System::ComponentModel::IContainer^  components;
	private: Telerik::WinControls::UI::RadButton^  rad_button_edit_creature_runner;



	private: Telerik::WinControls::UI::RadCheckBox^  check_custom_runner;
	protected: Telerik::WinControls::UI::RadButton^  radButton2;

	protected: Telerik::WinControls::UI::RadSpinEditor^  spin_point;
	private:
	protected: Telerik::WinControls::UI::RadLabel^  radLabel1;
	protected: Telerik::WinControls::UI::RadSpinEditor^  spin_min_point;
	protected: Telerik::WinControls::UI::RadLabel^  radLabel9;
	private: Telerik::WinControls::UI::RadCheckBox^  check_attack_if_trapped;
	protected:



	protected: Telerik::WinControls::UI::RadSpinEditor^  radSpinMaxRange;
	private:
	protected: Telerik::WinControls::UI::RadSpinEditor^  radSpinMinRange;
	protected: Telerik::WinControls::UI::RadLabel^  radLabel11;
	protected: System::Windows::Forms::Label^  label1;
	private: Telerik::WinControls::UI::RadLabel^  radLabel13;
	protected:
	private: Telerik::WinControls::UI::RadLabel^  radLabel12;
	private: Telerik::WinControls::UI::RadDropDownList^  drop_down_list_default_weapon;
	private: Telerik::WinControls::UI::RadDropDownList^  drop_down_list_target_weapon;



	private: Telerik::WinControls::UI::RadPageView^  radPageView1;
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewCreatures;
	private: Telerik::WinControls::UI::RadPageView^  radPageView2;
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewCreatureSettings;
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewCreatureAdvanced;
	private: Telerik::WinControls::UI::RadPageViewPage^  radPageViewSettings;
	private: Telerik::WinControls::UI::RadMenu^  radMenu1;
	private: Telerik::WinControls::UI::RadCheckBox^  check_attack_if_block_path;

	private: Telerik::WinControls::UI::RadCheckBox^  check_target_any_monster;
	private: Telerik::WinControls::UI::RadCheckBox^  check_wait_pulse;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox4;
	protected: Telerik::WinControls::UI::RadLabel^  radLabel10;
	private:
	protected: Telerik::WinControls::UI::RadLabel^  radLabel15;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListOldMode;
	protected:
	protected: Telerik::WinControls::UI::RadSpinEditor^  SpinEditorPointsToChange;
	private:
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListNewMode;
	protected:
	protected: Telerik::WinControls::UI::RadLabel^  radLabel14;
private: Telerik::WinControls::UI::RadDropDownList^  DropDownListConsiderPath;
protected:
protected: Telerik::WinControls::UI::RadLabel^  radLabel16;
private: Telerik::WinControls::UI::RadDropDownList^  DropDownListConsiderPathTwo;
protected:
protected: Telerik::WinControls::UI::RadLabel^  radLabel17;
private: Telerik::WinControls::UI::RadCheckBox^  CheckBoxPulseTrapped;
private: Telerik::WinControls::UI::RadLabel^  radLabel18;
private: Telerik::WinControls::UI::RadDropDownList^  DropDownListRunnerMode;


protected:
private:
private:
	private:

	protected:
	protected:
	protected:
	private:
	public:
	protected: static Hunter^ unique;

	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Hunter();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew Hunter();
	}	public: static void ReloadForm(){
		unique->Hunter_Load(nullptr, nullptr);
	}

	bool enabled_updates;

#pragma region Windows Form Designer generated code
	void InitializeComponent(void){
		this->components = (gcnew System::ComponentModel::Container());
		Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0",
			L"Creatures"));
		this->label6 = (gcnew System::Windows::Forms::Label());
		this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
		this->gifPictureBox = (gcnew System::Windows::Forms::PictureBox());
		this->txname_monster = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->dropDownGroup = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->btnew_monster = (gcnew Telerik::WinControls::UI::RadButton());
		this->btdelete = (gcnew Telerik::WinControls::UI::RadButton());
		this->trackdranger_monster = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radLabel8 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->boxloot_monster = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->boxmax_hp_monster = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->boxmin_hp_monster = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
		this->boxdiagonal_monster = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->trackorder_monster = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->boxdistance_monster = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->radLabel7 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->spin_point = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->boxshooter_monster = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
		this->label1 = (gcnew System::Windows::Forms::Label());
		this->radSpinMaxRange = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radSpinMinRange = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radLabel11 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->ListMonster = (gcnew Telerik::WinControls::UI::RadListView());
		this->creaturesImageList = (gcnew System::Windows::Forms::ImageList(this->components));
		this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
		this->radPageView1 = (gcnew Telerik::WinControls::UI::RadPageView());
		this->radPageViewCreatures = (gcnew Telerik::WinControls::UI::RadPageViewPage());
		this->radPageView2 = (gcnew Telerik::WinControls::UI::RadPageView());
		this->radPageViewCreatureSettings = (gcnew Telerik::WinControls::UI::RadPageViewPage());
		this->radPageViewCreatureAdvanced = (gcnew Telerik::WinControls::UI::RadPageViewPage());
		this->radLabel18 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->DropDownListRunnerMode = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->check_attack_if_block_path = (gcnew Telerik::WinControls::UI::RadCheckBox());
		this->radLabel12 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->drop_down_list_target_weapon = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->check_attack_if_trapped = (gcnew Telerik::WinControls::UI::RadCheckBox());
		this->radButton2 = (gcnew Telerik::WinControls::UI::RadButton());
		this->radPageViewSettings = (gcnew Telerik::WinControls::UI::RadPageViewPage());
		this->CheckBoxPulseTrapped = (gcnew Telerik::WinControls::UI::RadCheckBox());
		this->DropDownListConsiderPathTwo = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->radLabel17 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->DropDownListConsiderPath = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->radLabel16 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->radGroupBox4 = (gcnew Telerik::WinControls::UI::RadGroupBox());
		this->radLabel10 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->radLabel15 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->DropDownListOldMode = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->SpinEditorPointsToChange = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->DropDownListNewMode = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->radLabel14 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->check_wait_pulse = (gcnew Telerik::WinControls::UI::RadCheckBox());
		this->check_target_any_monster = (gcnew Telerik::WinControls::UI::RadCheckBox());
		this->radLabel13 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->drop_down_list_default_weapon = (gcnew Telerik::WinControls::UI::RadDropDownList());
		this->spin_min_point = (gcnew Telerik::WinControls::UI::RadSpinEditor());
		this->radLabel9 = (gcnew Telerik::WinControls::UI::RadLabel());
		this->rad_button_edit_creature_runner = (gcnew Telerik::WinControls::UI::RadButton());
		this->check_custom_runner = (gcnew Telerik::WinControls::UI::RadCheckBox());
		this->radPanel5 = (gcnew Telerik::WinControls::UI::RadPanel());
		this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
		this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
		this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
		this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
		this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
		this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
		this->radToggleButtonHunter = (gcnew Telerik::WinControls::UI::RadToggleButton());
		this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
		this->radPanelContent = (gcnew Telerik::WinControls::UI::RadPanel());
		this->radContextMenu1 = (gcnew Telerik::WinControls::UI::RadContextMenu(this->components));
		this->radContextMenuManager1 = (gcnew Telerik::WinControls::UI::RadContextMenuManager());
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
		this->radGroupBox1->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->gifPictureBox))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->txname_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownGroup))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnew_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btdelete))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackdranger_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxloot_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxmax_hp_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxmin_hp_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
		this->radGroupBox2->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxdiagonal_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackorder_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxdistance_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_point))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxshooter_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
		this->radGroupBox3->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinMaxRange))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinMinRange))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel11))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListMonster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
		this->radPanel2->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->BeginInit();
		this->radPageView1->SuspendLayout();
		this->radPageViewCreatures->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView2))->BeginInit();
		this->radPageView2->SuspendLayout();
		this->radPageViewCreatureSettings->SuspendLayout();
		this->radPageViewCreatureAdvanced->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel18))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListRunnerMode))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_attack_if_block_path))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->drop_down_list_target_weapon))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_attack_if_trapped))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton2))->BeginInit();
		this->radPageViewSettings->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->CheckBoxPulseTrapped))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListConsiderPathTwo))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel17))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListConsiderPath))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel16))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->BeginInit();
		this->radGroupBox4->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel10))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel15))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListOldMode))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpinEditorPointsToChange))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListNewMode))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel14))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_wait_pulse))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_target_any_monster))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->drop_down_list_default_weapon))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_min_point))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->rad_button_edit_creature_runner))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_custom_runner))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->BeginInit();
		this->radPanel5->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
		this->radPanel4->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonHunter))->BeginInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
		this->radPanel3->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelContent))->BeginInit();
		this->radPanelContent->SuspendLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
		this->SuspendLayout();
		// 
		// label6
		// 
		this->label6->AutoSize = true;
		this->label6->Location = System::Drawing::Point(138, 37);
		this->label6->Name = L"label6";
		this->label6->Size = System::Drawing::Size(15, 13);
		this->label6->TabIndex = 22;
		this->label6->Text = L"~";
		// 
		// radGroupBox1
		// 
		this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
		this->radGroupBox1->Controls->Add(this->gifPictureBox);
		this->radGroupBox1->Controls->Add(this->txname_monster);
		this->radGroupBox1->Controls->Add(this->dropDownGroup);
		this->radGroupBox1->HeaderText = L"";
		this->radGroupBox1->Location = System::Drawing::Point(243, 10);
		this->radGroupBox1->Name = L"radGroupBox1";
		this->radGroupBox1->Size = System::Drawing::Size(263, 68);
		this->radGroupBox1->TabIndex = 37;
		this->radGroupBox1->ThemeName = L"ControlDefault";
		// 
		// gifPictureBox
		// 
		this->gifPictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
		this->gifPictureBox->Location = System::Drawing::Point(5, 9);
		this->gifPictureBox->Name = L"gifPictureBox";
		this->gifPictureBox->Size = System::Drawing::Size(51, 51);
		this->gifPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
		this->gifPictureBox->TabIndex = 54;
		this->gifPictureBox->TabStop = false;
		// 
		// txname_monster
		// 
		this->txname_monster->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
		this->txname_monster->Location = System::Drawing::Point(62, 9);
		this->txname_monster->Name = L"txname_monster";
		this->txname_monster->NullText = L"Name";
		this->txname_monster->Size = System::Drawing::Size(190, 20);
		this->txname_monster->TabIndex = 53;
		this->txname_monster->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::txname_monster_SelectedIndexChanged);
		this->txname_monster->TextChanged += gcnew System::EventHandler(this, &Hunter::txname_monster_TextChanged);
		// 
		// dropDownGroup
		// 
		this->dropDownGroup->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->dropDownGroup->Location = System::Drawing::Point(62, 40);
		this->dropDownGroup->Name = L"dropDownGroup";
		this->dropDownGroup->NullText = L"Group";
		this->dropDownGroup->Size = System::Drawing::Size(190, 20);
		this->dropDownGroup->TabIndex = 52;
		this->dropDownGroup->ThemeName = L"ControlDefault";
		this->dropDownGroup->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::radDropDownList2_SelectedIndexChanged);
		// 
		// btnew_monster
		// 
		this->btnew_monster->Location = System::Drawing::Point(243, 83);
		this->btnew_monster->Name = L"btnew_monster";
		this->btnew_monster->Size = System::Drawing::Size(130, 24);
		this->btnew_monster->TabIndex = 42;
		this->btnew_monster->Text = L"New";
		this->btnew_monster->ThemeName = L"ControlDefault";
		this->btnew_monster->Click += gcnew System::EventHandler(this, &Hunter::btnew_monster_Click_1);
		// 
		// btdelete
		// 
		this->btdelete->Location = System::Drawing::Point(378, 83);
		this->btdelete->Name = L"btdelete";
		this->btdelete->Size = System::Drawing::Size(128, 24);
		this->btdelete->TabIndex = 43;
		this->btdelete->Text = L"Remove";
		this->btdelete->ThemeName = L"ControlDefault";
		this->btdelete->Click += gcnew System::EventHandler(this, &Hunter::btdelete_Click);
		// 
		// trackdranger_monster
		// 
		this->trackdranger_monster->Location = System::Drawing::Point(58, 9);
		this->trackdranger_monster->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, 0 });
		this->trackdranger_monster->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
		this->trackdranger_monster->Name = L"trackdranger_monster";
		this->trackdranger_monster->Size = System::Drawing::Size(50, 20);
		this->trackdranger_monster->TabIndex = 3;
		this->trackdranger_monster->TabStop = false;
		this->trackdranger_monster->ThemeName = L"ControlDefault";
		this->trackdranger_monster->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
		this->trackdranger_monster->ValueChanged += gcnew System::EventHandler(this, &Hunter::trackdranger_monster_ValueChanged);
		// 
		// radLabel8
		// 
		this->radLabel8->Location = System::Drawing::Point(6, 10);
		this->radLabel8->Name = L"radLabel8";
		this->radLabel8->Size = System::Drawing::Size(43, 18);
		this->radLabel8->TabIndex = 50;
		this->radLabel8->Text = L"Danger";
		this->radLabel8->ThemeName = L"ControlDefault";
		// 
		// boxloot_monster
		// 
		this->boxloot_monster->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->boxloot_monster->Location = System::Drawing::Point(68, 7);
		this->boxloot_monster->Name = L"boxloot_monster";
		this->boxloot_monster->Size = System::Drawing::Size(50, 20);
		this->boxloot_monster->TabIndex = 44;
		this->boxloot_monster->ThemeName = L"ControlDefault";
		this->boxloot_monster->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::boxloot_monster_SelectedIndexChanged);
		// 
		// radLabel6
		// 
		this->radLabel6->Location = System::Drawing::Point(6, 7);
		this->radLabel6->Name = L"radLabel6";
		this->radLabel6->Size = System::Drawing::Size(29, 18);
		this->radLabel6->TabIndex = 48;
		this->radLabel6->Text = L"Loot";
		this->radLabel6->ThemeName = L"ControlDefault";
		// 
		// boxmax_hp_monster
		// 
		this->boxmax_hp_monster->Location = System::Drawing::Point(178, 33);
		this->boxmax_hp_monster->Name = L"boxmax_hp_monster";
		this->boxmax_hp_monster->Size = System::Drawing::Size(50, 20);
		this->boxmax_hp_monster->TabIndex = 1;
		this->boxmax_hp_monster->TabStop = false;
		this->boxmax_hp_monster->ThemeName = L"ControlDefault";
		this->boxmax_hp_monster->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 100, 0, 0, 0 });
		this->boxmax_hp_monster->ValueChanged += gcnew System::EventHandler(this, &Hunter::boxmax_hp_monster_ValueChanged);
		// 
		// boxmin_hp_monster
		// 
		this->boxmin_hp_monster->Location = System::Drawing::Point(68, 33);
		this->boxmin_hp_monster->Name = L"boxmin_hp_monster";
		this->boxmin_hp_monster->Size = System::Drawing::Size(49, 20);
		this->boxmin_hp_monster->TabIndex = 0;
		this->boxmin_hp_monster->TabStop = false;
		this->boxmin_hp_monster->ThemeName = L"ControlDefault";
		this->boxmin_hp_monster->ValueChanged += gcnew System::EventHandler(this, &Hunter::boxmin_hp_monster_ValueChanged);
		// 
		// radLabel2
		// 
		this->radLabel2->Location = System::Drawing::Point(6, 34);
		this->radLabel2->Name = L"radLabel2";
		this->radLabel2->Size = System::Drawing::Size(56, 18);
		this->radLabel2->TabIndex = 44;
		this->radLabel2->Text = L"Hp Range";
		this->radLabel2->ThemeName = L"ControlDefault";
		// 
		// radGroupBox2
		// 
		this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
		this->radGroupBox2->Controls->Add(this->boxdiagonal_monster);
		this->radGroupBox2->Controls->Add(this->boxmax_hp_monster);
		this->radGroupBox2->Controls->Add(this->radLabel4);
		this->radGroupBox2->Controls->Add(this->trackorder_monster);
		this->radGroupBox2->Controls->Add(this->boxloot_monster);
		this->radGroupBox2->Controls->Add(this->boxmin_hp_monster);
		this->radGroupBox2->Controls->Add(this->radLabel6);
		this->radGroupBox2->Controls->Add(this->radLabel2);
		this->radGroupBox2->Controls->Add(this->boxdistance_monster);
		this->radGroupBox2->Controls->Add(this->label6);
		this->radGroupBox2->Controls->Add(this->radLabel5);
		this->radGroupBox2->Controls->Add(this->radLabel7);
		this->radGroupBox2->HeaderText = L"";
		this->radGroupBox2->Location = System::Drawing::Point(3, 3);
		this->radGroupBox2->Name = L"radGroupBox2";
		this->radGroupBox2->Size = System::Drawing::Size(236, 87);
		this->radGroupBox2->TabIndex = 38;
		this->radGroupBox2->ThemeName = L"ControlDefault";
		// 
		// boxdiagonal_monster
		// 
		this->boxdiagonal_monster->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->boxdiagonal_monster->Location = System::Drawing::Point(178, 8);
		this->boxdiagonal_monster->Name = L"boxdiagonal_monster";
		this->boxdiagonal_monster->Size = System::Drawing::Size(50, 20);
		this->boxdiagonal_monster->TabIndex = 44;
		this->boxdiagonal_monster->ThemeName = L"ControlDefault";
		this->boxdiagonal_monster->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::boxdiagonal_monster_SelectedIndexChanged);
		// 
		// radLabel4
		// 
		this->radLabel4->Location = System::Drawing::Point(122, 8);
		this->radLabel4->Name = L"radLabel4";
		this->radLabel4->Size = System::Drawing::Size(51, 18);
		this->radLabel4->TabIndex = 46;
		this->radLabel4->Text = L"Diagonal";
		this->radLabel4->ThemeName = L"ControlDefault";
		// 
		// trackorder_monster
		// 
		this->trackorder_monster->Location = System::Drawing::Point(68, 59);
		this->trackorder_monster->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, 0 });
		this->trackorder_monster->Minimum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
		this->trackorder_monster->Name = L"trackorder_monster";
		this->trackorder_monster->Size = System::Drawing::Size(50, 20);
		this->trackorder_monster->TabIndex = 3;
		this->trackorder_monster->TabStop = false;
		this->trackorder_monster->ThemeName = L"ControlDefault";
		this->trackorder_monster->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
		this->trackorder_monster->ValueChanged += gcnew System::EventHandler(this, &Hunter::trackorder_monster_ValueChanged);
		// 
		// boxdistance_monster
		// 
		this->boxdistance_monster->Location = System::Drawing::Point(179, 59);
		this->boxdistance_monster->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
		this->boxdistance_monster->Name = L"boxdistance_monster";
		this->boxdistance_monster->Size = System::Drawing::Size(50, 20);
		this->boxdistance_monster->TabIndex = 2;
		this->boxdistance_monster->TabStop = false;
		this->boxdistance_monster->ThemeName = L"ControlDefault";
		this->boxdistance_monster->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1, 0, 0, 0 });
		this->boxdistance_monster->ValueChanged += gcnew System::EventHandler(this, &Hunter::boxdistance_monster_ValueChanged);
		// 
		// radLabel5
		// 
		this->radLabel5->Location = System::Drawing::Point(123, 60);
		this->radLabel5->Name = L"radLabel5";
		this->radLabel5->Size = System::Drawing::Size(49, 18);
		this->radLabel5->TabIndex = 47;
		this->radLabel5->Text = L"Distance";
		this->radLabel5->ThemeName = L"ControlDefault";
		// 
		// radLabel7
		// 
		this->radLabel7->Location = System::Drawing::Point(7, 60);
		this->radLabel7->Name = L"radLabel7";
		this->radLabel7->Size = System::Drawing::Size(42, 18);
		this->radLabel7->TabIndex = 49;
		this->radLabel7->Text = L"Priority";
		this->radLabel7->ThemeName = L"ControlDefault";
		// 
		// spin_point
		// 
		this->spin_point->Location = System::Drawing::Point(58, 57);
		this->spin_point->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 999999999, 0, 0, 0 });
		this->spin_point->Name = L"spin_point";
		this->spin_point->Size = System::Drawing::Size(50, 20);
		this->spin_point->TabIndex = 51;
		this->spin_point->TabStop = false;
		this->spin_point->ThemeName = L"ControlDefault";
		this->spin_point->ValueChanged += gcnew System::EventHandler(this, &Hunter::spin_point_ValueChanged);
		// 
		// radLabel1
		// 
		this->radLabel1->Location = System::Drawing::Point(6, 58);
		this->radLabel1->Name = L"radLabel1";
		this->radLabel1->Size = System::Drawing::Size(37, 18);
		this->radLabel1->TabIndex = 52;
		this->radLabel1->Text = L"Points";
		this->radLabel1->ThemeName = L"ControlDefault";
		// 
		// boxshooter_monster
		// 
		this->boxshooter_monster->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->boxshooter_monster->Location = System::Drawing::Point(177, 8);
		this->boxshooter_monster->Name = L"boxshooter_monster";
		this->boxshooter_monster->Size = System::Drawing::Size(50, 20);
		this->boxshooter_monster->TabIndex = 45;
		this->boxshooter_monster->ThemeName = L"ControlDefault";
		this->boxshooter_monster->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::boxshooter_monster_SelectedIndexChanged);
		// 
		// radLabel3
		// 
		this->radLabel3->Location = System::Drawing::Point(123, 9);
		this->radLabel3->Name = L"radLabel3";
		this->radLabel3->Size = System::Drawing::Size(46, 18);
		this->radLabel3->TabIndex = 45;
		this->radLabel3->Text = L"Shooter";
		this->radLabel3->ThemeName = L"ControlDefault";
		// 
		// radGroupBox3
		// 
		this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
		this->radGroupBox3->Controls->Add(this->spin_point);
		this->radGroupBox3->Controls->Add(this->label1);
		this->radGroupBox3->Controls->Add(this->radLabel1);
		this->radGroupBox3->Controls->Add(this->radSpinMaxRange);
		this->radGroupBox3->Controls->Add(this->boxshooter_monster);
		this->radGroupBox3->Controls->Add(this->radLabel8);
		this->radGroupBox3->Controls->Add(this->trackdranger_monster);
		this->radGroupBox3->Controls->Add(this->radSpinMinRange);
		this->radGroupBox3->Controls->Add(this->radLabel3);
		this->radGroupBox3->Controls->Add(this->radLabel11);
		this->radGroupBox3->HeaderText = L"";
		this->radGroupBox3->Location = System::Drawing::Point(0, 93);
		this->radGroupBox3->Name = L"radGroupBox3";
		this->radGroupBox3->Size = System::Drawing::Size(242, 88);
		this->radGroupBox3->TabIndex = 39;
		this->radGroupBox3->ThemeName = L"ControlDefault";
		// 
		// label1
		// 
		this->label1->AutoSize = true;
		this->label1->Location = System::Drawing::Point(3, 126);
		this->label1->Name = L"label1";
		this->label1->Size = System::Drawing::Size(96, 13);
		this->label1->TabIndex = 57;
		this->label1->Text = L"Default weapon :";
		// 
		// radSpinMaxRange
		// 
		this->radSpinMaxRange->Location = System::Drawing::Point(177, 32);
		this->radSpinMaxRange->Name = L"radSpinMaxRange";
		this->radSpinMaxRange->Size = System::Drawing::Size(50, 20);
		this->radSpinMaxRange->TabIndex = 51;
		this->radSpinMaxRange->TabStop = false;
		this->radSpinMaxRange->ThemeName = L"ControlDefault";
		this->radSpinMaxRange->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
		this->radSpinMaxRange->ValueChanged += gcnew System::EventHandler(this, &Hunter::radSpinMaxRange_ValueChanged);
		// 
		// radSpinMinRange
		// 
		this->radSpinMinRange->Location = System::Drawing::Point(58, 33);
		this->radSpinMinRange->Name = L"radSpinMinRange";
		this->radSpinMinRange->Size = System::Drawing::Size(50, 20);
		this->radSpinMinRange->TabIndex = 50;
		this->radSpinMinRange->TabStop = false;
		this->radSpinMinRange->ThemeName = L"ControlDefault";
		this->radSpinMinRange->ValueChanged += gcnew System::EventHandler(this, &Hunter::radSpinMinRange_ValueChanged);
		// 
		// radLabel11
		// 
		this->radLabel11->Location = System::Drawing::Point(8, 34);
		this->radLabel11->Name = L"radLabel11";
		this->radLabel11->Size = System::Drawing::Size(38, 18);
		this->radLabel11->TabIndex = 53;
		this->radLabel11->Text = L"Range";
		this->radLabel11->ThemeName = L"ControlDefault";
		// 
		// ListMonster
		// 
		this->ListMonster->AllowEdit = false;
		listViewDetailColumn1->HeaderText = L"Creatures";
		listViewDetailColumn1->MaxWidth = 230;
		listViewDetailColumn1->MinWidth = 233;
		listViewDetailColumn1->Width = 233;
		this->ListMonster->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(1) { listViewDetailColumn1 });
		this->ListMonster->Cursor = System::Windows::Forms::Cursors::Default;
		this->ListMonster->HeaderHeight = 25;
		this->ListMonster->HorizontalScrollState = Telerik::WinControls::UI::ScrollState::AlwaysHide;
		this->ListMonster->ItemSize = System::Drawing::Size(200, 40);
		this->ListMonster->ItemSpacing = -1;
		this->ListMonster->Location = System::Drawing::Point(3, 10);
		this->ListMonster->Name = L"ListMonster";
		this->radContextMenuManager1->SetRadContextMenu(this->ListMonster, this->radContextMenu1);
		this->ListMonster->RightToLeft = System::Windows::Forms::RightToLeft::No;
		this->ListMonster->ShowGridLines = true;
		this->ListMonster->Size = System::Drawing::Size(234, 299);
		this->ListMonster->SmallImageList = this->creaturesImageList;
		this->ListMonster->TabIndex = 43;
		this->ListMonster->ThemeName = L"ControlDefault";
		this->ListMonster->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
		this->ListMonster->SelectedItemChanged += gcnew System::EventHandler(this, &Hunter::ListMonster_SelectedItemChanged);
		this->ListMonster->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Hunter::ListMonster_ItemRemoving);
		this->ListMonster->DragDrop += gcnew System::Windows::Forms::DragEventHandler(this, &Hunter::ListMonster_DragDrop);
		// 
		// creaturesImageList
		// 
		this->creaturesImageList->ColorDepth = System::Windows::Forms::ColorDepth::Depth8Bit;
		this->creaturesImageList->ImageSize = System::Drawing::Size(16, 16);
		this->creaturesImageList->TransparentColor = System::Drawing::Color::Transparent;
		// 
		// radPanel2
		// 
		this->radPanel2->Controls->Add(this->radPageView1);
		this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
		this->radPanel2->Location = System::Drawing::Point(0, 0);
		this->radPanel2->Name = L"radPanel2";
		this->radPanel2->Size = System::Drawing::Size(530, 393);
		this->radPanel2->TabIndex = 47;
		this->radPanel2->Text = L"radPanel2";
		// 
		// radPageView1
		// 
		this->radPageView1->Controls->Add(this->radPageViewCreatures);
		this->radPageView1->Controls->Add(this->radPageViewSettings);
		this->radPageView1->Dock = System::Windows::Forms::DockStyle::Fill;
		this->radPageView1->Location = System::Drawing::Point(0, 0);
		this->radPageView1->Name = L"radPageView1";
		this->radPageView1->SelectedPage = this->radPageViewCreatures;
		this->radPageView1->Size = System::Drawing::Size(530, 393);
		this->radPageView1->TabIndex = 46;
		this->radPageView1->Text = L"radPageView1";
		(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
		// 
		// radPageViewCreatures
		// 
		this->radPageViewCreatures->Controls->Add(this->radPageView2);
		this->radPageViewCreatures->Controls->Add(this->ListMonster);
		this->radPageViewCreatures->Controls->Add(this->radButton2);
		this->radPageViewCreatures->Controls->Add(this->btnew_monster);
		this->radPageViewCreatures->Controls->Add(this->radGroupBox1);
		this->radPageViewCreatures->Controls->Add(this->btdelete);
		this->radPageViewCreatures->ItemSize = System::Drawing::SizeF(64, 28);
		this->radPageViewCreatures->Location = System::Drawing::Point(10, 37);
		this->radPageViewCreatures->Name = L"radPageViewCreatures";
		this->radPageViewCreatures->Size = System::Drawing::Size(509, 345);
		this->radPageViewCreatures->Text = L"Creatures";
		// 
		// radPageView2
		// 
		this->radPageView2->Controls->Add(this->radPageViewCreatureSettings);
		this->radPageView2->Controls->Add(this->radPageViewCreatureAdvanced);
		this->radPageView2->Font = (gcnew System::Drawing::Font(L"Segoe UI", 8.25F));
		this->radPageView2->ForeColor = System::Drawing::SystemColors::ControlDark;
		this->radPageView2->Location = System::Drawing::Point(243, 112);
		this->radPageView2->Name = L"radPageView2";
		this->radPageView2->SelectedPage = this->radPageViewCreatureAdvanced;
		this->radPageView2->Size = System::Drawing::Size(263, 231);
		this->radPageView2->TabIndex = 60;
		this->radPageView2->Text = L"radPageView2";
		(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView2->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
		(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView2->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
		// 
		// radPageViewCreatureSettings
		// 
		this->radPageViewCreatureSettings->Controls->Add(this->radGroupBox2);
		this->radPageViewCreatureSettings->ItemSize = System::Drawing::SizeF(102, 28);
		this->radPageViewCreatureSettings->Location = System::Drawing::Point(10, 37);
		this->radPageViewCreatureSettings->Name = L"radPageViewCreatureSettings";
		this->radPageViewCreatureSettings->Size = System::Drawing::Size(242, 183);
		this->radPageViewCreatureSettings->Text = L"Creature Settings";
		// 
		// radPageViewCreatureAdvanced
		// 
		this->radPageViewCreatureAdvanced->Controls->Add(this->radLabel18);
		this->radPageViewCreatureAdvanced->Controls->Add(this->DropDownListRunnerMode);
		this->radPageViewCreatureAdvanced->Controls->Add(this->check_attack_if_block_path);
		this->radPageViewCreatureAdvanced->Controls->Add(this->radGroupBox3);
		this->radPageViewCreatureAdvanced->Controls->Add(this->radLabel12);
		this->radPageViewCreatureAdvanced->Controls->Add(this->drop_down_list_target_weapon);
		this->radPageViewCreatureAdvanced->Controls->Add(this->check_attack_if_trapped);
		this->radPageViewCreatureAdvanced->ItemSize = System::Drawing::SizeF(66, 28);
		this->radPageViewCreatureAdvanced->Location = System::Drawing::Point(10, 37);
		this->radPageViewCreatureAdvanced->Name = L"radPageViewCreatureAdvanced";
		this->radPageViewCreatureAdvanced->Size = System::Drawing::Size(242, 183);
		this->radPageViewCreatureAdvanced->Text = L"Advanced";
		// 
		// radLabel18
		// 
		this->radLabel18->Location = System::Drawing::Point(6, 47);
		this->radLabel18->Name = L"radLabel18";
		this->radLabel18->Size = System::Drawing::Size(75, 18);
		this->radLabel18->TabIndex = 62;
		this->radLabel18->Text = L"Runner Mode";
		// 
		// DropDownListRunnerMode
		// 
		this->DropDownListRunnerMode->Location = System::Drawing::Point(2, 67);
		this->DropDownListRunnerMode->Name = L"DropDownListRunnerMode";
		this->DropDownListRunnerMode->Size = System::Drawing::Size(118, 20);
		this->DropDownListRunnerMode->TabIndex = 61;
		this->DropDownListRunnerMode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListRunnerMode_SelectedIndexChanged);
		// 
		// check_attack_if_block_path
		// 
		this->check_attack_if_block_path->Location = System::Drawing::Point(125, 69);
		this->check_attack_if_block_path->Name = L"check_attack_if_block_path";
		this->check_attack_if_block_path->Size = System::Drawing::Size(117, 18);
		this->check_attack_if_block_path->TabIndex = 60;
		this->check_attack_if_block_path->Text = L"Attack if block path";
		this->check_attack_if_block_path->CheckStateChanged += gcnew System::EventHandler(this, &Hunter::CheckBoxAttackIfBlockPath_CheckStateChanged);
		// 
		// radLabel12
		// 
		this->radLabel12->Location = System::Drawing::Point(4, 1);
		this->radLabel12->Name = L"radLabel12";
		this->radLabel12->Size = System::Drawing::Size(83, 18);
		this->radLabel12->TabIndex = 59;
		this->radLabel12->Text = L"Target Weapon";
		// 
		// drop_down_list_target_weapon
		// 
		this->drop_down_list_target_weapon->Location = System::Drawing::Point(2, 19);
		this->drop_down_list_target_weapon->Name = L"drop_down_list_target_weapon";
		this->drop_down_list_target_weapon->Size = System::Drawing::Size(118, 20);
		this->drop_down_list_target_weapon->TabIndex = 57;
		this->drop_down_list_target_weapon->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListTargetWeapon_SelectedIndexChanged);
		// 
		// check_attack_if_trapped
		// 
		this->check_attack_if_trapped->Location = System::Drawing::Point(125, 47);
		this->check_attack_if_trapped->Name = L"check_attack_if_trapped";
		this->check_attack_if_trapped->Size = System::Drawing::Size(104, 18);
		this->check_attack_if_trapped->TabIndex = 6;
		this->check_attack_if_trapped->Text = L"Attack if trapped";
		this->check_attack_if_trapped->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Hunter::CheckBoxAttackIfTrapped_ToggleStateChanged);
		// 
		// radButton2
		// 
		this->radButton2->Location = System::Drawing::Point(3, 315);
		this->radButton2->Name = L"radButton2";
		this->radButton2->Size = System::Drawing::Size(234, 26);
		this->radButton2->TabIndex = 45;
		this->radButton2->Text = L"Auto Generate Screen Creatures";
		this->radButton2->ThemeName = L"ControlDefault";
		this->radButton2->Click += gcnew System::EventHandler(this, &Hunter::radButton2_Click);
		// 
		// radPageViewSettings
		// 
		this->radPageViewSettings->Controls->Add(this->CheckBoxPulseTrapped);
		this->radPageViewSettings->Controls->Add(this->DropDownListConsiderPathTwo);
		this->radPageViewSettings->Controls->Add(this->radLabel17);
		this->radPageViewSettings->Controls->Add(this->DropDownListConsiderPath);
		this->radPageViewSettings->Controls->Add(this->radLabel16);
		this->radPageViewSettings->Controls->Add(this->radGroupBox4);
		this->radPageViewSettings->Controls->Add(this->check_wait_pulse);
		this->radPageViewSettings->Controls->Add(this->check_target_any_monster);
		this->radPageViewSettings->Controls->Add(this->radLabel13);
		this->radPageViewSettings->Controls->Add(this->drop_down_list_default_weapon);
		this->radPageViewSettings->Controls->Add(this->spin_min_point);
		this->radPageViewSettings->Controls->Add(this->radLabel9);
		this->radPageViewSettings->Controls->Add(this->rad_button_edit_creature_runner);
		this->radPageViewSettings->Controls->Add(this->check_custom_runner);
		this->radPageViewSettings->ItemSize = System::Drawing::SizeF(56, 28);
		this->radPageViewSettings->Location = System::Drawing::Point(10, 37);
		this->radPageViewSettings->Name = L"radPageViewSettings";
		this->radPageViewSettings->Size = System::Drawing::Size(509, 345);
		this->radPageViewSettings->Text = L"Settings";
		// 
		// CheckBoxPulseTrapped
		// 
		this->CheckBoxPulseTrapped->Location = System::Drawing::Point(247, 107);
		this->CheckBoxPulseTrapped->Name = L"CheckBoxPulseTrapped";
		this->CheckBoxPulseTrapped->Size = System::Drawing::Size(98, 18);
		this->CheckBoxPulseTrapped->TabIndex = 62;
		this->CheckBoxPulseTrapped->Text = L"Pulse if trapped";
		this->CheckBoxPulseTrapped->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Hunter::CheckBoxPulseTrapped_ToggleStateChanged);
		// 
		// DropDownListConsiderPathTwo
		// 
		this->DropDownListConsiderPathTwo->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->DropDownListConsiderPathTwo->Location = System::Drawing::Point(358, 30);
		this->DropDownListConsiderPathTwo->Name = L"DropDownListConsiderPathTwo";
		this->DropDownListConsiderPathTwo->Size = System::Drawing::Size(148, 20);
		this->DropDownListConsiderPathTwo->TabIndex = 72;
		this->DropDownListConsiderPathTwo->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListConsiderPathTwo_SelectedIndexChanged);
		// 
		// radLabel17
		// 
		this->radLabel17->Location = System::Drawing::Point(247, 31);
		this->radLabel17->Name = L"radLabel17";
		this->radLabel17->Size = System::Drawing::Size(105, 18);
		this->radLabel17->TabIndex = 71;
		this->radLabel17->Text = L"Consider Path Two :";
		this->radLabel17->ThemeName = L"ControlDefault";
		// 
		// DropDownListConsiderPath
		// 
		this->DropDownListConsiderPath->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->DropDownListConsiderPath->Location = System::Drawing::Point(358, 7);
		this->DropDownListConsiderPath->Name = L"DropDownListConsiderPath";
		this->DropDownListConsiderPath->Size = System::Drawing::Size(148, 20);
		this->DropDownListConsiderPath->TabIndex = 71;
		this->DropDownListConsiderPath->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListConsiderPath_SelectedIndexChanged);
		// 
		// radLabel16
		// 
		this->radLabel16->Location = System::Drawing::Point(247, 7);
		this->radLabel16->Name = L"radLabel16";
		this->radLabel16->Size = System::Drawing::Size(105, 18);
		this->radLabel16->TabIndex = 70;
		this->radLabel16->Text = L"Consider Path One :";
		this->radLabel16->ThemeName = L"ControlDefault";
		// 
		// radGroupBox4
		// 
		this->radGroupBox4->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
		this->radGroupBox4->Controls->Add(this->radLabel10);
		this->radGroupBox4->Controls->Add(this->radLabel15);
		this->radGroupBox4->Controls->Add(this->DropDownListOldMode);
		this->radGroupBox4->Controls->Add(this->SpinEditorPointsToChange);
		this->radGroupBox4->Controls->Add(this->DropDownListNewMode);
		this->radGroupBox4->Controls->Add(this->radLabel14);
		this->radGroupBox4->HeaderText = L"";
		this->radGroupBox4->Location = System::Drawing::Point(3, 113);
		this->radGroupBox4->Name = L"radGroupBox4";
		this->radGroupBox4->Size = System::Drawing::Size(238, 83);
		this->radGroupBox4->TabIndex = 68;
		// 
		// radLabel10
		// 
		this->radLabel10->Location = System::Drawing::Point(5, 6);
		this->radLabel10->Name = L"radLabel10";
		this->radLabel10->Size = System::Drawing::Size(161, 18);
		this->radLabel10->TabIndex = 64;
		this->radLabel10->Text = L"Points to change attack mode :";
		this->radLabel10->ThemeName = L"ControlDefault";
		// 
		// radLabel15
		// 
		this->radLabel15->Location = System::Drawing::Point(5, 57);
		this->radLabel15->Name = L"radLabel15";
		this->radLabel15->Size = System::Drawing::Size(65, 18);
		this->radLabel15->TabIndex = 66;
		this->radLabel15->Text = L"Old Mode : ";
		this->radLabel15->ThemeName = L"ControlDefault";
		// 
		// DropDownListOldMode
		// 
		this->DropDownListOldMode->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->DropDownListOldMode->Location = System::Drawing::Point(76, 57);
		this->DropDownListOldMode->Name = L"DropDownListOldMode";
		this->DropDownListOldMode->Size = System::Drawing::Size(157, 20);
		this->DropDownListOldMode->TabIndex = 66;
		this->DropDownListOldMode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListOldMode_SelectedIndexChanged);
		// 
		// SpinEditorPointsToChange
		// 
		this->SpinEditorPointsToChange->Location = System::Drawing::Point(172, 5);
		this->SpinEditorPointsToChange->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999999, 0, 0, 0 });
		this->SpinEditorPointsToChange->Name = L"SpinEditorPointsToChange";
		this->SpinEditorPointsToChange->Size = System::Drawing::Size(61, 20);
		this->SpinEditorPointsToChange->TabIndex = 63;
		this->SpinEditorPointsToChange->TabStop = false;
		this->SpinEditorPointsToChange->ThemeName = L"ControlDefault";
		this->SpinEditorPointsToChange->ValueChanged += gcnew System::EventHandler(this, &Hunter::SpinEditorPointsToChange_ValueChanged);
		// 
		// DropDownListNewMode
		// 
		this->DropDownListNewMode->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
		this->DropDownListNewMode->Location = System::Drawing::Point(76, 31);
		this->DropDownListNewMode->Name = L"DropDownListNewMode";
		this->DropDownListNewMode->Size = System::Drawing::Size(157, 20);
		this->DropDownListNewMode->TabIndex = 65;
		this->DropDownListNewMode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListNewMode_SelectedIndexChanged);
		// 
		// radLabel14
		// 
		this->radLabel14->Location = System::Drawing::Point(5, 31);
		this->radLabel14->Name = L"radLabel14";
		this->radLabel14->Size = System::Drawing::Size(70, 18);
		this->radLabel14->TabIndex = 65;
		this->radLabel14->Text = L"New Mode : ";
		this->radLabel14->ThemeName = L"ControlDefault";
		// 
		// check_wait_pulse
		// 
		this->check_wait_pulse->Location = System::Drawing::Point(247, 83);
		this->check_wait_pulse->Name = L"check_wait_pulse";
		this->check_wait_pulse->Size = System::Drawing::Size(73, 18);
		this->check_wait_pulse->TabIndex = 62;
		this->check_wait_pulse->Text = L"Wait pulse";
		this->check_wait_pulse->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Hunter::radCheckBox1_ToggleStateChanged_1);
		// 
		// check_target_any_monster
		// 
		this->check_target_any_monster->Location = System::Drawing::Point(247, 59);
		this->check_target_any_monster->Name = L"check_target_any_monster";
		this->check_target_any_monster->Size = System::Drawing::Size(117, 18);
		this->check_target_any_monster->TabIndex = 61;
		this->check_target_any_monster->Text = L"Target any monster";
		this->check_target_any_monster->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Hunter::check_target_any_monster_ToggleStateChanged_1);
		// 
		// radLabel13
		// 
		this->radLabel13->Location = System::Drawing::Point(3, 7);
		this->radLabel13->Name = L"radLabel13";
		this->radLabel13->Size = System::Drawing::Size(87, 18);
		this->radLabel13->TabIndex = 60;
		this->radLabel13->Text = L"Default Weapon";
		// 
		// drop_down_list_default_weapon
		// 
		this->drop_down_list_default_weapon->Location = System::Drawing::Point(96, 7);
		this->drop_down_list_default_weapon->Name = L"drop_down_list_default_weapon";
		this->drop_down_list_default_weapon->Size = System::Drawing::Size(145, 20);
		this->drop_down_list_default_weapon->TabIndex = 58;
		this->drop_down_list_default_weapon->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Hunter::DropDownListDefaultWeapon_SelectedIndexChanged);
		// 
		// spin_min_point
		// 
		this->spin_min_point->Location = System::Drawing::Point(96, 29);
		this->spin_min_point->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 99999999, 0, 0, 0 });
		this->spin_min_point->Name = L"spin_min_point";
		this->spin_min_point->Size = System::Drawing::Size(145, 20);
		this->spin_min_point->TabIndex = 53;
		this->spin_min_point->TabStop = false;
		this->spin_min_point->ThemeName = L"ControlDefault";
		this->spin_min_point->ValueChanged += gcnew System::EventHandler(this, &Hunter::spin_min_point_ValueChanged);
		// 
		// radLabel9
		// 
		this->radLabel9->Location = System::Drawing::Point(3, 30);
		this->radLabel9->Name = L"radLabel9";
		this->radLabel9->Size = System::Drawing::Size(85, 18);
		this->radLabel9->TabIndex = 54;
		this->radLabel9->Text = L"Requerid points";
		this->radLabel9->ThemeName = L"ControlDefault";
		// 
		// rad_button_edit_creature_runner
		// 
		this->rad_button_edit_creature_runner->Location = System::Drawing::Point(3, 83);
		this->rad_button_edit_creature_runner->Name = L"rad_button_edit_creature_runner";
		this->rad_button_edit_creature_runner->Size = System::Drawing::Size(238, 24);
		this->rad_button_edit_creature_runner->TabIndex = 5;
		this->rad_button_edit_creature_runner->Text = L"Custom Creature Runner";
		this->rad_button_edit_creature_runner->Click += gcnew System::EventHandler(this, &Hunter::radButton1_Click);
		// 
		// check_custom_runner
		// 
		this->check_custom_runner->Location = System::Drawing::Point(3, 59);
		this->check_custom_runner->Name = L"check_custom_runner";
		this->check_custom_runner->Size = System::Drawing::Size(115, 18);
		this->check_custom_runner->TabIndex = 5;
		this->check_custom_runner->Text = L"Use custom runner";
		this->check_custom_runner->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Hunter::check_custom_runner_ToggleStateChanged);
		// 
		// radPanel5
		// 
		this->radPanel5->Controls->Add(this->radMenu1);
		this->radPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
		this->radPanel5->Location = System::Drawing::Point(100, 0);
		this->radPanel5->Name = L"radPanel5";
		this->radPanel5->Size = System::Drawing::Size(430, 22);
		this->radPanel5->TabIndex = 2;
		this->radPanel5->Text = L"radPanel5";
		// 
		// radMenu1
		// 
		this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
		this->radMenu1->Location = System::Drawing::Point(0, 0);
		this->radMenu1->Name = L"radMenu1";
		this->radMenu1->Size = System::Drawing::Size(430, 20);
		this->radMenu1->TabIndex = 0;
		this->radMenu1->Text = L"radMenu1";
		// 
		// radMenuItem1
		// 
		this->radMenuItem1->AccessibleDescription = L"Menu";
		this->radMenuItem1->AccessibleName = L"Menu";
		this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
			this->bt_save_, this->radMenuSeparatorItem1,
				this->bt_load_
		});
		this->radMenuItem1->Name = L"radMenuItem1";
		this->radMenuItem1->Text = L"Menu";
		// 
		// bt_save_
		// 
		this->bt_save_->AccessibleDescription = L"Save";
		this->bt_save_->AccessibleName = L"Save";
		this->bt_save_->Name = L"bt_save_";
		this->bt_save_->Text = L"Save";
		this->bt_save_->Click += gcnew System::EventHandler(this, &Hunter::bt_save__Click);
		// 
		// radMenuSeparatorItem1
		// 
		this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
		this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
		this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
		this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
		// 
		// bt_load_
		// 
		this->bt_load_->AccessibleDescription = L"Load";
		this->bt_load_->AccessibleName = L"Load";
		this->bt_load_->Name = L"bt_load_";
		this->bt_load_->Text = L"Load";
		this->bt_load_->Click += gcnew System::EventHandler(this, &Hunter::bt_load__Click);
		// 
		// radPanel4
		// 
		this->radPanel4->Controls->Add(this->radToggleButtonHunter);
		this->radPanel4->Dock = System::Windows::Forms::DockStyle::Left;
		this->radPanel4->Location = System::Drawing::Point(0, 0);
		this->radPanel4->Name = L"radPanel4";
		this->radPanel4->Size = System::Drawing::Size(100, 22);
		this->radPanel4->TabIndex = 1;
		this->radPanel4->Text = L"radPanel4";
		// 
		// radToggleButtonHunter
		// 
		this->radToggleButtonHunter->Dock = System::Windows::Forms::DockStyle::Fill;
		this->radToggleButtonHunter->Location = System::Drawing::Point(0, 0);
		this->radToggleButtonHunter->Name = L"radToggleButtonHunter";
		this->radToggleButtonHunter->Size = System::Drawing::Size(100, 22);
		this->radToggleButtonHunter->TabIndex = 1;
		this->radToggleButtonHunter->Text = L"radToggleButton1";
		this->radToggleButtonHunter->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Hunter::radToggleButtonHunter_ToggleStateChanged);
		// 
		// radPanel3
		// 
		this->radPanel3->Controls->Add(this->radPanel5);
		this->radPanel3->Controls->Add(this->radPanel4);
		this->radPanel3->Dock = System::Windows::Forms::DockStyle::Top;
		this->radPanel3->Location = System::Drawing::Point(0, 0);
		this->radPanel3->Name = L"radPanel3";
		this->radPanel3->Size = System::Drawing::Size(530, 22);
		this->radPanel3->TabIndex = 0;
		this->radPanel3->Text = L"radPanel3";
		// 
		// radPanelContent
		// 
		this->radPanelContent->Controls->Add(this->radPanel2);
		this->radPanelContent->Dock = System::Windows::Forms::DockStyle::Fill;
		this->radPanelContent->Location = System::Drawing::Point(0, 22);
		this->radPanelContent->Name = L"radPanelContent";
		this->radPanelContent->Size = System::Drawing::Size(530, 393);
		this->radPanelContent->TabIndex = 48;
		this->radPanelContent->Text = L"radPanel1";
		// 
		// radContextMenu1
		// 
		this->radContextMenu1->DropDownOpening += gcnew System::ComponentModel::CancelEventHandler(this, &Hunter::radContextMenu1_DropDownOpening);
		// 
		// Hunter
		// 
		this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
		this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
		this->AutoSize = true;
		this->ClientSize = System::Drawing::Size(530, 415);
		this->Controls->Add(this->radPanelContent);
		this->Controls->Add(this->radPanel3);
		this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
		this->MaximizeBox = false;
		this->Name = L"Hunter";
		// 
		// 
		// 
		this->RootElement->ApplyShapeToControl = true;
		this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
		this->Text = L",";
		this->ThemeName = L"ControlDefault";
		this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Hunter::Hunter_FormClosing);
		this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Hunter::Hunter_FormClosed);
		this->Load += gcnew System::EventHandler(this, &Hunter::Hunter_Load);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
		this->radGroupBox1->ResumeLayout(false);
		this->radGroupBox1->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->gifPictureBox))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->txname_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownGroup))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btnew_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->btdelete))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackdranger_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxloot_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxmax_hp_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxmin_hp_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
		this->radGroupBox2->ResumeLayout(false);
		this->radGroupBox2->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxdiagonal_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->trackorder_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxdistance_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_point))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->boxshooter_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
		this->radGroupBox3->ResumeLayout(false);
		this->radGroupBox3->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinMaxRange))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinMinRange))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel11))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListMonster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
		this->radPanel2->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->EndInit();
		this->radPageView1->ResumeLayout(false);
		this->radPageViewCreatures->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView2))->EndInit();
		this->radPageView2->ResumeLayout(false);
		this->radPageViewCreatureSettings->ResumeLayout(false);
		this->radPageViewCreatureAdvanced->ResumeLayout(false);
		this->radPageViewCreatureAdvanced->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel18))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListRunnerMode))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_attack_if_block_path))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->drop_down_list_target_weapon))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_attack_if_trapped))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton2))->EndInit();
		this->radPageViewSettings->ResumeLayout(false);
		this->radPageViewSettings->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->CheckBoxPulseTrapped))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListConsiderPathTwo))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel17))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListConsiderPath))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel16))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->EndInit();
		this->radGroupBox4->ResumeLayout(false);
		this->radGroupBox4->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel10))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel15))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListOldMode))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpinEditorPointsToChange))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListNewMode))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel14))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_wait_pulse))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_target_any_monster))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->drop_down_list_default_weapon))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_min_point))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->rad_button_edit_creature_runner))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_custom_runner))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->EndInit();
		this->radPanel5->ResumeLayout(false);
		this->radPanel5->PerformLayout();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
		this->radPanel4->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonHunter))->EndInit();
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
		this->radPanel3->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanelContent))->EndInit();
		this->radPanelContent->ResumeLayout(false);
		(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
		this->ResumeLayout(false);

	}
#pragma endregion

	void additemonview(String^ name, String^ min_hp, String^ max_hp,
		bool shooter, bool loot, bool diagonal, String^ distance, String^ order, String^ group, String^ danger);

	void loadItemOnView();
	void loadAll();
	System::Void btnew_monster_Click_1(System::Object^  sender, System::EventArgs^  e);
	System::Void btdelete_Click(System::Object^  sender, System::EventArgs^  e);
	System::Void ListMonster_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
	System::Void Hunter_Load(System::Object^  sender, System::EventArgs^  e);
	int GetCurrentGroup();
	void block_input(bool state);
	TargetPtr getSelectedCreature();
	std::shared_ptr<TargetNonShared> getSelectedCreatureAtGroup();

	System::Void txname_monster_TextChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void boxmin_hp_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void boxmax_hp_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void boxshooter_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	System::Void boxloot_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	System::Void boxdiagonal_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	System::Void trackorder_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void trackdranger_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void trackgroup_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void boxdistance_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void radDropDownList2_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	System::Void ListMonster_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e);
	System::Void radContextMenu1_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e);

	void update_with_selected();

	bool save_script(std::string file_name);
	bool load_script(std::string file_name);

	void UpdateGif();

	System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e);
	System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e);

	System::Void txname_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);

	Telerik::WinControls::UI::RadMenuItem^ ItemDuplicate;

	void creatContextMenu();

	System::Void MenuDuplicate_Click(Object^ sender, EventArgs^ e);
	void update_idiom_listbox();
	void update_idiom();
	bool panel_state = false;

	private: System::Void bt_adv_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void DropDownListLureType_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	private: System::Void check_target_any_monster_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void radMenuItem2_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void check_custom_runner_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void radToggleButtonHunter_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void Hunter_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
				 check_remove_duplicates(false);
	}

			 void check_remove_duplicates(bool remove_from_interface){
				 std::vector<std::string> duplicated = HunterManager::get()->get_duplicated_row_name();
				 if (duplicated.size()){
					 Telerik::WinControls::RadMessageBox::Show(this, "Duplicated Creatures will be removed, just one creature per name.", "Warning!");
				 }
				 for (auto dup : duplicated){
					 String^ id = gcnew String(dup.c_str());
					 HunterManager::get()->delete_id(dup);
					 if (!remove_from_interface)
						 continue;
					 Telerik::WinControls::UI::ListViewDataItem^ _found = nullptr;
					 for each(Telerik::WinControls::UI::ListViewDataItem^ item in ListMonster->Items){
						 if ((String^)item->Value == id){
							 _found = item;
							 break;
						 }
					 }
					 if (_found != nullptr){
						 ListMonster->Items->Remove(_found);
					 }
				 }

			 }

	private: System::Void Hunter_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (GeneralManager::get()->get_practice_mode()){
					 this->Hide();
					 e->Cancel = true;
					 return;
				 }
	}
	private: System::Void radButton2_Click(System::Object^  sender, System::EventArgs^  e) {
				 NeutralBot::AutoGenerateLoot^ tempForm = gcnew NeutralBot::AutoGenerateLoot(2);
				 tempForm->ShowDialog();
				 bool check_exist = true;
				 if (tempForm->comfirmed){
					 for each(String^ item in tempForm->selectedItems){
						 additemonview(item, "0", "100", false, true, false, "1", "1", "Group 0", "1");

						 ListMonster->SelectedIndex = ListMonster->Items->Count - 1;
					 }
				 }
	}
	private: System::Void spin_point_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (!enabled_updates)
					 return;
				 TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
				 if (!creature_info)
					 return;
				 creature_info->set_point((int)spin_point->Value);
	}
	private: System::Void spin_min_point_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 HunterManager::get()->set_min_points((uint32_t)spin_min_point->Value);
	}
	private: System::Void CheckBoxAttackIfTrapped_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (!enabled_updates)
					 return;
				 TargetPtr creature_info = getSelectedCreature();
				 if (!creature_info)
					 return;

				 creature_info->set_attack_if_trapped(check_attack_if_trapped->Checked);
	}
	private: System::Void SpinEditorDistanceConsiderNear_ValueChanged(System::Object^  sender, System::EventArgs^  e) {

	}
	private: System::Void radSpinMinRange_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (!enabled_updates)
					 return;
				 TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
				 if (!creature_info)
					 return;
				 creature_info->set_target_range_min((int)radSpinMinRange->Value);
	}
	private: System::Void radSpinMaxRange_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (!enabled_updates)
					 return;
				 TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
				 if (!creature_info)
					 return;
				 creature_info->set_target_range_max((int)radSpinMaxRange->Value);
	}
	private: System::Void radCheckBox1_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 //HunterManager::get()->set_wait_pulse(radCheckBox1->Checked);
	}
	private: System::Void DropDownListDefaultWeapon_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 uint32_t item_id = ItemsManager::get()->getitem_idFromName(managed_util::fromSS(drop_down_list_default_weapon->Text));
				 if (item_id == UINT32_MAX || item_id <= 0){
					 HunterManager::get()->set_weapon(0);
					 return;
				 }

				 HunterManager::get()->set_weapon(item_id);
	}
	private: System::Void DropDownListRunnerMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				if (!enabled_updates)
					return;

				TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
				if (!creature_info)
					return;

				int index = DropDownListRunnerMode->SelectedIndex;
				creature_info->set_move_mode_type((move_mode_t)index);
	}
	private: System::Void DropDownListTargetWeapon_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
				 if (!creature_info)
					 return;

				 uint32_t item_id = ItemsManager::get()->getitem_idFromName(managed_util::fromSS(drop_down_list_target_weapon->Text));
				 if (item_id == UINT32_MAX || item_id <= 0){
					 creature_info->set_weapon(0);
					 return;
				 }

				 creature_info->set_weapon(item_id);
	}
	private: System::Void CheckBoxAttackIfBlockPath_CheckStateChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 TargetPtr creature_info = getSelectedCreature();
				 if (!creature_info)
					 return;

				 creature_info->set_attack_if_block_path(check_attack_if_block_path->Checked);
	}
	private: System::Void radCheckBox1_ToggleStateChanged_1(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (!enabled_updates)
					 return;

				 HunterManager::get()->set_wait_pulse(check_wait_pulse->Checked);
	}
	private: System::Void check_target_any_monster_ToggleStateChanged_1(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (!enabled_updates)
					 return;

				 HunterManager::get()->set_any_monster(check_target_any_monster->Checked);
	}
	private: System::Void ListMonster_DragDrop(System::Object^  sender, System::Windows::Forms::DragEventArgs^  e) {
	}
			 void loadChangeMode(){
				 int new_mode = HunterManager::get()->get_new_mode() - 1;
				 int old_mode = HunterManager::get()->get_old_mode() - 1;

				 int path_index = HunterManager::get()->get_path_index();
				 int path_index_two = HunterManager::get()->get_path_index_two();

				 for each (auto item in DropDownListConsiderPath->Items){
					 int item_value = (int)item->Value;
					 if (item_value == path_index)
						 DropDownListConsiderPath->SelectedItem = item;
				 }

				 for each (auto item in DropDownListConsiderPathTwo->Items){
					 int item_value = (int)item->Value;
					 if (item_value == path_index_two)
						 DropDownListConsiderPathTwo->SelectedItem = item;
				 }
				
				 DropDownListNewMode->SelectedIndex = new_mode;
				 DropDownListOldMode->SelectedIndex = old_mode;

				 SpinEditorPointsToChange->Value = HunterManager::get()->get_min_points_to_change();
			 }
	private: System::Void DropDownListNewMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 int new_mode = DropDownListNewMode->SelectedIndex + 1;
				 HunterManager::get()->set_new_mode((attack_mode_t)new_mode);
	}
	private: System::Void DropDownListOldMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 int old_mode = DropDownListOldMode->SelectedIndex + 1;
				 HunterManager::get()->set_old_mode((attack_mode_t)old_mode);
	}
	private: System::Void SpinEditorPointsToChange_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 HunterManager::get()->set_min_points_to_change((uint32_t)SpinEditorPointsToChange->Value);
	}
	private: System::Void DropDownListConsiderPath_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (!enabled_updates)
					 return;

				 int path_index = (int)DropDownListConsiderPath->SelectedItem->Value;
				 HunterManager::get()->set_path_index(path_index);
	}
private: System::Void DropDownListConsiderPathTwo_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
			 if (!enabled_updates)
				 return;

			 int path_index = (int)DropDownListConsiderPathTwo->SelectedItem->Value;
			 HunterManager::get()->set_path_index_two(path_index);
}
private: System::Void CheckBoxPulseTrapped_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
			 if (!enabled_updates)
				 return;

			 HunterManager::get()->set_pulse_if_trapped(CheckBoxPulseTrapped->Checked);			 
}

};
}
