#pragma once 
#include <boost\asio.hpp>
#include <boost\bind.hpp>
#include <boost\thread.hpp>

#include <iostream>
#include <stdint.h>
#include <string>

#include <boost\chrono.hpp>
#include <functional>
#include <vector>
#include <memory>
#include <mutex>

class Client : public std::enable_shared_from_this<Client> {
	std::shared_ptr<boost::asio::ip::tcp::socket> _my_socket;
	boost::asio::io_service& _io_service;

	char recv_buffer[100000];
	std::string send_buffer;
	std::mutex mtx;

	bool is_sending = false;
	bool is_recving = false;
	bool is_close = false;

	std::string current_server_ip = "";
	uint16_t current_server_port = 0;
	
	bool connection_error = false;
	bool is_connecting = false;
public:
	Client(boost::asio::io_service& _service) : _io_service(_service){

	}

	void check_send_data();

	void set_connect(std::string ip, uint16_t port);

	void connect(bool lock = false);

	void on_connect(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder, const boost::system::error_code& error);

	void check_set_recv();

	void add_send_data(std::shared_ptr<std::string> datadata);

	void close();

	void on_recv(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder,
		const boost::system::error_code& error, const size_t bytes_transferred);

	void on_send(std::shared_ptr<boost::asio::ip::tcp::socket> socket_holder,
		const boost::system::error_code& error, const size_t bytes_transferred,
		std::shared_ptr<std::string> _data_holder);
};