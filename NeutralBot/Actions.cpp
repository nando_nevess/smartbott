#include "Core\Actions.h"
#include "Core\Pathfinder.h"

BaseActions gBaseActions;
void BaseActions::move_item_container_to_coordinate(ContainerPtr container, uint32_t slot_index, Coordinate coord, uint32_t count){

}
void BaseActions::move_item_container_to_container(ContainerPtr container, uint32_t slot_index_src, ContainerPtr container_dest, uint32_t slot_index_dest, uint32_t count){

}
void BaseActions::move_item_container_to_slot(ContainerPtr container, uint32_t slot_index, invetory_slot_t slot, uint32_t count){

}

void BaseActions::move_item_coordinate_to_container(Coordinate coord, ContainerPtr container, uint32_t slot_index, uint32_t count){

}
void BaseActions::move_item_coordinate_to_slot(Coordinate coord, uint32_t slot_index, invetory_slot_t slot, uint32_t count){

}
void BaseActions::move_item_coordinate_to_coordinate(Coordinate coord, Coordinate coord_target){

}

void BaseActions::move_item_slot_to_slot(invetory_slot_t slot, invetory_slot_t slot_dest, uint32_t count){

}

void BaseActions::move_item_slot_to_coordinate(invetory_slot_t slot, Coordinate coord, uint32_t count){

}
void BaseActions::move_item_slot_to_container(invetory_slot_t slot, ContainerPtr container, uint32_t slot_index, uint32_t count){

}

void BaseActions::use_item_on_inventory(invetory_slot_t slot, uint32_t item_id/*ignore item id*/){

}
void BaseActions::use_item_on_container(ContainerPtr container, uint32_t slot_index, uint32_t item_id/*ignore item id*/){

}
void BaseActions::use_item_on_coordinate(Coordinate coordinate, uint32_t item_id/*ignore item id*/){

}

void BaseActions::use_crosshair_item_on_inventory(invetory_slot_t slot, uint32_t item_id/*ignore item id*/){

}
void BaseActions::use_crosshair_item_on_coordinate(Coordinate coord, uint32_t item_id/*ignore item id*/){

}
void BaseActions::use_crosshair_item_on_container(ContainerPtr container, uint32_t slot_index, uint32_t item_id /*ignore item id*/){

}


std::shared_ptr<boost::mutex::scoped_lock> BaseActions::lock_scope(){
	return std::shared_ptr<boost::mutex::scoped_lock>(new boost::mutex::scoped_lock(mtx_access));
}
