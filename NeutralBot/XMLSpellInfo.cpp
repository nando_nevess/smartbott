#include "XMLSpellInfo.h"


XMLSpellInfo::XMLSpellInfo()
{
}

XMLSpellInfo::XMLSpellInfo(std::string name, std::string cast, int min_hp, int max_hp, int mana, int level, int magicLevel, bool premium, int category, int type, int soul, int range, int cooldownCategory, int cooldownSpell, int cooldownId)
{
	set_name(name);
	set_min_hp(min_hp);
	set_max_hp(max_hp);
	set_cast(cast);
	set_name(name);
	set_level(level);
	set_magicLevel(magicLevel);
	set_premium(premium);
	set_category(category);
	set_type(type);
	set_soul(soul);
	set_range(range);
	set_cooldownCategory(cooldownCategory);
	set_cooldownSpell(cooldownSpell);
	set_cooldownId(cooldownId);
}

void XMLSpellInfo::set_category(int num){
	if (num == XMLCategory::Attack) {
		category = XMLCategory::Attack;
	}
	else if (num == XMLCategory::Healing) {
		category = XMLCategory::Healing;
	}
	else if (num == XMLCategory::Special) {
		category = XMLCategory::Special;
	}
	else if (num == XMLCategory::Support) {
		category = XMLCategory::Support;
	}
}

int XMLSpellInfo::get_category(){
	return category;
}

void XMLSpellInfo::set_type(int num) {
	if (num == XMLspell_type::Area) {
		type = XMLspell_type::Area;
	}
	else if (num == XMLspell_type::Instant) {
		type = XMLspell_type::Instant;
	}
	else if (num == XMLspell_type::Missile) {
		type = XMLspell_type::Missile;
	}
	else if (num == XMLspell_type::Strike) {
		type = XMLspell_type::Strike;
	}
}

int XMLSpellInfo::get_type() {
	return type;
}