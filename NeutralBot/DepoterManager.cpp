#include "DepoterManager.h"
#include "Core\Actions.h"

std::string DepotItem::get_itemName() {
	return itemName;
}
void DepotItem::set_itemName(std::string itemName) {
	this->itemName = itemName;

	int item_id = ItemsManager::get()->getitem_idFromName(itemName);
	if (item_id)
		this->item_id = item_id;
}
int DepotItem::get_item_id() {
	return item_id;
}

std::string DepotContainer::requestNewitem_id(){
	int id = 0;
	std::string current_id = "item" + std::to_string(id);

	while (mapDepotItem.find(current_id) != mapDepotItem.end()){
		id++;
		current_id = "item" + std::to_string(id);
	}

	mapDepotItem[current_id] = std::shared_ptr<DepotItem>(new DepotItem);
	mapDepotItem[current_id]->set_itemName("none");
	return current_id;
}
std::string DepotContainer::addDepotItem(std::string item, uint32_t maxqty, uint32_t minqty){
	std::string id_ = requestNewitem_id();
	mapDepotItem[id_]->set_itemName(item);
	mapDepotItem[id_]->set_maxqty(maxqty);
	mapDepotItem[id_]->set_minqty(minqty);
	return id_;
}
void DepotContainer::removeDepotItem(std::string id_){
	auto it = std::find_if(mapDepotItem.begin(), mapDepotItem.end(), [&](std::pair<std::string, std::shared_ptr<DepotItem>> info_pair){
		return _stricmp(&id_[0], &info_pair.first[0]) == 0;
	});
	
	if (it != mapDepotItem.end())
		mapDepotItem.erase(id_);
}

int DepotContainer::get_item_id() {
	return this->Containeritem_id;
}
void DepotContainer::set_ContainerName(std::string ContainerName) {
	this->ContainerName = ContainerName;
	int searchitem_id = ItemsManager::get()->getitem_idFromName(ContainerName);

	if (searchitem_id > 0)
		this->Containeritem_id = searchitem_id;
}
std::string DepotContainer::get_ContainerName() {
	
	return ItemsManager::get()->getItemNameFromId(Containeritem_id);
}
std::map<std::string, std::shared_ptr<DepotItem>> DepotContainer::getMapDepoItens(){
	return mapDepotItem;
}
std::shared_ptr<DepotItem> DepotContainer::getDepotItemById(std::string item) {
	auto depotItem = std::find_if(mapDepotItem.begin(), mapDepotItem.end(), [&](std::pair<std::string, std::shared_ptr<DepotItem>> info_pair){
		return _stricmp(&item[0], &info_pair.first[0]) == 0;
	});

	if (depotItem == mapDepotItem.end())
		return 0;

	return depotItem->second;
}

Json::Value DepotContainer::parse_class_to_json() {
	Json::Value depoterIdClass;
	Json::Value depoterItemList;

	depoterIdClass["ContainerName"] = this->ContainerName;
	depoterIdClass["Containeritem_id"] = this->Containeritem_id;

	for (auto it : mapDepotItem){
		Json::Value depoitItem;

		depoitItem["itemName"] = it.second->get_itemName();
		depoitItem["maxqty"] = it.second->get_maxqty();
		depoitItem["minqty"] = it.second->get_minqty();
		depoitItem["item_id"] = it.second->get_item_id();

		depoterItemList[it.first] = depoitItem;
	}

	depoterIdClass["depoterList"] = depoterItemList;
	return depoterIdClass;
}
void DepotContainer::parse_json_to_class(Json::Value jsonObject) {
	if (!jsonObject["ContainerName"].empty() || !jsonObject["ContainerName"].isNull())
		this->ContainerName = jsonObject["ContainerName"].asString();

	if (!jsonObject["ContaineritemId"].empty() && !jsonObject["ContaineritemId"].isNull())
		this->Containeritem_id = jsonObject["ContaineritemId"].asInt();
	else if (!jsonObject["Containeritem_id"].empty() && !jsonObject["Containeritem_id"].isNull())
		this->Containeritem_id = jsonObject["Containeritem_id"].asInt();

	if (!jsonObject["depoterList"].empty() || !jsonObject["depoterList"].isNull()){
		Json::Value depoterItemList = jsonObject["depoterList"];
		Json::Value::Members members = depoterItemList.getMemberNames();

		for (auto member : members){
			Json::Value depotItemJson = depoterItemList[member];
			std::shared_ptr<DepotItem> depotItem(new DepotItem);

			depotItem->set_itemName(depotItemJson["itemName"].asString());
			depotItem->set_maxqty(depotItemJson["maxqty"].asInt());
			depotItem->set_minqty(depotItemJson["minqty"].asInt());
			mapDepotItem[member] = depotItem;
		}
	}
}

std::shared_ptr<DepoterManager> DepoterManager::mDepoterManager;

DepoterManager::DepoterManager(){
	use_depot_old_style = true;
	requestNewDepotContainer();
}

void DepoterManager::reset(){
	mDepoterManager = std::shared_ptr<DepoterManager>(new DepoterManager);
}
void DepoterManager::clear(){
	ContainerName = "";
	mapDepotContainer.clear();
}

std::string DepoterManager::get_ContainerName() {
	return ContainerName;
}
void DepoterManager::set_ContainerName(std::string itemName) {
	this->ContainerName = itemName;

	int item_id = ItemsManager::get()->getitem_idFromName(itemName);
	if (item_id)
		this->Containeritem_id = item_id;
}

void DepoterManager::set_DepotBox(std::string itemName, int index){
	this->DepotBoxName = itemName;
	this->DepotBoxId = 22796 + index;
}
int DepoterManager::get_item_id() {
	return Containeritem_id;
}

std::string DepoterManager::requestNewDepotContainer(){
	int id = 0;
	std::string current_id = "depot" + std::to_string(id);
	while (mapDepotContainer.find(current_id) != mapDepotContainer.end()){
		id++;
		current_id = "depot" + std::to_string(id);
	}
	mapDepotContainer[current_id] = std::shared_ptr<DepotContainer>(new DepotContainer);
	return current_id;
}
void DepoterManager::removeDepotContainer(std::string id_){
	auto i = std::find_if(mapDepotContainer.begin(), mapDepotContainer.end(), [&](std::pair<std::string, std::shared_ptr<DepotContainer>> info_pair){
		return _stricmp(&id_[0], &info_pair.first[0]) == 0;});
	if (i != mapDepotContainer.end())
		mapDepotContainer.erase(i);
}
std::shared_ptr<DepotContainer> DepoterManager::getDepotContainer(std::string id_){
	auto it = std::find_if(mapDepotContainer.begin(), mapDepotContainer.end(), [&](std::pair<std::string, std::shared_ptr<DepotContainer>> info_pair){
		return _stricmp(&id_[0], &info_pair.first[0]) == 0; });
	
		if (it == mapDepotContainer.end())
		return 0;

	return it->second;
}
std::map<std::string, std::shared_ptr<DepotContainer>> DepoterManager::getMapDepotContainer(){
	return mapDepotContainer;
}

bool DepoterManager::checkNecessaryDepot() {
	for (auto depot : mapDepotContainer) {
		std::map<std::string, std::shared_ptr<DepotItem>> itemList = depot.second->getMapDepoItens();
		std::map<uint32_t, ItemContainerPtr> containersList = ContainerManager::get()->get_containers();

		for (auto container : containersList) {
			for (auto item : itemList) {
				int item_id = item.second->get_item_id();

				if (container.second->get_item_count_by_id(item_id) >= item.second->get_maxqty())
					return true;
			}
		}
	}
	return false;
}

bool DepoterManager::checkNecessaryDepot(std::string DepotContainer_) {	
	auto depot = std::find_if(mapDepotContainer.begin(), mapDepotContainer.end(), [&](std::pair<std::string, std::shared_ptr<DepotContainer>> info_pair){
		return _stricmp(&DepotContainer_[0], &info_pair.first[0]) == 0; });
	if (depot == mapDepotContainer.end())
		return false;

	if (!depot->second)
		return false;

	Actions::reopen_containers();
	std::map<std::string, std::shared_ptr<DepotItem>> itemList = depot->second->getMapDepoItens();
	for (auto item : itemList) {
		int item_id = item.second->get_item_id();

		if (ContainerManager::get()->get_item_count(item_id) >= item.second->get_maxqty())
			return true;
	}

	return false;
}
bool DepoterManager::changeDepoId(std::string newId, std::string oldId){
	if (newId == "" || oldId == "")
		return false;

	auto it = std::find_if(mapDepotContainer.begin(), mapDepotContainer.end(), [&](std::pair<std::string, std::shared_ptr<DepotContainer>> info_pair){
		return _stricmp(&oldId[0], &info_pair.first[0]) == 0; });
	if (it == mapDepotContainer.end())
		return false;

	mapDepotContainer[newId] = it->second;
	mapDepotContainer.erase(it);
	return true;
}

Json::Value DepoterManager::parse_class_to_json() {
	Json::Value depoterClass;
	Json::Value depoterList;

	for (auto depot : mapDepotContainer){
		std::string depotId = depot.first;
		depoterList[depotId] = mapDepotContainer[depotId]->parse_class_to_json();
	}

	depoterClass["depoterList"] = depoterList;
	depoterClass["depoterContainer"] = this->ContainerName;
	depoterClass["DepotBoxId"] = this->DepotBoxId;
	depoterClass["DepotBoxName"] = this->DepotBoxName;
	depoterClass["depoterContainerId"] = this->Containeritem_id;
	depoterClass["use_depot_old_style"] = this->use_depot_old_style;

	return depoterClass;
}
void DepoterManager::parse_json_to_class(Json::Value jsonObject) {
	if (!jsonObject["depoterContainer"].empty() || !jsonObject["depoterContainer"].isNull())
		this->ContainerName = jsonObject["depoterContainer"].asString();

	if (!jsonObject["depoterContainerId"].empty() || !jsonObject["depoterContainerId"].isNull())
		this->Containeritem_id = jsonObject["depoterContainerId"].asInt();

	if (!jsonObject["DepotBoxId"].empty() || !jsonObject["DepotBoxId"].isNull())
		this->DepotBoxId = jsonObject["DepotBoxId"].asInt();

	if (!jsonObject["DepotBoxName"].empty() || !jsonObject["DepotBoxName"].isNull())
		this->DepotBoxName = jsonObject["DepotBoxName"].asString();

	if (!jsonObject["use_depot_old_style"].isNull() || !jsonObject["use_depot_old_style"].empty())
		this->use_depot_old_style = jsonObject["use_depot_old_style"].asBool();
	else
		this->use_depot_old_style = false;

	if (!jsonObject["depoterList"].empty() || !jsonObject["depoterList"].isNull()){
		Json::Value depoterList = jsonObject["depoterList"];
		Json::Value::Members members = depoterList.getMemberNames();

		for (auto member : members){
			std::shared_ptr<DepotContainer> depot(new DepotContainer);
			mapDepotContainer[member] = depot;
			depot->parse_json_to_class(depoterList[member]);
		}
	}
}

