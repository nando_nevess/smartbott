#pragma once

#define DEVLOPMENT_MODE_H
//#ifdef DEV_MODE

#include "Core\Util.h"
#include "Core\Time.h"
#include <Windows.h>



#define LOG_TIME_INIT\
	DevelopmentMethodTimeCost LOG_TIME(__FUNCTION__);

#define LOG_TIME_SET_SUB\
	LOG_TIME.log_sub_time(std::to_string((long long)__LINE__), false);

#define LOG_TIME_SET_RESET\
	LOG_TIME.log_sub_time(std::to_string((long long)__LINE__), true);

#define MONITOR_THREAD(THREAD_NAME)\
	if(!DevelopmentManager::get()->thread_is_on(GetCurrentThreadId(), THREAD_NAME)){\
		return;\
	}\
	Sleep(100);\
	auto auto_handler = DevelopmentManager::get()->registerThread(GetCurrentThreadId(), THREAD_NAME);



class DevelopmentManager{ 
public:
	class ThreadAutoRelease{
	public:
		uint32_t thread_id;
		ThreadAutoRelease(uint32_t _thread_id) : thread_id(_thread_id){}
		~ThreadAutoRelease(){
			DevelopmentManager::get()->
				unregisterThread(thread_id);
		}
	};

	neutral_mutex mtx_access;

	std::map<std::string/*mainidentifier*/, std::map<std::string/*subidentifier*/, uint64_t/*value*/>> time_costs;

	std::map<std::string/*mainidentifier*/, std::map<std::string/*subidentifier*/, uint64_t/*value*/>> get_time_costs();

	std::map<uint32_t/*threadid*/, std::string/*name*/> threads;

	std::map<uint32_t/*threadid*/, std::string/*name*/> get_threads();

	void set_id_time(std::string main_id, std::string sub_id, uint64_t value);

	uint64_t get_id_time(std::string main_id, std::string sub_id);

	std::shared_ptr<ThreadAutoRelease> registerThread(uint32_t threadId, std::string name);

	void registerThreadNotAutoDelete(uint32_t threadId, std::string name);

	void unregisterThread(uint32_t threadId);

	bool thread_is_on(uint32_t threadId, std::string name);

	static DevelopmentManager* get();

	
};


class DevelopmentMethodTimeCost{
	TimeChronometer start_time;
	TimeChronometer sub_start_time;
	std::string main_id;
	void log_main_time();

public:
	DevelopmentMethodTimeCost(std::string _main_id);

	~DevelopmentMethodTimeCost();
	void reset_sub_time();
	void reset_main_time();
	void log_sub_time(std::string log_id, bool reset = false);
};

namespace DevelopmentUtil{
	uint64_t get_thread_delta(uint32_t thread_id);
	void suspend_thread(uint32_t tid);
	void resume_thread(uint32_t tid);
}

void register_in_monitor(char* thread_name);

//#else
//
//#define LOG_TIME_INIT
//#define LOG_TIME_SET_SUB
//#define LOG_TIME_SET_RESET

//	#endif