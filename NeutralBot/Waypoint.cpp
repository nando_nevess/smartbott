#pragma once
#include "Depot.h"
#include "Repot.h"
#include "Sell.h"
#include "AdvancedRepoter.h"
#include "Waypoint.h"
#include "WaypointInfo.h"
#include "LanguageManager.h"
#include "ManagedUtil.h"
#include "LuaBackground.h"
#include "Core\Map.h"

using namespace NeutralBot;

Waypoint::Waypoint(void){
	InitializeComponent();
	MineInitialize();

	current_hud_orientation = orientation_enum_t::orientation_center;
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate(); 
	   
	miniMapControl1->set_current_coordinate(current_coordinate.x, current_coordinate.y, current_coordinate.z);
	miniMapControl1->onCoordinateMouseDown += gcnew MiniMap::delegateCoordinateMouseEvent(this, &Waypoint::on_coordinate_mouse_down);
	miniMapControl1->onCoordinateMouseUp += gcnew MiniMap::delegateCoordinateMouseEvent(this, &Waypoint::on_coordinate_mouse_up);
	miniMapControl1->onCoordinateMouseClick += gcnew MiniMap::delegateCoordinateClick(this, &Waypoint::on_coordinate_click);
	miniMapControl1->onCoordinateMouseMove += gcnew MiniMap::delegateCoordinateMouseEvent(this, &Waypoint::on_coordinate_mouse_move);
	miniMapControl1->onWaypointMouseOut += gcnew MiniMap::delegateWaypoint(this, &Waypoint::on_waypoint_out);
	miniMapControl1->onWaypointMouseEnter += gcnew MiniMap::delegateWaypoint(this, &Waypoint::on_waypoint_enter);
	miniMapControl1->onWaypointMoved += gcnew MiniMap::delegateWaypoint(this, &Waypoint::on_waypoint_moved);
	miniMapControl1->onWaypointSelectedChanged += gcnew MiniMap::delegateWaypointChanged(this, &Waypoint::on_waypoint_selected_changed);

	this->waypointPageView->ViewElement->ItemDragService->Stopped += gcnew System::EventHandler(this, &Waypoint::waypointPageViewStopped);
	this->waypointPageView->ViewElement->ItemDragService->PreviewDragOver += gcnew System::EventHandler<Telerik::WinControls::RadDragOverEventArgs^>(this, &Waypoint::waypointPageViewPreviewDragOver);
	this->waypointPageView->ViewElement->ItemDragService->PreviewDragStart += gcnew System::EventHandler<Telerik::WinControls::PreviewDragStartEventArgs^>(this, &Waypoint::waypointPageViewPreviewDragStart);
	
	waypointPageView->ViewElement->AllowEdit = false;
	this->radMenuComboItem1->ComboBoxElement->SelectedIndex = 0;
	radiocenter->ToggleState = Telerik::WinControls::Enumerations::ToggleState::On;
	managed_util::setToggleCheckButtonStyle(radToggleButtonWaypointer);

	NeutralBot::FastUIPanelController::get()->install_controller(this);
}

Waypoint::~Waypoint(){
	unique = nullptr;
	if (components){
		managed_util::unsetToggleCheckButtonStyle(radToggleButtonWaypointer);

		delete components;
	}
}

System::Void Waypoint::loadAll(){
	auto myWaypointPathList = WaypointManager::get()->getwaypointPathList();
	for (auto waypoint_path : myWaypointPathList){
		if (!waypoint_path.second)
			continue;


		AddNewPage(waypoint_path.first.ToString(), gcnew String(&waypoint_path.second->getName()[0]));



		currentListView->BeginUpdate();

		auto waypoint_info_list = waypoint_path.second->getwaypointInfoList();
		for (uint32_t index = 0; index < waypoint_info_list.size(); index++) {
			Telerik::WinControls::UI::ListViewDataItem^ newListViewDataItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(index.ToString()));
			currentListView->Items->Add(newListViewDataItem);

			newListViewDataItem["ColumnLabel"] = gcnew String(&waypoint_info_list[index]->get_label()[0]);
			newListViewDataItem["ColumnX"] = waypoint_info_list[index]->get_position_x();
			newListViewDataItem["ColumnY"] = waypoint_info_list[index]->get_position_y();
			newListViewDataItem["ColumnZ"] = waypoint_info_list[index]->get_position_z();
			newListViewDataItem["ColumnAction"] =
				gcnew String(&(LanguageManager::get()->get_translation(std::string("waypoint_t_") + std::to_string((long long)
				waypoint_info_list[index]->get_action()))[0]));


		}
		
		currentListView->EndUpdate();
	}
	
	if (waypointPageView->Pages->Count > 0) {
		waypointPageView->SelectedPage = waypointPageView->Pages[waypointPageView->Pages->Count - 1];
		waypointPathLabelTextBox->Text = gcnew String(WaypointManager::get()->getWaypointPath(Convert::ToInt32(waypointPageView->SelectedPage->Name))->get_label().c_str());
	}

	updateMiniMapWaypoints();
}

System::Void Waypoint::Waypoint_Load(System::Object^  sender, System::EventArgs^  e) {
	disable_update = true;
	isChangingIndex = true;

	loadAll();
		
	if (waypointPageView->Pages->Count <= 0)
		AddNewPage(nullptr, "New");

	disable_update = false;
	isChangingIndex = false;
	
	this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");
	update_idiom();
}

System::Void Waypoint::waypointPageContextMenuOpen(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e){
	if (!can_execute())
		return;

	if (e->Button == System::Windows::Forms::MouseButtons::Right)
		waypointPageContextMenu->Show(waypointPageView->PointToScreen(e->Location));	
}

System::Void Waypoint::waypointConfigureItemMenuOpen(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e){
	if (!can_execute())
		return;

	if (e->Button == System::Windows::Forms::MouseButtons::Right && currentListView->Items->Count > 0){
		selectedItemWithRightCLick = currentListView->ElementTree->GetElementAtPoint(e->Location);
		String^ x = selectedItemWithRightCLick->GetType()->ToString();
		if (!selectedItemWithRightCLick || x != "Telerik.WinControls.UI.DetailListViewDataCellElement")
			return;

		waypointConfigureItem->Show(waypointPageView->PointToScreen(e->Location));
	}
}

void Waypoint::AddNewPage(String^ identifier, String^ text){
	unLoadAction();
	Telerik::WinControls::UI::RadPageViewPage^ radPageViewPage = (gcnew Telerik::WinControls::UI::RadPageViewPage());
	Telerik::WinControls::UI::RadListView^ radListView = (gcnew Telerik::WinControls::UI::RadListView());

	Telerik::WinControls::UI::ListViewDetailColumn^  ColumnLabel = (gcnew Telerik::WinControls::UI::ListViewDetailColumn("ColumnLabel", "Label"));
	Telerik::WinControls::UI::ListViewDetailColumn^  ColumnX = (gcnew Telerik::WinControls::UI::ListViewDetailColumn("ColumnX", "X"));
	Telerik::WinControls::UI::ListViewDetailColumn^  ColumnY = (gcnew Telerik::WinControls::UI::ListViewDetailColumn("ColumnY", "Y"));
	Telerik::WinControls::UI::ListViewDetailColumn^  ColumnZ = (gcnew Telerik::WinControls::UI::ListViewDetailColumn("ColumnZ", "Z"));
	Telerik::WinControls::UI::ListViewDetailColumn^  ColumnAction = (gcnew Telerik::WinControls::UI::ListViewDetailColumn("ColumnAction", "Action"));

	radPageViewPage->Controls->Add(radListView);
	radPageViewPage->ItemSize = System::Drawing::SizeF(39, 28);
	radPageViewPage->Location = System::Drawing::Point(10, 37);
	radPageViewPage->TextChanged += gcnew System::EventHandler(this, &Waypoint::waypointPageView_TextChanged);
	radPageViewPage->Size = System::Drawing::Size(773, 392);

	if (!identifier)
		radPageViewPage->Name = WaypointManager::get()->requestNewWaypointPathId().ToString();	
	else
		radPageViewPage->Name = identifier;
	
	if (text)
		radPageViewPage->Text = text;
	else
		radPageViewPage->Text = L"New";

	ColumnLabel->HeaderText = L"Label";
	ColumnLabel->Width = 100;
	ColumnX->HeaderText = L"X";
	ColumnX->MaxWidth = 50;
	ColumnX->MinWidth = 50;
	ColumnX->Width = 50;
	ColumnY->HeaderText = L"Y";
	ColumnY->MaxWidth = 50;
	ColumnY->MinWidth = 50;
	ColumnY->Width = 50;
	ColumnZ->HeaderText = L"Z";
	ColumnZ->MaxWidth = 50;
	ColumnZ->MinWidth = 50;
	ColumnZ->Width = 50;
	ColumnAction->HeaderText = L"Action";
	ColumnAction->Width = 150;

	radListView->AllowEdit = true;
	radListView->AllowDragDrop = true;
	radListView->Columns->AddRange(gcnew cli::array<Telerik::WinControls::UI::ListViewDetailColumn^>(5) { ColumnLabel, ColumnX, ColumnY, ColumnZ, ColumnAction });
	radListView->ItemSpacing = -1;
	radListView->Location = System::Drawing::Point(4, 3);
	radListView->Name = L"radListView1";
	radListView->TabIndex = 0;
	radListView->Text = L"radListView1";
	radListView->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
	radListView->Dock = System::Windows::Forms::DockStyle::Fill;
	
	radListView->MouseClick += gcnew System::Windows::Forms::MouseEventHandler(this, &Waypoint::waypointConfigureItemMenuOpen);
	radListView->DoubleClick += gcnew System::EventHandler(this, &Waypoint::waypointListViewDoubleClick);
	radListView->ItemValueChanged += gcnew Telerik::WinControls::UI::ListViewItemValueChangedEventHandler(this, &Waypoint::waypointListViewItemValueChanged);
	radListView->ItemRemoved += gcnew Telerik::WinControls::UI::ListViewItemEventHandler(this, &Waypoint::waypointListViewItemRemoved);
	radListView->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Waypoint::waypointListViewItemRemoving);
	radListView->SelectedIndexChanged += gcnew System::EventHandler(this, &Waypoint::waypointListViewSelectedIndexChanged);
	radListView->EditorRequired += gcnew Telerik::WinControls::UI::ListViewItemEditorRequiredEventHandler(this, &Waypoint::waypointList_EditorRequired);
	
	radListView->ListViewElement->DragDropService->PreviewDragOver += gcnew System::EventHandler<Telerik::WinControls::RadDragOverEventArgs^>(this, &Waypoint::waypointListViewPreviewDragOver);
	radListView->ListViewElement->DragDropService->Stopped += gcnew System::EventHandler(this, &Waypoint::waypointListViewStopped);	

	waypointPageView->Controls->Add(radPageViewPage);
	waypointPageView->SelectedPage = radPageViewPage;
}

System::Void Waypoint::waypointListViewStopped(System::Object^  sender, System::EventArgs^  e){
	std::shared_ptr<WaypointPath> waypointPath = WaypointManager::get()->getWaypointPath(Convert::ToInt32(waypointPageView->SelectedPage->Name));
	if (!waypointPath){
		disable_update = false;
		isChangingIndex = false;
		return;
	}

	int newIndex = currentListView->Items->IndexOf(currentListView->SelectedItem);
	if (last_index == newIndex){
		disable_update = false;
		isChangingIndex = false;
		return;
	}

	waypointPath->changeIndexWaypointInfo(last_index, newIndex);

	disable_update = false;
	isChangingIndex = false;

	update_item();
}
System::Void Waypoint::waypointListViewPreviewDragOver(System::Object^  sender, Telerik::WinControls::RadDragOverEventArgs^  e){
	if (!can_execute()){
		e->CanDrop = false;
		return;
	}

	disable_update = true;
	isChangingIndex = true;
	last_index = currentListView->Items->IndexOf(currentListView->ListViewElement->DragDropService->DraggedItem);
}

System::Void Waypoint::waypointPageViewStopped(System::Object^  sender, System::EventArgs^  e){
	int newIndex = waypointPageView->Pages->IndexOf(currentWaypointPath);

	if (last_index_path == newIndex){
		disable_update = false;
		isChangingIndex = false;
		return;
	}

	WaypointManager::get()->changeIndexWaypointPath(Convert::ToInt32(currentWaypointPath->Name), newIndex);
	std::map<int, std::shared_ptr<WaypointPath>> myMap = WaypointManager::get()->getwaypointPathList();

	int index = 100;
	for each(auto page in waypointPageView->Pages){
		page->Name = Convert::ToString(index);
		index++;
	}

	for (auto path : myMap){
		String^ internalName = managed_util::fromString(path.second->getName());		
		for each(auto page in waypointPageView->Pages){
			if (internalName->Trim()->ToLower() == page->Text->Trim()->ToLower())
				page->Name = Convert::ToString(path.first);
		}
	}

	disable_update = false;
	isChangingIndex = false;
}
System::Void  Waypoint::waypointPageViewPreviewDragStart(System::Object^  sender, Telerik::WinControls::PreviewDragStartEventArgs^  e){
	if (!can_execute()){
		e->CanStart = false;
		return;
	}

	disable_update = true;
	isChangingIndex = true;
	last_index_path = waypointPageView->Pages->IndexOf(currentWaypointPath);
}
System::Void Waypoint::waypointPageViewPreviewDragOver(System::Object^  sender, Telerik::WinControls::RadDragOverEventArgs^  e){
	/*if (!can_execute()){
		e->CanDrop = false;
		return;
	}

	last_index_path = waypointPageView->Pages->IndexOf(currentWaypointPath);*/
}

System::Void Waypoint::waypointListViewItemValueChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangedEventArgs^  e) {
	if (disable_update)
		return;

	unLoadAction();
	std::shared_ptr<WaypointPath> waypointPath = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
	std::shared_ptr<WaypointInfo> waypointInfo = waypointPath->getWaypointInfo(currentListView->Items->IndexOf(e->Item));

	if (!e->ListViewElement->CurrentColumn)
		return;

	if (e->ListViewElement->CurrentColumn->Name == "ColumnLabel") {
		Object^ interfaceValue = e->ListViewElement->CurrentItem[e->ListViewElement->CurrentColumn];
		if (interfaceValue == nullptr)
			return;
		std::string currentValue = marshal_as<std::string>(interfaceValue->ToString());
		waypointInfoLabelTextBox->Text = interfaceValue->ToString();
		waypointInfo->set_label(currentValue);
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnX") {
		Object^ interfaceValue = e->ListViewElement->CurrentItem[e->ListViewElement->CurrentColumn];
		if (interfaceValue == nullptr)
			return;
		int currentValue = Convert::ToInt32(interfaceValue->ToString());
		waypointInfo->set_position_x(currentValue);
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnY") {
		Object^ interfaceValue = e->ListViewElement->CurrentItem[e->ListViewElement->CurrentColumn];
		if (interfaceValue == nullptr)
			return;
		int currentValue = Convert::ToInt32(interfaceValue->ToString());
		waypointInfo->set_position_y(currentValue);
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnZ") {
		Object^ interfaceValue = e->ListViewElement->CurrentItem[e->ListViewElement->CurrentColumn];
		if (interfaceValue == nullptr)
			return;
		int currentValue = Convert::ToInt32(interfaceValue->ToString());
		waypointInfo->set_position_z(currentValue);
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnAction") {
		Object^ interfaceValue = e->ListViewElement->CurrentItem[e->ListViewElement->CurrentColumn];
		if (interfaceValue == nullptr)
			return;

		waypoint_t currentValue = WaypointManager::getStringAsWaypoint(managed_util::fromSS(interfaceValue->ToString()));
		waypointInfo->set_action(currentValue);

		loadAction((int)currentListView->Items->IndexOf(e->Item));
	}
}

System::Void Waypoint::waypointList_EditorRequired(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEditorRequiredEventArgs^  e) {
	if (isChangingIndex || disable_update)
		return;

	if (!can_execute())
		return;

	if (e->ListViewElement->CurrentColumn->Name == "ColumnAction") {
		Telerik::WinControls::UI::ListViewDropDownListEditor^ editortype = gcnew Telerik::WinControls::UI::ListViewDropDownListEditor();

		for (int i = 1; i < waypoint_t::waypoint_t_last; i++) {
			Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
			item->Text = gcnew String(&WaypointManager::getWaypointAsString((waypoint_t)i)[0]);
			((Telerik::WinControls::UI::BaseDropDownListEditorElement^)editortype->EditorElement)->Items->Add(item);
		}
		editortype->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;

		e->Editor = editortype;
		return;
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnX") {
		auto editorr = gcnew Telerik::WinControls::UI::ListViewSpinEditor();
		editorr->MaxValue = 99999;
		e->Editor = editorr;
		return;
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnY") {
		auto editorr = gcnew Telerik::WinControls::UI::ListViewSpinEditor();
		editorr->MaxValue = 99999;
		e->Editor = editorr;
		return;
	}
	else if (e->ListViewElement->CurrentColumn->Name == "ColumnZ") {
		auto editor = gcnew Telerik::WinControls::UI::ListViewSpinEditor();

		editor->MaxValue = 99999;
		e->Editor = editor;
		return;
	}
}

System::Void Waypoint::waypointLurePointFromMinimap(System::Object^ sender, System::EventArgs^ args){
	Telerik::WinControls::RadMessageBox::Show(this, "Sorry option under development", "Error!");

	return;
	if (!lastMouseClickCoord)
		return;

//	addWaypointLure(true, lastMouseClickCoord);

}

System::Void Waypoint::waypointPageAddNewPage(System::Object^ sender, System::EventArgs^ args){
	disable_update = true;
	isChangingIndex = true;

	AddNewPage(nullptr, "New");

	disable_update = false;
	isChangingIndex = false;
}

System::Void Waypoint::waypointInsertDown(System::Object^ sender, System::EventArgs^ args) {
	if (!currentWaypointPath || !currentListView || currentListView->Items->Count <= 0)
		return;

	std::shared_ptr<WaypointPath> current_path = getCurrentWaypointPath();
	if (!current_path)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ Item = currentListView->SelectedItem;
	int item_index = currentListView->Items->IndexOf(Item);
	auto waypointInfo = current_path->getWaypointInfo(item_index);
	if (!waypointInfo)
		return;

	Coordinate newPosition = Coordinate{ waypointInfo->get_position().x - 1, waypointInfo->get_position().y - 1, waypointInfo->get_position().z };

	auto newWaypointInfo = std::shared_ptr<WaypointInfo>(new WaypointInfo);

	newWaypointInfo->set_action(waypointInfo->get_action());
	newWaypointInfo->set_position(newPosition);
	newWaypointInfo->set_label(waypointInfo->get_label());
	newWaypointInfo->set_side_to_go(waypointInfo->get_side_to_go());

	item_index = item_index + 1;
	Telerik::WinControls::UI::ListViewDataItem^ newListViewDataItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(item_index.ToString()));
	if (currentListView->Items->Count == item_index)
		currentListView->Items->Add(newListViewDataItem);
	else
		currentListView->Items->Insert(item_index, newListViewDataItem);

	currentListView->SelectedItem = newListViewDataItem;

	Telerik::WinControls::UI::RadListDataItem^ action = waypointActionDropDownList->Items[waypointInfo->get_action() - 1];

	newListViewDataItem["ColumnLabel"] = gcnew String(waypointInfo->get_label().c_str());;
	newListViewDataItem["ColumnX"] = newWaypointInfo->get_position_x();
	newListViewDataItem["ColumnY"] = newWaypointInfo->get_position_y();
	newListViewDataItem["ColumnZ"] = newWaypointInfo->get_position_z();
	newListViewDataItem["ColumnAction"] = action->Text;
	
	current_path->insertWaypointInfo(item_index, newWaypointInfo);
	updateMiniMapWaypoints();
	update_item();
	disable_update = false;
	isChangingIndex = false;	
}

System::Void Waypoint::waypointInsert(System::Object^ sender, System::EventArgs^ args) {
	if (!currentWaypointPath || !currentListView || currentListView->Items->Count <= 0)
		return;

	std::shared_ptr<WaypointPath> current_path = getCurrentWaypointPath();
	if (!current_path)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ Item = currentListView->SelectedItem;
	int item_index = currentListView->Items->IndexOf(Item);

	auto waypointInfo = current_path->getWaypointInfo(item_index);	
	if (!waypointInfo)
		return;

	Coordinate newPosition = Coordinate{ waypointInfo->get_position().x + 1, waypointInfo->get_position().y + 1, waypointInfo->get_position().z };

	auto newWaypointInfo = std::shared_ptr<WaypointInfo>(new WaypointInfo);

	newWaypointInfo->set_action(waypointInfo->get_action());
	newWaypointInfo->set_position(newPosition);
	newWaypointInfo->set_label(waypointInfo->get_label());
	newWaypointInfo->set_side_to_go(waypointInfo->get_side_to_go());
	
	Telerik::WinControls::UI::ListViewDataItem^ newListViewDataItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(item_index.ToString()));
	currentListView->Items->Insert(item_index, newListViewDataItem);
	currentListView->SelectedItem = newListViewDataItem;

	Telerik::WinControls::UI::RadListDataItem^ action = waypointActionDropDownList->Items[waypointInfo->get_action() -1];

	newListViewDataItem["ColumnLabel"] = gcnew String(waypointInfo->get_label().c_str());
	newListViewDataItem["ColumnX"] = newWaypointInfo->get_position_x();
	newListViewDataItem["ColumnY"] = newWaypointInfo->get_position_y();
	newListViewDataItem["ColumnZ"] = newWaypointInfo->get_position_z();
	newListViewDataItem["ColumnAction"] = action->Text;
		
	current_path->insertWaypointInfo(item_index, newWaypointInfo);
	updateMiniMapWaypoints();
	update_item();

	disable_update = false;
	isChangingIndex = false;
}

System::Void Waypoint::bt_update_coord_Click(System::Object^  sender, System::EventArgs^  e){
	if (!currentWaypointPath || !currentListView || currentListView->Items->Count <= 0)
		return;

	std::shared_ptr<WaypointPath> current_path = getCurrentWaypointPath();
	if (!current_path)
		return;

	int item_index = currentListView->Items->IndexOf(currentListView->SelectedItem);
	auto waypointInfo = current_path->getWaypointInfo(item_index);
	if (!waypointInfo)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ newListViewDataItem = currentListView->SelectedItem;
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	Telerik::WinControls::UI::RadListDataItem^ action = waypointActionDropDownList->Items[waypointInfo->get_action() - 1];

	newListViewDataItem["ColumnLabel"] = gcnew String( waypointInfo->get_label().c_str());
	newListViewDataItem["ColumnX"] = current_coord.getX();
	newListViewDataItem["ColumnY"] = current_coord.getY();
	newListViewDataItem["ColumnZ"] = current_coord.getZ();
	newListViewDataItem["ColumnAction"] = action->Text;

	waypointInfo->set_position_x(current_coord.getX());
	waypointInfo->set_position_y(current_coord.getY());
	waypointInfo->set_position_z(current_coord.getZ());

	updateMiniMapWaypoints();
}

System::Void Waypoint::waypointRemove(System::Object^ sender, System::EventArgs^ args) {
	if (!currentWaypointPath || !currentListView)
		return;

	std::shared_ptr<WaypointPath> current_path = getCurrentWaypointPath();
	if (!current_path)
		return;

	Telerik::WinControls::UI::DetailListViewDataCellElement^ element = (Telerik::WinControls::UI::DetailListViewDataCellElement^) selectedItemWithRightCLick;
	Telerik::WinControls::UI::ListViewDataItem^ Item = element->Row;
	int item_index = currentListView->Items->IndexOf(Item);

	currentListView->Items->RemoveAt(item_index);
	current_path->removeWaypointInfo(item_index);
	updateMiniMapWaypoints();
}

System::Void Waypoint::waypointSetCurrentWaypointInfo(System::Object^ sender, System::EventArgs^ args) {
	if (!currentWaypointPath || !currentListView)
		return;

	Telerik::WinControls::UI::DetailListViewDataCellElement^ element = (Telerik::WinControls::UI::DetailListViewDataCellElement^) selectedItemWithRightCLick;
	Telerik::WinControls::UI::ListViewDataItem^ Item = element->Row;

	int item_index = currentListView->Items->IndexOf(Item);
	std::string current_path_str = managed_util::fromSS(currentWaypointPath->Text);
	WaypointManager::get()->setCurrentWaypointPathByLabel(current_path_str, item_index);
}

System::Void Waypoint::waypointAddLurePointForSelected(System::Object^ sender, System::EventArgs^ args){
	Telerik::WinControls::RadMessageBox::Show(this, "Sorry option under development", "Error!");
	return;
	if (currentListView && currentListView->SelectedIndex != -1){
		MiniMap::Coordinate^ add_coord;
		std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
		if (current_path){
			std::shared_ptr<WaypointInfo> _it;
			if (currentListView->Items->Count > 0){
				_it = current_path->getWaypointInfo(
					currentListView->SelectedIndex);
			}
			else{
				_it = current_path->getWaypointInfo(
					currentListView->SelectedIndex);
			}
			if (!add_coord)
				return;
			add_coord = gcnew MiniMap::Coordinate(
			_it->get_position_x(),
			_it->get_position_y(),
			_it->get_position_z());
		}
		if (!add_coord)
			return;
		add_coord->x += 2;
		add_coord->y += 2;
		addWaypointLure(false, add_coord);
	}
	
}

System::Void Waypoint::waypointListViewDoubleClick(System::Object^  sender, System::EventArgs^ e){
	Telerik::WinControls::UI::RadListView^ listView = (Telerik::WinControls::UI::RadListView^) sender;
	if (!listView->SelectedItem)
		return;

	if (!can_execute()){
		listView->AllowEdit = false;
		return;
	}
	else
		listView->AllowEdit = true;

	auto current_waypoint_info = getSelectedWaypointInfo();
	if (current_waypoint_info){
		if (current_waypoint_info->get_action() == waypoint_t::waypoint_lua_script){
			std::string current_script = current_waypoint_info->get_additionalInfo("script");
			NeutralBot::LuaBackground^ mluaEditor = gcnew NeutralBot::LuaBackground(gcnew String(&current_script[0]));
			mluaEditor->hide_left_pane();
			mluaEditor->ShowDialog();
			current_waypoint_info->set_additionalInfo("script", marshal_as<std::string>(mluaEditor->fastColoredTextBox1->Text));

		}
		else
			listView->BeginEdit();
	}
}

System::Void Waypoint::waypointListViewSelectedIndexChanged(System::Object^  sender, System::EventArgs^ e) {
		update_item();	
}

void Waypoint::update_item(){
	isChangingIndex = true;
	disable_update = true;

	int currentIndex = currentListView->SelectedIndex;
	currentListViewItem = currentListView->SelectedItem;

	std::shared_ptr<WaypointPath> waypointPath = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
	if (!waypointPath){
		disable_update = false;
		isChangingIndex = false;
		return;
	}
	std::shared_ptr<WaypointInfo> waypointInfo = waypointPath->getWaypointInfo(currentIndex);
	if (!waypointInfo){
		disable_update = false;
		isChangingIndex = false;
		return;
	}

	if (can_execute()) {
		loadAction(waypointInfo->get_action());
		String^ label = gcnew String(waypointInfo->get_label().c_str());

		waypointInfoLabelTextBox->Text = label;
		waypointActionDropDownList->SelectedIndex = waypointInfo->get_action() - 1;

		clear_button_sides();
		Telerik::WinControls::UI::RadToggleButton^ button = get_button_side(get_orientation_by_side(waypointInfo->get_side_to_go()));
		if (button)
			button->ToggleState = Telerik::WinControls::Enumerations::ToggleState::On;
		
		for each(auto control in radGroupBox2->Controls) {
			Telerik::WinControls::RadControl^ component = (Telerik::WinControls::RadControl^) control;
			if (component->Name->Contains("label"))
				continue;

			std::string additionalInfo = waypointInfo->get_additionalInfo(marshal_as<std::string>(component->Name));
			component->Text = gcnew String(additionalInfo.c_str());
		}
	}	
	
	std::shared_ptr<WaypointInfo> current_info = getSelectedWaypointInfo();
	if (!current_info){
		disable_update = false;
		isChangingIndex = false;
		return;
	}

	MiniMap::WaypointInfo^ info = gcnew MiniMap::WaypointInfo();
	info->x = current_info->get_position_x();
	info->y = current_info->get_position_y();
	info->z = current_info->get_position_z();
	info->label = gcnew String(current_info->get_label().c_str());
	info->identifier = gcnew String(Convert::ToString((int)current_info.get()));
	info->identifier_next = nullptr;

	miniMapControl1->selectedWaypoint = info;
	miniMapControl1->update(false);
	disable_update = false;
	isChangingIndex = false;
}

System::Void Waypoint::waypointListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	e->Item->Text = currentListView->Items->IndexOf(e->Item).ToString();
}

System::Void Waypoint::waypointListViewItemRemoved(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEventArgs^  e) {
	int current_waypoint_path_index = Convert::ToInt32(currentWaypointPath->Name);
	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(current_waypoint_path_index);

	int current_index = Convert::ToInt32(e->Item->Text);
	int next = current_path->getNextWaypointInfoId(current_index);
	int before = current_path->getBeforeWaypointInfoId(current_index);

	auto current_info = current_path->getWaypointInfo(current_index);
	auto next_info = current_path->getWaypointInfo(next);
	auto before_info = current_path->getWaypointInfo(before);

	current_path->removeWaypointInfo(current_index);

	MiniMap::WaypointInfo^ info_current = miniMapControl1->getWaypointInfoById(Convert::ToString((int)current_info.get()));
	MiniMap::WaypointInfo^ info_next = miniMapControl1->getWaypointInfoById(Convert::ToString((int)next_info.get()));
	MiniMap::WaypointInfo^ info_before = miniMapControl1->getWaypointInfoById(Convert::ToString((int)before_info.get()));
	if (info_before)
		info_before->identifier_next = Convert::ToString((int)next_info.get());

	miniMapControl1->removeWaypointById(Convert::ToString((int)current_info.get()));
}

std::shared_ptr<WaypointInfo> Waypoint::addCoordinate(MiniMap::Coordinate^ coord, waypoint_t type, orientation_enum_t orientatio_type){
	if (!currentWaypointPath || !currentListView)
		return nullptr;

	bool edit_before = get_waypoint_edition_mode() == waypoint_edition_mode_t::edit_insert;

	if (edit_before) {
		if (currentListView->SelectedIndex == -1){
			Telerik::WinControls::RadMessageBox::Show(this, "Handle error MUST SELECT WAYPOINT TYPE");
			return nullptr;
		}
	}

	Coordinate current_coordinate;
	if (!coord)
		current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	else
	{
		current_coordinate.x = coord->x;
		current_coordinate.y = coord->y;
		current_coordinate.z = coord->z;
	}

	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));

	int newWaypointInfoIndex = current_path->requestNewWaypointInfoIndex();

	Telerik::WinControls::UI::ListViewDataItem^ newListViewDataItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(newWaypointInfoIndex.ToString()));
	currentListView->Items->AddRange(gcnew cli::array<Telerik::WinControls::UI::ListViewDataItem^>(1){ newListViewDataItem });

	String^ label = "";// waypointLabelTextBox->Text;
	Telerik::WinControls::UI::RadListDataItem^ action;

	if (edit_before)
		action = waypointActionDropDownList->SelectedItem;
	else
		action = waypointActionDropDownList->Items[(int)type];	
	
	Coordinate playerPos(current_coordinate.x, current_coordinate.y, current_coordinate.z);

	newListViewDataItem["ColumnLabel"] = "";
	newListViewDataItem["ColumnX"] = playerPos.x;
	newListViewDataItem["ColumnY"] = playerPos.y;
	newListViewDataItem["ColumnZ"] = playerPos.z;
	newListViewDataItem["ColumnAction"] = action->Text;

	side_t::side_t side_type = get_side_by_orientation(orientatio_type);
	std::shared_ptr<WaypointInfo> newWaypointInfo(new WaypointInfo(marshal_as<std::string>(label), (waypoint_t)(int)action->Value, playerPos, side_type));

	radGroupBox2->Controls->Clear();

	current_path->setWaypointInfo(newWaypointInfoIndex, newWaypointInfo);

	MiniMap::WaypointInfo^ info = gcnew MiniMap::WaypointInfo();
	info->x = newWaypointInfo->get_position_x();
	info->y = newWaypointInfo->get_position_y();
	info->z = newWaypointInfo->get_position_z();
	info->identifier = gcnew String(Convert::ToString((int)newWaypointInfo.get()));

	int current_index = newWaypointInfoIndex;
	int next = current_path->getNextWaypointInfoId(current_index);
	int before = current_path->getBeforeWaypointInfoId(current_index);

	auto current_info = current_path->getWaypointInfo(current_index);
	auto next_info = current_path->getWaypointInfo(next);
	auto before_info = current_path->getWaypointInfo(before);

	MiniMap::WaypointInfo^ info_before = miniMapControl1->getWaypointInfoById(Convert::ToString((int)before_info.get()));
	
	if (info_before)
		info_before->identifier_next = Convert::ToString((int)current_info.get());
	
	info->identifier_next = Convert::ToString((int)next_info.get());
	miniMapControl1->addWaypoint(info);

	if (currentListView->Items->Count > 0)
		currentListView->SelectedIndex = newWaypointInfoIndex;

	return newWaypointInfo;
}

System::Void Waypoint::waypointAddButtonClick(System::Object^  sender, System::EventArgs^  e) {
	if (!can_execute()) 
		return;

	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (current_coord.is_null())
		return;

	if (currentListView && currentListView->Items->Count > 1)
		currentListView->SelectedIndex = currentListView->Items->Count - 1;

	clear_button_sides();

	if (automatic_lure){
		if (lure_reverse)
			addWaypointLure(false, gcnew MiniMap::Coordinate(current_coord.getX(), current_coord.getY(), current_coord.getZ()));		
		else
			addWaypointLure(false, nullptr);
	}
	addCoordinate(genMiniMapCoordFromNormalCoord(current_coord), (waypoint_t)0, get_waypoint_selected_orientation());

	this->radiocenterNew->ToggleState = Telerik::WinControls::Enumerations::ToggleState::On;
}

System::Void Waypoint::waypointPageView_NewPageRequested(System::Object^  sender, System::EventArgs^  e) {
	waypointPageAddNewPage(sender, e);
}

System::Void Waypoint::Waypoint_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
	if (GeneralManager::get()->get_practice_mode()){
		this->Hide();
		return;
	}
}

System::Void Waypoint::waypointPageView_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
	miniMapControl1->removeAllWaypoints();

	currentWaypointPath = waypointPageView->SelectedPage;
	if (!currentWaypointPath)
		return;

	array<Control^>^ radListViewList = currentWaypointPath->Controls->Find("radListView1", true);
	if (radListViewList->Length <= 0)
		return;

	disable_update = true;
	isChangingIndex = true;

	currentListView = (Telerik::WinControls::UI::RadListView^)radListViewList[0];
	currentListViewItem = currentListView->SelectedItem;
	waypointPathLabelTextBox->Text = gcnew String(WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name))->getName().c_str());
	last_index_path = waypointPageView->Pages->IndexOf(currentWaypointPath);

	disable_update = false;
	isChangingIndex = false;

	updateMiniMapWaypoints();
	update_item();
}

System::Void Waypoint::waypointActionDropDownList_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (get_waypoint_edition_mode() == waypoint_edition_mode_t::edit_insert)
		return;

	if (isChangingIndex || !can_execute())
		return;

	auto selectedItem = waypointActionDropDownList->SelectedItem;
	if (!selectedItem)
		return;

	loadAction((int)selectedItem->Value);
	
	if (!currentListViewItem)
		return;

	currentListViewItem["ColumnAction"] = selectedItem->Text;
	std::shared_ptr<WaypointInfo> current_wpt = getSelectedWaypointInfo();
	if (!current_wpt)
		return;

	current_wpt->set_action((waypoint_t)(int)selectedItem->Value);
}

void Waypoint::loadWaypointOptions(){
	for (int i = 1; i < waypoint_t::waypoint_t_last; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem;
		item->Text = gcnew String(&(LanguageManager::get()->get_translation(std::string("waypoint_t_") + std::to_string((long long)i))[0]));
		item->Value = i;
		waypointActionDropDownList->Items->Add(item);
	}
}

void Waypoint::update_idiom(){
	radGroupBox2->Text = gcnew String(&GET_TR(managed_util::fromSS(radGroupBox2->Text))[0]);
	waypointAddButton->Text = gcnew String(&GET_TR(managed_util::fromSS(waypointAddButton->Text))[0]);
	waypointPageView->Text = gcnew String(&GET_TR(managed_util::fromSS(waypointPageView->Text))[0]);
	radMenuItem2->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem2->Text))[0]);
	radMenuItem3->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem3->Text))[0]);
	radGroupBox2->Text = gcnew String(&GET_TR(managed_util::fromSS(radGroupBox2->Text))[0]);
	//waypointCleanButton->Text = gcnew String(&GET_TR(managed_util::fromSS(waypointCleanButton->Text))[0]);
	radMenuItem6->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem6->Text))[0]);
	radMenuItem4->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem4->Text))[0]);
	radMenuItem5->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem5->Text))[0]);
	radHideMiniMap->Text = gcnew String(&GET_TR(managed_util::fromSS(radHideMiniMap->Text))[0]);
	checkBoxTopMost->Text = gcnew String(&GET_TR(managed_util::fromSS(checkBoxTopMost->Text))[0]);
	checkBoxShowOtherLevels->Text = gcnew String(&GET_TR(managed_util::fromSS(checkBoxShowOtherLevels->Text))[0]);
	radRepeatButton2->Text = gcnew String(&GET_TR(managed_util::fromSS(radRepeatButton2->Text))[0]);
	radRepeatButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radRepeatButton1->Text))[0]);
	btnlever->Text = gcnew String(&GET_TR(managed_util::fromSS(btnlever->Text))[0]);
	radButton11->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton11->Text))[0]);
	btnuse->Text = gcnew String(&GET_TR(managed_util::fromSS(btnuse->Text))[0]);
	btnladder->Text = gcnew String(&GET_TR(managed_util::fromSS(btnladder->Text))[0]);
	btnmachete->Text = gcnew String(&GET_TR(managed_util::fromSS(btnmachete->Text))[0]);
	btnwalk->Text = gcnew String(&GET_TR(managed_util::fromSS(btnwalk->Text))[0]);
	btnrope->Text = gcnew String(&GET_TR(managed_util::fromSS(btnrope->Text))[0]);
	btnlua->Text = gcnew String(&GET_TR(managed_util::fromSS(btnlua->Text))[0]);
	btnshovel->Text = gcnew String(&GET_TR(managed_util::fromSS(btnshovel->Text))[0]);
	btnstand->Text = gcnew String(&GET_TR(managed_util::fromSS(btnstand->Text))[0]);
	radMenuItem7->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem7->Text))[0]);
	pathMenu->Text = gcnew String(&GET_TR(managed_util::fromSS(pathMenu->Text))[0]);
	radMenuItem9->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem9->Text))[0]);
	radMenuItem10->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem10->Text))[0]);
	waypointerMenu->Text = gcnew String(&GET_TR(managed_util::fromSS(waypointerMenu->Text))[0]);
	radMenuItem11->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem11->Text))[0]);
	radMenuItem12->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem12->Text))[0]);
	radMenuItem8->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem8->Text))[0]);
	radToggleButtonWaypointer->Text = gcnew String(&GET_TR(managed_util::fromSS(radToggleButtonWaypointer->Text))[0]);
	int newLocationX = checkBoxTopMost->Location.X + checkBoxTopMost->Size.Width + 12;
}

System::Void Waypoint::loadAction(int waypoint_t_index) {
	unLoadAction();

	std::map<int, std::shared_ptr<actionInfo>> actionInfoList = WaypointManager::get()->getActionInfoList(waypoint_t_index);
	if (actionInfoList.size() == 0)
		return;

	System::Drawing::Point point(4, 0);

	for (auto infoList : actionInfoList){
		std::shared_ptr<actionInfo> actionComponentInfo = infoList.second;
		if (actionComponentInfo->get_type() == "label") {
			Telerik::WinControls::UI::RadLabel^ radLabel = gcnew Telerik::WinControls::UI::RadLabel();

			point.Y = 20 + point.Y;
			radLabel->Location = point;
			radLabel->Name = gcnew String(actionComponentInfo->get_id().c_str());
			radLabel->Size = System::Drawing::Size(27, 18);
			radLabel->TabIndex = 2;
			radLabel->Text = gcnew String(actionComponentInfo->get_text().c_str());

			radGroupBox2->Controls->Add(radLabel);
		}
		else if (actionComponentInfo->get_type() == "textBox") {
			Telerik::WinControls::UI::RadTextBox^ radTextBox = gcnew Telerik::WinControls::UI::RadTextBox();

			point.Y = 20 + point.Y;
			radTextBox->Location = point;
			radTextBox->Name = gcnew String(actionComponentInfo->get_id().c_str());
			radTextBox->Size = System::Drawing::Size(178, 20);
			radTextBox->TabIndex = 4;
			radTextBox->Text = gcnew String(actionComponentInfo->get_text().c_str());
			radTextBox->TextChanged += gcnew System::EventHandler(this, &Waypoint::onActionComponent_TextChanged);
			radGroupBox2->Controls->Add(radTextBox);
		}
		else if (actionComponentInfo->get_type() == "dropdownlist") {
			Telerik::WinControls::UI::RadDropDownList^ radTextBox = gcnew Telerik::WinControls::UI::RadDropDownList();

			point.Y = 20 + point.Y;
			radTextBox->Location = point;
			radTextBox->Name = gcnew String(actionComponentInfo->get_id().c_str());
			radTextBox->Size = System::Drawing::Size(178, 20);
			radTextBox->TabIndex = 4;
			radTextBox->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			auto options = actionComponentInfo->get_options();
			for (auto it : options){
				Telerik::WinControls::UI::RadListDataItem^ radListDataItem1 =
					gcnew Telerik::WinControls::UI::RadListDataItem(gcnew String(&it.second[0]), gcnew String(&it.first[0]));
					radTextBox->Items->Add(radListDataItem1);
			}
			radTextBox->SelectedIndex = 1;
			radTextBox->Text = gcnew String(actionComponentInfo->get_text().c_str());
			radTextBox->TextChanged += gcnew System::EventHandler(this, &Waypoint::onActionComponent_TextChanged);
			radGroupBox2->Controls->Add(radTextBox);
		}
		else if (actionComponentInfo->get_type() == "dropdownlistbool") {
			Telerik::WinControls::UI::RadDropDownList^ radTextBox = gcnew Telerik::WinControls::UI::RadDropDownList();

			point.Y = 20 + point.Y;
			radTextBox->Location = point;
			radTextBox->Name = gcnew String(actionComponentInfo->get_id().c_str());
			radTextBox->Size = System::Drawing::Size(178, 20);
			radTextBox->TabIndex = 4;
			radTextBox->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
			radTextBox->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem(gcnew String("False"), System::Boolean::FalseString));
			radTextBox->Items->Add(gcnew Telerik::WinControls::UI::RadListDataItem(gcnew String("True"), System::Boolean::TrueString));
			
			if (gcnew String(actionComponentInfo->get_text().c_str()) == "")
				radTextBox->SelectedIndex = 0;

			radTextBox->Text = gcnew String(actionComponentInfo->get_text().c_str());
			radTextBox->TextChanged += gcnew System::EventHandler(this, &Waypoint::onActionComponent_TextChanged);
			radGroupBox2->Controls->Add(radTextBox);
		}
		
		else if (actionComponentInfo->get_type() == "Button") {
			Telerik::WinControls::UI::RadButton^ radButton = gcnew Telerik::WinControls::UI::RadButton();

			point.Y = 30 + point.Y;
			//point.X = 26 + point.X;
			radButton->Location = point;
			radButton->Name = gcnew String(actionComponentInfo->get_id().c_str());
			radButton->Size = System::Drawing::Size(178, 23);
			radButton->TabIndex = 4;
			radButton->Text = gcnew String(actionComponentInfo->get_text().c_str());
			radButton->Click += gcnew System::EventHandler(this, &Waypoint::onActionComponent_Click);
			radGroupBox2->Controls->Add(radButton);
		}
	}
}

System::Void Waypoint::onActionComponent_Click(System::Object^ sender, System::EventArgs^  e) {
	std::shared_ptr<WaypointInfo> current_waypoint_info = getSelectedWaypointInfo();
	if (!current_waypoint_info)
		return;

	waypoint_t current_action = current_waypoint_info->get_action();
	switch (current_action){
	case waypoint_check_necesary_deposit_go_to_label:
		Neutral::Depot::ShowUnique();
		break;
	case waypoint_check_necesary_deposit_any_goto_label:
		Neutral::Depot::ShowUnique();
		break;
	case waypoint_check_necesary_repot_go_to_label:
		Neutral::Repot::ShowUnique();
		break;
	case waypoint_check_necesary_repot_any_go_to_label:
		Neutral::Repot::ShowUnique();
		break;
	case waypoint_deposit_items_by_depot_id:
		Neutral::Depot::ShowUnique();
		break;
	case waypoint_deposit_items_all_depot:
		Neutral::Depot::ShowUnique();
		break;
	case waypoint_withdraw_all_repots_if_fail_goto_label:
		Neutral::Repot::ShowUnique();
		break;
	case waypoint_repot_here_by_id:
		Neutral::Repot::ShowUnique();
		break;
	case waypoint_repot_from_depot_by_id:
		Neutral::AdvancedRepoter::ShowUnique();
		break;
	case waypoint_sell_items_by_sell_id:
		NeutralBot::Sell::ShowUnique();
		break;
	case waypoint_sell_all_items:
		NeutralBot::Sell::ShowUnique();
		break;
	default:
		return;
		break;

	}
}

System::Void Waypoint::unLoadAction() {
	if (radGroupBox2->Controls->Count > 0)
		radGroupBox2->Controls->Clear();
}

System::Void Waypoint::TextEditorInitialize(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e){
	waypointPageView->ViewElement->ActiveEditor->Validated += gcnew System::EventHandler(this, &Waypoint::onPageEditName);
}

System::Void Waypoint::onPageEditName(System::Object^ sender, System::EventArgs^ args){
	auto waypoint_path = getCurrentWaypointPath();
	if (!waypoint_path || !currentWaypointPath)
		return;
	waypoint_path->setName(marshal_as<std::string>(currentWaypointPath->Text));
}

std::shared_ptr<WaypointInfo> Waypoint::getSelectedWaypointInfo(){
	if (!currentListView)
		return nullptr;

	std::shared_ptr<WaypointPath> current_path = getCurrentWaypointPath();
	if (!current_path)
		return nullptr;

	return current_path->getWaypointInfo(currentListView->SelectedIndex);
}

std::shared_ptr<WaypointPath> Waypoint::getCurrentWaypointPath(){
	if (!currentWaypointPath)
		return nullptr;
	return WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
}

System::Void Waypoint::miniMapControl1_onCoordinateMouseClick(MiniMap::Coordinate^  A_0, System::EventArgs^ args) {
	MouseEventArgs^ args_mouse = (MouseEventArgs^)args;
	if (args_mouse->Button != System::Windows::Forms::MouseButtons::Right)
		return;
	lastMouseClickCoord = A_0;
	miniMapMenu->Items->Clear();
	Telerik::WinControls::UI::RadMenuItem^ newItem = gcnew Telerik::WinControls::UI::RadMenuItem();
	newItem->Text = "New Waypoint";
	newItem->Click += gcnew System::EventHandler(this, &Waypoint::MenuNewWaypointClick);
	miniMapMenu->Items->Add(newItem);
	miniMapMenu->Show(System::Windows::Forms::Cursor::Position);
}

System::Void Waypoint::addWaypointLure(bool minimap, MiniMap::Coordinate^ coord){
	if (automatic_lure){
		MiniMap::Coordinate^ add_coord;
		
		if (!coord && currentListView)
		{
			if (currentListView->Items->Count > 0)
			{

				std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
				if (current_path){
					std::shared_ptr<WaypointInfo> _it;
					if (currentListView->Items->Count > 0){
						_it = current_path->getWaypointInfo(
							currentListView->Items->Count - 1);
					}
					else{
						_it = current_path->getWaypointInfo(
							currentListView->SelectedIndex);
					}
					if (_it)
						add_coord = gcnew MiniMap::Coordinate(
							_it->get_position_x(),
							_it->get_position_y(),
							_it->get_position_z());
				}
					
			}
		}
		else if (coord){
			add_coord = coord;
		}
		if (!add_coord){
			if (!coord)
				return;
			add_coord = coord;
			return;
		}
			

		add_coord->x += 1;
		add_coord->y += 1;

		if (minimap)
			auto waypointInfo = addCoordinate(add_coord, (waypoint_t)43,orientation_enum_t::orientation_center);//get_waypoint_selected_orientation()
		else
			auto waypointInfo = addCoordinate(add_coord, (waypoint_t)43, orientation_enum_t::orientation_center);

		add_coord->x--;
		add_coord->y--;
		std::shared_ptr<WaypointInfo> current_wpt = getSelectedWaypointInfo();
		if (!current_wpt)
			return;

		currentListViewItem["ColumnAction"] = gcnew String(&(LanguageManager::get()->get_translation(std::string("waypoint_t_") + std::to_string((long long)current_wpt->get_action()))[0]));
	}

	update_item();
}

System::Void Waypoint::MenuNewWaypointClick(System::Object^  sender, System::EventArgs^  e){
	if (!lastMouseClickCoord)
		return;

	if (currentListView && currentListView->Items->Count > 1){
		currentListView->SelectedIndex = currentListView->Items->Count - 1;
	}

	if (automatic_lure){
		if (lure_reverse)
			addWaypointLure(false, lastMouseClickCoord);		
		else
			addWaypointLure(false, nullptr);
	}

	addCoordinate(lastMouseClickCoord, (waypoint_t)0, orientation_enum_t::orientation_center);
}

System::Void Waypoint::onActionComponent_TextChanged(System::Object^ sender, System::EventArgs^  e) {
	SaveActionComponentValue(sender);
}

System::Void Waypoint::onActionComponent_SelectedIndexChanged(System::Object^ sender){
	SaveActionComponentValue(sender);
}

System::Void Waypoint::SaveActionComponentValue(System::Object^ sender){
	if (isChangingIndex)
		return;

	System::Windows::Forms::Control^ ctrl = (System::Windows::Forms::Control^)sender;
	String^ id = ctrl->Name;
	System::Type^ type = sender->GetType();
	if (type == Telerik::WinControls::UI::RadTextBox::typeid){
		Telerik::WinControls::UI::RadTextBox^ textBox = (Telerik::WinControls::UI::RadTextBox^) sender;
		std::shared_ptr<WaypointInfo> current_selected_waypoint = getSelectedWaypointInfo();

		if (!current_selected_waypoint)
			return;

		current_selected_waypoint->set_additionalInfo(marshal_as<std::string>(id), marshal_as<std::string>(textBox->Text));
	}
	else if (type == Telerik::WinControls::UI::RadDropDownList::typeid){
		Telerik::WinControls::UI::RadDropDownList^ textBox = (Telerik::WinControls::UI::RadDropDownList^) sender;
		std::shared_ptr<WaypointInfo> current_selected_waypoint = getSelectedWaypointInfo();
		
		if (!current_selected_waypoint)
			return;

		current_selected_waypoint->set_additionalInfo(marshal_as<std::string>(id), marshal_as<std::string>(textBox->SelectedItem->Value->ToString()));
	}
}

void Waypoint::on_coordinate_mouse_down(MiniMap::Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args){
}

void Waypoint::on_coordinate_mouse_up(MiniMap::Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args){
}

void Waypoint::on_coordinate_click(MiniMap::Coordinate^ coord, System::EventArgs^){
}

void Waypoint::on_coordinate_mouse_move(MiniMap::Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args){
}

void Waypoint::on_waypoint_out(MiniMap::WaypointInfo^){
}

void Waypoint::on_waypoint_enter(MiniMap::WaypointInfo^){
}

void Waypoint::on_waypoint_moved(MiniMap::WaypointInfo^ info){
	if (!currentWaypointPath || !info)
		return;

	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
	std::shared_ptr<WaypointInfo> current_wpt = current_path->getWaypointInfoByRawPointer((WaypointInfo*)Convert::ToInt32(info->identifier));
	if (!current_wpt)
		return;
	current_wpt->set_position_x(info->x);
	current_wpt->set_position_y(info->y);
	int index = current_path->getWaypointIndexOf(current_wpt);
	if (index < 0 || index >= currentListView->Items->Count)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ item = currentListView->Items[index];
	item["ColumnX"] = info->x;
	item["ColumnY"] = info->y;
	miniMapControl1->update(false);
}

void Waypoint::select_waypoint_in_list_by_identifier(String^ identifier){
	if (!currentListView)
		return;
	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
	if (!current_path)
		return;

	int pointer = Convert::ToUInt32(identifier);


	for each(Telerik::WinControls::UI::ListViewDataItem^ item in currentListView->Items){
		auto wpt = current_path->getWaypointInfo(Convert::ToInt32(item->Value));
		if (wpt){
			if ((int)wpt.get() == pointer){
				currentListView->SelectedItem = item;
				break;
			}
		}
	}
}

void Waypoint::on_waypoint_selected_changed(MiniMap::WaypointInfo^ _old, MiniMap::WaypointInfo^ _new){
	if (!currentWaypointPath)
		return;

	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
	if (_new){
		std::shared_ptr<WaypointInfo> current_wpt = getSelectedWaypointInfo();
		std::shared_ptr<WaypointInfo> new_wpt = current_path->getWaypointInfoByRawPointer((WaypointInfo*)Convert::ToInt32(_new->identifier));
		if (current_wpt != new_wpt){
			select_waypoint_in_list_by_identifier(Convert::ToString((int)new_wpt.get()));
		}
	}
}

void Waypoint::updateMiniMapWaypoints(){
	if (!currentWaypointPath)
		return;

	miniMapControl1->removeAllWaypoints();

	std::shared_ptr<WaypointPath> current_path = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));
	if (current_path == nullptr)
		return;


	int current_index = 0;
	for each (Telerik::WinControls::UI::ListViewDataItem^ item in currentListView->Items){
		int next = current_path->getNextWaypointInfoId(current_index);
		int before = current_path->getBeforeWaypointInfoId(current_index);

		auto current_info = current_path->getWaypointInfo(current_index);
		if (!current_info)
			return;

		auto next_info = current_path->getWaypointInfo(next);
		auto before_info = current_path->getWaypointInfo(before);

		MiniMap::WaypointInfo^ info_before = miniMapControl1->getWaypointInfoById(Convert::ToString((int)before_info.get()));
		if (info_before)
			info_before->identifier_next = Convert::ToString((int)current_info.get());

		MiniMap::WaypointInfo^ info = gcnew MiniMap::WaypointInfo();
		info->x = current_info->get_position_x();
		info->y = current_info->get_position_y();
		info->z = current_info->get_position_z();
		info->label = gcnew String(&current_info->get_label()[0]);
		info->identifier = gcnew String(Convert::ToString((int)current_info.get()));
		info->identifier_next = Convert::ToString((int)next_info.get());

		miniMapControl1->addWaypoint(info);
		current_index++;
	}
}

System::Void Waypoint::waypointPageView_PageRemoving(System::Object^  sender, Telerik::WinControls::UI::RadPageViewCancelEventArgs^  e) {
	if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente remove este path? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::No){
		e->Cancel = true;
		return;
	}

	int page_id = Convert::ToInt32(e->Page->Name);
	WaypointManager::get()->removeWaypointPath(page_id);
}

System::Void Waypoint::waypointInfoLabelTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_update || isChangingIndex)
		return;

	if (!currentListViewItem)
		return;

	disable_update = true;
	isChangingIndex = true;

	currentListViewItem["ColumnLabel"] = waypointInfoLabelTextBox->Text;
	std::shared_ptr<WaypointInfo> current_wpt = getSelectedWaypointInfo();
	if (!current_wpt)
		return;

	current_wpt->set_label(marshal_as<std::string>(waypointInfoLabelTextBox->Text));

	isChangingIndex = false;
	disable_update = false;
}

System::Void Waypoint::waypointPathLabelTextBox_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_update || isChangingIndex)
		return;

	if (!currentWaypointPath)
		return;

	disable_update = true;
	isChangingIndex = true;

	currentWaypointPath = waypointPageView->SelectedPage;
	std::shared_ptr<WaypointPath> editedWaypointPath = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name));

	editedWaypointPath->setName(marshal_as<std::string>(waypointPathLabelTextBox->Text));
	editedWaypointPath->set_label(marshal_as<std::string>(waypointPathLabelTextBox->Text));

	if (currentWaypointPath)
		currentWaypointPath->Text = waypointPathLabelTextBox->Text;

	isChangingIndex = false;
	disable_update = false;	
}

System::Void Waypoint::waypointPageView_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	/*if (disable_update || isChangingIndex)
		return;

	disable_update = true;
	isChangingIndex = true;

	Telerik::WinControls::UI::RadPageViewPage^ page = (Telerik::WinControls::UI::RadPageViewPage^) sender;

	std::shared_ptr<WaypointPath> editedWaypointPath = WaypointManager::get()->getWaypointPath(Convert::ToInt32(page->Name));

	editedWaypointPath->setName(marshal_as<std::string>(page->Text));

	if (waypointPageView->SelectedPage)
		waypointPathLabelTextBox->Text = waypointPageView->SelectedPage->Text;

	disable_update = false;
	isChangingIndex = false;*/
}

bool Waypoint::can_execute() {
	return !(NeutralManager::get()->get_core_states(CORE_WAYPOINTER) == ENABLED);
}

System::Void Waypoint::update_current_waypoint(System::Object^  sender, System::EventArgs^  e) {
	if (!currentListView || !currentWaypointPath)
		return;

	update_minimap_current_coord_axis();

	if (can_execute())
		return;
	
	unLoadAction();

	/*if (can_execute()){
		if (!currentListView->AllowDragDrop)
			currentListView->AllowDragDrop = true;
		return;
	}

	if (currentListView->AllowDragDrop)
		currentListView->AllowDragDrop = false;*/

	if (!currentWaypointPath)
		return;

	auto current_waypoint_path = WaypointManager::get()->get_currentWaypointPathId();
	if (current_waypoint_path > waypointPageView->Pages->Count)
		current_waypoint_path = 0;

	if (!this->waypointPageView->Pages[current_waypoint_path])
		return;	

	auto current_selected_page = this->waypointPageView->Pages[current_waypoint_path];
	this->waypointPageView->SelectedPage = current_selected_page;

	if (!currentListView)
		return;

	auto current_waypoint_info = WaypointManager::get()->getcurrentWaypointInfoId();

	if (currentListView->SelectedIndex != current_waypoint_info)
		currentListView->SelectedIndex = current_waypoint_info;
}

System::Void Waypoint::Waypoint_VisibleChanged(System::Object^  sender, System::EventArgs^  e) {
	if (Waypoint::Visible) {
		miniMapControl1->update(true);
	}
}
System::Void Waypoint::buttonZoomIn_Click(System::Object^  sender, System::EventArgs^  e) {
	miniMapControl1->IncreaseZoom(1);
}
System::Void Waypoint::buttonZoomOut_Click(System::Object^  sender, System::EventArgs^  e) {
	miniMapControl1->DescreaseZoom(1);
}
System::Void Waypoint::buttonUpLevel_Click(System::Object^  sender, System::EventArgs^  e) {
	miniMapControl1->decrease_z(1);
}
System::Void Waypoint::buttonDownLevel_Click(System::Object^  sender, System::EventArgs^  e) {
	miniMapControl1->increase_z(1);
}
System::Void Waypoint::radHideMiniMap_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!panelMiniMap->Visible){
		this->radHideMiniMap->Text = "Hide Map";
	}
	else{
		this->radHideMiniMap->Text = "Show Map";
	}
	panelMiniMap->Visible = !panelMiniMap->Visible;
}

System::Void Waypoint::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	std::shared_ptr<WaypointInfo> selected = getSelectedWaypointInfo();
	if (selected){
		miniMapControl1->set_current_coordinate(selected->get_position_x(), selected->get_position_y(), selected->get_position_z());
	}
}
System::Void Waypoint::radButton2_Click(System::Object^  sender, System::EventArgs^  e) {
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (!current_coordinate.is_null()){
		miniMapControl1->set_current_coordinate(current_coordinate.x, current_coordinate.y, current_coordinate.z);
	}
}
System::Void Waypoint::dropDownEditionMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {

	if (this->radMenuComboItem1->ComboBoxElement->SelectedIndex == -1)
		this->radMenuComboItem1->ComboBoxElement->SelectedIndex = 0;
	set_edition_mode((waypoint_edition_mode_t)this->radMenuComboItem1->ComboBoxElement->SelectedIndex);
}

void Waypoint::set_edition_mode(waypoint_edition_mode_t mode){
	switch (mode){
	case waypoint_edition_mode_t::edit_insert:{
												  set_mode_edit_insert();
	}
		break;
	case waypoint_edition_mode_t::hud_mode:{
											   set_mode_hud_mode();
	}
		break;
	case waypoint_edition_mode_t::insert_edit:{
												  set_mode_insert_edit();
	}
		break;
	case waypoint_edition_mode_t::minimap_plus_hud_mode:{
															set_mode_minimap_plus_mode_hud();
	}
		break;
	}
}

void Waypoint::set_mode_edit_insert(){
	radPanelMenu->Visible = true;
	panelEdit->Visible = true;
	panelMiniMap->Visible = false;
	panelMiniMap->Location = System::Drawing::Point(panelEdit->Width + 2, panelEdit->Location.Y);
	panelMiniMap->Height = panelEdit->Height;
	panelMiniMap->Width = panelEdit->Height;
	panelMiniMap->Visible = true;
	panelHudMode->Visible = false;
	radMenu1->Visible = true;
	checkBoxTopMost->Checked = false;
	radMenuItem7->Visibility = Telerik::WinControls::ElementVisibility::Visible;
	radMenuComboItem1->Visibility = Telerik::WinControls::ElementVisibility::Visible;
	radMenuItem8->Visibility = Telerik::WinControls::ElementVisibility::Visible;
}

void Waypoint::set_mode_insert_edit(){
	radPanelMenu->Visible = true;
	panelEdit->Visible = true;
	panelMiniMap->Visible = false;
	panelMiniMap->Location = System::Drawing::Point(panelEdit->Width + 2, panelEdit->Location.Y);
	panelMiniMap->Height = panelEdit->Height;
	panelMiniMap->Width = panelEdit->Height;
	panelMiniMap->Visible = true;
	panelHudMode->Visible = false;
	radMenu1->Visible = true;
	checkBoxTopMost->Checked = false;
	radMenuItem7->Visibility = Telerik::WinControls::ElementVisibility::Visible;
	radMenuComboItem1->Visibility = Telerik::WinControls::ElementVisibility::Visible;
	radMenuItem8->Visibility = Telerik::WinControls::ElementVisibility::Visible;
}

void Waypoint::set_mode_hud_mode(){
	panelEdit->Visible = false;
	panelMiniMap->Visible = false;
	panelHudMode->Visible = true;
	panelHudMode->Location = System::Drawing::Point(0, 23);
	radMenuItem7->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	radMenuComboItem1->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	radMenuItem8->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	checkBoxTopMost->Checked = true;
}

void Waypoint::set_mode_minimap_plus_mode_hud(){
	panelEdit->Visible = false;
	panelHudMode->Location = System::Drawing::Point(0, 23);
	panelMiniMap->Location = System::Drawing::Point(panelHudMode->Width + 2, 0);
	panelMiniMap->Width = panelHudMode->Height;
	panelMiniMap->Height = panelHudMode->Height;
	panelMiniMap->Visible = true;
	panelHudMode->Visible = true;
	radMenuItem7->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	radMenuComboItem1->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	radMenuItem8->Visibility = Telerik::WinControls::ElementVisibility::Hidden;
	//radMenu1->Visible = false;
	//radPanel1->Visible = false;
	//radPanel2->Visible = false;
	//radPanelMenu->Visible = false;
	checkBoxTopMost->Checked = true;
}

System::Void Waypoint::checkBoxTopMost_CheckedChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	this->TopMost = checkBoxTopMost->Checked;
}

Waypoint::waypoint_edition_mode_t Waypoint::get_waypoint_edition_mode(){
	return (waypoint_edition_mode_t)(this->radMenuComboItem1->ComboBoxElement->SelectedIndex == -1 ? 0 : this->radMenuComboItem1->ComboBoxElement->SelectedIndex);
}

Waypoint::orientation_enum_t Waypoint::get_waypoint_selected_orientation(){
	return current_hud_orientation;
}

side_t::side_t Waypoint::get_side_by_orientation(orientation_enum_t type){
	switch (type){
	case orientation_enum_t::orientation_center:
		return side_t::side_t::side_center;
		break;
	case orientation_enum_t::orientation_east:
		return  side_t::side_t::side_east;
		break;
	case orientation_enum_t::orientation_north:
		return side_t::side_t::side_north;
		break;
	case orientation_enum_t::orientation_south:
		return side_t::side_t::side_south;
		break;
	case orientation_enum_t::orientation_west:
		return side_t::side_t::side_west;
		break;
	default:
		return side_t::side_t::side_center;
		break;
	}
	return side_t::side_t::side_center;
}

Waypoint::orientation_enum_t Waypoint::get_orientation_by_side(side_t::side_t type){
	switch (type){
	case side_t::side_t::side_center:
		return orientation_enum_t::orientation_center;
		break;
	case side_t::side_t::side_east:
		return  orientation_enum_t::orientation_east;
		break;
	case side_t::side_t::side_north:
		return  orientation_enum_t::orientation_north;
		break;
	case side_t::side_t::side_south:
		return  orientation_enum_t::orientation_south;
		break;
	case side_t::side_t::side_west:
		return orientation_enum_t::orientation_west;
		break;
	default:
		return orientation_enum_t::orientation_center;
		break;
	}
	return orientation_enum_t::orientation_center;
}

Coordinate Waypoint::get_offset_to_current_orientation(){
	orientation_enum_t type = get_waypoint_selected_orientation();
	switch (type)
	{
	case orientation_enum_t::orientation_center:
													return Coordinate(0, 0, 0);
		break;
	case orientation_enum_t::orientation_east:
												  return Coordinate(1, 0, 0);
		break;
	case orientation_enum_t::orientation_north:
												   return Coordinate(0, -1, 0);
		break;
	case orientation_enum_t::orientation_northeast:
													   return Coordinate(1, -1, 0);
		break;
	case orientation_enum_t::orientation_northwest:
													   return Coordinate(-1, -1, 0);
		break;
	case orientation_enum_t::orientation_south:
												   return Coordinate(0, 1, 0);
		break;
	case orientation_enum_t::orientation_southeast:
													   return Coordinate(1, 1, 0);
		break;
	case orientation_enum_t::orientation_southwest:
													   return Coordinate(-1, 1, 0);
		break;
	case orientation_enum_t::orientation_west:
												  return Coordinate(-1, 0, 0);
		break;
	default:
		break;
	}
	return Coordinate(0, 0, 0);
}

MiniMap::Coordinate^ Waypoint::genMiniMapCoordFromNormalCoord(Coordinate coord){
	return gcnew MiniMap::Coordinate(coord.x, coord.y, coord.z);
}

System::Void Waypoint::btn_on_add_hud(System::Object^  sender, System::EventArgs^  e) {
	if (!can_execute())
		return;
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	if (current_coord.is_null()){
		Telerik::WinControls::RadMessageBox::Show(this, "Handle error SELECT AN TIBIA CHARACTER");
		return;
	}

	orientation_enum_t orientation = get_waypoint_selected_orientation();
	Telerik::WinControls::UI::RadButton^ sender_button = (Telerik::WinControls::UI::RadButton^)sender;
	String^ action_button = sender_button->Name->Substring(String("btn").Length);

	if (action_button == "walk")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_walk_to), orientation_enum_t::orientation_center);
	else if (action_button == "lua")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_lua_script), orientation_enum_t::orientation_center);
	else if (action_button == "stand")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_walk_to), orientation_enum_t::orientation_center);
	else if (action_button == "shovel")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_shovel), orientation_enum_t::orientation_center);
	else if (action_button == "rope")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_rope), orientation_enum_t::orientation_center);
	else if (action_button == "machete")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_machete), orientation_enum_t::orientation_center);
	else if (action_button == "ladder")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_hole_or_step), orientation_enum_t::orientation_center);
	else if (action_button == "use")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_use), orientation_enum_t::orientation_center);
	else if (action_button == "lever")
		addCoordinate(genMiniMapCoordFromNormalCoord(current_coord + get_offset_to_current_orientation()), (waypoint_t)(-1 + waypoint_t::waypoint_use_lever), orientation_enum_t::orientation_center);

	}

System::Void Waypoint::radButton11_Click(System::Object^  sender, System::EventArgs^  e) {
	this->radMenuComboItem1->ComboBoxElement->SelectedIndex = 0;
}

System::Void Waypoint::radionortheast_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	Telerik::WinControls::UI::RadToggleButton^ sender_radio = (Telerik::WinControls::UI::RadToggleButton^)sender;

	if (sender_radio->ToggleState != Telerik::WinControls::Enumerations::ToggleState::On){
		if (!currentSideButton || currentSideButton == sender)
			sender_radio->ToggleState = Telerik::WinControls::Enumerations::ToggleState::On;
		return;
	}

	if (sender == currentSideButton)
		return;

	Telerik::WinControls::UI::RadToggleButton^ old = currentSideButton;
	currentSideButton = sender_radio;
	if (old)
		old->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;

	String^ type = sender_radio->Name->Substring(String("radio").Length);
	if (type == "center" || type == "centerNew")
		current_hud_orientation = orientation_enum_t::orientation_center;	
	else if (type == "south" || type == "southNew")
		current_hud_orientation = orientation_enum_t::orientation_south;	
	else if (type == "west" || type == "westNew")
		current_hud_orientation = orientation_enum_t::orientation_west;	
	else if (type == "north" || type == "northNew")
		current_hud_orientation = orientation_enum_t::orientation_north;	
	else if (type == "east" || type == "eastNew")
		current_hud_orientation = orientation_enum_t::orientation_east;	
	else if (type == "northeast" || type == "northeastNew")
		current_hud_orientation = orientation_enum_t::orientation_northeast;	
	else if (type == "southeast" || type == "southeastNew")
		current_hud_orientation = orientation_enum_t::orientation_southeast;	
	else if (type == "southwest" || type == "southwestNew")
		current_hud_orientation = orientation_enum_t::orientation_southwest;	
	else if (type == "northwest" || type == "northwestNew")
		current_hud_orientation = orientation_enum_t::orientation_northwest;	
}

System::Void Waypoint::radio_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	Telerik::WinControls::UI::RadToggleButton^ sender_radio = (Telerik::WinControls::UI::RadToggleButton^)sender;

	if (sender_radio->ToggleState != Telerik::WinControls::Enumerations::ToggleState::On){
		if (!currentSideButtonMini || currentSideButtonMini == sender){
			//currentSideButtonMini = sender_radio;
			sender_radio->ToggleState = Telerik::WinControls::Enumerations::ToggleState::On;
		}
		return;
	}

	if (sender == currentSideButtonMini)
		return;

	std::shared_ptr<WaypointInfo> current_wpt = getSelectedWaypointInfo();
	if (!current_wpt)
		return;

	Telerik::WinControls::UI::RadToggleButton^ old = currentSideButtonMini;
	currentSideButtonMini = sender_radio;

	if (old)
		old->ToggleState = Telerik::WinControls::Enumerations::ToggleState::Off;

	disable_update = true;
	String^ type = sender_radio->Name->Substring(String("radio").Length);
	if (type == "center" || type == "centerNew"){
		current_hud_orientation = orientation_enum_t::orientation_center;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_center);
	}
	else if (type == "south" || type == "southNew"){
		current_hud_orientation = orientation_enum_t::orientation_south;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_south);
	}
	else if (type == "west" || type == "westNew"){
		current_hud_orientation = orientation_enum_t::orientation_west;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_west);
	}
	else if (type == "north" || type == "northNew"){
		current_hud_orientation = orientation_enum_t::orientation_north;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_north);
	}
	else if (type == "east" || type == "eastNew"){
		current_hud_orientation = orientation_enum_t::orientation_east;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_east);
	}
	else if (type == "northeast" || type == "northeastNew"){
		current_hud_orientation = orientation_enum_t::orientation_northeast;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_north_east);
	}
	else if (type == "southeast" || type == "southeastNew"){
		current_hud_orientation = orientation_enum_t::orientation_southeast;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_south_east);
	}
	else if (type == "southwest" || type == "southwestNew"){
		current_hud_orientation = orientation_enum_t::orientation_southwest;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_south_west);
	}
	else if (type == "northwest" || type == "northwestNew"){
		current_hud_orientation = orientation_enum_t::orientation_northwest;
		if (!isChangingIndex)
			current_wpt->set_side_to_go(side_t::side_north_west);
	}
	disable_update = false;
}

System::Void Waypoint::radRepeatButton2_Click(System::Object^  sender, System::EventArgs^  e) {
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	miniMapControl1->set_current_coordinate(current_coord.x, current_coord.y, current_coord.z);
}
 
System::Void Waypoint::radRepeatButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	std::shared_ptr<WaypointInfo> selected = getSelectedWaypointInfo();
	if (!selected)
		return;
	
	miniMapControl1->set_current_coordinate(selected->get_position_x(), selected->get_position_y(), selected->get_position_z());
}

System::Void Waypoint::checkBoxShowOtherLevels_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	miniMapControl1->down_level_count_show = checkBoxShowOtherLevels->ToggleState == Telerik::WinControls::Enumerations::ToggleState::On ? 3 : 1;
}

System::Void Waypoint::radMenuItem9_Click(System::Object^  sender, System::EventArgs^  e) {
}

System::Void Waypoint::pathMenu_Click(System::Object^  sender, System::EventArgs^  e) {
}

System::Void Waypoint::buttonSavePath_Click(System::Object^  sender, System::EventArgs^  e) {
	SaveFileDialog ^ saveWaypointPath = gcnew SaveFileDialog();
	saveWaypointPath->Filter = "Waypoint Script|*.way";
	saveWaypointPath->Title = "Save Waypoint Path";

	if (saveWaypointPath->ShowDialog() == System::Windows::Forms::DialogResult::OK)
	{
		System::IO::StreamWriter^ file = gcnew System::IO::StreamWriter(saveWaypointPath->FileName);
		Json::Value waypointPath = WaypointManager::get()->getWaypointPath(Convert::ToInt32(currentWaypointPath->Name))->parse_class_to_json();
		file->WriteLine(gcnew String(waypointPath.toStyledString().c_str()));
		file->Close();
		Telerik::WinControls::RadMessageBox::Show(this, "Handle sucess save waypoint path", " HANDLE SUCCESS CAPTION");
	}
}

System::Void Waypoint::buttonLoadPath_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!can_execute()){
		Telerik::WinControls::RadMessageBox::Show(this, "Handle error pause bot first", " HANDLE ERROR CAPTION");
		return;
	}

	disable_update = true;
	isChangingIndex = true;

	OpenFileDialog ^ openWaypointPath = gcnew OpenFileDialog();
	openWaypointPath->Filter = "Waypoint Script|*.way";
	openWaypointPath->Title = "Open Waypoint Path";

	if (openWaypointPath->ShowDialog() == System::Windows::Forms::DialogResult::OK) {
		std::ifstream file;
		std::string fileName = marshal_as<std::string>(openWaypointPath->FileName);
		file.open(fileName);

		Json::Reader reader;
		Json::Value waypointPath;
		reader.parse(file, waypointPath);
		file.close();

		uint32_t id = WaypointManager::get()->requestNewWaypointPathId();
		std::shared_ptr<WaypointPath> newWaypointPath = WaypointManager::get()->getWaypointPath(id);
		newWaypointPath->parse_json_to_class(waypointPath,200);

		AddNewPage(id.ToString(), gcnew String(&newWaypointPath->getName()[0]));
		auto waypoint_info_list = newWaypointPath->getwaypointInfoList();

		for (uint32_t index = 0; index < waypoint_info_list.size(); index++){
			Telerik::WinControls::UI::ListViewDataItem^ newListViewDataItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(index.ToString()));
			currentListView->Items->Add(newListViewDataItem);

			String^ label = waypointInfoLabelTextBox->Text;

			newListViewDataItem["ColumnLabel"] = gcnew String(&waypoint_info_list[index]->get_label()[0]);
			newListViewDataItem["ColumnX"] = waypoint_info_list[index]->get_position_x();
			newListViewDataItem["ColumnY"] = waypoint_info_list[index]->get_position_y();
			newListViewDataItem["ColumnZ"] = waypoint_info_list[index]->get_position_z();
			newListViewDataItem["ColumnAction"] =
				gcnew String(&(LanguageManager::get()->get_translation(std::string("waypoint_t_") + std::to_string((long long)
				waypoint_info_list[index]->get_action()))[0]));

		}
	}

	updateMiniMapWaypoints();
	disable_update = false;
	isChangingIndex = false;
}

System::Void Waypoint::buttonSaveWaypointer_Click(System::Object^  sender, System::EventArgs^  e) {
	SaveFileDialog ^ saveWaypointPath = gcnew SaveFileDialog();
	saveWaypointPath->Filter = "Waypoint Full|*.wayfull";
	saveWaypointPath->Title = "Save Waypoint Path";

	if (saveWaypointPath->ShowDialog() == System::Windows::Forms::DialogResult::OK){
		System::IO::StreamWriter^ file = gcnew System::IO::StreamWriter(saveWaypointPath->FileName);
		Json::Value waypointPath = WaypointManager::get()->parse_class_to_json();
		file->WriteLine(gcnew String(waypointPath.toStyledString().c_str()));
		file->Close();
		Telerik::WinControls::RadMessageBox::Show(this, "Handle sucess save waypoint full", " HANDLE SUCCESS CAPTION");
	}
}

System::Void Waypoint::buttonLoadWaypointer_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!can_execute()){
		Telerik::WinControls::RadMessageBox::Show(this, "Handle error pause bot first", " HANDLE ERROR CAPTION");
		return;
	}

	OpenFileDialog ^ openWaypointPath = gcnew OpenFileDialog();
	openWaypointPath->Filter = "Waypoint Full|*.wayfull";
	openWaypointPath->Title = "Open Waypoint Full Path";

	if (openWaypointPath->ShowDialog() != System::Windows::Forms::DialogResult::OK)
		return;

	std::string fileName = marshal_as<std::string>(openWaypointPath->FileName);
	Json::Value jsonfile = loadJsonFile(fileName);
	if (!jsonfile.empty()){
		waypointPageView->Pages->Clear();
		WaypointManager::get()->clear();
		WaypointManager::get()->parse_json_to_class(jsonfile,200);
		loadAll(); 
	}
	else
	{
		Telerik::WinControls::RadMessageBox::Show(this, "Handle error fail to load file", " HANDLE ERROR CAPTION");
		return;
	}
}

void Waypoint::update_minimap_current_coord_axis(){
	/*Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	miniMapControl1->set_to_draw_point("character_position", gcnew PointF((float)current_coord.x, (float)current_coord.y));
	miniMapControl1->update(false);*/
} 
