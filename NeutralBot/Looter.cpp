#pragma once
#include "Looter.h"
#include "LuaBackgroundManager.h"
#include "ControlManager2.h"
#include "DepoterManager.h"
using namespace Neutral;

void Looter::addItemOnView(String^ idBp, String^ Item, loot_item_condition_t Condition,
	loot_item_priority_t Important, int value, int group, int minqty){
	array<Control^>^ getRadListView = this->PageBps->SelectedPage->Controls->Find("list_" + idBp, true);
	std::shared_ptr<LooterContainer> Items = LooterManager::get()->get_looter_container_by_id(managed_util::fromSS(idBp));

	std::string id =
		Items->additem(
		managed_util::fromSS(Item), Important, Condition, value, group, minqty);

	if (getRadListView->Length < 0)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];

	Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(&id[0]));
	newItem->Tag = gcnew String(&id[0]);
	newItem->Text = Item;
	newItem->Image = managed_util::get_item_image(newItem->Text, 20);
	radListView->Items->Add(newItem);
	radListView->SelectedIndex = radListView->Items->Count - 1;
	updateSelectedData();
}

Void Looter::waypointPageAddNewPage(std::string PageNameId){
	Telerik::WinControls::UI::RadPageViewPage^ radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
	PageBps->Pages->Add(radPageViewPage1);
	Telerik::WinControls::UI::RadListView^ radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
	Telerik::WinControls::UI::RadDropDownList^ dropDownList = (gcnew Telerik::WinControls::UI::RadDropDownList());
	Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn7 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0", L"Item"));
	
	// box_namebp
	radPageViewPage1->Controls->Add(dropDownList);
	dropDownList->Location = System::Drawing::Point(0, 0);
	dropDownList->Name = "box_" + gcnew String(PageNameId.c_str());
	dropDownList->Dock = System::Windows::Forms::DockStyle::Top;
	dropDownList->TabIndex = 1;
	
	dropDownList->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
	dropDownList->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::box_namebpSelectedIndexChanged);
	dropDownList->DropDownListElement->ItemHeight = 20;
	dropDownList->DropDownListElement->DropDownHeight = 30 * 8;


	std::vector<std::string> arraycontainer = ItemsManager::get()->get_containers();

	dropDownList->BeginUpdate();
	for (auto container : arraycontainer){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = gcnew String(container.c_str());
		item->Image = managed_util::get_item_image(gcnew String(container.c_str()), 20);
		dropDownList->Items->Add(item);
	}
	dropDownList->EndUpdate();

	// radPageViewPage1
	radPageViewPage1->Controls->Add(radListView1);
	radPageViewPage1->ItemSize = System::Drawing::SizeF(39, 28);
	radPageViewPage1->Location = System::Drawing::Point(10, 37);
	radPageViewPage1->Name = "Page_" + gcnew String(PageNameId.c_str());
	radPageViewPage1->Size = System::Drawing::Size(dropDownList->Width, (radPageViewPage1->Parent->Height - dropDownList->Height) - 25);
	radPageViewPage1->Text = gcnew String(PageNameId.c_str());
	
	//radListView1
	radListView1->AutoScroll = false;
	radListView1->Location = System::Drawing::Point(0, 25);
	radListView1->AllowEdit = false;
	radListView1->ShowGridLines = true;
	radListView1->Name = "list_" + gcnew String(PageNameId.c_str());
	radListView1->TabIndex = 10;
	radListView1->Size = System::Drawing::Size(302, 138);
	radListView1->ViewType = Telerik::WinControls::UI::ListViewType::ListView;
	radListView1->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Looter::ItemListViewItemRemoving);
	radListView1->SelectedItemChanged += gcnew System::EventHandler(this, &Looter::ListView1ItemChanged);
	radListView1->ListViewElement->ItemSize = System::Drawing::Size(radListView1->ListViewElement->ItemSize.Width, 25);
	PageBps->SelectedPage = radPageViewPage1;
}

void Looter::loadItemOnView(std::string id){
	array<Control^>^ getRadListView = Controls->Find("list_" + gcnew String(&id[0]), true);
	Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];

	auto& items = LooterManager::get()->containers[id]->items;

	radListView->BeginUpdate();
	for (auto it = items.begin(); it != items.end(); it++){
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(it->first.c_str()));
		newItem->Text = gcnew String(&it->second->get_name()[0]);
		newItem->Tag = gcnew String(it->first.c_str());
		newItem->Image = managed_util::get_item_image(newItem->Text, 20);
		radListView->Items->Add(newItem);
	}
	radListView->EndUpdate();

	if (radListView->Items->Count){
		radListView->SelectedIndex = -1;
		radListView->SelectedIndex = radListView->Items->Count - 1;
	}
}

void Looter::loadAll(){
	for (auto it = LooterManager::get()->containers.begin(); it != LooterManager::get()->containers.end(); it++){
		
		waypointPageAddNewPage(it->first);

		loadItemOnView(it->first);

		Telerik::WinControls::UI::RadDropDownList^ radDownList = getBpCurrentDropDownList();		
		if (radDownList)
			radDownList->Text = gcnew String(&it->second->get_container_name()[0]);		
	}
	

	disable_item_update = true;

	auto myMapTakeSkin = TakeSkinManager::get()->getMapTakeSkin();
	list_take_skin->BeginUpdate();
	for (auto item : myMapTakeSkin){		
		array<String^>^ columnArrayItems = { Convert::ToString(item.second->item_id), Convert::ToString(item.second->corpse_id), GET_MANAGED_TR(std::string("take_skin_condition_t_") + std::to_string(item.second->condition_index)) };
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(item.first), columnArrayItems);
		newItem->Text = (Convert::ToString(item.first));
		list_take_skin->Items->Add(newItem);
		

		if (list_take_skin->Items->Count){
			list_take_skin->SelectedIndex = -1;
			list_take_skin->SelectedIndex = list_take_skin->Items->Count - 1;
		}
	}
	list_take_skin->EndUpdate();
	

	disable_item_update = false;

	disable_item_update = true;
	check_take_skin->Checked = TakeSkinManager::get()->get_take_skin_state();
	std::pair<uint32_t, uint32_t> eat_from_corpses_delay = LooterManager::get()->get_eat_from_corpses_delays();
	radSpinEditorMaxEatFoodFromCorpses->Value = eat_from_corpses_delay.second;
	radSpinEditorMinEatFoodFromCorpses->Value = eat_from_corpses_delay.first;
	spin_time_to_clean->Value = LooterManager::get()->get_time_to_clean();
	spin_max_distance->Value = LooterManager::get()->get_max_distance();
	spin_min_cap->Value = LooterManager::get()->get_min_cap();
	check_eat_food_from_corpses->Checked = LooterManager::get()->get_eat_from_corpses_state();
	check_open_next_container->Checked = LooterManager::get()->get_open_next_container();
	check_auto_reopen_containers->Checked =	LooterManager::get()->get_auto_reopen_container();
	disable_item_update = false;
}

System::Void Looter::bt_add_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!PageBps->SelectedPage)
		return;

	String^ item = "new item";
	loot_item_condition_t condition = (loot_item_condition_t)0;
	loot_item_priority_t important = (loot_item_priority_t)3;
	int value = 0;
	int group = 0;
	int minqty = 0;

	if (item == String::Empty)
		return;

	addItemOnView(PageBps->SelectedPage->Text, item, condition, important, value, group, minqty);
}

System::Void Looter::bt_delete_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!PageBps->SelectedPage)
		return;

	Telerik::WinControls::UI::RadListView^ radListView = get_current_listview();
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = radListView->SelectedItem;

	if (itemRow){
		radListView->Items->Remove(itemRow);
		LooterManager::get()->containers[managed_util::fromSS(PageBps->SelectedPage->Text)]->delete_idItem(managed_util::fromSS(itemRow->Tag->ToString()));
	}
	if (radListView->Items->Count <= 0){
		radGroupBox1->Enabled = false;
	}
}

void Looter::add_in_setup(std::string id){
	String^ looter_id = gcnew String(id.c_str());
	String^ container_name = "none";

	std::shared_ptr<ControlInfo> controlInfoRule = ControlManager::get()->getControlInfoRule("LooterGroup");
	if (!controlInfoRule)
		return;

	String^ label_control_id = gcnew String(controlInfoRule->requestNewById(id+"label").c_str());
	String^ combo_control_id = gcnew String(controlInfoRule->requestNewById(id).c_str());

	auto controlRule_ = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(combo_control_id));
	if (controlRule_){
		controlRule_->set_vectorItem(ItemsManager::get()->get_containers());
		controlRule_->set_property("name", managed_util::fromSS(looter_id));
		controlRule_->set_property("text", managed_util::fromSS(container_name));

		controlRule_->set_property("type", "combobox");
		controlRule_->set_property("sizeW", std::to_string(140));
		controlRule_->set_property("sizeH", std::to_string(20));
		controlRule_->set_property("locationX", std::to_string(10));
		controlRule_->set_property("locationY", std::to_string(60));
	}

	auto controlRule = controlInfoRule->getControlInfoChieldRule(managed_util::fromSS(label_control_id));
	if (controlRule){
		controlRule->set_property("text", managed_util::fromSS(looter_id));
		controlRule->set_property("name", managed_util::fromSS(label_control_id));
		controlRule->set_property("type", "label");
		controlRule->set_property("sizeW", std::to_string(55));
		controlRule->set_property("sizeH", std::to_string(18));
		controlRule->set_property("locationX", std::to_string(10));
		controlRule->set_property("locationY", std::to_string(50));
	}
}

System::Void Looter::PageBps_NewPageRequested(System::Object^  sender, System::EventArgs^  e) {
	std::string id = LooterManager::get()->request_new_bp_id();
	waypointPageAddNewPage(id);
	add_in_setup(id);

	close_button();
}

System::Void Looter::PageBps_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e) {
	LooterManager::get()->delete_bp_id(managed_util::fromSS(e->Page->Text));
	close_button();

	auto control_rule = ControlManager::get()->getControlInfoRule("LooterGroup");
	if (!control_rule)
		return;
	
	control_rule->removeControl(managed_util::fromSS(e->Page->Text));
	control_rule->removeControl(managed_util::fromSS(e->Page->Text) + "label");
}

System::Void Looter::ItemListViewDoubleClick(System::Object^  sender, System::EventArgs^ e) {
	Telerik::WinControls::UI::RadListView^ listView = (Telerik::WinControls::UI::RadListView^) sender;
	listView->BeginEdit();
}

System::Void Looter::ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	if (!get_current_listview()->SelectedItem)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ itemRow = get_current_listview()->SelectedItem;
	auto count = get_current_listview()->Items->Count;
	if (count <= 1){
		radGroupBox1->Enabled = false;
	}

	if (itemRow)
		LooterManager::get()->containers[managed_util::fromSS(PageBps->SelectedPage->Text)]
		->delete_idItem(managed_util::fromSS((String^)itemRow->Tag));

}

System::Void Looter::Looter_Load(System::Object^  sender, System::EventArgs^  e) {
	this->SuspendLayout();
	this->panel_adv->Hide();

	managed_util::setToggleCheckButtonStyle(radToggleButtonLooter);

	loadAll();

	this->Size = System::Drawing::Size(610, 310);
	disable_condition();

	std::vector<std::string> arrayitems = ItemsManager::get()->get_itens();
	tx_nameitem->BeginUpdate();
	for (auto item : arrayitems)
		tx_nameitem->Items->Add(gcnew String(item.c_str()));
	tx_nameitem->EndUpdate();
	
	disable_item_update = true;

	update_idiom();

	dropDownLootGroup->BeginUpdate();
	for (int i = 0; i < 10; i++)
		dropDownLootGroup->Items->Add("Group " + i);

	dropDownLootGroup->SelectedIndex = 0;
	dropDownLootGroup->EndUpdate();

	dropDownLootSequence->BeginUpdate();
	for (int i = 0; i < loot_sequence_t::loot_sequence_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_sequence_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootSequence->Items->Add(item);
	}
	dropDownLootSequence->SelectedIndex = LooterManager::get()->get_current_sequence_type();
	dropDownLootSequence->EndUpdate();


	dropDownLootFilterMode->BeginUpdate();
	for (int i = 0; i < loot_filter_t::loot_filter_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_filter_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootFilterMode->Items->Add(item);
	}
	dropDownLootFilterMode->SelectedIndex = LooterManager::get()->get_current_filter_type();
	dropDownLootFilterMode->EndUpdate();


	dropDownLootType->BeginUpdate();
	for (int i = 0; i < loot_type_t::loot_type_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_type_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootType->Items->Add(item);
	}
	dropDownLootType->SelectedIndex = 0;
	dropDownLootType->EndUpdate();


	dropDownLootAction->BeginUpdate();
	for (int i = 0; i < loot_item_action_t::loot_item_action_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_item_action_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootAction->Items->Add(item);
	}
	dropDownLootAction->EndUpdate();


	dropDownLootConditionADV->BeginUpdate();
	for (int i = 0; i < loot_item_new_condition_t::loot_item_new_condition_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_item_new_condition_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootConditionADV->Items->Add(item);
	}
	dropDownLootConditionADV->EndUpdate();

	dropDownLootImportant->BeginUpdate();
	for (int i = 0; i < loot_item_priority_t::loot_item_priority_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_item_priority_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootImportant->Items->Add(item);
	}
	dropDownLootImportant->SelectedIndex = 0;
	dropDownLootImportant->EndUpdate();


	box_condition_take_skin->BeginUpdate();
	for (int i = 0; i < take_skin_condition_t::take_skin_condition_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("take_skin_condition_t_") + std::to_string(i));
		item->Value = i;
		box_condition_take_skin->Items->Add(item);
	}
	box_condition_take_skin->EndUpdate();

	tx_minqty->Value = 0;

	PageBps->ViewElement->AllowEdit = true;
	PageBps->ViewElement->EditorInitialized += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEditorEventArgs^>(this, &Looter::Page_EditorInitialized);
	
	radGroupBox1->Enabled = false;	
	radGroupBox4->Enabled = false;
	check_take_skin->Enabled = false;

	check_min_cap->Checked = LooterManager::get()->get_min_cap_to_loot();
	CheckBoxRecoorder->Checked = LooterManager::get()->get_recoorder();
	CheckBoxRecoordOnlyHunter->Checked = LooterManager::get()->get_recoord_only_when_hunter_on();
	
	creatContextMenu();
	close_button();
	UpdateGif();
	
	if (list_take_skin->Items->Count > 0){
		radGroupBox4->Enabled = true;
		check_take_skin->Enabled = true;
	}
	
	DropDownListPriorityLooter->Text = Convert::ToString(TakeSkinManager::get()->get_priority_looter());
	disable_item_update = false;

	updateSelectedData(); 

	this->ResumeLayout();
}

System::Void Looter::box_namebpSelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	Telerik::WinControls::UI::RadDropDownList^ radDownList = (Telerik::WinControls::UI::RadDropDownList^)sender;

	if (radDownList->Text == "")
		return;

	std::shared_ptr<LooterContainer> srd_bp = LooterManager::get()->get_looter_container_by_id(managed_util::fromSS(PageBps->SelectedPage->Text));
	srd_bp->set_container_name(managed_util::fromSS(radDownList->Text));

	ControlManager::get()->changePropertyLooter(managed_util::fromSS(radDownList->Text), managed_util::fromSS(PageBps->SelectedPage->Text), "text");
}
Telerik::WinControls::UI::RadDropDownList^ Looter::getBpCurrentDropDownList(){
	array<Control^>^ getRadDownList = PageBps->SelectedPage->Controls->Find("box_" + PageBps->SelectedPage->Text, true);
	return (Telerik::WinControls::UI::RadDropDownList^) getRadDownList[0];
}

int Looter::getCurrentGroup(){
	return dropDownLootGroup->SelectedIndex;
}

void Looter::disableFieldsUpdate(bool state){
	static bool last_state = false;
	if (last_state == state)
		return;
}

LooterContainerPtr Looter::getSelectedContainer(){
	if (!PageBps->SelectedPage)
		return nullptr;
	return LooterManager::get()->get_looter_container_by_id(managed_util::fromSS(PageBps->SelectedPage->Text));
}

LooterItemPtr Looter::getSelectedItem(){
	Telerik::WinControls::UI::RadListView^ list = get_current_listview();

	if (!list || !list->SelectedItem || !list->SelectedItems->Count)
		return nullptr;
	LooterContainerPtr container = getSelectedContainer();
	if (!container)
		return nullptr;

	String^ row_id = (String^)list->SelectedItem->Tag;
	return container->GetLooterRuleById(managed_util::fromSS(row_id));
}

LooterItemNonSharedPtr Looter::getSelectedItemNonShared(){
	int current_group = getCurrentGroup();
	if (current_group < 0)
		return nullptr;

	LooterItemPtr selected_item = getSelectedItem();
	if (!selected_item){
		radGroupBox1->Enabled = false;
		return nullptr;
	}
	radGroupBox1->Enabled = true;

	return selected_item->get_at_group(current_group);
}

System::Void Looter::ListView1ItemChanged(System::Object^  sender, System::EventArgs^  e) {
	Telerik::WinControls::UI::RadListViewElement^ list = (Telerik::WinControls::UI::RadListViewElement^)sender;
	updateSelectedData();
}

void Looter::updateSelectedData(){


	LooterItemPtr item = getSelectedItem();
	if (!item){
		return;
	}


	LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
	if (!nomshared)
		return;
	

	disable_item_update = true;
	tx_nameitem->Text = gcnew String(&item->get_name()[0]);
	dropDownLootImportant->SelectedIndex = nomshared->important;
	tx_minqty->Value = nomshared->minimun_count;
	ListCondition->Items->Clear();
	

	updateSelectedItemChangeMain();
	

	UpdateGif();
	

	disable_item_update = false;
}

System::Void Looter::dropDownLootGroup_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	updateSelectedData();
}

System::Void Looter::dropDownLootImportant_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (disable_item_update)
		return;

	LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
	if (!nomshared)
		return;

	nomshared->set_important((loot_item_priority_t)dropDownLootImportant->SelectedIndex);
}

System::Void Looter::tx_minqty_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
	if (!nomshared)
		return;

	nomshared->set_minimun_count((int)tx_minqty->Value);
}

System::Void Looter::dropDownLootSequence_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (disable_item_update)
		return;

	LooterManager::get()->set_current_sequence_type((loot_sequence_t)dropDownLootSequence->SelectedIndex);
}

System::Void Looter::dropDownLootFilterMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	LooterManager::get()->set_current_filter_type((loot_filter_t)dropDownLootFilterMode->SelectedIndex);
}

void Looter::UpdateGif(){
	itemPictureBox->Image = managed_util::get_item_image(tx_nameitem->Text, itemPictureBox->Height);
}

System::Void Looter::tx_nameitem_TextChanged(System::Object^  sender, System::EventArgs^  e) {

	UpdateGif();

	if (disable_item_update)
		return;

	auto list = get_current_listview();
	if (!list)
		return;

	if (!list->SelectedItem)
		return;


	Telerik::WinControls::UI::ListViewDataItem^ radItem = (Telerik::WinControls::UI::ListViewDataItem^)list->SelectedItem;
	list->SelectedItem[0] = tx_nameitem->Text;
	list->SelectedItem->Image = managed_util::get_item_image(tx_nameitem->Text, 20);

	auto selected = getSelectedItem();
	if (!selected)
		return;

	selected->set_name(managed_util::fromSS(tx_nameitem->Text));
}

System::Void Looter::Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e){
	this->PageBps->SelectedPage->TextChanged += gcnew System::EventHandler(this, &Looter::PageRename_TextChanged);
	text = e->Value->ToString();
}

System::Void Looter::PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e){
	String^ newValue = PageBps->SelectedPage->Text;
	String^ oldValue = text;

	/*PageBps->SelectedPage->Text = oldValue;
	return;

	String^ newValue = PageBps->SelectedPage->Text;
	String^ oldValue = text;*/

	if (newValue == "" || oldValue == ""){
		PageBps->SelectedPage->Text = oldValue;
		return;
	}

	if (newValue == oldValue){
		PageBps->SelectedPage->Text = oldValue;
		return;
	}

	int count = 0;
	for each(auto items in PageBps->Pages){
		if (items->Text == newValue){
			if (count == 0)
				count++;
			else{
				PageBps->SelectedPage->Text = oldValue;
				return;
			}
		}
	}

	std::string newName = managed_util::to_std_string(newValue);
	std::string oldName = managed_util::to_std_string(oldValue);

	String^ list_name = "list_" + text;
	array<Control^>^ getRadListView = this->PageBps->SelectedPage->Controls->Find(list_name, true);

	if (!getRadListView->Length){
		PageBps->SelectedPage->Text = oldValue;
		return;
	}
	
	if (!LooterManager::get()->change_bp_id(newName, oldName)){
		PageBps->SelectedPage->Text = oldValue;
		return;
	}

	auto radlistview = (Telerik::WinControls::UI::RadListView^)getRadListView[0];

	text = newValue;

	radlistview->Name = "list_" + newValue;

	ControlManager::get()->changePropertyLooter(newName, oldName, "name");
	ControlManager::get()->changePropertyLooter(newName, oldName + "label", "text");
	ControlManager::get()->changePropertyLooter(newName + "label", oldName + "label", "name");
}
void Looter::update_idiom_listbox(){
	dropDownLootSequence->BeginUpdate();
	dropDownLootSequence->Items->Clear();
	for (int i = 0; i < loot_sequence_t::loot_sequence_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_sequence_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootSequence->Items->Add(item);
	}
	dropDownLootSequence->SelectedIndex = LooterManager::get()->get_current_sequence_type();
	dropDownLootSequence->EndUpdate();
	
	dropDownLootFilterMode->BeginUpdate();
	dropDownLootFilterMode->Items->Clear();
	for (int i = 0; i < loot_filter_t::loot_filter_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_filter_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootFilterMode->Items->Add(item);
	}
	dropDownLootFilterMode->SelectedIndex = LooterManager::get()->get_current_filter_type();
	dropDownLootFilterMode->EndUpdate();


	dropDownLootType->BeginUpdate();
	dropDownLootType->Items->Clear();
	for (int i = 0; i < loot_type_t::loot_type_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_type_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootType->Items->Add(item);
	}
	dropDownLootType->SelectedIndex = 0;
	dropDownLootType->EndUpdate();


	dropDownLootAction->BeginUpdate();
	dropDownLootAction->Items->Clear();
	for (int i = 0; i < loot_item_action_t::loot_item_action_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_item_action_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootAction->Items->Add(item);
	}
	dropDownLootAction->EndUpdate();


	dropDownLootConditionADV->BeginUpdate();
	dropDownLootConditionADV->Items->Clear();
	for (int i = 0; i < loot_item_new_condition_t::loot_item_new_condition_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_item_new_condition_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootConditionADV->Items->Add(item);
	}
	dropDownLootConditionADV->EndUpdate();

	dropDownLootImportant->BeginUpdate();
	dropDownLootImportant->Items->Clear();
	for (int i = 0; i < loot_item_priority_t::loot_item_priority_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("loot_item_priority_t_") + std::to_string(i));
		item->Value = i;
		dropDownLootImportant->Items->Add(item);
	}
	dropDownLootImportant->SelectedIndex = 0;
	dropDownLootImportant->EndUpdate();
	
	box_condition_take_skin->BeginUpdate();
	box_condition_take_skin->Items->Clear();
	for (int i = 0; i < take_skin_condition_t::take_skin_condition_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("take_skin_condition_t_") + std::to_string(i));
		item->Value = i;
		box_condition_take_skin->Items->Add(item);
	}
	box_condition_take_skin->EndUpdate();
}

void Looter::update_idiom(){
	radLabel6->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel6->Text))[0]);
	radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
	radLabel5->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel5->Text))[0]);
	radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
	radLabel15->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel15->Text))[0]);
	radLabel16->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel16->Text))[0]);
	radLabel17->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel17->Text))[0]);
	radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
	bt_refresh_slots->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_refresh_slots->Text))[0]);
	bt_refresh_backpack->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_refresh_backpack->Text))[0]);
	bt_refresh_tile->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_refresh_tile->Text))[0]);
	list_take_skin->Text = gcnew String(&GET_TR(managed_util::fromSS(list_take_skin->Text))[0]);
	bt_add->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_add->Text))[0]);
	bt_add_condition->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_add_condition->Text))[0]);
	bt_delete_condition->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete_condition->Text))[0]);
	radLabel14->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel14->Text))[0]);
	radLabel11->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel11->Text))[0]);
	CheckBoxRecoorder->Text = gcnew String(&GET_TR(managed_util::fromSS(CheckBoxRecoorder->Text))[0]);
	bt_new_take_skin->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_new_take_skin->Text))[0]);
	bt_delete_take_skin->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete_take_skin->Text))[0]);
	radLabel12->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel12->Text))[0]);
	radLabel13->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel13->Text))[0]);
	bt_delete->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete->Text))[0]);
	radGroupBox2->Text = gcnew String(&GET_TR(managed_util::fromSS(radGroupBox2->Text))[0]);
	bt_advanced->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_advanced->Text))[0]);
	bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
	bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);
	check_min_cap->Text = gcnew String(&GET_TR(managed_util::fromSS(check_min_cap->Text))[0]);
	radLabel9->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel9->Text))[0]);
	check_force_move->Text = gcnew String(&GET_TR(managed_util::fromSS(check_force_move->Text))[0]);
	check_force_move->Text = gcnew String(&GET_TR(managed_util::fromSS(check_force_move->Text))[0]);
	radPageViewPage1->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPage1->Text))[0]);
	radPageViewPage2->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewPage2->Text))[0]);
	page_take_skin->Text = gcnew String(&GET_TR(managed_util::fromSS(page_take_skin->Text))[0]);
	radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);
	check_auto_reopen_containers->Text = gcnew String(&GET_TR(managed_util::fromSS(check_auto_reopen_containers->Text))[0]);
	check_open_next_container->Text = gcnew String(&GET_TR(managed_util::fromSS(check_open_next_container->Text))[0]);
	check_eat_food_from_corpses->Text = gcnew String(&GET_TR(managed_util::fromSS(check_eat_food_from_corpses->Text))[0]);
	radLabel8->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel8->Text))[0]);
	radMenuItem2->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem2->Text))[0]);

	for each(auto colum in ListCondition->Columns)
		colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);

	for each(auto colum in list_take_skin->Columns)
		colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
}

System::Void Looter::check_eat_food_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	//LuaBackgroundManager::get()->updateActiveTools(managed_util::fromSS(check_eat_food->Name), check_eat_food->Checked);
	//LuaBackgroundManager::get()->updateDelayTools(managed_util::fromSS(check_eat_food->Name), 60000);
}

System::Void Looter::bt_refill_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	//LuaBackgroundManager::get()->updateActiveTools(managed_util::fromSS(bt_refill->Name), bt_refill->Checked);
}

System::Void  Looter::check_anti_idle_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	if (disable_item_update)
		return;
	//LuaBackgroundManager::get()->updateActiveTools(managed_util::fromSS(check_anti_idle->Name), check_anti_idle->Checked);
	//LuaBackgroundManager::get()->updateDelayTools(managed_util::fromSS(check_anti_idle->Name), 30000);
}

bool Looter::save_script(std::string file_name){
	Json::Value file;
	file["version"] = 0100;

	file["LooterManager"] = LooterManager::get()->parse_class_to_json();
	file["TakeSkinManager"] = TakeSkinManager::get()->parse_class_to_json();

	return saveJsonFile(file_name, file);
}

bool Looter::load_script(std::string file_name){
	Json::Value file = loadJsonFile(file_name);
	if (file.isNull())
		return false;

	if (!file["LooterManager"].isNull() && !file["LooterManager"].empty())
		LooterManager::get()->parse_json_to_class(file["LooterManager"]);

	if (!file["TakeSkinManager"].isNull() && !file["TakeSkinManager"].empty())
		TakeSkinManager::get()->parse_json_to_class(file["TakeSkinManager"]);

	return true;
}

void Looter::creatContextMenu(){
}

System::Void Looter::MenuNew_Click(Object^ sender, EventArgs^ e){

	Telerik::WinControls::UI::RadMenuItem^ ItemAdd = (Telerik::WinControls::UI::RadMenuItem^)sender;

	std::vector<std::string> vector_itens;

	std::shared_ptr<DepotContainer> Item = DepoterManager::get()->getDepotContainer(managed_util::fromSS(ItemAdd->Text));
	auto& myMapDepot = Item->getMapDepoItens();

	for (auto it_ = myMapDepot.begin(); it_ != myMapDepot.end(); it_++)
		vector_itens.push_back(it_->second->itemName);

	if (std::find(vector_itens.begin(), vector_itens.end(), getSelectedItem()->get_name()) == vector_itens.end()){
		Item->addDepotItem(getSelectedItem()->get_name(), 0, 0);
	}
}

System::Void Looter::MenuAddAll_Click(Object^ sender, EventArgs^ e){
	std::vector<std::string> vector_itens;

	Telerik::WinControls::UI::RadMenuItem^ ItemAdd = (Telerik::WinControls::UI::RadMenuItem^)sender;
	std::shared_ptr<DepotContainer> Item = DepoterManager::get()->getDepotContainer(managed_util::fromSS(ItemAdd->Text));
	auto& myMapDepot = Item->getMapDepoItens();
	auto items = LooterManager::get()->containers[managed_util::fromSS(PageBps->SelectedPage->Text)]->items;

	for (auto it_ = myMapDepot.begin(); it_ != myMapDepot.end(); it_++)
		vector_itens.push_back(it_->second->itemName);

	for (auto it = items.begin(); it != items.end(); it++){
		if (std::find(vector_itens.begin(), vector_itens.end(), it->second->get_name()) == vector_itens.end()){
			Item->addDepotItem(it->second->get_name(), 0, 0);
		}
	}
}


System::Void Looter::radMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
	uint32_t page_count = PageBps->Pages->Count;
	for (uint32_t i = 0; i < page_count; i++){
		ItemContainerPtr container_ptr = ContainerManager::get()->get_container_by_index(i);
		if (!container_ptr){
			Telerik::WinControls::RadMessageBox::Show(this,"HANDLE FAIL TO ASSING CONTAINERS");
			break;
		}

		String^ tibia_container_name = (gcnew String(&container_ptr->get_caption()[0]))->ToLower();

		array<Control^>^ getRadDownList = PageBps->Pages[i]->Controls->Find("box_" + PageBps->Pages[i]->Text, true);
		
	
		int bp_index = 0;
		Telerik::WinControls::UI::RadListDataItemCollection^ items = ((Telerik::WinControls::UI::RadDropDownList^) getRadDownList[0])->Items;
		for each(Telerik::WinControls::UI::RadListDataItem^ item in items){
			
			if (item->Text->ToLower() == tibia_container_name){
				((Telerik::WinControls::UI::RadDropDownList^) getRadDownList[0])->SelectedIndex = bp_index;
				break;
			}
			bp_index++;
		}
	}

}

System::Void Looter::radSpinEditorMaxEatFood_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;
}

System::Void Looter::radSpinEditorMinEatFood_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;
}

	System::Void Looter::box_item_id_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 //LuaBackgroundManager::get()->updateitem_id(managed_util::fromSS(bt_refill->Name), managed_util::fromSS(box_item_id->Text));
	}

System::Void Looter::radSpinEditorMaxEatFoodFromCorpses_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;
	LooterManager::get()->set_eat_from_corpses_delays(std::pair<uint32_t, uint32_t>((uint32_t)radSpinEditorMinEatFoodFromCorpses->Value, (uint32_t)radSpinEditorMaxEatFoodFromCorpses->Value));
}

System::Void Looter::radSpinEditorMinEatFoodFromCorpses_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;
	LooterManager::get()->set_eat_from_corpses_delays(std::pair<uint32_t, uint32_t>((uint32_t)radSpinEditorMinEatFoodFromCorpses->Value, (uint32_t)radSpinEditorMaxEatFoodFromCorpses->Value));
}

System::Void Looter::check_eat_food_from_corpses_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	if (disable_item_update)
		return;

	LooterManager::get()->set_eat_from_corpses_state(check_eat_food_from_corpses->Checked);
}
System::Void Looter::check_drop_pot_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;
				// LuaBackgroundManager::get()->updateActiveTools(managed_util::fromSS(check_drop_pot->Name), check_drop_pot->Checked);
				// LuaBackgroundManager::get()->updateDelayTools(managed_util::fromSS(check_drop_pot->Name), 30000);
	}








































/////////////////////////////////////////////////////////////**********************

///////////////////////////////////////////////////////////
/////////////////*/

System::Void Looter::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	if (!PageBps->SelectedPage)
	{
		Telerik::WinControls::RadMessageBox::Show(this, "Handle errro must select backpack");
		return;
	}
	bool check_exist;
	NeutralBot::AutoGenerateLoot^ tempForm = gcnew NeutralBot::AutoGenerateLoot("Looter");
	tempForm->ShowDialog();
	if (tempForm->comfirmed){
		for each(String^ item in tempForm->selectedItems){
			loot_item_condition_t condition = (loot_item_condition_t)0;// (loot_item_new_condition_t)(int)dropDownLootCondition->SelectedItem->Value;
			loot_item_priority_t important = (loot_item_priority_t)3;// (loot_item_priority_t)(int)dropDownLootImportant->SelectedItem->Value;
			int value = 0;// Convert::ToInt32(tx_value->Value);
			int group = 0;//dropDownLootGroup->SelectedIndex;//TODO
			int minqty = 0;//Convert::ToInt32(tx_minqty->Value);
			check_exist = false;

			if (item == String::Empty)
				continue;

			if (!managed_util::check_exist_item(item->ToLower())){
				continue;
			}


			for each(auto list_item in get_current_listview()->Items){
				if (list_item->Text->ToLower() == item->ToLower()){
					check_exist = true;
					break;
				}
			}

			if (check_exist)
				continue;

			addItemOnView(PageBps->SelectedPage->Text, item, condition, important, value, group, minqty);
		}
	}
}

void Looter::close_button(){
	if (PageBps->Pages->Count <= 1)
		(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
	else
		(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
}

System::Void Looter::bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::SaveFileDialog fileDialog;
	fileDialog.Filter = "Looter Script|*.Loot";
	fileDialog.Title = "Save Script";

	System::Windows::Forms::DialogResult result = fileDialog.ShowDialog();

	if (result == System::Windows::Forms::DialogResult::Cancel)
		return;

	String^ path = fileDialog.FileName;
	if (!save_script(managed_util::fromSS(path)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Saved", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved", "Save/Load");
}
System::Void Looter::bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::OpenFileDialog fileDialog;
	fileDialog.Filter = "Looter Script|*.Loot";
	fileDialog.Title = "Open Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	PageBps->Pages->Clear();
	list_take_skin->Items->Clear();
	LooterManager::get()->clear();

	if (!load_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

	loadAll();
}

System::Void Looter::check_open_next_container_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
		LooterManager::get()->set_open_next_container(check_open_next_container->IsChecked);
	}
	System::Void Looter::check_auto_reopen_containers_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
		LooterManager::get()->set_auto_reopen_container(check_auto_reopen_containers->IsChecked);
	}
	System::Void Looter::radSpinEditorMaxEatFoodFromCorpses_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		try{
			int max = Convert::ToInt32(radSpinEditorMaxEatFoodFromCorpses->Value);
			int min = Convert::ToInt32(radSpinEditorMinEatFoodFromCorpses->Value);
			LooterManager::get()->set_eat_from_corpses_delays(std::pair<uint32_t, uint32_t>(min, max));
		}
		catch (...){
		}
	}
	System::Void Looter::PageBps_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e) {
		auto listview = get_current_listview();

		if (!listview)
			return;

		if (listview->Items->Count <= 0)
			radGroupBox1->Enabled = false;
		else
			radGroupBox1->Enabled = true;

		updateSelectedData();
	}

	System::Void Looter::dropDownLootType_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
		if (disable_item_update)
			return;

		//LooterManager::get()->set_current_sequence_type((loot_sequence_t)dropDownLootType->SelectedIndex);
	}
	Telerik::WinControls::UI::RadListView^ Looter::get_current_listview(){
				 auto selected_page = this->PageBps->SelectedPage;

				 if (!selected_page)
					 return nullptr;

				 String^ list_name = "list_" + PageBps->SelectedPage->Text;
				 array<Control^>^ getRadListView = selected_page->Controls->Find(list_name, true);

				 if (!getRadListView->Length)
					 return nullptr;

				 return (Telerik::WinControls::UI::RadListView^) getRadListView[0];
			 }














	void Looter::updateSelectedItemTakeSkin(){
				 if (disable_item_update)
					 return;

				 if (!list_take_skin->SelectedItem)
					 return;

				 std::map<uint32_t, std::shared_ptr<TakeSkinStruct>> mapTakeSkin = TakeSkinManager::get()->getMapTakeSkin();
				 int corpes_id = Convert::ToInt32(list_take_skin->SelectedItem->Text);

				 if (!mapTakeSkin[corpes_id])
					 return;

				 disable_item_update = true;				
				 tx_corposes_id->Text = Convert::ToString((int)mapTakeSkin[corpes_id]->corpse_id);
				 tx_item_id_take_skin->Text = Convert::ToString((int)mapTakeSkin[corpes_id]->item_id);
				 box_condition_take_skin->SelectedIndex = (int)mapTakeSkin[corpes_id]->condition_index;
				 disable_item_update = false;
			 }



	System::Void Looter::bt_new_take_skin_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!radGroupBox4->Enabled || !check_take_skin->Enabled){
			radGroupBox4->Enabled = true;
			check_take_skin->Enabled = true;
		}
		array<String^>^ columnArrayItems = { "0", "0", "take_skin_condition_t_0" };
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(tx_corposes_id->Text, columnArrayItems);
		newItem->Text = Convert::ToString(TakeSkinManager::get()->request_new_id());
		list_take_skin->Items->Add(newItem);
		list_take_skin->SelectedItem = newItem;

		updateSelectedItemTakeSkin();
	}
	System::Void Looter::bt_delete_take_skin_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!list_take_skin->SelectedItem)
			return;
		
		int current_id = Convert::ToInt32(list_take_skin->SelectedItem->Text);
		if (!TakeSkinManager::get()->remove_in_map(current_id))
			return;

		list_take_skin->Items->Remove(list_take_skin->SelectedItem);
		
		if (list_take_skin->Items->Count <= 0){
			radGroupBox4->Enabled = false;
			check_take_skin->Enabled = false;
		}
	}




	System::Void Looter::tx_corposes_id_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
		if (!Char::IsDigit(e->KeyChar) && e->KeyChar != (char)8)
			e->Handled = true;
	}
	System::Void Looter::take_skin_list_ItemChanged(System::Object^  sender, System::EventArgs^  e) {
		updateSelectedItemTakeSkin();
	}
	System::Void Looter::tx_corposes_id_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		if (!list_take_skin->SelectedItem)
			return;

		std::map<uint32_t, std::shared_ptr<TakeSkinStruct>> mapTakeSkin = TakeSkinManager::get()->getMapTakeSkin();

		disable_item_update = true;
		int map_id = Convert::ToInt32(list_take_skin->SelectedItem->Text);
		list_take_skin->SelectedItem["Corposes Id"] = tx_corposes_id->Text;
		int corporses_id = 0;
		try{
			corporses_id = Convert::ToInt32(tx_corposes_id->Text->Trim());
		}
		catch (...){
			corporses_id = 0;
		}

		TakeSkinManager::get()->change_corposes_id(map_id, corporses_id);
		disable_item_update = false;
	}
	System::Void Looter::box_condition_take_skin_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
		if (!list_take_skin->SelectedItem)
			return;

		std::map<uint32_t, std::shared_ptr<TakeSkinStruct>> mapTakeSkin = TakeSkinManager::get()->getMapTakeSkin();

		disable_item_update = true;
		int map_id = Convert::ToInt32(list_take_skin->SelectedItem->Text);
		list_take_skin->SelectedItem["Condition"] = box_condition_take_skin->Text;
		int condition_index = box_condition_take_skin->SelectedIndex;

		TakeSkinManager::get()->change_condition_index(map_id, condition_index);
		disable_item_update = false;
	}
	System::Void Looter::radCheckBox1_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
		TakeSkinManager::get()->set_take_skin_state(check_take_skin->Checked);
	}
	System::Void Looter::tx_item_id_take_skin_TextChanged(System::Object^  sender, System::EventArgs^  e) {
		if (!list_take_skin->SelectedItem)
			return;

		std::map<uint32_t, std::shared_ptr<TakeSkinStruct>> mapTakeSkin = TakeSkinManager::get()->getMapTakeSkin();

		disable_item_update = true;
		int map_id = Convert::ToInt32(list_take_skin->SelectedItem->Text);
		list_take_skin->SelectedItem["Item Id"] = tx_item_id_take_skin->Text->Trim();
		int corporses_id;

		try{
			corporses_id = Convert::ToInt32(tx_item_id_take_skin->Text->Trim());
		}
		catch (...){
			corporses_id = 0;
		}


		TakeSkinManager::get()->change_item_id(map_id, corporses_id);
		disable_item_update = false;
	}
	System::Void Looter::list_take_skin_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
		if (!list_take_skin->SelectedItem){
			e->Cancel = true;
			return;
		}

		int current_id = Convert::ToInt32(list_take_skin->SelectedItem->Text);
		if (!TakeSkinManager::get()->remove_in_map(current_id)){
			e->Cancel = true;
			return;
		}

		if (list_take_skin->Items->Count <= 0){
			radGroupBox4->Enabled = false;
			check_take_skin->Enabled = false;
		}
	}
	System::Void Looter::spin_time_to_clean_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (disable_item_update)
			return;

		LooterManager::get()->set_time_to_clean((int)spin_time_to_clean->Value);
	}
	System::Void Looter::spin_max_distance_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	LooterManager::get()->set_max_distance((int)spin_max_distance->Value);
}

	System::Void Looter::bt_advanced_Click(System::Object^  sender, System::EventArgs^  e) {
		if (!panel_state){
			this->panel_adv->Show();
			panel_state = true; 
			this->Size = System::Drawing::Size(610, 530);
			return;
		}

		if (panel_state){
			this->panel_adv->Hide();
			panel_state = false;
			this->Size = System::Drawing::Size(610, 310);
			return;
		}

	}





	System::Void Looter::ListAlerts_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
		updateSelectedItemCondition();
	}
	System::Void Looter::ListAlerts_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
		if (disable_item_update)
			return;
		if (!ListCondition->SelectedItem)
			return;
		LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
		if (!nomshared)
			return;

		disable_condition();

		int index = ListCondition->Items->IndexOf(ListCondition->SelectedItem);
		nomshared->remove_vector_condition(index);
	}

	System::Void Looter::bt_delete_condition_Click(System::Object^  sender, System::EventArgs^  e) {
		if (disable_item_update)
			return;

		if (!ListCondition->SelectedItem)
			return;

		LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
		if (!nomshared)
			return;

		disable_item_update = true;
		int index = ListCondition->Items->IndexOf(ListCondition->SelectedItem);
		nomshared->remove_vector_condition(index);
		ListCondition->Items->Remove(ListCondition->SelectedItem);
		disable_condition();
		disable_item_update = false;
	}
	System::Void Looter::bt_add_condition_Click(System::Object^  sender, System::EventArgs^  e) {
		addItemOnConditionList((loot_item_action_t)0, (loot_item_new_condition_t)0, 0);
	}

	void Looter::updateSelectedItemCondition(){
				 if (disable_item_update)
					 return;

				 if (!ListCondition->SelectedItem)
					 return;

				 LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
				 if (!nomshared)
					 return;

				 std::vector<std::shared_ptr<LooterConditions>> vector_condition = nomshared->get_vector_condition();

				 disable_item_update = true;
				 int index = ListCondition->Items->IndexOf(ListCondition->SelectedItem);
				 dropDownLootAction->SelectedIndex = vector_condition[index]->action;
				 dropDownLootConditionADV->SelectedIndex = vector_condition[index]->condition;
				 tx_valueADV->Value = vector_condition[index]->value;
				 disable_condition();

				 disable_item_update = false;
			 }
	void Looter::addItemOnConditionList(loot_item_action_t action, loot_item_new_condition_t condition, int value){
				 if (disable_item_update)
					 return;

				 LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
				 if (!nomshared)
					 return;

				 dropDownLootAction->SelectedIndex = 0;
				 dropDownLootConditionADV->SelectedIndex = 0;
				 tx_valueADV->Value = 0;

				 nomshared->add_vector_condition(action, condition, value);

				 array<String^>^ columnArrayItems = { dropDownLootAction->Text, dropDownLootConditionADV->Text, Convert::ToString(tx_valueADV->Value) };
				 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(nomshared->get_vector_condition().size() - 1), columnArrayItems);

				 ListCondition->Items->Add(newItem);
				 ListCondition->SelectedItem = newItem;
				 disable_condition();
			 }

	System::Void Looter::dropDownLootConditionADV_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
		if (disable_item_update)
			return;

		if (!ListCondition->SelectedItem)
			return;

		LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
		if (!nomshared)
			return;

		disable_item_update = true;
		int index = ListCondition->Items->IndexOf(ListCondition->SelectedItem);
		nomshared->vector_condition[index]->condition = (loot_item_new_condition_t)dropDownLootConditionADV->SelectedIndex;
		ListCondition->SelectedItem["Condition"] = dropDownLootConditionADV->Text;
		disable_item_update = false;
	}
	System::Void Looter::tx_valueADV_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
		if (disable_item_update)
			return;

		if (!ListCondition->SelectedItem)
			return;

		LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
		if (!nomshared)
			return;

		std::vector<std::shared_ptr<LooterConditions>> vector_condition = nomshared->get_vector_condition();

		disable_item_update = true;
		int index = ListCondition->Items->IndexOf(ListCondition->SelectedItem);
		ListCondition->SelectedItem["Value"] = tx_valueADV->Value;
		nomshared->vector_condition[index]->value = (int)tx_valueADV->Value;
		disable_item_update = false;
	}

	void Looter::disable_condition(){
				 if (ListCondition->Items->Count <= 0 && radGroupBox5->Enabled){
					 radGroupBox5->Enabled = false;
					 radGroupBox6->Enabled = false;
				 }
				 else if (ListCondition->Items->Count > 0 && !radGroupBox5->Enabled){
					 radGroupBox5->Enabled = true;
					 radGroupBox6->Enabled = true;
				 }
			 }
	void Looter::updateSelectedItemChangeMain(){
				 LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
				 if (!nomshared)
					 return;

				 disable_item_update = true;
				 auto myMapTakeSkin = nomshared->get_vector_condition();
				 int index = 0;
				 for (auto item : myMapTakeSkin){
					 array<String^>^ columnArrayItems = { GET_MANAGED_TR(std::string("loot_item_action_t_") + std::to_string(item->action)), GET_MANAGED_TR(std::string("loot_item_new_condition_t_") + std::to_string(item->condition)), Convert::ToString(item->value) };
					 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem(Convert::ToString(index), columnArrayItems);
					 newItem->Tag = (Convert::ToString(index));
					 ListCondition->Items->Add(newItem);
					 index++;
				 }				
				 disable_item_update = false;

				 updateSelectedItemCondition();
				 disable_condition();				
			 }
	System::Void Looter::dropDownLootAction_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	if (disable_item_update)
		return;

	if (!ListCondition->SelectedItem)
		return;

	LooterItemNonSharedPtr nomshared = getSelectedItemNonShared();
	if (!nomshared)
		return;

	disable_item_update = true;
	int index = ListCondition->Items->IndexOf(ListCondition->SelectedItem);
	ListCondition->SelectedItem["Action"] = dropDownLootAction->Text;
	nomshared->vector_condition[index]->action = (loot_item_action_t)dropDownLootAction->SelectedIndex;
	disable_item_update = false;
}
	System::Void Looter::radToggleButtonLooter_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {

	if (!radToggleButtonLooter->IsChecked){
		return;
	}
	check_remove_duplicates(true);
}