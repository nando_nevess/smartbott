#include "Core/Util.h"

static enum lua_id_t{ LUA_BACKGROUND1, LUA_BACKGROUND2, LUA_BACKGROUND3, LUA_WAYPOINTER, LUA_SETUP, LUA_HUD };

class LuaLogger{
public:
	boost::mutex mtx_access;;
	
	void lock(){
		mtx_access.lock();
	}

	void unlock(){
		mtx_access.unlock();
	}

	void addError(lua_id_t id, std::string error){
		lock();
		int current_size = mLogBuffer[id].size();
		if (current_size >= 50)
			mLogBuffer[id].pop_back();

		mLogBuffer[id].push_front(error);
		unlock();
	}

	std::list<std::string>& getList(lua_id_t id){
		return mLogBuffer[id];
	}

	std::map<lua_id_t, std::list<std::string>/* lua errors */> mLogBuffer;
};