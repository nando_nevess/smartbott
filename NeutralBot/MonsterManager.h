#pragma once
#include "Core\Util.h"
#include <json\json.h>

struct MonsterStruct{
	std::string name;
	DEFAULT_GET_SET(std::string, name);
	std::vector<std::string> items_id;
};

class MonsterManager{
	std::map<std::string, std::shared_ptr<MonsterStruct>> mapMonsterStruct;
	std::vector<std::string> MonsterList;

	MonsterManager();
	
public:

	std::shared_ptr<MonsterStruct> get_creature_by_name(std::string name);

	void addinvector(std::string monstername);

	std::vector<std::string> get_MonsterList();

	std::map<std::string, std::shared_ptr<MonsterStruct>> get_mapMonsterStruct();

	Json::Value parse_class_to_json();

	void parse_json_to_class(Json::Value jsonObject);

	bool load_script(std::string file_name);

	bool saveJsonFile(std::string file, Json::Value json);

	bool save_script(std::string file_name);

	static MonsterManager* get();
};