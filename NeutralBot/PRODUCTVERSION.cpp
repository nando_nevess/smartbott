#pragma once
#include "PRODUCTVERSION.h"

bool GetProductVersion(std::string libPath, uint16_t *MajorVersion, uint16_t *MinorVersion, uint16_t *BuildNumber, uint16_t *RevisionNumber){

	DWORD dwHandle, dwLen;
	UINT BufLen;
	LPTSTR lpData;
	VS_FIXEDFILEINFO *pFileInfo;

	dwLen = GetFileVersionInfoSize(&(libPath)[0], &dwHandle);
	if (!dwLen)
		return false;

	lpData = (LPTSTR)malloc(dwLen);
	if (!lpData)
		return false;

	if (!GetFileVersionInfo(&libPath[0], dwHandle, dwLen, lpData)){
		free(lpData);
		return false;
	}
	if (VerQueryValue(lpData, "\\", (LPVOID *)&pFileInfo, (PUINT)&BufLen)){
		*MajorVersion = HIWORD(pFileInfo->dwFileVersionMS);
		*MinorVersion = LOWORD(pFileInfo->dwFileVersionMS);
		*BuildNumber = HIWORD(pFileInfo->dwFileVersionLS);
		*RevisionNumber = LOWORD(pFileInfo->dwFileVersionLS);

		free(lpData);
		return true;
	}
	free(lpData);
	return false;
}

bool GetProductVersion(std::string libPath, PRODUCT_VERSION& version){
	return GetProductVersion(libPath, &version.MajorVersion, &version.MinorVersion, &version.BuildNumber,
		&version.RevisionNumber);
}

PRODUCT_VERSION::PRODUCT_VERSION(){
	MajorVersion = 0;
	MinorVersion = 0;
	BuildNumber = 0;
	RevisionNumber = 0;
}
PRODUCT_VERSION::PRODUCT_VERSION(uint16_t MajorVersion, uint16_t MinorVersion, uint16_t BuildNumber, uint16_t RevisionNumber){
	this->MajorVersion = MajorVersion;
	this->MinorVersion = MinorVersion;
	this->BuildNumber = BuildNumber;
	this->RevisionNumber = RevisionNumber;
}

uint64_t PRODUCT_VERSION::GetUint32Version(){
	uint16_t data[4];
	data[3] = MajorVersion;
	data[2] = MinorVersion;
	data[1] = BuildNumber;
	data[0] = RevisionNumber;
	return *(uint64_t*)data;
}

uint32_t PRODUCT_VERSION::GetTibiaVersion(uint32_t pid){
	HANDLE hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ, FALSE, pid);

	if (!hProcess)
		return 0;

	char process_name[MAX_PATH];
	memset(process_name, 0, sizeof(process_name));

	if (NULL == hProcess)
		return 0;

	HMODULE hMod;
	DWORD cbNeeded;

	if (!EnumProcessModules(hProcess, &hMod, sizeof(hMod), &cbNeeded))
		return 0;

	if (GetModuleFileNameEx(hProcess, NULL, process_name, MAX_PATH) == 0)
		return 0;

	PRODUCT_VERSION ver;
	if (!GetProductVersion(process_name, ver))
		return 0;

	std::string versionStr = std::to_string((long long)ver.MajorVersion) + std::to_string((long long)ver.MinorVersion) + std::to_string((long long)ver.BuildNumber);
	uint32_t version = std::stoi(versionStr);

	return version;
}