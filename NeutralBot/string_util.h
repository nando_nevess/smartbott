#pragma once
#include <algorithm> 
#include <functional> 
#include <cctype>
#include <locale>


	namespace string_util{
		// trim from start
		std::string &ltrim(std::string &s);

		// trim from end
		std::string &rtrim(std::string &s);

		// trim from both ends
		std::string &trim(std::string &s);

		std::string trim2(std::string& str);

		std::string &lower(std::string &s);

		std::string create_secure_string(char *entrace);

		std::string create_secure_string(char &entrace);

		bool string_compare(std::string str1, std::string str2, bool ignore_case = false);

	}
