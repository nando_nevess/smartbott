#pragma once
#include "Spells.h"
#include "XMLSpellManager.h"
#include "MonsterManager.h"

using namespace Neutral;

void Spells::loadSpellsInfo(){
	//radDropDownList1->BeginUpdate();

	std::vector<XMLSpellInfo> spells = gXMLSpellManager.getspellsInfoList();
	for (XMLSpellInfo info : spells){
		int may_int = 0;
		String^ nm = gcnew String(&info.get_cast()[0]);

		if (System::Int32::TryParse(nm, may_int))
			radDropDownList1->Items->Add(gcnew String(&ItemsManager::get()->getItemNameFromId(may_int)[0]));
		else
			radDropDownList1->Items->Add(nm);

	}

	spellDropDownUseType->SelectedIndex = (int)SpellManager::get()->get_spell_type();
	spellDropDownspell_type->SelectedIndex = 0;

	//radDropDownList1->EndUpdate();
}

System::Void Spells::Spells_Load(System::Object^  sender, System::EventArgs^  e) {
	disable_notify = true;

	update_idiom();

	update_idiom_listbox();

	loadSpellManager();

	loadFilesLua();

	creatContextMenu();

	loadPotions();

	loadSpellsInfo();

	loadConditionAndActions();	
	
	boxMonsterName->BeginUpdate();
	std::vector<std::string> monsterlist = MonsterManager::get()->get_MonsterList();
	for (auto monster : monsterlist)
		boxMonsterName->Items->Add(gcnew String(monster.c_str()));
	boxMonsterName->EndUpdate();
		


	disable_notify = false;
	
	if (ListViewCondition->Items->Count <= 0)
		disable_page_condition();
	else
		enable_page_condition();

	this->radTreeView1->NodeMouseDoubleClick += gcnew Telerik::WinControls::UI::RadTreeView::TreeViewEventHandler(this, &Spells::radTreeView1_NodeMouseDoubleClick);
}

System::Void Spells::loadSpellManager() {
	std::map<int, SpellAttackPtr> attack_spell_list = SpellManager::get()->getAttackList();
	std::map<int, SpellHealthPtr> health_spell_list = SpellManager::get()->getHealthList();

	//spellSpellListHeal->BeginUpdate();
	for (auto spell : health_spell_list) {
		Telerik::WinControls::UI::ListViewDataItem^ listViewDataItem1 = (gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(spell.second->get_visual_name().c_str())));
		
		listViewDataItem1->CheckState = (Telerik::WinControls::Enumerations::ToggleState)spell.second->get_checked();
		spellSpellListHeal->Items->Add(listViewDataItem1);

		listViewDataItem1["Id"] = spell.first;
		listViewDataItem1["spell_type"] = static_cast<System::Int32^>(0);
	}
	//spellSpellListHeal->EndUpdate();

	//spellSpellListAttack->BeginUpdate();
	for (auto spell : attack_spell_list) {
		Telerik::WinControls::UI::ListViewDataItem^ listViewDataItem1 = (gcnew Telerik::WinControls::UI::ListViewDataItem(gcnew String(spell.second->get_visual_name().c_str())));

		listViewDataItem1->CheckState = (Telerik::WinControls::Enumerations::ToggleState) spell.second->get_checked();		
		this->spellSpellListAttack->Items->Add(listViewDataItem1);
		
		listViewDataItem1["Id"] = spell.first;
		listViewDataItem1["spell_type"] = (int)spell.second->get_spell_type();
	}

	if (spellSpellListHeal->Items->Count){
		spellSpellListHeal->SelectedIndex = -1;
		spellSpellListHeal->SelectedIndex = spellSpellListHeal->Items->Count - 1;
	}
	//spellSpellListAttack->EndUpdate();
}

System::Void Spells::spellAddButton_Click(System::Object^  sender, System::EventArgs^  e) {
	if (radPageView1->SelectedPage->Name == "radPageViewPage3")
		return;

	String^ spellId = "new spell";



	Telerik::WinControls::UI::ListViewDataItem^ listViewDataItem1 = (gcnew Telerik::WinControls::UI::ListViewDataItem(spellId));


	int spell_type = this->spellDropDownspell_type->SelectedIndex;
	if (radPageView1->Pages->IndexOf(radPageView1->SelectedPage) == 0){
		spell_type = 0;
		this->spellDropDownspell_type->SelectedIndex = 0;
	}
	else{
		if (this->spellDropDownspell_type->SelectedIndex < 1){
			this->spellDropDownspell_type->SelectedIndex = 1;
			spell_type = 1;
		}

	}

	int id;
	if (spell_type > 0) {
		id = SpellManager::get()->addAttackSpellInList();
		std::shared_ptr<SpellAttack> spell = SpellManager::get()->getAttackSpellInListById(id);
		spell->set_visual_name(msclr::interop::marshal_as<std::string>((String^)spellId));
		spell->set_spell_type((spell_type_t)spell_type);
	}
	else {
		id = SpellManager::get()->addNewHealthSpell();
		std::shared_ptr<SpellHealth> spell = SpellManager::get()->getHealthSpellInListById(id);
		spell->set_visual_name(msclr::interop::marshal_as<std::string>((String^)spellId));
	}


	get_current_list_view()->Items->Add(listViewDataItem1);


	listViewDataItem1["Id"] = id;
	listViewDataItem1["spell_type"] = spell_type;
	listViewDataItem1->Text = spellId;

	loadDateInGrid();
}

System::Void Spells::loadDateInGrid(){
	Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;
	if (!item || !item["spell_type"])
		return;

	int spell_type = (int)item["spell_type"];
	int id = Convert::ToInt32(item["Id"]->ToString());

	Telerik::WinControls::UI::RadPropertyStore^ GridSpell = getGrid(spell_type);
	if (spell_type > 0) {
		std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);
		if (attack){
			GridSpell["Spell Type"]->Value = (System::Int32)attack.get()->get_spell_type();
			GridSpell["Order"]->Value = attack.get()->get_order();
			GridSpell["Spell Condition"]->Value = attack.get()->get_spell_condition();
			GridSpell["Necessary Hp"]->Value = attack.get()->get_min_hp();
			GridSpell["Necessary Mp"]->Value = attack.get()->get_min_mp();
			GridSpell["Cooldown"]->Value = attack.get()->get_cooldown();
			GridSpell["Maximum Sqm"]->Value = attack.get()->get_maximum_sqm();
			GridSpell["Monster Qty on Sqm Area Min"]->Value = attack.get()->get_monster_in_area_min();
			GridSpell["Monster Qty on Sqm Area Max"]->Value = attack.get()->get_monster_in_area_max();
			GridSpell["Monster Qty in List Min"]->Value = attack.get()->get_monster_qty_list_min();
			GridSpell["Monster Qty in List Max"]->Value = attack.get()->get_monster_qty_list_max();
			GridSpell["Use Percent in HP"]->Value = attack.get()->get_use_percent_hp();
			GridSpell["Use Percent in MP"]->Value = attack.get()->get_use_percent_mp();

			spellDropDownspell_type->SelectedIndex = (System::Int32)attack.get()->get_spell_type();
			//ListMonster->BeginUpdate();
			ListMonster->Items->Clear();
			for (auto monsterInfo : attack.get()->get_monster_list()){
				Telerik::WinControls::UI::ListViewDataItem^ dataItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
				dataItem->SubItems->Add(gcnew String(&monsterInfo.second->get_name()[0]));
				dataItem->Tag = Convert::ToString(monsterInfo.first);
				ListMonster->Items->Add(dataItem);
			}
			//ListMonster->EndUpdate();
						
			//disable_notify = true;
			ListViewCondition->BeginUpdate();
			ListViewCondition->Items->Clear();
			for (auto conditionRule : attack.get()->vector_condition){
				Telerik::WinControls::UI::ListViewDataItem^ dataItem = gcnew Telerik::WinControls::UI::ListViewDataItem();

				ListViewCondition->Items->Add(dataItem);

				dataItem["Condition"] = GET_MANAGED_TR(std::string("spells_conditions_t_") + std::to_string(conditionRule->condition_type));
				dataItem["Action"] = GET_MANAGED_TR(std::string("spell_actions_t_") + std::to_string(conditionRule->action_type));
				dataItem["Value"] = gcnew String(conditionRule->get_value().c_str());				

			}
			ListViewCondition->EndUpdate();
			//disable_notify = false;
			
			if (check_any_monster)
				check_any_monster->Checked = attack->get_any_monster();

			//radDropDownList1->BeginUpdate();
			String^ visual_cast_value = gcnew String(attack.get()->get_visual_cast_value().c_str());
			int index_of_visual_cast = radDropDownList1->Items->IndexOf(visual_cast_value);
			if (index_of_visual_cast != -1)
				radDropDownList1->SelectedIndex = index_of_visual_cast;
			else
				radDropDownList1->Text = visual_cast_value;
			
			//radDropDownList1->EndUpdate();

			if (get_current_list_view()->Items->Count < 1){
				ListMonster->Enabled = false;
				radGroupBox1->Enabled = false;
				radGroupBox2->Enabled = false;
				radButton1->Enabled = false;
			}
			else{

				if (ListMonster->Items->Count > 0)
					radGroupBox2->Enabled = true;
				else
					radGroupBox2->Enabled = false;

				radButton1->Enabled = true;
				ListMonster->Enabled = true;
				radGroupBox1->Enabled = true;
			}

			SpinEditorValuePoint->Value = attack.get()->get_required_value_point();
		}
	}
	else {
		std::shared_ptr<SpellHealth> health = SpellManager::get()->getHealthSpellInListById(id);

		if (health){
			GridSpell["Spell Type"]->Value = static_cast<System::Int32^>(0);
			int x = health.get()->get_health_type();
			GridSpell["Health Type"]->Value = x;
			GridSpell["Order"]->Value = health.get()->get_order();
			GridSpell["Min Hp"]->Value = health.get()->get_min_hp();
			GridSpell["Max Hp"]->Value = health.get()->get_max_hp();
			GridSpell["Min Mp"]->Value = health.get()->get_min_mp();
			GridSpell["Max Mp"]->Value = health.get()->get_max_mp();
			GridSpell["Delay"]->Value = health.get()->get_cooldown();
			GridSpell["Use Percent in HP"]->Value = health.get()->get_use_percent_hp();
			GridSpell["Use Percent in MP"]->Value = health.get()->get_use_percent_mp();
			/*GridSpell["No use With Loot"]->Value = health.get()->get_no_use_with_hunt();
			GridSpell["No use With Hunt"]->Value = health.get()->get_no_use_with_loot();*/

			spellDropDownspell_type->SelectedIndex = (System::Int32)health.get()->get_spell_type();

			String^ visual_cast_value = gcnew String(health.get()->get_visual_cast_value().c_str());
			int index_of_visual_cast = radDropDownList1->Items->IndexOf(visual_cast_value);
			if (index_of_visual_cast != -1)
				radDropDownList1->SelectedIndex = index_of_visual_cast;
			else
				radDropDownList1->Text = visual_cast_value;

			ListMonster->Enabled = false;
			radGroupBox1->Enabled = false;
			radGroupBox2->Enabled = false;
			radButton1->Enabled = false;

			//disable_notify = true;
			ListViewCondition->BeginUpdate();
			ListViewCondition->Items->Clear();
			for (auto conditionRule : health.get()->vector_condition){
				Telerik::WinControls::UI::ListViewDataItem^ dataItem = gcnew Telerik::WinControls::UI::ListViewDataItem();

				ListViewCondition->Items->Add(dataItem);

				dataItem["Condition"] = GET_MANAGED_TR(std::string("spells_conditions_t_") + std::to_string(conditionRule->condition_type));
				dataItem["Action"] = GET_MANAGED_TR(std::string("spell_actions_t_") + std::to_string(conditionRule->action_type));
				dataItem["Value"] = gcnew String(conditionRule->get_value().c_str());

			}		
			ListViewCondition->EndUpdate();
			//disable_notify = false;

			SpinEditorValuePoint->Value = health.get()->get_required_value_point();
		}
	}


	if (ListViewCondition->Items->Count <= 0)
		disable_page_condition();
	else
		enable_page_condition();

	updateSelectedItemCondition();
	updateSelectedData();

	spellTextBoxId->Text = item->Text;
	this->spellSpellProperty->SelectedObject = GridSpell;
}

System::Void Spells::spellSpellList_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {

	disable_notify = true;
	loadDateInGrid();
	disable_notify = false;
}

System::Void Spells::spellSpellProperty_Edited(System::Object^ sender, Telerik::WinControls::UI::PropertyGridItemEditedEventArgs^  e){
	Telerik::WinControls::UI::PropertyGridItem^ Item = (Telerik::WinControls::UI::PropertyGridItem^) e->Item;
	int spell_type = Convert::ToInt32(get_current_list_view()->SelectedItem["spell_type"]->ToString());
	int id = Convert::ToInt32(get_current_list_view()->SelectedItem["Id"]->ToString());

	if (spell_type <= 0) {
		std::shared_ptr<SpellHealth> health = SpellManager::get()->getHealthSpellInListById(id);
		if (Item->Name == "Health Type"){
			health.get()->set_health_type((int)Item->Value);
		}
		else if (Item->Name == "Order"){
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			health.get()->set_order((int)Item->Value);
		}
		else if (Item->Name == "Min Hp") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			health.get()->set_min_hp((int)Item->Value);
		}
		else if (Item->Name == "Max Hp") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			health.get()->set_max_hp((int)Item->Value);
		}
		else if (Item->Name == "Min Mp") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			health.get()->set_min_mp((int)Item->Value);
		}
		else if (Item->Name == "Max Mp") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			health.get()->set_max_mp((int)Item->Value);
		}
		else if (Item->Name == "Delay") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			health.get()->set_cooldown((int)Item->Value);
		}
		else if (Item->Name == "Use Percent in HP") {
			health.get()->set_use_percent_hp(Convert::ToBoolean(Convert::ToString(Item->Value)));
		}
		else if (Item->Name == "Use Percent in MP") {
			health.get()->set_use_percent_mp(Convert::ToBoolean(Convert::ToString(Item->Value)));
		}
		/*else if (Item->Name == "No use With Loot") {
			health.get()->set_no_use_with_loot(Convert::ToBoolean(Convert::ToString(Item->Value)));
		}
		else if (Item->Name == "No use With Hunt") {
			health.get()->set_no_use_with_hunt(Convert::ToBoolean(Convert::ToString(Item->Value)));
		}*/
	}
	else {
		std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);
		if (Item->Name == "Order") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_order((int)Item->Value);
		}
		else  if (Item->Name == "Spell Condition") {
			attack.get()->set_spell_condition((int)Item->Value);
		}
		else if (Item->Name == "Necessary Hp") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_min_hp((int)Item->Value);
		}
		else if (Item->Name == "Necessary Mp") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_min_mp((int)Item->Value);
		}
		else if (Item->Name == "Cooldown") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_cooldown((int)Item->Value);
		}
		else if (Item->Name == "Maximum Sqm") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_maximum_sqm((int)Item->Value);
		}
		else if (Item->Name == "Monster Qty on Sqm Area Min") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_monster_in_area_min((int)Item->Value);
		}
		else if (Item->Name == "Monster Qty on Sqm Area Max") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_monster_in_area_max((int)Item->Value);
		}
		else if (Item->Name == "Monster Qty in List Min") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_monster_qty_list_min((int)Item->Value);
		}
		else if (Item->Name == "Monster Qty in List Max") {
			if ((System::Int32)Item->Value < 0)
				Item->Value = static_cast<System::Int32^>(0);
			attack.get()->set_monster_qty_list_max((int)Item->Value);
		}
		else if (Item->Name == "Use Percent in HP") {
			attack.get()->set_use_percent_hp(Convert::ToBoolean(Convert::ToString(Item->Value)));
		}
		else if (Item->Name == "Use Percent in MP") {
			attack.get()->set_use_percent_mp(Convert::ToBoolean(Convert::ToString(Item->Value)));
		}
	}
}

System::Void Spells::spellsListViewItemValueChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangedEventArgs^  e) {
	Telerik::WinControls::UI::ListViewDataItem^ item = e->Item;
	int id = Convert::ToInt32(item["id"]);
	int spell_type = Convert::ToInt32(item["spell_type"]);

	if (spell_type > 0) {
		std::shared_ptr<SpellAttack> spell = SpellManager::get()->getAttackSpellInListById(id);
		spell->set_visual_name(marshal_as<std::string>(item->Text->ToString()));
	}
	else {
		std::shared_ptr<SpellHealth> spell = SpellManager::get()->getHealthSpellInListById(id);
		spell->set_visual_name(marshal_as<std::string>(item->Text->ToString()));
	}
}

System::Void Spells::spellSpellList_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	Telerik::WinControls::UI::ListViewDataItem^ item = e->Item;
	int id = Convert::ToInt32(item["id"]);
	int spell_type = Convert::ToInt32(item["spell_type"]);

	if (spell_type > 0){
		SpellManager::get()->removeAttackSpellInList(id);

		auto control_rule = ControlManager::get()->getControlInfoRule("SpellsAttackGroup");
		if (!control_rule)
			return;

		control_rule->removeControl(std::to_string(id) + "Attack");
		
		if (spellSpellListAttack->Items->Count <= 1){
			ListMonster->Enabled = false;
			radGroupBox1->Enabled = false;
			radGroupBox2->Enabled = false;
			radButton1->Enabled = false;
		}
	}
	else{
		SpellManager::get()->removeHealthSpellInList(id);
		auto control_rule = ControlManager::get()->getControlInfoRule("SpellsHealthGroup");
		if (!control_rule)
			return;

		control_rule->removeControl(std::to_string(id) + "Health");
	}
}

Telerik::WinControls::UI::RadPropertyStore^ Spells::getGrid(int spell_type){
	if (spell_type > 0){
		Telerik::WinControls::UI::RadPropertyStore^ radPropertyStoreAttack = (gcnew Telerik::WinControls::UI::RadPropertyStore());
		Telerik::WinControls::UI::PropertyStoreItem^ spell_type = (gcnew Telerik::WinControls::UI::PropertyStoreItem(spell_typeManaged::typeid, "Spell Type", gcnew spell_typeManaged, "Type of the spells"));
		spell_type->ReadOnly = true;
		Telerik::WinControls::UI::PropertyStoreItem^ order = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Order", gcnew SpellConditionManaged, "Priority to cast magic"));
		Telerik::WinControls::UI::PropertyStoreItem^ spell_condition = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellConditionManaged::typeid, "Spell Condition", gcnew SpellConditionManaged, "Type of the spells condition"));
		Telerik::WinControls::UI::PropertyStoreItem^ necessaryHp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Necessary Hp", nullptr, "Min health to cast attack spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ necessaryMp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Necessary Mp", nullptr, "Min mana to cast attack spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ cooldown = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Cooldown", nullptr, "Cooldown to cast attack spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ maximum_sqm = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Maximum Sqm", nullptr, "Sqm area to search for creatures"));
		Telerik::WinControls::UI::PropertyStoreItem^ monsterQtyOnSqmAreaMin = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Monster Qty on Sqm Area Min", nullptr, "Min monsters in Sqm Area to cast attack spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ monsterQtyOnSqmAreaMax = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Monster Qty on Sqm Area Max", nullptr, "Max monsters in Sqm Area to cast attack spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ monsterQtyOfListMin = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Monster Qty in List Min", nullptr, "Min monster in sqm area on Monsters list min"));
		Telerik::WinControls::UI::PropertyStoreItem^ monsterQtyOfListMax = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Monster Qty in List Max", nullptr, "Max monster in sqm area on Monsters list min"));
		Telerik::WinControls::UI::PropertyStoreItem^ use_percent_hp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellUsePercentManaged::typeid, "Use Percent in HP", gcnew SpellUsePercentManaged, "Use Percent in HP"));
		Telerik::WinControls::UI::PropertyStoreItem^ use_percent_mp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellUsePercentManaged::typeid, "Use Percent in MP", gcnew SpellUsePercentManaged, "Use Percent in MP"));

		radPropertyStoreAttack->Add(spell_type);
		radPropertyStoreAttack->Add(order);
		radPropertyStoreAttack->Add(spell_condition);
		radPropertyStoreAttack->Add(necessaryHp);
		radPropertyStoreAttack->Add(necessaryMp);
		radPropertyStoreAttack->Add(cooldown);
		radPropertyStoreAttack->Add(maximum_sqm);
		radPropertyStoreAttack->Add(monsterQtyOnSqmAreaMin);
		radPropertyStoreAttack->Add(monsterQtyOnSqmAreaMax);
		radPropertyStoreAttack->Add(monsterQtyOfListMin);
		radPropertyStoreAttack->Add(monsterQtyOfListMax);
		radPropertyStoreAttack->Add(use_percent_hp);
		radPropertyStoreAttack->Add(use_percent_mp);
		return radPropertyStoreAttack;
	}
	else{

		Telerik::WinControls::UI::RadPropertyStore^ radPropertyStoreHealth = (gcnew Telerik::WinControls::UI::RadPropertyStore());
		Telerik::WinControls::UI::PropertyStoreItem^ spell_type = (gcnew Telerik::WinControls::UI::PropertyStoreItem(spell_typeManaged::typeid, "Spell Type", gcnew spell_typeManaged, "Type of the spells"));
		spell_type->ReadOnly = true;

		/*Telerik::WinControls::UI::PropertyStoreItem^ no_use_with_loot = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellUsePercentManaged::typeid, "No use With Loot", gcnew SpellUsePercentManaged, "No use With Loot"));
		Telerik::WinControls::UI::PropertyStoreItem^ no_use_with_hunt = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellUsePercentManaged::typeid, "No use With Hunt", gcnew SpellUsePercentManaged, "No use With Hunt"));*/
		Telerik::WinControls::UI::PropertyStoreItem^ health_type = (gcnew Telerik::WinControls::UI::PropertyStoreItem(Spellhealth_type::typeid, "Health Type", gcnew Spellhealth_type, "Choose type of health ( Mana Or Life )"));
		Telerik::WinControls::UI::PropertyStoreItem^ order = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Order", nullptr, "Priority to cast magic"));
		//Telerik::WinControls::UI::PropertyStoreItem^ hotkeys = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::String::typeid, "Hotkeys", gcnew System::String(""), "Hotkeys to cast spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ min_hp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Min Hp", nullptr, "Min health to cast spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ max_hp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Max Hp", nullptr, "Max health to cast spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ min_mp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Min Mp", nullptr, "Min mana to cast spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ max_mp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Max Mp", nullptr, "Max mana to cast spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ delay = (gcnew Telerik::WinControls::UI::PropertyStoreItem(System::Int32::typeid, "Delay", nullptr, "Delay to cast Spell"));
		Telerik::WinControls::UI::PropertyStoreItem^ use_percent_hp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellUsePercentManaged::typeid, "Use Percent in HP", gcnew SpellUsePercentManaged, "Use Percent in HP"));
		Telerik::WinControls::UI::PropertyStoreItem^ use_percent_mp = (gcnew Telerik::WinControls::UI::PropertyStoreItem(SpellUsePercentManaged::typeid, "Use Percent in MP", gcnew SpellUsePercentManaged, "Use Percent in MP"));



		radPropertyStoreHealth->Add(spell_type);
		radPropertyStoreHealth->Add(health_type);
		radPropertyStoreHealth->Add(order);
		//radPropertyStoreHealth->Add(hotkeys);
		radPropertyStoreHealth->Add(min_hp);
		radPropertyStoreHealth->Add(max_hp);
		radPropertyStoreHealth->Add(min_mp);
		radPropertyStoreHealth->Add(max_mp);
		radPropertyStoreHealth->Add(delay);
		radPropertyStoreHealth->Add(use_percent_hp);
		radPropertyStoreHealth->Add(use_percent_mp);
		/*radPropertyStoreHealth->Add(no_use_with_loot);
		radPropertyStoreHealth->Add(no_use_with_hunt);*/
		return radPropertyStoreHealth;
	}

}

System::Void Spells::radDropDownList1_TextChanged(System::Object^  sender, System::EventArgs^  e){
	if (disable_notify)
		return;

	if (get_current_list_view()->SelectedIndex < 0)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;

	if (!item)
		return;

	if (!item["spell_type"])
		return;

	int spell_type = (int)item["spell_type"];
	int id = Convert::ToInt32(item["Id"]->ToString());

	Telerik::WinControls::UI::RadPropertyStore^ GridSpell = getGrid(spell_type);

	if (spell_type <= 0) {
		std::shared_ptr<SpellHealth> health = SpellManager::get()->getHealthSpellInListById(id);
		health->set_visual_cast_value(marshal_as<std::string>(radDropDownList1->Text));
	}
	else {
		std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);
		attack->set_visual_cast_value(marshal_as<std::string>(radDropDownList1->Text));
	}
}

System::Void Spells::spellDropDownUseType_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	/*use_spell_type spell_use = (use_spell_type)spellDropDownUseType->SelectedIndex;
	SpellManager::get()->set_spell_type(spell_use);*/

	if (disable_notify)
		return;

	int spell_type = this->spellDropDownspell_type->SelectedIndex;
	if (radPageView1->Pages->IndexOf(radPageView1->SelectedPage) == 0){
		spell_type = 0;
		this->spellDropDownspell_type->SelectedIndex = 0;
	}
	else{
		if (this->spellDropDownspell_type->SelectedIndex < 1){
			this->spellDropDownspell_type->SelectedIndex = 1;
			spell_type = 1;
		}

	}

	disable_notify = true;
	Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;
	if (!item || !item["spell_type"])
		return;

	item["spell_type"] = (int)spell_type;
	int id = Convert::ToInt32(item["Id"]->ToString());

	Telerik::WinControls::UI::RadPropertyStore^ GridSpell = getGrid(spell_type);
	if (spell_type > 0) {
		std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);
		if (!attack)
			return;

		attack->set_spell_type((spell_type_t)spell_type);
	}
	else{
		std::shared_ptr<SpellHealth> health = SpellManager::get()->getHealthSpellInListById(id);
		if (!health)
			return;

		health->set_spell_type((spell_type_t)spell_type);
	}

	loadDateInGrid();
	disable_notify = false;
}

Telerik::WinControls::UI::RadListView^ Spells::get_current_list_view(){
	if (radPageView1->Pages->IndexOf(radPageView1->SelectedPage) == 0)
		return spellSpellListHeal;
	return spellSpellListAttack;
}

void Spells::setDefault(String^ nameSpell){
	if (disable_notify)
		return;

	if (get_current_list_view()->SelectedIndex < 0)
		return;

	Telerik::WinControls::UI::ListViewDataItem^ item = get_current_list_view()->SelectedItem;

	if (!item)
		return;

	if (!item["spell_type"])
		return;

	int spell_type = (int)item["spell_type"];
	int id = Convert::ToInt32(item["Id"]->ToString());

	if (spell_type <= 0) {
		std::shared_ptr<SpellHealth> health = SpellManager::get()->getHealthSpellInListById(id);
		std::vector<XMLSpellInfo> spells = gXMLSpellManager.getspellsInfoList();
		for (XMLSpellInfo info : spells){
			String^ temp = gcnew String(info.get_cast().c_str());
			if (temp == nameSpell){
				health->set_min_mp(info.get_mana());
				health->set_max_mp(9999);
				health->set_cooldown(info.get_cooldownCategory());
				health->set_min_hp(info.get_min_hp());
				health->set_max_hp(info.get_max_hp());
				health->set_use_percent_hp(true);
			}
		}
	}
	else {
		std::shared_ptr<SpellAttack> attack = SpellManager::get()->getAttackSpellInListById(id);
		std::vector<XMLSpellInfo> spells = gXMLSpellManager.getspellsInfoList();
		for (XMLSpellInfo info : spells){
			String^ temp = gcnew String(info.get_cast().c_str());
			if (temp == nameSpell){
				attack->set_min_mp(info.get_mana());
				attack->set_max_mp(9999);
				attack->set_maximum_sqm(info.get_range());
				attack->set_cooldown(info.get_cooldownCategory());
				attack->set_monster_in_area_min(1);
				attack->set_monster_in_area_max(50);
				attack->set_monster_qty_list_min(1);
				attack->set_monster_qty_list_max(50);
			}
		}
	}

	loadDateInGrid();
}
