#pragma once
#include "HUDManager.h"
#include <Shobjidl.h>
#include <iostream>
#include <thread>
#include <mutex>
#include <fstream>
#include <istream>
#include <Shlwapi.h>
#include <windows.h>
#include <objidl.h>
#include <gdiplus.h>
#include <locale>


using namespace Gdiplus;

#pragma comment (lib, "gdiplus.lib")
#pragma comment(lib, "Shlwapi.lib")

std::map<uint32_t /*HUD_ID*/, std::shared_ptr<HUDInfo>> HUDManager::get_mapHUDInfo(){
	return mapHUDInfo;
}

void HUDManager::reset_mapHUDInfo(){
	((std::mutex*)mutex_ptr)->lock();
	mapHUDInfo.clear();
	((std::mutex*)mutex_ptr)->unlock();
}

uint32_t HUDManager::generateNewId(){
	((std::mutex*)mutex_ptr)->lock();
	uint32_t current_id = 0;

	while (mapHUDInfo.find(current_id) != mapHUDInfo.end())
		current_id++;

	mapHUDInfo[current_id] = std::shared_ptr<HUDInfo>(new HUDInfo);
	mapHUDInfo[current_id]->hudargb = { 0, 0, 0, 0 };
	mapHUDInfo[current_id]->hudcoord = { 0, 0 };
	mapHUDInfo[current_id]->hudrect = { 0, 0, 0, 0 };
	mapHUDInfo[current_id]->hudsize = { 0, 0 };

	((std::mutex*)mutex_ptr)->unlock();
	return current_id;
}

ImagePtr ImageManager::load(std::string filename){
	TimeChronometerTEMP time;

	Image* pImage = new Image(HUDManager::get_default()->string_to_wchar(filename).c_str());

	if (pImage->GetLastStatus() != Ok)
		return nullptr;

	mapImage[filename] = std::shared_ptr<ImageClass>(new ImageClass);
	mapImage[filename]->Image = (uint32_t)pImage;
	mapImage[filename]->time_on = time;

	return mapImage[filename];
}

void ImageManager::delete_image(){
	for (auto imageit : mapImage)
	if (imageit.second->time_on.elapsed_milliseconds() > 10000)
		mapImage.erase(imageit.first);
}

ImagePtr ImageManager::creat_new_imageclass(uint32_t temp_image, std::string filename){
	TimeChronometerTEMP time;

	mapImage[filename] = std::shared_ptr<ImageClass>(new ImageClass);
	mapImage[filename]->Image = temp_image;
	mapImage[filename]->time_on = time;

	return mapImage[filename];
}

ImagePtr ImageManager::get_image(std::string filename){
	auto imageIterator = mapImage.find(filename);
	if (imageIterator == mapImage.end())
		return load(filename);

	delete_image();

	mapImage[filename]->time_on.reset();
	return mapImage[filename];
}

ImagePtr ImageManager::get_item(std::string filename){
	std::pair<char*, uint32_t> data_info_pair((char*)0, 0);

	if (!get_image_funct)
		return nullptr;

	data_info_pair = get_image_funct(filename + ".gif");

	if (data_info_pair.first != 0){
		auto temp_image = (get_image_by_buffer(data_info_pair.first, data_info_pair.second));
		auto temp_map = creat_new_imageclass(temp_image, filename);

		return temp_map;
	}
	return nullptr;
}

uint32_t ImageManager::get_image_by_buffer(char* buffer, int length){
	IStream* steram = SHCreateMemStream((const BYTE*)buffer, length);

	if (!steram)
		return 0;

	Image* pImage1 = Gdiplus::Image::FromStream(steram);
	auto temp = pImage1->GetLastStatus();

	if (temp != Ok)
		return 0;

	if (!pImage1)
		return 0;

	return (uint32_t)pImage1;
}

uint32_t HUDManager::drawrect(HUDRECT rect){
	if (!ptrgraphics)
		return 0;

	if (!rect.is_valid())
		return 0;

	Pen pen__(Color(BorderColor.a, BorderColor.r, BorderColor.g, BorderColor.b), (Gdiplus::REAL) BorderSize);
	((Gdiplus::Graphics*)ptrgraphics)->DrawRectangle(&pen__, rect.X, rect.Y, rect.Width, rect.Height);

	uint32_t draw_id = generateNewId();

	mapHUDInfo[draw_id]->hudrect = HUDRECT{ rect.X, rect.Y, rect.Width, rect.Height };
	return draw_id;
}
void HUDManager::GetRoundRectPath(uint32_t* pathINT, HUDRECT r, int diax, int diay){
	if (!r.is_valid())
		return;

	// diameter can't exceed width or height
	if (diax > r.Width)	diax = r.Width;
	if (diay > r.Height) diay = r.Height;

	// define a corner 
	Gdiplus::Rect Corner(r.X, r.Y, diax, diay);

	Gdiplus::GraphicsPath* path = ((Gdiplus::GraphicsPath*)pathINT);

	// begin path
	path->Reset();

	// top left
	path->AddArc(Corner, 180, 90);

	// tweak needed for radius of 10 (dia of 20)
	if (diax == 20)
	{
		Corner.Width += 1;
		Corner.Height += 1;
		r.Width -= 1; r.Height -= 1;
	}

	// top right
	Corner.X += (r.Width - diax - 1);
	path->AddArc(Corner, 270, 90);

	// bottom right
	Corner.Y += (r.Height - diay - 1);
	path->AddArc(Corner, 0, 90);

	// bottom left
	Corner.X -= (r.Width - diax - 1);
	path->AddArc(Corner, 90, 90);

	// end path
	path->CloseFigure();
}
uint32_t HUDManager::drawroundrect(HUDRECT rect, int radiusX, int radiusY){
	if (!ptrgraphics)
		return 0;

	if (!rect.is_valid())
		return 0;

	Gdiplus::Graphics* graphics = (Gdiplus::Graphics*)ptrgraphics;

	int sz = GradientMultiColor.size();
	Color* colors_ptr = new Color[sz];
	REAL* blendPositions = new REAL[sz];

	LinearGradientBrush linGrBrush(Gdiplus::Rect(SetPosition.X + rect.X, SetPosition.Y + rect.Y, rect.Width, rect.Height),
		Color(0, 0, 0, 0),
		Color(0, 0, 0, 0),
		Gdiplus::LinearGradientMode::LinearGradientModeVertical);

	int index = 0;
	for (auto interpolations_pair : GradientMultiColor){
		//blendPositions[index] = REAL(interpolations_pair.first);
		colors_ptr[index] = Color(interpolations_pair.second.a, interpolations_pair.second.r, interpolations_pair.second.g, interpolations_pair.second.b);
		index++;
	}

	blendPositions[0] = REAL(0.0f);
	blendPositions[1] = REAL(0.23f);
	blendPositions[2] = REAL(1.0f);

	linGrBrush.SetInterpolationColors(colors_ptr, blendPositions, sz);

	int oldPageUnit = graphics->SetPageUnit(UnitPixel);
	Pen pen_in(Color(BorderColor.a, BorderColor.r, BorderColor.g, BorderColor.b), (Gdiplus::REAL) BorderSize);
	Gdiplus::GraphicsPath path_;

	pen_in.SetAlignment(PenAlignmentCenter);
	GetRoundRectPath((uint32_t*)&path_, rect, (2 * radiusX), (2 * radiusY));

	graphics->DrawPath(&pen_in, &path_);

	graphics->FillPath(&linGrBrush, &path_);
	graphics->SetPageUnit((Unit)oldPageUnit);

	uint32_t draw_id = generateNewId();
	mapHUDInfo[draw_id]->set_hudrect(HUDRECT{ rect.X, rect.Y, rect.Width, rect.Height });

	delete colors_ptr;
	delete blendPositions;

	return draw_id;
}
uint32_t HUDManager::drawcircle(HUDRECT rect){
	if (!ptrgraphics)
		return 0;
	if (!rect.is_valid())
		return 0;

	Pen pen__(Color(BorderColor.a, BorderColor.r, BorderColor.g, BorderColor.b), (Gdiplus::REAL)BorderSize);
	((Gdiplus::Graphics*)ptrgraphics)->DrawEllipse(&pen__, SetPosition.X + rect.X, SetPosition.Y + rect.Y, rect.Width, rect.Height);

	uint32_t draw_id = generateNewId();
	mapHUDInfo[draw_id]->hudrect = HUDRECT{ rect.X, rect.Y, rect.Width, rect.Height };
	return draw_id;
}
void HUDManager::drawline(HUDCOORD POINT_1, HUDCOORD POINT_2){
	if (!ptrgraphics)
		return;

	Pen pen__(Color(BorderColor.a, BorderColor.r, BorderColor.g, BorderColor.b), (Gdiplus::REAL) BorderSize);
	((Gdiplus::Graphics*)ptrgraphics)->DrawLine(&pen__, SetPosition.X + POINT_1.X, SetPosition.Y + POINT_1.Y, SetPosition.X + POINT_2.X, SetPosition.Y + POINT_2.Y);
}
void HUDManager::drawitem(std::string Path_or_Name, HUDCOORD XY, HUDSIZE ZOOM){
	if (!ptrgraphics)
		return;

	Graphics* graphics = (Graphics*)ptrgraphics;

	auto image_ = ImageManager::get()->get_item(Path_or_Name);

	if (!image_)
		return;

	Image* temp_image = (Image*)image_->Image;

	graphics->DrawImage(temp_image, XY.X, XY.Y, ZOOM.Width, ZOOM.Height);
}
void HUDManager::drawimage(std::string Path_or_Name, HUDCOORD XY, HUDCOORD SRCXY, HUDSIZE Size){
	if (!ptrgraphics)
		return;

	Graphics* graphics = (Graphics*)ptrgraphics;

	auto image_ = ImageManager::get()->get_image(Path_or_Name);

	if (!image_)
		return;

	Image* temp_image = (Image*)image_->Image;

	graphics->DrawImage(temp_image, XY.X, XY.Y, SRCXY.X, SRCXY.Y, Size.Width, Size.Height, Gdiplus::Unit::UnitPixel);
}
void HUDManager::drawtext(std::string Text, HUDCOORD Point_){
	if (!ptrgraphics)
		return;

	FontFamily fontFamily(string_to_wchar(FontName).c_str());
	FontStyle fontStyle = FontStyle::FontStyleBold;
	GraphicsPath path;

	path.AddString(string_to_wchar(Text).c_str(),
		wcslen(string_to_wchar(Text).c_str()), &fontFamily, fontStyle, (Gdiplus::REAL)FontSize, Gdiplus::Point(Point_.X, Point_.Y), NULL);

	((Gdiplus::Graphics*)ptrgraphics)->SetSmoothingMode(SmoothingModeAntiAlias);
	((Gdiplus::Graphics*)ptrgraphics)->SetInterpolationMode(InterpolationModeHighQualityBicubic);

	Pen pen(Color(BorderFontColor.a, BorderFontColor.r, BorderFontColor.g, BorderFontColor.b), (Gdiplus::REAL)BorderFontSize);
	((Gdiplus::Graphics*)ptrgraphics)->DrawPath(&pen, &path);

	SolidBrush brush(Color(FontColor.a, FontColor.r, FontColor.g, FontColor.b));
	((Gdiplus::Graphics*)ptrgraphics)->FillPath(&brush, &path);
}









bool HUDManager::CreateHudWindow(){
	WNDCLASSEX wcl = { 0 };
	wcl.cbSize = sizeof(wcl);
	wcl.style = CS_HREDRAW | CS_VREDRAW;
	wcl.lpfnWndProc = DefWindowProc;
	wcl.cbClsExtra = 0;
	wcl.cbWndExtra = 0;
	wcl.hInstance = 0;
	wcl.hIcon = LoadIcon(0, IDI_APPLICATION);
	wcl.hCursor = LoadCursor(0, IDC_ARROW);
	wcl.hbrBackground = 0;
	wcl.lpszMenuName = 0;
	wcl.lpszClassName = "HUD";
	wcl.hIconSm = 0;

	if (!RegisterClassEx(&wcl))
		return 0;

	HUDSIZE sizeScreen = get_size_desktop();

	hWnd = (uint32_t)CreateWindowEx(WS_EX_LAYERED | WS_EX_TOOLWINDOW | WS_EX_TRANSPARENT | WS_EX_TOPMOST, wcl.lpszClassName,
		"HUD", WS_POPUP, 0, 0, sizeScreen.Width,
		sizeScreen.Height, 0, 0, wcl.hInstance, 0);

	return true;
}

void HUDManager::startLoop(){
	extern void register_in_monitor(char*);
	register_in_monitor(__FUNCTION__);

	if (hWnd){
		MSG msg = { 0 };
		ShowWindow((HWND)hWnd, SW_SHOW);
		UpdateWindow((HWND)hWnd);
		bool hidden = false;
		long int time_top = 0;
		long int last_cycle = GetTickCount();

		while (true){
			Sleep(10);
			
			if (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)){
				if (msg.message == WM_QUIT)
					break;

				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else{
				if (!enabled && !hidden){
					hidden = true;
					::ShowWindow((HWND)hWnd, SW_HIDE);
					continue;
				}

				if (!get_windowshandle){
					Sleep(1);
					continue;
				}
				WINDOWPLACEMENT status;
				GetWindowPlacement((HWND)get_windowshandle(), &status);
				if (visible && enabled && (uint32_t)get_windowshandle() == (uint32_t)GetForegroundWindow()){
					time_top = time_top + (GetTickCount() - last_cycle);
					if (hidden && time_top > 200){
						hidden = false;
						::ShowWindow((HWND)hWnd, SW_SHOW);
					}

				}
				else{
					time_top = 0;
					if (!hidden){
						hidden = true;
						std::cout << "\n FUNCTION: " << __FUNCTION__ << " LINE: " << __LINE__ << " SW_HIDE";
						::ShowWindow((HWND)hWnd, SW_HIDE);
						SetForegroundWindow(GetForegroundWindow());
					}
				}
				last_cycle = GetTickCount();
			}
		}
	}
}

HUDManager::HUDManager(){
	GradientMultiColor.push_back(std::pair<float, HUDARGB>(0.0f, { 255, 255, 255, 255 }));
	enabled = true;
	visible = true;
	initialized = false;
	FontName = "Tahoma";
	SetPosition = HUDCOORD{ 0, 0 };

	mutex_ptr = (uint32_t*)new std::mutex;
}

HUDManager::~HUDManager(){
	SelectObject((HDC)memDC, (HBITMAP)hOldBmp);
	DeleteObject((HBITMAP)hMemBmp);
	DeleteDC((HDC)memDC);
}

HUDSIZE HUDManager::get_size_desktop(){
	::RECT desktop_;

	GetWindowRect(GetDesktopWindow(), &desktop_);

	return HUDSIZE{ desktop_.right, desktop_.bottom };
}

HUDSIZE HUDManager::get_size_client(){
	::RECT client;
	if (!get_windowshandle)
		return HUDSIZE{ 0, 0 };

	GetWindowRect((HWND)get_windowshandle(), &client);

	return HUDSIZE{ client.right, client.bottom };
}

void HUDManager::swap_buffer(){
	if (!ptrgraphics)
		return;

	copy_from_window();
	
	((Gdiplus::Graphics*)ptrgraphics)->Clear(Gdiplus::Color(0, 0, 0, 0));
}

void HUDManager::set_enabled(bool in){
	enabled = in;
}

bool HUDManager::is_visible(){
	bool temp = visible;
	return temp;
}

void HUDManager::set_visible(bool in){
	visible = in;
}

bool HUDManager::get_enable(){
	bool temp = enabled;
	return temp;
}

void HUDManager::init(){
	if (initialized)
		return;

	initialized = true;
	GdiplusStartupInput gdiplusStartupInput;
	ULONG_PTR           gdiplusToken;

	// Initialize GDI+.
	GdiplusStartup(&gdiplusToken, &gdiplusStartupInput, NULL);
	CreateHudWindow();

	HUDSIZE sizeScreen = get_size_desktop();

	hDC = (uint32_t)GetDC((HWND)hWnd);
	memDC = (uint32_t)CreateCompatibleDC((HDC)hDC);

	hMemBmp = (uint32_t)CreateCompatibleBitmap((HDC)hDC, sizeScreen.Width, sizeScreen.Width);
	hOldBmp = (uint32_t)SelectObject((HDC)memDC, (HBITMAP)hMemBmp);
	ptrgraphics = (uint32_t*)new Gdiplus::Graphics((HDC)memDC);

	((Gdiplus::Graphics*)ptrgraphics)->SetTextRenderingHint(TextRenderingHintAntiAlias);
	((Gdiplus::Graphics*)ptrgraphics)->SetSmoothingMode(SmoothingModeAntiAlias);
	((Gdiplus::Graphics*)ptrgraphics)->SetInterpolationMode(InterpolationModeHighQualityBicubic);

	std::thread(std::bind(&HUDManager::startLoop, this)).detach(); 
}

std::wstring HUDManager::string_to_wchar(std::string string){
	std::wstring_convert< std::codecvt<wchar_t, char, std::mbstate_t> > conv;
	std::wstring wstr = conv.from_bytes(string);
	return wstr;
}

void HUDManager::copy_from_window(){

	if (!get_windowshandle)
		return;

	RECT rect;
	GetClientRect((HWND)get_windowshandle(), &rect);

	POINT real = { 0, 0 };
	MapWindowPoints(HWND_DESKTOP, (HWND)get_windowshandle(), &real, 1);

	HUDCOORD coord;
	coord.X = -real.x;
	coord.Y = -real.y;

	int WinWidth = rect.right - rect.left;
	int WinHeight = rect.bottom - rect.top;

	POINT ptDest = { coord.X, coord.Y };
	POINT ptSrc = { 0, 0 };
	SIZE client = { WinWidth, WinHeight };
	BLENDFUNCTION blendFunc = { AC_SRC_OVER, 0, 255, AC_SRC_ALPHA };

	int res = UpdateLayeredWindow((HWND)hWnd, (HDC)hDC, &ptDest, &client,
		(HDC)memDC, &ptSrc, 0, &blendFunc, ULW_ALPHA);

}

uint32_t* HUDManager::get_image(char* buffer, int length){
	IStream* steram = SHCreateMemStream((const BYTE*)buffer, length);

	if (!steram)
		return 0;

	Image* pImage1 = Gdiplus::Image::FromStream(steram);

	if (!pImage1)
		return 0;

	return (uint32_t*)pImage1;
}

HUDManager* HUDManager::get_default(){
	static HUDManager* m = nullptr;
	if (!m)
		m = new HUDManager();
	return m;
}

HUDSIZE HUDManager::measurestring(std::string text){
	if (!ptrgraphics)
		return HUDSIZE{ 0, 0 };

	Gdiplus::Graphics* graphics = (Gdiplus::Graphics*)ptrgraphics;

	Font font_(string_to_wchar(FontName).c_str(), (Gdiplus::REAL)FontSize);

	RectF layoutRect((Gdiplus::REAL)SetPosition.X, (Gdiplus::REAL)SetPosition.Y, (Gdiplus::REAL)0, (Gdiplus::REAL)0);
	RectF boundRect;

	graphics->MeasureString(string_to_wchar(text).c_str(), wcslen(string_to_wchar(text).c_str()), &font_, layoutRect, &boundRect);

	graphics->DrawRectangle(&Pen(Color(0, 0, 0, 0)), boundRect);

	return HUDSIZE{ (int)boundRect.Width, (int)boundRect.Height };
}

void HUDManager::setfontstyle(std::string fontname, uint32_t size, uint32_t fontweight, HUDARGB  fontcolor, uint32_t bordersize, HUDARGB bordercolor){
	FontName = fontname;
	this->FontSize = size;
	this->FontWeight = fontweight;
	this->FontColor = fontcolor;
	this->BorderFontColor = bordercolor;
	this->BorderFontSize = bordersize;
}

void HUDManager::setfontname(std::string fontname){
	this->FontName = fontname;
}

void HUDManager::setfontsize(uint32_t size){
	this->FontSize = size;
}

void HUDManager::setfontborder(int BorderSize, HUDARGB bordercolor){
	this->BorderSize = BorderSize;
}

void HUDManager::setbordersize(int BorderSize){
	this->BorderSize = BorderSize;
}

void HUDManager::setfontcolor(HUDARGB FontColor){
	this->FontColor = FontColor;
}

void HUDManager::setbordercolor(HUDARGB color){
	BorderColor = color;
}

void HUDManager::addgradcolors(std::vector<std::pair< float, HUDARGB >>& intporlations_info){
	GradientMultiColor = intporlations_info;
}

void HUDManager::setposition(HUDCOORD position){
	SetPosition = position;
}