#include <string>
#include "string_util.h"
#include "Core/AdvancedRepoterCore.h"
#include "ManagedUtil.h"
#include "Core\ItemsManager.h"
#include "LanguageManager.h"
#include "GifManager.h"
#include "GeneralManager.h"
#pragma once

namespace Neutral {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class AdvancedRepoter : public Telerik::WinControls::UI::RadForm{
	public:	AdvancedRepoter(void){
			InitializeComponent();

			this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");

			NeutralBot::FastUIPanelController::get()->install_controller(this);
			
		}
	protected: ~AdvancedRepoter(){
			unique = nullptr;
			if (components)
				delete components;
		}
	protected:
	 Telerik::WinControls::UI::RadSpinEditor^  box_min;
	 Telerik::WinControls::UI::RadLabel^  radLabel6;
	 Telerik::WinControls::UI::RadLabel^  radLabel2;
	 Telerik::WinControls::UI::RadSpinEditor^  box_max;
	 Telerik::WinControls::UI::RadDropDownList^  box_itemname;
	 Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
	 Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	 Telerik::WinControls::UI::RadMenuItem^  bt_save_;
	 Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
	 Telerik::WinControls::UI::RadMenuItem^  bt_load_;
	 Telerik::WinControls::UI::RadMenu^  radMenu1;
	 Telerik::WinControls::UI::RadPanel^  radPanel1;
	 Telerik::WinControls::UI::RadPanel^  radPanel2;
	 System::Windows::Forms::PictureBox^  pictureBoxItem;
	 System::Windows::Forms::PictureBox^  pictureBoxCharacterContainer;
	public: Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
	protected:
	public: Telerik::WinControls::UI::RadLabel^  radLabel3;
	public: Telerik::WinControls::UI::RadDropDownList^  box_chest;
	protected: System::Windows::Forms::PictureBox^  pictureBoxDepotBox;
	public:
	public:

	protected: static AdvancedRepoter^ unique;

	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew AdvancedRepoter();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew AdvancedRepoter();
	}
	public: static void CloseUnique(){
				if (unique)unique->Close();
			}

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}	public: static void ReloadForm(){
		unique->AdvancedRepoter_Load(nullptr, nullptr);
	}
	 Telerik::WinControls::UI::RadPageView^  PageRepot;
	 Telerik::WinControls::UI::RadLabel^  radLabel1;
	 Telerik::WinControls::UI::RadButton^  bt_delete;
	 Telerik::WinControls::UI::RadButton^  bt_add;
	 Telerik::WinControls::UI::RadDropDownList^  box_charactercontainer;
	 Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	 System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem3 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem4 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem5 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem6 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem7 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem8 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem9 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem10 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem11 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem12 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem13 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem14 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem15 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem16 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem17 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem18 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 this->PageRepot = (gcnew Telerik::WinControls::UI::RadPageView());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->bt_delete = (gcnew Telerik::WinControls::UI::RadButton());
				 this->bt_add = (gcnew Telerik::WinControls::UI::RadButton());
				 this->box_charactercontainer = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->pictureBoxItem = (gcnew System::Windows::Forms::PictureBox());
				 this->box_min = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->box_max = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->box_itemname = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->pictureBoxCharacterContainer = (gcnew System::Windows::Forms::PictureBox());
				 this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
				 this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				 this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
				 this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->box_chest = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->pictureBoxDepotBox = (gcnew System::Windows::Forms::PictureBox());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageRepot))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_charactercontainer))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxItem))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_min))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_max))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_itemname))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
				 this->radGroupBox2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxCharacterContainer))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
				 this->radPanel1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
				 this->radPanel2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
				 this->radGroupBox3->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_chest))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDepotBox))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // PageRepot
				 // 
				 this->PageRepot->BackColor = System::Drawing::Color::Transparent;
				 this->PageRepot->Location = System::Drawing::Point(0, 3);
				 this->PageRepot->Name = L"PageRepot";
				 this->PageRepot->Size = System::Drawing::Size(325, 261);
				 this->PageRepot->TabIndex = 13;
				 this->PageRepot->Text = L"PageRepot";
				 this->PageRepot->ThemeName = L"ControlDefault";
				 this->PageRepot->NewPageRequested += gcnew System::EventHandler(this, &AdvancedRepoter::PageRepot_NewPageRequested);
				 this->PageRepot->PageRemoved += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEventArgs^ >(this, &AdvancedRepoter::PageRepot_PageRemoved);
				 this->PageRepot->SelectedPageChanged += gcnew System::EventHandler(this, &AdvancedRepoter::PageRepot_SelectedPageChanged);
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->NewItemVisibility = Telerik::WinControls::UI::StripViewNewItemVisibility::End;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
				 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(77, 6);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(106, 18);
				 this->radLabel1->TabIndex = 5;
				 this->radLabel1->Text = L"Character Container";
				 this->radLabel1->ThemeName = L"ControlDefault";
				 // 
				 // bt_delete
				 // 
				 this->bt_delete->Location = System::Drawing::Point(480, 232);
				 this->bt_delete->Name = L"bt_delete";
				 this->bt_delete->Size = System::Drawing::Size(138, 24);
				 this->bt_delete->TabIndex = 10;
				 this->bt_delete->Text = L"Delete";
				 this->bt_delete->ThemeName = L"ControlDefault";
				 this->bt_delete->Click += gcnew System::EventHandler(this, &AdvancedRepoter::bt_delete_Click);
				 // 
				 // bt_add
				 // 
				 this->bt_add->Location = System::Drawing::Point(331, 232);
				 this->bt_add->Name = L"bt_add";
				 this->bt_add->Size = System::Drawing::Size(138, 24);
				 this->bt_add->TabIndex = 9;
				 this->bt_add->Text = L"New";
				 this->bt_add->ThemeName = L"ControlDefault";
				 this->bt_add->Click += gcnew System::EventHandler(this, &AdvancedRepoter::bt_add_Click_1);
				 // 
				 // box_charactercontainer
				 // 
				 this->box_charactercontainer->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
				 this->box_charactercontainer->Location = System::Drawing::Point(74, 25);
				 this->box_charactercontainer->Name = L"box_charactercontainer";
				 this->box_charactercontainer->Size = System::Drawing::Size(210, 20);
				 this->box_charactercontainer->TabIndex = 16;
				 this->box_charactercontainer->ThemeName = L"ControlDefault";
				 this->box_charactercontainer->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &AdvancedRepoter::box_charactercontainer_SelectedIndexChanged);
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->pictureBoxItem);
				 this->radGroupBox1->Controls->Add(this->box_min);
				 this->radGroupBox1->Controls->Add(this->radLabel6);
				 this->radGroupBox1->Controls->Add(this->radLabel2);
				 this->radGroupBox1->Controls->Add(this->box_max);
				 this->radGroupBox1->Controls->Add(this->box_itemname);
				 this->radGroupBox1->HeaderText = L"";
				 this->radGroupBox1->Location = System::Drawing::Point(331, 165);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(287, 62);
				 this->radGroupBox1->TabIndex = 18;
				 this->radGroupBox1->ThemeName = L"ControlDefault";
				 // 
				 // pictureBoxItem
				 // 
				 this->pictureBoxItem->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				 this->pictureBoxItem->Location = System::Drawing::Point(9, 6);
				 this->pictureBoxItem->Name = L"pictureBoxItem";
				 this->pictureBoxItem->Size = System::Drawing::Size(54, 50);
				 this->pictureBoxItem->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				 this->pictureBoxItem->TabIndex = 24;
				 this->pictureBoxItem->TabStop = false;
				 // 
				 // box_min
				 // 
				 this->box_min->Location = System::Drawing::Point(126, 34);
				 this->box_min->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
				 this->box_min->Name = L"box_min";
				 this->box_min->Size = System::Drawing::Size(51, 20);
				 this->box_min->TabIndex = 21;
				 this->box_min->TabStop = false;
				 this->box_min->ThemeName = L"ControlDefault";
				 this->box_min->ValueChanged += gcnew System::EventHandler(this, &AdvancedRepoter::box_min_ValueChanged);
				 // 
				 // radLabel6
				 // 
				 this->radLabel6->Location = System::Drawing::Point(74, 35);
				 this->radLabel6->Name = L"radLabel6";
				 this->radLabel6->Size = System::Drawing::Size(46, 18);
				 this->radLabel6->TabIndex = 20;
				 this->radLabel6->Text = L"Min Qty";
				 this->radLabel6->ThemeName = L"ControlDefault";
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(180, 37);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(48, 18);
				 this->radLabel2->TabIndex = 18;
				 this->radLabel2->Text = L"Max Qty";
				 this->radLabel2->ThemeName = L"ControlDefault";
				 // 
				 // box_max
				 // 
				 this->box_max->Location = System::Drawing::Point(232, 34);
				 this->box_max->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
				 this->box_max->Name = L"box_max";
				 this->box_max->Size = System::Drawing::Size(51, 20);
				 this->box_max->TabIndex = 22;
				 this->box_max->TabStop = false;
				 this->box_max->ThemeName = L"ControlDefault";
				 this->box_max->ValueChanged += gcnew System::EventHandler(this, &AdvancedRepoter::box_max_ValueChanged);
				 // 
				 // box_itemname
				 // 
				 this->box_itemname->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
				 this->box_itemname->Location = System::Drawing::Point(74, 9);
				 this->box_itemname->Name = L"box_itemname";
				 this->box_itemname->NullText = L"Item Name";
				 this->box_itemname->Size = System::Drawing::Size(209, 20);
				 this->box_itemname->TabIndex = 23;
				 this->box_itemname->ThemeName = L"ControlDefault";
				 this->box_itemname->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &AdvancedRepoter::box_itemname_SelectedIndexChanged);
				 this->box_itemname->TextChanged += gcnew System::EventHandler(this, &AdvancedRepoter::box_itemname_TextChanged);
				 // 
				 // radGroupBox2
				 // 
				 this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox2->Controls->Add(this->pictureBoxCharacterContainer);
				 this->radGroupBox2->Controls->Add(this->radLabel1);
				 this->radGroupBox2->Controls->Add(this->box_charactercontainer);
				 this->radGroupBox2->HeaderText = L"";
				 this->radGroupBox2->Location = System::Drawing::Point(331, 96);
				 this->radGroupBox2->Name = L"radGroupBox2";
				 this->radGroupBox2->Size = System::Drawing::Size(287, 63);
				 this->radGroupBox2->TabIndex = 21;
				 // 
				 // pictureBoxCharacterContainer
				 // 
				 this->pictureBoxCharacterContainer->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				 this->pictureBoxCharacterContainer->Location = System::Drawing::Point(9, 6);
				 this->pictureBoxCharacterContainer->Name = L"pictureBoxCharacterContainer";
				 this->pictureBoxCharacterContainer->Size = System::Drawing::Size(54, 50);
				 this->pictureBoxCharacterContainer->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				 this->pictureBoxCharacterContainer->TabIndex = 25;
				 this->pictureBoxCharacterContainer->TabStop = false;
				 // 
				 // radMenuItem1
				 // 
				 this->radMenuItem1->AccessibleDescription = L"Menu";
				 this->radMenuItem1->AccessibleName = L"Menu";
				 this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
					 this->bt_save_, this->radMenuSeparatorItem1,
						 this->bt_load_
				 });
				 this->radMenuItem1->Name = L"radMenuItem1";
				 this->radMenuItem1->Text = L"Menu";
				 // 
				 // bt_save_
				 // 
				 this->bt_save_->AccessibleDescription = L"Save";
				 this->bt_save_->AccessibleName = L"Save";
				 this->bt_save_->Name = L"bt_save_";
				 this->bt_save_->Text = L"Save";
				 this->bt_save_->Click += gcnew System::EventHandler(this, &AdvancedRepoter::bt_save__Click);
				 // 
				 // radMenuSeparatorItem1
				 // 
				 this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
				 // 
				 // bt_load_
				 // 
				 this->bt_load_->AccessibleDescription = L"Load";
				 this->bt_load_->AccessibleName = L"Load";
				 this->bt_load_->Name = L"bt_load_";
				 this->bt_load_->Text = L"Load";
				 this->bt_load_->Click += gcnew System::EventHandler(this, &AdvancedRepoter::bt_load__Click);
				 // 
				 // radMenu1
				 // 
				 this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
				 this->radMenu1->Location = System::Drawing::Point(0, 0);
				 this->radMenu1->Name = L"radMenu1";
				 this->radMenu1->Size = System::Drawing::Size(623, 20);
				 this->radMenu1->TabIndex = 22;
				 this->radMenu1->Text = L"radMenu1";
				 // 
				 // radPanel1
				 // 
				 this->radPanel1->Controls->Add(this->radMenu1);
				 this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
				 this->radPanel1->Location = System::Drawing::Point(0, 0);
				 this->radPanel1->Name = L"radPanel1";
				 this->radPanel1->Size = System::Drawing::Size(623, 20);
				 this->radPanel1->TabIndex = 23;
				 this->radPanel1->Text = L"radPanel1";
				 // 
				 // radPanel2
				 // 
				 this->radPanel2->Controls->Add(this->radGroupBox3);
				 this->radPanel2->Controls->Add(this->PageRepot);
				 this->radPanel2->Controls->Add(this->bt_add);
				 this->radPanel2->Controls->Add(this->radGroupBox2);
				 this->radPanel2->Controls->Add(this->radGroupBox1);
				 this->radPanel2->Controls->Add(this->bt_delete);
				 this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
				 this->radPanel2->Location = System::Drawing::Point(0, 20);
				 this->radPanel2->Name = L"radPanel2";
				 this->radPanel2->Size = System::Drawing::Size(623, 267);
				 this->radPanel2->TabIndex = 24;
				 // 
				 // radGroupBox3
				 // 
				 this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox3->Controls->Add(this->radLabel3);
				 this->radGroupBox3->Controls->Add(this->box_chest);
				 this->radGroupBox3->Controls->Add(this->pictureBoxDepotBox);
				 this->radGroupBox3->HeaderText = L"";
				 this->radGroupBox3->Location = System::Drawing::Point(331, 28);
				 this->radGroupBox3->Name = L"radGroupBox3";
				 this->radGroupBox3->Size = System::Drawing::Size(287, 62);
				 this->radGroupBox3->TabIndex = 25;
				 this->radGroupBox3->ThemeName = L"ControlDefault";
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(77, 6);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(59, 18);
				 this->radLabel3->TabIndex = 25;
				 this->radLabel3->Text = L"Depot Box";
				 this->radLabel3->ThemeName = L"ControlDefault";
				 // 
				 // box_chest
				 // 
				 this->box_chest->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::Suggest;
				 radListDataItem1->Text = L"Depot Chest";
				 radListDataItem2->Text = L"Depot Box I";
				 radListDataItem3->Text = L"Depot Box II";
				 radListDataItem4->Text = L"Depot Box III";
				 radListDataItem5->Text = L"Depot Box IV";
				 radListDataItem6->Text = L"Depot Box V";
				 radListDataItem7->Text = L"Depot Box VI";
				 radListDataItem8->Text = L"Depot Box VII";
				 radListDataItem9->Text = L"Depot Box VIII";
				 radListDataItem10->Text = L"Depot Box IX";
				 radListDataItem11->Text = L"Depot Box X";
				 radListDataItem12->Text = L"Depot Box XI";
				 radListDataItem13->Text = L"Depot Box XII";
				 radListDataItem14->Text = L"Depot Box XIII";
				 radListDataItem15->Text = L"Depot Box XIV";
				 radListDataItem16->Text = L"Depot Box XV";
				 radListDataItem17->Text = L"Depot Box XVI";
				 radListDataItem18->Text = L"Depot Box XVII";
				 this->box_chest->Items->Add(radListDataItem1);
				 this->box_chest->Items->Add(radListDataItem2);
				 this->box_chest->Items->Add(radListDataItem3);
				 this->box_chest->Items->Add(radListDataItem4);
				 this->box_chest->Items->Add(radListDataItem5);
				 this->box_chest->Items->Add(radListDataItem6);
				 this->box_chest->Items->Add(radListDataItem7);
				 this->box_chest->Items->Add(radListDataItem8);
				 this->box_chest->Items->Add(radListDataItem9);
				 this->box_chest->Items->Add(radListDataItem10);
				 this->box_chest->Items->Add(radListDataItem11);
				 this->box_chest->Items->Add(radListDataItem12);
				 this->box_chest->Items->Add(radListDataItem13);
				 this->box_chest->Items->Add(radListDataItem14);
				 this->box_chest->Items->Add(radListDataItem15);
				 this->box_chest->Items->Add(radListDataItem16);
				 this->box_chest->Items->Add(radListDataItem17);
				 this->box_chest->Items->Add(radListDataItem18);
				 this->box_chest->Location = System::Drawing::Point(74, 25);
				 this->box_chest->Name = L"box_chest";
				 this->box_chest->Size = System::Drawing::Size(210, 20);
				 this->box_chest->TabIndex = 26;
				 this->box_chest->ThemeName = L"ControlDefault";
				 this->box_chest->TextChanged += gcnew System::EventHandler(this, &AdvancedRepoter::box_chest_TextChanged);
				 // 
				 // pictureBoxDepotBox
				 // 
				 this->pictureBoxDepotBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				 this->pictureBoxDepotBox->Location = System::Drawing::Point(9, 6);
				 this->pictureBoxDepotBox->Name = L"pictureBoxDepotBox";
				 this->pictureBoxDepotBox->Size = System::Drawing::Size(54, 50);
				 this->pictureBoxDepotBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				 this->pictureBoxDepotBox->TabIndex = 24;
				 this->pictureBoxDepotBox->TabStop = false;
				 // 
				 // AdvancedRepoter
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(623, 287);
				 this->Controls->Add(this->radPanel2);
				 this->Controls->Add(this->radPanel1);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::Fixed3D;
				 this->MaximizeBox = false;
				 this->Name = L"AdvancedRepoter";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				 this->Text = L"Advanced Repoter";
				 this->ThemeName = L"ControlDefault";
				 this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &AdvancedRepoter::AdvancedRepoter_FormClosing);
				 this->Load += gcnew System::EventHandler(this, &AdvancedRepoter::AdvancedRepoter_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageRepot))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_charactercontainer))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxItem))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_min))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_max))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_itemname))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
				 this->radGroupBox2->ResumeLayout(false);
				 this->radGroupBox2->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxCharacterContainer))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
				 this->radPanel1->ResumeLayout(false);
				 this->radPanel1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
				 this->radPanel2->ResumeLayout(false);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
				 this->radGroupBox3->ResumeLayout(false);
				 this->radGroupBox3->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_chest))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBoxDepotBox))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion
			 System::String^ text;
			 bool disable_item_update;
			 Void waypointPageAddNewPage(std::string pageid);
			 void addItemOnView(String^ ContainerName, String^ idBp, String^ Item, int max, int min);
			 void loadItemOnView(std::string id_);
			 void loadAll();
			 void updateSelectedData();
			 Telerik::WinControls::UI::RadListView^  getItemListView();
			 bool clean = false;

	 System::Void ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
	 System::Void ItemListViewItemChanged(System::Object^  sender, System::EventArgs^  e);
	 System::Void PageRepot_NewPageRequested(System::Object^  sender, System::EventArgs^  e);
	 System::Void PageRepot_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e);
	 System::Void bt_add_Click_1(System::Object^  sender, System::EventArgs^  e);
	 System::Void bt_delete_Click(System::Object^  sender, System::EventArgs^  e);
	 System::Void AdvancedRepoter_Load(System::Object^  sender, System::EventArgs^  e);
	 System::Void Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e);
	 System::Void PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e);
	 System::Void PageRepot_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e);
	 System::Void box_itemname_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	 System::Void box_min_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	 System::Void box_max_ValueChanged(System::Object^  sender, System::EventArgs^  e);

	 void close_button(){
		 if (PageRepot->Pages->Count <= 1)
			 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
		 else
			 (cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageRepot->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::Auto;
	 }

	 System::Void box_charactercontainer_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
		if (disable_item_update)
			return;

		if (!PageRepot->SelectedPage)
			return;

		std::shared_ptr<AdvancedRepoterId> Itens = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
		Itens->set_characterBackpack(managed_util::fromSS(box_charactercontainer->Text));

		this->pictureBoxCharacterContainer->Image = managed_util::get_item_image(box_charactercontainer->Text, this->pictureBoxCharacterContainer->Height);
	}
			
	 bool save_script(std::string file_name){
				 Json::Value file;
				 file["version"] = 0100;

				 file["ADVRepoterManager"] = AdvancedRepoterManager::get()->parse_class_to_json();

				 return saveJsonFile(file_name, file);
			 }
	 bool load_script(std::string file_name){
				 Json::Value file = loadJsonFile(file_name);
				 if (file.isNull())
					 return false;

				 AdvancedRepoterManager::get()->parse_json_to_class(file["ADVRepoterManager"]);
				 return true;
			 }

	 System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::OpenFileDialog fileDialog;
				 fileDialog.Filter = "Container Repoter|*.Crepot";
				 fileDialog.Title = "Open Script";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 clean = true;
				 PageRepot->Pages->Clear();
				 AdvancedRepoterManager::get()->clear();
				 clean = false;

				 box_charactercontainer->Text = "";
				 box_chest->Text = "";

				 if (!load_script(managed_util::fromSS(fileDialog.FileName)))
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded.", "Save/Load");
				 else
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded.", "Save/Load");
				 
				 loadAll();
	}
	 System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::SaveFileDialog fileDialog;
				 fileDialog.Filter = "Container Repoter|*.Crepot";
				 fileDialog.Title = "Save Script";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 if (!save_script(managed_util::fromSS(fileDialog.FileName)))
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Saved.", "Save/Load");
				 else
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved.", "Save/Load");
	}
			
			 void update_idiom(){
				 bt_delete->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_delete->Text))[0]);
				 bt_add->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_add->Text))[0]);
				 radLabel6->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel6->Text))[0]);
				 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
				 bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
				 bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);
			 }

private: System::Void box_chest_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	if (disable_item_update)
		return;

	if (!PageRepot->SelectedPage)
		return;

	std::shared_ptr<AdvancedRepoterId> Itens = AdvancedRepoterManager::get()->getRepotIdRule(managed_util::fromSS(PageRepot->SelectedPage->Text));
	Itens->set_DepotBox(managed_util::fromSS(box_chest->Text),
		managed_util::fromSS(box_chest->Text));

	this->pictureBoxDepotBox->Image = managed_util::get_item_image(box_chest->Text, this->pictureBoxDepotBox->Height);
}
private: System::Void box_itemname_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	pictureBoxItem->Image = managed_util::get_item_image(box_itemname->Text, 30);
}
private: System::Void AdvancedRepoter_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
			 if (GeneralManager::get()->get_practice_mode()){
				 this->Hide();
				 e->Cancel = true;
				 return;
			 }
}
};
}