#pragma once
#include "KeywordManager.h"
#include <iostream>
#include "ManagedUtil.h"
#include "LanguageManager.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class Keywords : public Telerik::WinControls::UI::RadForm {
	public:
		Keywords(void){
			InitializeComponent();
			FastUIPanelController::get()->install_controller(this);
		}

	protected:	~Keywords(){
					unique = nullptr;
					if (components)
						delete components;
	}
	public: static void CloseUnique(){
				if (unique)unique->Close();
	}
	private: Telerik::WinControls::UI::RadCheckBox^  radCheckBox1;
	public:
	protected: static Keywords^ unique;
	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
		
				if (unique == nullptr)
					unique = gcnew Keywords();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}	public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew Keywords();
	}
	private: Telerik::WinControls::UI::RadButton^  radButton1;
	protected:
	private: Telerik::WinControls::UI::RadPanel^  radPanel1;
	private: Telerik::WinControls::UI::RadPanel^  radPanel2;
	private: Telerik::WinControls::UI::RadPanel^  radPanel3;
	private: Telerik::WinControls::UI::RadPanel^  radPanel4;
	private: System::Windows::Forms::Panel^  panel1;
	private: Telerik::WinControls::UI::RadCheckBox^  check_tables;
	private: Telerik::WinControls::UI::RadCheckBox^  check_functions;
	private: Telerik::WinControls::UI::RadCheckBox^  check_class;
	private: System::Windows::Forms::RichTextBox^  richTextBox1;


	protected:

	private:
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(Keywords::typeid));
			this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
			this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->richTextBox1 = (gcnew System::Windows::Forms::RichTextBox());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->check_tables = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->check_functions = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->check_class = (gcnew Telerik::WinControls::UI::RadCheckBox());
			this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
			this->radCheckBox1 = (gcnew Telerik::WinControls::UI::RadCheckBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
			this->radPanel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
			this->radPanel4->SuspendLayout();
			this->panel1->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_tables))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_functions))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_class))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
			this->radPanel2->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
			this->radPanel3->SuspendLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckBox1))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
			this->SuspendLayout();
			// 
			// radButton1
			// 
			this->radButton1->Dock = System::Windows::Forms::DockStyle::Top;
			this->radButton1->Location = System::Drawing::Point(0, 0);
			this->radButton1->Name = L"radButton1";
			this->radButton1->Size = System::Drawing::Size(357, 24);
			this->radButton1->TabIndex = 1;
			this->radButton1->Text = L"Refresh";
			this->radButton1->Click += gcnew System::EventHandler(this, &Keywords::radButton1_Click);
			// 
			// radPanel1
			// 
			this->radPanel1->Controls->Add(this->radPanel4);
			this->radPanel1->Controls->Add(this->panel1);
			this->radPanel1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel1->Location = System::Drawing::Point(0, 24);
			this->radPanel1->Name = L"radPanel1";
			this->radPanel1->Size = System::Drawing::Size(357, 259);
			this->radPanel1->TabIndex = 3;
			this->radPanel1->Text = L"radPanel1";
			// 
			// radPanel4
			// 
			this->radPanel4->Controls->Add(this->richTextBox1);
			this->radPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel4->Location = System::Drawing::Point(0, 23);
			this->radPanel4->Name = L"radPanel4";
			this->radPanel4->Size = System::Drawing::Size(357, 236);
			this->radPanel4->TabIndex = 4;
			this->radPanel4->Text = L"radPanel4";
			// 
			// richTextBox1
			// 
			this->richTextBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->richTextBox1->Location = System::Drawing::Point(0, 0);
			this->richTextBox1->Name = L"richTextBox1";
			this->richTextBox1->ReadOnly = true;
			this->richTextBox1->Size = System::Drawing::Size(357, 236);
			this->richTextBox1->TabIndex = 0;
			this->richTextBox1->Text = L"";
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->radCheckBox1);
			this->panel1->Controls->Add(this->check_tables);
			this->panel1->Controls->Add(this->check_functions);
			this->panel1->Controls->Add(this->check_class);
			this->panel1->Dock = System::Windows::Forms::DockStyle::Top;
			this->panel1->Location = System::Drawing::Point(0, 0);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(357, 23);
			this->panel1->TabIndex = 3;
			// 
			// check_tables
			// 
			this->check_tables->Location = System::Drawing::Point(192, 2);
			this->check_tables->Name = L"check_tables";
			this->check_tables->Size = System::Drawing::Size(52, 18);
			this->check_tables->TabIndex = 5;
			this->check_tables->Text = L"Tables";
			// 
			// check_functions
			// 
			this->check_functions->Location = System::Drawing::Point(88, 2);
			this->check_functions->Name = L"check_functions";
			this->check_functions->Size = System::Drawing::Size(68, 18);
			this->check_functions->TabIndex = 5;
			this->check_functions->Text = L"Functions";
			// 
			// check_class
			// 
			this->check_class->Location = System::Drawing::Point(3, 2);
			this->check_class->Name = L"check_class";
			this->check_class->Size = System::Drawing::Size(45, 18);
			this->check_class->TabIndex = 5;
			this->check_class->Text = L"Class";
			// 
			// radPanel2
			// 
			this->radPanel2->AutoSize = true;
			this->radPanel2->Controls->Add(this->radButton1);
			this->radPanel2->Dock = System::Windows::Forms::DockStyle::Top;
			this->radPanel2->Location = System::Drawing::Point(0, 0);
			this->radPanel2->Name = L"radPanel2";
			this->radPanel2->Size = System::Drawing::Size(357, 24);
			this->radPanel2->TabIndex = 4;
			this->radPanel2->Text = L"radPanel2";
			// 
			// radPanel3
			// 
			this->radPanel3->Controls->Add(this->radPanel1);
			this->radPanel3->Controls->Add(this->radPanel2);
			this->radPanel3->Dock = System::Windows::Forms::DockStyle::Fill;
			this->radPanel3->Location = System::Drawing::Point(0, 0);
			this->radPanel3->Name = L"radPanel3";
			this->radPanel3->Size = System::Drawing::Size(357, 283);
			this->radPanel3->TabIndex = 5;
			this->radPanel3->Text = L"radPanel3";
			// 
			// radCheckBox1
			// 
			this->radCheckBox1->Location = System::Drawing::Point(287, 2);
			this->radCheckBox1->Name = L"radCheckBox1";
			this->radCheckBox1->Size = System::Drawing::Size(42, 18);
			this->radCheckBox1->TabIndex = 6;
			this->radCheckBox1->Text = L"Vars";
			// 
			// Keywords
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(357, 283);
			this->Controls->Add(this->radPanel3);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->Name = L"Keywords";
			// 
			// 
			// 
			this->RootElement->ApplyShapeToControl = true;
			this->Text = L"Keywords";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
			this->radPanel1->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
			this->radPanel4->ResumeLayout(false);
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_tables))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_functions))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_class))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
			this->radPanel2->ResumeLayout(false);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
			this->radPanel3->ResumeLayout(false);
			this->radPanel3->PerformLayout();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radCheckBox1))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);
			 void update_idiom(){
				 radButton1->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton1->Text))[0]);
				 check_class->Text = gcnew String(&GET_TR(managed_util::fromSS(check_class->Text))[0]);
				 check_functions->Text = gcnew String(&GET_TR(managed_util::fromSS(check_functions->Text))[0]);
				 check_tables->Text = gcnew String(&GET_TR(managed_util::fromSS(check_tables->Text))[0]);
				 radCheckBox1->Text = gcnew String(&GET_TR(managed_util::fromSS(radCheckBox1->Text))[0]);
			 }
};
}

