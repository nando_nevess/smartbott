#include "DevelopmentForm.h"
#include "MemoryCounter.h"

using namespace NeutralBot;

void DevelopmentForm::creatMemory(){
	auto myMap = MemoryCounter::get_class_count();
	for (auto it : myMap){
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
		radListView2->Items->Add(newItem);

		newItem->Text = gcnew String(it.first.c_str());
		newItem["ClassName"] = gcnew String(it.first.c_str());
		newItem["Counter"] = it.second;
	}
}

void DevelopmentForm::updateMemory(){
	for each (auto item in radListView2->Items){
		item["Counter"] = MemoryCounter::get_counter(managed_util::fromSS(item->Text));
	}
}