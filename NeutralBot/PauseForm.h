#pragma once
#include "Core\Util.h"
#include "Core\Neutral.h"

namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class PauseForm : public System::Windows::Forms::Form{

	public:	PauseForm(void){
				InitializeComponent();
				this->Location = System::Drawing::Point(20, 20);
				hide();
				temp = false;
	}
	protected:	~PauseForm(){
					unique = nullptr;
					if (components)
						delete components;
	}
	private: System::Windows::Forms::Timer^  timer2;
	protected:

	protected:

	protected: static PauseForm^ unique;

	public: static void ShowUnique(){
				if (unique == nullptr)
					unique = gcnew PauseForm();

				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
		if (unique == nullptr)
			unique = gcnew PauseForm();
	}

	private: Telerik::WinControls::UI::RadLabel^  labelPause;
	private: System::Windows::Forms::Timer^  timer1;
	private: System::ComponentModel::IContainer^  components;
	protected:

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->labelPause = (gcnew Telerik::WinControls::UI::RadLabel());
			this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
			this->timer2 = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->labelPause))->BeginInit();
			this->SuspendLayout();
			// 
			// labelPause
			// 
			this->labelPause->BackColor = System::Drawing::Color::Transparent;
			this->labelPause->Font = (gcnew System::Drawing::Font(L"Comic Sans MS", 36, System::Drawing::FontStyle::Bold, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(0)));
			this->labelPause->ForeColor = System::Drawing::Color::Lime;
			this->labelPause->Location = System::Drawing::Point(12, 12);
			this->labelPause->Name = L"labelPause";
			this->labelPause->Size = System::Drawing::Size(150, 75);
			this->labelPause->TabIndex = 0;
			this->labelPause->Text = L"Pause";
			// 
			// timer1
			// 
			this->timer1->Enabled = true;
			this->timer1->Tick += gcnew System::EventHandler(this, &PauseForm::timer1_Tick);
			// 
			// timer2
			// 
			this->timer2->Interval = 2000;
			this->timer2->Tick += gcnew System::EventHandler(this, &PauseForm::timer2_Tick);
			// 
			// PauseForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->BackColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->CausesValidation = false;
			this->ClientSize = System::Drawing::Size(274, 106);
			this->ControlBox = false;
			this->Controls->Add(this->labelPause);
			this->ForeColor = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::None;
			this->MaximizeBox = false;
			this->MinimizeBox = false;
			this->Name = L"PauseForm";
			this->ShowIcon = false;
			this->ShowInTaskbar = false;
			this->StartPosition = System::Windows::Forms::FormStartPosition::Manual;
			this->Text = L"PauseForm";
			this->TopMost = true;
			this->TransparencyKey = System::Drawing::Color::FromArgb(static_cast<System::Int32>(static_cast<System::Byte>(0)), static_cast<System::Int32>(static_cast<System::Byte>(64)),
				static_cast<System::Int32>(static_cast<System::Byte>(0)));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->labelPause))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
		bool temp = true;
		bool first = true;
		bool first2 = true;
		HWND current_window;

		void show_Pause(){
			if (temp)
				return;

			this->Show();
			labelPause->Text = "Pause";
			labelPause->Visible = true;
			labelPause->ForeColor = System::Drawing::Color::Red;
			timer2->Enabled = true;
			temp = true;
		}

		void show_UnPause(){
			if (!temp)
				return;

			this->Show();
			labelPause->Text = "UnPause";
			labelPause->Visible = true;
			labelPause->ForeColor = System::Drawing::Color::Lime;
			timer2->Enabled = true;
			temp = false;
		}

		void hide(){
			labelPause->Visible = false;
			first = false;
		}
		
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {	
				 if (NeutralManager::get()->get_pause_state() == STATUS_RUNNING){
					 show_UnPause();
					 return;
				 }
				 else if (NeutralManager::get()->get_pause_state() == STATUS_PAUSED){
					 show_Pause();
					 return;
				 }
	}

	private: System::Void timer2_Tick(System::Object^  sender, System::EventArgs^  e) {
				 hide();
				 this->Hide();
				 timer2->Enabled = false;
	}
};
}
