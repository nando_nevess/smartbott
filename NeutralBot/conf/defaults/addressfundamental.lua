function dectohex(num)
	return tonumber(num, 16);
end

lookAddressIdChar1090					= dectohex("0x6D202C")
lookAddressPrimeiroIdBattle1090	 		= dectohex("0x72DE20")
lookAddressLogged1090					= dectohex("0x53470C")

lookAddressIdChar1092					= dectohex("0x6D3034")
lookAddressPrimeiroIdBattle1092 		= dectohex("0x72F210")
lookAddressLogged1092					= dectohex("0x53570C")

lookAddressIdChar1093					= dectohex("0x6D5034")
lookAddressPrimeiroIdBattle1093 		= dectohex("0x72F6F0")
lookAddressLogged1093					= dectohex("0x53770C")

lookAddressIdChar1094					= dectohex("0x6D9050")
lookAddressPrimeiroIdBattle1094 		= dectohex("0x7353D0")
lookAddressLogged1094					= dectohex("0x53B7C8")

lookAddressIdChar1095					= dectohex("0x6D9050")
lookAddressPrimeiroIdBattle1095 		= dectohex("0x7352C0")
lookAddressLogged1095					= dectohex("0x53B7C8")

lookAddressIdChar1096					= dectohex("0x6D9050")
lookAddressPrimeiroIdBattle1096 		= dectohex("0x733790")
lookAddressLogged1096					= dectohex("0x53B7E8")