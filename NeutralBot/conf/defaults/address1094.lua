function dectohex(num)
	return tonumber(num, 16);
end

lookAddressIdChar					= dectohex("0x6D9050")--
lookAddressPrimeiroIdBattle 				= dectohex("0x7353D0")--
AddressSkullModeNew					= dectohex("0x54C8AC")--
AddressSkullModeOld					= dectohex("0x54C9D6")--
lookAddressLogged					= dectohex("0x53B7C8")--

AddressBasePing						= {dectohex("0x1A18C"),  dectohex("0x30")}

AddressActualHotkeySchema 			= dectohex("0x525C08")--

AddressHWND							= dectohex("0x53B6B4")--
AddressTreino						= dectohex("0x53B6C4")--
XorAddress							= dectohex("0x53B740")--
AddressExp							= dectohex("0x53B748")--
AddressLvl							= dectohex("0x53B758")--

AddressSoul							= dectohex("0x53B75C")--
AddressMagicLvl						= dectohex("0x53B760")--
AddressMagicLvlPc					= dectohex("0x53B768")--
AddressTargetRed					= dectohex("0x53B770")--
AddressMp							= dectohex("0x53B774")--
AddressFirstPc						= dectohex("0x53B77C")--
AddressWhiteTarget					= dectohex("0x5376F4")
AddressStamina						= dectohex("0x53B7C0")--
AddressStatus						= dectohex("0x53B6BC")--

AddressOfWindow						= dectohex("0x53BA2C")--
AddressOfBpPointer					= dectohex("0x53BA28")--
AdressEquip							= dectohex("0x53B9CC")--

AddressMouseX1						= dectohex("0x54C688")--
AddressFollow						= dectohex("0x54B884")--
AddressCooldownBar					= dectohex("0x54C79E")--
AddressTelaY						= dectohex("0x54D28C")--
address_mouse						= dectohex("0x54C688")--
address_mouse_2						= address_mouse;
AddressTelaX						= dectohex("0x54D228")--
AddressServerMessage				= dectohex("0x6D5654")--

AddressMessagePlayer				= dectohex("0x58F0A0")--

AddressMessageNotPossible			= dectohex("0x58EE80")--


address_pointer_spells_basic		= dectohex("0x590B20")--
address_pointer_spells				= address_pointer_spells_basic 	+ 8
address_total_spells				= address_pointer_spells 		+ 4
AddressExpHour						= dectohex("0x590BBC")--

addressOfFirstMap					= dectohex("0x594B58")--
pointer_vip_players					= dectohex("0x6D50BC")--
AddressBaseLogList					= dectohex("0x6B39A0")--
AddressCtrl							= dectohex("0x6D52D0")--
AddressSchemaHotkeyReference		= dectohex("0x6D5380")--
address_item_to_be_used				= dectohex("0x6D55BC")--
AddressMouseX2						= dectohex("0x6D55D0")--

--6FF4F0
AddressMouseY2						= AddressMouseX2 + 4;
address_item_to_be_moved			= dectohex("0x6D55E0")--

AddressCap							= dectohex("0x6D9040")--

AddressHp							= dectohex("0x6D9000")--
AddressZGO							= dectohex("0x6D9008")--
AddressFirst						= dectohex("0x6D900C")--

AddressYGO							= dectohex("0x6D9044")--
AddressMaxHp						= dectohex("0x6D9048")--
AddressXGO							= dectohex("0x6D904C")--

AddressX							= dectohex("0x6D9054")--
AddressY							= AddressX + 4--
AddressZ							= AddressY + 4--

base_address_in_gui					= dectohex("0x77B3D4")--
AddressHelmet						= dectohex("0x77B578")--

PointerInicioMap					= dectohex("0x77B5C0")--
PointerInicioOffsetMap				= dectohex("0x7800F0")--

address_bp_base_new					= dectohex("0x781854")--
address_tibia_time					= dectohex("0x7823E0")--

AddressMouseY1						= AddressMouseX1 + 4
AddressMaxMp						= XorAddress + 4;


AddressClubPc					= AddressFirstPc + 4;
AddressSwordPc					= AddressFirstPc + 4 * 2;
AddressAxePc					= AddressFirstPc + 4 * 3;
AddressDistancePc				= AddressFirstPc + 4 * 4;
AddressShieldingPc				= AddressFirstPc + 4 * 5;
AddressFishingPc				= AddressFirstPc + 4 * 6;

AddressMouse_fix_x				= AddressMouseX1;
AddressMouse_fix_y				= AddressMouse_fix_x + 4;

AddressClub						= AddressFirst		+ 4;
AddressSword					= AddressFirst		+ 4 *2;
AddressAxe						= AddressFirst		+ 4 *3;
AddressDistance					= AddressFirst		+ 4 *4;
AddressShielding				= AddressFirst		+ 4 *5;
AddressFishing					= AddressFirst		+ 4 *6;

offset_beetwen_body_item		= dectohex("0x20");	

AddressAmulet					= AddressHelmet		- offset_beetwen_body_item * 1;
AddressBag						= AddressHelmet		- offset_beetwen_body_item * 2;
AddressMainBp					= AddressBag;
AddressArmor					= AddressHelmet		- offset_beetwen_body_item * 3;
AddressShield					= AddressHelmet		- offset_beetwen_body_item * 4;
AddressWeapon					= AddressHelmet		- offset_beetwen_body_item * 5;
AddressLeg						= AddressHelmet		- offset_beetwen_body_item * 6;
AddressBoot						= AddressHelmet		- offset_beetwen_body_item * 7;
AddressRing						= AddressHelmet		- offset_beetwen_body_item * 8;
AddressRope						= AddressHelmet		- offset_beetwen_body_item * 9;
AddressLogged					= lookAddressLogged;	
AddressIdChar					= lookAddressIdChar;
AddressPrimeiroNomeBattle		= lookAddressPrimeiroIdBattle + 4;




AddressAcc							= dectohex("0x5D1380")--ignore deprecated
AddressPass							= dectohex("0x5D138C")--ignore deprecated
AddressIndexChar					= dectohex("0x5D13BC")--ignore deprecated


