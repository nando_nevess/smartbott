#pragma once
#include "Core\Util.h"
#include "Core\HunterCore.h"

#pragma pack(push,1)


class TakeSkinCore{
	TimeChronometer time;
	TimeChronometer last_need_execute;
	bool take_skin_state;
	bool var_need_execute;
	neutral_mutex mtx_access;
	bool can_reset = true;
	bool update_need_execute(bool state);

public:
	TakeSkinCore();
	
	void execute_take_skin();

	void run();

	bool can_execute();

	bool use_item_corporses(Coordinate coord, uint32_t item_to_use, uint32_t corporses_id);

	bool need_operate();
	
	static TakeSkinCore* get(){
		static TakeSkinCore* mGifManager = nullptr;

		if (!mGifManager)
			mGifManager = new TakeSkinCore();

		return mGifManager;
	}
};
#pragma pack(pop)