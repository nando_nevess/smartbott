#pragma once
#include <iostream>  
#include <typeinfo>
#include "AlertsManager.h"
#include "Core\Util.h"
#include "LanguageManager.h"
#include "ManagedUtil.h"
#include "Core\AlertsCore.h"

namespace Neutral {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Media;

	public ref class Alerts : public Telerik::WinControls::UI::RadForm{
	public:
		Alerts(void){
			InitializeComponent();
			managed_util::setToggleCheckButtonStyle(radToggleButtonAlerts);
			NeutralBot::FastUIPanelController::get()->install_controller(this);
		}
		static void CloseUnique(){
			if (unique)unique->Close();
		}
	protected: ~Alerts(){
				   managed_util::unsetToggleCheckButtonStyle(radToggleButtonAlerts);
				   unique = nullptr;
				   if (components)
					   delete components;
	}

	protected:
		Telerik::WinControls::UI::RadPanel^  radPanel1;
		Telerik::WinControls::UI::RadPanel^  radPanel4;
		Telerik::WinControls::UI::RadMenu^  radMenu1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItemSave;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItemLoad;
		Telerik::WinControls::UI::RadPanel^  radPanel3;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonAlerts;
		Telerik::WinControls::UI::RadPanel^  radPanel2;
	private: Telerik::WinControls::UI::RadMenuItem^  MenuItemConfigSound;
	protected:
	protected:

	protected: static Alerts^ unique;
	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Alerts();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Alerts();
	}

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	public: static void ReloadForm(){
				unique->Alerts_Load(nullptr, nullptr);
	}
			System::ComponentModel::Container ^components;
			Telerik::WinControls::UI::RadListView^  ListAlerts;
#pragma region Windows Form Designer generated code
			void InitializeComponent(void)
			{
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0",
					L"Type"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 1",
					L"Sound"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 2",
					L"Pause Bot"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn4 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 3",
					L"Logout"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn5 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 4",
					L"Value"));
				Telerik::WinControls::UI::ListViewDataItem^  listViewDataItem1 = (gcnew Telerik::WinControls::UI::ListViewDataItem(L"Alert0", gcnew cli::array< System::String^  >(5) {
					L"edit",
						L"False", L"False", L"False", L"0"
				}));
				this->ListAlerts = (gcnew Telerik::WinControls::UI::RadListView());
				this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItemSave = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuItemLoad = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radToggleButtonAlerts = (gcnew Telerik::WinControls::UI::RadToggleButton());
				this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->MenuItemConfigSound = (gcnew Telerik::WinControls::UI::RadMenuItem());
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListAlerts))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
				this->radPanel1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
				this->radPanel4->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
				this->radPanel3->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonAlerts))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
				this->radPanel2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				this->SuspendLayout();
				// 
				// ListAlerts
				// 
				listViewDetailColumn1->HeaderText = L"Type";
				listViewDetailColumn1->MaxWidth = 220;
				listViewDetailColumn1->MinWidth = 220;
				listViewDetailColumn1->Width = 220;
				listViewDetailColumn2->HeaderText = L"Sound";
				listViewDetailColumn2->MaxWidth = 50;
				listViewDetailColumn2->MinWidth = 50;
				listViewDetailColumn2->Width = 50;
				listViewDetailColumn3->HeaderText = L"Pause Bot";
				listViewDetailColumn3->MaxWidth = 50;
				listViewDetailColumn3->MinWidth = 50;
				listViewDetailColumn3->Width = 50;
				listViewDetailColumn4->HeaderText = L"Logout";
				listViewDetailColumn4->MaxWidth = 50;
				listViewDetailColumn4->MinWidth = 50;
				listViewDetailColumn4->Width = 50;
				listViewDetailColumn5->HeaderText = L"Value";
				listViewDetailColumn5->MaxWidth = 50;
				listViewDetailColumn5->MinWidth = 50;
				listViewDetailColumn5->Width = 50;
				this->ListAlerts->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(5) {
					listViewDetailColumn1,
						listViewDetailColumn2, listViewDetailColumn3, listViewDetailColumn4, listViewDetailColumn5
				});
				this->ListAlerts->Dock = System::Windows::Forms::DockStyle::Fill;
				this->ListAlerts->HorizontalScrollState = Telerik::WinControls::UI::ScrollState::AlwaysHide;
				listViewDataItem1->Text = L"Alert0";
				this->ListAlerts->Items->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDataItem^  >(1) { listViewDataItem1 });
				this->ListAlerts->ItemSpacing = -1;
				this->ListAlerts->Location = System::Drawing::Point(0, 0);
				this->ListAlerts->Name = L"ListAlerts";
				// 
				// 
				// 
				this->ListAlerts->RootElement->SmoothingMode = System::Drawing::Drawing2D::SmoothingMode::Default;
				this->ListAlerts->ShowGridLines = true;
				this->ListAlerts->Size = System::Drawing::Size(418, 218);
				this->ListAlerts->TabIndex = 0;
				this->ListAlerts->ThemeName = L"ControlDefault";
				this->ListAlerts->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				this->ListAlerts->EditorRequired += gcnew Telerik::WinControls::UI::ListViewItemEditorRequiredEventHandler(this, &Alerts::ListAlerts_EditorRequired);
				this->ListAlerts->ItemValueChanged += gcnew Telerik::WinControls::UI::ListViewItemValueChangedEventHandler(this, &Alerts::ListAlerts_ItemValueChanged);
				this->ListAlerts->ItemValueChanging += gcnew Telerik::WinControls::UI::ListViewItemValueChangingEventHandler(this, &Alerts::ListAlerts_ItemValueChanging);
				this->ListAlerts->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Alerts::ListAlerts_ItemRemoving);
				this->ListAlerts->DoubleClick += gcnew System::EventHandler(this, &Alerts::ListAlerts_DoubleClick);
				// 
				// radPanel1
				// 
				this->radPanel1->Controls->Add(this->radPanel4);
				this->radPanel1->Controls->Add(this->radPanel3);
				this->radPanel1->Dock = System::Windows::Forms::DockStyle::Top;
				this->radPanel1->Location = System::Drawing::Point(0, 0);
				this->radPanel1->Name = L"radPanel1";
				this->radPanel1->Size = System::Drawing::Size(418, 22);
				this->radPanel1->TabIndex = 1;
				this->radPanel1->Text = L"radPanel1";
				// 
				// radPanel4
				// 
				this->radPanel4->Controls->Add(this->radMenu1);
				this->radPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel4->Location = System::Drawing::Point(100, 0);
				this->radPanel4->Name = L"radPanel4";
				this->radPanel4->Size = System::Drawing::Size(318, 22);
				this->radPanel4->TabIndex = 3;
				this->radPanel4->Text = L"radPanel4";
				// 
				// radMenu1
				// 
				this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItem1, this->MenuItemConfigSound });
				this->radMenu1->Location = System::Drawing::Point(0, 0);
				this->radMenu1->Name = L"radMenu1";
				this->radMenu1->Size = System::Drawing::Size(318, 20);
				this->radMenu1->TabIndex = 0;
				this->radMenu1->Text = L"radMenu1";
				// 
				// radMenuItem1
				// 
				this->radMenuItem1->AccessibleDescription = L"Menu";
				this->radMenuItem1->AccessibleName = L"Menu";
				this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItemSave, this->radMenuItemLoad });
				this->radMenuItem1->Name = L"radMenuItem1";
				this->radMenuItem1->Text = L"Menu";
				// 
				// radMenuItemSave
				// 
				this->radMenuItemSave->AccessibleDescription = L"radMenuItemSave";
				this->radMenuItemSave->AccessibleName = L"radMenuItemSave";
				this->radMenuItemSave->Name = L"radMenuItemSave";
				this->radMenuItemSave->Text = L"Save";
				this->radMenuItemSave->Click += gcnew System::EventHandler(this, &Alerts::bt_save__Click);
				// 
				// radMenuItemLoad
				// 
				this->radMenuItemLoad->AccessibleDescription = L"radMenuItemLoad";
				this->radMenuItemLoad->AccessibleName = L"radMenuItemLoad";
				this->radMenuItemLoad->Name = L"radMenuItemLoad";
				this->radMenuItemLoad->Text = L"Load";
				this->radMenuItemLoad->Click += gcnew System::EventHandler(this, &Alerts::bt_load__Click);
				// 
				// radPanel3
				// 
				this->radPanel3->Controls->Add(this->radToggleButtonAlerts);
				this->radPanel3->Dock = System::Windows::Forms::DockStyle::Left;
				this->radPanel3->Location = System::Drawing::Point(0, 0);
				this->radPanel3->Name = L"radPanel3";
				this->radPanel3->Size = System::Drawing::Size(100, 22);
				this->radPanel3->TabIndex = 2;
				this->radPanel3->Text = L"radPanel3";
				// 
				// radToggleButtonAlerts
				// 
				this->radToggleButtonAlerts->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radToggleButtonAlerts->Location = System::Drawing::Point(0, 0);
				this->radToggleButtonAlerts->Name = L"radToggleButtonAlerts";
				this->radToggleButtonAlerts->Size = System::Drawing::Size(100, 22);
				this->radToggleButtonAlerts->TabIndex = 0;
				this->radToggleButtonAlerts->Text = L"radToggleButton1";
				// 
				// radPanel2
				// 
				this->radPanel2->Controls->Add(this->ListAlerts);
				this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel2->Location = System::Drawing::Point(0, 22);
				this->radPanel2->Name = L"radPanel2";
				this->radPanel2->Size = System::Drawing::Size(418, 218);
				this->radPanel2->TabIndex = 2;
				this->radPanel2->Text = L"radPanel2";
				// 
				// MenuItemConfigSound
				// 
				this->MenuItemConfigSound->AccessibleDescription = L"Config Sound";
				this->MenuItemConfigSound->AccessibleName = L"Config Sound";
				this->MenuItemConfigSound->Name = L"MenuItemConfigSound";
				this->MenuItemConfigSound->Text = L"Config Sound";
				this->MenuItemConfigSound->Click += gcnew System::EventHandler(this, &Alerts::MenuItemConfigSound_Click);
				// 
				// Alerts
				// 
				this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				this->BackgroundImageLayout = System::Windows::Forms::ImageLayout::None;
				this->ClientSize = System::Drawing::Size(418, 240);
				this->Controls->Add(this->radPanel2);
				this->Controls->Add(this->radPanel1);
				this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				this->MaximizeBox = false;
				this->Name = L"Alerts";
				// 
				// 
				// 
				this->RootElement->ApplyShapeToControl = true;
				this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				this->Text = L"Alerts";
				this->ThemeName = L"ControlDefault";
				this->Load += gcnew System::EventHandler(this, &Alerts::Alerts_Load);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListAlerts))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
				this->radPanel1->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
				this->radPanel4->ResumeLayout(false);
				this->radPanel4->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
				this->radPanel3->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonAlerts))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
				this->radPanel2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				this->ResumeLayout(false);

			}
#pragma endregion
			SoundPlayer^ Player = gcnew SoundPlayer;
			bool disable_update = false;
			void loadItemOnView();
			void loadAll(); 

			System::Void ListAlerts_EditorRequired(System::Object^  sender, Telerik::WinControls::UI::ListViewItemEditorRequiredEventArgs^  e);
			System::Void ListAlerts_DoubleClick(System::Object^  sender, System::EventArgs^  e);
			System::Void ListAlerts_ItemValueChanging(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangingEventArgs^  e);
			System::Void ListAlerts_ItemValueChanged(System::Object^  sender, Telerik::WinControls::UI::ListViewItemValueChangedEventArgs^  e);
			System::Void Alerts_Load(System::Object^  sender, System::EventArgs^  e);
			System::Void ListAlerts_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);

			bool save_script(std::string file_name);
			bool load_script(std::string file_name);

			System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e);
			System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e);

			void update_idiom();

	private: System::Void MenuItemConfigSound_Click(System::Object^  sender, System::EventArgs^  e);

	};
}
