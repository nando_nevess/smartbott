#pragma once 
#include "Core\Util.h"


class NaviJsonManager{
public:
	std::string parse_json_to_string(Json::Value jsonValue){
		if (jsonValue.isNull() || jsonValue.empty())
			return "none";

		return jsonValue.toStyledString();
	}
	Json::Value parse_string_to_json(std::string stringValue){
		Json::Value root;
		Json::Reader reader;

		reader.parse(stringValue, root);

		return root;
	}

	static NaviJsonManager* get(){
		static NaviJsonManager* m = nullptr;
		if (!m)
			m = new NaviJsonManager();
		return m;
	}
};
