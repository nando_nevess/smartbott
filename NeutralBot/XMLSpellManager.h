#pragma once
#include <3PartLib\xml\xml.h>
#include "XMLSpellInfo.h"

class XMLSpellManager
{

	std::vector<XMLSpellInfo> spellsInfoList;

public:
	std::vector<XMLSpellInfo> getspellsInfoList();
	XMLSpellManager();
	void LoadSpells();
	int getSpellCooldownId(std::string name);

	bool XMLconvertStringToBoolean(std::string string);
	int XMLconvertStringToInt(std::string string);
};

extern XMLSpellManager gXMLSpellManager;
