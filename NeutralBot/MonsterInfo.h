#pragma once
#include "Core\Util.h"

class MonsterInfo{
	std::string name;
	int id;
	int min_hp =0;
	int max_hp= 100;

public:
	GET_SET(std::string, name);
	GET_SET(int, min_hp);
	GET_SET(int, max_hp);
	GET_SET(int, id);
	MonsterInfo();
	MonsterInfo(std::string name, int min_hp, int max_hp,int id);
};

