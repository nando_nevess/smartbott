#include "Core\NpcTrade.h"
#include "Core\Interface.h"
#include "Core\Input.h"
#include "Core\ContainerManager.h"


int pixel_offset_trade_items = 14;
int pixel_maximun_trade_size = 164;
const int offset_between_item_trade = 0x40;
const int offset_number_in_list = 0x3C;
const int is_not_in_trade_list = UINT32_MAX;

bool NpcTrade::is_open(){
	return Interface->get_trade_container() != nullptr;
}

bool NpcTrade::resize(){
	auto trade_interface = Interface->get_trade_container();
	if (trade_interface)
		return trade_interface->resize();
	else
		return false;
}

bool NpcTrade::scroll_up(int count){
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;
	
	Point click_point = trade_interface->get_real_location() + Point(COORDINATES::COORDINATE_TRADE_FIRST_ITEM.X, COORDINATES::COORDINATE_TRADE_SCROLL.Y);
	if (click_point.is_null())
		return false;
	
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->mouse_scroll_up(click_point, count);
	return true;
}

bool NpcTrade::scroll_down(int count){
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;

	Point click_point = trade_interface->get_real_location() + Point(COORDINATES::COORDINATE_TRADE_FIRST_ITEM.X, COORDINATES::COORDINATE_TRADE_SCROLL.Y);
	if (click_point.is_null())
		return false;

	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	input->mouse_scroll_down(click_point, count);
	return false;
}

bool NpcTrade::set_sell(){
	auto input = HighLevelVirtualInput::get_default();

	while (get_elapsed_time() < 300){
		if (!is_buy_setted())
			return true;

		auto trade_interface = Interface->get_trade_container();
		if (!trade_interface)
			return false;

		Point click_point = trade_interface->get_real_location() + Point(COORDINATES::COORDINATE_TRADE_BUTTON_SELL.X, COORDINATES::COORDINATE_TRADE_BUTTON_SELL.Y);
		if (click_point.is_null())
			return false;

		if (!input->can_use_input())
			continue;

		input->click_left(click_point);
		Sleep(50);
	}
	return false;
}

bool  NpcTrade::is_buy_setted(){
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;
	//48 -- buy 1 -- sell 0
	int res = (int)TibiaProcess::get_default()->read_byte(trade_interface->self_address + 0x44, { 0x34, 0x48 }, false);
	return res != 0;
}

bool NpcTrade::set_buy(){
	auto input = HighLevelVirtualInput::get_default();

	while (get_elapsed_time() < 500){
		if (is_buy_setted())
			return true;

		auto trade_interface = Interface->get_trade_container();
		if (!trade_interface)
			return false;

		Point click_point = trade_interface->get_real_location() + Point(COORDINATES::COORDINATE_TRADE_BUTTON_BUY.X,
			COORDINATES::COORDINATE_TRADE_BUTTON_BUY.Y); 
		if (click_point.is_null())
			return false;

		if (!input->can_use_input())
			return false;

		input->click_left(click_point);
		Sleep(50);
	}
	return false;
}

bool NpcTrade::press_ok(){
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;

	Point click_point(trade_interface->get_real_location().x + COORDINATES::COORDINATE_TRADE_BUTTON_OK.X, 
		trade_interface->get_real_location().y + trade_interface->heigth - COORDINATES::COORDINATE_TRADE_BUTTON_OK.Y);
	if (click_point.is_null())
		return false;
	
	auto input = HighLevelVirtualInput::get_default();
	TimeChronometer time;
	while (time.elapsed_milliseconds() < input->get_time_out()){
		if (input->can_use_input())
			break;
		else
			Sleep(1);
	}

	if (!input->can_use_input())
		return false;

	Sleep(200);
	input->click_left(click_point);
	return true;
}

uint32_t NpcTrade::selected_count(){
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return UINT32_MAX;

	return TibiaProcess::get_default()->read_int( trade_interface->self_address + 0x44, { 0x24, 0x34, 0x2c, 0x30 }, false);
}

uint32_t NpcTrade::hidden_item_count(){
	return TibiaProcess::get_default()->read_int(Interface->get_trade_container()->self_address + 0x44, { 0x34, 0x24, 0x24, 0x24, 0x30 }, false);
}

uint32_t NpcTrade::selected_item_id(){
	return TibiaProcess::get_default()->read_int(Interface->get_trade_container()->self_address + 0x44, { 0x24, 0x14C }, false);
}

bool NpcTrade::set_selected_index(uint32_t index){
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;	

	int visible_count = visible_item_count();
	int first_visible = hidden_item_count();
	int dif = 0;

	if (visible_count <= 3)
		dif = index - first_visible;
	else
		dif = index - first_visible + 2;

	auto location = trade_interface->get_real_location();

	if (dif >= 0 && dif <= visible_count){
		if (visible_count <= 3)
			dif -= 1;
		else
			dif -= 3;

		auto input = HighLevelVirtualInput::get_default();
		TimeChronometer time;
		while (time.elapsed_milliseconds() < input->get_time_out()){
			if (input->can_use_input())
				break;
			else
				Sleep(1);
		}

		if (!input->can_use_input())
			return false;		

		input->click_left(Point(location.x + trade_interface->width / 2, location.y + 45 + (dif * 14) + 5));
		return true;
	} 
	else {
		if (dif < 0)
			scroll_up(abs(dif) + visible_count);
		else if (dif > 0)
			scroll_down(dif - visible_count);
	}

	return false;
}

bool NpcTrade::select_item_by_id(uint32_t item_id){
	/*uint32_t selected_id = selected_item_id(); 
	if (selected_id == item_id)
		return true;

	Sleep(50);*/

	int index = index_of_item_id(item_id);
	if (index == -1)
		return false;	

	return set_selected_index(index);
}

item_trade NpcTrade::get_item_info(){
	item_trade retval = item_trade();

	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return retval;


	int address = TibiaProcess::get_default()->read_int(trade_interface->self_address + 0x44, { 0x34 }, false) != sizeof(item_trade);
	if (!address)
		return retval;


	TibiaProcess::get_default()->read_memory_block(address, &retval, sizeof(item_trade), false);
	
	return retval;
}

int NpcTrade::index_of_item_id(uint32_t item_id){	
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;

	int total_itens_on_buffer = TibiaProcess::get_default()->read_int(trade_interface->self_address + 0x44, { 0x34, 0x34 }, false);
	if (!total_itens_on_buffer)
		return false;

	int current_address = TibiaProcess::get_default()->read_int(trade_interface->self_address + 0x44, { 0x34, 0x38 }, false);

	bool not_finished = true;
	if (!current_address)
		return false;

	for (int i = 0; i < total_itens_on_buffer; i++){
		if (!is_open())
			break;

		int current_index = TibiaProcess::get_default()->read_int(current_address + offset_number_in_list, false);
		if (current_index == (uint32_t)-1){
			current_address += offset_between_item_trade;
			continue;
		}
			
		int index_item_id = TibiaProcess::get_default()->read_int(current_address, false);
		if (item_id == index_item_id)
			return (current_index + 1);

		current_address += offset_between_item_trade;
	}

	return -1;
}

bool NpcTrade::set_count(int count) {
	auto trade_interface = Interface->get_trade_container();
	if (!trade_interface)
		return false;

	if (count > 100)
		count = 100;

	else if (count < 0)
		return true;

	auto input = HighLevelVirtualInput::get_default();

	for (int i = 0; i < 3; i++){

		int s_count = selected_count();
		if (s_count == UINT32_MAX)//not found
			return false;

		int dif = s_count - count;

		if (!dif)
			return true;

		auto location = trade_interface->get_real_location();

		int cord_y = location.y + trade_interface->heigth - 60;
		int cord_x = location.x + 65;

		Sleep(50);
		TimeChronometer time;
		while (time.elapsed_milliseconds() < input->get_time_out()){
			if (input->can_use_input())
				break;
			else
				Sleep(1);
		}

		if (!input->can_use_input())
			return false;

		Sleep(50);

		if (dif > 0)
			input->mouse_scroll_up(cord_x, cord_y, abs(dif));
		 else
			input->mouse_scroll_down(cord_x, cord_y, abs(dif));

		Sleep(50);
	}
	return false;
}

bool NpcTrade::do_selected_option(int32_t item_id, int32_t count, bool condition_count,bool reorganize){
	if (count == 0)
		count = 100;

	int count_now = 0;

	uint32_t timeout_x_count = count ? count / 100 : 0;
	timeout_x_count = (timeout_x_count + 1) * 2000;
	
	while (count > 0 && is_open() && get_elapsed_time() < timeout_x_count){
		Sleep(10);
		
		if (selected_item_id() != item_id){
			select_item_by_id(item_id);
			continue;
		}

		if (count >= 100)
			count_now = 100;
		else
			count_now = count;
		
		if (condition_count){
			if (!set_count(count_now))
				continue;			
		}
		else
			set_count(count_now);

		Sleep(50);

		if (!press_ok())			
			continue;		
		
		count -= count_now;

		if (reorganize)
			ContainerManager::get()->reorganize_container();

		if (get_elapsed_time() >= timeout_x_count)
			return false;
	}
	return true;
}

uint32_t NpcTrade::visible_item_count(){
	auto trade = Interface->get_trade_container();

		TibiaProcess::get_default()->read_int(Interface->get_trade_container()->self_address + 0x44, { 0x24, 0x14C }, false);
	if (!trade)
		return UINT32_MAX;

	int list_height = trade->heigth - 117;
	if (list_height <= 0)
		return UINT32_MAX;
	int visible_count = list_height / 14;
	return visible_count;
}

bool NpcTrade::sell_item(int item_id, int qty){
	lock_guard lock(mtx_access);	
	reset_time();
	if (!set_sell())
		return false;
	return do_selected_option(item_id, qty, false, false);
}

bool NpcTrade::buy_item(int item_id, int qty){
	lock_guard lock(mtx_access);
	reset_time();
	if (!set_buy())
		return false;
	return do_selected_option(item_id, qty,false);
}

NpcTrade* NpcTrade::get(){
	static NpcTrade* mNpcTrade = nullptr;
	if (!mNpcTrade)
		mNpcTrade = new NpcTrade;
	return mNpcTrade;
}

