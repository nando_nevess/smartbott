#pragma once
#include "LooterManager.h"
#include <msclr/marshal_cppstd.h>
#include "Core\Map.h"
#include "ManagedUtil.h"
#include "HudTest.h"
#include "Core\ItemsManager.h"
#include "LanguageManager.h"
#include "Core\Inventory.h"
#include "Core\ContainerManager.h"
#include "TakeSkinCore.h"
#include "TakeSkinManager.h"
#include "GeneralManager.h"
#include "AutoGenerateLoot.h"
#include "GifManager.h"
#include "Core\\Time.h"


namespace Neutral {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace Telerik::WinControls::UI;

	public ref class Looter : public Telerik::WinControls::UI::RadForm{
		Telerik::WinControls::UI::RadLabel^  radLabel1;
		Telerik::WinControls::UI::RadLabel^  radLabel4;
		Telerik::WinControls::UI::RadLabel^  radLabel5;
		Telerik::WinControls::UI::RadLabel^  radLabel6;
		Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
		Telerik::WinControls::UI::RadDropDownList^  dropDownLootImportant;
		Telerik::WinControls::UI::RadDropDownList^  tx_nameitem;
		Telerik::WinControls::UI::RadSpinEditor^  tx_minqty;
		Telerik::WinControls::UI::RadButton^  bt_add;
		Telerik::WinControls::UI::RadButton^  bt_delete;
		Telerik::WinControls::UI::RadPageView^  PageBps;
		Telerik::WinControls::UI::RadDropDownList^  dropDownLootSequence;
		Telerik::WinControls::UI::RadDropDownList^  dropDownLootFilterMode;
		Telerik::WinControls::UI::RadDropDownList^  dropDownLootGroup;
		System::ComponentModel::IContainer^  components;
		Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
		Telerik::WinControls::UI::RadPageView^  radPageView1;
		Telerik::WinControls::UI::RadPageViewPage^  radPageViewPage1;
		Telerik::WinControls::UI::RadPageViewPage^  radPageViewPage2;
		Telerik::WinControls::UI::RadGroupBox^  radGroupBox3;
		Telerik::WinControls::UI::RadButton^  radButton1;
		System::Windows::Forms::PictureBox^  itemPictureBox;
		Telerik::WinControls::UI::RadPanel^  radPanel1;
		Telerik::WinControls::UI::RadMenu^  radMenu1;
		Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_save_;
		Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
		Telerik::WinControls::UI::RadMenuItem^  bt_load_;
		Telerik::WinControls::UI::RadPanel^  radPanel2;
		Telerik::WinControls::UI::RadToggleButton^  radToggleButtonLooter;
		Telerik::WinControls::UI::RadPanel^  radPanel3;
		Telerik::WinControls::UI::RadPanel^  radPanel5;
		Telerik::WinControls::UI::RadPanel^  radPanel4;
		System::Windows::Forms::ImageList^  containerImageList;
	private: Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem2;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem2;
	private: Telerik::WinControls::UI::RadCheckBox^  check_eat_food_from_corpses;
	private: Telerik::WinControls::UI::RadLabel^  radLabel8;
	private: Telerik::WinControls::UI::RadSpinEditor^  radSpinEditorMaxEatFoodFromCorpses;
	private: Telerik::WinControls::UI::RadSpinEditor^  radSpinEditorMinEatFoodFromCorpses;
	private: Telerik::WinControls::UI::RadCheckBox^  check_auto_reopen_containers;
	private: Telerik::WinControls::UI::RadCheckBox^  check_open_next_container;
	private: Telerik::WinControls::UI::RadDropDownList^  dropDownLootType;
	private: Telerik::WinControls::RootRadElement^  object_5d488b9a_0f42_479b_991e_49bf28826923;
	private: System::Windows::Forms::Label^  label1;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_time_to_clean;
	private: Telerik::WinControls::UI::RadLabel^  radLabel11;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_max_distance;
	private: Telerik::WinControls::UI::RadPanel^  panel_adv;
	private: Telerik::WinControls::UI::RadMenuItem^  bt_advanced;
	public: Telerik::WinControls::UI::RadListView^  ListCondition;
	private:
	private: Telerik::WinControls::UI::RadLabel^  radLabel12;
	private: Telerik::WinControls::UI::RadSpinEditor^  tx_valueADV;
	public:
	private: Telerik::WinControls::UI::RadButton^  bt_delete_condition;
	private: Telerik::WinControls::UI::RadDropDownList^  dropDownLootConditionADV;
	private: Telerik::WinControls::UI::RadButton^  bt_add_condition;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox5;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox6;
	private: Telerik::WinControls::UI::RadLabel^  radLabel14;
	private: Telerik::WinControls::UI::RadDropDownList^  dropDownLootAction;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel7;
	private: Telerik::WinControls::UI::RadButton^  radButton3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel10;
	private: Telerik::WinControls::UI::RadPageViewPage^  page_take_skin;
	private: Telerik::WinControls::UI::RadLabel^  slot_armor;
	private: Telerik::WinControls::UI::RadLabel^  slot_amulet;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot6;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot8;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot7;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot5;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot4;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot3;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot2;
	private: Telerik::WinControls::UI::RadLabel^  bp_slot1;
	private: System::Windows::Forms::PictureBox^  pictureBox4;
	private: Telerik::WinControls::UI::RadLabel^  x_mais_y_menos;
	private: Telerik::WinControls::UI::RadLabel^  x_zero_y_menos;
	private: Telerik::WinControls::UI::RadLabel^  x_menos_y_menos;
	private: Telerik::WinControls::UI::RadLabel^  x_menos_y_zero;
	private: Telerik::WinControls::UI::RadLabel^  x_menos_y_mais;
	private: Telerik::WinControls::UI::RadLabel^  x_mais_y_zero;
	private: Telerik::WinControls::UI::RadLabel^  x_mais_y_mais;
	private: Telerik::WinControls::UI::RadLabel^  x_zero_y_mais;
	private: Telerik::WinControls::UI::RadButton^  bt_refresh_slots;
	private: Telerik::WinControls::UI::RadButton^  bt_refresh_backpack;
	private: Telerik::WinControls::UI::RadButton^  bt_refresh_tile;
	private: Telerik::WinControls::UI::RadListView^  list_take_skin;
	private: Telerik::WinControls::UI::RadCheckBox^  check_take_skin;
	private: Telerik::WinControls::UI::RadButton^  bt_new_take_skin;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox4;
	private: Telerik::WinControls::UI::RadButton^  bt_delete_take_skin;
	private: Telerik::WinControls::UI::RadLabel^  radLabel9;
	private: Telerik::WinControls::UI::RadDropDownList^  box_condition_take_skin;
	private: Telerik::WinControls::UI::RadTextBox^  tx_corposes_id;
	private: Telerik::WinControls::UI::RadTextBox^  tx_item_id_take_skin;
	private: Telerik::WinControls::UI::RadLabel^  radLabel17;
	private: Telerik::WinControls::UI::RadLabel^  radLabel16;
	private: Telerik::WinControls::UI::RadLabel^  radLabel15;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_min_cap;
	private: Telerik::WinControls::UI::RadCheckBox^  check_force_move;
	private: Telerik::WinControls::UI::RadCheckBox^  check_min_cap;
private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox9;
private: Telerik::WinControls::UI::RadLabel^  slot_rope;
private: Telerik::WinControls::UI::RadLabel^  slot_shield;
private: Telerik::WinControls::UI::RadLabel^  slot_boots;
private: Telerik::WinControls::UI::RadLabel^  slot_ring;
private: Telerik::WinControls::UI::RadLabel^  slot_leg;
private: Telerik::WinControls::UI::RadLabel^  slot_weapon;
private: Telerik::WinControls::UI::RadLabel^  slot_bag;
private: Telerik::WinControls::UI::RadLabel^  slot_helmet;
private: System::Windows::Forms::PictureBox^  pictureBox3;
private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox8;
private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox7;
private: System::Windows::Forms::PictureBox^  pictureBox1;
private: Telerik::WinControls::UI::RadDropDownList^  DropDownListPriorityLooter;
private: Telerik::WinControls::UI::RadLabel^  radLabel2;
private: Telerik::WinControls::UI::RadCheckBox^  CheckBoxRecoordOnlyHunter;
private: Telerik::WinControls::UI::RadCheckBox^  CheckBoxRecoorder;
	private: Telerik::WinControls::UI::RadLabel^  radLabel13;
	private:

	public: Looter(void){
				disable_item_update = true;
				InitializeComponent();

				this->Icon = gcnew System::Drawing::Icon(System::Environment::CurrentDirectory + "\\img\\smartboticon.ico");

				Bitmap^ image1 = dynamic_cast<Bitmap^>(Image::FromFile(System::Environment::CurrentDirectory + "\\img\\tiles.png", true));
				this->pictureBox1->Image = image1;
				Bitmap^ image4 = dynamic_cast<Bitmap^>(Image::FromFile(System::Environment::CurrentDirectory + "\\img\\invetory.png", true));
				this->pictureBox3->Image = image4;
				Bitmap^ image3 = dynamic_cast<Bitmap^>(Image::FromFile(System::Environment::CurrentDirectory + "\\img\\slots.png", true));
				this->pictureBox4->Image = image3;

				NeutralBot::FastUIPanelController::get()->install_controller(this);
				
	}
	protected: ~Looter(){
				   managed_util::unsetToggleCheckButtonStyle(radToggleButtonLooter);
				   unique = nullptr;
				   if (components){
					   delete components;
				   }
	}

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	protected: static Looter^ unique;
				public: static void ReloadForm(){
							unique->Looter_Load(nullptr, nullptr);
				}
	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew Looter();
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew Looter();
	}
			static void CloseUnique(){
				if (unique)unique->Close();
			}

#pragma region Windows Form Designer generated code
			void InitializeComponent(void)
			{
				this->components = (gcnew System::ComponentModel::Container());
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Action",
					L"Action"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Condition",
					L"Condition"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn3 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Value",
					L"Value"));
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn4 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Item Id",
					L"Item Id"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn5 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Corposes Id",
					L"Corposes Id"));
				Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn6 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Condition",
					L"Condition"));
				this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel6 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->dropDownLootGroup = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->tx_minqty = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->dropDownLootImportant = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->tx_nameitem = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->bt_delete = (gcnew Telerik::WinControls::UI::RadButton());
				this->bt_add = (gcnew Telerik::WinControls::UI::RadButton());
				this->PageBps = (gcnew Telerik::WinControls::UI::RadPageView());
				this->dropDownLootSequence = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->dropDownLootFilterMode = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->CheckBoxRecoorder = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->CheckBoxRecoordOnlyHunter = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->spin_max_distance = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->radLabel11 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->label1 = (gcnew System::Windows::Forms::Label());
				this->spin_time_to_clean = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->dropDownLootType = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->spin_min_cap = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->radPageView1 = (gcnew Telerik::WinControls::UI::RadPageView());
				this->radPageViewPage1 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
				this->panel_adv = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radGroupBox6 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->radLabel14 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->dropDownLootAction = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->bt_delete_condition = (gcnew Telerik::WinControls::UI::RadButton());
				this->bt_add_condition = (gcnew Telerik::WinControls::UI::RadButton());
				this->radGroupBox5 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->radLabel12 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel13 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->dropDownLootConditionADV = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->tx_valueADV = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->ListCondition = (gcnew Telerik::WinControls::UI::RadListView());
				this->itemPictureBox = (gcnew System::Windows::Forms::PictureBox());
				this->radButton1 = (gcnew Telerik::WinControls::UI::RadButton());
				this->radPageViewPage2 = (gcnew Telerik::WinControls::UI::RadPageViewPage());
				this->radGroupBox3 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->check_min_cap = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->check_force_move = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->check_auto_reopen_containers = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->check_open_next_container = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->check_eat_food_from_corpses = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->radLabel8 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel9 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radSpinEditorMaxEatFoodFromCorpses = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->radSpinEditorMinEatFoodFromCorpses = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				this->page_take_skin = (gcnew Telerik::WinControls::UI::RadPageViewPage());
				this->DropDownListPriorityLooter = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radGroupBox9 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->slot_rope = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_shield = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_boots = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_ring = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_leg = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_weapon = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_bag = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_helmet = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bt_refresh_slots = (gcnew Telerik::WinControls::UI::RadButton());
				this->slot_amulet = (gcnew Telerik::WinControls::UI::RadLabel());
				this->slot_armor = (gcnew Telerik::WinControls::UI::RadLabel());
				this->pictureBox3 = (gcnew System::Windows::Forms::PictureBox());
				this->radGroupBox8 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->bt_refresh_backpack = (gcnew Telerik::WinControls::UI::RadButton());
				this->bp_slot1 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot2 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot3 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot4 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot5 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot7 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot8 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->bp_slot6 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->pictureBox4 = (gcnew System::Windows::Forms::PictureBox());
				this->radGroupBox7 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->bt_refresh_tile = (gcnew Telerik::WinControls::UI::RadButton());
				this->x_zero_y_mais = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_mais_y_mais = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_mais_y_zero = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_menos_y_mais = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_menos_y_zero = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_menos_y_menos = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_zero_y_menos = (gcnew Telerik::WinControls::UI::RadLabel());
				this->x_mais_y_menos = (gcnew Telerik::WinControls::UI::RadLabel());
				this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
				this->list_take_skin = (gcnew Telerik::WinControls::UI::RadListView());
				this->check_take_skin = (gcnew Telerik::WinControls::UI::RadCheckBox());
				this->bt_new_take_skin = (gcnew Telerik::WinControls::UI::RadButton());
				this->radGroupBox4 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				this->radLabel17 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel16 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel15 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->box_condition_take_skin = (gcnew Telerik::WinControls::UI::RadDropDownList());
				this->tx_corposes_id = (gcnew Telerik::WinControls::UI::RadTextBox());
				this->tx_item_id_take_skin = (gcnew Telerik::WinControls::UI::RadTextBox());
				this->bt_delete_take_skin = (gcnew Telerik::WinControls::UI::RadButton());
				this->radPanel1 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radToggleButtonLooter = (gcnew Telerik::WinControls::UI::RadToggleButton());
				this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->bt_save_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
				this->bt_load_ = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radMenuSeparatorItem2 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
				this->radMenuItem2 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->bt_advanced = (gcnew Telerik::WinControls::UI::RadMenuItem());
				this->radPanel2 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radPanel3 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radPanel5 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->radPanel4 = (gcnew Telerik::WinControls::UI::RadPanel());
				this->containerImageList = (gcnew System::Windows::Forms::ImageList(this->components));
				this->object_5d488b9a_0f42_479b_991e_49bf28826923 = (gcnew Telerik::WinControls::RootRadElement());
				this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radLabel7 = (gcnew Telerik::WinControls::UI::RadLabel());
				this->radButton3 = (gcnew Telerik::WinControls::UI::RadButton());
				this->radLabel10 = (gcnew Telerik::WinControls::UI::RadLabel());
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				this->radGroupBox1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootGroup))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_minqty))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootImportant))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_nameitem))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageBps))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootSequence))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootFilterMode))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
				this->radGroupBox2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->CheckBoxRecoorder))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->CheckBoxRecoordOnlyHunter))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_max_distance))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel11))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_time_to_clean))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootType))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_min_cap))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->BeginInit();
				this->radPageView1->SuspendLayout();
				this->radPageViewPage1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_adv))->BeginInit();
				this->panel_adv->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox6))->BeginInit();
				this->radGroupBox6->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel14))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootAction))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete_condition))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add_condition))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox5))->BeginInit();
				this->radGroupBox5->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootConditionADV))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_valueADV))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListCondition))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->itemPictureBox))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->BeginInit();
				this->radPageViewPage2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->BeginInit();
				this->radGroupBox3->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_min_cap))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_force_move))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_auto_reopen_containers))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_open_next_container))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_eat_food_from_corpses))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinEditorMaxEatFoodFromCorpses))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinEditorMinEatFoodFromCorpses))->BeginInit();
				this->page_take_skin->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListPriorityLooter))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox9))->BeginInit();
				this->radGroupBox9->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_rope))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_shield))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_boots))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_ring))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_leg))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_weapon))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_bag))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_helmet))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_refresh_slots))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_amulet))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_armor))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox8))->BeginInit();
				this->radGroupBox8->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_refresh_backpack))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot2))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot3))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot4))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot5))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot7))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot8))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot6))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox7))->BeginInit();
				this->radGroupBox7->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_refresh_tile))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_zero_y_mais))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_mais_y_mais))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_mais_y_zero))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_menos_y_mais))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_menos_y_zero))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_menos_y_menos))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_zero_y_menos))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_mais_y_menos))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->list_take_skin))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_take_skin))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_new_take_skin))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->BeginInit();
				this->radGroupBox4->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel17))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel16))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel15))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_condition_take_skin))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_corposes_id))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_item_id_take_skin))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete_take_skin))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->BeginInit();
				this->radPanel1->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLooter))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->BeginInit();
				this->radPanel2->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->BeginInit();
				this->radPanel3->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->BeginInit();
				this->radPanel5->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->BeginInit();
				this->radPanel4->SuspendLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton3))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel10))->BeginInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				this->SuspendLayout();
				// 
				// radLabel1
				// 
				this->radLabel1->Location = System::Drawing::Point(6, 9);
				this->radLabel1->Name = L"radLabel1";
				this->radLabel1->Size = System::Drawing::Size(36, 18);
				this->radLabel1->TabIndex = 2;
				this->radLabel1->Text = L"Name";
				this->radLabel1->ThemeName = L"ControlDefault";
				// 
				// radLabel4
				// 
				this->radLabel4->Location = System::Drawing::Point(6, 60);
				this->radLabel4->Name = L"radLabel4";
				this->radLabel4->Size = System::Drawing::Size(56, 18);
				this->radLabel4->TabIndex = 5;
				this->radLabel4->Text = L"Important";
				this->radLabel4->ThemeName = L"ControlDefault";
				// 
				// radLabel5
				// 
				this->radLabel5->Location = System::Drawing::Point(137, 36);
				this->radLabel5->Name = L"radLabel5";
				this->radLabel5->Size = System::Drawing::Size(51, 18);
				this->radLabel5->TabIndex = 6;
				this->radLabel5->Text = L"Min. Qty.";
				this->radLabel5->ThemeName = L"ControlDefault";
				// 
				// radLabel6
				// 
				this->radLabel6->Location = System::Drawing::Point(6, 35);
				this->radLabel6->Name = L"radLabel6";
				this->radLabel6->Size = System::Drawing::Size(38, 18);
				this->radLabel6->TabIndex = 7;
				this->radLabel6->Text = L"Group";
				this->radLabel6->ThemeName = L"ControlDefault";
				// 
				// radGroupBox1
				// 
				this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox1->Controls->Add(this->dropDownLootGroup);
				this->radGroupBox1->Controls->Add(this->tx_minqty);
				this->radGroupBox1->Controls->Add(this->radLabel6);
				this->radGroupBox1->Controls->Add(this->radLabel5);
				this->radGroupBox1->Controls->Add(this->dropDownLootImportant);
				this->radGroupBox1->Controls->Add(this->tx_nameitem);
				this->radGroupBox1->Controls->Add(this->radLabel1);
				this->radGroupBox1->Controls->Add(this->radLabel4);
				this->radGroupBox1->HeaderText = L"";
				this->radGroupBox1->Location = System::Drawing::Point(321, 80);
				this->radGroupBox1->Name = L"radGroupBox1";
				this->radGroupBox1->Size = System::Drawing::Size(256, 88);
				this->radGroupBox1->TabIndex = 26;
				this->radGroupBox1->ThemeName = L"ControlDefault";
				// 
				// dropDownLootGroup
				// 
				this->dropDownLootGroup->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootGroup->Location = System::Drawing::Point(66, 35);
				this->dropDownLootGroup->Name = L"dropDownLootGroup";
				this->dropDownLootGroup->Size = System::Drawing::Size(65, 20);
				this->dropDownLootGroup->TabIndex = 16;
				this->dropDownLootGroup->ThemeName = L"ControlDefault";
				this->dropDownLootGroup->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootGroup_SelectedIndexChanged);
				// 
				// tx_minqty
				// 
				this->tx_minqty->Location = System::Drawing::Point(194, 35);
				this->tx_minqty->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 9999, 0, 0, 0 });
				this->tx_minqty->Name = L"tx_minqty";
				this->tx_minqty->Size = System::Drawing::Size(58, 20);
				this->tx_minqty->TabIndex = 12;
				this->tx_minqty->TabStop = false;
				this->tx_minqty->ThemeName = L"ControlDefault";
				this->tx_minqty->ValueChanged += gcnew System::EventHandler(this, &Looter::tx_minqty_ValueChanged);
				// 
				// dropDownLootImportant
				// 
				this->dropDownLootImportant->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootImportant->Location = System::Drawing::Point(66, 60);
				this->dropDownLootImportant->Name = L"dropDownLootImportant";
				this->dropDownLootImportant->Size = System::Drawing::Size(186, 20);
				this->dropDownLootImportant->TabIndex = 14;
				this->dropDownLootImportant->ThemeName = L"ControlDefault";
				this->dropDownLootImportant->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootImportant_SelectedIndexChanged);
				// 
				// tx_nameitem
				// 
				this->tx_nameitem->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				this->tx_nameitem->Location = System::Drawing::Point(66, 9);
				this->tx_nameitem->Name = L"tx_nameitem";
				this->tx_nameitem->Size = System::Drawing::Size(186, 20);
				this->tx_nameitem->TabIndex = 15;
				this->tx_nameitem->ThemeName = L"ControlDefault";
				this->tx_nameitem->TextChanged += gcnew System::EventHandler(this, &Looter::tx_nameitem_TextChanged);
				// 
				// bt_delete
				// 
				this->bt_delete->Location = System::Drawing::Point(387, 50);
				this->bt_delete->Name = L"bt_delete";
				this->bt_delete->Size = System::Drawing::Size(190, 24);
				this->bt_delete->TabIndex = 15;
				this->bt_delete->Text = L"Remove";
				this->bt_delete->ThemeName = L"ControlDefault";
				this->bt_delete->Click += gcnew System::EventHandler(this, &Looter::bt_delete_Click);
				// 
				// bt_add
				// 
				this->bt_add->Location = System::Drawing::Point(387, 18);
				this->bt_add->Name = L"bt_add";
				this->bt_add->Size = System::Drawing::Size(190, 24);
				this->bt_add->TabIndex = 14;
				this->bt_add->Text = L"New";
				this->bt_add->ThemeName = L"ControlDefault";
				this->bt_add->Click += gcnew System::EventHandler(this, &Looter::bt_add_Click);
				// 
				// PageBps
				// 
				this->PageBps->ImeMode = System::Windows::Forms::ImeMode::NoControl;
				this->PageBps->Location = System::Drawing::Point(-5, -5);
				this->PageBps->Name = L"PageBps";
				this->PageBps->ShowItemToolTips = false;
				this->PageBps->Size = System::Drawing::Size(326, 212);
				this->PageBps->TabIndex = 19;
				this->PageBps->ThemeName = L"ControlDefault";
				this->PageBps->UseCompatibleTextRendering = false;
				this->PageBps->NewPageRequested += gcnew System::EventHandler(this, &Looter::PageBps_NewPageRequested);
				this->PageBps->PageRemoved += gcnew System::EventHandler<Telerik::WinControls::UI::RadPageViewEventArgs^ >(this, &Looter::PageBps_PageRemoved);
				this->PageBps->SelectedPageChanged += gcnew System::EventHandler(this, &Looter::PageBps_SelectedPageChanged);
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->NewItemVisibility = Telerik::WinControls::UI::StripViewNewItemVisibility::End;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->StripAlignment = Telerik::WinControls::UI::StripViewAlignment::Top;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ShowItemCloseButton = false;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ItemDragMode = Telerik::WinControls::UI::PageViewItemDragMode::None;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ItemSpacing = 0;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ItemSizeMode = Telerik::WinControls::UI::PageViewItemSizeMode::EqualHeight;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ItemContentOrientation = Telerik::WinControls::UI::PageViewContentOrientation::Auto;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ShouldPaint = true;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->DrawBorder = false;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->ShowHorizontalLine = false;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->PageBps->GetChildAt(0)))->BackColor = System::Drawing::Color::Transparent;
				(cli::safe_cast<Telerik::WinControls::UI::StripViewItemContainer^>(this->PageBps->GetChildAt(0)->GetChildAt(0)))->DrawFill = false;
				(cli::safe_cast<Telerik::WinControls::UI::StripViewItemContainer^>(this->PageBps->GetChildAt(0)->GetChildAt(0)))->DrawBorder = false;
				(cli::safe_cast<Telerik::WinControls::UI::StripViewItemContainer^>(this->PageBps->GetChildAt(0)->GetChildAt(0)))->DrawBackgroundImage = true;
				(cli::safe_cast<Telerik::WinControls::UI::StripViewItemContainer^>(this->PageBps->GetChildAt(0)->GetChildAt(0)))->FlipText = false;
				// 
				// dropDownLootSequence
				// 
				this->dropDownLootSequence->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootSequence->Location = System::Drawing::Point(7, 15);
				this->dropDownLootSequence->Name = L"dropDownLootSequence";
				this->dropDownLootSequence->NullText = L"Sequence Type";
				this->dropDownLootSequence->Size = System::Drawing::Size(174, 20);
				this->dropDownLootSequence->TabIndex = 28;
				this->dropDownLootSequence->ThemeName = L"ControlDefault";
				this->dropDownLootSequence->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootSequence_SelectedIndexChanged);
				// 
				// dropDownLootFilterMode
				// 
				this->dropDownLootFilterMode->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootFilterMode->Location = System::Drawing::Point(7, 41);
				this->dropDownLootFilterMode->Name = L"dropDownLootFilterMode";
				this->dropDownLootFilterMode->NullText = L"Filter Type";
				this->dropDownLootFilterMode->Size = System::Drawing::Size(174, 20);
				this->dropDownLootFilterMode->TabIndex = 29;
				this->dropDownLootFilterMode->ThemeName = L"ControlDefault";
				this->dropDownLootFilterMode->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootFilterMode_SelectedIndexChanged);
				// 
				// radGroupBox2
				// 
				this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox2->Controls->Add(this->CheckBoxRecoorder);
				this->radGroupBox2->Controls->Add(this->CheckBoxRecoordOnlyHunter);
				this->radGroupBox2->Controls->Add(this->spin_max_distance);
				this->radGroupBox2->Controls->Add(this->radLabel11);
				this->radGroupBox2->Controls->Add(this->label1);
				this->radGroupBox2->Controls->Add(this->spin_time_to_clean);
				this->radGroupBox2->Controls->Add(this->dropDownLootType);
				this->radGroupBox2->Controls->Add(this->dropDownLootSequence);
				this->radGroupBox2->Controls->Add(this->dropDownLootFilterMode);
				this->radGroupBox2->HeaderText = L"Loot Rules";
				this->radGroupBox2->Location = System::Drawing::Point(2, 96);
				this->radGroupBox2->Name = L"radGroupBox2";
				this->radGroupBox2->Size = System::Drawing::Size(439, 98);
				this->radGroupBox2->TabIndex = 32;
				this->radGroupBox2->Text = L"Loot Rules";
				// 
				// CheckBoxRecoorder
				// 
				this->CheckBoxRecoorder->Location = System::Drawing::Point(362, 15);
				this->CheckBoxRecoorder->Name = L"CheckBoxRecoorder";
				this->CheckBoxRecoorder->Size = System::Drawing::Size(72, 18);
				this->CheckBoxRecoorder->TabIndex = 36;
				this->CheckBoxRecoorder->Text = L"Recoorder";
				this->CheckBoxRecoorder->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::CheckBoxRecoorder_ToggleStateChanged);
				// 
				// CheckBoxRecoordOnlyHunter
				// 
				this->CheckBoxRecoordOnlyHunter->Location = System::Drawing::Point(187, 67);
				this->CheckBoxRecoordOnlyHunter->Name = L"CheckBoxRecoordOnlyHunter";
				this->CheckBoxRecoordOnlyHunter->Size = System::Drawing::Size(169, 18);
				this->CheckBoxRecoordOnlyHunter->TabIndex = 37;
				this->CheckBoxRecoordOnlyHunter->Text = L"Recoord only when hunter on";
				this->CheckBoxRecoordOnlyHunter->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::CheckBoxRecoordOnlyHunter_ToggleStateChanged);
				// 
				// spin_max_distance
				// 
				this->spin_max_distance->Location = System::Drawing::Point(279, 40);
				this->spin_max_distance->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 120, 0, 0, 0 });
				this->spin_max_distance->Name = L"spin_max_distance";
				this->spin_max_distance->Size = System::Drawing::Size(77, 20);
				this->spin_max_distance->TabIndex = 34;
				this->spin_max_distance->TabStop = false;
				this->spin_max_distance->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 60, 0, 0, 0 });
				this->spin_max_distance->ValueChanged += gcnew System::EventHandler(this, &Looter::spin_max_distance_ValueChanged);
				// 
				// radLabel11
				// 
				this->radLabel11->Location = System::Drawing::Point(187, 41);
				this->radLabel11->Name = L"radLabel11";
				this->radLabel11->Size = System::Drawing::Size(72, 18);
				this->radLabel11->TabIndex = 35;
				this->radLabel11->Text = L"Max distance";
				// 
				// label1
				// 
				this->label1->AutoSize = true;
				this->label1->Location = System::Drawing::Point(184, 16);
				this->label1->Name = L"label1";
				this->label1->Size = System::Drawing::Size(74, 13);
				this->label1->TabIndex = 33;
				this->label1->Text = L"Time to clean";
				// 
				// spin_time_to_clean
				// 
				this->spin_time_to_clean->Location = System::Drawing::Point(279, 14);
				this->spin_time_to_clean->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 9999999, 0, 0, 0 });
				this->spin_time_to_clean->Name = L"spin_time_to_clean";
				this->spin_time_to_clean->Size = System::Drawing::Size(77, 20);
				this->spin_time_to_clean->TabIndex = 32;
				this->spin_time_to_clean->TabStop = false;
				this->spin_time_to_clean->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 30000, 0, 0, 0 });
				this->spin_time_to_clean->ValueChanged += gcnew System::EventHandler(this, &Looter::spin_time_to_clean_ValueChanged);
				// 
				// dropDownLootType
				// 
				this->dropDownLootType->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootType->Location = System::Drawing::Point(7, 67);
				this->dropDownLootType->Name = L"dropDownLootType";
				this->dropDownLootType->NullText = L"Loot Type";
				this->dropDownLootType->Size = System::Drawing::Size(174, 20);
				this->dropDownLootType->TabIndex = 30;
				this->dropDownLootType->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootType_SelectedIndexChanged);
				// 
				// spin_min_cap
				// 
				this->spin_min_cap->Location = System::Drawing::Point(246, 59);
				this->spin_min_cap->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 120, 0, 0, 0 });
				this->spin_min_cap->Name = L"spin_min_cap";
				this->spin_min_cap->Size = System::Drawing::Size(58, 20);
				this->spin_min_cap->TabIndex = 36;
				this->spin_min_cap->TabStop = false;
				this->spin_min_cap->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 50, 0, 0, 0 });
				this->spin_min_cap->ValueChanged += gcnew System::EventHandler(this, &Looter::spin_min_cap_ValueChanged);
				// 
				// radPageView1
				// 
				this->radPageView1->Controls->Add(this->radPageViewPage1);
				this->radPageView1->Controls->Add(this->radPageViewPage2);
				this->radPageView1->Controls->Add(this->page_take_skin);
				this->radPageView1->DefaultPage = this->radPageViewPage1;
				this->radPageView1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPageView1->ForeColor = System::Drawing::Color::Black;
				this->radPageView1->Location = System::Drawing::Point(0, 0);
				this->radPageView1->Name = L"radPageView1";
				this->radPageView1->PageBackColor = System::Drawing::Color::Transparent;
				this->radPageView1->SelectedPage = this->radPageViewPage2;
				this->radPageView1->Size = System::Drawing::Size(602, 473);
				this->radPageView1->TabIndex = 35;
				this->radPageView1->Text = L"nj";
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->StripButtons = Telerik::WinControls::UI::StripViewButtons::None;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->ItemAlignment = Telerik::WinControls::UI::StripViewItemAlignment::Near;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->ItemFitMode = Telerik::WinControls::UI::StripViewItemFitMode::None;
				(cli::safe_cast<Telerik::WinControls::UI::RadPageViewStripElement^>(this->radPageView1->GetChildAt(0)))->StripAlignment = Telerik::WinControls::UI::StripViewAlignment::Top;
				// 
				// radPageViewPage1
				// 
				this->radPageViewPage1->Controls->Add(this->bt_add);
				this->radPageViewPage1->Controls->Add(this->panel_adv);
				this->radPageViewPage1->Controls->Add(this->itemPictureBox);
				this->radPageViewPage1->Controls->Add(this->radGroupBox1);
				this->radPageViewPage1->Controls->Add(this->radButton1);
				this->radPageViewPage1->Controls->Add(this->PageBps);
				this->radPageViewPage1->Controls->Add(this->bt_delete);
				this->radPageViewPage1->ItemSize = System::Drawing::SizeF(48, 28);
				this->radPageViewPage1->Location = System::Drawing::Point(10, 37);
				this->radPageViewPage1->Name = L"radPageViewPage1";
				this->radPageViewPage1->Size = System::Drawing::Size(581, 425);
				this->radPageViewPage1->Text = L"Looter";
				// 
				// panel_adv
				// 
				this->panel_adv->Controls->Add(this->radGroupBox6);
				this->panel_adv->Controls->Add(this->bt_delete_condition);
				this->panel_adv->Controls->Add(this->bt_add_condition);
				this->panel_adv->Controls->Add(this->radGroupBox5);
				this->panel_adv->Controls->Add(this->ListCondition);
				this->panel_adv->Location = System::Drawing::Point(0, 213);
				this->panel_adv->Name = L"panel_adv";
				this->panel_adv->Size = System::Drawing::Size(577, 209);
				this->panel_adv->TabIndex = 36;
				// 
				// radGroupBox6
				// 
				this->radGroupBox6->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox6->Controls->Add(this->radLabel14);
				this->radGroupBox6->Controls->Add(this->dropDownLootAction);
				this->radGroupBox6->HeaderText = L"";
				this->radGroupBox6->Location = System::Drawing::Point(361, 66);
				this->radGroupBox6->Name = L"radGroupBox6";
				this->radGroupBox6->Size = System::Drawing::Size(212, 53);
				this->radGroupBox6->TabIndex = 23;
				// 
				// radLabel14
				// 
				this->radLabel14->Location = System::Drawing::Point(5, 5);
				this->radLabel14->Name = L"radLabel14";
				this->radLabel14->Size = System::Drawing::Size(38, 18);
				this->radLabel14->TabIndex = 18;
				this->radLabel14->Text = L"Action";
				this->radLabel14->ThemeName = L"ControlDefault";
				// 
				// dropDownLootAction
				// 
				this->dropDownLootAction->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootAction->Location = System::Drawing::Point(5, 29);
				this->dropDownLootAction->Name = L"dropDownLootAction";
				this->dropDownLootAction->Size = System::Drawing::Size(202, 20);
				this->dropDownLootAction->TabIndex = 21;
				this->dropDownLootAction->ThemeName = L"ControlDefault";
				this->dropDownLootAction->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootAction_SelectedIndexChanged);
				// 
				// bt_delete_condition
				// 
				this->bt_delete_condition->Location = System::Drawing::Point(361, 36);
				this->bt_delete_condition->Name = L"bt_delete_condition";
				this->bt_delete_condition->Size = System::Drawing::Size(212, 24);
				this->bt_delete_condition->TabIndex = 16;
				this->bt_delete_condition->Text = L"Remove";
				this->bt_delete_condition->ThemeName = L"ControlDefault";
				this->bt_delete_condition->Click += gcnew System::EventHandler(this, &Looter::bt_delete_condition_Click);
				// 
				// bt_add_condition
				// 
				this->bt_add_condition->Location = System::Drawing::Point(361, 6);
				this->bt_add_condition->Name = L"bt_add_condition";
				this->bt_add_condition->Size = System::Drawing::Size(212, 24);
				this->bt_add_condition->TabIndex = 15;
				this->bt_add_condition->Text = L"New";
				this->bt_add_condition->ThemeName = L"ControlDefault";
				this->bt_add_condition->Click += gcnew System::EventHandler(this, &Looter::bt_add_condition_Click);
				// 
				// radGroupBox5
				// 
				this->radGroupBox5->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox5->Controls->Add(this->radLabel12);
				this->radGroupBox5->Controls->Add(this->radLabel13);
				this->radGroupBox5->Controls->Add(this->dropDownLootConditionADV);
				this->radGroupBox5->Controls->Add(this->tx_valueADV);
				this->radGroupBox5->HeaderText = L"";
				this->radGroupBox5->Location = System::Drawing::Point(361, 125);
				this->radGroupBox5->Name = L"radGroupBox5";
				this->radGroupBox5->Size = System::Drawing::Size(212, 78);
				this->radGroupBox5->TabIndex = 22;
				// 
				// radLabel12
				// 
				this->radLabel12->Location = System::Drawing::Point(5, 5);
				this->radLabel12->Name = L"radLabel12";
				this->radLabel12->Size = System::Drawing::Size(55, 18);
				this->radLabel12->TabIndex = 18;
				this->radLabel12->Text = L"Condition";
				this->radLabel12->ThemeName = L"ControlDefault";
				// 
				// radLabel13
				// 
				this->radLabel13->Location = System::Drawing::Point(5, 55);
				this->radLabel13->Name = L"radLabel13";
				this->radLabel13->Size = System::Drawing::Size(34, 18);
				this->radLabel13->TabIndex = 19;
				this->radLabel13->Text = L"Value";
				this->radLabel13->ThemeName = L"ControlDefault";
				// 
				// dropDownLootConditionADV
				// 
				this->dropDownLootConditionADV->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->dropDownLootConditionADV->Location = System::Drawing::Point(5, 29);
				this->dropDownLootConditionADV->Name = L"dropDownLootConditionADV";
				this->dropDownLootConditionADV->Size = System::Drawing::Size(202, 20);
				this->dropDownLootConditionADV->TabIndex = 21;
				this->dropDownLootConditionADV->ThemeName = L"ControlDefault";
				this->dropDownLootConditionADV->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::dropDownLootConditionADV_SelectedIndexChanged);
				// 
				// tx_valueADV
				// 
				this->tx_valueADV->Location = System::Drawing::Point(45, 54);
				this->tx_valueADV->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 9999, 0, 0, 0 });
				this->tx_valueADV->Name = L"tx_valueADV";
				this->tx_valueADV->Size = System::Drawing::Size(46, 20);
				this->tx_valueADV->TabIndex = 20;
				this->tx_valueADV->TabStop = false;
				this->tx_valueADV->ThemeName = L"ControlDefault";
				this->tx_valueADV->ValueChanged += gcnew System::EventHandler(this, &Looter::tx_valueADV_ValueChanged);
				// 
				// ListCondition
				// 
				this->ListCondition->AllowEdit = false;
				listViewDetailColumn1->HeaderText = L"Action";
				listViewDetailColumn1->MaxWidth = 100;
				listViewDetailColumn1->MinWidth = 100;
				listViewDetailColumn1->Width = 100;
				listViewDetailColumn2->HeaderText = L"Condition";
				listViewDetailColumn2->MaxWidth = 192;
				listViewDetailColumn2->MinWidth = 192;
				listViewDetailColumn2->Width = 192;
				listViewDetailColumn3->HeaderText = L"Value";
				listViewDetailColumn3->MaxWidth = 60;
				listViewDetailColumn3->MinWidth = 60;
				listViewDetailColumn3->Width = 60;
				this->ListCondition->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(3) {
					listViewDetailColumn1,
						listViewDetailColumn2, listViewDetailColumn3
				});
				this->ListCondition->HorizontalScrollState = Telerik::WinControls::UI::ScrollState::AlwaysHide;
				this->ListCondition->ItemSpacing = -1;
				this->ListCondition->Location = System::Drawing::Point(3, 6);
				this->ListCondition->Name = L"ListCondition";
				// 
				// 
				// 
				this->ListCondition->RootElement->SmoothingMode = System::Drawing::Drawing2D::SmoothingMode::Default;
				this->ListCondition->ShowGridLines = true;
				this->ListCondition->Size = System::Drawing::Size(352, 197);
				this->ListCondition->TabIndex = 1;
				this->ListCondition->ThemeName = L"ControlDefault";
				this->ListCondition->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				this->ListCondition->SelectedItemChanged += gcnew System::EventHandler(this, &Looter::ListAlerts_SelectedItemChanged);
				this->ListCondition->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Looter::ListAlerts_ItemRemoving);
				// 
				// itemPictureBox
				// 
				this->itemPictureBox->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->itemPictureBox->Location = System::Drawing::Point(321, 18);
				this->itemPictureBox->Name = L"itemPictureBox";
				this->itemPictureBox->Size = System::Drawing::Size(56, 56);
				this->itemPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
				this->itemPictureBox->TabIndex = 35;
				this->itemPictureBox->TabStop = false;
				// 
				// radButton1
				// 
				this->radButton1->Location = System::Drawing::Point(321, 174);
				this->radButton1->Name = L"radButton1";
				this->radButton1->Size = System::Drawing::Size(256, 24);
				this->radButton1->TabIndex = 15;
				this->radButton1->Text = L"Auto Generate Loot List";
				this->radButton1->ThemeName = L"ControlDefault";
				this->radButton1->Click += gcnew System::EventHandler(this, &Looter::radButton1_Click);
				// 
				// radPageViewPage2
				// 
				this->radPageViewPage2->Controls->Add(this->radGroupBox3);
				this->radPageViewPage2->Controls->Add(this->radGroupBox2);
				this->radPageViewPage2->ItemSize = System::Drawing::SizeF(56, 28);
				this->radPageViewPage2->Location = System::Drawing::Point(10, 37);
				this->radPageViewPage2->Name = L"radPageViewPage2";
				this->radPageViewPage2->Size = System::Drawing::Size(581, 425);
				this->radPageViewPage2->Text = L"Settings";
				// 
				// radGroupBox3
				// 
				this->radGroupBox3->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox3->Controls->Add(this->check_min_cap);
				this->radGroupBox3->Controls->Add(this->check_force_move);
				this->radGroupBox3->Controls->Add(this->spin_min_cap);
				this->radGroupBox3->Controls->Add(this->check_auto_reopen_containers);
				this->radGroupBox3->Controls->Add(this->check_open_next_container);
				this->radGroupBox3->Controls->Add(this->check_eat_food_from_corpses);
				this->radGroupBox3->Controls->Add(this->radLabel8);
				this->radGroupBox3->Controls->Add(this->radLabel9);
				this->radGroupBox3->Controls->Add(this->radSpinEditorMaxEatFoodFromCorpses);
				this->radGroupBox3->Controls->Add(this->radSpinEditorMinEatFoodFromCorpses);
				this->radGroupBox3->HeaderText = L"";
				this->radGroupBox3->Location = System::Drawing::Point(3, 3);
				this->radGroupBox3->Name = L"radGroupBox3";
				this->radGroupBox3->Size = System::Drawing::Size(439, 87);
				this->radGroupBox3->TabIndex = 0;
				// 
				// check_min_cap
				// 
				this->check_min_cap->Location = System::Drawing::Point(154, 60);
				this->check_min_cap->Name = L"check_min_cap";
				this->check_min_cap->Size = System::Drawing::Size(83, 18);
				this->check_min_cap->TabIndex = 5;
				this->check_min_cap->Text = L"Min cap loot";
				this->check_min_cap->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::check_min_cap_ToggleStateChanged);
				// 
				// check_force_move
				// 
				this->check_force_move->Location = System::Drawing::Point(154, 36);
				this->check_force_move->Name = L"check_force_move";
				this->check_force_move->Size = System::Drawing::Size(78, 18);
				this->check_force_move->TabIndex = 4;
				this->check_force_move->Text = L"Force move";
				this->check_force_move->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::check_force_move_ToggleStateChanged);
				// 
				// check_auto_reopen_containers
				// 
				this->check_auto_reopen_containers->Location = System::Drawing::Point(5, 60);
				this->check_auto_reopen_containers->Name = L"check_auto_reopen_containers";
				this->check_auto_reopen_containers->Size = System::Drawing::Size(138, 18);
				this->check_auto_reopen_containers->TabIndex = 10;
				this->check_auto_reopen_containers->Text = L"Auto reopen containers";
				this->check_auto_reopen_containers->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::check_auto_reopen_containers_ToggleStateChanged);
				// 
				// check_open_next_container
				// 
				this->check_open_next_container->Location = System::Drawing::Point(5, 36);
				this->check_open_next_container->Name = L"check_open_next_container";
				this->check_open_next_container->Size = System::Drawing::Size(122, 18);
				this->check_open_next_container->TabIndex = 3;
				this->check_open_next_container->Text = L"Open next container";
				this->check_open_next_container->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::check_open_next_container_ToggleStateChanged);
				// 
				// check_eat_food_from_corpses
				// 
				this->check_eat_food_from_corpses->Location = System::Drawing::Point(5, 12);
				this->check_eat_food_from_corpses->Name = L"check_eat_food_from_corpses";
				this->check_eat_food_from_corpses->Size = System::Drawing::Size(130, 18);
				this->check_eat_food_from_corpses->TabIndex = 9;
				this->check_eat_food_from_corpses->Text = L"Eat food from corpses";
				this->check_eat_food_from_corpses->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::check_eat_food_from_corpses_ToggleStateChanged);
				// 
				// radLabel8
				// 
				this->radLabel8->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				this->radLabel8->Location = System::Drawing::Point(307, 12);
				this->radLabel8->Name = L"radLabel8";
				this->radLabel8->Size = System::Drawing::Size(70, 18);
				this->radLabel8->TabIndex = 6;
				this->radLabel8->Text = L"max seconds";
				// 
				// radLabel9
				// 
				this->radLabel9->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				this->radLabel9->Location = System::Drawing::Point(154, 12);
				this->radLabel9->Name = L"radLabel9";
				this->radLabel9->Size = System::Drawing::Size(69, 18);
				this->radLabel9->TabIndex = 5;
				this->radLabel9->Text = L"min seconds";
				// 
				// radSpinEditorMaxEatFoodFromCorpses
				// 
				this->radSpinEditorMaxEatFoodFromCorpses->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				this->radSpinEditorMaxEatFoodFromCorpses->Location = System::Drawing::Point(383, 11);
				this->radSpinEditorMaxEatFoodFromCorpses->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000, 0, 0, 0 });
				this->radSpinEditorMaxEatFoodFromCorpses->Name = L"radSpinEditorMaxEatFoodFromCorpses";
				this->radSpinEditorMaxEatFoodFromCorpses->Size = System::Drawing::Size(51, 20);
				this->radSpinEditorMaxEatFoodFromCorpses->TabIndex = 8;
				this->radSpinEditorMaxEatFoodFromCorpses->TabStop = false;
				this->radSpinEditorMaxEatFoodFromCorpses->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 120, 0, 0, 0 });
				this->radSpinEditorMaxEatFoodFromCorpses->TextChanged += gcnew System::EventHandler(this, &Looter::radSpinEditorMaxEatFoodFromCorpses_TextChanged);
				// 
				// radSpinEditorMinEatFoodFromCorpses
				// 
				this->radSpinEditorMinEatFoodFromCorpses->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Right));
				this->radSpinEditorMinEatFoodFromCorpses->Location = System::Drawing::Point(246, 11);
				this->radSpinEditorMinEatFoodFromCorpses->Name = L"radSpinEditorMinEatFoodFromCorpses";
				this->radSpinEditorMinEatFoodFromCorpses->Size = System::Drawing::Size(58, 20);
				this->radSpinEditorMinEatFoodFromCorpses->TabIndex = 7;
				this->radSpinEditorMinEatFoodFromCorpses->TabStop = false;
				this->radSpinEditorMinEatFoodFromCorpses->Value = System::Decimal(gcnew cli::array< System::Int32 >(4) { 10, 0, 0, 0 });
				this->radSpinEditorMinEatFoodFromCorpses->TextChanged += gcnew System::EventHandler(this, &Looter::radSpinEditorMaxEatFoodFromCorpses_TextChanged);
				// 
				// page_take_skin
				// 
				this->page_take_skin->Controls->Add(this->DropDownListPriorityLooter);
				this->page_take_skin->Controls->Add(this->radLabel2);
				this->page_take_skin->Controls->Add(this->radGroupBox9);
				this->page_take_skin->Controls->Add(this->radGroupBox8);
				this->page_take_skin->Controls->Add(this->radGroupBox7);
				this->page_take_skin->Controls->Add(this->list_take_skin);
				this->page_take_skin->Controls->Add(this->check_take_skin);
				this->page_take_skin->Controls->Add(this->bt_new_take_skin);
				this->page_take_skin->Controls->Add(this->radGroupBox4);
				this->page_take_skin->Controls->Add(this->bt_delete_take_skin);
				this->page_take_skin->ItemSize = System::Drawing::SizeF(63, 28);
				this->page_take_skin->Location = System::Drawing::Point(10, 37);
				this->page_take_skin->Name = L"page_take_skin";
				this->page_take_skin->Size = System::Drawing::Size(581, 425);
				this->page_take_skin->Text = L"Take Skin";
				// 
				// DropDownListPriorityLooter
				// 
				this->DropDownListPriorityLooter->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				radListDataItem1->Text = L"True";
				radListDataItem2->Text = L"False";
				this->DropDownListPriorityLooter->Items->Add(radListDataItem1);
				this->DropDownListPriorityLooter->Items->Add(radListDataItem2);
				this->DropDownListPriorityLooter->Location = System::Drawing::Point(444, 182);
				this->DropDownListPriorityLooter->Name = L"DropDownListPriorityLooter";
				this->DropDownListPriorityLooter->Size = System::Drawing::Size(134, 20);
				this->DropDownListPriorityLooter->TabIndex = 175;
				this->DropDownListPriorityLooter->Text = L"False";
				this->DropDownListPriorityLooter->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::DropDownListPriorityLooter_SelectedIndexChanged);
				// 
				// radLabel2
				// 
				this->radLabel2->Location = System::Drawing::Point(355, 182);
				this->radLabel2->Name = L"radLabel2";
				this->radLabel2->Size = System::Drawing::Size(83, 18);
				this->radLabel2->TabIndex = 174;
				this->radLabel2->Text = L"Priority looter : ";
				// 
				// radGroupBox9
				// 
				this->radGroupBox9->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox9->Controls->Add(this->slot_rope);
				this->radGroupBox9->Controls->Add(this->slot_shield);
				this->radGroupBox9->Controls->Add(this->slot_boots);
				this->radGroupBox9->Controls->Add(this->slot_ring);
				this->radGroupBox9->Controls->Add(this->slot_leg);
				this->radGroupBox9->Controls->Add(this->slot_weapon);
				this->radGroupBox9->Controls->Add(this->slot_bag);
				this->radGroupBox9->Controls->Add(this->slot_helmet);
				this->radGroupBox9->Controls->Add(this->bt_refresh_slots);
				this->radGroupBox9->Controls->Add(this->slot_amulet);
				this->radGroupBox9->Controls->Add(this->slot_armor);
				this->radGroupBox9->Controls->Add(this->pictureBox3);
				this->radGroupBox9->HeaderText = L"";
				this->radGroupBox9->Location = System::Drawing::Point(410, 232);
				this->radGroupBox9->Name = L"radGroupBox9";
				this->radGroupBox9->Size = System::Drawing::Size(127, 190);
				this->radGroupBox9->TabIndex = 173;
				// 
				// slot_rope
				// 
				this->slot_rope->Location = System::Drawing::Point(86, 136);
				this->slot_rope->Name = L"slot_rope";
				this->slot_rope->Size = System::Drawing::Size(12, 18);
				this->slot_rope->TabIndex = 178;
				this->slot_rope->Text = L"0";
				// 
				// slot_shield
				// 
				this->slot_shield->Location = System::Drawing::Point(86, 98);
				this->slot_shield->Name = L"slot_shield";
				this->slot_shield->Size = System::Drawing::Size(12, 18);
				this->slot_shield->TabIndex = 177;
				this->slot_shield->Text = L"0";
				// 
				// slot_boots
				// 
				this->slot_boots->Location = System::Drawing::Point(48, 157);
				this->slot_boots->Name = L"slot_boots";
				this->slot_boots->Size = System::Drawing::Size(12, 18);
				this->slot_boots->TabIndex = 176;
				this->slot_boots->Text = L"0";
				// 
				// slot_ring
				// 
				this->slot_ring->Location = System::Drawing::Point(10, 136);
				this->slot_ring->Name = L"slot_ring";
				this->slot_ring->Size = System::Drawing::Size(12, 18);
				this->slot_ring->TabIndex = 175;
				this->slot_ring->Text = L"0";
				// 
				// slot_leg
				// 
				this->slot_leg->Location = System::Drawing::Point(48, 121);
				this->slot_leg->Name = L"slot_leg";
				this->slot_leg->Size = System::Drawing::Size(12, 18);
				this->slot_leg->TabIndex = 174;
				this->slot_leg->Text = L"0";
				// 
				// slot_weapon
				// 
				this->slot_weapon->Location = System::Drawing::Point(10, 98);
				this->slot_weapon->Name = L"slot_weapon";
				this->slot_weapon->Size = System::Drawing::Size(12, 18);
				this->slot_weapon->TabIndex = 173;
				this->slot_weapon->Text = L"0";
				// 
				// slot_bag
				// 
				this->slot_bag->Location = System::Drawing::Point(86, 59);
				this->slot_bag->Name = L"slot_bag";
				this->slot_bag->Size = System::Drawing::Size(12, 18);
				this->slot_bag->TabIndex = 172;
				this->slot_bag->Text = L"0";
				// 
				// slot_helmet
				// 
				this->slot_helmet->Location = System::Drawing::Point(48, 47);
				this->slot_helmet->Name = L"slot_helmet";
				this->slot_helmet->Size = System::Drawing::Size(12, 18);
				this->slot_helmet->TabIndex = 171;
				this->slot_helmet->Text = L"0";
				// 
				// bt_refresh_slots
				// 
				this->bt_refresh_slots->Location = System::Drawing::Point(10, 5);
				this->bt_refresh_slots->Name = L"bt_refresh_slots";
				this->bt_refresh_slots->Size = System::Drawing::Size(107, 23);
				this->bt_refresh_slots->TabIndex = 141;
				this->bt_refresh_slots->Text = L"Refresh";
				this->bt_refresh_slots->Click += gcnew System::EventHandler(this, &Looter::bt_refresh_slots_Click);
				// 
				// slot_amulet
				// 
				this->slot_amulet->Location = System::Drawing::Point(10, 59);
				this->slot_amulet->Name = L"slot_amulet";
				this->slot_amulet->Size = System::Drawing::Size(12, 18);
				this->slot_amulet->TabIndex = 162;
				this->slot_amulet->Text = L"0";
				// 
				// slot_armor
				// 
				this->slot_armor->Location = System::Drawing::Point(48, 84);
				this->slot_armor->Name = L"slot_armor";
				this->slot_armor->Size = System::Drawing::Size(12, 18);
				this->slot_armor->TabIndex = 163;
				this->slot_armor->Text = L"0";
				// 
				// pictureBox3
				// 
				this->pictureBox3->BorderStyle = System::Windows::Forms::BorderStyle::Fixed3D;
				this->pictureBox3->Location = System::Drawing::Point(10, 34);
				this->pictureBox3->Name = L"pictureBox3";
				this->pictureBox3->Size = System::Drawing::Size(107, 148);
				this->pictureBox3->TabIndex = 151;
				this->pictureBox3->TabStop = false;
				// 
				// radGroupBox8
				// 
				this->radGroupBox8->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox8->Controls->Add(this->bt_refresh_backpack);
				this->radGroupBox8->Controls->Add(this->bp_slot1);
				this->radGroupBox8->Controls->Add(this->bp_slot2);
				this->radGroupBox8->Controls->Add(this->bp_slot3);
				this->radGroupBox8->Controls->Add(this->bp_slot4);
				this->radGroupBox8->Controls->Add(this->bp_slot5);
				this->radGroupBox8->Controls->Add(this->bp_slot7);
				this->radGroupBox8->Controls->Add(this->bp_slot8);
				this->radGroupBox8->Controls->Add(this->bp_slot6);
				this->radGroupBox8->Controls->Add(this->pictureBox4);
				this->radGroupBox8->HeaderText = L"";
				this->radGroupBox8->Location = System::Drawing::Point(222, 232);
				this->radGroupBox8->Name = L"radGroupBox8";
				this->radGroupBox8->Size = System::Drawing::Size(182, 190);
				this->radGroupBox8->TabIndex = 172;
				// 
				// bt_refresh_backpack
				// 
				this->bt_refresh_backpack->Location = System::Drawing::Point(10, 5);
				this->bt_refresh_backpack->Name = L"bt_refresh_backpack";
				this->bt_refresh_backpack->Size = System::Drawing::Size(162, 23);
				this->bt_refresh_backpack->TabIndex = 140;
				this->bt_refresh_backpack->Text = L"Refresh";
				this->bt_refresh_backpack->Click += gcnew System::EventHandler(this, &Looter::bt_refresh_backpack_Click);
				// 
				// bp_slot1
				// 
				this->bp_slot1->Location = System::Drawing::Point(22, 63);
				this->bp_slot1->Name = L"bp_slot1";
				this->bp_slot1->Size = System::Drawing::Size(12, 18);
				this->bp_slot1->TabIndex = 153;
				this->bp_slot1->Text = L"0";
				// 
				// bp_slot2
				// 
				this->bp_slot2->Location = System::Drawing::Point(59, 63);
				this->bp_slot2->Name = L"bp_slot2";
				this->bp_slot2->Size = System::Drawing::Size(12, 18);
				this->bp_slot2->TabIndex = 154;
				this->bp_slot2->Text = L"0";
				// 
				// bp_slot3
				// 
				this->bp_slot3->Location = System::Drawing::Point(96, 63);
				this->bp_slot3->Name = L"bp_slot3";
				this->bp_slot3->Size = System::Drawing::Size(12, 18);
				this->bp_slot3->TabIndex = 155;
				this->bp_slot3->Text = L"0";
				// 
				// bp_slot4
				// 
				this->bp_slot4->Location = System::Drawing::Point(133, 63);
				this->bp_slot4->Name = L"bp_slot4";
				this->bp_slot4->Size = System::Drawing::Size(12, 18);
				this->bp_slot4->TabIndex = 156;
				this->bp_slot4->Text = L"0";
				// 
				// bp_slot5
				// 
				this->bp_slot5->Location = System::Drawing::Point(22, 98);
				this->bp_slot5->Name = L"bp_slot5";
				this->bp_slot5->Size = System::Drawing::Size(12, 18);
				this->bp_slot5->TabIndex = 157;
				this->bp_slot5->Text = L"0";
				// 
				// bp_slot7
				// 
				this->bp_slot7->Location = System::Drawing::Point(96, 98);
				this->bp_slot7->Name = L"bp_slot7";
				this->bp_slot7->Size = System::Drawing::Size(12, 18);
				this->bp_slot7->TabIndex = 158;
				this->bp_slot7->Text = L"0";
				// 
				// bp_slot8
				// 
				this->bp_slot8->Location = System::Drawing::Point(133, 98);
				this->bp_slot8->Name = L"bp_slot8";
				this->bp_slot8->Size = System::Drawing::Size(12, 18);
				this->bp_slot8->TabIndex = 159;
				this->bp_slot8->Text = L"0";
				// 
				// bp_slot6
				// 
				this->bp_slot6->Location = System::Drawing::Point(59, 98);
				this->bp_slot6->Name = L"bp_slot6";
				this->bp_slot6->Size = System::Drawing::Size(12, 18);
				this->bp_slot6->TabIndex = 160;
				this->bp_slot6->Text = L"0";
				// 
				// pictureBox4
				// 
				this->pictureBox4->BorderStyle = System::Windows::Forms::BorderStyle::FixedSingle;
				this->pictureBox4->Location = System::Drawing::Point(10, 34);
				this->pictureBox4->Name = L"pictureBox4";
				this->pictureBox4->Size = System::Drawing::Size(162, 148);
				this->pictureBox4->TabIndex = 152;
				this->pictureBox4->TabStop = false;
				// 
				// radGroupBox7
				// 
				this->radGroupBox7->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox7->Controls->Add(this->bt_refresh_tile);
				this->radGroupBox7->Controls->Add(this->x_zero_y_mais);
				this->radGroupBox7->Controls->Add(this->x_mais_y_mais);
				this->radGroupBox7->Controls->Add(this->x_mais_y_zero);
				this->radGroupBox7->Controls->Add(this->x_menos_y_mais);
				this->radGroupBox7->Controls->Add(this->x_menos_y_zero);
				this->radGroupBox7->Controls->Add(this->x_menos_y_menos);
				this->radGroupBox7->Controls->Add(this->x_zero_y_menos);
				this->radGroupBox7->Controls->Add(this->x_mais_y_menos);
				this->radGroupBox7->Controls->Add(this->pictureBox1);
				this->radGroupBox7->HeaderText = L"";
				this->radGroupBox7->Location = System::Drawing::Point(46, 232);
				this->radGroupBox7->Name = L"radGroupBox7";
				this->radGroupBox7->Size = System::Drawing::Size(170, 190);
				this->radGroupBox7->TabIndex = 171;
				// 
				// bt_refresh_tile
				// 
				this->bt_refresh_tile->Location = System::Drawing::Point(10, 5);
				this->bt_refresh_tile->Name = L"bt_refresh_tile";
				this->bt_refresh_tile->Size = System::Drawing::Size(150, 23);
				this->bt_refresh_tile->TabIndex = 139;
				this->bt_refresh_tile->Text = L"Refresh";
				this->bt_refresh_tile->Click += gcnew System::EventHandler(this, &Looter::bt_refresh_tile_Click);
				// 
				// x_zero_y_mais
				// 
				this->x_zero_y_mais->BackColor = System::Drawing::Color::Transparent;
				this->x_zero_y_mais->Location = System::Drawing::Point(61, 150);
				this->x_zero_y_mais->Name = L"x_zero_y_mais";
				this->x_zero_y_mais->Size = System::Drawing::Size(12, 18);
				this->x_zero_y_mais->TabIndex = 144;
				this->x_zero_y_mais->Text = L"0";
				// 
				// x_mais_y_mais
				// 
				this->x_mais_y_mais->BackColor = System::Drawing::Color::Transparent;
				this->x_mais_y_mais->Location = System::Drawing::Point(113, 150);
				this->x_mais_y_mais->Name = L"x_mais_y_mais";
				this->x_mais_y_mais->Size = System::Drawing::Size(12, 18);
				this->x_mais_y_mais->TabIndex = 145;
				this->x_mais_y_mais->Text = L"0";
				// 
				// x_mais_y_zero
				// 
				this->x_mais_y_zero->BackColor = System::Drawing::Color::Transparent;
				this->x_mais_y_zero->Location = System::Drawing::Point(113, 102);
				this->x_mais_y_zero->Name = L"x_mais_y_zero";
				this->x_mais_y_zero->Size = System::Drawing::Size(12, 18);
				this->x_mais_y_zero->TabIndex = 147;
				this->x_mais_y_zero->Text = L"0";
				// 
				// x_menos_y_mais
				// 
				this->x_menos_y_mais->BackColor = System::Drawing::Color::Transparent;
				this->x_menos_y_mais->ForeColor = System::Drawing::Color::Black;
				this->x_menos_y_mais->Location = System::Drawing::Point(10, 150);
				this->x_menos_y_mais->Name = L"x_menos_y_mais";
				this->x_menos_y_mais->Size = System::Drawing::Size(12, 18);
				this->x_menos_y_mais->TabIndex = 143;
				this->x_menos_y_mais->Text = L"0";
				// 
				// x_menos_y_zero
				// 
				this->x_menos_y_zero->BackColor = System::Drawing::Color::Transparent;
				this->x_menos_y_zero->Location = System::Drawing::Point(10, 102);
				this->x_menos_y_zero->Name = L"x_menos_y_zero";
				this->x_menos_y_zero->Size = System::Drawing::Size(12, 18);
				this->x_menos_y_zero->TabIndex = 146;
				this->x_menos_y_zero->Text = L"0";
				// 
				// x_menos_y_menos
				// 
				this->x_menos_y_menos->BackColor = System::Drawing::Color::Transparent;
				this->x_menos_y_menos->Location = System::Drawing::Point(10, 53);
				this->x_menos_y_menos->Name = L"x_menos_y_menos";
				this->x_menos_y_menos->Size = System::Drawing::Size(12, 18);
				this->x_menos_y_menos->TabIndex = 148;
				this->x_menos_y_menos->Text = L"0";
				// 
				// x_zero_y_menos
				// 
				this->x_zero_y_menos->BackColor = System::Drawing::Color::Transparent;
				this->x_zero_y_menos->Location = System::Drawing::Point(61, 53);
				this->x_zero_y_menos->Name = L"x_zero_y_menos";
				this->x_zero_y_menos->Size = System::Drawing::Size(12, 18);
				this->x_zero_y_menos->TabIndex = 149;
				this->x_zero_y_menos->Text = L"0";
				// 
				// x_mais_y_menos
				// 
				this->x_mais_y_menos->BackColor = System::Drawing::Color::Transparent;
				this->x_mais_y_menos->Location = System::Drawing::Point(113, 53);
				this->x_mais_y_menos->Name = L"x_mais_y_menos";
				this->x_mais_y_menos->Size = System::Drawing::Size(12, 18);
				this->x_mais_y_menos->TabIndex = 150;
				this->x_mais_y_menos->Text = L"0";
				// 
				// pictureBox1
				// 
				this->pictureBox1->Location = System::Drawing::Point(10, 34);
				this->pictureBox1->Name = L"pictureBox1";
				this->pictureBox1->Size = System::Drawing::Size(150, 148);
				this->pictureBox1->TabIndex = 142;
				this->pictureBox1->TabStop = false;
				// 
				// list_take_skin
				// 
				this->list_take_skin->AllowEdit = false;
				listViewDetailColumn4->HeaderText = L"Item Id";
				listViewDetailColumn4->MaxWidth = 50;
				listViewDetailColumn4->MinWidth = 50;
				listViewDetailColumn4->Width = 50;
				listViewDetailColumn5->HeaderText = L"Corposes Id";
				listViewDetailColumn5->MaxWidth = 70;
				listViewDetailColumn5->MinWidth = 70;
				listViewDetailColumn5->Width = 70;
				listViewDetailColumn6->HeaderText = L"Condition";
				listViewDetailColumn6->MaxWidth = 225;
				listViewDetailColumn6->MinWidth = 225;
				listViewDetailColumn6->Width = 225;
				this->list_take_skin->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(3) {
					listViewDetailColumn4,
						listViewDetailColumn5, listViewDetailColumn6
				});
				this->list_take_skin->ItemSpacing = -1;
				this->list_take_skin->Location = System::Drawing::Point(3, 3);
				this->list_take_skin->Name = L"list_take_skin";
				this->list_take_skin->ShowGridLines = true;
				this->list_take_skin->Size = System::Drawing::Size(346, 225);
				this->list_take_skin->TabIndex = 8;
				this->list_take_skin->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				this->list_take_skin->SelectedItemChanged += gcnew System::EventHandler(this, &Looter::take_skin_list_ItemChanged);
				this->list_take_skin->ItemRemoving += gcnew Telerik::WinControls::UI::ListViewItemCancelEventHandler(this, &Looter::list_take_skin_ItemRemoving);
				// 
				// check_take_skin
				// 
				this->check_take_skin->Location = System::Drawing::Point(362, 70);
				this->check_take_skin->Name = L"check_take_skin";
				this->check_take_skin->Size = System::Drawing::Size(67, 18);
				this->check_take_skin->TabIndex = 7;
				this->check_take_skin->Text = L"Take Skin";
				this->check_take_skin->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::radCheckBox1_ToggleStateChanged);
				// 
				// bt_new_take_skin
				// 
				this->bt_new_take_skin->Location = System::Drawing::Point(355, 3);
				this->bt_new_take_skin->Name = L"bt_new_take_skin";
				this->bt_new_take_skin->Size = System::Drawing::Size(223, 27);
				this->bt_new_take_skin->TabIndex = 6;
				this->bt_new_take_skin->Text = L"New";
				this->bt_new_take_skin->Click += gcnew System::EventHandler(this, &Looter::bt_new_take_skin_Click);
				// 
				// radGroupBox4
				// 
				this->radGroupBox4->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				this->radGroupBox4->Controls->Add(this->radLabel17);
				this->radGroupBox4->Controls->Add(this->radLabel16);
				this->radGroupBox4->Controls->Add(this->radLabel15);
				this->radGroupBox4->Controls->Add(this->box_condition_take_skin);
				this->radGroupBox4->Controls->Add(this->tx_corposes_id);
				this->radGroupBox4->Controls->Add(this->tx_item_id_take_skin);
				this->radGroupBox4->HeaderText = L"";
				this->radGroupBox4->Location = System::Drawing::Point(355, 79);
				this->radGroupBox4->Name = L"radGroupBox4";
				this->radGroupBox4->Size = System::Drawing::Size(223, 97);
				this->radGroupBox4->TabIndex = 5;
				// 
				// radLabel17
				// 
				this->radLabel17->Location = System::Drawing::Point(7, 67);
				this->radLabel17->Name = L"radLabel17";
				this->radLabel17->Size = System::Drawing::Size(54, 18);
				this->radLabel17->TabIndex = 174;
				this->radLabel17->Text = L"condition";
				// 
				// radLabel16
				// 
				this->radLabel16->Location = System::Drawing::Point(5, 42);
				this->radLabel16->Name = L"radLabel16";
				this->radLabel16->Size = System::Drawing::Size(65, 18);
				this->radLabel16->TabIndex = 173;
				this->radLabel16->Text = L"Corposes Id";
				// 
				// radLabel15
				// 
				this->radLabel15->Location = System::Drawing::Point(5, 16);
				this->radLabel15->Name = L"radLabel15";
				this->radLabel15->Size = System::Drawing::Size(41, 18);
				this->radLabel15->TabIndex = 172;
				this->radLabel15->Text = L"Item Id";
				// 
				// box_condition_take_skin
				// 
				this->box_condition_take_skin->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				this->box_condition_take_skin->Location = System::Drawing::Point(76, 67);
				this->box_condition_take_skin->MaxLength = 65535;
				this->box_condition_take_skin->Name = L"box_condition_take_skin";
				this->box_condition_take_skin->Size = System::Drawing::Size(142, 20);
				this->box_condition_take_skin->TabIndex = 40;
				this->box_condition_take_skin->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &Looter::box_condition_take_skin_SelectedIndexChanged);
				// 
				// tx_corposes_id
				// 
				this->tx_corposes_id->AcceptsTab = true;
				this->tx_corposes_id->HideSelection = false;
				this->tx_corposes_id->Location = System::Drawing::Point(76, 41);
				this->tx_corposes_id->MaxLength = 65535;
				this->tx_corposes_id->Name = L"tx_corposes_id";
				this->tx_corposes_id->Size = System::Drawing::Size(142, 20);
				this->tx_corposes_id->TabIndex = 40;
				this->tx_corposes_id->WordWrap = false;
				this->tx_corposes_id->TextChanged += gcnew System::EventHandler(this, &Looter::tx_corposes_id_TextChanged);
				this->tx_corposes_id->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Looter::tx_corposes_id_KeyPress);
				// 
				// tx_item_id_take_skin
				// 
				this->tx_item_id_take_skin->AcceptsTab = true;
				this->tx_item_id_take_skin->HideSelection = false;
				this->tx_item_id_take_skin->Location = System::Drawing::Point(76, 15);
				this->tx_item_id_take_skin->MaxLength = 65535;
				this->tx_item_id_take_skin->Name = L"tx_item_id_take_skin";
				this->tx_item_id_take_skin->Size = System::Drawing::Size(142, 20);
				this->tx_item_id_take_skin->TabIndex = 171;
				this->tx_item_id_take_skin->WordWrap = false;
				this->tx_item_id_take_skin->TextChanged += gcnew System::EventHandler(this, &Looter::tx_item_id_take_skin_TextChanged);
				this->tx_item_id_take_skin->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &Looter::tx_corposes_id_KeyPress);
				// 
				// bt_delete_take_skin
				// 
				this->bt_delete_take_skin->Location = System::Drawing::Point(355, 36);
				this->bt_delete_take_skin->Name = L"bt_delete_take_skin";
				this->bt_delete_take_skin->Size = System::Drawing::Size(223, 27);
				this->bt_delete_take_skin->TabIndex = 5;
				this->bt_delete_take_skin->Text = L"Remove";
				this->bt_delete_take_skin->Click += gcnew System::EventHandler(this, &Looter::bt_delete_take_skin_Click);
				// 
				// radPanel1
				// 
				this->radPanel1->Controls->Add(this->radToggleButtonLooter);
				this->radPanel1->Dock = System::Windows::Forms::DockStyle::Left;
				this->radPanel1->Location = System::Drawing::Point(0, 0);
				this->radPanel1->Name = L"radPanel1";
				this->radPanel1->Size = System::Drawing::Size(100, 22);
				this->radPanel1->TabIndex = 36;
				this->radPanel1->Text = L"radPanel1";
				// 
				// radToggleButtonLooter
				// 
				this->radToggleButtonLooter->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radToggleButtonLooter->Location = System::Drawing::Point(0, 0);
				this->radToggleButtonLooter->Name = L"radToggleButtonLooter";
				this->radToggleButtonLooter->Size = System::Drawing::Size(100, 22);
				this->radToggleButtonLooter->TabIndex = 0;
				this->radToggleButtonLooter->Text = L"radToggleButton1";
				this->radToggleButtonLooter->ToggleStateChanged += gcnew Telerik::WinControls::UI::StateChangedEventHandler(this, &Looter::radToggleButtonLooter_ToggleStateChanged);
				// 
				// radMenu1
				// 
				this->radMenu1->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(2) { this->radMenuItem1, this->bt_advanced });
				this->radMenu1->Location = System::Drawing::Point(0, 0);
				this->radMenu1->Name = L"radMenu1";
				this->radMenu1->Size = System::Drawing::Size(502, 20);
				this->radMenu1->TabIndex = 0;
				this->radMenu1->Text = L"radMenu1";
				// 
				// radMenuItem1
				// 
				this->radMenuItem1->AccessibleDescription = L"Menu";
				this->radMenuItem1->AccessibleName = L"Menu";
				this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(5) {
					this->bt_save_, this->radMenuSeparatorItem1,
						this->bt_load_, this->radMenuSeparatorItem2, this->radMenuItem2
				});
				this->radMenuItem1->Name = L"radMenuItem1";
				this->radMenuItem1->Text = L"Menu";
				// 
				// bt_save_
				// 
				this->bt_save_->AccessibleDescription = L"Save";
				this->bt_save_->AccessibleName = L"Save";
				this->bt_save_->Name = L"bt_save_";
				this->bt_save_->Text = L"Save";
				this->bt_save_->Click += gcnew System::EventHandler(this, &Looter::bt_save__Click);
				// 
				// radMenuSeparatorItem1
				// 
				this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
				this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
				this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
				this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
				// 
				// bt_load_
				// 
				this->bt_load_->AccessibleDescription = L"Load";
				this->bt_load_->AccessibleName = L"Load";
				this->bt_load_->Name = L"bt_load_";
				this->bt_load_->Text = L"Load";
				this->bt_load_->Click += gcnew System::EventHandler(this, &Looter::bt_load__Click);
				// 
				// radMenuSeparatorItem2
				// 
				this->radMenuSeparatorItem2->AccessibleDescription = L"radMenuSeparatorItem2";
				this->radMenuSeparatorItem2->AccessibleName = L"radMenuSeparatorItem2";
				this->radMenuSeparatorItem2->Name = L"radMenuSeparatorItem2";
				this->radMenuSeparatorItem2->Text = L"radMenuSeparatorItem2";
				// 
				// radMenuItem2
				// 
				this->radMenuItem2->AccessibleDescription = L"radMenuItem2";
				this->radMenuItem2->AccessibleName = L"radMenuItem2";
				this->radMenuItem2->Name = L"radMenuItem2";
				this->radMenuItem2->Text = L"Auto Assign Loot Containers.";
				this->radMenuItem2->Click += gcnew System::EventHandler(this, &Looter::radMenuItem2_Click);
				// 
				// bt_advanced
				// 
				this->bt_advanced->AccessibleDescription = L"Advanced";
				this->bt_advanced->AccessibleName = L"Advanced";
				this->bt_advanced->Name = L"bt_advanced";
				this->bt_advanced->Text = L"Advanced";
				this->bt_advanced->Click += gcnew System::EventHandler(this, &Looter::bt_advanced_Click);
				// 
				// radPanel2
				// 
				this->radPanel2->Controls->Add(this->radPageView1);
				this->radPanel2->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel2->Location = System::Drawing::Point(0, 0);
				this->radPanel2->Name = L"radPanel2";
				this->radPanel2->Size = System::Drawing::Size(602, 473);
				this->radPanel2->TabIndex = 37;
				this->radPanel2->Text = L"radPanel2";
				// 
				// radPanel3
				// 
				this->radPanel3->Controls->Add(this->radPanel5);
				this->radPanel3->Controls->Add(this->radPanel1);
				this->radPanel3->Dock = System::Windows::Forms::DockStyle::Top;
				this->radPanel3->Location = System::Drawing::Point(0, 0);
				this->radPanel3->Name = L"radPanel3";
				this->radPanel3->Size = System::Drawing::Size(602, 22);
				this->radPanel3->TabIndex = 38;
				this->radPanel3->Text = L"radPanel3";
				// 
				// radPanel5
				// 
				this->radPanel5->Controls->Add(this->radMenu1);
				this->radPanel5->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel5->Location = System::Drawing::Point(100, 0);
				this->radPanel5->Name = L"radPanel5";
				this->radPanel5->Size = System::Drawing::Size(502, 22);
				this->radPanel5->TabIndex = 37;
				this->radPanel5->Text = L"radPanel5";
				// 
				// radPanel4
				// 
				this->radPanel4->Controls->Add(this->radPanel2);
				this->radPanel4->Dock = System::Windows::Forms::DockStyle::Fill;
				this->radPanel4->Location = System::Drawing::Point(0, 22);
				this->radPanel4->Name = L"radPanel4";
				this->radPanel4->Size = System::Drawing::Size(602, 473);
				this->radPanel4->TabIndex = 39;
				this->radPanel4->Text = L"radPanel4";
				// 
				// containerImageList
				// 
				this->containerImageList->ColorDepth = System::Windows::Forms::ColorDepth::Depth8Bit;
				this->containerImageList->ImageSize = System::Drawing::Size(16, 16);
				this->containerImageList->TransparentColor = System::Drawing::Color::Transparent;
				// 
				// object_5d488b9a_0f42_479b_991e_49bf28826923
				// 
				this->object_5d488b9a_0f42_479b_991e_49bf28826923->Name = L"object_5d488b9a_0f42_479b_991e_49bf28826923";
				this->object_5d488b9a_0f42_479b_991e_49bf28826923->StretchHorizontally = true;
				this->object_5d488b9a_0f42_479b_991e_49bf28826923->StretchVertically = true;
				// 
				// radLabel3
				// 
				this->radLabel3->Location = System::Drawing::Point(5, 44);
				this->radLabel3->Name = L"radLabel3";
				this->radLabel3->Size = System::Drawing::Size(65, 18);
				this->radLabel3->TabIndex = 2;
				// 
				// radLabel7
				// 
				this->radLabel7->Location = System::Drawing::Point(7, 69);
				this->radLabel7->Name = L"radLabel7";
				this->radLabel7->Size = System::Drawing::Size(55, 18);
				this->radLabel7->TabIndex = 4;
				// 
				// radButton3
				// 
				this->radButton3->Location = System::Drawing::Point(172, 159);
				this->radButton3->Name = L"radButton3";
				this->radButton3->Size = System::Drawing::Size(110, 24);
				this->radButton3->TabIndex = 6;
				this->radButton3->Text = L"radButton3";
				// 
				// radLabel10
				// 
				this->radLabel10->Location = System::Drawing::Point(5, 18);
				this->radLabel10->Name = L"radLabel10";
				this->radLabel10->Size = System::Drawing::Size(41, 18);
				this->radLabel10->TabIndex = 4;
				// 
				// Looter
				// 
				this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				this->ClientSize = System::Drawing::Size(602, 495);
				this->Controls->Add(this->radPanel4);
				this->Controls->Add(this->radPanel3);
				this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				this->MaximizeBox = false;
				this->Name = L"Looter";
				// 
				// 
				// 
				this->RootElement->ApplyShapeToControl = true;
				this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
				this->Text = L"Looter";
				this->ThemeName = L"ControlDefault";
				this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &Looter::Looter_FormClosing);
				this->FormClosed += gcnew System::Windows::Forms::FormClosedEventHandler(this, &Looter::Looter_FormClosed);
				this->Load += gcnew System::EventHandler(this, &Looter::Looter_Load);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel6))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				this->radGroupBox1->ResumeLayout(false);
				this->radGroupBox1->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootGroup))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_minqty))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootImportant))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_nameitem))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->PageBps))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootSequence))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootFilterMode))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
				this->radGroupBox2->ResumeLayout(false);
				this->radGroupBox2->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->CheckBoxRecoorder))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->CheckBoxRecoordOnlyHunter))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_max_distance))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel11))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_time_to_clean))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootType))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_min_cap))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPageView1))->EndInit();
				this->radPageView1->ResumeLayout(false);
				this->radPageViewPage1->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->panel_adv))->EndInit();
				this->panel_adv->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox6))->EndInit();
				this->radGroupBox6->ResumeLayout(false);
				this->radGroupBox6->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel14))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootAction))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete_condition))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_add_condition))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox5))->EndInit();
				this->radGroupBox5->ResumeLayout(false);
				this->radGroupBox5->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel12))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel13))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->dropDownLootConditionADV))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_valueADV))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListCondition))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->itemPictureBox))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton1))->EndInit();
				this->radPageViewPage2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox3))->EndInit();
				this->radGroupBox3->ResumeLayout(false);
				this->radGroupBox3->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_min_cap))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_force_move))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_auto_reopen_containers))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_open_next_container))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_eat_food_from_corpses))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel8))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel9))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinEditorMaxEatFoodFromCorpses))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radSpinEditorMinEatFoodFromCorpses))->EndInit();
				this->page_take_skin->ResumeLayout(false);
				this->page_take_skin->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListPriorityLooter))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox9))->EndInit();
				this->radGroupBox9->ResumeLayout(false);
				this->radGroupBox9->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_rope))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_shield))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_boots))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_ring))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_leg))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_weapon))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_bag))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_helmet))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_refresh_slots))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_amulet))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->slot_armor))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox3))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox8))->EndInit();
				this->radGroupBox8->ResumeLayout(false);
				this->radGroupBox8->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_refresh_backpack))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot2))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot3))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot4))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot5))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot7))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot8))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bp_slot6))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox4))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox7))->EndInit();
				this->radGroupBox7->ResumeLayout(false);
				this->radGroupBox7->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_refresh_tile))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_zero_y_mais))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_mais_y_mais))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_mais_y_zero))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_menos_y_mais))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_menos_y_zero))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_menos_y_menos))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_zero_y_menos))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->x_mais_y_menos))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->list_take_skin))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->check_take_skin))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_new_take_skin))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox4))->EndInit();
				this->radGroupBox4->ResumeLayout(false);
				this->radGroupBox4->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel17))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel16))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel15))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_condition_take_skin))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_corposes_id))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->tx_item_id_take_skin))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->bt_delete_take_skin))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel1))->EndInit();
				this->radPanel1->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radToggleButtonLooter))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel2))->EndInit();
				this->radPanel2->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel3))->EndInit();
				this->radPanel3->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel5))->EndInit();
				this->radPanel5->ResumeLayout(false);
				this->radPanel5->PerformLayout();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radPanel4))->EndInit();
				this->radPanel4->ResumeLayout(false);
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel7))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radButton3))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel10))->EndInit();
				(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				this->ResumeLayout(false);

			}
#pragma endregion

			bool disable_item_update;
			System::String^ text;
			void addItemOnView(String^ idBp, String^ Item, loot_item_condition_t Condition, loot_item_priority_t Important, int value, int group, int minqty);
			Void waypointPageAddNewPage(std::string PageNameId);
			void loadItemOnView(std::string id);
			void loadAll();
			void update_idiom();
			void update_idiom_listbox();

			System::Void bt_add_Click(System::Object^  sender, System::EventArgs^  e);
			System::Void bt_delete_Click(System::Object^  sender, System::EventArgs^  e);
			System::Void PageBps_NewPageRequested(System::Object^  sender, System::EventArgs^  e);
			System::Void PageBps_PageRemoved(System::Object^  sender, Telerik::WinControls::UI::RadPageViewEventArgs^  e);
			System::Void ItemListViewDoubleClick(System::Object^  sender, System::EventArgs^ e);
			System::Void ItemListViewItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
			System::Void Looter_Load(System::Object^  sender, System::EventArgs^  e);
			Telerik::WinControls::UI::RadDropDownList^ getBpCurrentDropDownList();


			void add_in_setup(std::string id);

			int getCurrentGroup();
			void disableFieldsUpdate(bool state);
			LooterContainerPtr getSelectedContainer();
			LooterItemPtr getSelectedItem();
			LooterItemNonSharedPtr getSelectedItemNonShared();

			System::Void ListView1ItemChanged(System::Object^  sender, System::EventArgs^  e);
			System::Void box_namebpSelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void dropDownLootGroup_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void dropDownLootImportant_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void tx_minqty_ValueChanged(System::Object^  sender, System::EventArgs^  e);
			System::Void dropDownLootSequence_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void dropDownLootFilterMode_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
			System::Void tx_nameitem_TextChanged(System::Object^  sender, System::EventArgs^  e);

			System::Void Page_EditorInitialized(System::Object^ sender, Telerik::WinControls::UI::RadPageViewEditorEventArgs^ e);
			System::Void PageRename_TextChanged(System::Object^  sender, System::EventArgs^ e);

			void updateSelectedData();
			void UpdateGif();
			bool save_script(std::string file_name);
			bool load_script(std::string file_name);

			System::Void check_eat_food_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
			System::Void bt_refill_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
			System::Void check_anti_idle_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
			System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e);

			void close_button();

			System::Void bt_save__Click(System::Object^  sender, System::EventArgs^  e);
			System::Void bt_load__Click(System::Object^  sender, System::EventArgs^  e);
			void creatContextMenu();
			System::Void MenuNew_Click(Object^ sender, EventArgs^ e);
			System::Void MenuAddAll_Click(Object^ sender, EventArgs^ e);

	private: System::Void radMenuItem2_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radSpinEditorMaxEatFood_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radSpinEditorMinEatFood_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radSpinEditorMaxEatFoodFromCorpses_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void radSpinEditorMinEatFoodFromCorpses_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void check_eat_food_from_corpses_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void check_open_next_container_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void check_auto_reopen_containers_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void radSpinEditorMaxEatFoodFromCorpses_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void PageBps_SelectedPageChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void box_item_id_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void check_drop_pot_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void dropDownLootType_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);

			 Telerik::WinControls::UI::RadListView^ get_current_listview();
			 void updateSelectedItemTakeSkin();

	private: System::Void bt_new_take_skin_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void bt_delete_take_skin_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void tx_corposes_id_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e);
	private: System::Void take_skin_list_ItemChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void tx_corposes_id_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void box_condition_take_skin_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	private: System::Void radCheckBox1_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);
	private: System::Void tx_item_id_take_skin_TextChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void list_take_skin_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
	private: System::Void spin_time_to_clean_ValueChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void spin_max_distance_ValueChanged(System::Object^  sender, System::EventArgs^  e);

			 bool panel_state = false;

	private: System::Void bt_advanced_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void ListAlerts_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e);
	private: System::Void ListAlerts_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e);
	private: System::Void bt_delete_condition_Click(System::Object^  sender, System::EventArgs^  e);
	private: System::Void bt_add_condition_Click(System::Object^  sender, System::EventArgs^  e);

			 void updateSelectedItemCondition();
			 void addItemOnConditionList(loot_item_action_t action, loot_item_new_condition_t condition, int value);

	private: System::Void dropDownLootConditionADV_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	private: System::Void tx_valueADV_ValueChanged(System::Object^  sender, System::EventArgs^  e);

			 void disable_condition();
			 void updateSelectedItemChangeMain();

	private: System::Void dropDownLootAction_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e);
	private: System::Void radToggleButtonLooter_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args);

			 void check_remove_duplicates(bool remove_from_interface){

				 auto duplicateds = LooterManager::get()->get_duplicated_items();

				 if (duplicateds.size()){
					 Telerik::WinControls::RadMessageBox::Show(this, "Duplicated Items in same or others containers will be removed, just one item per container.", "Warning!");
				 }

				 for (auto it : duplicateds){

					 std::shared_ptr<LooterContainer>& container = LooterManager::get()->containers[it.first];

					 for (auto item : it.second){
						 container->delete_idItem(item.first);
					 }
					 if (!remove_from_interface)
						 continue;

					 array<Control^>^ getRadListView = this->PageBps->SelectedPage->Controls->Find("list_" + gcnew String(&it.first[0]), true);
					 if (getRadListView->Length < 0)
						 continue;

					 Telerik::WinControls::UI::RadListView^ radListView = (Telerik::WinControls::UI::RadListView^) getRadListView[0];
					 for (auto it_item : it.second){
						 Telerik::WinControls::UI::ListViewDataItem^ found_item = nullptr;
						 for each(auto lv_item in radListView->Items){

							 if ((String^)lv_item->Tag == gcnew String(&it_item.first[0])){
								 found_item = lv_item;
								 break;
							 }
						 }
						 if (found_item)
							 radListView->Items->Remove(found_item);
					 }
				 }
			 }

	private: System::Void Looter_FormClosed(System::Object^  sender, System::Windows::Forms::FormClosedEventArgs^  e) {
				 check_remove_duplicates(false);
	}
	private: System::Void Looter_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (GeneralManager::get()->get_practice_mode()){
					 this->Hide();
					 e->Cancel = true;
					 return;
				 }
	}
	private: System::Void bt_refresh_tile_Click(System::Object^  sender, System::EventArgs^  e) {
				 Coordinate coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
				 auto x_mais_y_menos = gMapTilesPtr->get_tile_at(Coordinate{ coord.x + 1, coord.y - 1, coord.z });
				 if (!x_mais_y_menos)
					 return;

				 auto x_zero_y_menos = gMapTilesPtr->get_tile_at(Coordinate{ coord.x, coord.y - 1, coord.z });
				 if (!x_zero_y_menos)
					 return;

				 auto x_menos_y_menos = gMapTilesPtr->get_tile_at(Coordinate{ coord.x - 1, coord.y - 1, coord.z });
				 if (!x_menos_y_menos)
					 return;

				 auto x_mais_y_zero = gMapTilesPtr->get_tile_at(Coordinate{ coord.x + 1, coord.y, coord.z });
				 if (!x_mais_y_zero)
					 return;

				 auto x_menos_y_zero = gMapTilesPtr->get_tile_at(Coordinate{ coord.x - 1, coord.y, coord.z });
				 if (!x_menos_y_zero)
					 return;

				 auto x_mais_y_mais = gMapTilesPtr->get_tile_at(Coordinate{ coord.x + 1, coord.y + 1, coord.z });
				 if (!x_mais_y_mais)
					 return;

				 auto x_menos_y_mais = gMapTilesPtr->get_tile_at(Coordinate{ coord.x - 1, coord.y + 1, coord.z });
				 if (!x_menos_y_mais)
					 return;

				 auto x_zero_y_mais = gMapTilesPtr->get_tile_at(Coordinate{ coord.x, coord.y + 1, coord.z });
				 if (!x_zero_y_mais)
					 return;

				 this->x_menos_y_mais->Text = Convert::ToString(x_menos_y_mais->get_top_item_id() != UINT32_MAX ? x_menos_y_mais->get_top_item_id() : 0);
				 this->x_zero_y_mais->Text = Convert::ToString(x_zero_y_mais->get_top_item_id() != UINT32_MAX ? x_zero_y_mais->get_top_item_id() : 0);
				 this->x_mais_y_mais->Text = Convert::ToString(x_mais_y_mais->get_top_item_id() != UINT32_MAX ? x_mais_y_mais->get_top_item_id() : 0);
				 this->x_menos_y_zero->Text = Convert::ToString(x_menos_y_zero->get_top_item_id() != UINT32_MAX ? x_menos_y_zero->get_top_item_id() : 0);
				 this->x_mais_y_zero->Text = Convert::ToString(x_mais_y_zero->get_top_item_id() != UINT32_MAX ? x_mais_y_zero->get_top_item_id() : 0);
				 this->x_menos_y_menos->Text = Convert::ToString(x_menos_y_menos->get_top_item_id() != UINT32_MAX ? x_menos_y_menos->get_top_item_id() : 0);
				 this->x_zero_y_menos->Text = Convert::ToString(x_zero_y_menos->get_top_item_id() != UINT32_MAX ? x_zero_y_menos->get_top_item_id() : 0);
				 this->x_mais_y_menos->Text = Convert::ToString(x_mais_y_menos->get_top_item_id() != UINT32_MAX ? x_mais_y_menos->get_top_item_id() : 0);
	}
	private: System::Void bt_refresh_slots_Click(System::Object^  sender, System::EventArgs^  e) {
				 slot_weapon->Text = Convert::ToString(Inventory::get()->body_weapon());
				 slot_amulet->Text = Convert::ToString(Inventory::get()->body_amulet());
				 slot_armor->Text = Convert::ToString(Inventory::get()->body_armor());
				 slot_bag->Text = Convert::ToString(Inventory::get()->body_bag());
				 slot_helmet->Text = Convert::ToString(Inventory::get()->body_helmet());
				 slot_boots->Text = Convert::ToString(Inventory::get()->body_boot());
				 slot_leg->Text = Convert::ToString(Inventory::get()->body_leg());
				 slot_ring->Text = Convert::ToString(Inventory::get()->body_ring());
				 slot_rope->Text = Convert::ToString(Inventory::get()->body_rope());
				 slot_shield->Text = Convert::ToString(Inventory::get()->body_shield());
	}
	private: System::Void bt_refresh_backpack_Click(System::Object^  sender, System::EventArgs^  e) {
				 ItemContainerPtr container = ContainerManager::get()->get_container_by_index(0);

				 if (!container)
					 return;

				 bp_slot1->Text = Convert::ToString(container->get_slot_id(0) != UINT32_MAX ? container->get_slot_id(0) : 0);
				 bp_slot2->Text = Convert::ToString(container->get_slot_id(1) != UINT32_MAX ? container->get_slot_id(1) : 0);
				 bp_slot3->Text = Convert::ToString(container->get_slot_id(2) != UINT32_MAX ? container->get_slot_id(2) : 0);
				 bp_slot4->Text = Convert::ToString(container->get_slot_id(3) != UINT32_MAX ? container->get_slot_id(3) : 0);
				 bp_slot5->Text = Convert::ToString(container->get_slot_id(4) != UINT32_MAX ? container->get_slot_id(4) : 0);
				 bp_slot6->Text = Convert::ToString(container->get_slot_id(5) != UINT32_MAX ? container->get_slot_id(5) : 0);
				 bp_slot7->Text = Convert::ToString(container->get_slot_id(6) != UINT32_MAX ? container->get_slot_id(6) : 0);
				 bp_slot8->Text = Convert::ToString(container->get_slot_id(7) != UINT32_MAX ? container->get_slot_id(7) : 0);
	}
	private: System::Void spin_min_cap_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_item_update)
					 return;

				 LooterManager::get()->set_min_cap((int)spin_min_cap->Value);
	}
	private: System::Void check_force_move_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 LooterManager::get()->set_force_move(check_force_move->Checked);
	}
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
				 NeutralBot::HudTest^ mluaEditor = gcnew NeutralBot::HudTest();
				 mluaEditor->Show();
	}
	private: System::Void check_min_cap_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
				 if (disable_item_update)
					 return;

				 LooterManager::get()->set_min_cap_to_loot(check_min_cap->Checked);
	}
	private: System::Void DropDownListPriorityLooter_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_item_update)
					 return;
				 
				 bool temp = false;
				 if (DropDownListPriorityLooter->Text->ToLower() == "true")
					 temp = true;

				 TakeSkinManager::get()->set_priority_looter(temp);
	}
private: System::Void CheckBoxRecoordOnlyHunter_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
			 if (disable_item_update)
				 return;

			 LooterManager::get()->set_recoord_only_when_hunter_on(CheckBoxRecoordOnlyHunter->Checked);
}
private: System::Void CheckBoxRecoorder_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
			 if (disable_item_update)
				 return;

			 LooterManager::get()->set_recoorder(CheckBoxRecoorder->Checked);
}
};
}










