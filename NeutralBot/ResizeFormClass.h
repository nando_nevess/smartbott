#include "GeneralManager.h"


namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	using namespace System::Threading;


	public ref class ModeInterfaceWindows : public Telerik::WinControls::UI::RadForm{
	public:
		System::Drawing::Size size_resized;
		System::Drawing::Size size_default;

		ModeInterfaceWindows();

		private: System::Void ThisShown(System::Object^  sender, System::EventArgs^  e);

	private: System::Void Form_Activated(System::Object^  sender, System::EventArgs^  e);
	private: System::Void Form_Deactivate(System::Object^  sender, System::EventArgs^  e);


		private: void update_mode_sizes();
	};

	public ref class PanelFormWindowsManager : public Telerik::WinControls::UI::RadForm {
		PanelFormWindowsManager(){

		}
		System::Collections::Generic::Dictionary<System::Int32, ModeInterfaceWindows^> forms;

		void add_form(uint32_t form_id, ModeInterfaceWindows^ form_ref){

		}

		void on_form_focus_out(){

		}

		void show_form(){

		}

		void check_hide(){

		}
		
		void check_show(){

		}

		void show(){

		}

		void hide(){

		}
	};

}
