#pragma once
#include "PlayerInventory.h"
#include <mutex>
#include "Core\Inventory.h"

Json::Value NaviPlayerInventory::parse_class_to_json(){
	return Inventory::get()->parse_class_to_json();
}
void NaviPlayerInventory::parse_json_to_class(Json::Value jsonValue){
	((std::mutex*)mtx_access)->lock();
	body_helmet = jsonValue["body_helmet"].asInt();
	body_amulet = jsonValue["body_amulet"].asInt();
	body_bag = jsonValue["body_bag"].asInt();
	body_armor = jsonValue["body_armor"].asInt();
	body_shield = jsonValue["body_shield"].asInt();
	body_weapon = jsonValue["body_weapon"].asInt();
	body_leg = jsonValue["body_leg"].asInt();
	body_boot = jsonValue["body_boot"].asInt();
	body_ring = jsonValue["body_ring"].asInt();
	body_rope = jsonValue["body_rope"].asInt();
	weapon_count = jsonValue["weapon_count"].asInt();
	ammunation_count = jsonValue["ammunation_count"].asInt();
	maximize = jsonValue["maximize"].asBool();
	((std::mutex*)mtx_access)->unlock();
}


NaviPlayerInventory::NaviPlayerInventory(){
	mtx_access = (uint32_t*)new std::mutex();
}