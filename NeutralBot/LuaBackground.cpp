#include "LuaBackground.h"
#include "LuaThread.h"

using namespace NeutralBot;

String^ LuaBackground::ShowUnique(String^ code){
	System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
	if (unique == nullptr)
		unique = gcnew LuaBackground(code);
	System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
	unique->BringToFront();
	unique->Show();
	return unique->fastColoredTextBox1->Text;
}

void LuaBackground::loadDirectoriesAndFiles(DirectoryInfo^ dir, Telerik::WinControls::UI::RadTreeNode^ parentNode){
	for each(FileInfo^ f in dir->GetFiles()){
		Telerik::WinControls::UI::RadTreeNode^ fileNode = gcnew Telerik::WinControls::UI::RadTreeNode(f->Name);
		fileNode->ImageKey = "lua_file";
		parentNode->Nodes->Add(fileNode);
	}

	for each(DirectoryInfo^ d in dir->GetDirectories()){
		Telerik::WinControls::UI::RadTreeNode^ dirNode = gcnew Telerik::WinControls::UI::RadTreeNode(d->Name);
		dirNode->ImageKey = "folder";
		dirNode->Tag = "folder";
		parentNode->Nodes->Add(dirNode);
		loadDirectoriesAndFiles(d, dirNode);
	}
}

void LuaBackground::creatContextMenu(){
	ItemNew = gcnew Telerik::WinControls::UI::RadMenuItem("New");
	ItemDelete = gcnew Telerik::WinControls::UI::RadMenuItem("Delete");
	ItemExecute = gcnew Telerik::WinControls::UI::RadMenuItem("Execute");

	ItemNew->Click += gcnew System::EventHandler(this, &LuaBackground::MenuNew_Click);
	ItemDelete->Click += gcnew System::EventHandler(this, &LuaBackground::MenuDelete_Click);
	ItemExecute->Click += gcnew System::EventHandler(this, &LuaBackground::MenuExecute_Click);


}

void LuaBackground::addEvents(){
	radTreeView1->SelectedNodeChanged += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewEventHandler(this, &LuaBackground::radTreeView1_SelectedNodeChanged);

	infoCodeThree->SelectedNodeChanged += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewEventHandler(this, &LuaBackground::infoCodeThree_SelectedNodesChanged);
	radTreeView2->SelectedNodeChanged += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewEventHandler(this, &LuaBackground::radTreeView2_SelectedNodeChanged);
	radTreeView2->NodeRemoving += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewCancelEventHandler(this, &LuaBackground::radTreeView2_NodeRemoving_1);
	radTreeView2->NodeMouseDoubleClick += gcnew Telerik::WinControls::UI::RadTreeView::TreeViewEventHandler(this, &LuaBackground::radTreeView2_NodeMouseDoubleClick_1);

	radThreeViewHuds->SelectedNodeChanged += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewEventHandler(this, &LuaBackground::radTreeView2_SelectedNodeChanged);
	radThreeViewHuds->NodeRemoving += gcnew Telerik::WinControls::UI::RadTreeView::RadTreeViewCancelEventHandler(this, &LuaBackground::radTreeView2_NodeRemoving_1);
	radThreeViewHuds->NodeMouseDoubleClick += gcnew Telerik::WinControls::UI::RadTreeView::TreeViewEventHandler(this, &LuaBackground::radTreeView2_NodeMouseDoubleClick_1);
}

void LuaBackground::loadFilesLua(){

	String^ dirFiles = Directory::GetCurrentDirectory() + "\\lua\\scripts";

	if (!Directory::Exists(dirFiles))
		_mkdir(&managed_util::fromSS(dirFiles)[0]);

	DirectoryInfo^ directory = gcnew DirectoryInfo(dirFiles);
	Telerik::WinControls::UI::RadTreeNode^ Node = gcnew Telerik::WinControls::UI::RadTreeNode(directory->Name);

	Node->Expanded = true;
	Node->ImageKey = "folder";
	loadDirectoriesAndFiles(directory, Node);
	radTreeView1->Nodes->Add(Node);

}

void LuaBackground::ReloadLuaFiles(){
	radTreeView1->BeginUpdate();
	radTreeView1->Nodes->Clear();

	loadFilesLua();
	radTreeView1->EndUpdate();
}

void LuaBackground::loadCodeinFiles(Telerik::WinControls::UI::RadTreeViewEventArgs^  e){
	Telerik::WinControls::UI::RadTreeNode^ father = e->Node->Parent;
	String^ dir = "\\";

	while (father){
		dir = "\\" + father->Text + dir;
		father = father->Parent;

		String^ finaldir = Directory::GetCurrentDirectory() + dir;

		if (!Directory::Exists(finaldir))
			continue;

		String ^path = finaldir + e->Node->Text;

		if (!File::Exists(path))
			continue;

		StreamReader^ streamReader = gcnew StreamReader(path);
		fastColoredTextBox1->Text = streamReader->ReadToEnd();
		streamReader->Close();
	}
}

void LuaBackground::loadItemOnView(){
	radThreeViewHuds->BeginUpdate();
	radTreeView2->BeginUpdate();
	std::map<std::string, std::shared_ptr<LuaBackgroundCodes>> mapNodes = LuaBackgroundManager::get()->getMap();
	for (auto it = mapNodes.begin(); it != mapNodes.end(); it++){
		Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode(gcnew String(it->first.c_str()));
		Telerik::WinControls::UI::RadTreeNode^ Delay = gcnew Telerik::WinControls::UI::RadTreeNode(Convert::ToString(it->second->get_delay()));
		Telerik::WinControls::UI::RadTreeNode^ SubScript = gcnew Telerik::WinControls::UI::RadTreeNode(gcnew String("InitCode"));

		NewNode->ImageKey = "code_edit_1";
		NewNode->CheckType = Telerik::WinControls::UI::CheckType::CheckBox;
		NewNode->Text = gcnew String(it->second->get_name().c_str());
		SubScript->Name = "InitCode";
		Delay->Name = "delay";
		Delay->ImageKey = "timer";
		NewNode->Nodes->Add(Delay);
		NewNode->Nodes->Add(SubScript);

		if (it->second->is_hud())
			radThreeViewHuds->Nodes[radThreeViewHuds->TopNode->Text]->Nodes->AddRange(NewNode);
		else
			radTreeView2->Nodes[radTreeView2->TopNode->Text]->Nodes->AddRange(NewNode);

		if (it->second->get_active())
			NewNode->CheckState = Telerik::WinControls::Enumerations::ToggleState::On;
	}
	radThreeViewHuds->EndUpdate();
	radTreeView2->EndUpdate();
}

void LuaBackground::addInAutoComplete(){
	auto items = gcnew List<FastColoredTextBoxNS::AutocompleteItem^>;
	FastColoredTextBoxNS::AutocompleteMenu^ popupMenu = gcnew FastColoredTextBoxNS::AutocompleteMenu(fastColoredTextBox1);
	popupMenu->MinFragmentLength = 2;
	popupMenu->AllowTabKey = true;

	std::vector<std::string> sources;

	auto& class_vector = KeywordManager::get()->class_vector;
	auto& function_vector = KeywordManager::get()->function_vector;

	for (auto lib_it = class_vector.begin(); lib_it != class_vector.end(); lib_it++){
		auto& class_infos = lib_it->second;
		for (auto class_info = class_infos.begin(); class_info != class_infos.end(); class_info++){
			sources.push_back((*class_info)->name);
			for (auto method = (*class_info)->methods.begin(); method != (*class_info)->methods.end(); method++){
				sources.push_back((*class_info)->name + "." + (*method)->method_string);
				for (auto suggestion = (*method)->suggestions.begin(); suggestion != (*method)->suggestions.end(); suggestion++){
					sources.push_back((*class_info)->name + "." + (*suggestion));

				}
			}
		}
	}

	for (auto functions = function_vector.begin(); functions != function_vector.end(); functions++){
		for (auto function = functions->second.begin(); function != functions->second.end(); function++){
			sources.push_back((*function)->method_string.c_str());
			for (auto suggestion = (*function)->suggestions.begin(); suggestion != (*function)->suggestions.end(); suggestion++){
				sources.push_back((*suggestion).c_str());
			}
		}
	}

	array<String^>^ sourcess = managed_util::convert_vector_to_array(sources);

	for each(auto item in sourcess)
		items->Add(gcnew MethodAutocompleteItem2(item));


	popupMenu->Items->SetAutocompleteItems(items);
	popupMenu->Items->MaximumSize = System::Drawing::Size(500, 300);
	popupMenu->Items->Width = 500;

}

System::Void LuaBackground::LuaBackground_Load(System::Object^  sender, System::EventArgs^  e) {
	loadItemOnView();
	loadFilesLua(); 
	creatContextMenu();
	update_idiom();
	addEvents();
	addInAutoComplete();
	loadStyle();
}

bool LuaBackground::is_hud_tab_selected(){
	return radPageView1->SelectedPage->TabIndex == 1;
}

Telerik::WinControls::UI::RadTreeView^ LuaBackground::get_current_three_view(){
	if (is_hud_tab_selected())
		return radThreeViewHuds;

	return radTreeView2;
}

System::Void LuaBackground::MenuNew_Click(Object^ sender, EventArgs^ e){
	String^ NodeName = gcnew String(LuaBackgroundManager::get()->request_new_id(is_hud_tab_selected()).c_str());

	Telerik::WinControls::UI::RadTreeNode^ NewNode = gcnew Telerik::WinControls::UI::RadTreeNode(NodeName);
	Telerik::WinControls::UI::RadTreeNode^ Delay = gcnew Telerik::WinControls::UI::RadTreeNode("1000");
	Telerik::WinControls::UI::RadTreeNode^ InitCode = gcnew Telerik::WinControls::UI::RadTreeNode(gcnew String("InitCode"));
	
	get_current_three_view()->Nodes[get_current_three_view()->TopNode->Text]->Nodes->AddRange(NewNode);
	NewNode->ImageKey = "code_edit_1";
	NewNode->Name = NodeName;
	NewNode->CheckType = Telerik::WinControls::UI::CheckType::CheckBox;
	NewNode->Nodes->Add(Delay);

	if (get_current_three_view()->TopNode->Text == "Huds")
		NewNode->Nodes->Add(InitCode);

	get_current_three_view()->SelectedNode = NewNode;

	Delay->Name = "delay";
	Delay->ImageKey = "timer";
	InitCode->Name = "InitCode";
	InitCode->ImageKey = "code_edit_1";
}
System::Void LuaBackground::MenuDelete_Click(Object^ sender, EventArgs^ e){
	if (get_current_three_view()->SelectedNode == nullptr)
		return;

	if (get_current_three_view()->SelectedNode->Text == get_current_three_view()->TopNode->Text)
		return;

	if (get_current_three_view()->SelectedNode->Parent->Text != get_current_three_view()->TopNode->Text)
		return;

	get_current_three_view()->SelectedNode->Remove();
}
System::Void LuaBackground::MenuExecute_Click(Object^ sender, EventArgs^ e){
	if (get_current_three_view()->SelectedNode == nullptr)
		return;

	if (get_current_three_view()->SelectedNode->Text == get_current_three_view()->TopNode->Text)
		return;

	if (get_current_three_view()->SelectedNode->Parent->Text != get_current_three_view()->TopNode->Text)
		return;

	LuaThread::get()->simple_execute(LuaBackgroundManager::get()->getLuaCode(managed_util::fromSS(get_current_three_view()->SelectedNode->Name)), managed_util::fromSS(get_current_three_view()->SelectedNode->Name));
}

System::Void LuaBackground::fastColoredTextBox1_TextChanged(System::Object^  sender, FastColoredTextBoxNS::TextChangedEventArgs^  e) {

	if (get_current_three_view()->SelectedNode == nullptr)
		return;

	if (get_current_three_view()->SelectedNode->Parent == nullptr)
		return;

	if (get_current_three_view()->SelectedNode->Parent->Text != get_current_three_view()->TopNode->Text){
		
		if (get_current_three_view()->SelectedNode->Name == "InitCode"){
			std::string id = managed_util::fromSS(get_current_three_view()->SelectedNode->Parent->Name);
			std::string code = managed_util::fromSS(fastColoredTextBox1->Text);

			LuaBackgroundManager::get()->updateInitCode(id, code);
		}
		else if (get_current_three_view()->SelectedNode->Name == "code"){
			std::string id = managed_util::fromSS(get_current_three_view()->SelectedNode->Parent->Name);
			std::string code = managed_util::fromSS(fastColoredTextBox1->Text);

			LuaBackgroundManager::get()->updateCode(id, code);
		}



	}
	else if (get_current_three_view()->SelectedNode->Parent->Text == get_current_three_view()->TopNode->Text){
		Telerik::WinControls::UI::RadTreeNodeCollection^ nodes = get_current_three_view()->SelectedNode->Nodes;

		for each(auto items in nodes){
			std::string id = managed_util::fromSS(get_current_three_view()->SelectedNode->Name);
			std::string code = managed_util::fromSS(fastColoredTextBox1->Text);

			LuaBackgroundManager::get()->updateCode(id, code);
		}
	}
}

System::Void LuaBackground::radTreeView2_ValueChanging(System::Object^  sender, Telerik::WinControls::UI::TreeNodeValueChangingEventArgs^  e) {
	if (!e->Node)
		return;

	if (e->Node->Name == "InitCode"){
		e->Cancel = true;
		return;
	}

	if (e->Node->Name == "delay"){
		try{
			Convert::ToInt32(e->NewValue);

			std::string id = managed_util::fromSS(e->Node->Parent->Name);
			uint32_t delay = Convert::ToInt32(e->NewValue);

			LuaBackgroundManager::get()->updateDelay(id, delay);
			return;
		}
		catch (...){
			e->Cancel = true;
			return;
		}
	}

	for each(auto items in e->Node->Parent->Nodes){
		if (e->NewValue->ToString() == items->Text){
			e->Cancel = true;
			return;
		}
	}

	std::string newName = managed_util::to_std_string(e->NewValue->ToString());
	std::string oldName = managed_util::to_std_string(e->Node->Name);

	if (!LuaBackgroundManager::get()->changeName(oldName, newName))
		e->Cancel = true;
}

System::Void LuaBackground::radTreeView2_Editing(System::Object^  sender, Telerik::WinControls::UI::TreeNodeEditingEventArgs^  e) {
	if (e->Node->Text == get_current_three_view()->TopNode->Text)
		e->Cancel = true;
}

System::Void LuaBackground::radTreeView2_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
	if (radTreeView1->SelectedNode)
		radTreeView1->SelectedNode = nullptr;

	if (e->Node == nullptr)
		return;

	if (e->Node == get_current_three_view()->TopNode){
		if (fastColoredTextBox1->Enabled){
			fastColoredTextBox1->Enabled = false;
			fastColoredTextBox1->Text = "";
		}
		return;
	}

	if (!fastColoredTextBox1->Enabled)
		fastColoredTextBox1->Enabled = true;

	if (e->Node->Parent->Text != get_current_three_view()->TopNode->Text){

		if (get_current_three_view()->SelectedNode->Name == "InitCode")
			fastColoredTextBox1->Text = gcnew String(LuaBackgroundManager::get()->getLuaInitCode(managed_util::fromSS(e->Node->Parent->Name)).c_str());		
		else if (get_current_three_view()->SelectedNode->Name == "code")
			fastColoredTextBox1->Text = gcnew String(LuaBackgroundManager::get()->getLuaCode(managed_util::fromSS(e->Node->Parent->Name)).c_str());
		else if (get_current_three_view()->SelectedNode->Name == "delay"){
			fastColoredTextBox1->Text = "";
			fastColoredTextBox1->Enabled = false;
		}
	}

	else if (e->Node->Parent->Text == get_current_three_view()->TopNode->Text){
		for each(auto items in e->Node->Nodes)
			fastColoredTextBox1->Text = gcnew String(LuaBackgroundManager::get()->getLuaCode(managed_util::fromSS(e->Node->Name)).c_str());
	}
}

System::Void LuaBackground::radTreeView2_NodeRemoving_1(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewCancelEventArgs^  e) {
	if (e->Node->Text == get_current_three_view()->TopNode->Text){
		e->Cancel = true;
		return;
	}

	if (e->Node->Parent->Text != get_current_three_view()->TopNode->Text){
		e->Cancel = true;
		return;
	}

	if (Telerik::WinControls::RadMessageBox::Show(this, "Deseja realmente remove o script? ", "Smart Bot", MessageBoxButtons::YesNo, Telerik::WinControls::RadMessageIcon::Question) == System::Windows::Forms::DialogResult::No){
		e->Cancel = true;
		return;
	}

	LuaBackgroundManager::get()->removeLuaCode(managed_util::to_std_string(e->Node->Name));
}

System::Void LuaBackground::radTreeView2_NodeCheckedChanged(System::Object^  sender, Telerik::WinControls::UI::TreeNodeCheckedEventArgs^  e) {
	if (e->Node == nullptr)
		return;

	if (e->Node->Parent == nullptr)
		return;

	LuaBackgroundManager::get()->updateActive(managed_util::fromSS(e->Node->Name), e->Node->Checked);
}

System::Void LuaBackground::radTreeView1_SelectedNodeChanged(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
	if (get_current_three_view()->SelectedNode)
		get_current_three_view()->SelectedNode = nullptr;
}

System::Void LuaBackground::fastColoredTextBox1_KeyUp_1(System::Object^  sender, System::Windows::Forms::KeyEventArgs^  e) {
	if (fastColoredTextBox1->Text == ""){
		return;
	}
	String^ name = get_current_three_view()->SelectedNode ? get_current_three_view()->SelectedNode->Name : "not found";
	String^ text = fastColoredTextBox1->Text;
	if (e->KeyCode == Keys::F5)
		LuaThread::get()->simple_execute(managed_util::fromSS(text), managed_util::fromSS(name));
}

System::Void LuaBackground::time_lua_log_Tick(System::Object^  sender, System::EventArgs^  e) {

	auto new_results = LuaHandler::get()->get_index_greater_than(last_index);
	if (!new_results.size())
		return;

	for (auto result : new_results){
		tx_lua_log->Text += gcnew String(&result.second[0]);
		if (result.first > last_index)
			last_index = result.first;
	}

	if (tx_lua_log->Lines->Length > 100){
		tx_lua_log->Select(0, tx_lua_log->GetFirstCharIndexFromLine(tx_lua_log->Lines->Length - 100));
		tx_lua_log->SelectedText = " ";
	}

	if (!tx_lua_log->SelectedText || !tx_lua_log->SelectedText->Length){
		tx_lua_log->SelectionStart = tx_lua_log->Text->Length;
		tx_lua_log->ScrollToCaret();
	}
}

System::Void LuaBackground::radMenuItem1_Click_1(System::Object^  sender, System::EventArgs^  e) {
	if (get_current_three_view()->SelectedNode != nullptr){
		if (get_current_three_view()->SelectedNode->Text != get_current_three_view()->TopNode->Text){
			if (get_current_three_view()->SelectedNode->Parent->Text == get_current_three_view()->TopNode->Text){
				String^ temp_string = gcnew String(&LuaBackgroundManager::get()->getLuaCode(managed_util::fromSS(get_current_three_view()->SelectedNode->Name))[0]);
				std::string temp_sddstring = managed_util::fromSS(temp_string->Trim());

				if (temp_sddstring != "")
					LuaThread::get()->simple_execute(temp_sddstring, managed_util::fromSS(get_current_three_view()->SelectedNode->Name));

				return;
			}
		}
	}
	String^ name = get_current_three_view()->SelectedNode ? get_current_three_view()->SelectedNode->Name : "not found";
	String^ text = fastColoredTextBox1->Text;
	LuaThread::get()->simple_execute(managed_util::fromSS(text), managed_util::fromSS(name));
}

void LuaBackground::loadStyle(){

	auto& class_vector = KeywordManager::get()->class_vector;
	auto& function_vector = KeywordManager::get()->function_vector;

	for (auto lib_it = class_vector.begin(); lib_it != class_vector.end(); lib_it++){
		auto& class_infos = lib_it->second;
		for (auto class_info = class_infos.begin(); class_info != class_infos.end(); class_info++){
			std::string class_name = (*class_info)->name;
			nameClass += "|" + gcnew String(class_name.c_str());
			for (auto method = (*class_info)->methods.begin(); method != (*class_info)->methods.end(); method++){
				temp_strMet = temp_strMet + "|" + gcnew String(class_name.c_str()) + "[\\.]" + gcnew String(&(*method)->method_string[0]);

				for (auto suggestion = (*method)->suggestions.begin(); suggestion != (*method)->suggestions.end(); suggestion++){
					temp_strMet = temp_strMet + "|" + gcnew String(class_name.c_str()) + "[\\.]" + gcnew String(&(*suggestion)[0]);
				}
			}
		}
	}

	for (auto functions = function_vector.begin(); functions != function_vector.end(); functions++){
		for (auto function = functions->second.begin(); function != functions->second.end(); function++){
			nameClass += "|" + gcnew String((*function)->method_string.c_str());
			for (auto suggestion = (*function)->suggestions.begin(); suggestion != (*function)->suggestions.end(); suggestion++){
				nameClass += "|" + gcnew String((*suggestion).c_str());
			}
		}
	}

	temp_strMet = temp_strMet + ")";
	nameClass = nameClass + ")";
}

void LuaBackground::loadCode(String^ idCode){
	fastColoredTextBox1->Text = idCode;
}

System::Void LuaBackground::fastColoredTextBox1_TextChangedDelayed_1(System::Object^  sender, FastColoredTextBoxNS::TextChangedEventArgs^  e) {
	fastColoredTextBox1->Range->SetStyle(FunctionNameStyle, temp_strMet);
}

System::Void LuaBackground::radTreeView2_NodeMouseDoubleClick_1(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
	if (e->Node->Text == get_current_three_view()->TopNode->Text)
		return;
	e->Node->BeginEdit();
}

int lastAdd = 0;

void LuaBackground::hide_left_pane(){
	splitPanelScripts->Collapsed = true;
}

void LuaBackground::hide_bottom_pane(){
	splitPanelOutput->Collapsed = true;
}

void LuaBackground::hide_right_pane(){
	splitPanelExample->Collapsed = true;
}

void LuaBackground::hide_libs_info(){
	splitPanelLibInfo->Collapsed = true;
}
System::Void LuaBackground::radSplitContainer1_Click(System::Object^  sender, System::EventArgs^  e) {
}
System::Void LuaBackground::radMenuItem1_Click(System::Object^  sender, System::EventArgs^  e) {

}

Telerik::WinControls::UI::RadTreeNode^ LuaBackground::get_node(lua_table_t type, cli::array<String^>^ paths){
	Telerik::WinControls::UI::RadTreeNode^ current_node = infoCodeThree->Nodes[paths[0]];
	if (current_node == nullptr)
		return nullptr;

	if (paths->Length == 1)
		return current_node;

	if (type == lua_table_t::lua_table_any){
		for (uint32_t type_start = lua_table_t::lua_table_class; lua_table_t::lua_table_total; type_start++){
			String^ node_str = getLuaTableNodeAsString((lua_table_t)type_start);
			if (!current_node->Nodes[node_str] || !current_node->Nodes[node_str]->Nodes[paths[1]]){
				continue;
			}
			Telerik::WinControls::UI::RadTreeNode^ temp_node = current_node->Nodes[node_str]->Nodes[paths[1]];
			if (temp_node){
				current_node = temp_node->Parent;
				break;
			}
		}
	}
	else if ((uint32_t)type < lua_table_t::lua_table_total)
		current_node = current_node->Nodes[getLuaTableNodeAsString(type)];
	else if (type != lua_table_t::lua_table_none)
		return nullptr;

	if (current_node == nullptr)
		return nullptr;

	for (int i = 1; i < paths->Length; i++){
		current_node = current_node->Nodes[paths[i]];
		if (current_node == nullptr)
			return nullptr;
	}
	return current_node;
}

String^ LuaBackground::get_lib_description(String^ libname){
	return gcnew String(&KeywordManager::get()->get_lib_description(managed_util::fromSS(libname))[0]);
}

String^ LuaBackground::get_table_description(String^ libname, String^ classname){
	return gcnew String(&KeywordManager::get()->get_table_description(managed_util::fromSS(libname), managed_util::fromSS(classname))[0]);
}

String^ LuaBackground::get_method_description(String^ libname, String^ classname, String^ methodname){
	return gcnew String(&KeywordManager::get()->get_method_description(managed_util::fromSS(libname), managed_util::fromSS(classname),
		managed_util::fromSS(methodname))[0]);
}

String^ LuaBackground::get_table_variable_description(String^ libname, String^ tablename, String^ varname){
	return gcnew String(&KeywordManager::get()->get_table_variable_description(managed_util::fromSS(libname), managed_util::fromSS(tablename),
		managed_util::fromSS(varname))[0]);
}

String^ LuaBackground::get_function_description(String^ libname, String^ functionname){
	return gcnew String(&KeywordManager::get()->get_function_description(managed_util::fromSS(libname), managed_util::fromSS(functionname))[0]);
}

void LuaBackground::update_current_description(){

}

void LuaBackground::set_description(String^ type, String^ description){
	/* if (!type){
	descriptionArea->Text = "";
	}
	else{
	if (description)
	descriptionArea->Text = type + "\n\n" + description;
	else
	descriptionArea->Text = type + "\n\nno description.";
	}*/
}

Telerik::WinControls::UI::RadTreeNode^ LuaBackground::get_class_node(String^ libname, String^ classname, lua_table_t type){
	auto main_node = get_lib_node(libname);

	Telerik::WinControls::UI::RadTreeNode^ current_node = main_node->Nodes[classname];
	if (current_node)
		return current_node;
	return add_class(libname, classname, type);
}

Telerik::WinControls::UI::RadTreeNode^ LuaBackground::get_class_method_node(String^ libname, String^ classname, String^ method){
	auto main_node = get_class_node(libname, classname, lua_table_t::lua_table_any);

	Telerik::WinControls::UI::RadTreeNode^ current_node = main_node->Nodes[method];
	if (current_node)
		return current_node;
	add_class_method(libname, classname, method);
	return main_node->Nodes[method];
}

Telerik::WinControls::UI::RadTreeNode^ LuaBackground::get_function_node(String^ libname, String^ functionname){
	auto main_node = get_lib_node(libname);

	Telerik::WinControls::UI::RadTreeNode^ current_node = main_node->Nodes[functionname];
	if (current_node)
		return current_node;
	add_function(libname, functionname);
	return main_node->Nodes[functionname];
}

Telerik::WinControls::UI::RadTreeNode^ LuaBackground::get_lib_node(String^ libname){
	Telerik::WinControls::UI::RadTreeNode^ current_node = infoCodeThree->Nodes[libname];
	if (current_node)
		return current_node;
	add_lib(libname);
	return infoCodeThree->Nodes[libname];
}

void LuaBackground::add_class_method(String^ libname, String^ classname, String^ method){

	if (!get_node(lua_table_t::lua_table_any, gcnew cli::array < String^ > {libname, classname, method})){
		auto current_node = get_class_node(libname, classname, lua_table_t::lua_table_any);
		current_node->Nodes->Add(method, "method_symbol");
		Telerik::WinControls::UI::RadTreeNode^ res_node = (Telerik::WinControls::UI::RadTreeNode^)current_node->Nodes[method];
		res_node->Name = "class_method";
	}
}

void LuaBackground::add_class_method_suggestion(String^ libname, String^ classname, String^ method, String^ suggestion){
	if (!get_node(lua_table_t::lua_table_any, gcnew cli::array < String^ > {libname, classname, method, suggestion})){
		auto current_node = get_class_method_node(libname, classname, method);
		current_node->Nodes->Add(suggestion, "method_symbol");
		Telerik::WinControls::UI::RadTreeNode^ res_node = (Telerik::WinControls::UI::RadTreeNode^)current_node->Nodes[suggestion];
		res_node->Name = "method_suggestion";
	}
}

String^ LuaBackground::getLuaTableNodeAsString(lua_table_t table_type){
	uint32_t int_type = (uint32_t)table_type;
	String^ type_node = nullptr;
	switch (table_type){
	case lua_table_t::lua_table_class:
	{
										 type_node = "Classes";
	}
		break;
	case lua_table_t::lua_table_enum:
	{
										type_node = "Enums";
	}
		break;
	case lua_table_t::lua_table_metatable:
	{
											 type_node = "Metatables";
	}
		break;
	case lua_table_t::lua_table_table:
	{
										 type_node = "Table";
	}
		break;
	}
	return type_node;
}

Telerik::WinControls::UI::RadTreeNode^ LuaBackground::add_class(String^ libname, String^ classname, lua_table_t lua_table_type){

	Telerik::WinControls::UI::RadTreeNode^ current_node = get_node(lua_table_type, gcnew cli::array < String^ > {libname, classname});
	if (current_node)
		return current_node;

	String^ type_as_str = getLuaTableNodeAsString(lua_table_type);
	current_node = get_lib_node(libname);

	if (!current_node->Nodes[type_as_str]){
		current_node->Nodes->Add(type_as_str, "table_type");
		current_node = current_node->Nodes[type_as_str];
	}
	else
		current_node = current_node->Nodes[type_as_str];
	current_node->Nodes->Add(classname, "class_symbol");

	Telerik::WinControls::UI::RadTreeNode^ res_node = (Telerik::WinControls::UI::RadTreeNode^)current_node->Nodes[classname];
	res_node->Name = "class";

	return current_node->Nodes[classname];
}

void LuaBackground::add_table_variable(String^ libname, String^ table_name, String^ variable_name){

	if (!get_node(lua_table_t::lua_table_any, gcnew cli::array < String^ > {libname, table_name, variable_name})){
		auto current_node = get_class_node(libname, table_name, lua_table_t::lua_table_any);
		current_node->Nodes->Add(variable_name, "method_symbol");
		Telerik::WinControls::UI::RadTreeNode^ res_node = (Telerik::WinControls::UI::RadTreeNode^)current_node->Nodes[variable_name];
		res_node->Name = "table_variable";
	}
}

void LuaBackground::add_function_suggestion(String^ libname, String^ function, String^ suggestion){
	if (!get_node(lua_table_t::lua_table_none, gcnew cli::array < String^ > {libname, function, suggestion})){
		auto current_node = get_function_node(libname, function);
		current_node->Nodes->Add(suggestion, "function_symbol");
		Telerik::WinControls::UI::RadTreeNode^ res_node = (Telerik::WinControls::UI::RadTreeNode^)current_node->Nodes[suggestion];
		res_node->Name = "function_suggestion";
	}
}

void LuaBackground::add_function(String^ libname, String^ function){
	if (!get_node(lua_table_t::lua_table_none, gcnew cli::array < String^ > {libname, function})){
		auto current_node = get_lib_node(libname);
		current_node->Nodes->Add(function, "function_symbol");		
		Telerik::WinControls::UI::RadTreeNode^ res_node = (Telerik::WinControls::UI::RadTreeNode^)current_node->Nodes[function];
		res_node->Name = "function";
	}
}

void LuaBackground::add_lib(String^ libname){
	if (!get_node(lua_table_t::lua_table_none, gcnew cli::array < String^ > {libname})){
		infoCodeThree->Nodes->Add(libname, "lib_symbol");
		Telerik::WinControls::UI::RadTreeNode^ res_node = get_node(lua_table_t::lua_table_none, gcnew cli::array < String^ > {libname});
		res_node->Name = "library";
	}
}

void LuaBackground::loadInformations(){
	auto& class_vector = KeywordManager::get()->class_vector;
	auto& function_vector = KeywordManager::get()->function_vector;

	infoCodeThree->BeginUpdate();

	for (auto lib_it = class_vector.begin(); lib_it != class_vector.end(); lib_it++){
		String^ libname = gcnew String(&lib_it->first[0]);
		add_lib(libname);
		auto& class_infos = lib_it->second;
		for (auto class_info = class_infos.begin(); class_info != class_infos.end(); class_info++){
			std::string class_name = (*class_info)->name;
			add_class(libname, gcnew String(class_name.c_str()), (*class_info)->type);
			for (auto method = (*class_info)->methods.begin(); method != (*class_info)->methods.end(); method++){
				String^ method_ = gcnew String(&(*method)->method_string[0]);
				String^ temp = method_->Trim();
				String^ _method = temp->Replace("_", " ");
				add_class_method(libname, gcnew String(class_name.c_str()), gcnew String(&(*method)->method_string[0]));
				for (auto suggestion = (*method)->suggestions.begin(); suggestion != (*method)->suggestions.end(); suggestion++){
					add_class_method_suggestion(libname, gcnew String(class_name.c_str()), gcnew String(&(*method)->method_string[0]),
						gcnew String(&(*suggestion)[0]));
				}
			}

			for (auto variable : (*class_info)->variables){
				add_table_variable(libname, gcnew String(class_name.c_str()), gcnew String(variable->name.c_str()));

			}
		}
	}

	for (auto functions = function_vector.begin(); functions != function_vector.end(); functions++){
		String^ libname = gcnew String(&functions->first[0]);
		add_lib(libname);
		for (auto function = functions->second.begin(); function != functions->second.end(); function++){
			String^ functionname = gcnew String((*function)->method_string.c_str());
			add_function(libname, functionname);
			for (auto suggestion = (*function)->suggestions.begin(); suggestion != (*function)->suggestions.end(); suggestion++){
				add_function_suggestion(libname, functionname, gcnew String((*suggestion).c_str()));
			}
		}
	}

	infoCodeThree->EndUpdate();
}

System::Void LuaBackground::infoCodeThree_SelectedNodeChanged_1(System::Object^  sender, Telerik::WinControls::UI::RadTreeViewEventArgs^  e) {
	update_current_description();
}

System::Void LuaBackground::infoCodeThree_DoubleClick(System::Object^  sender, System::EventArgs^  e) {
	Telerik::WinControls::UI::RadTreeView^ sender_as_tree_view = (Telerik::WinControls::UI::RadTreeView^)sender;

	if (!sender_as_tree_view->SelectedNode){
		return;
	}
	auto selected = sender_as_tree_view->SelectedNode;

	String^ desc;
	if (selected->Name == "library"){
		desc = get_lib_description(selected->Text);
	}
	else if (selected->Name == "function"){
		desc = get_function_description(selected->Parent->Text, selected->Text);
	}
	else if (selected->Name == "function_suggestion"){
		desc = get_function_description(selected->Parent->Parent->Text, selected->Parent->Text);
	}
	else if (selected->Name == "class"){
		desc = get_table_description(selected->Parent->Text, selected->Text);
	}
	else if (selected->Name == "method_suggestion"){
		desc = get_method_description(selected->Parent->Parent->Text, selected->Parent->Text, selected->Text);
	}
	else if (selected->Name == "variable"){
		desc = get_method_description(selected->Parent->Parent->Text, selected->Parent->Text, selected->Text);
	}
	else if (selected->Name == "table_variable"){
		//	desc = get_table_variable_description(root->Text, selected->Parent->Text, selected->Text);
	}


	set_description((String^)selected->Name, desc);

	LuaBackground^ mLuaEditor = (gcnew LuaBackground((String^)selected->Name + "\n\n" + desc));
	mLuaEditor->hide_bottom_pane();
	mLuaEditor->hide_right_pane();
	mLuaEditor->hide_left_pane();
	mLuaEditor->hide_libs_info();
	mLuaEditor->Show();
}

System::Void LuaBackground::radMenuItem3_Click(System::Object^  sender, System::EventArgs^  e) {
	(gcnew LuaStyleEditor)->Show();
}