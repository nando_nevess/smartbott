#pragma once
#include "ThreadHandler.h"
#include "Core\Time.h"
#include <Thread>
#include <mutex>
#include "DevelopmentManager.h"


ThreadHandler::ThreadHandler(){
	shared_luaCore = std::shared_ptr<LuaCore>(new LuaCore);
	luaCore = shared_luaCore.get();

	thread_ptr = (int)new std::thread( 
		std::bind(
		[&](){
			MONITOR_THREAD(__FUNCTION__)
			if (!is_alive){
				is_alive = true;
				start_loop();
				is_alive = false;
			}
		}
	)
	);
}

bool ThreadHandler::get_is_alive(){
	lock_guard _lock(mtx_access);
	return is_alive;
}
bool ThreadHandler::get_is_used(){
	lock_guard _lock(mtx_access);
	return is_used;
}
std::shared_ptr<LuaBackgroundCodes> ThreadHandler::get_current_script(){
	lock_guard _lock(mtx_access);
	return current_script;
}

void ThreadHandler::execute_script(std::string code, std::string name){
	luaCore->RunScript(code, name);

}

void ThreadHandler::remove_current_script(){
	lock_guard _lock(mtx_access);

	if (!current_script)
		return;

	current_script->reset_timer();
	current_script->set_thread_on(false);
	is_used = false;
	current_script = nullptr;
}

ThreadHandler::~ThreadHandler(){
	delete(luaCore);
}

void ThreadHandler::start_loop(){
	while (true){
		Sleep(10);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;

		while (is_alive){
			Sleep(10);

			if (need_execute()){
				execute_script(current_script->get_code(), current_script->get_name());
				remove_current_script();
			}

		}
	}
}
std::shared_ptr<ThreadHandler> ThreadHandler::getPtr(){
	return shared_from_this();
}

bool ThreadHandler::set_current_script(std::shared_ptr<LuaBackgroundCodes> script){
	lock_guard _lock(mtx_access);
	
	if (!script)
		return false;

	if (!current_script){
		current_script = script;
		current_script->set_thread_on(true);
		is_used = true;
		return true;
	}

	return false;
}

bool ThreadHandler::need_execute(){
	lock_guard _lock(mtx_access);

	if (!current_script)
		return false;

	return true;
}
