#pragma once
#include "ManagedUtil.h"
#include "GifManager.h"
#include "GeneralManager.h"
#include "Core\ItemsManager.h"
#include "Core\LooterCore.h"
#include "LanguageManager.h"

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class ItemsInfo : public Telerik::WinControls::UI::RadForm{
	public: ItemsInfo(void){
				InitializeComponent();
				NeutralBot::FastUIPanelController::get()->install_controller(this);
	}
	protected: ~ItemsInfo(){
				   unique = nullptr;
				   if (components)
					   delete components;
	}
	private: Telerik::WinControls::UI::RadButton^  ButtonResetItemCount;
	protected:
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListVisible;
	private: Telerik::WinControls::UI::RadLabel^  radLabel5;
	private: Telerik::WinControls::UI::RadButton^  ButtonResetAllHud;

	protected: static ItemsInfo^ unique;

	public: static void HideUnique(){
				if (unique)unique->Hide();
	}
	public: static void CloseUnique(){
				if (unique)unique->Close();
	}
	public: static void ReloadForm(){
				unique->ItemsInfo_Load(nullptr, nullptr);
	}
	public: static void ShowUnique(){
		System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew ItemsInfo();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew ItemsInfo();
	}
	private: Telerik::WinControls::UI::RadListView^  ItemListView;
	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_buy_price;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: System::Windows::Forms::TextBox^  tx_item_id;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_sell_price;
	private: Telerik::WinControls::UI::RadDropDownList^  box_item_name;
	protected:
	protected:
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem1 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 Telerik::WinControls::UI::RadListDataItem^  radListDataItem2 = (gcnew Telerik::WinControls::UI::RadListDataItem());
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ItemsInfo::typeid));
				 this->ItemListView = (gcnew Telerik::WinControls::UI::RadListView());
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->ButtonResetItemCount = (gcnew Telerik::WinControls::UI::RadButton());
				 this->DropDownListVisible = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel5 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->spin_buy_price = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->tx_item_id = (gcnew System::Windows::Forms::TextBox());
				 this->spin_sell_price = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->box_item_name = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->ButtonResetAllHud = (gcnew Telerik::WinControls::UI::RadButton());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ItemListView))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonResetItemCount))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListVisible))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_buy_price))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_sell_price))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_item_name))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonResetAllHud))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // ItemListView
				 // 
				 this->ItemListView->AllowColumnReorder = false;
				 this->ItemListView->AllowColumnResize = false;
				 this->ItemListView->AllowEdit = false;
				 this->ItemListView->AllowRemove = false;
				 this->ItemListView->ItemSize = System::Drawing::Size(200, 25);
				 this->ItemListView->Location = System::Drawing::Point(12, 12);
				 this->ItemListView->Name = L"ItemListView";
				 this->ItemListView->ShowGridLines = true;
				 this->ItemListView->Size = System::Drawing::Size(250, 276);
				 this->ItemListView->TabIndex = 0;
				 this->ItemListView->SelectedItemChanged += gcnew System::EventHandler(this, &ItemsInfo::ItemListView_SelectedItemChanged);
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->ButtonResetItemCount);
				 this->radGroupBox1->Controls->Add(this->DropDownListVisible);
				 this->radGroupBox1->Controls->Add(this->radLabel5);
				 this->radGroupBox1->Controls->Add(this->radLabel4);
				 this->radGroupBox1->Controls->Add(this->spin_buy_price);
				 this->radGroupBox1->Controls->Add(this->radLabel3);
				 this->radGroupBox1->Controls->Add(this->radLabel2);
				 this->radGroupBox1->Controls->Add(this->radLabel1);
				 this->radGroupBox1->Controls->Add(this->tx_item_id);
				 this->radGroupBox1->Controls->Add(this->spin_sell_price);
				 this->radGroupBox1->Controls->Add(this->box_item_name);
				 this->radGroupBox1->HeaderText = L"";
				 this->radGroupBox1->Location = System::Drawing::Point(268, 12);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(297, 119);
				 this->radGroupBox1->TabIndex = 2;
				 // 
				 // ButtonResetItemCount
				 // 
				 this->ButtonResetItemCount->Location = System::Drawing::Point(179, 62);
				 this->ButtonResetItemCount->Name = L"ButtonResetItemCount";
				 this->ButtonResetItemCount->Size = System::Drawing::Size(110, 20);
				 this->ButtonResetItemCount->TabIndex = 10;
				 this->ButtonResetItemCount->Text = L"Reset item count";
				 this->ButtonResetItemCount->Click += gcnew System::EventHandler(this, &ItemsInfo::ButtonResetItemCount_Click);
				 // 
				 // DropDownListVisible
				 // 
				 radListDataItem1->Text = L"True";
				 radListDataItem2->Text = L"False";
				 this->DropDownListVisible->Items->Add(radListDataItem1);
				 this->DropDownListVisible->Items->Add(radListDataItem2);
				 this->DropDownListVisible->Location = System::Drawing::Point(80, 62);
				 this->DropDownListVisible->Name = L"DropDownListVisible";
				 this->DropDownListVisible->Size = System::Drawing::Size(93, 20);
				 this->DropDownListVisible->TabIndex = 9;
				 this->DropDownListVisible->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &ItemsInfo::DropDownListVisible_SelectedIndexChanged);
				 // 
				 // radLabel5
				 // 
				 this->radLabel5->Location = System::Drawing::Point(5, 63);
				 this->radLabel5->Name = L"radLabel5";
				 this->radLabel5->Size = System::Drawing::Size(75, 18);
				 this->radLabel5->TabIndex = 8;
				 this->radLabel5->Text = L"Visible in Hud";
				 // 
				 // radLabel4
				 // 
				 this->radLabel4->Location = System::Drawing::Point(5, 90);
				 this->radLabel4->Name = L"radLabel4";
				 this->radLabel4->Size = System::Drawing::Size(41, 18);
				 this->radLabel4->TabIndex = 7;
				 this->radLabel4->Text = L"Item Id";
				 // 
				 // spin_buy_price
				 // 
				 this->spin_buy_price->Location = System::Drawing::Point(216, 36);
				 this->spin_buy_price->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1215752191, 23, 0, 0 });
				 this->spin_buy_price->Name = L"spin_buy_price";
				 this->spin_buy_price->Size = System::Drawing::Size(73, 20);
				 this->spin_buy_price->TabIndex = 2;
				 this->spin_buy_price->TabStop = false;
				 this->spin_buy_price->ValueChanged += gcnew System::EventHandler(this, &ItemsInfo::spin_buy_price_ValueChanged);
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(153, 37);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(52, 18);
				 this->radLabel3->TabIndex = 6;
				 this->radLabel3->Text = L"Buy price";
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(5, 37);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(51, 18);
				 this->radLabel2->TabIndex = 5;
				 this->radLabel2->Text = L"Sell price";
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(5, 10);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(60, 18);
				 this->radLabel1->TabIndex = 4;
				 this->radLabel1->Text = L"Item name";
				 // 
				 // tx_item_id
				 // 
				 this->tx_item_id->Location = System::Drawing::Point(80, 88);
				 this->tx_item_id->Name = L"tx_item_id";
				 this->tx_item_id->ReadOnly = true;
				 this->tx_item_id->Size = System::Drawing::Size(207, 20);
				 this->tx_item_id->TabIndex = 3;
				 // 
				 // spin_sell_price
				 // 
				 this->spin_sell_price->Location = System::Drawing::Point(80, 36);
				 this->spin_sell_price->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1215752191, 23, 0, 0 });
				 this->spin_sell_price->Name = L"spin_sell_price";
				 this->spin_sell_price->Size = System::Drawing::Size(73, 20);
				 this->spin_sell_price->TabIndex = 1;
				 this->spin_sell_price->TabStop = false;
				 this->spin_sell_price->ValueChanged += gcnew System::EventHandler(this, &ItemsInfo::spin_sell_price_ValueChanged);
				 // 
				 // box_item_name
				 // 
				 this->box_item_name->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->box_item_name->Location = System::Drawing::Point(80, 10);
				 this->box_item_name->Name = L"box_item_name";
				 this->box_item_name->Size = System::Drawing::Size(209, 20);
				 this->box_item_name->TabIndex = 0;
				 this->box_item_name->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &ItemsInfo::box_item_name_SelectedIndexChanged);
				 // 
				 // ButtonResetAllHud
				 // 
				 this->ButtonResetAllHud->Location = System::Drawing::Point(455, 264);
				 this->ButtonResetAllHud->Name = L"ButtonResetAllHud";
				 this->ButtonResetAllHud->Size = System::Drawing::Size(110, 24);
				 this->ButtonResetAllHud->TabIndex = 3;
				 this->ButtonResetAllHud->Text = L"Reset All Hud";
				 this->ButtonResetAllHud->Click += gcnew System::EventHandler(this, &ItemsInfo::ButtonResetAllHud_Click);
				 // 
				 // ItemsInfo
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(569, 295);
				 this->Controls->Add(this->ButtonResetAllHud);
				 this->Controls->Add(this->radGroupBox1);
				 this->Controls->Add(this->ItemListView);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->MaximizeBox = false;
				 this->Name = L"ItemsInfo";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->Text = L"Items Info";
				 this->FormClosing += gcnew System::Windows::Forms::FormClosingEventHandler(this, &ItemsInfo::ItemsInfo_FormClosing);
				 this->Load += gcnew System::EventHandler(this, &ItemsInfo::ItemsInfo_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ItemListView))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonResetItemCount))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListVisible))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel5))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_buy_price))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_sell_price))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->box_item_name))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonResetAllHud))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion



			 bool disable_item_update = false;


			 void update_idiom(){
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
				 radLabel5->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel5->Text))[0]);
				 ButtonResetItemCount->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonResetItemCount->Text))[0]);
				 ButtonResetAllHud->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonResetAllHud->Text))[0]);
			 }

	private: System::Void ItemsInfo_Load(System::Object^  sender, System::EventArgs^  e) {
				 disable_item_update = true;
				 update_idiom();
				 ItemListView->BeginUpdate();
				 std::map <uint32_t, std::shared_ptr<ItemsInfoLooted>> myMap = LooterCore::get()->get_map_items_looted();
				 for (auto item : myMap){
					 if (!item.second)
						 continue;

					 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
					 newItem->Tag = (int)item.second->item_id;
					 newItem->Text = gcnew String(item.second->name.c_str());
					 newItem->Image = managed_util::get_item_image(newItem->Text, 25);
					 box_item_name->Items->Add(gcnew String(item.second->name.c_str()));
					 ItemListView->Items->Add(newItem);
				 }
				 ItemListView->EndUpdate();
				 disable_item_update = false;
	}
	private: System::Void ItemListView_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
				 updateSelectedData();
	}

			 System::Void updateSelectedData(){
				 if (disable_item_update)
					 return;

				 if (!ItemListView->SelectedItem)
					 return;

				 auto itemsInfo = LooterCore::get()->getRuleItemsLooted((int)ItemListView->SelectedItem->Tag);
				 if (!itemsInfo)
					 return;

				 auto itemsInfoLooted = LooterCore::get()->getRuleItemsLooted((int)ItemListView->SelectedItem->Tag);
				 if (!itemsInfoLooted)
					 return;

				 std::string temp = managed_util::fromSS(ItemListView->SelectedItem->Text);

				 String^ ItemName = gcnew String(itemsInfo->name.c_str());
				 uint32_t buyPrice = itemsInfo->buy_price;
				 uint32_t sellPrice = itemsInfo->sell_price;
				 uint32_t item_id = itemsInfo->item_id;
				 bool visible_hud = itemsInfo->visible_in_hud;

				 disable_item_update = true;
				 DropDownListVisible->Text = Convert::ToString(visible_hud);
				 box_item_name->Text = ItemName;
				 spin_buy_price->Value = buyPrice;
				 spin_sell_price->Value = sellPrice;
				 tx_item_id->Text = Convert::ToString(item_id);
				 disable_item_update = false;
			 }

	private: System::Void spin_sell_price_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 auto itemsInfo = LooterCore::get()->getRuleItemsLooted((int)ItemListView->SelectedItem->Tag);

				 if (!itemsInfo)
					 return;

				 itemsInfo->sell_price = Convert::ToInt32(spin_sell_price->Value);
	}
	private: System::Void spin_buy_price_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 auto itemsInfo = LooterCore::get()->getRuleItemsLooted((int)ItemListView->SelectedItem->Tag);

				 if (!itemsInfo)
					 return;

				 itemsInfo->buy_price = Convert::ToInt32(spin_buy_price->Value);
	}
	private: System::Void box_item_name_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 for each(auto item in ItemListView->Items)
				 if (item->Text == box_item_name->Text)
					 ItemListView->SelectedItem = item;

	}
	private: System::Void ItemsInfo_FormClosing(System::Object^  sender, System::Windows::Forms::FormClosingEventArgs^  e) {
				 if (GeneralManager::get()->get_practice_mode()){
					 this->Hide();
					 e->Cancel = true;
					 return;
				 }
	}
	private: System::Void DropDownListVisible_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (!ItemListView->SelectedItem)
					 return;
				 
				 auto itemsInfo = LooterCore::get()->getRuleItemsLooted((int)ItemListView->SelectedItem->Tag);

				 if (!itemsInfo)
					 return;

				 bool temp = true;
				 if (DropDownListVisible->Text->ToLower() == "false")
					 temp = false;

				 itemsInfo->set_visible_in_hud(temp);
	}
private: System::Void ButtonResetItemCount_Click(System::Object^  sender, System::EventArgs^  e) {
			 if (!ItemListView->SelectedItem)
				 return;

			 if (!ItemListView->SelectedItem)
				 return;

			 auto itemsInfo = LooterCore::get()->getRuleItemsLooted((int)ItemListView->SelectedItem->Tag);

			 if (!itemsInfo)
				 return;

			 itemsInfo->reset_count();
}
private: System::Void ButtonResetAllHud_Click(System::Object^  sender, System::EventArgs^  e) {
			 LooterCore::get()->reset_hud();
}
};
}
