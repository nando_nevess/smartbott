#pragma once
#include "Core\Util.h"
#include "Core\Time.h"

enum utility_t : uint32_t{
	utility_none,
	utility_eat_food,
	utility_anti_afk,
	utility_drop_vial,
	utility_auto_fishing,
	utility_refil_amun,
	utility_screenshot,
	utility_rune_maker,
	utility_anti_paralyze,
	utility_auto_mana_shield,
	utility_auto_ring,
	utility_auto_haste,
	utility_anti_poison,
	utility_auto_sio,
	utility_auto_utura,
	utility_soft_change,
	utility_auto_mount,
	utility_total
};

struct ScriptInfo{
	utility_t script_type;
	uint32_t mana = 0;
	uint32_t hp = 0;
	uint32_t soul = 0;
	TimeChronometer time;
	uint32_t delay = 2000;
	bool active = false;
	std::string value_string = "";
	std::map<std::string,uint32_t> map_friend;

public:
	std::map<std::string, uint32_t> get_map_friend(){
		return map_friend;
	}

	void remove_map_friend(std::string index){
		auto it = map_friend.find(index);
		if (it != map_friend.end())
			map_friend.erase(index);
	}
	void add_map_friend(std::string in){
		map_friend[in] = 0;
	}
	
	uint32_t get_value_map_friend(std::string in){
		return map_friend[in];
	}
	void set_value_map_friend(std::string key,uint32_t in){
		map_friend[key] = in;
	}

	std::string request_new_id(){
		uint32_t current_id = 0;

		while (map_friend.find(std::to_string(current_id)) != map_friend.end())
			current_id++;

		map_friend[std::to_string(current_id)] = 0;
		return std::to_string(current_id);
	}
	bool changeFriendName(std::string newId, std::string oldId){
		if (newId == "" || oldId == "")
			return false;

		auto it = map_friend.find(oldId);
		if (it == map_friend.end())
			return false;

		map_friend[newId] = it->second;
		map_friend.erase(it);
		return true;
	}

	void set_hp(uint32_t in){
		hp = in;
	}
	uint32_t get_hp(){
		return hp;
	}

	void set_delay(uint32_t in){
		delay = in;
	}	
	uint32_t get_delay(){
		return delay;
	}

	void set_active(bool in){
		active = in;
	}
	bool get_active(){
		return active;
	}

	void set_script_type(utility_t in){
		script_type = in;
	}
	utility_t get_script_type(){
		return script_type;
	}

	void set_mana(uint32_t in){
		mana = in;
	}
	uint32_t get_mana(){
		return mana;
	}

	void set_soul(uint32_t in){
		soul = in;
	}
	uint32_t get_soul(){
		return soul;
	}
	
	void set_value_string(std::string in){
		value_string = in;
	}
	std::string get_value_string(){
		return value_string;
	}

	Json::Value parse_class_to_json() {
		Json::Value takeskinclass;
		Json::Value takeskinList;

		for (auto it : map_friend)
			takeskinList[it.first] = it.second;
		
		takeskinclass["takeskinList"] = takeskinList;
		return takeskinclass;
	}
	void parse_json_to_class(Json::Value jsonObject) {
		if (!jsonObject["takeskinList"].empty() || !jsonObject["takeskinList"].isNull()){
			Json::Value takeskinList = jsonObject["takeskinList"];

			Json::Value::Members members = takeskinList.getMemberNames();

			for (auto member : members)
				map_friend[member] = takeskinList[member].asInt();
			
		}
	}
};

class UtilityCore{
	std::map<std::string, std::shared_ptr<ScriptInfo>> map_scriptInfo;
	int current_level;

public:
	void set_current_level(int state){
		current_level = state;
	}
	int get_current_level(){
		return current_level;
	}

	UtilityCore::UtilityCore();

	std::map<std::string, std::shared_ptr<ScriptInfo>> get_map_scriptInfo();

	void run_thread();

	void clear();

	void creat_new_map();

	std::string get_script_utility(utility_t type);

	std::shared_ptr<ScriptInfo> get_rule_scriptInfo(std::string key);
	
	void execute_script(utility_t type, ScriptInfo* info);

	void execute_auto_haste(ScriptInfo* info);

	void execute_anti_poison(ScriptInfo* info);

	void execute_soft_change(ScriptInfo* info);

	void execute_auto_ring(ScriptInfo* info);

	void execute_auto_mana_shield(ScriptInfo* info);

	void execute_anti_paralyze(ScriptInfo* info);

	void execute_rune_maker(ScriptInfo* info);

	void execute_screenshot(ScriptInfo* info);

	void execute_refil_amun(ScriptInfo* info);

	void execute_auto_fishing(ScriptInfo* info);

	void execute_drop_vial(ScriptInfo* info);

	void execute_anti_afk(ScriptInfo* info);

	void execute_auto_utura(ScriptInfo* info);

	void execute_eat_food(ScriptInfo* info);
	
	void execute_auto_mount(ScriptInfo* info);	

	void execute_auto_sio(ScriptInfo* info);

	Json::Value parse_class_to_json() {
		Json::Value takeskinclass;
		Json::Value takeskinList;

		for (auto it : map_scriptInfo){
			Json::Value takeskinItem;

			takeskinItem["active"] = it.second->get_active();
			takeskinItem["delay"] = it.second->get_delay();
			takeskinItem["mana"] = it.second->get_mana();
			takeskinItem["hp"] = it.second->get_hp();
			takeskinItem["soul"] = it.second->get_soul();
			takeskinItem["value_string"] = it.second->get_value_string();
			takeskinItem["script_type"] = it.second->get_script_type();
			takeskinItem["friend"] = it.second->parse_class_to_json();

			takeskinList[it.first] = takeskinItem;
		}

		takeskinclass["takeskinList"] = takeskinList;
		return takeskinclass;
	}

	void parse_json_to_class(Json::Value jsonObject) {
		if (!jsonObject["takeskinList"].empty() || !jsonObject["takeskinList"].isNull()){
			Json::Value takeskinList = jsonObject["takeskinList"];

			Json::Value::Members members = takeskinList.getMemberNames();

			for (auto member : members){
				Json::Value takeskinItemJson = takeskinList[member];
				std::string keyScriptInfo = boost::lexical_cast<std::string>(member);

				std::shared_ptr<ScriptInfo> takeskinItem = std::shared_ptr<ScriptInfo>(new ScriptInfo);

				if (!takeskinItemJson["delay"].empty() || !takeskinItemJson["delay"].isNull())
					takeskinItem->set_delay(takeskinItemJson["delay"].asInt());
				else
					takeskinItem->set_delay(2000);

				if (!takeskinItemJson["active"].empty() || !takeskinItemJson["active"].isNull())
					takeskinItem->set_active(takeskinItemJson["active"].asBool());
				else
					takeskinItem->set_active(false);

				if (!takeskinItemJson["mana"].empty() || !takeskinItemJson["mana"].isNull())
					takeskinItem->set_mana(takeskinItemJson["mana"].asInt());
				else
					takeskinItem->set_mana(0);

				if (!takeskinItemJson["hp"].empty() || !takeskinItemJson["hp"].isNull())
					takeskinItem->set_hp(takeskinItemJson["hp"].asInt());
				else
					takeskinItem->set_hp(0);

				if (!takeskinItemJson["script_type"].empty() || !takeskinItemJson["script_type"].isNull())
					takeskinItem->set_script_type((utility_t)takeskinItemJson["script_type"].asInt());
				else
					takeskinItem->set_script_type((utility_t)0);

				if (!takeskinItemJson["soul"].empty() || !takeskinItemJson["soul"].isNull())
					takeskinItem->set_soul(takeskinItemJson["soul"].asInt());
				else
					takeskinItem->set_soul(0);

				if (!takeskinItemJson["soul"].empty() || !takeskinItemJson["soul"].isNull())
					takeskinItem->set_soul(takeskinItemJson["soul"].asInt());
				else
					takeskinItem->set_soul(0);

				if (!takeskinItemJson["value_string"].empty() || !takeskinItemJson["value_string"].isNull())
					takeskinItem->set_value_string(takeskinItemJson["value_string"].asString());
				else
					takeskinItem->set_value_string("");

				if (!takeskinItemJson["friend"].empty() || !takeskinItemJson["friend"].isNull())
					takeskinItem->parse_json_to_class(takeskinItemJson["friend"]);

				map_scriptInfo[keyScriptInfo] = takeskinItem;
			}
		}
	}
	
	static UtilityCore* get(){
		static UtilityCore* m = nullptr;
		if (!m)
			m = new UtilityCore();
		return m;
	}
};