#pragma once
#include "Hunter.h"
#include "WaypointManager.h"

using namespace NeutralBot;

void Hunter::additemonview(String^ name, String^ min_hp, String^ max_hp,
	bool shooter, bool loot, bool diagonal, String^ distance, String^ order, String^ group, String^ danger){
	String^ groupSubstr = group->Substring(strlen("Group "), group->Length - strlen("Group "));
	int intGroup = 0;
	try{
		intGroup = Convert::ToInt32(groupSubstr);
	}
	catch (...){
		Telerik::WinControls::RadMessageBox::Show(this,"Handle invalid selected group", "Error");
		return;
	}
	std::string id = HunterManager::get()->request_new_creature();
	TargetPtr target = HunterManager::get()->get_target_rule_by_id(id);

	target->set_name(managed_util::fromSS(name));
	

	TargetNonSharedPtr creature_non_shared = target->get_at_group(intGroup);

	creature_non_shared->set_loot(loot);
	creature_non_shared->set_danger(Convert::ToInt32(danger));

	creature_non_shared->set_min_hp(Convert::ToInt32(min_hp));
	creature_non_shared->set_max_hp(Convert::ToInt32(max_hp));
	creature_non_shared->set_shooter(shooter);

	creature_non_shared->set_distance(Convert::ToInt32(distance));
	creature_non_shared->set_diagonal(diagonal);
	creature_non_shared->set_point(0);
	creature_non_shared->set_order(Convert::ToInt32(order));

	creature_non_shared->set_weapon_id(0);

	Telerik::WinControls::UI::ListViewDataItem^ dataItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
	dataItem->SubItems->Add(name);
	
	dataItem->Value = gcnew String(&id[0]);
	ListMonster->Items->Add(dataItem);
	
	dataItem->Image = managed_util::get_creature_image(name, 35);
}

void Hunter::loadItemOnView(){
	auto item_map = HunterManager::get()->get_target_rules();
	
	ListMonster->BeginUpdate();	
	for (auto target : item_map){
		Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
		newItem[0] = gcnew String(&target.second->get_name()[0]);
		newItem->Value = gcnew String(&target.first[0]);
		newItem->Image = managed_util::get_creature_image(gcnew String(&target.second->get_name()[0]), 35);
		
		ListMonster->Items->Add(newItem);
	}	
	ListMonster->EndUpdate();
}

void Hunter::loadAll(){
	enabled_updates = false;

	loadItemOnView();

	dropDownGroup->BeginUpdate();

	for (int i = 0; i < 10; i++){
		RadListDataItem^ itemRow = gcnew RadListDataItem;
		itemRow->Value = i;
		itemRow->Text = ("Group " + (i + 1));
		dropDownGroup->Items->Add(itemRow);
	}
	dropDownGroup->EndUpdate();

	enabled_updates = false;

	DropDownListConsiderPath->BeginUpdate();
	DropDownListConsiderPath->Items->Clear();

	std::map<int, std::shared_ptr<WaypointPath>> myMapPath = WaypointManager::get()->getwaypointPathList();
	for (auto path : myMapPath){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = gcnew String(path.second->getName().c_str());
		item->Value = (int)path.first;

		DropDownListConsiderPath->Items->Add(item);
	}
	Telerik::WinControls::UI::RadListDataItem^ item_ = gcnew Telerik::WinControls::UI::RadListDataItem();
	item_->Text = "Consider All";
	item_->Value = (int)myMapPath.size();

	DropDownListConsiderPath->Items->Add(item_);
	DropDownListConsiderPath->SelectedItem = item_;

	DropDownListConsiderPath->EndUpdate();

	DropDownListConsiderPathTwo->BeginUpdate();
	DropDownListConsiderPathTwo->Items->Clear();
	for (auto path : myMapPath){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = gcnew String(path.second->getName().c_str());
		item->Value = (int)path.first;

		DropDownListConsiderPathTwo->Items->Add(item);
	}

	Telerik::WinControls::UI::RadListDataItem^ _item_ = gcnew Telerik::WinControls::UI::RadListDataItem();
	_item_->Text = "Consider All";
	_item_->Value = (int)myMapPath.size();

	DropDownListConsiderPathTwo->Items->Add(_item_);
	DropDownListConsiderPathTwo->SelectedItem = _item_;
	DropDownListConsiderPathTwo->EndUpdate();

	DropDownListNewMode->BeginUpdate();
	DropDownListNewMode->Items->Clear();
	for (int i = 0; i < attack_mode_t::attack_mode_no_change; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("attack_mode_t_") + std::to_string(i));
		item->Value = i;
		DropDownListNewMode->Items->Add(item);
	}
	DropDownListNewMode->EndUpdate();

	DropDownListRunnerMode->BeginUpdate();
	DropDownListRunnerMode->Items->Clear();
	for (int i = 0; i < move_mode_t::move_mode_t_total; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("move_mode_t_") + std::to_string(i));
		item->Value = i;
		DropDownListRunnerMode->Items->Add(item);
	}
	DropDownListRunnerMode->EndUpdate();

	DropDownListOldMode->BeginUpdate();
	DropDownListOldMode->Items->Clear();
	for (int i = 0; i < attack_mode_t::attack_mode_no_change; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("attack_mode_t_") + std::to_string(i));
		item->Value = i;
		DropDownListOldMode->Items->Add(item);
	}

	DropDownListOldMode->EndUpdate();

	dropDownGroup->SelectedIndex = 0;

	boxshooter_monster->Items->Add(gcnew RadListDataItem("false", false));
	boxshooter_monster->Items->Add(gcnew RadListDataItem("true", true));
	//boxshooter_monster->SelectedIndex = 0;
	
	check_target_any_monster->Checked = HunterManager::get()->get_any_monster();

	boxloot_monster->Items->Add(gcnew RadListDataItem("false", false));
	boxloot_monster->Items->Add(gcnew RadListDataItem("true", true));
	//boxloot_monster->SelectedIndex = 1;

	boxdiagonal_monster->Items->Add(gcnew RadListDataItem("false", false));
	boxdiagonal_monster->Items->Add(gcnew RadListDataItem("true", true));
	//boxdiagonal_monster->SelectedIndex = 0;

	spin_min_point->Value = HunterManager::get()->get_min_points();
	
	loadChangeMode();
	enabled_updates = true;
}

System::Void Hunter::btnew_monster_Click_1(System::Object^  sender, System::EventArgs^  e) {
	enabled_updates = false;
	additemonview("new", "0", "100", false, true, false, "1", "1", "Group 0", "1");
	ListMonster->SelectedIndex = ListMonster->Items->Count - 1;
	enabled_updates = true;
}

System::Void Hunter::btdelete_Click(System::Object^  sender, System::EventArgs^  e) {
	ListViewDataItem^ itemRow = ListMonster->SelectedItem;
	if (!itemRow)
		return;
	HunterManager::get()->delete_id(managed_util::fromSS((String^)itemRow->Value));
	ListMonster->Items->Remove(itemRow);

	enabled_updates = false;
	if (ListMonster->Items->Count <= 0){
		radGroupBox1->Enabled = false;
		radGroupBox2->Enabled = false;
		radGroupBox3->Enabled = false;
	}
	enabled_updates = true;
}

System::Void Hunter::ListMonster_ItemRemoving(System::Object^  sender, Telerik::WinControls::UI::ListViewItemCancelEventArgs^  e) {
	int index = ListMonster->SelectedIndex;
	Telerik::WinControls::UI::ListViewDataItem^ itemRow = ListMonster->SelectedItem;

	if (itemRow)
		HunterManager::get()->delete_id(managed_util::fromSS(itemRow->Text));
	
	if (index < ListMonster->Items->Count)
		ListMonster->SelectedIndex = index;

	enabled_updates = false;
	if (ListMonster->Items->Count <= 1){
		radGroupBox1->Enabled = false;
		radGroupBox2->Enabled = false;
		radGroupBox3->Enabled = false;
	}
	enabled_updates = true;
}

System::Void Hunter::Hunter_Load(System::Object^  sender, System::EventArgs^  e) { 
	loadAll();

	creatContextMenu();

	auto monsterlist = managed_util::convert_vector_to_array(MonsterManager::get()->get_MonsterList());
	txname_monster->Items->AddRange(monsterlist);

	CheckBoxPulseTrapped->Checked = HunterManager::get()->get_pulse_if_trapped();

	update_with_selected();
}

int Hunter::GetCurrentGroup(){
	return dropDownGroup->SelectedIndex + 1;
}

void Hunter::block_input(bool state){
	txname_monster->Enabled = state;
	boxmin_hp_monster->Enabled = state;
	boxmax_hp_monster->Enabled = state;
	boxshooter_monster->Enabled = state;
	boxloot_monster->Enabled = state;
	boxdiagonal_monster->Enabled = state;
	trackorder_monster->Enabled = state;
	trackdranger_monster->Enabled = state;
	boxdistance_monster->Enabled = state;
}

TargetPtr Hunter::getSelectedCreature(){
	int group = dropDownGroup->SelectedIndex;
	if (group < 0)
		return nullptr;
	if (!ListMonster->SelectedItem)
		return 0;

	std::string creature_id = managed_util::fromSS((String^)ListMonster->SelectedItem->Value);
	return HunterManager::get()->get_target_rule_by_id(creature_id);
}

std::shared_ptr<TargetNonShared> Hunter::getSelectedCreatureAtGroup(){
	TargetPtr curr_target = getSelectedCreature();
	if (!curr_target)
		return nullptr;
	return curr_target->get_at_group(dropDownGroup->SelectedIndex);
}

System::Void Hunter::txname_monster_TextChanged(System::Object^  sender, System::EventArgs^  e) {
	if (!enabled_updates)
		return;
	if (ListMonster->SelectedIndex == -1)
		return;
	TargetPtr target = getSelectedCreature();

	if (!target)
		return;

	target->set_name(managed_util::fromSS(txname_monster->Text));
	ListMonster->SelectedItem[0] = txname_monster->Text;
	
	ListMonster->SelectedItem->Image = managed_util::get_creature_image(txname_monster->Text, 35);

}

System::Void Hunter::boxmin_hp_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (!enabled_updates)
		return;
	//hp min
	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature_info)
		return;
	creature_info->set_min_hp((int)boxmin_hp_monster->Value);
}

System::Void Hunter::boxmax_hp_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	if (!enabled_updates)
		return;
	//hp max
	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature_info)
		return;
	creature_info->set_max_hp((int)boxmax_hp_monster->Value);
}

System::Void Hunter::boxshooter_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	//shooter
	if (!enabled_updates)
		return;

	if (boxshooter_monster->SelectedIndex == -1)
		return;

	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature_info)
		return;

	creature_info->set_shooter((bool)boxshooter_monster->SelectedItem->Value);
}

System::Void Hunter::boxloot_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	//loot
	if (!enabled_updates)
		return;
	if (boxloot_monster->SelectedIndex == -1)
		return;

	TargetNonSharedPtr creature = getSelectedCreatureAtGroup();
	if (!creature)
		return;

	creature->set_loot((bool)boxloot_monster->SelectedItem->Value);
}

System::Void Hunter::boxdiagonal_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	//diagonal
	if (!enabled_updates)
		return;
	if (boxdiagonal_monster->SelectedIndex == -1)
		return;

	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature_info)
		return;

	creature_info->set_diagonal((bool)boxdiagonal_monster->SelectedItem->Value);
}

System::Void Hunter::trackorder_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	//order
	if (!enabled_updates)
		return;
	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature_info)
		return;

	creature_info->set_order((int)trackorder_monster->Value);
}

System::Void Hunter::trackdranger_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	//danger
	if (!enabled_updates)
		return;
	TargetNonSharedPtr creature = getSelectedCreatureAtGroup();
	if (!creature)
		return;

	creature->set_danger((int)trackdranger_monster->Value);
}

System::Void Hunter::trackgroup_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	//group
}

System::Void Hunter::boxdistance_monster_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
	//distance
	if (!enabled_updates)
		return;
	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature_info)
		return;
	creature_info->set_distance((int)boxdistance_monster->Value);
}
		 
System::Void Hunter::radDropDownList2_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	//group
	block_input(!(dropDownGroup->SelectedIndex == -1));
	update_with_selected();

}

void Hunter::update_with_selected(){
	enabled_updates = false;

	TargetPtr creature = getSelectedCreature();
	if (!creature){
		if (radGroupBox2->Enabled && radGroupBox1->Enabled && radGroupBox3->Enabled){
			radGroupBox1->Enabled = false;
			radGroupBox2->Enabled = false;
			radGroupBox3->Enabled = false;
		}
	}

	TargetNonSharedPtr creature_info = getSelectedCreatureAtGroup();
	if (!creature || !creature_info){		
		enabled_updates = true;
		return;
	}

	if (!radGroupBox2->Enabled && !radGroupBox1->Enabled && !radGroupBox3->Enabled){
		radGroupBox1->Enabled = true;
		radGroupBox2->Enabled = true;
		radGroupBox3->Enabled = true;
	}
	
	txname_monster->Text = gcnew String(&creature->get_name()[0]);
	boxmin_hp_monster->Value = creature_info->get_min_hp();
	check_attack_if_block_path->Checked = creature->get_attack_if_trapped();
	check_attack_if_trapped->Checked = creature->get_attack_if_block_path();

	if (creature_info->get_weapon_id() != 0 && creature_info->get_weapon_id() != UINT32_MAX){
		std::string str_item_name = ItemsManager::get()->getItemNameFromId(creature_info->get_weapon_id());
		drop_down_list_target_weapon->Text = gcnew String(&str_item_name[0]);
	}
	else
		drop_down_list_target_weapon->Text = "none";

	spin_point->Value = creature_info->get_point();
	boxmax_hp_monster->Value = creature_info->get_max_hp();
	boxshooter_monster->SelectedIndex = creature_info->get_shooter() ? 1 : 0;
	boxloot_monster->Text = Convert::ToString(creature_info->get_loot());
	boxdiagonal_monster->Text = Convert::ToString(creature_info->get_diagonal());
	trackorder_monster->Value = creature_info->get_order();
	trackdranger_monster->Value = creature_info->get_danger();
	boxdistance_monster->Value = creature_info->get_distance();
	DropDownListRunnerMode->SelectedIndex = (int)creature_info->get_move_mode_type();
	//DropDownListLureType->SelectedIndex = (int)creature_info->get_type_lure() ? 1 : 0;

	radSpinMinRange->Value = creature_info->get_target_range_min();
	radSpinMaxRange->Value = creature_info->get_target_range_max();

	UpdateGif();

	enabled_updates = true;
}



System::Void Hunter::ListMonster_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
	if (!ListMonster->SelectedItem)
		return;

	update_with_selected();
}
System::Void Hunter::radContextMenu1_DropDownOpening(System::Object^  sender, System::ComponentModel::CancelEventArgs^  e) {
	if (ListMonster->Items->Count < 1)
		e->Cancel = true;
}

bool Hunter::save_script(std::string file_name){
	Json::Value file;
	file["version"] = 0100;

	file["HunterManager"] = HunterManager::get()->parse_class_to_json();

	return saveJsonFile(file_name, file);
}
bool Hunter::load_script(std::string file_name){
	Json::Value file = loadJsonFile(file_name);
	if (file.isNull())
		return false;

	HunterManager::get()->parse_json_to_class(file["HunterManager"]);
	return true;
}

void Hunter::UpdateGif(){
	std::string monsterName = managed_util::fromSS(txname_monster->Text + ".gif");
	std::pair<char*, uint32_t> data_buffer = GifManager::get()->get_item_creature_buffer(monsterName);
	if (data_buffer.first){
		System::IO::UnmanagedMemoryStream^ readStream =	gcnew System::IO::UnmanagedMemoryStream
			((unsigned char*)data_buffer.first, data_buffer.second);

		Image^ new_image = System::Drawing::Image::FromStream(readStream);
		
		if (new_image->Width > gifPictureBox->Width || new_image->Height > gifPictureBox->Height)
			gifPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::StretchImage;
		else
			gifPictureBox->SizeMode = System::Windows::Forms::PictureBoxSizeMode::CenterImage;
		
		this->gifPictureBox->Image = new_image;
	}
	else
		this->gifPictureBox->Image = nullptr;
}

System::Void Hunter::bt_save__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::SaveFileDialog fileDialog;
	fileDialog.Filter = "Hunter Script|*.Hunt";
	fileDialog.Title = "Save Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
		return;

	if (!save_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Saved", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved", "Save/Load");
}
System::Void Hunter::bt_load__Click(System::Object^  sender, System::EventArgs^  e) {
	System::Windows::Forms::OpenFileDialog fileDialog;
	fileDialog.Filter = "Hunter Script|*.Hunt";
	fileDialog.Title = "Open Script";

	if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel){
		return;
	}

	HunterManager::get()->Clear();
	ListMonster->Items->Clear();

	if (!load_script(managed_util::fromSS(fileDialog.FileName)))
		Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded", "Save/Load");
	else
		Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded", "Save/Load");

	loadAll();
}

System::Void Hunter::txname_monster_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	UpdateGif();
}


void Hunter::creatContextMenu(){
	ItemDuplicate = gcnew Telerik::WinControls::UI::RadMenuItem("Duplicate");

	radContextMenu1->Items->Add(ItemDuplicate);

	ItemDuplicate->Click += gcnew System::EventHandler(this, &Hunter::MenuDuplicate_Click);
}

System::Void Hunter::MenuDuplicate_Click(Object^ sender, EventArgs^ e){
	enabled_updates = false;
	auto temp = getSelectedCreature();
	additemonview(gcnew String(temp->get_name().c_str()), "0", "100", false, true, false, "1", "1", "Group 0", "1");
	txname_monster->Text = ListMonster->SelectedItem[0]->ToString();
	enabled_updates = true;
}
void Hunter::update_idiom_listbox(){
	DropDownListConsiderPath->BeginUpdate();
	DropDownListConsiderPath->Items->Clear();

	std::map<int, std::shared_ptr<WaypointPath>> myMapPath = WaypointManager::get()->getwaypointPathList();
	for (auto path : myMapPath){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = gcnew String(path.second->getName().c_str());
		item->Value = (int)path.first;

		DropDownListConsiderPath->Items->Add(item);
	}
	Telerik::WinControls::UI::RadListDataItem^ item_ = gcnew Telerik::WinControls::UI::RadListDataItem();
	item_->Text = "Consider All";
	item_->Value = (int)myMapPath.size();

	DropDownListConsiderPath->Items->Add(item_);
	DropDownListConsiderPath->SelectedItem = item_;

	DropDownListConsiderPath->EndUpdate();

	DropDownListConsiderPathTwo->BeginUpdate();
	DropDownListConsiderPathTwo->Items->Clear();
	for (auto path : myMapPath){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = gcnew String(path.second->getName().c_str());
		item->Value = (int)path.first;

		DropDownListConsiderPathTwo->Items->Add(item);
	}

	Telerik::WinControls::UI::RadListDataItem^ _item_ = gcnew Telerik::WinControls::UI::RadListDataItem();
	_item_->Text = "Consider All";
	_item_->Value = (int)myMapPath.size();

	DropDownListConsiderPathTwo->Items->Add(_item_);
	DropDownListConsiderPathTwo->SelectedItem = _item_;
	DropDownListConsiderPathTwo->EndUpdate();

	DropDownListNewMode->BeginUpdate();
	DropDownListNewMode->Items->Clear();
	for (int i = 0; i < attack_mode_t::attack_mode_no_change; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("attack_mode_t_") + std::to_string(i));
		item->Value = i;
		DropDownListNewMode->Items->Add(item);
	}
	DropDownListNewMode->EndUpdate();

	DropDownListOldMode->BeginUpdate();
	DropDownListOldMode->Items->Clear();
	for (int i = 0; i < attack_mode_t::attack_mode_no_change; i++){
		Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
		item->Text = GET_MANAGED_TR(std::string("attack_mode_t_") + std::to_string(i));
		item->Value = i;
		DropDownListOldMode->Items->Add(item);
	}

	DropDownListOldMode->EndUpdate();
}
void Hunter::update_idiom(){
	radPageViewCreatureAdvanced->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewCreatureAdvanced->Text))[0]);
	radPageViewCreatureSettings->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewCreatureSettings->Text))[0]);
	btnew_monster->Text = gcnew String(&GET_TR(managed_util::fromSS(btnew_monster->Text))[0]);
	btdelete->Text = gcnew String(&GET_TR(managed_util::fromSS(btdelete->Text))[0]);
	radButton2->Text = gcnew String(&GET_TR(managed_util::fromSS(radButton2->Text))[0]);
	radPageViewCreatures->Text = gcnew String(&GET_TR(managed_util::fromSS(radPageViewCreatures->Text))[0]);
	check_custom_runner->Text = gcnew String(&GET_TR(managed_util::fromSS(check_custom_runner->Text))[0]);
	check_target_any_monster->Text = gcnew String(&GET_TR(managed_util::fromSS(check_target_any_monster->Text))[0]);
	check_wait_pulse->Text = gcnew String(&GET_TR(managed_util::fromSS(check_wait_pulse->Text))[0]);
	CheckBoxPulseTrapped->Text = gcnew String(&GET_TR(managed_util::fromSS(CheckBoxPulseTrapped->Text))[0]);
	check_attack_if_trapped->Text = gcnew String(&GET_TR(managed_util::fromSS(check_attack_if_trapped->Text))[0]);
	check_attack_if_block_path->Text = gcnew String(&GET_TR(managed_util::fromSS(check_attack_if_block_path->Text))[0]);
	radLabel17->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel17->Text))[0]);
	radLabel16->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel16->Text))[0]);
	radLabel15->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel15->Text))[0]);
	radLabel14->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel14->Text))[0]);
	radLabel12->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel12->Text))[0]);
	radLabel10->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel10->Text))[0]);
	radLabel9->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel9->Text))[0]);
	radLabel8->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel8->Text))[0]);
	radLabel7->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel7->Text))[0]);
	radLabel6->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel6->Text))[0]);
	radLabel5->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel5->Text))[0]);
	radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
	radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
	radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
	radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
	bt_save_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_save_->Text))[0]);
	bt_load_->Text = gcnew String(&GET_TR(managed_util::fromSS(bt_load_->Text))[0]);

	for each(auto colum in ListMonster->Columns)
		colum->HeaderText = gcnew String(&GET_TR(managed_util::fromSS(colum->HeaderText))[0]);
}

System::Void Hunter::bt_adv_Click(System::Object^  sender, System::EventArgs^  e) {
	/*if (!panel_state){
		this->panel_adv->Show();
		panel_state = true;
		this->Size = System::Drawing::Size(this->Size.Width + 230, this->Size.Height);
		return;
	}

	if (panel_state){
		this->panel_adv->Hide();
		panel_state = false;
		this->Size = System::Drawing::Size(this->Size.Width - 230, this->Size.Height);
		return;
	}
	*/

}

System::Void Hunter::DropDownListLureType_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
	/*if (!enabled_updates)
		return;

	lure_mode_t mode = (lure_mode_t)DropDownListLureType->SelectedIndex;
	auto creature = getSelectedCreatureAtGroup();

	if (!creature)
		return;

	creature->set_type_lure(mode);*/
}

System::Void Hunter::check_target_any_monster_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	HunterManager::get()->set_any_monster(check_target_any_monster->Checked);
}

System::Void Hunter::radMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
}

System::Void Hunter::radButton1_Click(System::Object^  sender, System::EventArgs^  e) {
	NeutralBot::LuaBackground^ mluaEditor = gcnew NeutralBot::LuaBackground(gcnew String(&HunterManager::get()->get_keep_distance_algorithm_data()[0]));
	
	mluaEditor->hide_left_pane();
	mluaEditor->ShowDialog();

	HunterManager::get()->set_keep_distance_algorithm(managed_util::fromSS(mluaEditor->fastColoredTextBox1->Text));
}

System::Void Hunter::check_custom_runner_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	HunterManager::get()->set_has_keek_distance_hook(check_custom_runner->Checked);
}

System::Void Hunter::radToggleButtonHunter_ToggleStateChanged(System::Object^  sender, Telerik::WinControls::UI::StateChangedEventArgs^  args) {
	if (radToggleButtonHunter->IsChecked)
		check_remove_duplicates(true);
	
	radPanelContent->Enabled = !radToggleButtonHunter->IsChecked;
}






















