#include "NavigationManager.h"
#include <thread>

class SendInfoManager{
	boost::asio::io_service _service;

	bool is_running_send_player_inventory = false;
	bool is_send_player_inventory = false;

	bool is_running_send_player_message = false;
	bool is_send_player_message = false;
	
	bool is_running_send_player_info = false;
	bool is_send_player_info = false;

	bool is_running_send_player_containers = false;
	bool is_send_player_containers = false;

	bool is_running_send_player_creatures = false;
	bool is_send_player_creatures = false;

	bool is_running_send_player_waypointer = false;
	bool is_send_player_waypointer = false;

	std::string simple_message = "";
	uint32_t work_delay = 500;

	boost::shared_ptr<boost::asio::deadline_timer> timer_geral;
	boost::shared_ptr<boost::asio::deadline_timer> timer_send_player_info;
	boost::shared_ptr<boost::asio::deadline_timer> timer_send_player_inventory;
	boost::shared_ptr<boost::asio::deadline_timer> timer_send_player_containers;
	boost::shared_ptr<boost::asio::deadline_timer> timer_send_player_creatures;
	boost::shared_ptr<boost::asio::deadline_timer> timer_send_player_waypointer;
	boost::shared_ptr<boost::asio::deadline_timer> timer_send_player_message;

	std::mutex mtx_access;

public:
	SendInfoManager(){		
		timer_geral = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));
		timer_send_player_info = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));
		timer_send_player_inventory = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));
		timer_send_player_containers = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));
		timer_send_player_creatures = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));
		timer_send_player_waypointer = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));
		timer_send_player_message = boost::shared_ptr<boost::asio::deadline_timer>(new boost::asio::deadline_timer(_service));

		std::thread([&](){
			Sleep(5000);
			start();

			while (true) {
				Sleep(50);
				try {
					_service.run();
				}
				catch (std::exception& ex) {
					std::cout << "\n Runner out reason: " << ex.what();
				}
			}
		}).detach();
	}

	void start(){
		timer_geral->expires_from_now(boost::posix_time::milliseconds(3000));

		timer_geral->async_wait(boost::bind(&SendInfoManager::invoke, this));
	}
	void invoke(){

		wait_to_send_player_info();
		wait_to_send_player_inventory();
		wait_to_send_player_containers();
		wait_to_send_player_creatures();
		wait_to_send_player_waypointer();
		wait_to_send_player_message();

		start();
	}

	void set_work_delay(uint32_t delay){
		std::lock_guard<std::mutex> _lock(mtx_access);

		work_delay = delay;
	}

	void set_simple_message(std::string message){
		std::lock_guard<std::mutex> _lock(mtx_access);		
		simple_message = message;
	}
	std::string get_simple_message(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return simple_message;
	}

	bool get_is_send_player_info(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return is_send_player_info;
	}
	void set_is_send_player_info(bool state){
		std::lock_guard<std::mutex> _lock(mtx_access);

		if (is_send_player_info == state)
			return;

		is_send_player_info = state;

		if (state && !is_running_send_player_info)
			wait_to_send_player_info();
	}

	bool get_is_send_player_inventory(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return is_send_player_inventory;
	}
	void set_is_send_player_inventory(bool state){
		std::lock_guard<std::mutex> _lock(mtx_access);

		if (is_send_player_inventory == state)
			return;

		is_send_player_inventory = state;

		if (state && !is_running_send_player_inventory)
			wait_to_send_player_inventory();
	}

	bool get_is_send_player_containers(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return is_send_player_containers;
	}
	void set_is_send_player_containers(bool state){
		std::lock_guard<std::mutex> _lock(mtx_access);

		if (is_send_player_containers == state)
			return;

		is_send_player_containers = state;

		if (state && !is_running_send_player_containers)
			wait_to_send_player_containers();
	}
	
	bool get_is_send_player_creatures(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return is_send_player_creatures;
	}
	void set_is_send_player_creatures(bool state){
		std::lock_guard<std::mutex> _lock(mtx_access);

		if (is_send_player_creatures == state)
			return;

		is_send_player_creatures = state;

		if (state && !is_running_send_player_creatures)
			wait_to_send_player_creatures();
	}
	
	bool get_is_send_player_waypointer(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return is_send_player_waypointer;
	}
	void set_is_send_player_waypointer(bool state){
		std::lock_guard<std::mutex> _lock(mtx_access);

		if (is_send_player_waypointer == state)
			return;

		is_send_player_waypointer = state;

		if (state && !is_running_send_player_waypointer)
			wait_to_send_player_waypointer();
	}
	
	bool get_is_send_player_message(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		return is_send_player_message;
	}
	void set_is_send_player_message(bool state){
		std::lock_guard<std::mutex> _lock(mtx_access);

		if (is_send_player_message == state)
			return;

		is_send_player_message = state;

		if (state && !is_running_send_player_message)
			wait_to_send_player_message();
	}

	void wait_to_send_player_info(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		
		if (!is_send_player_info){
			is_running_send_player_info = false;
			return;
		}

		if (is_running_send_player_info)
			return;

		is_running_send_player_info = true;

		timer_send_player_info->expires_from_now(boost::posix_time::milliseconds(work_delay));
				
		timer_send_player_info->async_wait(std::bind([&](){
			//std::cout << "\n async_wait";
			is_running_send_player_info = false;
			NavigationManager::get()->send_player_info();
			wait_to_send_player_info();
		}));
	}
	void wait_to_send_player_inventory(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		if (!is_send_player_inventory){
			is_running_send_player_inventory = false;
			return;
		}

		if (is_running_send_player_inventory)
			return;

		is_running_send_player_inventory = true;

		timer_send_player_inventory->expires_from_now(boost::posix_time::milliseconds(work_delay));

		timer_send_player_inventory->async_wait(std::bind([&](){
			is_running_send_player_inventory = false;
			NavigationManager::get()->send_player_inventory();
			wait_to_send_player_inventory();
		}));
	}
	void wait_to_send_player_containers(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		if (!is_send_player_containers){
			is_running_send_player_containers = false;
			return;
		}

		if (is_running_send_player_containers)
			return;

		is_running_send_player_containers = true;

		timer_send_player_containers->expires_from_now(boost::posix_time::milliseconds(work_delay));

		timer_send_player_containers->async_wait(std::bind([&](){
			is_running_send_player_containers = false;
			NavigationManager::get()->send_player_containers();
			wait_to_send_player_containers();
		}));
	}
	void wait_to_send_player_creatures(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		if (!is_send_player_creatures){
			is_running_send_player_creatures = false;
			return;
		}

		if (is_running_send_player_creatures)
			return;

		is_running_send_player_creatures = true;

		timer_send_player_creatures->expires_from_now(boost::posix_time::milliseconds(work_delay));

		timer_send_player_creatures->async_wait(std::bind([&](){
			is_running_send_player_creatures = false;
			NavigationManager::get()->send_player_creatures();
			wait_to_send_player_creatures();
		}));
	}
	void wait_to_send_player_waypointer(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		if (!is_send_player_waypointer){
			is_running_send_player_waypointer = false;
			return;
		}

		if (is_running_send_player_waypointer)
			return;

		is_running_send_player_waypointer = true;

		timer_send_player_waypointer->expires_from_now(boost::posix_time::milliseconds(work_delay));

		timer_send_player_waypointer->async_wait(std::bind([&](){
			is_running_send_player_waypointer = false;
			NavigationManager::get()->send_player_waypointer();
			wait_to_send_player_waypointer();
		}));
	}
	void wait_to_send_player_message(){
		std::lock_guard<std::mutex> _lock(mtx_access);
		if (!is_send_player_message){
			is_running_send_player_message = false;
			return;
		}

		if (is_running_send_player_message)
			return;

		is_running_send_player_message = true;

		timer_send_player_message->expires_from_now(boost::posix_time::milliseconds(work_delay));

		timer_send_player_message->async_wait(std::bind([&](){
			is_running_send_player_message = false;
			NavigationManager::get()->send_player_message("");
			wait_to_send_player_message();
		}));
	} 

	static SendInfoManager* get(){
		static SendInfoManager* m = nullptr;
		if (!m)
			m = new SendInfoManager();
		return m;
	}
};