#pragma once
#include "Core\InfoCore.h"
#include "Core\LooterCore.h"
#include "Core\Messages.h"
#include "RepoterManager.h"
#include "TakeSkinCore.h"
#include "Core\ItemsManager.h"
#include "Core\Actions.h"
#include "Core\SpellCasterCore.h"
#include <regex>
#include "TakeSkinManager.h"
#include "string_util.h"
#include <thread>
#include "Core\FloatingMiniMenu.h"
#include "LooterManager.h"
#include "Core\ContainerManager.h"
#include "Core\Pathfinder.h"
#include "DevelopmentManager.h"


LooterCore* LooterCore::mLooterCore;

void LooterCore::init(){
	static neutral_mutex mtx_once;
	mtx_once.lock();

	if (mLooterCore){
		mtx_once.unlock();
		return;
	}

	mLooterCore = new LooterCore;
	mtx_once.unlock();

	std::thread([&](){
		MONITOR_THREAD("LooterCore::start");
		mLooterCore->start();
	}).detach();


	std::thread([&](){ 
		MONITOR_THREAD("LastLootControl::check_loot_position")
			LastLootControl::check_loot_position();
	}).detach();

	std::thread([&](){ 
		MONITOR_THREAD("LooterCore::run_check_looted");
		mLooterCore->run_check_looted();
	}).detach();

}

uint32_t LastLootControl::coord_monster_x = 0;
uint32_t LastLootControl::coord_monster_y = 0;
bool LastLootControl::lootcontrol_state = false;
bool LastLootControl::add_to_fail = false;

std::shared_ptr<LootControlStateAutoDisable> LooterCore::set_lootcontrol(Coordinate coord){
	LastLootControl::add_to_fail = false;
	LastLootControl::lootcontrol_state = true;
	LastLootControl::coord_monster_x = coord.getX();
	LastLootControl::coord_monster_y = coord.getY();
	return
		std::shared_ptr<LootControlStateAutoDisable>(new LootControlStateAutoDisable());
}

LootControlStateAutoDisable::~LootControlStateAutoDisable(){
	LastLootControl::lootcontrol_state = false;
}

void LooterCore::disable_loot_control(){
	LastLootControl::lootcontrol_state = false;
}

void LastLootControl::check_loot_position(){
	while (true){
		Sleep(10);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;

		if (!lootcontrol_state)
			continue;

		if (coord_monster_x == 0 || coord_monster_y == 0)
			continue;

		Coordinate character_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
		Coordinate monster_coord = Coordinate(coord_monster_x, coord_monster_y, character_coord.getZ());

		if (character_coord.get_axis_max_dist(monster_coord) > 1){
			lootcontrol_state = false;
			add_to_fail = true;
			continue;
		}

		if (!TibiaProcess::get_default()->character_info->is_walking())
			continue;

		auto going_step = TibiaProcess::get_default()->character_info->get_going_step();
		if (going_step.is_null())
			continue;

		uint32_t dist = monster_coord.get_axis_max_dist(going_step);
		if (dist >= 1){
			lootcontrol_state = false;
			add_to_fail = true;
			continue;
		}
	}
}

void LooterCore::start(){
	while (true){
		Sleep(5);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		run_async();
	}
}

enum local_looter_action{
	looter_action_drop,
	looter_action_drop_from_own,
	looter_action_loot,
	looter_action_total_t,
	looter_action_none = looter_action_total_t
};

struct MoveItemStruct{
	uint32_t item_id;
	uint32_t count;
	bool* has_action;
	ContainerPosition* source;
	ContainerPosition* dest;
	ItemContainer* source_container_ptr;
	ItemContainer* dest_container_ptr;
	std::vector<std::string> args;
};

#define LOOT_DROP move_loot_drop(mstruct);
#define LOOT_MOVE move_loot_container(mstruct);
#define LOOT_DROP_SELF move_loot_drop_self(mstruct);

void move_loot_drop(MoveItemStruct* mstruct){
	*mstruct->has_action = true;
	Actions::move_item_container_to_coordinate(*mstruct->source, TibiaProcess::get_default()->character_info->get_self_coordinate(), mstruct->count, mstruct->item_id);
}

void move_loot_container(MoveItemStruct* mstruct){
	*mstruct->has_action = true;

	Actions::check_move_item_container_to_container(*mstruct->source, *mstruct->dest, mstruct->count, mstruct->item_id, false);
}

void move_loot_drop_self(MoveItemStruct* mstruct){
	*mstruct->has_action = true;
	Actions::move_item_container_to_coordinate(*mstruct->source,
		TibiaProcess::get_default()->character_info->get_self_coordinate(), mstruct->count, mstruct->item_id);
}

action_retval_t move_item_mode_loot_item_condition_none(MoveItemStruct* mstruct){
	LOOT_MOVE
		return action_retval_success;
}

action_retval_t move_item_mode_drop_if_destination_full(MoveItemStruct* mstruct){
	if (mstruct->dest_container_ptr->is_full() && mstruct->dest_container_ptr->has_next())
		LOOT_DROP
	else
	LOOT_MOVE
	return action_retval_success;
}

action_retval_t move_item_mode_drop_if_cap_bellow(MoveItemStruct* mstruct){
	uint32_t cap_min = 0;
	try{
		cap_min = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	if (TibiaProcess::get_default()->character_info->cap() < cap_min)
		LOOT_DROP
	else
	LOOT_MOVE
	return action_retval_success;
}

action_retval_t move_item_mode_drop_if_have_more_than(MoveItemStruct* mstruct){
	uint32_t value = 0;
	try{
		value = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	if (ContainerManager::get()->get_item_count(mstruct->item_id) > value)
		LOOT_DROP
	else
	LOOT_MOVE
	return action_retval_success;
}

action_retval_t move_item_mode_dont_loot_if_have_more_than(MoveItemStruct* mstruct){
	uint32_t value = 0;
	try{
		value = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	if (ContainerManager::get()->get_item_count(mstruct->item_id) > value)
		return action_retval_success;
	else
		LOOT_MOVE
		return action_retval_success;
}

action_retval_t move_item_mode_dont_loot_if_cap_bellow_than(MoveItemStruct* mstruct){
	uint32_t value = 0;
	try{
		value = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	if (TibiaProcess::get_default()->character_info->cap() < value)
		return action_retval_success;
	else
		LOOT_MOVE
		return action_retval_success;
}

action_retval_t move_item_mode_drop(MoveItemStruct* mstruct){
		LOOT_DROP
		return action_retval_success;
}

action_retval_t move_item_mode_drop_from_own_backpack_if_have_more_than(MoveItemStruct* mstruct){
	uint32_t value = 0;
	try{
		value = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	//TODO
	if (ContainerManager::get()->get_item_count(mstruct->item_id) > value)
		LOOT_DROP_SELF
	else
		LOOT_MOVE
	return action_retval_success;
}

action_retval_t move_item_mode_drop_from_own_backpack_if_cap_bellow_than(MoveItemStruct* mstruct){
	uint32_t value = 0;
	try{
		value = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	//TODO
	if (TibiaProcess::get_default()->character_info->cap() < value)
		LOOT_DROP_SELF
	else
		LOOT_MOVE
	return action_retval_success;

}

action_retval_t move_item_mode_loot_item_new_condition_none(MoveItemStruct* mstruct){
	return action_retval_success;
}

action_retval_t move_item_mode_if_destination_full_cancel2(uint32_t mstruct){
	auto container = ContainerManager::get()->get_container_by_id(mstruct);
	if (!container)
		return action_retval_fail;

	if (container->is_full() && container->has_next())
		return action_retval_fail;

	return action_retval_success;
}

action_retval_t move_item_mode_if_cap_bellow_than_cancel2(uint32_t mstruct){
	uint32_t cap_min = mstruct;
	if (TibiaProcess::get_default()->character_info->cap() < cap_min)
		return action_retval_fail;

	return action_retval_success;
}

action_retval_t move_item_mode_if_have_more_than_cancel2(uint32_t item ,uint32_t mstruct){
	uint32_t value = mstruct;

	if (ContainerManager::get()->get_item_count(item) >= value)
		return action_retval_fail;

	return action_retval_success;

}

action_retval_t move_item_mode_if_destination_full_cancel(MoveItemStruct* mstruct){
	if (mstruct->dest_container_ptr->is_full() && mstruct->dest_container_ptr->has_next())
		return action_retval_fail;

	return action_retval_success;
}

action_retval_t move_item_mode_if_cap_bellow_than_cancel(MoveItemStruct* mstruct){
	uint32_t cap_min = 0;
	try{
		cap_min = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	if (TibiaProcess::get_default()->character_info->cap() < cap_min)
		return action_retval_fail;

	return action_retval_success;
}

action_retval_t move_item_mode_if_have_more_than_cancel(MoveItemStruct* mstruct){
	uint32_t value = 0;
	try{
		value = boost::lexical_cast<uint32_t>(mstruct->args[0]);
	}
	catch (...){
	}
	if (ContainerManager::get()->get_item_count(mstruct->item_id) > value)
		return action_retval_fail;

	return action_retval_success;
}

#define MOVE_LOOT_MODE(type) move_item_mode_##type(mstruct);

action_retval_t check_condition(loot_item_new_condition_t type_condition, MoveItemStruct* mstruct, bool condition_more){
	action_retval_t move_or_drop;

	switch (type_condition){
	case loot_item_new_condition_t::if_destination_full_cancel:
		move_or_drop = MOVE_LOOT_MODE(if_destination_full_cancel);
		break;
	case loot_item_new_condition_t::if_cap_bellow_than_cancel:
		move_or_drop = MOVE_LOOT_MODE(if_cap_bellow_than_cancel);
		break;
	case loot_item_new_condition_t::if_have_more_than_cancel:
		move_or_drop = MOVE_LOOT_MODE(if_have_more_than_cancel);
		break;
	}

	return move_or_drop;
}

action_retval_t move_item_mode(LooterItemNonSharedPtr type, MoveItemStruct* mstruct){
	std::vector<std::shared_ptr<LooterConditions>> vector_condition = type->get_vector_condition();
	if (vector_condition.size() <= 0){
		switch (type->get_condition()){
		case loot_item_condition_none:
			return MOVE_LOOT_MODE(loot_item_condition_none)
				break;
		case drop_if_destination_full:
			return MOVE_LOOT_MODE(drop_if_destination_full)
				break;
		case drop_if_cap_bellow:
			return MOVE_LOOT_MODE(drop_if_cap_bellow)
				break;
		case drop_if_have_more_than:
			return MOVE_LOOT_MODE(drop_if_have_more_than)
				break;
		case dont_loot_if_have_more_than:
			return MOVE_LOOT_MODE(dont_loot_if_have_more_than)
				break;
		case dont_loot_if_cap_bellow_than:
			return MOVE_LOOT_MODE(dont_loot_if_cap_bellow_than)
				break;
		case drop:
			return MOVE_LOOT_MODE(drop)
				break;
		case drop_from_own_backpack_if_have_more_than:
			return MOVE_LOOT_MODE(drop_from_own_backpack_if_have_more_than)
				break;
		case drop_from_own_backpack_if_cap_bellow_than:
			return MOVE_LOOT_MODE(drop_from_own_backpack_if_cap_bellow_than)
				break;
		}
	}

	action_retval_t execute_action = action_retval_t::action_retval_fail;
	loot_item_action_t last_action = loot_item_action_t::loot_item_action_total;
	loot_item_action_t current_action;
	for (auto it : vector_condition){
		if (last_action == it->action)
			continue;

		current_action = it->action;
		mstruct->args.push_back(std::to_string(it->value));
		execute_action = check_condition(it->condition, mstruct, true);

		if (execute_action != action_retval_t::action_retval_success)
			continue;

		switch (current_action){
		case loot_item_action_t::loot_item_action_move:
			LOOT_MOVE;
			break;
		case loot_item_action_t::loot_item_action_drop:
			LOOT_DROP;
			break;
		case loot_item_action_t::loot_item_action_pick_up:			
			break;
		}

		last_action = current_action;
	}


	return action_retval_t::action_retval_success;
}

action_retval_t LooterCore::move_items(int32_t timeout){
	std::vector<uint32_t> our_containers_id;
	TimeChronometer timer;

	for (auto container : LooterManager::get()->containers) 
		our_containers_id.push_back(container.second->get_container_id());	

	uint64_t cant_carry_count = InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_is_too_heavy_to_carry)->count;

	Actions::reopen_containers(false, [&]()-> bool { 
		return timer.elapsed_milliseconds() > 10000; 
	});

	timer.reset();
	bool has_action = false;
	uint32_t loop_try = 0;

	do{
		Sleep(5);
		auto current_container_list = ContainerManager::get()->get_containers();
		for (auto container : current_container_list) {
			if (!container.second)
				continue;

			container.second->maximize();

			auto our_container = std::find(our_containers_id.begin(), our_containers_id.end(), container.second->get_id());
			if (!container.second->is_browse_field() && (our_container != our_containers_id.end()))
				continue;

			uint32_t item_count = 0;
			auto all_items = container.second->get_all_items_id();
			for (auto item : all_items){
				std::shared_ptr<LooterContainer> owner_in_for = LooterManager::get()->get_container_owns_by_item_id(item);					
				if (owner_in_for)
					break;

				if (all_items.size() - 1 < item_count)
					return action_retval_t::action_retval_success;
				
				item_count++;			
			}

			int total_items = container.second->header_info->used_slots;
			for (int slot_index = total_items - 1; slot_index >= 0; slot_index--){
				if (!container.second->is_open || LastLootControl::add_to_fail)
					return action_retval_t::action_retval_trying;

				int item_id = container.second->slots[slot_index].id;
				std::shared_ptr<LooterContainer> owner = LooterManager::get()->get_container_owns_by_item_id(item_id);
				if (!owner)
					continue;							

				std::shared_ptr<ItemContainer> destination = ContainerManager::get()->get_container_by_id(owner->get_container_id());
				if (!destination || destination->is_full()){
					action_retval_t open_check_dest_container_state;
					ContainerManager::get()->resize_all_containers();
					while ((open_check_dest_container_state = Actions::open_container_while_full(owner->get_container_id())) == action_retval_timeout){}
					
					if (look_for_stackable_in_full_container)
						if (open_check_dest_container_state != action_retval_t::action_retval_success)
							continue;
									
					destination = ContainerManager::get()->get_container_by_id(owner->get_container_id());
					ContainerManager::get()->resize_all_containers();

					refresh_items_in_map_looted();
				}

				if (!destination)
					continue;

				LooterItemNonSharedPtr current_item_info = LooterManager::get()->get_looter_item_by_item_id(item_id);
				if (!current_item_info)
					continue;				
				

				uint32_t loot_index = destination->get_slot_index_to_move_item(item_id);
				if (loot_index == UINT32_MAX)
					continue;		

				refresh_items_in_map_looted();
				looted_time.reset();

				ContainerPosition current_souce_pos(container.second->get_container_index(), slot_index);
				ContainerPosition current_dest_pos(destination->get_container_index(), loot_index);
				MoveItemStruct current_item_struct;
				current_item_struct.count = 100;
				current_item_struct.has_action = &has_action;
				current_item_struct.item_id = item_id;
				current_item_struct.source = &current_souce_pos;
				current_item_struct.dest = &current_dest_pos;
				current_item_struct.source_container_ptr;
				current_item_struct.dest_container_ptr = destination.get();
				//current_item_struct.args.push_back(std::to_string(current_item_info->get_value()));
				
				if (LastLootControl::add_to_fail)
					return action_retval_t::action_retval_trying;				

				move_item_mode(current_item_info, &current_item_struct);

				/*if (LooterManager::get()->get_current_loot_type() == loot_type_t::loot_type_t_fast){
					TibiaProcess::get_default()->wait_ping_delay(1);
					break;
				}*/
			}
		}
		loop_try++;
		TibiaProcess::get_default()->wait_ping_delay(1);
		if (!has_action && timer.elapsed_milliseconds() < timeout)
			break;
		
		else if (loop_try >= 5)
			break;
		
		else if (InfoCore::get()->get_warning_message_info(warning_message_type::warning_message_is_too_heavy_to_carry)->count - cant_carry_count > 1)
			break;

	} while (true);


	/*if (has_action)
		return action_retval_t::action_retval_timeout;*/

	if (LastLootControl::add_to_fail)
		return action_retval_t::action_retval_trying;

	return action_retval_t::action_retval_success;
}

void LooterCore::refresh_containers_state(){
	last_containers_state = ContainerManager::get()->get_containers();
}

std::vector<std::pair<uint32_t/*bpid*/, uint32_t/*address tibia*/>> current_cotainer_states;

int32_t get_count_not_owner();

int32_t get_count_warning_message();

bool LooterCore::is_pulsed(){
	lock_guard m_lock(mtx_access);
	return m_is_pulsed;
}

bool LooterCore::check_pulse(){

	if (get_current_sequence_mode() == loot_sequence_t::loot_sequence_wait_pulse){

		lock_guard m_lock(mtx_access);
		if (!m_is_pulsed)
			return false;
	}
	return true;
}

void LooterCore::unset_pulse(){
	lock_guard m_lock(mtx_access);
	m_is_pulsed = false;
}

void LooterCore::pulse(){
	lock_guard m_lock(mtx_access);
	m_is_pulsed = true;
}

std::map <uint32_t, std::shared_ptr<ItemsInfoLooted>> LooterCore::get_map_items_looted(){
	return map_items_looted;
}

std::vector<std::shared_ptr<LootCreatureOnQueue>> LootListQueue::getLootsCreaturesOnQueueCopy(){
	lock_guard lock(mtx_access);
	return LootsCreaturesOnQueue;
}

std::vector<std::shared_ptr<LootCreatureOnQueue>> LootListQueue::getRemovedLootsCreaturesOnQueueCopy(){
	lock_guard lock(mtx_access);
	return RemovedLootsCreaturesOnQueue;
}

void LootListQueue::clear_loot_list(){
	lock_guard lock(mtx_access);
	LootsCreaturesOnQueue.clear();
	creatures_on_queue = 0;
}

core_priority LooterCore::get_core_priority(){
	core_priority current_prior = CoreBase::get_core_priority();
	if (current_prior == core_priority::core_priority_normal){
		if (this->get_current_sequence_mode() == loot_sequence_t::loot_sequence_wait_pulse && !is_pulsed()){
			return core_priority::core_priority_lower;
		}
	}
	return current_prior;
}

LootListQueue* LootListQueue::get(){
	static LootListQueue* mLootListQueue = nullptr;
	if (!mLootListQueue)
		mLootListQueue = new LootListQueue;
	
	return mLootListQueue;
}

std::shared_ptr<ItemsInfoLooted> LooterCore::getRuleItemsLooted(uint32_t item_id){
	if (item_id == 0)
		return nullptr;

	return map_items_looted[item_id];
}

bool LooterCore::wait_loot_complete(uint32_t timeout){
	TimeChronometer timer;
	while (need_operate()){

		if ((uint32_t)timer.elapsed_milliseconds() > timeout)
			return false;
		
		Sleep(50);
	}
	return true;
}

bool LooterCore::wait_pulse_out(uint32_t timeout){
	TimeChronometer timer;
	while (true){
		if (!NeutralManager::get()->get_looter_state())
			return false;
		
		if (!m_is_pulsed)
			return true;
		
		if (!need_operate())
			return true;
		
		if ((uint32_t)timer.elapsed_milliseconds() > timeout)
			return false;
		
		Sleep(50);
	}
	return true;
}

bool LooterCore::wait_pulse(uint32_t timeout){
	TimeChronometer timer;
	while (true){
		if (!m_is_pulsed)
			return true;

		if (!NeutralManager::get()->get_looter_state())
			return false;		

		if ((uint32_t)timer.elapsed_milliseconds() > timeout)
			return false;

		Sleep(50);
	}
	return true;
}

void LooterCore::add_valious_loot_time(std::string message){
	lock_guard _guard(mtx_access);
	valious_loot_time.push_back(std::pair<TimeChronometer, std::string>(TimeChronometer(), message) );
}

void LooterCore::clear_valious_loot_time(bool lock){
	for (auto it = valious_loot_time.begin(); it != valious_loot_time.end();){
		if (it->first.elapsed_milliseconds() < 5 * 60 * 1000)
			continue;

		it = valious_loot_time.erase(it);
	}
	/*if (lock){
		lock_guard _guard(mtx_access);
		valious_loot_time.clear();
	}
	else{
		valious_loot_time.clear();
	}*/
}

void LooterCore::reset_hud(){
	mtx_access.lock();
	for (auto it : map_items_looted){
		it.second->count = 0;
		it.second->count_temp = 0;
	}

	InfoCore::get()->itensusedmap.clear();

	refresh_items_in_map_looted();
	mtx_access.unlock();
}

bool LooterCore::is_near_valious_time(TimeChronometer* d_time){
	lock_guard _guard(mtx_access);

	int32_t passed_milli = d_time->elapsed_milliseconds();
	int32_t time_delay = ((int32_t)TibiaProcess::get_default()->get_action_wait_delay()) * -2;
	for (auto it : valious_loot_time){
		int32_t diff = passed_milli - it.first.elapsed_milliseconds();
		if (diff > time_delay)
			return true;		
	}
	return false;
}

void LooterCore::run_async() {
	if (!can_execute())
		return;

	while (true) {
		Sleep(5);

		if (!check_pulse())
			continue;

		if (!HunterCore::get()->need_operate())
			pick_up_item();

		mtx_access.lock();
		LootCreatureOnQueuePtr creature_to_loot = LootListQueue::get()->get_nearest_ready_loot(true);
		if (!creature_to_loot) {
			//clear_valious_loot_time(false);
			set_priority_normal();
			waken_state = false;
			var_need_operate = false;
			var_is_looting = false;
			unset_pulse();

			mtx_access.unlock();
			return;
		}

		CreatureOnBattlePtr creature_died = creature_to_loot->get_creature_died();
		if (!creature_died) {
			set_priority_normal();
			waken_state = false;
			var_need_operate = false;

			mtx_access.unlock();
			return;
		}
		mtx_access.unlock();

		if (LooterManager::get()->get_current_filter_type() != loot_filter_t::loot_filter_t_from_target){
			std::shared_ptr<TimeChronometer> d_time = creature_to_loot->get_died_time();

			if (!LooterCore::get()->is_near_valious_time(d_time.get())){
				if (d_time->elapsed_milliseconds() > 1500){
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_warning_is_not_valious);
					var_is_looting = false;
					continue;
				}
			}
		}


		if (!TakeSkinManager::get()->get_priority_looter()){
			if (TakeSkinCore::get()->need_operate()){
				var_is_looting = false;
				continue;
			}
		}

		if (LooterCore::get_current_sequence_mode() != loot_sequence_t::loot_sequence_inteligent || !HunterCore::get()->need_operate()){
			if (creature_to_loot->get_died_time()->elapsed_milliseconds() > 240000){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_timeout);
				var_is_looting = false;
				continue;
			}
			if (TibiaProcess::get_default()->character_info->cap() < LooterManager::get()->get_min_cap() && LooterManager::get()->get_min_cap_to_loot()){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_timeout);
				var_is_looting = false;
				continue;
			}
			if ((uint32_t)creature_to_loot->get_timeout_count() >= max_try_loot_count) {
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_timeout);
				var_is_looting = false;
				continue;
			}

			if ((uint32_t)creature_to_loot->get_fail_count() >= max_try_loot_count) {
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_unreachable);
				var_is_looting = false;
				continue;
			}

			if ((uint32_t)creature_to_loot->get_count_cannot_use() >= try_loot_count_warning_message) {
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_action_retval_warning_cannot_use);
				var_is_looting = false;
				continue;
			}
			if ((uint32_t)creature_to_loot->get_fail_move_item_count() >= max_try_loot_count) {
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_cant_loot_all);
				var_is_looting = false;
				continue;
			}

			if (creature_to_loot->get_timeout_count() >= 4){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_timeout);
				var_is_looting = false;
				continue;
			}
			else if (creature_to_loot->get_fail_count() >= 4){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_action_retval_fail);
				var_is_looting = false;
				continue;
			}
			else if (creature_to_loot->get_count_not_owner() >= 1){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_warning_message_not_owner);
				var_is_looting = false;
				continue;
			}
			else if (creature_to_loot->get_count_first_go_upstairs() >= 2){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_warning_message_upstairs);
				var_is_looting = false;
				continue;
			}
			else if (creature_to_loot->get_count_first_go_downstairs() >= 2){
				LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_warning_message_downstairs);
				var_is_looting = false;
				continue;
			}
		}

		Coordinate creature_coordinate = creature_died->get_coordinate_monster();
		Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
		auto target = BattleList::get()->get_target();

		if (NeutralManager::get()->get_core_states(CORE_HUNTER) == ENABLED){
			auto state = HunterCore::get()->get_state_info();
			if (state.state == true || (state.state == false && state.time_changed_state < 1000)){
				if (creature_coordinate.get_axis_max_dist(current_coordinate) > 1){
					var_is_looting = false;
					continue;
				}
			}
		}

		uint32_t timeout_open_corposes = (uint32_t)std::min((float)TibiaProcess::get_default()->get_action_delay_promedy_time(9), (float)3000);

		uint32_t action_delay = TibiaProcess::get_default()->get_action_delay_promedy_time(1);
		uint32_t time_died = creature_to_loot->get_died_time()->elapsed_milliseconds();

		if (time_died < action_delay)
			Sleep(action_delay - time_died);

		std::shared_ptr<LootControlStateAutoDisable> loot_control_state_disabler;


		switch (LooterCore::get_current_sequence_mode()){
#pragma region CASE_SEQUENCE_INSTANT
		case loot_sequence_t::loot_sequence_instant:{
			set_priority_high();
			var_need_operate = true;
			var_is_looting = true;

			if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) <= 1) {
				loot_control_state_disabler = set_lootcontrol(creature_coordinate);
				action_retval_t open_retval = Actions::open_container_at_coordinate(creature_to_loot->get_creature_died()->get_coordinate_monster(), timeout_open_corposes, creature_to_loot->get_stack_pos());
				if (open_retval == action_retval_t::action_retval_trying || LastLootControl::add_to_fail) {
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_fail_count();
					}
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_timeout){
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_timeout_count();
					}
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_fail){
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_fail_count();
					}
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_not_owner){
					creature_to_loot->add_count_not_owner();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_cannot_use){
					creature_to_loot->add_count_cannot_use();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_first_go_upstairs){
					creature_to_loot->add_count_first_go_upstairs();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_first_go_downstairs){
					creature_to_loot->add_count_first_go_downstairs();
					continue;
				}

				try_eat();

				action_retval_t move_items_retval = move_items(10000);
				if (move_items_retval == action_retval_t::action_retval_trying || LastLootControl::add_to_fail) {
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_fail_count();
					}
					var_is_looting = false;
					continue;
				}
				if (move_items_retval == action_retval_t::action_retval_success) {
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_looted);
					var_is_looting = false;
					continue;
				}
				else if (move_items_retval == action_retval_t::action_retval_trying) {
					creature_to_loot->add_fail_count();

					continue;
				}
				continue;
			}
			else {
				if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) > LooterManager::get()->get_max_distance()){
					var_is_looting = false;
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_out_of_range);
					continue;
				}

				bool reachable = Pathfinder::get()->wait_reachability(creature_coordinate, 1000, map_front_t, 10, false);
				if (!reachable) {
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_unreachable);
					var_is_looting = false;
					var_need_operate = false;
					continue;
				}

				Actions::goto_near_coord(creature_coordinate, true, 1, map_type_t::map_front_t, 500
					, true, pathfinder_state_placeholder, false,
					[](){
					if (NeutralManager::get()->get_core_states(CORE_HUNTER) == ENABLED){
						auto state = HunterCore::get()->get_state_info();
						if (state.state == true || (state.state == false && state.time_changed_state < 500)){
							return false;
						}
					}
					return true;
				}
				);
				var_is_looting = false;
				var_need_operate = false;
				continue;
			}
			var_is_looting = false;
			var_need_operate = false;
		}
			break;
#pragma endregion
#pragma region CASE_SEQUENCE_AFTER_KILL
		case loot_sequence_t::loot_sequence_after_kill_all: {
			set_priority_normal();
			if (HunterCore::get()->need_operate())
				continue;

			var_is_looting = true;
			if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) <= 1) {
				loot_control_state_disabler = set_lootcontrol(creature_coordinate);
				//close_all_non_looter_containers();
				action_retval_t open_retval = Actions::open_container_at_coordinate(creature_to_loot->get_creature_died()->get_coordinate_monster(), timeout_open_corposes, creature_to_loot->get_stack_pos());
				if (open_retval == action_retval_t::action_retval_trying || LastLootControl::add_to_fail) {
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_fail_count();
					}
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_timeout){
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_timeout_count();
					}
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_fail){
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_fail_count();
					}
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_not_owner){
					creature_to_loot->add_count_not_owner();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_cannot_use){
					creature_to_loot->add_count_cannot_use();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_first_go_upstairs){
					creature_to_loot->add_count_first_go_upstairs();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_first_go_downstairs){
					creature_to_loot->add_count_first_go_downstairs();
					continue;
				}
				try_eat();

				action_retval_t move_items_retval = move_items(10000);
				if (move_items_retval == action_retval_t::action_retval_trying || LastLootControl::add_to_fail) {
					if (!HunterCore::get()->need_operate()){
						creature_to_loot->add_fail_count();
					}
					var_is_looting = false;
					continue;
				}

				if (move_items_retval == action_retval_t::action_retval_success) {
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_looted);
				}
				else if (move_items_retval == action_retval_t::action_retval_success) {
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_looted);
					var_is_looting = false;
					continue;
				}
				else if (move_items_retval == action_retval_t::action_retval_trying) {
					creature_to_loot->add_fail_count();

					if ((uint32_t)creature_to_loot->get_fail_move_item_count() >= max_try_loot_count)
						LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_cant_loot_all);

					var_is_looting = false;
					continue;
				}
				continue;
			}
			else {
				if (HunterCore::get()->need_operate()) {
					var_is_looting = false;
					continue;
				}

				if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) > LooterManager::get()->get_max_distance()){
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_out_of_range);
					var_is_looting = false;
					continue;
				}


				bool reachable = Pathfinder::get()->wait_reachability(creature_coordinate, 1000, map_front_t, 10, false);
				if (!reachable) {
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_unreachable);
					var_is_looting = false;
					continue;
				}
				Actions::goto_near_coord(creature_coordinate, true, 1, map_type_t::map_front_t, 500, true, pathfinder_state_placeholder, false, [&]()-> bool { if (!HunterCore::get()->need_operate())	return true; return false; });
				continue;
			}
		}
			break;
#pragma endregion
#pragma region CASE_SEQUENCE_INTELIGENT
		case loot_sequence_t::loot_sequence_wait_pulse:
		case loot_sequence_t::loot_sequence_inteligent: {
			set_priority_normal();
			if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) <= 1){
				if (TibiaProcess::get_default()->character_info->is_walking())
					continue;

				if (target){
					if (current_coordinate.get_axis_max_dist(target->get_coordinate_monster()) > 1)
						continue;
				}

				var_is_looting = true;
				if (!MapTiles::can_click_coord(creature_to_loot->get_creature_died()->get_coordinate_monster())){
					if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) > 1)
						continue;

					Actions::goto_coord_on_screen(creature_to_loot->get_creature_died()->get_coordinate_monster(), 0);

					if (TibiaProcess::get_default()->character_info->get_self_coordinate() != creature_to_loot->get_creature_died()->get_coordinate_monster()){

						if (!HunterCore::get()->need_operate())
							creature_to_loot->add_fail_count();

						TibiaProcess::get_default()->character_info->wait_stop_waking();
						continue;
					}
				}

				loot_control_state_disabler = set_lootcontrol(creature_coordinate);

				action_retval_t open_retval = Actions::open_container_at_coordinate(creature_to_loot->get_creature_died()->get_coordinate_monster(),
					timeout_open_corposes, creature_to_loot->get_stack_pos(), false, false, [&]()-> bool {
					if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) > 1)
						return true;

					return false;
				});

				if (open_retval == action_retval_t::action_retval_trying || LastLootControl::add_to_fail) {
					if (!HunterCore::get()->need_operate())
						creature_to_loot->add_fail_count();

					continue;
				}
				else if (open_retval == action_retval_t::action_retval_timeout){
					if (!HunterCore::get()->need_operate())
						creature_to_loot->add_timeout_count();

					continue;
				}
				else if (open_retval == action_retval_t::action_retval_fail){
					if (!HunterCore::get()->need_operate())
						creature_to_loot->add_fail_count();

					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_not_owner){
					creature_to_loot->add_count_not_owner();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_cannot_use){
					creature_to_loot->add_count_cannot_use();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_first_go_upstairs){
					creature_to_loot->add_count_first_go_upstairs();
					continue;
				}
				else if (open_retval == action_retval_t::action_retval_warning_message_first_go_downstairs){
					creature_to_loot->add_count_first_go_downstairs();
					continue;
				}

				try_eat();

				action_retval_t move_items_retval = move_items(10000);
				if (move_items_retval == action_retval_t::action_retval_trying || LastLootControl::add_to_fail) {

					if (!HunterCore::get()->need_operate())
						creature_to_loot->add_fail_count();

					var_is_looting = false;
					continue;
				}
				else if (move_items_retval == action_retval_t::action_retval_success) {
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_looted);
					var_is_looting = false;
					continue;
				}

				continue;
			}
			else {
				if (HunterCore::get()->need_operate())
					continue;

				if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) > LooterManager::get()->get_max_distance()){
					var_is_looting = false;
					LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_out_of_range);
					continue;
				}

				bool reachable = Pathfinder::get()->wait_reachability(creature_coordinate, 1000, map_front_t, 10, true);
				if (!reachable) {
					if (!HunterCore::get()->need_operate())
						LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_unreachable);

					continue;
				}

				if (HunterCore::get()->need_operate())
					continue;

				var_is_looting = true;
				Actions::goto_near_coord(creature_coordinate, true, 1, map_type_t::map_front_t, 500, true, pathfinder_state_placeholder, false, [&]()-> bool { 
					if (!HunterCore::get()->get_need())
						return true; 

					return false; });
				var_is_looting = false;
				continue;
			}
		}
			break;
#pragma endregion
		}
	}
}

bool LooterCore::need_operate(){
	if ((var_need_operate || LootListQueue::get()->creatures_on_queue)
		&&
		(get_core_priority() == core_priority::core_priority_normal
		|| (get_core_priority() < core_priority::core_priority_normal && is_pulsed())))
		return true;
	return false;
}

bool LooterCore::can_execute(){
	if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state())
		return false;

	if (!NeutralManager::get()->get_bot_state())
		return false;

	if (SpellCasterCore::get()->need_operate_looter())
		return false;

	if (NeutralManager::get()->get_core_states(CORE_LOOTER) != ENABLED)	
		return false;

	if (time_to_pause.elapsed_milliseconds() < timeout_to_pause)
		return false;

	switch (get_current_sequence_mode()) {
		case loot_sequence_t::loot_sequence_instant:
			return true;
		break;
		case loot_sequence_t::loot_sequence_after_kill_all:{
			if (NeutralManager::get()->get_core_states(CORE_HUNTER) == ENABLED){
				auto state = HunterCore::get()->get_state_info();
				if (state.state == true || (state.state == false && state.time_changed_state < 500))
					return false;
				else
					return true;				
			}
		}
		break;
		case loot_sequence_t::loot_sequence_inteligent:
			return true;
		break;
		case loot_sequence_t::loot_sequence_wait_pulse:{
			return is_pulsed();
		}
		break;
	}
	return false;
}

bool LooterCore::is_looting() {
	if (!var_is_looting)
		return false;

	return true;
}

LooterCore::LooterCore(){
}

void LooterCore::set_wake_state(bool state){
	mtx_access.lock();
	waken_state = state;
	mtx_access.unlock();
}

LooterCore* LooterCore::get(){
	if (!mLooterCore)
		init();
	return mLooterCore;
}

void LootCreatureOnQueue::set_string(std::string loot_message){
	this->loot_message = loot_message;
	process_loot_message();
	message_assigned = true;
}

void LootListQueue::process_target_change(CreatureOnBattlePtr creature){
	if (LooterCore::get_current_filter_mode() == loot_filter_t::loot_filter_t_from_target){
		if (creature){
			auto creature_found = get_loot_creature_by_creature_id(creature->id);
			if (creature_found){
				creature_found->set_creature(creature);
			}
		}
	}
}

std::shared_ptr<LootCreatureOnQueue> LootListQueue::get_loot_creature_by_creature_id(uint32_t creature_id){
	for (auto creature_loot : LootsCreaturesOnQueue){
		auto creature_died = creature_loot->get_creature_died();
		if (creature_died)
		if (creature_died->id == creature_id)
			return creature_loot;
	}
	return nullptr;
}

void LootCreatureOnQueue::update_died_time(){
	died_time = std::shared_ptr<TimeChronometer>(new TimeChronometer);
}

std::map<std::string, uint32_t> LootCreatureOnQueue::get_items_from_message(std::string& message){
	std::map<std::string, uint32_t> retval;
	std::regex rgx(": (.*)");//(\([^\)]+\))
	std::smatch match;
	const std::string rgex_message = message + ",";
	if (std::regex_search(rgex_message.begin(), rgex_message.end(), match, rgx)){
		std::string real_match = match[1];

		int last = 0;
		int next = real_match.find(",");
		while (next != std::string::npos || real_match.size() > 2) {
			std::string real_match_ = real_match.substr(last, next);
			const std::string real_message = string_util::trim2(real_match_);

			if (real_message.find("a ") == 0){

				std::regex rgx_a("a (.*)");
				if (std::regex_search(real_message.begin(), real_message.end(), match, rgx_a)){
					auto it = retval.find(match[1]);
					if (it != retval.end())
						it->second += 1;
					else
						retval[match[1]] = 1;
				}

			}
			else if (real_message.find("an ") == 0){
				std::regex rgx_a("an (.*)");
				if (std::regex_search(real_message.begin(), real_message.end(), match, rgx_a)){
					auto it = retval.find(match[1]);
					if (it != retval.end())
						it->second += 1;
					else
						retval[match[1]] = 1;
				}
			}
			else if (real_message[0] >= '0' && real_message[0] <= '9'){
				// is digit
				size_t last_index = real_message.find_first_not_of("0123456789");
				std::string count_str = real_message.substr(0, last_index);
				std::string item_str = real_message.substr(last_index + 1, real_message.length() - last_index);
				auto it = retval.find(item_str);
				if (it != retval.end())
					it->second += atoi(&count_str[0]);
				else
					retval[item_str] = atoi(&count_str[0]);
			}
			else{//just item_name
				auto it = retval.find(real_message);
				if (it != retval.end())
					it->second += 1;
				else
					retval[real_message] = 1;
			}

			if (next == std::string::npos)
				break;
			int temp_next = next + 1;
			int temp_last = real_match.size() - (next + 1);
			real_match = real_match.substr(next + 1, real_match.size() - (next + 1));
			next = real_match.find(",");
		}
	}
	return retval;
}

void LootCreatureOnQueue::process_loot_message(){
	loot_items = get_items_from_message(loot_message);
}

LootListQueue::LootListQueue(){
	BattleList::get()->add_event(hunter_event_t::hunter_event_creature_die, std::bind(&LootListQueue::process_creature_died, this, std::placeholders::_1));
	Messages::get()->add_tibia_message_callbacks(std::bind(&LootListQueue::process_message_string, this, std::placeholders::_1));
}

void LootListQueue::remove_loot_from_loot(std::shared_ptr<LootCreatureOnQueue> loot_creature, loot_queue_remove_reason remove_reason){
	mtx_access.lock();
	for (auto it = LootsCreaturesOnQueue.begin(); it != LootsCreaturesOnQueue.end();){
		if (it->get() == loot_creature.get()){
			it->get()->remove_reason = remove_reason;
			RemovedLootsCreaturesOnQueue.push_back(*it);
			if (RemovedLootsCreaturesOnQueue.size() > 50)
				RemovedLootsCreaturesOnQueue.erase(RemovedLootsCreaturesOnQueue.begin());

			it = LootsCreaturesOnQueue.erase(it);
			creatures_on_queue--;
		}
		else
			it++;
	}
	mtx_access.unlock();
}

void LootListQueue::process_message_string(std::string message_str){
	if (message_str.find("Loot of") != std::string::npos){
		std::string creature_name;
		const std::string message = message_str;
		std::regex rgx("Loot of an (.*):");
		std::regex rgx2("Loot of a (.*):");
		std::regex rgx3("Loot of (.*):");
		std::smatch match;

		if (std::regex_search(message.begin(), message.end(), match, rgx))
			creature_name = std::string(match[1]);
		else if (std::regex_search(message.begin(), message.end(), match, rgx2))
			creature_name = std::string(match[1]);
		else if (std::regex_search(message.begin(), message.end(), match, rgx3))
			creature_name = std::string(match[1]);

		if (!creature_name.empty()){

			auto items = LootCreatureOnQueue::get_items_from_message(message_str);
			if (LootCreatureOnQueue::match_some_loot(items)){
				for (auto item : items){
				}
				LooterCore::get()->add_valious_loot_time(message_str);
			}

			if (BattleList::get()->last_target_id || BattleList::get()->current_target_id){
				last_message_time.reset();
				last_message = message_str;
				find_message_owner();
			}
		}
	}
}

std::shared_ptr<LootCreatureOnQueue> LootListQueue::get_latest_died(){
	std::shared_ptr<LootCreatureOnQueue> retval;
	int32_t time_died = INT32_MAX;
	for (auto died : LootsCreaturesOnQueue){
		auto died_time = died->get_died_time();
		if (!died_time)
			continue;
		if (time_died < died_time->elapsed_milliseconds())
			continue;
		retval = died;
		time_died = died_time->elapsed_milliseconds();
	}
	return retval;
}

std::shared_ptr<LootCreatureOnQueue> LootListQueue::get_latest_died_with_message(){
	std::shared_ptr<LootCreatureOnQueue> retval;
	int32_t time_died = INT32_MAX;
	for (auto died : LootsCreaturesOnQueue){
		auto died_time = died->get_died_time();
		if (!died_time)
			continue;
		if (time_died < died_time->elapsed_milliseconds())
			continue;
		if (died->get_loot_message().empty())
			continue;
		retval = died;
		time_died = died_time->elapsed_milliseconds();
	}
	return retval;
}

std::shared_ptr<LootCreatureOnQueue> LootListQueue::get_latest_died_without_message(){
	std::shared_ptr<LootCreatureOnQueue> retval;
	int32_t time_died = INT32_MAX;
	for (auto died : LootsCreaturesOnQueue){
		auto died_time = died->get_died_time();

		if (!died_time)
			continue;

		int died_time_elapsed = died_time->elapsed_milliseconds();

		if (time_died < died_time_elapsed)
			continue;

		if (!died->get_loot_message().empty())
			continue;

		retval = died;
		time_died = died_time->elapsed_milliseconds();
	}
	return retval;
}

void LootListQueue::find_message_owner(){
	mtx_access.lock();
	if (last_message.length() > 1){
		int last_message_time_elapsed = last_message_time.elapsed_milliseconds();
		if (last_message_time_elapsed < 400){
			auto latest_died = get_latest_died_without_message();
			if (latest_died){
				latest_died->set_string(last_message);
				last_message = "";
			}
		}
		else
			last_message = "";
	}
	mtx_access.unlock();
}

void LootListQueue::wake_looter_core(){
	LooterCore::get()->set_wake_state(true);
}

void LootListQueue::clear_not_reachable_loots(){

}

void LootListQueue::clear_older_loots_than(uint32_t milliseconds){

}

void LootListQueue::add_creature_to_queue(std::shared_ptr<LootCreatureOnQueue> creature){
	lock_guard lock(mtx_access);
	creature->queue_id = loot_queue_id;
	loot_queue_id++;
	Coordinate self_coord(
		creature->get_creature_died()->pos_x,
		creature->get_creature_died()->pos_z,
		creature->get_creature_died()->pos_y);

	for (auto creature_queeu : LootsCreaturesOnQueue) {
		Coordinate coord(
			creature_queeu->get_creature_died()->pos_x,
			creature_queeu->get_creature_died()->pos_z,
			creature_queeu->get_creature_died()->pos_y);

		if (self_coord == coord)
			creature_queeu->set_stack_pos(creature_queeu->get_stack_pos() - 1);
	}


	LootsCreaturesOnQueue.push_back(creature);
	creatures_on_queue++;
}

void LootListQueue::process_creature_died(CreatureOnBattlePtr creature){
	if (NeutralManager::get()->is_paused() || !NeutralManager::get()->get_bot_state())
		return;

	if (NeutralManager::get()->get_core_states(CORE_LOOTER) != ENABLED)
		return;
	
	if (LooterManager::get()->get_recoord_only_when_hunter_on())
		if (NeutralManager::get()->get_core_states(CORE_HUNTER) != ENABLED)
			return;

		//TODO take from looter manager
	switch (LooterCore::get_current_filter_mode()){
	case loot_filter_t::loot_filter_t_from_target:{
		auto temp = HunterManager::get()->get_target_by_values(creature->name, creature->life);
		if (BattleList::get()->current_target_id == creature->id || BattleList::get()->last_target_id == creature->id){
			std::shared_ptr<LootCreatureOnQueue> new_loot_creature(new LootCreatureOnQueue);
			new_loot_creature->set_died(creature);

			if (!temp.second)
				new_loot_creature->set_looted(true);
			else
				new_loot_creature->set_looted(temp.second->get_loot());

			new_loot_creature->update_died_time();
			add_creature_to_queue(new_loot_creature);
			find_message_owner();
		}
	}
		break;
	case loot_filter_t::loot_filter_t_from_all:{
		auto temp = HunterManager::get()->get_target_by_values(creature->name, creature->life);
		std::shared_ptr<LootCreatureOnQueue> new_loot_creature(new LootCreatureOnQueue);
		new_loot_creature->set_died(creature);

		if (!temp.second)
			new_loot_creature->set_looted(true);
		else
			new_loot_creature->set_looted(temp.second->get_loot());

		new_loot_creature->update_died_time();
		add_creature_to_queue(new_loot_creature);
		find_message_owner();
	}
		break;
	case loot_filter_t::loot_filter_t_from_all_on_hunter:{
		auto temp = HunterManager::get()->get_target_by_values(creature->name, creature->life);
		if (temp.first && temp.second){
			std::shared_ptr<LootCreatureOnQueue> new_loot_creature(new LootCreatureOnQueue);
			new_loot_creature->set_died(creature);

			if (!temp.second)
				new_loot_creature->set_looted(true);
			else
				new_loot_creature->set_looted(temp.second->get_loot());

			new_loot_creature->update_died_time();
			add_creature_to_queue(new_loot_creature);
			find_message_owner();
		}
	}
		break;
	}	
}

inline bool MatchCreatureFilter(LootCreatureOnQueue* creature_raw_ptr){
	switch (LooterCore::get_current_filter_mode()){
		case (loot_filter_t::loot_filter_t_from_all) : {
			return true;
		}
		break;
		case (loot_filter_t::loot_filter_t_from_all_on_hunter) : {
			auto creature = creature_raw_ptr->get_creature_died();
			if (!creature)
				return false;
			return HunterManager::get()->contains_creature_name(creature->name);
		}
		break;
		case (loot_filter_t::loot_filter_t_from_target) : {
			auto items = creature_raw_ptr->get_loot_items();
			if (items.size()) {
				if (LooterCore::get_current_filter_mode() == loot_filter_t::loot_filter_t_from_target){
					if (!creature_raw_ptr->match_some_loot())
						return false;
				}

				for (auto item : items){
					auto loot_item = LooterManager::get()->get_item_by_name_and_count((std::string&)item.first, item.second);
					if (loot_item.first)
						return true;
				}
			}
		}
		break;
	}

	return false;
}

std::shared_ptr<LootCreatureOnQueue> LootListQueue::get_nearest_ready_loot(bool clear_not_rearchable){
	//TODO ADicionar max distance
	lock_guard lock(mtx_access);

	uint32_t last_dist = UINT32_MAX;
	std::shared_ptr<LootCreatureOnQueue> last_creature = nullptr;
	Coordinate distance;
	Coordinate current_coord = TibiaProcess::get_default()->character_info->get_self_coordinate();
	uint32_t ping_med = TibiaProcess::get_default()->get_action_delay_promedy_time(1);
	uint32_t last_found_cost = UINT32_MAX;
	std::map<std::shared_ptr<LootCreatureOnQueue>, loot_queue_remove_reason> creatures_to_remove;
	auto sequence_mode = LooterCore::get_current_sequence_mode();

	if (sequence_mode == loot_sequence_instant || sequence_mode == loot_sequence_t::loot_sequence_wait_pulse){
		for (auto it = LootsCreaturesOnQueue.begin(); it != LootsCreaturesOnQueue.end(); it++){
			int32_t time_died = it->get()->get_died_time()->elapsed_milliseconds();
			if (time_died <= (int32_t)ping_med)
				continue;

			if (!it->get()->get_looted()){
				creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
				continue;
			}

			/*if (LooterCore::get_current_filter_mode() != loot_filter_t::loot_filter_t_from_all ){
				creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
				continue;
			}*/

			//match_some_loot
			/*if (time_died > 15000){
			creatures_to_remove.push_back(*it);
			continue;
			}*/
			if (LooterCore::get_current_filter_mode() == loot_filter_t::loot_filter_t_from_target && it->get()->message_assigned){
				if (!MatchCreatureFilter(it->get())){
					creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
					continue;
				}
			}

			//check coost to go, minor than older
			uint32_t current_distance = it->get()->get_creature_died()->get_coordinate_monster().get_total_distance(current_coord);

			if (current_distance >= last_dist)
				continue;

			//check rearchability

			uint32_t cost = 0;
			bool reachable = PathFinderCore::get()->is_reachable(it->get()->get_creature_died()->get_coordinate_monster(), map_front_t, true, true, cost);
			if ((reachable && cost <= last_found_cost) ||
				current_coord == it->get()->get_creature_died()->get_coordinate_monster()){
				last_dist = current_distance;
				last_creature = *it;
			}
			else if (!reachable && clear_not_rearchable)//eliminate if not rearchable
				creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_unreachable;
		}
	}
	else if (LooterCore::get_current_sequence_mode() == loot_sequence_inteligent){
		for (auto it = LootsCreaturesOnQueue.begin(); it != LootsCreaturesOnQueue.end(); it++){
			int32_t time_died = it->get()->get_died_time()->elapsed_milliseconds();
			if (time_died <= (int32_t)ping_med)
				continue;

			if (!it->get()->get_looted()){
				creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
				continue;
			}

			/*if (LooterCore::get_current_filter_mode() == loot_filter_t::loot_filter_t_from_all){
				creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
				continue;
			}*/

			if (LooterCore::get_current_filter_mode() == loot_filter_t::loot_filter_t_from_target /*&& it->get()->message_assigned*/){
				if (!MatchCreatureFilter(it->get())){
					creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
					continue;
				}
			}

			uint32_t current_distance = current_coord.get_total_distance(
				it->get()->get_creature_died()->get_coordinate_monster());

			if (last_creature) {
				if (current_distance > last_dist)
					continue;
			}
			//TODO current_distance 
			uint32_t cost = 0;
			bool reachable = PathFinderCore::get()->is_reachable(it->get()->get_creature_died()->get_coordinate_monster(), map_front_t, true, true, cost);
			if ((reachable && cost <= last_found_cost) || current_coord == it->get()->get_creature_died()->get_coordinate_monster()){
				last_dist = current_distance;
				last_creature = *it;
			}
			else if (!reachable && clear_not_rearchable)//eliminate if not rearchable
				creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_unreachable;
			//CHECK FAZER ELE ABRIR MSM QND VC ESTIVER EM CIMA
		}
	}
	else if (LooterCore::get_current_sequence_mode() == loot_sequence_after_kill_all){
		if (!HunterCore::get()->need_operate()) {
			for (auto it = LootsCreaturesOnQueue.begin(); it != LootsCreaturesOnQueue.end(); it++){
				int32_t time_died = it->get()->get_died_time()->elapsed_milliseconds();
				if (time_died <= (int32_t)ping_med)
					continue;

				/*if (time_died > 15000){
				creatures_to_remove.push_back(*it);
				continue;
				}*/

				if (!it->get()->get_looted()){
					creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
					continue;
				}

				/*if (LooterCore::get_current_filter_mode() != loot_filter_t::loot_filter_t_from_all){
					creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
					continue;
				}*/

				if (LooterCore::get_current_filter_mode() == loot_filter_t::loot_filter_t_from_target && it->get()->message_assigned){
					if (!MatchCreatureFilter(it->get())){
						creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_not_match;
						continue;
					}
				}

				uint32_t current_distance = current_coord.get_total_distance(
					it->get()->get_creature_died()->get_coordinate_monster());

				if (last_creature) {
					if (current_distance > last_dist)
						continue;
				}
				//TODO current_distance 
				uint32_t cost = 0;
				bool reachable = PathFinderCore::get()->is_reachable(it->get()->get_creature_died()->get_coordinate_monster(), map_front_t, true, true, cost);
				if ((reachable && cost <= last_found_cost) || current_coord == it->get()->get_creature_died()->get_coordinate_monster()){
					last_dist = current_distance;
					last_creature = *it;
				}
				else if (!reachable && clear_not_rearchable)//eliminate if not rearchable
					creatures_to_remove[*it] = loot_queue_remove_reason::remove_reason_unreachable;
			}
		}
	}
	//message_assigned
	if (!last_creature){
		for (auto creature_to_remove : creatures_to_remove){
			auto _found = std::find(LootsCreaturesOnQueue.begin(), LootsCreaturesOnQueue.end(), creature_to_remove.first);
			if (_found != LootsCreaturesOnQueue.end())
				remove_loot_from_loot(creature_to_remove.first, creature_to_remove.second);		

		}
	}

	return last_creature;
}

bool LootListQueue::has_some_loot_ready(){
	return false;
}

uint32_t LooterCore::max_try_loot_count = 5;
uint32_t LooterCore::try_loot_count_warning_message = 2;

bool LooterCore::pick_up_item(){
	uint32_t backpack_id = Inventory::get()->body_bag();
	if (!backpack_id)
		return false;

	ItemContainerPtr main_container = ContainerManager::get()->get_container_by_id(backpack_id);
	if (!main_container)
		return true;

	std::vector<uint32_t> open_containers_id = ContainerManager::get()->get_all_containers_id_opened();
	std::vector<uint32_t> current_containers_items = ContainerManager::get()->get_all_items_is_in_open_containers();
	std::vector<uint32_t> all_items = LooterManager::get()->get_all_items();

	for (auto item_in_loot : all_items){
		bool pick_up = false;

		std::shared_ptr<LooterContainer> owner = LooterManager::get()->get_container_owns_by_item_name(ItemsManager::get()->getItemNameFromId(item_in_loot));
		if (!owner)
			continue;

		LooterItemNonSharedPtr itemShared = LooterManager::get()->get_looter_item_by_item_id(item_in_loot);
		if (!itemShared)
			continue;

		std::vector<std::shared_ptr<LooterConditions>> vector_condition = itemShared->get_vector_condition();

		if (vector_condition.size() <= 0){
			if (itemShared->get_condition() != loot_item_condition_t::loot_from_the_floor_while_cap_more_than)
				continue;

			if (TibiaProcess::get_default()->character_info->cap() < itemShared->get_value())
				continue;

			pick_up = true;
		}


		if (std::find(open_containers_id.begin(), open_containers_id.end(), owner->get_container_id()) != open_containers_id.end()){
			ItemContainerPtr destination_container = ContainerManager::get()->get_container_by_id(owner->get_container_id());

			if (!destination_container)
				continue;

			if (destination_container->is_full())
				if (!destination_container->has_next())
					continue;

			if (!pick_up){
				action_retval_t execute_action = action_retval_t::action_retval_fail;
				for (auto condition : vector_condition){
					if (condition->action != loot_item_action_pick_up)
						continue;

					switch (condition->condition){
					case loot_item_new_condition_t::if_destination_full_cancel:
						execute_action = move_item_mode_if_destination_full_cancel2(destination_container->get_id());
						break;
					case loot_item_new_condition_t::if_cap_bellow_than_cancel:
						execute_action = move_item_mode_if_cap_bellow_than_cancel2(condition->value);
						break;
					case loot_item_new_condition_t::if_have_more_than_cancel:
						execute_action = move_item_mode_if_have_more_than_cancel2(item_in_loot,condition->value);
						break;
					}

					if (execute_action != action_retval_t::action_retval_success)
						continue;

					pick_up = true;
				}
			}
			
			if (!pick_up)
				continue;

			if (HunterCore::get()->need_operate() || var_is_looting || var_need_operate)
				return false;

			Coordinate coordinate = gMapTilesPtr->find_coordinate_with_top_id(item_in_loot, map_front_mini_t, true);
			if (coordinate.is_null())
				continue;

			if (BattleList::get()->get_target())
				return false;

			looted_time.reset();

			var_need_operate = true;
			var_is_looting = true;
		
			for (int try_count = 0; try_count < 3; try_count++){
				if (HunterCore::get()->need_operate() || var_is_looting || var_need_operate)
					return false;

				Actions::pick_up_item(item_in_loot, 0, destination_container->get_id());
				
				MapTiles::wait_refresh();

				Coordinate coordinate = gMapTilesPtr->find_coordinate_with_top_id(item_in_loot, map_front_mini_t, true);
				if (coordinate.is_null())
					break;
			}

			var_need_operate = false;
			var_is_looting = false;
		}
		else if (std::find(current_containers_items.begin(), current_containers_items.end(), owner->get_container_id()) != open_containers_id.end()){
			continue;
		}
	}
	return false;
}

bool LootCreatureOnQueue::match_some_loot(std::map< std::string/*item name*/, uint32_t /*item_count*/ >& items){
	//TODO ESSA FUNCAO SO TA BATENDO PRA ITEMS QUE SAO PEGOS, OS QUE TEM CONDICAO DE DROP ETC AINDA PRECISAM SER FEITO, OU VERIFICACOES DE CAP ETC ETC
	uint32_t backpack_id = Inventory::get()->body_bag();
	if (!backpack_id)
		return false;
	auto main_container = ContainerManager::get()->get_container_by_id(backpack_id);
	if (!main_container)
		return true;

	std::vector<uint32_t> open_containers_id = ContainerManager::get()->get_all_containers_id_opened();
	std::vector<uint32_t> current_containers_items = ContainerManager::get()->get_all_items_is_in_open_containers();
	for (auto item_in_loot : items){
		std::shared_ptr<LooterContainer> owner = LooterManager::get()->get_container_owns_by_item_name((std::string&)item_in_loot.first);
		if (!owner)
			continue;
		if (std::find(open_containers_id.begin(), open_containers_id.end(), owner->get_container_id()) != open_containers_id.end()){
			ItemContainerPtr destination_container = ContainerManager::get()->get_container_by_id(owner->get_container_id());

			/*
			return true, cuzz the somethig went wrong cuzz, this id was in the open container list, and was not found now, it may cause loot
			lost if we ignore this error, the we will return true.
			*/
			if (!destination_container)
				return true;

			/*
			if it is not full then we found
			*/
			if (!destination_container->is_full())
				return true;
			/*
			if it is full and has not next then return false
			*/
			if (destination_container->has_next())
				return true;
		}
		else if (std::find(current_containers_items.begin(), current_containers_items.end(), owner->get_container_id()) !=
			open_containers_id.end()){
			//the bag item exist in some current open container.
			return true;
		}

	}
	return false;

}

bool LootCreatureOnQueue::match_some_loot(){
	return match_some_loot(loot_items);
}

void LooterCore::close_all_non_looter_containers(){
	std::map<std::string /*id*/, std::shared_ptr<LooterContainer>>& looter_containers = LooterManager::get()->containers;
	std::vector<uint32_t> looter_containers_ids;
	/*static const std::function<void(std::pair<std::string, std::shared_ptr<LooterContainer>>& container_in_looter)> function_push_container_id
	=;*/

	std::for_each(looter_containers.begin(), looter_containers.end(),
		[&](std::map<std::string /*id*/, std::shared_ptr<LooterContainer>>::value_type& container_in_looter)->void{
		looter_containers_ids.push_back(container_in_looter.second->get_container_id());
	});

	auto not_our_containers = ContainerManager::get()->get_containers_not_in(looter_containers_ids);
	for (auto not_our_container : not_our_containers){
		not_our_container.second->close();
	}
}

LootCreatureOnQueue::LootCreatureOnQueue(){
	stack_pos = 0;
	timeout_count = 0;
	count_warning_message = 0;
	fail_count = 0;
}

std::map<std::string/*item name*/, uint32_t /*item_count*/> LootCreatureOnQueue::get_loot_items(){
	lock_guard lock(mtx_access);
	return loot_items;
}

int32_t LootCreatureOnQueue::get_stack_pos(){
	lock_guard lock(mtx_access);
	return stack_pos;
}

void LootCreatureOnQueue::set_stack_pos(int32_t stack_pos){
	lock_guard lock(mtx_access);
	this->stack_pos = stack_pos;
}

CreatureOnBattlePtr LootCreatureOnQueue::get_creature_died(){
	lock_guard lock(mtx_access);
	return creature_died;
}

std::string LootCreatureOnQueue::get_loot_message(){
	lock_guard lock(mtx_access);
	return loot_message;
}

void LootCreatureOnQueue::set_loot_message(std::string message){
	lock_guard lock(mtx_access);
	loot_message = message;
}

std::shared_ptr<TimeChronometer> LootCreatureOnQueue::get_died_time(){
	return died_time;
}

void LootCreatureOnQueue::set_died(CreatureOnBattlePtr died_creature){
	set_creature(died_creature);
}

void LootCreatureOnQueue::set_creature(CreatureOnBattlePtr creature){
	lock_guard lock(mtx_access);
	creature_died = creature;
}

void LootCreatureOnQueue::add_timeout_count(){
	timeout_count++;
}

void LootCreatureOnQueue::add_fail_count(){
	fail_count++;
}

void LootCreatureOnQueue::add_fail_move_item_count() {
	fail_move_item_count++;
}

int32_t LootCreatureOnQueue::get_timeout_count(){
	return timeout_count;
}

int32_t LootCreatureOnQueue::get_fail_count(){
	return fail_count;
}

int32_t LootCreatureOnQueue::get_fail_move_item_count() {
	return fail_move_item_count;
}

void LooterCore::update_next_delay_eat_from_corpses(){
	next_delay_eat_from_corpses = LooterManager::get()->get_next_eat_from_corpses_delay();
}

uint32_t LooterCore::elapsed_eat_from_corpses(){
	return last_try_eat_from_corpses.elapsed_milliseconds();
}

void LooterCore::remove_timeout(int timeout){
	std::vector<std::shared_ptr<LootCreatureOnQueue>> creatures = LootListQueue::get()->getLootsCreaturesOnQueueCopy();
	for (auto creature_to_loot : creatures){
		if (!creature_to_loot)
			continue;

		if (creature_to_loot->get_died_time()->elapsed_milliseconds() < timeout)
			continue;
		
		LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_timeout);
		mtx_access.lock();
		var_is_looting = false;
		var_need_operate = false;
		mtx_access.unlock();
		continue;		
	}
}

void LooterCore::remove_min_cap(){
}

void LooterCore::remove_max_distance(int max_distance){
	Coordinate current_coordinate = TibiaProcess::get_default()->character_info->get_self_coordinate();
	std::vector<std::shared_ptr<LootCreatureOnQueue>> creatures = LootListQueue::get()->getLootsCreaturesOnQueueCopy();
	for (auto creature_to_loot : creatures){
		if (!creature_to_loot)
			continue;

		CreatureOnBattlePtr creature_died = creature_to_loot->get_creature_died();
		if (!creature_died)
			continue;

		if (current_coordinate.get_axis_max_dist(creature_died->get_coordinate_monster()) < (uint32_t)max_distance)
			continue;

		LootListQueue::get()->remove_loot_from_loot(creature_to_loot, loot_queue_remove_reason::remove_reason_out_of_range);
		mtx_access.lock();
		var_is_looting = false;
		var_need_operate = false;
		mtx_access.unlock();
	}
}

bool LooterCore::try_eat(){
	if (!LooterManager::get()->get_eat_from_corpses_state())
		return false;

	if (elapsed_eat_from_corpses() < next_delay_eat_from_corpses)
		return false;

	int success_count = 0;
	int count_try = rand() % 3;
	for (int i = 0; i < count_try; i++){
		std::vector<uint32_t> containers_indexes = ContainerManager::get()->get_open_container_indexes();
		std::sort(containers_indexes.begin(), containers_indexes.end(), std::greater<uint32_t>());
		for (uint32_t container_index : containers_indexes){
			if (Actions::get()->eat_food(container_index)){
				success_count++;
				break;
				//Sleep(50);
			}
		}
	}
	if (success_count){
		update_next_delay_eat_from_corpses();
		return true;
	}
	return false;
}

loot_sequence_t LooterCore::get_current_sequence_mode(){
	return LooterManager::get()->get_current_sequence_type();
}

void LooterCore::set_current_filter_mode(loot_filter_t in){
	LooterManager::get()->set_current_filter_type(in);
}

loot_filter_t LooterCore::get_current_filter_mode(){
	return LooterManager::get()->get_current_filter_type();
}

void LooterCore::set_max_try_loot_count(uint32_t in){
	max_try_loot_count = in;
}

uint32_t LooterCore::get_max_try_loot_count(){
	return max_try_loot_count;
}

void LooterCore::set_current_sequence_mode(loot_sequence_t type){
	LooterManager::get()->set_current_sequence_type(type);
	LootListQueue::get()->clear_loot_list();
}

Json::Value LooterCore::parse_class_to_json() {
	Json::Value spellClasse;
	Json::Value repotItemList;

	for (auto it : map_items_looted){
		Json::Value item;

		if (!it.second)
			continue;

		item["body_address"] = it.second->body_address;
		item["container_id"] = it.second->container_id;
		item["item_id"] = it.second->item_id;
		item["name"] = it.second->name;
		item["sell_price"] = it.second->sell_price;
		item["buy_price"] = it.second->buy_price;
				
		repotItemList[std::to_string(it.first)] = item;
	}

	spellClasse["ItemList"] = repotItemList;
	return spellClasse;
}

void LooterCore::parse_json_to_class(Json::Value jsonObject) {
	Json::Value repotItemList = jsonObject["ItemList"];

	if (repotItemList.isNull() || repotItemList.empty())
		return;

	Json::Value::Members members = repotItemList.getMemberNames();

	for (auto member : members){
		Json::Value item = repotItemList[member];
		std::shared_ptr<ItemsInfoLooted> newItem(new ItemsInfoLooted);

		uint32_t item_id = item["item_id"].asInt();
		newItem->item_id = item_id;
		newItem->name = item["name"].asString();
		newItem->sell_price = item["sell_price"].asInt();
		newItem->buy_price = item["buy_price"].asInt();

		map_items_looted[atoi(&member[0])] = newItem;
}
}

void LooterCore::refresh_items_in_map_looted(){
	std::vector<uint32_t> items_for_loot = LooterManager::get()->get_all_items();
	if (!items_for_loot.size())
		return;

	for (auto item : items_for_loot){
		if (item == UINT32_MAX || item <= 0)
			continue;

		auto lootContainer = LooterManager::get()->get_container_owns_by_item_id(item);
		if (!lootContainer)
			continue;

		if (lootContainer->get_container_id() <= 0)
			continue;

		ItemContainerPtr container = ContainerManager::get()->get_container_by_id(lootContainer->get_container_id());
		if (!container)
			continue;

		mtx_access.lock();
		std::shared_ptr<ItemsInfoLooted> itemInfoLooted = map_items_looted[item];
		if (!itemInfoLooted)
			map_items_looted[item] = std::shared_ptr<ItemsInfoLooted>(new ItemsInfoLooted);

		map_items_looted[item]->set_count_temp(container->get_item_count_by_id(item));
		map_items_looted[item]->item_id = item;
		map_items_looted[item]->name = ItemsManager::get()->getItemNameFromId(item);
		map_items_looted[item]->set_body_address(container->header_info->body_address);
		map_items_looted[item]->set_container_id(container->get_id());
		mtx_access.unlock();
	}
}

void LooterCore::refresh_items_looted(){
	for (auto item : map_items_looted){
		uint32_t item_id = item.first;
		std::shared_ptr<ItemsInfoLooted> items_looted = item.second;

		if (!item.second || item_id == UINT32_MAX || item_id <= 0 || !items_looted)
			continue;
	
		ItemContainerPtr container = ContainerManager::get()->get_container_by_id(items_looted->container_id);
		if (!container)
			continue;

		int itemCount = container->get_item_count_by_id(item_id);
		if (itemCount == items_looted->count_temp)
			continue;

		int virtualCount = itemCount - items_looted->count_temp;
		if (virtualCount == 0)
			continue;

		if (virtualCount < 0){
			items_looted->count_temp = itemCount;
			continue;
		}

		int temp = items_looted->count_temp;

		items_looted->count_temp = itemCount;
		items_looted->set_count(virtualCount);
		
		continue;
	}
}

void LooterCore::run_check_looted(){
	while (true){
		Sleep(10);

		if (NeutralManager::get()->time_load.elapsed_milliseconds() < 800)
			continue;

		if (!NeutralManager::get()->get_bot_state())
			continue;

		if (!NeutralManager::get()->get_looter_state())
			continue;
						
		InfoCore::get()->weapon_and_ammunation_wasted();

		if (looted_time.elapsed_milliseconds() > 1000)
			refresh_items_in_map_looted();
		else
			refresh_items_looted();
	}
}

int32_t LootCreatureOnQueue::get_count_first_go_downstairs(){
	return count_first_go_downstairs;
}

int32_t LootCreatureOnQueue::get_count_first_go_upstairs(){
	return count_first_go_upstairs;
}

void LootCreatureOnQueue::add_count_first_go_downstairs(){
	count_first_go_downstairs++;
}

void LootCreatureOnQueue::add_count_first_go_upstairs(){
	count_first_go_upstairs++;
}

int32_t LootCreatureOnQueue::get_count_cannot_use(){
	return count_cannot_use;
}

void LootCreatureOnQueue::add_count_not_owner(){
	count_not_owner++;
}

int32_t LootCreatureOnQueue::get_count_not_owner(){
	return count_not_owner;
}

int32_t LootCreatureOnQueue::get_count_warning_message(){
	return count_warning_message;
}

void LootCreatureOnQueue::add_count_warning_message(){
	count_warning_message++;
}

void LootCreatureOnQueue::add_count_cannot_use(){
	count_cannot_use++;
}

bool LootCreatureOnQueue::get_looted(){
	return looted;
	}

void LootCreatureOnQueue::set_looted(bool in){
	looted = in;
}