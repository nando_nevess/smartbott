#pragma once

namespace NeutralBot {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class HudTest : public System::Windows::Forms::Form{
	public:	HudTest(void){
				InitializeComponent();
	}

	protected:~HudTest(){
				  if (components)
					  delete components;
	}

	protected:
	private: System::Windows::Forms::Timer^  timer1;
	private: Telerik::WinControls::UI::RadListView^  radListView1;
	private: System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 this->components = (gcnew System::ComponentModel::Container());
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn1 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 0",
					 L"name"));
				 Telerik::WinControls::UI::ListViewDetailColumn^  listViewDetailColumn2 = (gcnew Telerik::WinControls::UI::ListViewDetailColumn(L"Column 1",
					 L"count"));
				 this->timer1 = (gcnew System::Windows::Forms::Timer(this->components));
				 this->radListView1 = (gcnew Telerik::WinControls::UI::RadListView());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // timer1
				 // 
				 this->timer1->Enabled = true;
				 this->timer1->Tick += gcnew System::EventHandler(this, &HudTest::timer1_Tick);
				 // 
				 // radListView1
				 // 
				 listViewDetailColumn1->HeaderText = L"name";
				 listViewDetailColumn1->Width = 300;
				 listViewDetailColumn2->HeaderText = L"count";
				 listViewDetailColumn2->Width = 100;
				 this->radListView1->Columns->AddRange(gcnew cli::array< Telerik::WinControls::UI::ListViewDetailColumn^  >(2) {
					 listViewDetailColumn1,
						 listViewDetailColumn2
				 });
				 this->radListView1->ItemSpacing = -1;
				 this->radListView1->Location = System::Drawing::Point(0, 0);
				 this->radListView1->Name = L"radListView1";
				 this->radListView1->Size = System::Drawing::Size(401, 330);
				 this->radListView1->TabIndex = 0;
				 this->radListView1->Text = L"radListView1";
				 this->radListView1->ViewType = Telerik::WinControls::UI::ListViewType::DetailsView;
				 // 
				 // HudTest
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(749, 330);
				 this->Controls->Add(this->radListView1);
				 this->Name = L"HudTest";
				 this->Text = L"HUDLOOTTEST";
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radListView1))->EndInit();
				 this->ResumeLayout(false);

			 }
#pragma endregion
	private: System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e);

	};

}