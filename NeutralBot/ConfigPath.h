#pragma once
#include "ConfigPathManager.h"
#include "ManagedUtil.h"
#include "LanguageManager.h"

namespace NeutralBot {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class ConfigPath : public Telerik::WinControls::UI::RadForm{

	public: ConfigPath(void){
				InitializeComponent();
	}
	protected: ~ConfigPath(){
				   unique = nullptr;
				   if (components)
					   delete components;
	}

	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox2;
	protected:
	private: Telerik::WinControls::UI::RadTextBox^  TextBoxIdPath;
	private: Telerik::WinControls::UI::RadLabel^  radLabel2;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListPropertyPath;
	private: Telerik::WinControls::UI::RadLabel^  radLabel4;
	private: Telerik::WinControls::UI::RadLabel^  radLabel1;
	private: Telerik::WinControls::UI::RadDropDownList^  DropDownListPredefinedPath;
	private: Telerik::WinControls::UI::RadListView^  ListViewPaths;
	private: Telerik::WinControls::UI::RadButton^  ButtonAdd;
	private: Telerik::WinControls::UI::RadButton^  ButtonRemove;
	private: Telerik::WinControls::UI::RadSpinEditor^  SpinEditorWalkDificult;
	private: Telerik::WinControls::UI::RadLabel^  radLabel3;
	private: Telerik::WinControls::UI::RadMenu^  radMenu1;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem1;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem2;
	private: Telerik::WinControls::UI::RadMenuSeparatorItem^  radMenuSeparatorItem1;
	private: Telerik::WinControls::UI::RadMenuItem^  radMenuItem3;
	protected:
	protected:

	protected: static ConfigPath^ unique;
	public: static void ShowUnique(){
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::WaitCursor;
				if (unique == nullptr)
					unique = gcnew ConfigPath();
				System::Windows::Forms::Cursor::Current = System::Windows::Forms::Cursors::Default;
				unique->BringToFront();
				unique->Show();
	}
	public: static void CreatUnique(){
				if (unique == nullptr)
					unique = gcnew ConfigPath();
	}
	public:	static void CloseUnique(){
				if (unique)unique->Close();
	}
	public: static void ReloadForm(){
				unique->ConfigPath_Load(nullptr, nullptr);
	}

	private: Telerik::WinControls::UI::RadGroupBox^  radGroupBox1;
	private: System::Windows::Forms::Label^  label2;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_time_to_wait;
	private: Telerik::WinControls::UI::RadSpinEditor^  spin_near_yellow_coord;
	private: System::Windows::Forms::Label^  label1;
	protected:
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
			 void InitializeComponent(void)
			 {
				 System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(ConfigPath::typeid));
				 this->radGroupBox1 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->label2 = (gcnew System::Windows::Forms::Label());
				 this->spin_time_to_wait = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->spin_near_yellow_coord = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->label1 = (gcnew System::Windows::Forms::Label());
				 this->radGroupBox2 = (gcnew Telerik::WinControls::UI::RadGroupBox());
				 this->SpinEditorWalkDificult = (gcnew Telerik::WinControls::UI::RadSpinEditor());
				 this->TextBoxIdPath = (gcnew Telerik::WinControls::UI::RadTextBox());
				 this->radLabel2 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel3 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->DropDownListPropertyPath = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->radLabel4 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->radLabel1 = (gcnew Telerik::WinControls::UI::RadLabel());
				 this->DropDownListPredefinedPath = (gcnew Telerik::WinControls::UI::RadDropDownList());
				 this->ListViewPaths = (gcnew Telerik::WinControls::UI::RadListView());
				 this->ButtonAdd = (gcnew Telerik::WinControls::UI::RadButton());
				 this->ButtonRemove = (gcnew Telerik::WinControls::UI::RadButton());
				 this->radMenu1 = (gcnew Telerik::WinControls::UI::RadMenu());
				 this->radMenuItem1 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radMenuItem2 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 this->radMenuSeparatorItem1 = (gcnew Telerik::WinControls::UI::RadMenuSeparatorItem());
				 this->radMenuItem3 = (gcnew Telerik::WinControls::UI::RadMenuItem());
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->BeginInit();
				 this->radGroupBox1->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_time_to_wait))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_near_yellow_coord))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->BeginInit();
				 this->radGroupBox2->SuspendLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpinEditorWalkDificult))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->TextBoxIdPath))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListPropertyPath))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListPredefinedPath))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewPaths))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonAdd))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemove))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->BeginInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->BeginInit();
				 this->SuspendLayout();
				 // 
				 // radGroupBox1
				 // 
				 this->radGroupBox1->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox1->Controls->Add(this->label2);
				 this->radGroupBox1->Controls->Add(this->spin_time_to_wait);
				 this->radGroupBox1->Controls->Add(this->spin_near_yellow_coord);
				 this->radGroupBox1->Controls->Add(this->label1);
				 this->radGroupBox1->HeaderText = L"Yellow Points";
				 this->radGroupBox1->Location = System::Drawing::Point(229, 178);
				 this->radGroupBox1->Name = L"radGroupBox1";
				 this->radGroupBox1->Size = System::Drawing::Size(244, 81);
				 this->radGroupBox1->TabIndex = 0;
				 this->radGroupBox1->Text = L"Yellow Points";
				 // 
				 // label2
				 // 
				 this->label2->AutoSize = true;
				 this->label2->Location = System::Drawing::Point(10, 25);
				 this->label2->Name = L"label2";
				 this->label2->Size = System::Drawing::Size(69, 13);
				 this->label2->TabIndex = 3;
				 this->label2->Text = L"Time to wait";
				 // 
				 // spin_time_to_wait
				 // 
				 this->spin_time_to_wait->Location = System::Drawing::Point(98, 21);
				 this->spin_time_to_wait->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000000000, 0, 0, 0 });
				 this->spin_time_to_wait->Name = L"spin_time_to_wait";
				 this->spin_time_to_wait->Size = System::Drawing::Size(134, 20);
				 this->spin_time_to_wait->TabIndex = 2;
				 this->spin_time_to_wait->TabStop = false;
				 this->spin_time_to_wait->ValueChanged += gcnew System::EventHandler(this, &ConfigPath::spin_time_to_wait_ValueChanged);
				 // 
				 // spin_near_yellow_coord
				 // 
				 this->spin_near_yellow_coord->Location = System::Drawing::Point(164, 47);
				 this->spin_near_yellow_coord->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000000000, 0, 0, 0 });
				 this->spin_near_yellow_coord->Name = L"spin_near_yellow_coord";
				 this->spin_near_yellow_coord->Size = System::Drawing::Size(68, 20);
				 this->spin_near_yellow_coord->TabIndex = 1;
				 this->spin_near_yellow_coord->TabStop = false;
				 this->spin_near_yellow_coord->ValueChanged += gcnew System::EventHandler(this, &ConfigPath::spin_near_yellow_coord_ValueChanged);
				 // 
				 // label1
				 // 
				 this->label1->AutoSize = true;
				 this->label1->Location = System::Drawing::Point(10, 49);
				 this->label1->Name = L"label1";
				 this->label1->Size = System::Drawing::Size(148, 13);
				 this->label1->TabIndex = 0;
				 this->label1->Text = L"Consider near yellow coord";
				 // 
				 // radGroupBox2
				 // 
				 this->radGroupBox2->AccessibleRole = System::Windows::Forms::AccessibleRole::Grouping;
				 this->radGroupBox2->Controls->Add(this->SpinEditorWalkDificult);
				 this->radGroupBox2->Controls->Add(this->TextBoxIdPath);
				 this->radGroupBox2->Controls->Add(this->radLabel2);
				 this->radGroupBox2->Controls->Add(this->radLabel3);
				 this->radGroupBox2->Controls->Add(this->DropDownListPropertyPath);
				 this->radGroupBox2->Controls->Add(this->radLabel4);
				 this->radGroupBox2->Controls->Add(this->radLabel1);
				 this->radGroupBox2->Controls->Add(this->DropDownListPredefinedPath);
				 this->radGroupBox2->HeaderText = L"";
				 this->radGroupBox2->Location = System::Drawing::Point(229, 26);
				 this->radGroupBox2->Name = L"radGroupBox2";
				 this->radGroupBox2->Size = System::Drawing::Size(244, 116);
				 this->radGroupBox2->TabIndex = 4;
				 // 
				 // SpinEditorWalkDificult
				 // 
				 this->SpinEditorWalkDificult->Location = System::Drawing::Point(86, 62);
				 this->SpinEditorWalkDificult->Maximum = System::Decimal(gcnew cli::array< System::Int32 >(4) { 1000000000, 0, 0, 0 });
				 this->SpinEditorWalkDificult->Name = L"SpinEditorWalkDificult";
				 this->SpinEditorWalkDificult->Size = System::Drawing::Size(146, 20);
				 this->SpinEditorWalkDificult->TabIndex = 12;
				 this->SpinEditorWalkDificult->TabStop = false;
				 this->SpinEditorWalkDificult->ValueChanged += gcnew System::EventHandler(this, &ConfigPath::SpinEditorWalkDificult_ValueChanged);
				 // 
				 // TextBoxIdPath
				 // 
				 this->TextBoxIdPath->Location = System::Drawing::Point(63, 88);
				 this->TextBoxIdPath->Name = L"TextBoxIdPath";
				 this->TextBoxIdPath->Size = System::Drawing::Size(169, 20);
				 this->TextBoxIdPath->TabIndex = 10;
				 this->TextBoxIdPath->TextChanged += gcnew System::EventHandler(this, &ConfigPath::TextBoxIdPath_TextChanged);
				 this->TextBoxIdPath->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &ConfigPath::TextBoxIdPath_KeyPress);
				 // 
				 // radLabel2
				 // 
				 this->radLabel2->Location = System::Drawing::Point(5, 36);
				 this->radLabel2->Name = L"radLabel2";
				 this->radLabel2->Size = System::Drawing::Size(75, 18);
				 this->radLabel2->TabIndex = 9;
				 this->radLabel2->Text = L"Property path";
				 // 
				 // radLabel3
				 // 
				 this->radLabel3->Location = System::Drawing::Point(5, 63);
				 this->radLabel3->Name = L"radLabel3";
				 this->radLabel3->Size = System::Drawing::Size(68, 18);
				 this->radLabel3->TabIndex = 11;
				 this->radLabel3->Text = L"Walk dificult";
				 // 
				 // DropDownListPropertyPath
				 // 
				 this->DropDownListPropertyPath->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->DropDownListPropertyPath->DropDownStyle = Telerik::WinControls::RadDropDownStyle::DropDownList;
				 this->DropDownListPropertyPath->Location = System::Drawing::Point(86, 36);
				 this->DropDownListPropertyPath->Name = L"DropDownListPropertyPath";
				 this->DropDownListPropertyPath->Size = System::Drawing::Size(146, 20);
				 this->DropDownListPropertyPath->TabIndex = 8;
				 this->DropDownListPropertyPath->SelectedIndexChanged += gcnew Telerik::WinControls::UI::Data::PositionChangedEventHandler(this, &ConfigPath::DropDownListPropertyPath_SelectedIndexChanged);
				 // 
				 // radLabel4
				 // 
				 this->radLabel4->Location = System::Drawing::Point(5, 89);
				 this->radLabel4->Name = L"radLabel4";
				 this->radLabel4->Size = System::Drawing::Size(41, 18);
				 this->radLabel4->TabIndex = 7;
				 this->radLabel4->Text = L"Id Path";
				 // 
				 // radLabel1
				 // 
				 this->radLabel1->Location = System::Drawing::Point(5, 10);
				 this->radLabel1->Name = L"radLabel1";
				 this->radLabel1->Size = System::Drawing::Size(86, 18);
				 this->radLabel1->TabIndex = 4;
				 this->radLabel1->Text = L"Predefined path";
				 // 
				 // DropDownListPredefinedPath
				 // 
				 this->DropDownListPredefinedPath->AutoCompleteMode = System::Windows::Forms::AutoCompleteMode::SuggestAppend;
				 this->DropDownListPredefinedPath->Location = System::Drawing::Point(97, 10);
				 this->DropDownListPredefinedPath->Name = L"DropDownListPredefinedPath";
				 this->DropDownListPredefinedPath->Size = System::Drawing::Size(135, 20);
				 this->DropDownListPredefinedPath->TabIndex = 0;
				 this->DropDownListPredefinedPath->TextChanged += gcnew System::EventHandler(this, &ConfigPath::DropDownListPredefinedPath_TextChanged);
				 // 
				 // ListViewPaths
				 // 
				 this->ListViewPaths->AllowColumnReorder = false;
				 this->ListViewPaths->AllowColumnResize = false;
				 this->ListViewPaths->AllowEdit = false;
				 this->ListViewPaths->AllowRemove = false;
				 this->ListViewPaths->HeaderHeight = 30;
				 this->ListViewPaths->ItemSize = System::Drawing::Size(200, 25);
				 this->ListViewPaths->Location = System::Drawing::Point(7, 26);
				 this->ListViewPaths->Name = L"ListViewPaths";
				 this->ListViewPaths->ShowGridLines = true;
				 this->ListViewPaths->Size = System::Drawing::Size(216, 233);
				 this->ListViewPaths->TabIndex = 3;
				 this->ListViewPaths->SelectedItemChanged += gcnew System::EventHandler(this, &ConfigPath::ListViewPaths_SelectedItemChanged);
				 // 
				 // ButtonAdd
				 // 
				 this->ButtonAdd->Location = System::Drawing::Point(229, 148);
				 this->ButtonAdd->Name = L"ButtonAdd";
				 this->ButtonAdd->Size = System::Drawing::Size(117, 24);
				 this->ButtonAdd->TabIndex = 5;
				 this->ButtonAdd->Text = L"Add";
				 this->ButtonAdd->Click += gcnew System::EventHandler(this, &ConfigPath::ButtonAdd_Click);
				 // 
				 // ButtonRemove
				 // 
				 this->ButtonRemove->Location = System::Drawing::Point(356, 148);
				 this->ButtonRemove->Name = L"ButtonRemove";
				 this->ButtonRemove->Size = System::Drawing::Size(117, 24);
				 this->ButtonRemove->TabIndex = 6;
				 this->ButtonRemove->Text = L"Remove";
				 this->ButtonRemove->Click += gcnew System::EventHandler(this, &ConfigPath::ButtonRemove_Click);
				 // 
				 // radMenu1
				 // 
				 this->radMenu1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(1) { this->radMenuItem1 });
				 this->radMenu1->Location = System::Drawing::Point(0, 0);
				 this->radMenu1->Name = L"radMenu1";
				 this->radMenu1->Size = System::Drawing::Size(479, 20);
				 this->radMenu1->TabIndex = 7;
				 this->radMenu1->Text = L"radMenu1";
				 // 
				 // radMenuItem1
				 // 
				 this->radMenuItem1->AccessibleDescription = L"Menu";
				 this->radMenuItem1->AccessibleName = L"Menu";
				 this->radMenuItem1->Items->AddRange(gcnew cli::array< Telerik::WinControls::RadItem^  >(3) {
					 this->radMenuItem2, this->radMenuSeparatorItem1,
						 this->radMenuItem3
				 });
				 this->radMenuItem1->Name = L"radMenuItem1";
				 this->radMenuItem1->Text = L"Menu";
				 // 
				 // radMenuItem2
				 // 
				 this->radMenuItem2->AccessibleDescription = L"Save";
				 this->radMenuItem2->AccessibleName = L"Save";
				 this->radMenuItem2->Name = L"radMenuItem2";
				 this->radMenuItem2->Text = L"Save";
				 this->radMenuItem2->Click += gcnew System::EventHandler(this, &ConfigPath::radMenuItem2_Click);
				 // 
				 // radMenuSeparatorItem1
				 // 
				 this->radMenuSeparatorItem1->AccessibleDescription = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->AccessibleName = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->Name = L"radMenuSeparatorItem1";
				 this->radMenuSeparatorItem1->Text = L"radMenuSeparatorItem1";
				 // 
				 // radMenuItem3
				 // 
				 this->radMenuItem3->AccessibleDescription = L"Load";
				 this->radMenuItem3->AccessibleName = L"Load";
				 this->radMenuItem3->Name = L"radMenuItem3";
				 this->radMenuItem3->Text = L"Load";
				 this->radMenuItem3->Click += gcnew System::EventHandler(this, &ConfigPath::radMenuItem3_Click);
				 // 
				 // ConfigPath
				 // 
				 this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
				 this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
				 this->ClientSize = System::Drawing::Size(479, 266);
				 this->Controls->Add(this->radMenu1);
				 this->Controls->Add(this->ButtonRemove);
				 this->Controls->Add(this->ButtonAdd);
				 this->Controls->Add(this->radGroupBox2);
				 this->Controls->Add(this->ListViewPaths);
				 this->Controls->Add(this->radGroupBox1);
				 this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
				 this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
				 this->MaximizeBox = false;
				 this->Name = L"ConfigPath";
				 // 
				 // 
				 // 
				 this->RootElement->ApplyShapeToControl = true;
				 this->Text = L"ConfigPath";
				 this->Load += gcnew System::EventHandler(this, &ConfigPath::ConfigPath_Load);
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox1))->EndInit();
				 this->radGroupBox1->ResumeLayout(false);
				 this->radGroupBox1->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_time_to_wait))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->spin_near_yellow_coord))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radGroupBox2))->EndInit();
				 this->radGroupBox2->ResumeLayout(false);
				 this->radGroupBox2->PerformLayout();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->SpinEditorWalkDificult))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->TextBoxIdPath))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel2))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel3))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListPropertyPath))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel4))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radLabel1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->DropDownListPredefinedPath))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ListViewPaths))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonAdd))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->ButtonRemove))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->radMenu1))->EndInit();
				 (cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this))->EndInit();
				 this->ResumeLayout(false);
				 this->PerformLayout();

			 }
#pragma endregion

			 bool disable_update = false;

			 void update_idiom_listbox(){
				 DropDownListPropertyPath->Items->Clear();
				 for (int i = 0; i < property_t::property_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(std::string("property_t_") + std::to_string(i));
					 item->Value = i;
					 DropDownListPropertyPath->Items->Add(item);
				 }
				 DropDownListPropertyPath->SelectedIndex = 0;
			 }

			 void update_idiom(){
				 radLabel4->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel4->Text))[0]);
				 radLabel3->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel3->Text))[0]);
				 radLabel2->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel2->Text))[0]);
				 radLabel1->Text = gcnew String(&GET_TR(managed_util::fromSS(radLabel1->Text))[0]);
				 ButtonAdd->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonAdd->Text))[0]);
				 ButtonRemove->Text = gcnew String(&GET_TR(managed_util::fromSS(ButtonRemove->Text))[0]);
				 radGroupBox1->Text = gcnew String(&GET_TR(managed_util::fromSS(radGroupBox1->Text))[0]);
				 label2->Text = gcnew String(&GET_TR(managed_util::fromSS(label2->Text))[0]);
				 label1->Text = gcnew String(&GET_TR(managed_util::fromSS(label1->Text))[0]);
				 radMenuItem3->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem3->Text))[0]);
				 radMenuItem2->Text = gcnew String(&GET_TR(managed_util::fromSS(radMenuItem2->Text))[0]);
			 }

	private: System::Void ConfigPath_Load(System::Object^  sender, System::EventArgs^  e) {
				 disable_update = true;

				 update_idiom_listbox();
				 update_idiom();

				 spin_time_to_wait->Value = (int)ConfigPathManager::get()->get_time_to_wait();
				 spin_near_yellow_coord->Value = (int)ConfigPathManager::get()->get_consider_near_yellow_coords();

				 DropDownListPropertyPath->Items->Clear();
				 for (int i = 0; i < property_t::property_total; i++){
					 Telerik::WinControls::UI::RadListDataItem^ item = gcnew Telerik::WinControls::UI::RadListDataItem();
					 item->Text = GET_MANAGED_TR(std::string("property_t_") + std::to_string(i));
					 item->Value = i;
					 DropDownListPropertyPath->Items->Add(item);
				 }
				 DropDownListPropertyPath->SelectedIndex = 0;

				 loadPaths();

				 updateItem();

				 disable_update = false;

	}
	private: System::Void spin_time_to_wait_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update)
					 return;

				 ConfigPathManager::get()->set_time_to_wait((uint32_t)spin_time_to_wait->Value);
	}
	private: System::Void spin_near_yellow_coord_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update)
					 return;

				 ConfigPathManager::get()->set_consider_near_yellow_coords((uint32_t)spin_near_yellow_coord->Value);
	}
			 
			 void loadPaths(){
				 std::vector<std::shared_ptr<ConfigPathProperty>> myVector = ConfigPathManager::get()->get_vector_path_property();
				 for (auto it : myVector){
					 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();
					 newItem->Text = gcnew String(it->get_path_name().c_str());

					 ListViewPaths->Items->Add(newItem);
				 }
			 }
			 void updateItem(){
				 if (disable_update)
					 return;

				 int index = ListViewPaths->Items->IndexOf(ListViewPaths->SelectedItem);
				 if (index < 0 || index > (int)ConfigPathManager::get()->get_vector_path_property().size())
					 return;

				 std::shared_ptr<ConfigPathProperty> rulePath = ConfigPathManager::get()->get_rule_path_property_by_index(index);
				 if (!rulePath)
					 return;

				 String^ namePath = gcnew String(rulePath->get_path_name().c_str());
				 int indexPropertyPath = (int)rulePath->get_property_type();
				 String^ idPath = Convert::ToString((int)rulePath->get_path_id());
				 int walkDificult = (int)rulePath->get_walk_dificult();


				 disable_update = true;
				 DropDownListPredefinedPath->Text = namePath;
				 DropDownListPropertyPath->SelectedIndex = indexPropertyPath;
				 TextBoxIdPath->Text = idPath;
				 SpinEditorWalkDificult->Value = walkDificult;
				 disable_update = false;
			 }
			 void addNewItem(){
				 disable_update = true;

				 ConfigPathManager::get()->creat_new_config_path_property();

				 Telerik::WinControls::UI::ListViewDataItem^ newItem = gcnew Telerik::WinControls::UI::ListViewDataItem();

				 newItem->Text = "New";

				 ListViewPaths->Items->Add(newItem);
				 DropDownListPredefinedPath->Text = "New";

				 disable_update = false;

				 updateItem();
			 }

	private: System::Void DropDownListPropertyPath_SelectedIndexChanged(System::Object^  sender, Telerik::WinControls::UI::Data::PositionChangedEventArgs^  e) {
				 if (disable_update || !ListViewPaths->SelectedItem)
					 return;

				 int index = ListViewPaths->Items->IndexOf(ListViewPaths->SelectedItem);
				 std::shared_ptr<ConfigPathProperty> rulePath = ConfigPathManager::get()->get_rule_path_property_by_index(index);
				 if (!rulePath)
					 return;

				 rulePath->set_property_type((property_t)DropDownListPropertyPath->SelectedIndex);
	}
	private: System::Void DropDownListPredefinedPath_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (!ListViewPaths->SelectedItem)
					 return;

				 if (disable_update || !ListViewPaths->SelectedItem)
					 return;

				 int index = ListViewPaths->Items->IndexOf(ListViewPaths->SelectedItem);
				 std::shared_ptr<ConfigPathProperty> rulePath = ConfigPathManager::get()->get_rule_path_property_by_index(index);
				 if (!rulePath)
					 return;

				 ListViewPaths->SelectedItem->Text = DropDownListPredefinedPath->Text;
				 rulePath->set_path_name(managed_util::fromSS(DropDownListPredefinedPath->Text));
	}

	private: System::Void TextBoxIdPath_TextChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update || !ListViewPaths->SelectedItem)
					 return;

				 int index = ListViewPaths->Items->IndexOf(ListViewPaths->SelectedItem);
				 std::shared_ptr<ConfigPathProperty> rulePath = ConfigPathManager::get()->get_rule_path_property_by_index(index);
				 if (!rulePath)
					 return;

				 rulePath->set_path_id(Convert::ToInt32(TextBoxIdPath->Text));
	}
	private: System::Void TextBoxIdPath_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
				 if (!Char::IsDigit(e->KeyChar))
					 e->Handled = true;
	}

	private: System::Void ListViewPaths_SelectedItemChanged(System::Object^  sender, System::EventArgs^  e) {
				 updateItem();
	}

	private: System::Void ButtonAdd_Click(System::Object^  sender, System::EventArgs^  e) {
				 addNewItem();
	}
	private: System::Void ButtonRemove_Click(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update || !ListViewPaths->SelectedItem)
					 return;

				 int index = ListViewPaths->Items->IndexOf(ListViewPaths->SelectedItem);

				 ConfigPathManager::get()->remove_to_vector_path(index);
				 ListViewPaths->Items->Remove(ListViewPaths->SelectedItem);
	}
	private: System::Void SpinEditorWalkDificult_ValueChanged(System::Object^  sender, System::EventArgs^  e) {
				 if (disable_update || !ListViewPaths->SelectedItem)
					 return;

				 int index = ListViewPaths->Items->IndexOf(ListViewPaths->SelectedItem);
				 std::shared_ptr<ConfigPathProperty> rulePath = ConfigPathManager::get()->get_rule_path_property_by_index(index);
				 if (!rulePath)
					 return;

				 rulePath->set_walk_dificult((uint32_t)SpinEditorWalkDificult->Value);
	}

			 bool save_script(std::string file_name){
				 Json::Value file;
				 file["version"] = 100;

				 file["ConfigPathManager"] = ConfigPathManager::get()->parse_class_to_json();

				 return saveJsonFile(file_name, file);
			 }
			 bool load_script(std::string file_name){
				 Json::Value file = loadJsonFile(file_name);
				 if (file.isNull())
					 return false;

				 ConfigPathManager::get()->parse_json_to_class(file["ConfigPathManager"]);
				 return true;
			 }

	private: System::Void radMenuItem2_Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::SaveFileDialog fileDialog;
				 fileDialog.Filter = "ConfigPath|*.ConfigPath";
				 fileDialog.Title = "Save ConfigPath";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 if (!save_script(managed_util::fromSS(fileDialog.FileName)))
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Saved.", "Save/Load");
				 else
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Saved.", "Save/Load");

	}
	private: System::Void radMenuItem3_Click(System::Object^  sender, System::EventArgs^  e) {
				 System::Windows::Forms::OpenFileDialog fileDialog;
				 fileDialog.Filter = "ConfigPath|*.ConfigPath";
				 fileDialog.Title = "Open ConfigPath";

				 if (fileDialog.ShowDialog() == System::Windows::Forms::DialogResult::Cancel)
					 return;

				 ConfigPathManager::get()->clear();
				 ListViewPaths->Items->Clear();

				 if (!load_script(managed_util::fromSS(fileDialog.FileName)))
					 Telerik::WinControls::RadMessageBox::Show(this, "Error Loaded.", "Save/Load");
				 else
					 Telerik::WinControls::RadMessageBox::Show(this, "Sucess Loaded.", "Save/Load");

				 updateItem();
	}
	};
}
