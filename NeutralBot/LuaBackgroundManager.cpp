#pragma once
#include "LuaBackgroundManager.h"
#include "HudManager.h"
#include <regex>
#include "LuaThread.h"


LuaBackgroundCodes::LuaBackgroundCodes(){
	InitCode = "--function inputevents(e) \n		--Notify.print_console('seu codigo')\n--end";
	name = "None";
	code = "None";
	refill_name = "None";
	itemname_orspellname = "None";
	active = false;
	thread_on = false;
	_is_hud = false;
	delay = 1000;
	hast_code = 0;
	mana_use = 0;
}

LuaBackgroundManager::LuaBackgroundManager(){
	hud_count_active = 1;
}

uint32_t LuaBackgroundManager::get_hud_count_active(){
	lock_guard _lock(mtx_acess);
	return hud_count_active;
}

void LuaBackgroundManager::increase_active_huds(){
	lock_guard _lock(mtx_acess);
	hud_count_active++;
	HUDManager::get_default()->set_enabled(hud_count_active> 0);
}

void LuaBackgroundManager::decrease_active_huds(){
	lock_guard _lock(mtx_acess);
	hud_count_active--;
	HUDManager::get_default()->set_enabled(hud_count_active> 0);
}

std::map<std::string, std::shared_ptr<LuaBackgroundCodes>> LuaBackgroundManager::getMap(){
	lock_guard _lock(mtx_acess);
	return mapBgLua;
};

std::string LuaBackgroundManager::getLuaInitCode(std::string current_id){
	auto it = mapBgLua.find(current_id);
	if (it == mapBgLua.end())
		return 0;
	return it->second->get_InitCode();
}

std::string LuaBackgroundManager::getLuaCode(std::string current_id){
	auto it = mapBgLua.find(current_id);
	if (it == mapBgLua.end())
		return 0;
	return it->second->get_code();
}

bool LuaBackgroundManager::changeName(std::string current_id, std::string newId){
	auto it = mapBgLua.find(current_id);

	if (it == mapBgLua.end())
		return false;

	mapBgLua[current_id]->set_name(newId);
	return true;
}

bool LuaBackgroundManager::removeLuaCode(std::string id_){
	lock_guard _lock(mtx_acess);
	auto it = mapBgLua.find(id_);

	if (it == mapBgLua.end())
		return false;

	mapBgLua.erase(id_);
	return true;
}

bool LuaBackgroundManager::updateActive(std::string id, bool ative){
	auto it = mapBgLua.find(id);
	if (it == mapBgLua.end())
		return false;

	mapBgLua[id]->set_enabled(ative);

	mtx_acess.lock();
	if (mapBgLua[id]->is_hud() && ative)
		hud_count_active = hud_count_active + 1;
	else if (mapBgLua[id]->is_hud() && !ative){
		hud_count_active = hud_count_active - 1;
		LuaThread::get()->clear_hud_vector();
	}
	mtx_acess.unlock();

	return true;
}

bool LuaBackgroundManager::updateCode(std::string id, std::string code){
	auto it = mapBgLua.find(id);

	if (it == mapBgLua.end())
		return false;

	mapBgLua[id]->set_code(code); 
	mapBgLua[id]->reset_last_update();
	return true;
}

bool LuaBackgroundManager::updateInitCode(std::string id, std::string code){
	lock_guard _lock(mtx_acess);
	auto it = mapBgLua.find(id);

	if (it == mapBgLua.end())
		return false;

	uint32_t hast_code = (uint32_t)&code;
	std::string temp_string = code;

	std::regex reg("inputevents");
	temp_string = std::regex_replace(temp_string, reg, "inputevents" + std::to_string(hast_code));

	mapBgLua[id]->set_InitCode(code);
	mapBgLua[id]->reset_last_update();
	mapBgLua[id]->set_hast_code(hast_code);
	return true;
}

bool LuaBackgroundManager::updateDelay(std::string id, uint32_t delay){
	auto it = mapBgLua.find(id);

	if (it == mapBgLua.end())
		return false;

	mapBgLua[id]->set_delay( delay );
	return true;
}

std::string LuaBackgroundManager::request_new_id(bool is_hud){
	int id = 0;
	std::string current_id = "Lua" + (is_hud ? std::string("Hud") : std::string("")) + "Script" + std::to_string(id);
	while (mapBgLua.find(current_id) != mapBgLua.end()){
		id++;
		current_id = "Lua" + (is_hud ? std::string("Hud") : std::string("")) + "Script" + std::to_string(id);
	}

	std::shared_ptr<LuaBackgroundCodes> new_code(new LuaBackgroundCodes);
	mapBgLua[current_id] = new_code;
	new_code->set_name( current_id );
	new_code->set_enabled(false);
	new_code->set_thread_on(false);
	new_code->set_code("");
	new_code->set_InitCode("--function inputevents(e) \n		--Notify.print_console('seu codigo')\n--end");
	new_code->set_delay( 1000 );

	if (is_hud)
		new_code->set_as_hud();

	return current_id;
}

void LuaBackgroundCodes::set_enabled(bool enabled){
	mtx_access.lock();
	if (enabled == active){
		mtx_access.unlock();
		return;
	}
		
	enabled ? LuaBackgroundManager::get()->increase_active_huds() :
		LuaBackgroundManager::get()->decrease_active_huds();
	
	active = enabled;
	mtx_access.unlock();
}

Json::Value LuaBackgroundCodes::parse_class_to_json(){
	Json::Value LuaBackgroundItem;	
	LuaBackgroundItem["InitCode"] = InitCode;
	LuaBackgroundItem["name"] = name;
	LuaBackgroundItem["itemname_orspellname"] = itemname_orspellname;
	LuaBackgroundItem["mana_use"] = mana_use;
	LuaBackgroundItem["code"] = code;
	LuaBackgroundItem["active"] = active;
	LuaBackgroundItem["delay"] = delay;
	LuaBackgroundItem["is_hud"] = _is_hud;

	return LuaBackgroundItem;
}

void LuaBackgroundCodes::parse_json_to_class(Json::Value& jsonObject){
	Json::Value LuaBackgroundItem;

	if (!jsonObject["InitCode"].isNull() || !jsonObject["InitCode"].empty())
		InitCode = jsonObject["InitCode"].asString();
		
	if (!jsonObject["name"].isNull() || !jsonObject["name"].empty())
		name = jsonObject["name"].asString();

	if (!jsonObject["code"].isNull() || !jsonObject["code"].empty())
		code = jsonObject["code"].asString();

	if (!jsonObject["active"].isNull() || !jsonObject["active"].empty())
		active = jsonObject["active"].asBool();

	if (!jsonObject["delay"].isNull() || !jsonObject["delay"].empty())
		delay = jsonObject["delay"].asUInt();
		
	if (!jsonObject["itemname_orspellname"].isNull() || !jsonObject["itemname_orspellname"].empty())
		itemname_orspellname = jsonObject["itemname_orspellname"].asString();

	if (!jsonObject["mana_use"].isNull() || !jsonObject["mana_use"].empty())
		mana_use = jsonObject["mana_use"].asInt();
	
	if (jsonObject["is_hud"].isNull() || jsonObject["is_hud"].empty())
		_is_hud = false;
	else
		_is_hud = jsonObject["is_hud"].asBool();
}

Json::Value LuaBackgroundManager::parse_class_to_json() {
	Json::Value luaBackground;
	Json::Value luaBackgroundList;

	for (auto it : mapBgLua)
		luaBackgroundList[it.first] = it.second->parse_class_to_json();

	luaBackground["luaBackgroundList"] = luaBackgroundList;
	return luaBackground;
}

void LuaBackgroundManager::parse_json_to_class(Json::Value jsonObject) {
	lock_guard _lock(mtx_acess);
	clear();

	if (!jsonObject["luaBackgroundList"].empty() || !jsonObject["luaBackgroundList"].isNull()){
		Json::Value luaBackgroundList = jsonObject["luaBackgroundList"];
		Json::Value::Members members = luaBackgroundList.getMemberNames();

		for (auto it : members) {
			Json::Value LuaBackgroundItem = luaBackgroundList[it];
			std::shared_ptr<LuaBackgroundCodes> LuaBackground(new LuaBackgroundCodes);
			LuaBackground->parse_json_to_class(luaBackgroundList[it]);
			mapBgLua[it] = LuaBackground;
		}
	}
}

bool LuaBackgroundManager::get_hud_state(){
	lock_guard _lock(mtx_acess);
	return hud_state;
}

void LuaBackgroundManager::set_hud_state(bool state){
	hud_state = state;
}



bool LuaBackgroundCodes::need_execute(){
	if (timer.elapsed_milliseconds() > (int)delay)
		return true;
	return false;
}

int LuaBackgroundCodes::get_last_init_elapsed(){
	mtx_access.lock();
	int temp = last_init.elapsed_milliseconds();
	mtx_access.unlock();
	return temp;
}
int LuaBackgroundCodes::get_last_update_elapsed(){
	mtx_access.lock();
	int temp = last_update.elapsed_milliseconds();
	mtx_access.unlock();
	return temp;
}
int LuaBackgroundCodes::get_timer_elapsed(){
	mtx_access.lock();
	int temp = timer.elapsed_milliseconds();
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::reset_last_init(){
	last_update.reset();
}
void LuaBackgroundCodes::reset_last_update(){
	last_update.reset();
}
void LuaBackgroundCodes::reset_timer(){
	last_update.reset();
}

uint32_t LuaBackgroundCodes::get_next_elapse_execution(){
	return delay - timer.elapsed_milliseconds();
}
uint32_t LuaBackgroundCodes::get_hast_code(){
	mtx_access.lock();
	uint32_t temp = hast_code;
	mtx_access.unlock();
	return temp;
}
void LuaBackgroundCodes::set_hast_code(uint32_t hast_code){
	mtx_access.lock();
	this->hast_code = hast_code;
	mtx_access.unlock();
}
uint32_t LuaBackgroundCodes::get_delay(){
	mtx_access.lock();
	uint32_t temp = delay;
	mtx_access.unlock();
	return temp;
}

uint32_t LuaBackgroundCodes::get_mana_use(){
	mtx_access.lock();
	uint32_t temp = mana_use;
	mtx_access.unlock();
	return temp;
}
void LuaBackgroundCodes::set_delay(uint32_t delay){
	mtx_access.unlock();
	this->delay = delay;
	mtx_access.unlock();
}

void LuaBackgroundCodes::set_mana_use(uint32_t delay){
	mtx_access.unlock();
	this->mana_use = delay;
	mtx_access.unlock();
}

bool LuaBackgroundCodes::get_enabled(){
	mtx_access.lock();
	bool temp = active;
	mtx_access.unlock();
	return temp;
}

bool LuaBackgroundCodes::get_thread_on(){
	mtx_access.lock();
	bool temp = thread_on;
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::set_thread_on(bool in){
	mtx_access.lock();
	this->thread_on = in;
	mtx_access.unlock();
}

bool LuaBackgroundCodes::is_running(){
	mtx_access.lock();
	bool temp = thread_on;
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::set_name(std::string name){
	mtx_access.lock();
	this->name = name;
	mtx_access.unlock();
}

std::string LuaBackgroundCodes::get_name(){
	mtx_access.lock();
	std::string temp = name;
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::set_refill_name(std::string code){
	mtx_access.lock();
	this->refill_name = code;
	mtx_access.unlock();
}
void LuaBackgroundCodes::set_code(std::string code){
	mtx_access.lock();
	this->code = code;
	mtx_access.unlock();
}

std::string LuaBackgroundCodes::get_InitCode(){
	mtx_access.lock();
	std::string temp = InitCode;
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::set_InitCode(std::string InitCode){
	mtx_access.lock();
	this->InitCode = InitCode;
	mtx_access.unlock();
}

std::string LuaBackgroundCodes::get_code(){
	//mtx_access.lock();
	std::string temp = code;
	//mtx_access.unlock();
	return temp;
}

bool LuaBackgroundCodes::is_hud(){
	mtx_access.lock();
	bool temp = _is_hud;
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::set_as_hud(){
	mtx_access.lock();
	_is_hud = true;
	mtx_access.unlock();
}

bool LuaBackgroundCodes::get_active(){
	mtx_access.lock();
	bool temp = active;
	mtx_access.unlock();
	return temp;
}

std::string LuaBackgroundCodes::get_itemname_orspellname(){
	mtx_access.lock();
	std::string temp = itemname_orspellname;
	mtx_access.unlock();
	return temp;
}

void LuaBackgroundCodes::set_itemname_orspellname(std::string& in_itemname_orspellname){
	mtx_access.lock();
	itemname_orspellname = in_itemname_orspellname;
	mtx_access.unlock();
}




std::shared_ptr<LuaBackgroundCodes> LuaBackgroundManager::getLuaScriptByName(std::string name){
	auto retval = mapBgLua.find(name);
	if (retval == mapBgLua.end())
		return nullptr;
	return retval->second;
}

void LuaBackgroundManager::setLuaScriptDelay(std::string name, uint32_t delay){
	auto script = getLuaScriptByName(name);
	if (script)
		return script->set_delay(delay);
}
bool LuaBackgroundManager::getLuaScriptActive(std::string name){
	auto script = getLuaScriptByName(name);
	if (script)
		return script->get_active();

	return false;
}

void LuaBackgroundManager::setLuaScriptCode(std::string name, std::string code){
	auto script = getLuaScriptByName(name);
	if (script)
		return script->set_code(code);
}

uint32_t LuaBackgroundManager::getLuaScriptDelay(std::string name){
	auto script = getLuaScriptByName(name);
	if (script)
		return script->get_delay();
	return 0;
}



LuaBackgroundManager* LuaBackgroundManager::get(){
	static LuaBackgroundManager* m = nullptr;
	if (!m)
		m = new LuaBackgroundManager();
	return m;
}

void LuaBackgroundManager::clear(){
	mapLuaTemporaryEvents.clear();
	mapBgLua.clear();
}

bool LuaBackgroundManager::request_hud_redraw(){
	new_hud_redraw = true;
	return new_hud_redraw;
}


std::shared_ptr<LuaBackgroundCodes> LuaBackgroundManager::get_elapsed_time_event(){
	lock_guard _scope_lock(mtx_acess);
	TimeChronometer timer;
	uint32_t elapsed_milli = timer.elapsed_milliseconds();
	for (auto lua_event = mapLuaTemporaryEvents.begin(); lua_event != mapLuaTemporaryEvents.end(); lua_event++){
		if (elapsed_milli >= lua_event->second.get()->get_delay()){
			std::shared_ptr<LuaBackgroundCodes> retval = lua_event->second;
			mapLuaTemporaryEvents.erase(lua_event);
			temporary_hud_count--;
			return retval;
		}
	}
	return nullptr;
}

void LuaBackgroundManager::schedule_event(std::string event_id, uint32_t time_from_now, std::string _event, bool hud_type){
	mtx_acess.lock();
	for (auto lua_event = mapLuaTemporaryEvents.begin(); lua_event != mapLuaTemporaryEvents.end(); lua_event++){
		if (lua_event->first == event_id){
			lua_event->second->set_code(_event);
			
			if (hud_type)
				lua_event->second->set_as_hud();

			lua_event->second->reset_timer();
			lua_event->second->set_delay(time_from_now);
			mtx_acess.unlock();
			return;
		}
	}
	temporary_hud_count++;
	std::shared_ptr<LuaBackgroundCodes> new_event(new LuaBackgroundCodes);
	new_event->set_delay(time_from_now);
	new_event->set_code(_event);
	
	if (hud_type)
		new_event->set_as_hud();

	mapLuaTemporaryEvents[event_id] = new_event;
	mtx_acess.unlock();
}

void LuaBackgroundManager::cancel_event(std::string event_id){
	mtx_acess.lock();
	for (auto lua_event = mapLuaTemporaryEvents.begin(); lua_event != mapLuaTemporaryEvents.end(); lua_event++){
		if (lua_event->first == event_id){
			mapLuaTemporaryEvents.erase(lua_event);
			temporary_hud_count--;
			mtx_acess.unlock();
			return;
		}
	}
	mtx_acess.unlock();
}