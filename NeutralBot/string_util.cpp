#include "string_util.h"
#include <iostream>

namespace string_util{
	// trim from start
	std::string &ltrim(std::string &s) {
		if (!s.size())
			return s;
		auto begin = s.begin();
		
		while (*begin == ' ' || *begin == '\n' || *begin == '\t' || *begin == 0xd || *begin == 0xa){
			s.pop_back();
			begin = s.begin();
			if (!s.size())
				return s;
		}
		return s;
	}

	// trim from endyu
	std::string &rtrim(std::string &s) {
		if (!s.size())
			return s;

		auto begin = s.rbegin();
		while (*begin == ' ' || *begin == '\n' || *begin == '\t' || *begin == 0xd || *begin == 0xa){
			s.pop_back();
			begin = s.rbegin();
			if (!s.size())
				return s;
		}
		return s;
	}

	// trim from both ends
	std::string &trim(std::string &s) {
		return ltrim(rtrim(s));
	}

	std::string trim2(std::string& str)
	{
		size_t first = str.find_first_not_of(' ');
		size_t last = str.find_last_not_of(' ');
		return str.substr(first, (last - first + 1));
	}

	std::string &lower(std::string &s){
		std::transform(s.begin(), s.end(), s.begin(), ::tolower);
		return s;
	}

	std::string create_secure_string(char *entrace){
		char buffer_secure_string[1024];
		int address = (int)entrace;
		if (address == 0)
			return "";

		char _buffer = -1;
		int i = 0;

		while (_buffer != 0){
			_buffer = *(char*)(address + i);
			if (i > 1022)
				_buffer = 0;

			buffer_secure_string[i] = _buffer;
			i++;
		}

		return std::string(&buffer_secure_string[0]);
	}

	std::string create_secure_string(char &entrace){
		char buffer_secure_string[1024];
		int address = (int)&entrace;
		if (address == 0)
			return "";

		char _buffer = -1;
		int i = 0;
		while (_buffer != 0){
			_buffer = *(char*)(address + i);
			if (i > 1022)
				_buffer = 0;

			buffer_secure_string[i] = _buffer;
			i++;
		}
		return std::string(&buffer_secure_string[0]);
	}
	
	bool string_compare(std::string str1, std::string str2, bool ignore_case){
		if (!ignore_case)
			return strcmp(&str1[0], &str2[0]) == 0;
		return _stricmp(&str1[0], &str2[0]) == 0;
	}
}
