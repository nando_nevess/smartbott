#include "GeneralManager.h"

std::vector<std::string> GeneralManager::get_recent_scripts(){
	return recent_scripts;
}

void GeneralManager::add_recent_scripts(std::string recent){
	auto temp = std::find(recent_scripts.begin(), recent_scripts.end(), recent);
	if (temp != recent_scripts.end())
		return;

	if (recent_scripts.size() < 4)
		recent_scripts.push_back(recent);
	else{
		recent_scripts.push_back(recent);
		recent_scripts.erase(recent_scripts.begin());
	}
}
void GeneralManager::remove_recent_scritps(std::string value){
	auto temp = std::find(recent_scripts.begin(), recent_scripts.end(), value);

	if (temp != recent_scripts.end())
		recent_scripts.erase(temp);
}

void GeneralManager::set_soundPath(std::string pathin){
	soundPath = pathin;
}

void GeneralManager::set_themaName(std::string themaNamein){
	themaName = themaNamein;
}

void GeneralManager::set_currentLang(std::string currentLangin){
	currentLang = currentLangin;
}
std::string GeneralManager::get_currentLang(){
	return currentLang;
}


std::string GeneralManager::get_soundPath(){
	return soundPath;
}

std::string GeneralManager::get_themaName(){
	return themaName;
}

Json::Value GeneralManager::parse_class_to_json(){
	Json::Value retval;
	Json::Value RecentScriptsList;
	Json::Value SoundScriptsList;
	Json::Value VectorSoundScriptsList;

	for (auto repot : recent_scripts)
		RecentScriptsList.append(repot);

	for (auto repot : vector_all_sound)
		VectorSoundScriptsList.append(repot);
	
	for (auto alert : map_sound)
		SoundScriptsList[std::to_string(alert.first)] = alert.second;

	retval["recent_scripts_list"] = RecentScriptsList;
	retval["SoundScriptsList"] = SoundScriptsList;
	retval["VectorSoundScriptsList"] = VectorSoundScriptsList;
	retval["soundPath"] = soundPath;
	retval["themaName"] = themaName;
	retval["currentLang"] = currentLang;
	retval["dev_mode"] = dev_mode;
	retval["practice_mode"] = practice_mode; 

	retval["auto_save_sec"] = auto_save_sec;
	retval["interface_most_top"] = interface_most_top;
	retval["interface_auto_resize"] = interface_auto_resize;
	retval["interface_shortcut_panel"] = interface_shortcut_panel;

	return retval;
}

void GeneralManager::parse_json_to_class(Json::Value jsonObject) {
	if (!jsonObject["SoundScriptsList"].empty() || !jsonObject["SoundScriptsList"].isNull()){
		Json::Value SoundScriptsList = jsonObject["SoundScriptsList"];

		for (auto it : SoundScriptsList)
			add_vector_all_sound(it.asString());
	}
	
	if (!jsonObject["SoundScriptsList"].empty() || !jsonObject["SoundScriptsList"].isNull()){
		Json::Value SoundScriptsList = jsonObject["SoundScriptsList"];

		Json::Value::Members members = SoundScriptsList.getMemberNames();
		for (auto member : members)
			map_sound[(alert_t)atoi(&member[0])] = SoundScriptsList[member].asString();		
	}

	Json::Value recent_scripts_list = jsonObject["recent_scripts_list"];
	for (auto it : recent_scripts_list)
		recent_scripts.push_back(it.asString());

	dev_mode = jsonObject["dev_mode"].asBool();
	soundPath = jsonObject["soundPath"].asString();
	themaName = jsonObject["themaName"].asString();
	currentLang = jsonObject["currentLang"].asString();

	if (jsonObject["auto_save_sec"].asInt() <= 60000)
		auto_save_sec = 60000;
	else
		auto_save_sec = jsonObject["auto_save_sec"].asInt();

	interface_most_top = jsonObject["interface_most_top"].asBool();
	interface_auto_resize = jsonObject["interface_auto_resize"].asBool();
	interface_shortcut_panel = jsonObject["interface_shortcut_panel"].asBool();
	practice_mode = jsonObject["practice_mode"].asBool();

}

GeneralManager* GeneralManager::get(){
	static GeneralManager* m = nullptr;
	if (!m)
		m = new GeneralManager();
	return m;
}

void GeneralManager::set_interface_most_top(bool state){
	interface_most_top = state;
}

void GeneralManager::set_interface_auto_resize(bool state){
	interface_auto_resize = state;
}

void GeneralManager::set_interface_shortcut(bool state){
	interface_shortcut_panel = state;
}

bool GeneralManager::get_interface_shortcut(){
	return interface_shortcut_panel;
}

bool GeneralManager::get_interface_auto_resize(){
	return interface_auto_resize;
}

bool GeneralManager::get_interface_most_top(){
	return interface_most_top;
}

GeneralManager::GeneralManager(){
	auto_save_sec = 60000;
	soundPath = DEFAULT_ALARM_PATH;
	themaName = DEFAULT_THEME;
}
