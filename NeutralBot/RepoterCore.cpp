#pragma once
#include "Core\RepoterCore.h"
#include "Core\ItemsManager.h"
#include "LooterManager.h"
#include "Core\LooterCore.h"
#include "RepoterManager.h"
#include "Core\NpcTrade.h"
#include "Core\Interface.h"

bool RepoterCore::open_trade(std::vector<std::string> words){
	if (!words.size())
		words = std::vector < std::string > {"hi", "trade"};

	if (NpcTrade::get()->is_open())
		return true;

	Inventory::get()->minimize();

	if (RepoterManager::get()->get_minimize_all_containers())
		ContainerManager::get()->minimize_all_container();
	
	for (int trying_loop = 0; trying_loop <= 3; trying_loop++){		
		uint32_t index_count = 0;

		for (auto it : words){
			if (NpcTrade::get()->is_open())
				return true;

			Actions::get()->says(it, "NPCs");

			if (index_count == 0){
				uint32_t delay = std::min(
					TibiaProcess::get_default()->get_action_wait_delay(8.0f), (uint32_t)3000);
					Sleep(delay);
			}
			else if (index_count == words.size() - 1){
				if (ContainerManager::get()->wait_open_trade(2000))

			return true;
			}
			else
				TibiaProcess::get_default()->wait_ping_delay(4.0f);			
		}	
	}
	return false;
}

bool RepoterCore::buy_item_repot_id(std::string repotId, int timeout){
	std::shared_ptr<RepotId> ruleItens = RepoterManager::get()->getRepoIdRule(string_util::lower(repotId));
	if (!ruleItens)
		return false;

	std::map<std::string, std::shared_ptr<RepotItens>> mapItens = ruleItens->getMapRepotItens();
	if (!mapItens.size())
		return false;
	
	Actions::get()->reopen_containers(false);

	if (!open_trade(std::vector<std::string>()))
		return false;

	int item_buy_sucess = 0;
	for (auto itens = mapItens.begin(); itens != mapItens.end(); itens++){
		if (itens->second->get_item_id() == 0xffffffff)
			continue;

		int total_try_count = 0;
		while (total_try_count <= Actions::Max_Count) {

			uint32_t count_item = ContainerManager::get()->get_item_count(itens->second->get_item_id());
			if (count_item >= itens->second->max){
				item_buy_sucess++;
				break;
			}

			int32_t buy_qty = (int32_t)itens->second->max - (int32_t)ContainerManager::get()->get_item_count(itens->second->get_item_id());
			if (buy_qty <= 0)
				continue;

			NpcTrade::get()->buy_item(itens->second->get_item_id(), buy_qty);

			TibiaProcess::get_default()->wait_ping_delay(2.0);
			total_try_count++;
		}
	}

	ContainerManager::get()->reorganize_container();

	if (item_buy_sucess == mapItens.size())
		return true;

	return false;
}

int RepoterCore::total_money_all_repot(){
	int total_money = 0;
	auto mapRepoid = RepoterManager::get()->getMapRepotId();
	
	if (!mapRepoid.size())
		return 0;

	for (auto it : mapRepoid)
		total_money += total_money_repot(it.first);
	
	return total_money;
}

int RepoterCore::total_money_repot(std::string id){
	int count_money = 0;
	std::shared_ptr<RepotId> mapId = RepoterManager::get()->getRepoIdRule(id);
	if (!mapId)
		return 0;

	auto mapItens = mapId->getMapRepotItens();
	for (auto it : mapItens){
		int id_Item = ItemsManager::get()->getitem_idFromName(it.second->itemName);
		int item_count_backpack = ContainerManager::get()->get_item_count(id_Item);
		if (item_count_backpack >= (int32_t)it.second->get_max())
			continue;

		count_money += (it.second->max - item_count_backpack) * it.second->gpeach;
	}
	return count_money;
}

bool RepoterCore::is_necessary_all_repot(){
	auto mapRepoid = RepoterManager::get()->getMapRepotId();
	if (mapRepoid.size() <= 0)
		return false;

	Actions::reopen_containers();
	for (auto it = mapRepoid.begin(); it != mapRepoid.end(); it++){
		if (is_necessary_repot(it->first))
			return true;
	}
	return false;
}

bool RepoterCore::is_necessary_repot(std::string id){
	std::shared_ptr<RepotId> mapId = RepoterManager::get()->getRepoIdRule(string_util::lower(id));
	if (!mapId)
		return false;
	
	auto mapItens = mapId->getMapRepotItens();

	if (mapItens.size() <= 0)
		return false;

	for (auto it = mapItens.begin(); it != mapItens.end(); it++){
		int count_item = 0;
		int id_Item = ItemsManager::get()->getitem_idFromName(it->second->itemName);
		
		count_item = ContainerManager::get()->get_item_count(id_Item);

		if (Inventory::get()->ammunation_count() > 1)
			if (Inventory::get()->body_rope() == id_Item)
				count_item = count_item + Inventory::get()->ammunation_count();

		if (Inventory::get()->weapon_count() > 1)
			if (Inventory::get()->body_weapon() == id_Item)
				count_item = count_item + Inventory::get()->weapon_count();

		if (count_item <= (int)it->second->min)
			return true;
	}
	return false;
}

bool RepoterCore::withdraw_repot(){
	Actions::get()->deposit_all();

	int total = total_money_all_repot();
	if (total <= 0)
		return true;

	for (int trying_loop = 0; trying_loop < 2; trying_loop++){
		Actions::get()->reopen_containers(false);

		Actions::get()->says("hi", "NPCs");

		uint32_t delay = std::min(TibiaProcess::get_default()->get_action_wait_delay(8.0f),(uint32_t)2000);

		Sleep(delay);

		Actions::get()->says("withdraw", "NPCs");

		TibiaProcess::get_default()->wait_ping_delay(2);

		Actions::get()->says(std::to_string(total), "NPCs");

		TibiaProcess::get_default()->wait_ping_delay(2);

		Actions::get()->says("yes", "NPCs");

		TibiaProcess::get_default()->wait_ping_delay(2);

		Actions::get()->says("balance", "NPCs");

		TibiaProcess::get_default()->wait_ping_delay(2);

		ContainerManager::get()->wait_money_change(2000);

		int count_money = ContainerManager::get()->get_total_money();
		if (count_money >= total)
			return true;
	}
	return false;
}

bool RepoterCore::sell_empty_vials() {
	Actions::reopen_containers();
	open_trade(std::vector<std::string>());

	if (!NpcTrade::get()->is_open())
		return false;

	auto empty_potions_list = ItemsManager::get()->get_items_as_empty_potions();
	for (int item_id : empty_potions_list) {
		int item_count = ContainerManager::get()->get_item_count(item_id);
		if (item_count <= 0)
			continue;

		int count_max = 0;
		while (item_count > 0 && count_max < 3) {
			int current_item_count_to_sell = item_count > 100 ? 100 : item_count;

			NpcTrade::get()->sell_item(item_id, current_item_count_to_sell);

			ContainerManager::get()->reorganize_container();

			int new_item_count = ContainerManager::get()->get_item_count(item_id);
			if (new_item_count == item_count)
				count_max++;

			item_count = new_item_count;
		}
	}

	return true;
}