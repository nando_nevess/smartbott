#include "LooterStatistics.h"
#include "Core\LooterCore.h"

using namespace Neutral;
String^ remove_reason_as_string(loot_queue_remove_reason reason){
	switch (reason){
	case loot_queue_remove_reason::remove_reason_looted:{
		return "looted";
	}
		break;
	case loot_queue_remove_reason::remove_reason_none:
	{
		return "none";
	}
		break;
	case loot_queue_remove_reason::remove_reason_open_timeout:
	{
		return "open timeout";
	}
		break;
	case loot_queue_remove_reason::remove_reason_timeout:
	{
		return "timeout";
	}
		break;
	case loot_queue_remove_reason::remove_reason_total:{
		return "total";
	}
		break;
	case loot_queue_remove_reason::remove_reason_unreachable:{
		return "unreachable";
	}
		break;

	case loot_queue_remove_reason::remove_reason_action_retval_fail:
	{
		return "max_fail";
	}
		break;
	case loot_queue_remove_reason::remove_reason_action_retval_warning_cannot_use:
	{
		return "cannot_use";
	}
		break;
	case loot_queue_remove_reason::remove_reason_cant_loot_all:
	{
		return "cant_loot_all";
	}
		break;
	case loot_queue_remove_reason::remove_reason_not_match:
	{
		return "dont_match_contents";
	}
		break;
	case loot_queue_remove_reason::remove_reason_trying:
	{
		return "max_try";
	}
		break;
	case loot_queue_remove_reason::remove_reason_warning_message_not_owner:
	{
		return "not_owner";
	}
		break;

	case loot_queue_remove_reason::remove_reason_out_of_range:
		return "range";
		break;
	case loot_queue_remove_reason::remove_reason_warning_message_upstairs:
		return "upstairs";
		break;
	case loot_queue_remove_reason::remove_reason_warning_message_downstairs:
		return "downstairs";
		break;
	case loot_queue_remove_reason::remove_reason_warning_is_not_valious:
		return "not_valious";
		break;		
	}
	return "no reason";
}

System::Void LooterStatistics::timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	std::vector<LootCreatureOnQueuePtr> loot_creatures = LootListQueue::get()->getLootsCreaturesOnQueueCopy();
	std::vector<LootCreatureOnQueuePtr> loot_creatures_removed = LootListQueue::get()->getRemovedLootsCreaturesOnQueueCopy();
	System::Collections::Generic::List<Telerik::WinControls::UI::ListViewDataItem^> removed_items;
	for (auto creature : loot_creatures){
		bool found = false;
		for each(Telerik::WinControls::UI::ListViewDataItem^ item in listViewToPerform->Items){
			if ((System::Int64)item->Value == creature->queue_id){
				found = true;
				break;
			}
		}
		if (found){
			continue;
		}
		Telerik::WinControls::UI::ListViewDataItem^ item = gcnew Telerik::WinControls::UI::ListViewDataItem((System::Int64) creature->queue_id);
		item->Value = (System::Int64) creature->queue_id;
		listViewToPerform->Items->Add(item);
	}
	for each(Telerik::WinControls::UI::ListViewDataItem^ item in listViewToPerform->Items){
		LootCreatureOnQueuePtr in_list = nullptr;
		for (auto loot_creature : loot_creatures){
			if (loot_creature->queue_id == (System::Int64)item->Value){
				in_list = loot_creature;
				break;
			}
		}
		if (!in_list){
			removed_items.Add(item);
		}
		else{
			CreatureOnBattlePtr creature = in_list->get_creature_died();
			if (creature){
				item["name"] = gcnew String(&creature->name[0]);
				item["position"] = gcnew String(&creature->get_coordinate_monster().ToString()[0]);
			}
			
			item["message"] = gcnew String(&in_list->get_loot_message()[0]);
			String^ temp_loot_Items = "";
			auto temp = in_list->get_loot_items();
			for (auto item : temp){
				String^ temp = "" + gcnew String(item.first.c_str()) + "," + Convert::ToString(item.second) + "; ";
				temp_loot_Items += temp;
			}
			item["stack"] = temp_loot_Items;
			item["timeout"] = Convert::ToString(in_list->get_timeout_count());
			item["fail"] = Convert::ToString(in_list->get_fail_count());
			item["finalstate"] = remove_reason_as_string(in_list->remove_reason);
			if (in_list->get_died_time())
				item["diedtime"] = Convert::ToString(in_list->get_died_time()->elapsed_milliseconds());
		}
	}
	
	for each(Telerik::WinControls::UI::ListViewDataItem^ item in removed_items){
		listViewToPerform->Items->Remove(item);
		radListViewPerformed->Items->Add(item);
	}

	System::Collections::Generic::List<Telerik::WinControls::UI::ListViewDataItem^> full_removed;

	for each (auto item in radListViewPerformed->Items){
		int64_t value = (int64_t)item->Value;
		LootCreatureOnQueuePtr in_list = nullptr;
		for (auto loot_creature : loot_creatures_removed){
			if (loot_creature->queue_id == (System::Int64)item->Value){
				in_list = loot_creature;
				break;
			}
		}
		if (!in_list){
			full_removed.Add(item);
		}
		else{
			CreatureOnBattlePtr creature = in_list->get_creature_died();
			if (creature){
				item["name"] = gcnew String(&creature->name[0]);
				item["position"] = gcnew String(&creature->get_coordinate_monster().ToString()[0]);
			}

			item["message"] = gcnew String(&in_list->get_loot_message()[0]);		
			auto temp_ = in_list->get_loot_items();
			String^ temp_loot_Items_ = "";
			for (auto item : temp_){
				String^ temp_ = "" + gcnew String(item.first.c_str()) + "," + Convert::ToString(item.second) + "; ";
				temp_loot_Items_ += temp_;
			}
			item["stack"] = temp_loot_Items_;
			item["timeout"] = Convert::ToString(in_list->get_timeout_count());
			item["fail"] = Convert::ToString(in_list->get_fail_count());
			item["finalstate"] = remove_reason_as_string(in_list->remove_reason);
			if (in_list->get_died_time())
				item["diedtime"] = Convert::ToString(in_list->get_died_time()->elapsed_milliseconds());
			item->Value = (int64_t)value;
		}
	}

	for each(auto item in full_removed){
		radListViewPerformed->Items->Remove(item);
	}
}