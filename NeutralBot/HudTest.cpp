#pragma once
#include "HudTest.h"
#include "Core\LooterCore.h"
#include "Core\ItemsManager.h"

using namespace NeutralBot;

System::Void HudTest::timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
	radListView1->Items->Clear();
	auto map = LooterCore::get()->get_map_items_looted();

	for (auto it : map){
		if (!it.second)
			continue;

		String^ ItemName = gcnew String(ItemsManager::get()->getItemNameFromId(it.first).c_str());

		Telerik::WinControls::UI::ListViewDataItem^ OIOI = (gcnew Telerik::WinControls::UI::ListViewDataItem(ItemName, gcnew cli::array< System::String^  >(2) 
		{ ItemName, Convert::ToString(it.second->count) }));

		this->radListView1->Items->Add(OIOI);
	}
}