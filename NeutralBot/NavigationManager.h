#pragma once
#include "PlayerInfo.h"
#include "PlayerInventory.h"
#include "PlayerCreatures.h"
#include "PlayerInventory.h"
#include "PlayerWaypoint.h"
#include "PlayerContainers.h"
#include "NavigationStream.h"
#include "JsonManager.h"
#include "ClientManager.h"
#include "PacketHeader.h"
#include "Scheduler.h"
#include <mutex>


enum protocol_t{
	protocol_none,
	protocol_text_message,
	protocol_data_message,
	protocol_player_info,
	protocol_player_inventory,
	protocol_player_containers,
	protocol_player_creatures,
	protocol_player_storage,
	protocol_change_room,
	protocol_player_waypointer,
	protocol_total
};

class Client;

class NavigationManager : public Scheduler{
	std::map<std::string/*nome do char*/, NaviPlayerInfoPtr> list_player_info;
	std::map<std::string/*nome do char*/, NaviPlayerInventoryPtr> list_player_inventory;
	std::map<std::string/*nome do char*/, NaviPlayerCreaturesPtr> list_player_creatures;
	std::map<std::string/*nome do char*/, NaviPlayerContainersPtr> list_player_containers;
	std::map<std::string/*nome do char*/, NaviPlayerWaypointerPtr > list_player_waypointer;

	std::string simple_string = "";
	std::string rest_buffer = "";
	
	std::mutex mtx;

	std::shared_ptr<Client> client_in;
	
	char recv_buffer[100000];

	std::string current_room = "";
	std::string current_pass = "";
	boost::asio::io_service svc;
public:
	NavigationManager() : Scheduler(svc){
		star_loop_client();
	}

	void star_loop_client();

	void add_player_info(std::string name_char, Json::Value json_value){
		auto it = list_player_info.find(name_char);

		if (it == list_player_info.end())
			list_player_info[name_char] = NaviPlayerInfoPtr(new	NaviPlayerInfo);

		list_player_info[name_char]->parse_json_to_class(json_value);
	}
	void add_player_inventory(std::string name_char, Json::Value json_value){
		auto it = list_player_inventory.find(name_char);

		if (it == list_player_inventory.end())
			list_player_inventory[name_char] = NaviPlayerInventoryPtr(new NaviPlayerInventory);

		list_player_inventory[name_char]->parse_json_to_class(json_value);
	}
	void add_player_creatures(std::string name_char, Json::Value json_value){
		auto it = list_player_creatures.find(name_char);

		if (it == list_player_creatures.end())
			list_player_creatures[name_char] = NaviPlayerCreaturesPtr(new NaviPlayerCreatures);

		list_player_creatures[name_char]->parse_json_to_class(json_value);
	}
	void add_player_containers(std::string name_char, Json::Value json_value){
		auto it = list_player_containers.find(name_char);

		if (it == list_player_containers.end())
			list_player_containers[name_char] = NaviPlayerContainersPtr(new	NaviPlayerContainers);

		list_player_containers[name_char]->parse_json_to_class(json_value);
	}
	void add_player_waypointer(std::string name_char, Json::Value json_value){
		auto it = list_player_waypointer.find(name_char);

		if (it == list_player_waypointer.end())
			list_player_waypointer[name_char] = NaviPlayerWaypointerPtr(new	NaviPlayerWaypointer);

		list_player_waypointer[name_char]->parse_json_to_class(json_value);
	}
	
	void connect_to_server(std::string ip, uint32_t port, std::string server_name, std::string server_pass);

	void frist_packet();

	void creat_room(std::string server_name, std::string server_pass = "");
	
	std::string change_message_to_packet(std::string& message, protocol_t protocol_in, std::string player_to_send = "");

	void close(){
		client_in->close();
	}

	void send_player_info(std::string player_to_send = "");
	void send_player_inventory(std::string player_to_send = "");
	void send_player_containers(std::string player_to_send = "");
	void send_player_creatures(std::string player_to_send = "");
	void send_player_waypointer(std::string player_to_send = "");
	void send_player_message(std::string message,std::string player_to_send = "");

	std::string get_simple_string(){
		return simple_string;
	}

	NaviPlayerInfoPtr get_rule_player_info(std::string name_char){
		auto it = std::find_if(list_player_info.begin(), list_player_info.end(),
			[&](std::pair<std::string, NaviPlayerInfoPtr> info_pair){
			return _stricmp(&name_char[0], &info_pair.first[0]) == 0; });

			if (it == list_player_info.end())
				return 0;

			return it->second;
	}
	NaviPlayerInventoryPtr get_rule_player_inventory(std::string name_char){
		auto it = std::find_if(list_player_inventory.begin(), list_player_inventory.end(),
			[&](std::pair<std::string, NaviPlayerInventoryPtr> info_pair){
			return _stricmp(&name_char[0], &info_pair.first[0]) == 0; });

			if (it == list_player_inventory.end())
				return 0;

			return it->second;
	}
	NaviPlayerCreaturesPtr get_rule_player_creatures(std::string name_char){
		auto it = std::find_if(list_player_creatures.begin(), list_player_creatures.end(),
			[&](std::pair<std::string, NaviPlayerCreaturesPtr> info_pair){
			return _stricmp(&name_char[0], &info_pair.first[0]) == 0; });

			if (it == list_player_creatures.end())
				return 0;

			return it->second;
	}
	NaviPlayerContainersPtr get_rule_player_containers(std::string name_char){
		auto it = std::find_if(list_player_containers.begin(), list_player_containers.end(),
			[&](std::pair<std::string, NaviPlayerContainersPtr> info_pair){
			return _stricmp(&name_char[0], &info_pair.first[0]) == 0; });

			if (it == list_player_containers.end())
				return 0;

			return it->second;
	}
	NaviPlayerWaypointerPtr get_rule_player_waypointer(std::string name_char){
		auto it = std::find_if(list_player_waypointer.begin(), list_player_waypointer.end(),
			[&](std::pair<std::string, NaviPlayerWaypointerPtr> info_pair){
			return _stricmp(&name_char[0], &info_pair.first[0]) == 0; });

			if (it == list_player_waypointer.end())
				return 0;

			return it->second;
	}

	void process_message(char* buffer, uint16_t dados_no_buffer);
	
	static NavigationManager* get(){
		static NavigationManager* m = nullptr;
		if (!m)
			m = new NavigationManager();
		return m;
	}
};