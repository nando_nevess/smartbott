#pragma once
#include "MonsterInfo.h"
#include "Core\constants.h"
#include "Core\ItemsManager.h" 

class SpellCondition{
public:	
	std::string str_value = ""; 
	uint32_t int_value;

	spells_conditions_t condition_type;
	spell_actions_t action_type;

	void set_value(std::string val);
	std::string get_value();

	SpellCondition(spells_conditions_t type, spell_actions_t action_type, std::string value);

	uint32_t get_integer_value();
};

class SpellBase{
	std::string visual_name = "";
	bool checked;
	uint32_t order = 0;
	uint32_t cooldown = 1000;
	uint32_t min_hp = 0;
	uint32_t item_id = 0;
	uint32_t max_hp = 0;
	uint32_t min_mp = 0;
	uint32_t max_mp = 0;
	uint32_t required_value_point = 0;
	bool use_percent_hp = false;
	bool use_percent_mp = false;
	bool no_use_with_loot = false;
	bool no_use_with_hunt = false;

	std::string visual_cast_value;
	spell_type_t spell_type = spell_type_healing;

	bool state = true;
public:

	uint32_t get_min_hp();

	void set_min_hp(uint32_t value);

	uint32_t get_max_hp();

	void set_max_hp(uint32_t value);

	void set_min_mp(uint32_t value);

	void set_no_use_with_loot(bool value);

	uint32_t get_min_mp();

	uint32_t get_max_mp();

	void set_max_mp(uint32_t value);

	uint32_t get_order();

	void set_order(uint32_t value);

	std::string get_visual_name();

	void set_visual_name(std::string value);

	uint32_t get_required_value_point();

	void set_required_value_point(uint32_t value);

	bool get_use_percent_mp();

	void set_use_percent_mp(bool value);

	bool get_use_percent_hp();

	void set_spell_type(spell_type_t value);

	spell_type_t get_spell_type();

	bool get_state();

	void set_state(bool value);

	bool get_no_use_with_hunt();

	void set_no_use_with_hunt(bool value);

	bool get_checked();

	void set_checked(bool value);

	bool get_no_use_with_loot();

	void set_use_percent_hp(bool value);

	uint32_t get_item_id();

	void set_item_id(uint32_t value);
	
	std::vector<std::shared_ptr<SpellCondition>> vector_condition;

	uint32_t add_vector_condition(std::string value, spells_conditions_t type, spell_actions_t actions_type);

	std::shared_ptr<SpellCondition> get_vector_condition_rule(uint32_t index);

	void set_cooldown(int newcooldown);

	int get_cooldown();

	void set_visual_cast_value(std::string& value);
	std::string get_visual_cast_value();


	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value spellInfo);
};

class SpellHealth : public SpellBase{
	uint32_t health_type = 0;

public:
	SpellHealth();
	SpellHealth(bool checked, uint32_t health_type, uint32_t order, 
		uint32_t min_hp, uint32_t max_hp, uint32_t min_mp, uint32_t max_mp,
		std::string hotkey, uint32_t cooldown);

	GET_SET(uint32_t, health_type);

	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value spellInfo);
};
typedef std::shared_ptr<SpellHealth> SpellHealthPtr;

class SpellAttack : public SpellBase
{

	uint32_t spell_condition = 0;
	bool any_monster = false;
	uint32_t maximum_sqm = 0;
	uint32_t monster_in_area_min = 0;
	uint32_t monster_in_area_max = 0;
	uint32_t monster_qty_list_min = 0;
	uint32_t monster_qty_list_max = 0;
	std::map<uint32_t, std::shared_ptr<MonsterInfo>> monsterList;

public:
	SpellAttack() {};
	SpellAttack(bool checked, spell_type_t spell_type, int spell_condition, int order, std::string visual_cast_value, int min_hp,
		int min_mp, int cooldown, int maximum_sqm, int monster_in_area_min, int monster_in_area_max,
		int monster_qty_list_min, int monster_qty_list_max);

	GET_SET(uint32_t, spell_condition);
	GET_SET(uint32_t, maximum_sqm);
	GET_SET(bool, any_monster);
	GET_SET(uint32_t, monster_in_area_min);
	GET_SET(uint32_t, monster_in_area_max);
	GET_SET(uint32_t, monster_qty_list_min);
	GET_SET(uint32_t, monster_qty_list_max);

	void add_monster_in_list_by_index(int index, std::shared_ptr<MonsterInfo> monster);
	bool contains_monster_in_list(std::string& monsterName, int creature_hp);
	std::map<uint32_t, std::shared_ptr<MonsterInfo>>& get_monster_list();
	int request_new_id();
	std::shared_ptr<MonsterInfo> get_monster_rule(int key);
	bool remove_id(int key);
	
	Json::Value parse_class_to_json();
	void parse_json_to_class(Json::Value spellInfo);
};
typedef std::shared_ptr<SpellAttack> SpellAttackPtr;


