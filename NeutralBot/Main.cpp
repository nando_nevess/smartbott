#include "SelectedCharacter.h"

void check_run_from_launcher(){
	extern DWORD get_parent_process_id();
	extern uint32_t get_process_id_from_name(std::string process_name);
	uint32_t parent_id = get_parent_process_id();
	uint32_t explorer_exe = get_process_id_from_name("explorer.exe");

	if (explorer_exe == parent_id){
		Telerik::WinControls::RadMessageBox::Show("Unable to run without launcher, please use launcher to open the system!", "Error!");
		TerminateProcess(GetCurrentProcess(), 0);
	}
}

[System::STAThread]
int main(array<System::String ^> ^ args){
	
	//check_run_from_launcher();
	
	AllocConsole();
	FILE *pFile;
	freopen_s(&pFile, "CONIN$", "r", stdin);
	freopen_s(&pFile, "CONOUT$", "w", stdout);
	freopen_s(&pFile, "CONOUT$", "w", stderr);
	
	register_in_monitor("MainThread");
	System::Windows::Forms::Application::Run(gcnew NeutralBot::Panorama());
	
	return 0;
}
