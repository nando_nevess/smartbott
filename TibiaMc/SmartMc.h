#pragma once
#include "TibiaMc.h"

namespace TibiaMc {
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	public ref class SmartMc : public System::Windows::Forms::Form{
	public:
		SmartMc(void){
			MultiClient::get();
			InitializeComponent();
		}

	protected:
		~SmartMc(){
			if (components)
				delete components;
		}
	private: System::Windows::Forms::PictureBox^  pictureBox1;
	protected:
	private: System::Windows::Forms::Button^  bt_active;
	private: System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void){
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(SmartMc::typeid));
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->bt_active = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureBox1.Image")));
			this->pictureBox1->Location = System::Drawing::Point(12, 12);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(300, 121);
			this->pictureBox1->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			// 
			// bt_active
			// 
			this->bt_active->Location = System::Drawing::Point(12, 139);
			this->bt_active->Name = L"bt_active";
			this->bt_active->Size = System::Drawing::Size(300, 51);
			this->bt_active->TabIndex = 1;
			this->bt_active->Text = L"Disable";
			this->bt_active->UseVisualStyleBackColor = true;
			this->bt_active->Click += gcnew System::EventHandler(this, &SmartMc::bt_active_Click);
			// 
			// SmartMc
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(326, 202);
			this->Controls->Add(this->bt_active);
			this->Controls->Add(this->pictureBox1);
			this->Icon = (cli::safe_cast<System::Drawing::Icon^>(resources->GetObject(L"$this.Icon")));
			this->MaximizeBox = false;
			this->Name = L"SmartMc";
			this->Text = L"SmartMc";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void bt_active_Click(System::Object^  sender, System::EventArgs^  e) {
				 
				 if (bt_active->Text == "Enable"){
					 bt_active->Text = "Disable";
					 MultiClient::get()->enable_mc();
				 }
				 else{
					 bt_active->Text = "Enable";
					 MultiClient::get()->disable_mc();
				 }
				 
	}
	}; 
}
