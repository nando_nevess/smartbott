#pragma once

class MultiClient{
	int timeout_resting;
	int MC_THREAD();

public:
	MultiClient();
	bool state = true;
	void enable_mc();
	void disable_mc();

	static MultiClient* get(){
		static MultiClient* mMultiClient = nullptr;
		if (!mMultiClient)
			mMultiClient = new MultiClient;
		return mMultiClient;
	}
};