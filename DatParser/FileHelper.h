#include <string>
#include <fstream>
#include <stdint.h>


class FileReader{
	std::ifstream file;
	bool file_good;
	uint32_t length;
	uint32_t carret_pos;
	char* data;
public:
	std::string file_path;
	FileReader(std::string path);

	~FileReader();

	void read_bytes(void* ptr_out, uint32_t len);

	bool is_good();

	bool is_end();

	uint32_t get_bytes_left();

	template <class T>
	T read(uint32_t error){
		T retval;
		if (get_bytes_left() < sizeof(T))
			error = 1;
		else
			retval = *(T*)(&data[carret_pos]);

		advance_carret(sizeof(T));
		error = 0;
		return retval;
	}

	template <class T>
	T read(){
		T retval;
		if (get_bytes_left() < sizeof(T))
			throw new std::exception("trying to read size greater than left in file");
		else
			retval = *(T*)(&data[carret_pos]);
		advance_carret(sizeof(T));
		return retval;
	}


	template <class T>
	T get(){
		return read<T>();
	}

	void advance_carret(uint32_t bytes_count){
		carret_pos += bytes_count;
	}

	std::string getString();
};


class FileWriter{
	/*FileWriter(std::string path) :file(path){
	file_path = path;
	}
	~FileWriter(){
	file.close();
	}
	std::ofstream file;
	std::string file_path;
	template<class T>
	void write(T value, uint32_t error){
	file.
	}

	template<class T>
	void write(T value){

	}*/

};