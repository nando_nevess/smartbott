#include "ItemsManager.h"

bool is_fluid(uint32_t item_id){
	auto info = ItemsManager::get()->getItemTypeById(item_id);
	if (!info)
		return false;


	return info->m_attribs.has(ThingAttr::ThingAttrFluidContainer);
}

int main(){
	ItemsManager::get()->load("Tibia.dat");


	bool result = is_fluid(268);
	return 0;
}


