local POSITION_X = 3
local POSITION_Y = 0

local MMH = {
    SHOW_ALL_ITEMS = false,
    SHOW_ALL_SUPPLIES = false,
}

function comma_value(n)
local left, num, right = string.match(n, '^([^%d]*%d)(%d*)(.-)$')
return left..(num:reverse() : gsub('(%d%d%d)', '%1,') : reverse())..right
end

MMH.COLORS = {}
MMH.COLORS.FONT_COLOR = HudManager.color(255, 255, 255, 255)
MMH.COLORS.SECTION_HEADER_BACKGROUND = { 0.0, HudManager.color(170, 36, 68, 105), 0.23, HudManager.color(170, 39, 73, 114), 0.76, HudManager.color(170, 21, 39, 60) }
MMH.COLORS.ENTRY_NAME_BACKGROUND = { 0.0, HudManager.color(170, 75, 75, 75), 0.23, HudManager.color(170, 45, 45, 45), 0.76, HudManager.color(170, 19, 19, 19) }
MMH.COLORS.ENTRY_VALUE_BACKGROUND = { 0.0, HudManager.color(140, 145, 95, 0), 0.23, HudManager.color(140, 158, 104, 0), 0.76, HudManager.color(140, 84, 55, 0) }
MMH.COLORS.RESULT_POSITIVE_BACKGROUND = { 0.0, HudManager.color(170, 65, 96, 12), 0.23, HudManager.color(170, 67, 99, 13), 0.76, HudManager.color(170, 6, 52, 36) }
MMH.COLORS.RESULT_NEGATIVE_BACKGROUND = { 0.0, HudManager.color(170, 90, 12, 15), 0.23, HudManager.color(170, 98, 13, 17), 0.76, HudManager.color(170, 52, 6, 9) }
MMH.COLORS.RESET_BUTTON_BACKGROUND = MMH.COLORS.RESULT_POSITIVE_BACKGROUND

MMH.ELEMENTS = {}
MMH.ELEMENTS.RESET_BUTTON = -1
MMH.ELEMENTS.SWITCH_ITEMS_LOOTED = -1
MMH.ELEMENTS.SWITCH_SUPPLIES_USED = -1

MMH.SECTIONS = {}
MMH.SECTIONS.ITEMS_LOOTED = true
MMH.SECTIONS.SUPPLIES_USED = true

HudManager.set_font_style('Tahoma', 10, 75, MMH.COLORS.FONT_COLOR, 2, HudManager.color(170, 0, 0, 0))
HudManager.set_border_color(HudManager.color(140, 0, 0, 0))
HudManager.set_border_size(1)

local ROW_QUANTITY = 0;
local STRING_WIDTH = HudManager.measure_string_width('TEMP')
local STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 3

local ITEMS_LOOTED_WORTH, ITEM_LOOTED_QUANTITY, ITEM_LOOTED_WORTH = 0, 0, 0
local SUPPLIES_USED_WORTH, SUPPLY_USED_QUANTITY, SUPPLY_USED_WORTH = 0, 0, 0

HudManager.set_font_size(10)

HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 0,POSITION_Y +3 , 240, 20, 3, 3)
HudManager.draw_text('Monitor My Hunting v1.0',POSITION_X +2, 20 / 2 - STRING_HEIGHT * 0.5 + 3+  POSITION_Y)

HudManager.set_font_size(9)

STRING_WIDTH = HudManager.measure_string_width('TEMP')
STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 0

HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 0, 25+  POSITION_Y, 240, 17, 3, 3)
HudManager.draw_text('Looting Accuracy',POSITION_X +  6, 23 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)

HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_VALUE_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 130, 25+  POSITION_Y, 110, 17, 3, 3)
HudManager.draw_text(string.format('%.2f', 0) .. '%', POSITION_X + 136, 23 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)

HudManager.set_font_size(10)

STRING_WIDTH = HudManager.measure_string_width('TEMP')
STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 3

HudManager.add_grad_colors(unpack(MMH.COLORS.SECTION_HEADER_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 0, 41 + 3+  POSITION_Y, 240, 20, 3, 3)
HudManager.draw_text('ITEMS LOOTED',POSITION_X +  2, 41 + 20 / 2 - STRING_HEIGHT * 0.5 + 3+  POSITION_Y)

if (MMH.SECTIONS.ITEMS_LOOTED) then
    HudManager.add_grad_colors(unpack(MMH.COLORS.RESULT_POSITIVE_BACKGROUND))
else
    HudManager.add_grad_colors(unpack(MMH.COLORS.RESULT_NEGATIVE_BACKGROUND))
end

MMH.ELEMENTS.SWITCH_ITEMS_LOOTED = HudManager.draw_round_rect(POSITION_X + 220, 41 + 3+  POSITION_Y, 20, 20, 2, 2)
HudManager.draw_text('X', POSITION_X + 225, 41 + 20 / 2 - STRING_HEIGHT * 0.5 + 3+  POSITION_Y)

HudManager.set_font_size(9)

local lootingitem = Info.items_looted()
for key, ItemEntry in pairs(lootingitem) do
    if (MMH.SHOW_ALL_ITEMS or ItemEntry.count > 0) then
        ITEM_LOOTED_QUANTITY = ItemEntry.count
        ITEM_LOOTED_WORTH = ItemEntry.sellprice * ITEM_LOOTED_QUANTITY
        
        if (MMH.SECTIONS.ITEMS_LOOTED) then
            HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
            HudManager.draw_round_rect(POSITION_X + 0, 64 + ROW_QUANTITY * 18 + 2, 240, 16, 3, 3)
            HudManager.draw_text(((#ItemEntry.name > 20 and string.match(string.sub(ItemEntry.name, 1, 20), '(.-)%s?$') .. '...') or ItemEntry.name),POSITION_X + 6, 64 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)
            
            HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_VALUE_BACKGROUND))
            HudManager.draw_round_rect(POSITION_X + 130, 64 + ROW_QUANTITY * 18 + 2, 110, 16, 3, 3)
            HudManager.draw_text(comma_value(ITEM_LOOTED_QUANTITY) .. ' (' ..math.floor(ITEM_LOOTED_WORTH / 100) / 10 .. 'K)', POSITION_X + 136, 64 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)
            
            ROW_QUANTITY = ROW_QUANTITY + 1
        end
        ITEMS_LOOTED_WORTH = ITEMS_LOOTED_WORTH + ITEM_LOOTED_WORTH
    end
end


STRING_WIDTH = HudManager.measure_string_width('TEMP')
STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 1

HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 0, 64 + ROW_QUANTITY * 18 + 2+  POSITION_Y, 240, 17, 3, 3)
HudManager.draw_text('Total: ' ..comma_value(ITEMS_LOOTED_WORTH) .. ' GPs', POSITION_X + 6, 64 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 3.5+  POSITION_Y)

ROW_QUANTITY = ROW_QUANTITY + 1

HudManager.set_font_size(10)

STRING_WIDTH = HudManager.measure_string_width('TEMP')
STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 3

HudManager.add_grad_colors(unpack(MMH.COLORS.SECTION_HEADER_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 0, 64 + ROW_QUANTITY * 18 + 3+  POSITION_Y, 240, 20, 3, 3)
HudManager.draw_text('SUPPLIES USED', POSITION_X + 2, 64 + ROW_QUANTITY * 18 + 20 / 2 - STRING_HEIGHT * 0.5 + 3+  POSITION_Y)

if (MMH.SECTIONS.SUPPLIES_USED) then
    HudManager.add_grad_colors(unpack(MMH.COLORS.RESULT_POSITIVE_BACKGROUND))
else
    HudManager.add_grad_colors(unpack(MMH.COLORS.RESULT_NEGATIVE_BACKGROUND))
end

MMH.ELEMENTS.SWITCH_SUPPLIES_USED = HudManager.draw_round_rect(POSITION_X + 220, 64 + ROW_QUANTITY * 18 + 3+  POSITION_Y, 20, 20, 2, 2)
HudManager.draw_text('X', POSITION_X + 225, 64 + ROW_QUANTITY * 18 + 20 / 2 - STRING_HEIGHT * 0.5 + 3+  POSITION_Y)

HudManager.set_font_size(9)

STRING_WIDTH = HudManager.measure_string_width('TEMP')
STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 1


local lootingitem_ = Info.items_used()
for key, ItemEntry in pairs(lootingitem_) do
    if (MMH.SHOW_ALL_SUPPLIES or ItemEntry.count > 0) then
        SUPPLY_USED_QUANTITY = ItemEntry.count
        SUPPLY_USED_WORTH = ItemEntry.buyprice * SUPPLY_USED_QUANTITY
        if (MMH.SECTIONS.SUPPLIES_USED) then
            HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
            HudManager.draw_round_rect(POSITION_X + 0, 87 + ROW_QUANTITY * 18 + 2+  POSITION_Y, 240, 16, 3, 3)
            HudManager.draw_text(((#ItemEntry.name > 20 and string.match(string.sub(ItemEntry.name, 1, 20), '(.-)%s?$') .. '...') or ItemEntry.name), POSITION_X + 6, 87 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)
            
            HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_VALUE_BACKGROUND))
            HudManager.draw_round_rect(POSITION_X + 130, 87 + ROW_QUANTITY * 18 + 2+  POSITION_Y, 110, 16, 3, 3)
            HudManager.draw_text(comma_value(SUPPLY_USED_QUANTITY) .. ' (' ..math.floor(SUPPLY_USED_WORTH / 100) / 10 .. 'K)', POSITION_X + 136, 87 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)
            
            HudManager.draw_item(ItemEntry.name,POSITION_X +  6, 82 + ROW_QUANTITY * 18 + 2+  POSITION_Y, 24, 24)
            ROW_QUANTITY = ROW_QUANTITY + 1
        end
        SUPPLIES_USED_WORTH = SUPPLIES_USED_WORTH + SUPPLY_USED_WORTH
    end
end

if (MMH.SECTIONS.SUPPLIES_USED) then
    HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
    HudManager.draw_round_rect(POSITION_X + 0, 87 + ROW_QUANTITY * 18 + 2+  POSITION_Y, 240, 17, 3, 3)
    
    HudManager.draw_text('Money Spent', POSITION_X + 6, 87 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)
    
    HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_VALUE_BACKGROUND))
    HudManager.draw_round_rect(POSITION_X + 130, 87 + ROW_QUANTITY * 18 + 2+  POSITION_Y, 110, 17, 3, 3)
    HudManager.draw_text(comma_value(Info.money_spent()) .. ' (' ..math.floor(Info.money_spent() / 100) / 10 .. 'K)', POSITION_X + 136, 87 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 4+  POSITION_Y)
    
    ROW_QUANTITY = ROW_QUANTITY + 1
end

SUPPLIES_USED_WORTH = SUPPLIES_USED_WORTH + 0
HudManager.add_grad_colors(unpack(MMH.COLORS.ENTRY_NAME_BACKGROUND))
HudManager.draw_round_rect(POSITION_X + 0, 87 + ROW_QUANTITY * 18 + 3+  POSITION_Y, 240, 17, 3, 3)
HudManager.draw_text('Total: ' ..comma_value(SUPPLIES_USED_WORTH) .. ' GPs', POSITION_X + 6, 87 + ROW_QUANTITY * 18 + 15 / 2 - STRING_HEIGHT * 0.5 + 5+  POSITION_Y)

ROW_QUANTITY = ROW_QUANTITY + 1

HudManager.set_font_size(10)

STRING_WIDTH = HudManager.measure_string_width('TEMP')
STRING_HEIGHT = HudManager.measure_string_height('TEMP') - 3

if (ITEMS_LOOTED_WORTH >= SUPPLIES_USED_WORTH) then
    HudManager.add_grad_colors(unpack(MMH.COLORS.RESULT_POSITIVE_BACKGROUND))
else
    HudManager.add_grad_colors(unpack(MMH.COLORS.RESULT_NEGATIVE_BACKGROUND))
end

HudManager.draw_round_rect(POSITION_X + 0, 87 + ROW_QUANTITY * 18 + 3+  POSITION_Y, 240, 20, 3, 3)
HudManager.draw_text(((ITEMS_LOOTED_WORTH >= SUPPLIES_USED_WORTH and('PROFIT: ')) or('WASTE: ')) ..comma_value(ITEMS_LOOTED_WORTH - SUPPLIES_USED_WORTH) .. ' GPs (' ..math.abs(math.floor(((ITEMS_LOOTED_WORTH - SUPPLIES_USED_WORTH) * 3600) / 
(BotManager.get_info_time_bot_running() / 1000) / 100) / 10) .. ' k/h)', POSITION_X + 2, 87 + ROW_QUANTITY * 18 + 20 / 2 - STRING_HEIGHT * 0.5 + 3+  POSITION_Y)
