#define CURL_STATICLIB


#include <Windows.h>
#include <WinInet.h>
#include <stdio.h>

#include <curl/curl.h>
#include <curl/easy.h>
#include "Downloader.h"
#include "ShortcutProvider.h"
#include "unzip.h"
#include "hashgen.h"

#include <iostream>
#include <vector>
#include <fstream>
#include <stdlib.h>
#include <tchar.h>
#include <psapi.h>
#include <ostream>
#include <sstream>
#include <algorithm>


//#include "Dialog.h"

//#include <NoPingShared\defines_all.h>
#include <boost\filesystem.hpp>

#pragma comment(lib, "Wininet.lib")
#pragma comment(lib, "libcurl.lib")

#pragma comment(lib,"psapi.lib")
#pragma comment(lib,"Advapi32.lib")

struct file{
	std::string name;
	std::string path;
	std::string hash;
	std::string link;
};

struct info{
	std::vector<file> files;// name  --  filehash
	std::string message;
	std::string hash;
};

class Download{
public:
	static bool ishttp(char *url);
	static bool httpverOK(HINTERNET hIurl);
	static bool getfname(char *url, char *fname);
	static unsigned long openfile(char *url, bool reload, ofstream &fout);
	static bool download(char *url, bool reload = false, void(*update)(unsigned long, unsigned long) = NULL);
};

class DLExc
{
private:
	char err[MAX_ERRMSG_SIZE];
public:
	DLExc(char *exc){
		if (strlen(exc) < MAX_ERRMSG_SIZE)
			strcpy(err, exc);
	}

	const char *geterr(){
		return err;
	}
};


struct version_node{
	std::string before;
	std::string next;
	std::string current;
};

struct version_file{
	std::string current_version;
	std::vector<version_node> versions;
	bool valid;
};
version_file get_version_file_from_string(std::string xml_string);
version_file get_version_file(std::string url);

bool is_hidden = false;



#define STOP_DOWNLOAD_AFTER_THIS_MANY_BYTES         9999999999
#define MINIMAL_PROGRESS_FUNCTIONALITY_INTERVAL     3 
void(*actual_update_curl)(unsigned long, unsigned long);
std::string FTP_USER = "download";
HWND main_hwnd = 0;
bool can_login = false;
std::string bar_text = "";
int percent = 0;

std::string get_hash(std::string file)
{
	SetLastError(0);
	FILE * f = fopen(&file[0], "r");
	int err = GetLastError();
	//If couldnt open specifier file or some another error
	if (!f)
		return "";
	fclose(f);

	MD5 my_md5;
	std::string hash = std::string(my_md5.digestFile(&file[0]));
	return hash;
}

std::string version_to_update = "";

const std::string USER_AGENT_NPTUNNEL = "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:38.0) Gecko/20101001 Firefox/38.0";
class Filter
{
public:
	std::string content_;
	static size_t handle(char * data, size_t size, size_t nmemb, void * p);
	size_t handle_impl(char * data, size_t size, size_t nmemb);
};

size_t Filter::handle(char * data, size_t size, size_t nmemb, void * p)
{
	return static_cast<Filter*>(p)->handle_impl(data, size, nmemb);
}

size_t Filter::handle_impl(char* data, size_t size, size_t nmemb)
{
	content_.append(data, size * nmemb);
	return size * nmemb;
}
bool curl_download_string(char* url, std::string& out, bool use_ftp){

	CURL *curl;
	CURLcode res = CURLE_OK;
	curl = curl_easy_init();

	if (!curl)
		return 0;


	Filter f;
	curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 2);
	curl_easy_setopt(curl, CURLOPT_URL, url);
	if (use_ftp)
		curl_easy_setopt(curl, CURLOPT_USERPWD, (FTP_USER + ":" + FTP_USER).c_str());
	curl_easy_setopt(curl, CURLOPT_WRITEDATA, &f);
	curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, &Filter::handle);
	curl_easy_setopt(curl, CURLOPT_USERAGENT, USER_AGENT_NPTUNNEL);

	struct curl_slist *headers = NULL; /* init to NULL is important */
	headers = curl_slist_append(headers, "Cache-Control: private, no-cache, no-store, must-revalidate, max-age=0");
	headers = curl_slist_append(headers, "Pragma: no-cache");
	curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);

	res = curl_easy_perform(curl);
	curl_slist_free_all(headers); /* free the header list */
	curl_easy_cleanup(curl);
	out = f.content_;
	return res == CURLE_OK;

}
struct myprogress {
	double lastruntime;
	CURL *curl;
};
static int xferinfo(void *p,
	curl_off_t dltotal, curl_off_t dlnow,
	curl_off_t ultotal, curl_off_t ulnow){
	struct myprogress *myp = (struct myprogress *)p;
	CURL *curl = myp->curl;
	double curtime = 0;

	curl_easy_getinfo(curl, CURLINFO_TOTAL_TIME, &curtime);
	actual_update_curl((unsigned long)dltotal, (unsigned long)dlnow);
	return 0;
}
static int older_progress(void *p,
	double dltotal, double dlnow,
	double ultotal, double ulnow)
{
	return xferinfo(p,
		(curl_off_t)dltotal,
		(curl_off_t)dlnow,
		(curl_off_t)ultotal,
		(curl_off_t)ulnow);
}
void update_percent_curl(unsigned long total, unsigned long now){
	std::cout << "\n " << total << "|" << now;
	extern void myprogess_set(int);
	if (!total || !now) {
		percent = 0;
		return;
	}

	percent = (int)(((float)now / (float)total)* (float)100);

}
size_t write_data(void *ptr, size_t size, size_t nmemb, FILE *stream) {
	size_t written;
	written = fwrite(ptr, size, nmemb, stream);
	return written;
}

bool curl_download(char * url, bool reload/*ignore*/, void(*update)(unsigned long, unsigned long), char* out_filename, bool use_ftp){
	actual_update_curl = update;
	CURL *curl;
	CURLcode res = CURLE_OK;
	struct myprogress prog;

	curl = curl_easy_init();
	if (curl) {
		curl_easy_setopt(curl, CURLOPT_CONNECTTIMEOUT, 2);
		prog.lastruntime = 0;
		prog.curl = curl;
		if (use_ftp)
			curl_easy_setopt(curl, CURLOPT_USERPWD, (FTP_USER + ":" + FTP_USER).c_str());

		//SET HEADERS
		struct curl_slist *headers = NULL; /* init to NULL is important */
		headers = curl_slist_append(headers, "Cookie: DRUPAL_UID = 0; saw_welcome = 1");
		headers = curl_slist_append(headers, "Cache-Control: private, no-cache, no-store, must-revalidate, max-age=0");
		headers = curl_slist_append(headers, "Pragma: no-cache");
		curl_easy_setopt(curl, CURLOPT_HTTPHEADER, headers);
		curl_easy_setopt(curl, CURLOPT_URL, url);

		curl_easy_setopt(curl, CURLOPT_PROGRESSFUNCTION, older_progress);

		curl_easy_setopt(curl, CURLOPT_PROGRESSDATA, &prog);

#if LIBCURL_VERSION_NUM >= 0x072000
		curl_easy_setopt(curl, CURLOPT_XFERINFOFUNCTION, xferinfo);
		curl_easy_setopt(curl, CURLOPT_XFERINFODATA, &prog);
#endif
		curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, write_data);
		curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 0L);
		FILE*fp = fopen(out_filename, "wb");

		curl_easy_setopt(curl, CURLOPT_WRITEDATA, fp);
		curl_easy_setopt(curl, CURLOPT_USERAGENT, USER_AGENT_NPTUNNEL);

		res = curl_easy_perform(curl);
		curl_slist_free_all(headers); /* free the header list */
		fclose(fp);
		curl_easy_cleanup(curl);
	}

	return res == CURLE_OK;

}

/**
Download a file

Pass the URL of the file to url

To specify an update function that is called after each buffer is read, pass a
pointer to that function as the third parameter. If no update function is
desired, then let the third parameter default to null.
*/
bool Download::download(char * url, bool reload, void(*update)(unsigned long, unsigned long))
{
	ofstream fout;              // output stream
	unsigned char buf[BUF_SIZE];// input buffer
	unsigned long numrcved;     // number of bytes read
	unsigned long filelen;      // length of the file on disk
	HINTERNET hIurl = 0, hInet = 0;     // internet handles
	unsigned long contentlen;   // length of content
	unsigned long len;          // length of contentlen
	unsigned long total = 0;    // running total of bytes received
	char header[80];            // holds Range header

	try
	{
		if (!ishttp(url))
		{
			//throw DLExc("Must be HTTP url");
			return false;
		}

		/*
		Open the file spcified by url.
		The open stream will be returned in fout. If reload is true, then any
		preexisting file will be truncated. The length of any preexisting file
		(after possible truncation) is returned.
		*/
		filelen = openfile(url, reload, fout);

		// See if internet connection is available
		if (InternetAttemptConnect(0) != ERROR_SUCCESS)
		{
			//throw DLExc("Can't connect");
			return false;
		}

		// Open internet connection
		hInet = InternetOpen(&USER_AGENT_NPTUNNEL[0], INTERNET_OPEN_TYPE_DIRECT, NULL, NULL, 0);
		if (hInet == NULL)
		{
			//throw DLExc("Can't open connection");
			return false;
		}

		// Construct header requesting range of data
		sprintf(header, "Range:bytes=%d-", filelen);

		// Open the URL and request range
		//hIurl = InternetOpenUrl(hInet, url, header, -1, INTERNET_FLAG_NO_CACHE_WRITE, 0);

		hIurl = InternetOpenUrl(hInet, url, header, strlen(header), INTERNET_FLAG_NO_CACHE_WRITE, 0);
		if (hIurl == NULL)
		{
			//throw DLExc("Can't open url");
			return false;
		}
		// Confirm that HTTP/1.1 or greater is supported
		if (!httpverOK(hIurl))
		{
			//throw DLExc("HTTP/1.1 not supported");
			return false;
		}

		// Get content length
		len = sizeof contentlen;
		if (!HttpQueryInfo(hIurl,HTTP_QUERY_CONTENT_LENGTH | HTTP_QUERY_FLAG_NUMBER, &contentlen, &len, NULL))
		{
			//throw DLExc("File or content length not found");
			return false;
		}

		// If existing file (if any) is not complete, then finish downloading
		if (filelen != contentlen && contentlen)
		{
			do
			{
				// Read a buffer of info
				if (!InternetReadFile(hIurl, &buf, BUF_SIZE, &numrcved))
					return false; //throw DLExc("Error occurred during download");

				// Write buffer to disk
				fout.write((const char *)buf, numrcved);
				if (!fout.good())
					return false; //throw DLExc("Error writing file");

				// update running total
				total += numrcved;

				// Call update function, if specified
				if (update && numrcved > 0)
				{
					update(total, contentlen);
				}
			} while (numrcved > 0);
		}
		else
		{
			if (update)
				update(filelen, filelen);
		}
	}
	catch (DLExc)
	{
		fout.close();
		InternetCloseHandle(hIurl);
		InternetCloseHandle(hInet);
		// rethrow the exception for use by the caller
		//throw;
		return false;
	}

	fout.close();
	InternetCloseHandle(hIurl);
	InternetCloseHandle(hInet);

	return true;
}


// Return true if HTTP version of 1.1 or greater
bool Download::httpverOK(HINTERNET hIurl)
{
	char str[80];
	unsigned long len = 79;

	if (!HttpQueryInfo(hIurl, HTTP_QUERY_VERSION, &str, &len, NULL))
		return false;

	// First, check major version number
	char *p = strchr(str, '/');
	p++;
	if (p && *p == '0')
		return false;       // can't use HTTP 0.x

	// Now, find start of minor HTTP version number
	p = strchr(str, '.');
	p++;

	// convert to int
	int minorVerNum = atoi(p);

	if (minorVerNum > 0)
		return true;

	return false;
}

// Extract the filename from the URL.
// Return false if the filename cannot be found
bool Download::getfname(char *url, char *fname)
{
	// Find last slash /
	char *p = strrchr(url, '/');

	// Copy filename afther the last slash
	if (p && (strlen(p) < MAX_FILENAME_SIZE))
	{
		p++;
		strcpy(fname, p);
		return true;
	}
	else
	{
		return false;
	}
}

/*
Open the output file, initialize the output stream, and return the file's
length. If reload is true, first truncate any preexisting file
*/
unsigned long Download::openfile(char *url, bool reload, ofstream &fout)
{
	char fname[MAX_FILENAME_SIZE];

	if (!getfname(url, fname))
		throw DLExc("File name error");

	if (!reload)
		fout.open(fname, ios::binary | ios::out | ios::app | ios::ate);
	else
		fout.open(fname, ios::binary | ios::out | ios::trunc);

	if (!fout)
		throw DLExc("Can't open output file");

	// get current file length
	return (unsigned long)fout.tellp();
}

// Confirm that the URL specifies HTTP
bool Download::ishttp(char *url)
{
	char str[5] = "";

	// get the first four characters from the URL
	strncpy(str, url, 4);

	// convert to lowercase
	for (char *p = str; *p; p++)
		*p = tolower(*p);

	return !strcmp("http", str);
}



std::string StringToLower(char * entrace){
	int len = strlen(entrace);
	char * temp = (char*)malloc(len + 1);
	temp[len] = 0;
	int dif = ('Z' - 'z');
	for (int i = 0; i < len; i++)
		if (entrace[i] <= 'Z' && entrace[i] >= 'A')
			temp[i] = entrace[i] - dif;
		else
			temp[i] = entrace[i];
	
	if (temp[0] == 0)
		return "";

	std::string ret(temp);
	free(temp);
	return ret;
}
std::string StringToLower(std::string & entrace){
	if (!(&entrace) || entrace.length() == 0)return "";
	return StringToLower((char *)&entrace[0]);
}


int download_string(char * entrace_url, string & out){
	char http[] = "HTTP";

	HINTERNET internet;
	HINTERNET url;
	char *outputfile = NULL;
	char *buf = NULL;
	DWORD bytesread;
	int k;

	for (k = 0; k < 4; k++)
		if (entrace_url[k] != http[k] && entrace_url[k] != (char)(http[k] + 32))
			return false;

	internet = InternetOpen(HTTP_USER_AGENT,
		INTERNET_OPEN_TYPE_PRECONFIG,
		NULL,
		NULL,
		0);

	if (internet == NULL)
		return false;

	url = InternetOpenUrl(internet,
		entrace_url,
		NULL,
		0,
		INTERNET_FLAG_PRAGMA_NOCACHE | INTERNET_FLAG_RELOAD | INTERNET_FLAG_RESYNCHRONIZE,
		0);

	if (url == NULL)
		return false;

	buf = (char*)malloc(BUFFER_SIZE + 1);
	if (buf == NULL){
		free(buf);
		InternetCloseHandle(url);
		InternetCloseHandle(internet);
		return false;
	}

	while (InternetReadFile(url, buf, BUFFER_SIZE, &bytesread))
		if (bytesread == 0) 
			break;
	
	out = string(buf);
	free(buf);
	InternetCloseHandle(url);
	InternetCloseHandle(internet);
	return true;
}





std::string get_this_client_hash(){
	return get_hash("Tibia.exe");
}

void replaceAll(std::string& str, const std::string& from, const std::string& to) {
	if (from.empty())
		return;
	size_t start_pos = 0;
	while ((start_pos = str.find(from, start_pos)) != std::string::npos) {
		str.replace(start_pos, from.length(), to);
		start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
	}
}

void create_shortcut(std::string version){
	/*ShortcutProvider sp;
	char current_dir[1024];
	ZeroMemory(current_dir, 1020);
	if (!GetCurrentDirectory(1024, current_dir))
		return;
	char* desktop = desktop_directory();
	if (desktop){
		CreateShortcut(&(std::string(".\\version_") + version + "\\" + std::string("smartbot.exe"))[0],
			"", &(std::string(desktop) + "\\Smart Bot " + version + ".lnk")[0], "", 
			&(std::string(current_dir) + "\\" + "version_" + version + "\\")[0], 0);
	}
	*/
	//HRESULT res = sp.Create(, "", &("Neutral Bot " + version)[0], "",0,], "", 0);
}

void start_version(std::string version){
	//create_shortcut(version);

	HMODULE hModule = GetModuleHandleA(NULL); 
	char path[MAX_PATH];
	GetModuleFileNameA(hModule, path, MAX_PATH);
	 
	boost::filesystem::path full_path = path;
	STARTUPINFO startupInfo = { 0 };

	startupInfo.cb = sizeof(startupInfo);
	PROCESS_INFORMATION processInformation;
	std::string full_path_str = full_path.branch_path().string();

	BOOL result = ::CreateProcess(
		0,
		&(full_path_str + "\\version_" + version + "\\smartbot.exe")[0],
		NULL,
		NULL,
		FALSE,
		NORMAL_PRIORITY_CLASS,
		NULL,
		&(full_path_str + "\\version_" + version + "\\")[0],
		&startupInfo,
		&processInformation
		);
	TerminateProcess(GetCurrentProcess(), NULL);
}

bool ExistModuleNameInProcess(DWORD processID, char* file){
	HMODULE hMods[1024];
	HANDLE hProcess;
	DWORD cbNeeded;
	unsigned int i;

	hProcess = OpenProcess(PROCESS_QUERY_INFORMATION | PROCESS_VM_READ | PROCESS_TERMINATE, FALSE, processID);
	if (NULL == hProcess)
		return false;

	if (EnumProcessModules(hProcess, hMods, sizeof(hMods), &cbNeeded)){
		for (i = 0; i < (cbNeeded / sizeof(HMODULE)); i++){
			TCHAR szModName[MAX_PATH];

			if (GetModuleFileNameEx(hProcess, hMods[i], szModName, sizeof(szModName) / sizeof(TCHAR)))
				if (strstr(szModName, file) != 0){
					CloseHandle(hProcess);
					return true;
				}
		}
	}

	CloseHandle(hProcess);
	return false;
}

void TerminateProcessWithModuleName(char* file){
	DWORD aProcesses[1024];
	DWORD cbNeeded;
	DWORD cProcesses;
	unsigned int i;

	int this_proc_id = GetCurrentProcessId();

	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
		return;

	cProcesses = cbNeeded / sizeof(DWORD);

	for (i = 0; i < cProcesses; i++){
		if (this_proc_id != aProcesses[i] && ExistModuleNameInProcess(aProcesses[i], file)){
			HANDLE hProcess;
			hProcess = OpenProcess(PROCESS_QUERY_INFORMATION |
				PROCESS_VM_READ | PROCESS_TERMINATE,
				FALSE, aProcesses[i]);
			TerminateProcess(hProcess, 0);
			CloseHandle(hProcess);
		}
	}
}

bool ExistProcessUsingFile(char* str){

	DWORD aProcesses[1024];
	DWORD cbNeeded;
	DWORD cProcesses;
	unsigned int i;
	int this_proc_id = GetCurrentProcessId();
	if (!EnumProcesses(aProcesses, sizeof(aProcesses), &cbNeeded))
		return false;
	cProcesses = cbNeeded / sizeof(DWORD);

	for (i = 0; i < cProcesses; i++)
		if (this_proc_id != aProcesses[i] && ExistModuleNameInProcess(aProcesses[i], str))
			return true;

	return false;
}


std::string string_to_lower(std::string str){
	std::transform(str.begin(), str.end(), str.begin(), ::tolower);
	return str;
}
bool need_restart = false;


#include <boost\filesystem.hpp>
using namespace boost::filesystem;

std::string find_file(path dir_path,         // in this directory,
	std::string file_hash){
	file_hash = StringToLower(file_hash);
	if (!exists(dir_path)) return false;
	directory_iterator end_itr; // default construction yields past-the-end
	for (directory_iterator itr(dir_path);
		itr != end_itr;
		++itr){
		if (is_directory(itr->status())){
			std::string found = find_file(itr->path(), file_hash);
			if (!found.empty())
				return found;
		}
		else{
			std::string hash = StringToLower(get_hash(itr->path().string()));
			if (hash == file_hash)
				return itr->path().string();
		}
	}
	return std::string();
}


void parse_all(std::string url){
	bar_text = "Trying to get instructions.";
	info this_ver_list;
	this_ver_list.hash = "";
	std::string ver = get_this_client_hash();

	TiXmlDocument doc;
	std::string document_as_std = "";

	if (!curl_download_string(&url[0], document_as_std, false)){
		bar_text = "Fail to get instructions.";
		can_login = false;
		return;
	}

	bar_text = "Parsing xml instructions.";
	doc.Parse((const char*)&document_as_std[0], 0, TIXML_ENCODING_UTF8);

	TiXmlNode * temp_node = doc.FirstChild("root");//root
	file temporal_file;
	if (temp_node){
		TiXmlElement * tem_element = temp_node->FirstChildElement("file");
		if (tem_element){
			temporal_file.hash = tem_element->Attribute("hash");
			temporal_file.link = tem_element->Attribute("link");
			temporal_file.name = tem_element->Attribute("name");
			temporal_file.path = tem_element->Attribute("path");
			this_ver_list.files.push_back(temporal_file);
			while (tem_element = tem_element->NextSiblingElement("file")){
				
				temporal_file.hash = tem_element->Attribute("hash");
				std::string hash = temporal_file.hash;
				std::cout << "\n h " << hash;
				temporal_file.link = tem_element->Attribute("link");
				temporal_file.name = tem_element->Attribute("name");
				if (tem_element->Attribute("path"))
					temporal_file.path = tem_element->Attribute("path");
				else
					std::cout << "\n miss";
				this_ver_list.files.push_back(temporal_file);
			}
		}
	}

	int hash_count = 0;
	for (auto it = this_ver_list.files.begin(); it != this_ver_list.files.end(); it++){
		bool file_status = false;
		for (int i = 0; i < 3; i++){
			std::string way;
			if (it->path != "")
				way = it->path + it->name;
			else
				way = it->name;

			if (StringToLower(get_hash(way)) != StringToLower(it->hash)){
				need_restart = true;
				bar_text = "Downloading " + it->name;
				hash_count++;
				/*if (string_to_lower(INJECT_API_X64) != string_to_lower(it->name) &&
					string_to_lower(INJECT_API_X32) != string_to_lower(it->name)){
					/*if (ExistProcessUsingFile(&it->name[0])){

						if (res != IDYES)
							TerminateProcess(GetCurrentProcess(), NULL);

						TerminateProcessWithModuleName(&it->name[0]);
					}*/
				//}
				if (!download_to_directory(it->path, it->link, it->hash, it->name, 0)){
					bar_text = "Fail to download " + it->name;
					can_login = false;
					return;
				}
			}
			else {
				file_status = true;
				break;
			}
		}
		if (!file_status){
			bar_text = "Fail to download " + it->name;
			can_login = false;
			return;
		}
	}

	bar_text = "All is okay. Enjoy the game!";
	can_login = true;
	Sleep(1000);

}

bool create_directory(std::string path){
	return false;
}

bool is_zip(std::string & entrace){
	if (entrace.find(".zip") == entrace.length() - 4)
		return true;
	return false;
}



version_file get_version_file_from_string(std::string xml_string){
	version_file retval;
	retval.valid = false;
	TiXmlDocument doc;

	doc.Parse(&xml_string[0], 0, TIXML_ENCODING_UTF8);

	TiXmlNode * temp_node = doc.FirstChild("root");//root
	file temporal_file;
	if (temp_node){
		TiXmlElement* tem_element = (TiXmlElement *)temp_node->FirstChild("versions");
		if (tem_element){
			tem_element = tem_element->FirstChildElement();
			while (tem_element){
				version_node v_node;
				v_node.before = tem_element->Attribute("before");
				v_node.next = tem_element->Attribute("next");
				v_node.current = tem_element->Attribute("current");
				retval.versions.push_back(v_node);
				tem_element = (TiXmlElement *)tem_element->NextSibling();
			}
		}
		TiXmlElement * latest = (TiXmlElement *)temp_node->FirstChild("latest");
		if (latest){
			retval.current_version = latest->Attribute("version");
			retval.valid = true;
		}
	}
	return retval;
}

version_file get_version_file(std::string url, std::string& out_xml){

	bar_text = "Trying get versions file.";


	curl_download_string(&url[0], out_xml, false);
	
	return get_version_file_from_string(out_xml);
}


void kill_thread(int time){
	Sleep(time);
	TerminateProcess(GetCurrentProcess(), 0);
}
uint32_t final_state = 0;
uint32_t get_final_state(){
	return final_state;
}

bool all_is_okay = false;
DWORD update_thread(){
	static bool thread1 = false;
	static bool thread2 = false;
	/*if (!thread1){
		thread1 = true;
		boost::thread(boost::bind(kill_thread, 1000 * 60 * 10)).detach();
	}
	*/
	//extern MyDlg dlg;
	version_file v_info;
	std::string last_versions_xml = "";
	
	while (true){
		for (int i = 0; i < 10; i++){
			std::ostringstream os;
			os << i;
			v_info = get_version_file("http://update1.tibiasmartbot.com/get_versions.php", last_versions_xml);
			if (v_info.valid){
				final_state = 2;
				break;
			}
				
		}

		if (!v_info.valid){
			bar_text = "Fail to update.";
			break;
		//	goto cont;
		}

		FILE* file_latest_versions = fopen("versions.xml", "w+");

		if (file_latest_versions){
			fwrite(&last_versions_xml[0], 1, last_versions_xml.length(), file_latest_versions);
			fclose(file_latest_versions);
		}



		/*((CComboBox*)dlg.GetDlgItem(IDC_COMBO2))->ResetContent();
		for (auto version = v_info.versions.begin(); version != v_info.versions.end(); version++)
			((CComboBox*)dlg.GetDlgItem(IDC_COMBO2))->AddString(&version->current[0]);
		
		((CComboBox*)dlg.GetDlgItem(IDC_COMBO2))->SetWindowTextA(&v_info.current_version[0]);
		*/
		bar_text = "Checking latest version...";



		for (int i = 0; i < 10; i++){
			std::ostringstream os;
			os << i;

			parse_all("http://update1.tibiasmartbot.com/update_launcher.php");

			if (can_login){
				break;
			}
		}

		if (need_restart){
			STARTUPINFO startupInfo = { 0 };
			startupInfo.cb = sizeof(startupInfo);

	
			LPSTR cmdArgs = " restart";
			if (is_hidden){
				cmdArgs = " restarthidden";
			}
			PROCESS_INFORMATION processInformation;
			BOOL result = ::CreateProcess(
				"SmartLauncher.exe",
				cmdArgs,
				NULL,
				NULL,
				FALSE,
				NORMAL_PRIORITY_CLASS,
				NULL,
				".\\",
				&startupInfo,
				&processInformation
				);

			TerminateProcess(GetCurrentProcess(), 0);
		}
		
		parse_all("http://update1.tibiasmartbot.com/update.php?ver=" + v_info.current_version);

		if (can_login){
			all_is_okay = true;
			bar_text = "Latest version complete!";
			start_version(v_info.current_version);
			final_state = 1;
			break;
		}

		/*for (int i = 0; i < 10; i++){
			std::ostringstream os;
			os << i;

			parse_all("http://server" + os.str() + ".neutralbot.net/repo/update.php?ver=" + v_info.current_version);

			if (can_login){
				all_is_okay = true;
				bar_text = "Latest version complete!";
				break;
			}
		}*/

		if (version_to_update != ""){
			bar_text = "Checking version" + version_to_update + "...";
			parse_all("http://update1.tibiasmartbot.com/update.php?ver=" + v_info.current_version);

			if (can_login){
				bar_text = "Version " + version_to_update + " complete!";
				start_version(version_to_update);
				version_to_update = "";
				final_state = 1;
				
				break;
			}
			/*for (int i = 0; i < 10; i++){
				std::ostringstream os;
				os << i;

				parse_all("http://server" + os.str() + ".neutralbot.net/repo/update.php?ver=" + v_info.current_version);

				if (can_login){
					bar_text = "Version " + version_to_update + " complete!";
					version_to_update = "";
					break;
				}
			}*/
		}
	//cont:
	/*	if (!thread2){
			thread2 = true;
			boost::thread(boost::bind(kill_thread, 1000 * 60 * 3)).detach();
		}*/
		//Sleep(20000);
	}
	return 0;
}

void start_update_thread(){
	CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)update_thread, NULL, NULL, NULL);
}

void update_percent(unsigned long total, unsigned long now){
	extern void myprogess_set(int);
	if (!total) total = 1;
	if (!now) now = 1;
	percent = total * 100 / now;
}

inline bool exists_test0(const std::string& name) {
	ifstream f(name.c_str());
	if (f.good()) {
		f.close();
		return true;
	}
	else {
		f.close();
		return false;
	}
}
std::string GetOwnFileName(){
	TCHAR szFileName[MAX_PATH];
	GetModuleFileName(NULL, szFileName, MAX_PATH);
	return szFileName;
}

std::string get_disponible_name(){
	std::string temp_name = "rnamefile";
	int index = 0;
	while (true){
		std::string tmp_file = temp_name + std::to_string((long long)index);
		if (!boost::filesystem::exists(tmp_file))
			return tmp_file;
		index++;
	}
	return "";
}

void remove_reserved_files(){
	std::string temp_name = "rnamefile";
	int index = 0;
	while (true){
		std::string tmp_file = temp_name + std::to_string((long long)index);
		if (!boost::filesystem::exists(tmp_file)){
			break;
		}

		boost::system::error_code error;
		boost::filesystem::remove(tmp_file, error);
		index++;
	}
}

void check_create_dir(std::string dir_full_path){
	try{
		boost::filesystem::create_directories(dir_full_path);
	}
	catch (...){}
}


uint32_t get_percent(){
	return percent;
}
std::string get_state(){
	return bar_text;
}


bool download_to_directory(std::string directory, std::string & url, std::string hash, std::string finalname, int trytimes)
{
	remove_reserved_files();
	boost::system::error_code error;
	boost::filesystem::remove("tmpfilex", error);

	if (directory != "" && directory != "./"){
		char current_dir[1024];
		ZeroMemory(current_dir, 1024);
		GetCurrentDirectory(1024, current_dir);
		check_create_dir(current_dir + std::string("\\") + directory);
	}

	if (trytimes > 2)
		return false;


	std::string local_found_file = find_file(".\\", hash);
	if (!local_found_file.empty()){
		if (!CopyFile(&local_found_file[0],".\\tmpfilex",false))
			if (!curl_download(&url[0], true, update_percent_curl, "tmpfilex", false) 
				|| StringToLower(get_hash("tmpfilex")) != hash){
			trytimes++;
			return download_to_directory("", url, hash, finalname, trytimes);
			}
	}
	else {
		
		if (!curl_download(&url[0], true, update_percent_curl, "tmpfilex", false) || StringToLower(get_hash("tmpfilex")) != hash){
			trytimes++;
			std::cout << "\n HASHES " << StringToLower(get_hash("tmpfilex")) << ":" << hash;
			return download_to_directory("", url, hash, finalname, trytimes);
		}
		std::cout << "\n HASHES " << StringToLower(get_hash("tmpfilex")) << ":" << hash;
	}

	if (boost::filesystem::exists(directory + finalname)){
		boost::filesystem::remove(directory + finalname, error);
		if (!error)
			return MoveFileEx("tmpfilex", &(directory + finalname)[0], MOVEFILE_REPLACE_EXISTING);

		std::string disponible_name = get_disponible_name();
		if (!MoveFileEx(&(directory + finalname)[0], &(".\\" + disponible_name)[0], MOVEFILE_REPLACE_EXISTING))
			return false;
		return MoveFileEx("tmpfilex", &(directory + finalname)[0], MOVEFILE_REPLACE_EXISTING);
	}
	else
		return MoveFileEx("tmpfilex", &(directory + finalname)[0], MOVEFILE_REPLACE_EXISTING);
}


void unzip(std::string zipfile, std::string outname){
	HZIP hz;
	hz = OpenZip(&zipfile[0], 0);
	SetUnzipBaseDir(hz, _T(".\\"));
	ZIPENTRY ze;
	GetZipItem(hz, -1, &ze);
	int numitems = ze.index;
	for (int zi = 0; zi < numitems; zi++){
		GetZipItem(hz, zi, &ze);
		if (ze.name == outname){
			rename(&outname[0], "tempfilex");
			remove("tempfilex");
			UnzipItem(hz, zi, ze.name);
			break;
		}
	}
	CloseZip(hz);
	remove(&zipfile[0]);
}
