#include "Updater.h"
#include <Windows.h>
#include "Downloader.h"


Updater::Updater(){
	_has_finished = false;
	_has_started = false;
}

bool Updater::get_current_state(std::string& file_name, uint32_t& percent){
	percent = get_percent();
	file_name = get_state();
	return true;
}

Updater* Updater::get(){
	static Updater* mUpdater = nullptr;
	if (!mUpdater){
		mUpdater = new Updater();
	}
	return mUpdater;
}

void Updater::start(){
	if (_has_started)
		return;
	_has_started = true;
	start_update_thread();
}

bool Updater::has_finished(){
	return get_final_state() == 1;
}
