#pragma once
#ifndef DOWNLOAD_H
#define DOWNLOAD_H

//#include "stdafx.h"

//#include "ApiFiles.h"

#include "hashgen.h"
#include "xml.h"
#include <stdint.h>
#include <string>

#include <Windows.h>
//#include "resource.h"





extern bool can_login;
extern std::string bar_text;
extern int percent;

using namespace std;
bool curl_download_string(char* url, std::string& out, bool use_ftp);
bool curl_download(char * url, bool reload/*ignore*/, void(*update)(unsigned long, unsigned long), char* out_filename, bool use_ftp);
#define MAX_ERRMSG_SIZE 80
#define MAX_FILENAME_SIZE 512
#define BUF_SIZE 10240           // 10 KB
#define HTTP_USER_AGENT "HTTPRIP"
#define BUFFER_SIZE 10240
bool download_to_directory(std::string directory, std::string & url, std::string hash, std::string finalname, int trytimes);
void unzip(std::string zipfile, std::string outname);

std::string StringToLower(char * entrace);

std::string StringToLower(std::string & entrace);

int download_string(char * entrace_url, string & out);




#endif


std::string get_this_client_hash();

void replaceAll(std::string& str, const std::string& from, const std::string& to);

void create_shortcut(std::string version);

void start_version(std::string version);

bool ExistModuleNameInProcess(DWORD processID, char* file);

void TerminateProcessWithModuleName(char* file);

bool ExistProcessUsingFile(char* str);


std::string string_to_lower(std::string str);

extern HWND main_hwnd;
extern bool can_login;
extern std::string bar_text;
extern int percent;

void parse_all(std::string url);

bool create_directory(std::string path);

bool is_zip(std::string & entrace);




DWORD update_thread();

void start_update_thread();

void update_percent(unsigned long total, unsigned long now);

inline bool exists_test0(const std::string& name);

std::string GetOwnFileName();

void update_percent_curl(unsigned long total, unsigned long now);

std::string get_disponible_name();

void remove_reserved_files();

void check_create_dir(std::string dir_full_path);

bool download_to_directory(std::string directory, std::string & url, std::string hash, std::string finalname, int trytimes);

void unzip(std::string zipfile, std::string outname);


std::string StringToLower(char * entrace);

std::string StringToLower(std::string & entrace);


uint32_t get_percent();
std::string get_state();
uint32_t get_final_state ();