#pragma once
#include "stdafx.h"
#include <string>
#include <iostream>
#include <ostream>
#include <sstream>
#include <NoPingShared\defines_all.h>
#include "hashgen.h"

extern HWND main_hwnd;
bool UpdateRegister(bool& did_action);
bool RequestUserRestartAfterUpdate();
extern bool reboot_permission;
extern bool reboot_after_update ;


#define USE_APP_INIT_DLL TRUE
#define UPDATE_ROUTE_DLL FALSE
#define USE_APP_INIT_ERROR FALSE

#if USE_APP_INIT_DLL

#define CHECK_RESET_PERMISSION if(reboot_after_update && !reboot_permission)\
{reboot_permission=RequestUserRestartAfterUpdate();if(!reboot_permission)TerminateProcess(GetCurrentProcess(), NULL);}

#else

#define CHECK_RESET_PERMISSION

#endif

bool GetSecureBootState();

BOOL Is64BitWindows();
#define INJECT_API_X64 "NpLoadApi64.dll"
#define INJECT_API_X32 "NpLoadApi32.dll"
#define LOADER_X32_DLL "route32.dll"
#define LOADER_X64_DLL "route64.dll"

std::string GetSystemVolumine();
std::string get_hash(std::string file);
bool CheckFileHash(std::string fileName);
bool UpdateFile(std::string fileName);
bool HasSomeFileToUpdate();
bool UpdateAllFiles();
bool RequestUserRestartAfterUpdate();
void CleanOldUpdateFiles();
bool CheckUpdateLocalDriverFiles();
int exist_mem(char* block_1, int size_1, char* block_2, int size_2);
bool SetRegisterApiKey(bool x64, bool& did_action);
bool UpdateRegister(bool& did_action);
bool GetSecureBootState();


int RegisterStartup(std::string file_path);