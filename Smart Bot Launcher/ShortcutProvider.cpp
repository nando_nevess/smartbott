

#include "ShortcutProvider.h"

HRESULT ShortcutProvider::Create(LPSTR pszTargetfile, LPSTR pszTargetargs,
	LPSTR pszLinkfile, LPSTR pszDescription,
	int iShowmode, LPSTR pszCurdir,
	LPSTR pszIconfile, int iIconindex)
{
	HRESULT       hRes;                  /* Returned COM result code */
	IShellLink*   pShellLink;            /* IShellLink object pointer */
	IPersistFile* pPersistFile;          /* IPersistFile object pointer */
	WCHAR wszLinkfile[MAX_PATH]; /* pszLinkfile as Unicode
								 string */
	int           iWideCharsWritten;     /* Number of wide characters
										 written */
	CoInitialize(NULL);
	hRes = E_INVALIDARG;
	if (
		(pszTargetfile != NULL) && (strlen(pszTargetfile) > 0) &&
		(pszTargetargs != NULL) &&
		(pszLinkfile != NULL) && (strlen(pszLinkfile) > 0) &&
		(pszDescription != NULL) &&
		(iShowmode >= 0) &&
		(pszCurdir != NULL) &&
		(pszIconfile != NULL) &&
		(iIconindex >= 0)
		)
	{
		hRes = CoCreateInstance(
			CLSID_ShellLink,     /* pre-defined CLSID of the IShellLink
								 object */
			 NULL,                 /* pointer to parent interface if part of
													   aggregate */
			CLSCTX_INPROC_SERVER, /* caller and called code are in same
			process */
			IID_IShellLink,      /* pre-defined interface of the
			IShellLink object */
			(LPVOID*)&pShellLink);         /* Returns a pointer to the IShellLink
																												object */
		if (SUCCEEDED(hRes))
		{
			/* Set the fields in the IShellLink object */
			hRes = pShellLink->SetPath(pszTargetfile);
			hRes = pShellLink->SetArguments(pszTargetargs);
			if (strlen(pszDescription) > 0)
			{
				hRes = pShellLink->SetDescription(pszDescription);
			}
			if (iShowmode > 0)
			{
				hRes = pShellLink->SetShowCmd(iShowmode);
			}
			if (strlen(pszCurdir) > 0)
			{
				hRes = pShellLink->SetWorkingDirectory(pszCurdir);
			}
			if (strlen(pszIconfile) > 0 && iIconindex >= 0)
			{
				hRes = pShellLink->SetIconLocation(pszIconfile, iIconindex);
			}

			/* Use the IPersistFile object to save the shell link */
			hRes = pShellLink->QueryInterface(
				IID_IPersistFile,         /* pre-defined interface of the
										  IPersistFile object */
										  (LPVOID*)&pPersistFile);            /* returns a pointer to the
																			  IPersistFile object */
			if (SUCCEEDED(hRes))
			{
				iWideCharsWritten = MultiByteToWideChar(CP_ACP, 0,
					pszLinkfile, -1,
					wszLinkfile, MAX_PATH);
				hRes = pPersistFile->Save(wszLinkfile, TRUE);
				pPersistFile->Release();
			}
			pShellLink->Release();
		}

	}
	CoUninitialize();
	return (hRes);
}






/**********************************************************************
* Function......: CreateShortcut
* Parameters....: lpszFileName - string that specifies a valid file name
*          lpszDesc - string that specifies a description for a
shortcut
*          lpszShortcutPath - string that specifies a path and
file name of a shortcut
* Returns.......: S_OK on success, error code on failure
* Description...: Creates a Shell link object (shortcut)
**********************************************************************/
HRESULT CreateShortcut(/*in*/ LPCTSTR lpszFileName,
	/*in*/ LPCTSTR lpszDesc,
	/*in*/ LPCTSTR lpszShortcutPath,
	/*in*/ LPCTSTR lpszArguments,
	/*in*/ LPCTSTR lpszWorkingDir,
	/*in*/ int iShowCmd
	)
{
	//::CoInitialize(NULL);
	HRESULT hRes = E_FAIL;
	DWORD dwRet = 0;

	ATL::CComPtr<IShellLink> ipShellLink;
	// buffer that receives the null-terminated string 

	// for the drive and path

	TCHAR szPath[MAX_PATH];
	// buffer that receives the address of the final 

	//file name component in the path

	LPTSTR lpszFilePart;
	WCHAR wszTemp[MAX_PATH];

	// Retrieve the full path and file name of a specified file

	dwRet = GetFullPathName(lpszFileName,
		sizeof(szPath) / sizeof(TCHAR),
		szPath, &lpszFilePart);
	if (!dwRet)
		return hRes;

	// Get a pointer to the IShellLink interface

	hRes = CoCreateInstance(CLSID_ShellLink,
		NULL,
		CLSCTX_INPROC_SERVER,
		IID_IShellLink,
		(void**)&ipShellLink);

	if (SUCCEEDED(hRes))
	{
		// Get a pointer to the IPersistFile interface

		CComQIPtr<IPersistFile> ipPersistFile(ipShellLink);

		// Set the path to the shortcut target and add the description

		hRes = ipShellLink->SetPath(szPath);
		if (FAILED(hRes))
			return hRes;

		hRes = ipShellLink->SetDescription(lpszDesc);
		
		if (FAILED(hRes)){
			return hRes;
		}

		hRes = ipShellLink->SetWorkingDirectory(lpszWorkingDir);
		if (FAILED(hRes)){
			return hRes;
		}


		hRes = ipShellLink->SetArguments(lpszArguments);
		if (FAILED(hRes)){
			return hRes;
		}

		hRes = ipShellLink->SetShowCmd(iShowCmd);
		if (FAILED(hRes)){
			return hRes;
		}

		// IPersistFile is using LPCOLESTR, so make sure 

		// that the string is Unicode

#if !defined _UNICODE
		MultiByteToWideChar(CP_ACP, 0,
			lpszShortcutPath, -1, wszTemp, MAX_PATH);
#else
		wcsncpy_s(wszTemp, lpszShortcutPath, MAX_PATH);
#endif

		// Write the shortcut to disk

		hRes = ipPersistFile->Save(wszTemp, TRUE);
	}
	//::CoUninitialize();
	return hRes;
}


char* desktop_directory()
{
	static TCHAR path[MAX_PATH + 1];
	if (SHGetSpecialFolderPath(HWND_DESKTOP, path, CSIDL_DESKTOPDIRECTORY, FALSE))
		return path;
	else
		return 0;
}