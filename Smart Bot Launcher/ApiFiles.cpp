#include "ApiFiles.h"

bool reboot_permission = false;
bool reboot_after_update = false;


BOOL Is64BitWindows()
{
#if defined(_WIN64)
	return TRUE;  // 64-bit programs run only on Win64
#elif defined(_WIN32)
	// 32-bit programs run on both 32-bit and 64-bit Windows
	// so must sniff
	BOOL f64 = FALSE;
	return IsWow64Process(GetCurrentProcess(), &f64) && f64;
#else
	return FALSE; // Win64 does not support Win16
#endif
}
#define INJECT_API_X64 "NpLoadApi64.dll"
#define INJECT_API_X32 "NpLoadApi32.dll"
#define LOADER_X32_DLL "route32.dll"
#define LOADER_X64_DLL "route64.dll"

std::string GetSystemVolumine(){
	TCHAR buffer[100];
	GetSystemWindowsDirectory(buffer, 100);
	buffer[2] = 0;
	return std::string(buffer);
}

std::string get_hash(std::string file)
{
	SetLastError(0);
	FILE * f = fopen(&file[0], "r");
	int err = GetLastError();
	//If couldnt open specifier file or some another error
	if (!f)
		return "";
	fclose(f);

	MD5 my_md5;
	std::string hash = std::string(my_md5.digestFile(&file[0]));
	return hash;
}

bool CheckFileHash(std::string fileName){
	std::string HASH1 = get_hash(GetSystemVolumine() + "\\NpAPI\\" + fileName);
	std::string HASH2 = get_hash(fileName);
	return HASH1 == HASH2;
}
bool UpdateFile(std::string fileName){
	if (CheckFileHash(fileName))
		return true;
	std::string destination = GetSystemVolumine() + "\\NpAPI\\" + fileName;
	//check file exists
	FILE* f = fopen(&destination[0], "r");
	if (f){
		fclose(f);
		std::string destination_old = GetSystemVolumine() + "\\NpAPI\\" + "old_" + fileName;;
		if (rename(&destination[0], &destination_old[0]))
			return false;
	}

	return 0 != CopyFile(&fileName[0], &destination[0], false);
}
bool HasSomeFileToUpdate(){
#if !USE_APP_INIT_DLL
	return false;
#endif
	if (!CheckFileHash(INJECT_API_X64))
		return true;
	if (!CheckFileHash(INJECT_API_X32))
		return true;

#if UPDATE_ROUTE_DLL
	if (!CheckFileHash(LOADER_X32_DLL))
		return true;
	if (!CheckFileHash(LOADER_X64_DLL))
		return true;
#endif

	return false;
}
bool UpdateAllFiles(){
#if !USE_APP_INIT_DLL
	return true;
#endif
	if (!UpdateFile(INJECT_API_X64))
		return false;
	if (!UpdateFile(INJECT_API_X32))
		return false;

#if UPDATE_ROUTE_DLL
	if (!UpdateFile(LOADER_X32_DLL))
		return false;
	if (!UpdateFile(LOADER_X64_DLL))
		return false;
#endif

	return true;
}
bool RequestUserRestartAfterUpdate(){
	if (!reboot_after_update)
		return true;
	int res = MessageBox(main_hwnd, "Application need to restart your machine after update want to to procced?",
		"Atention!", MB_YESNO | MB_TOPMOST);
	return res == IDYES;
}
void CleanOldUpdateFiles(){
	remove(&(GetSystemVolumine() + "\\NpAPI\\" + std::string("old_") + INJECT_API_X64)[0]);
	remove(&(GetSystemVolumine() + "\\NpAPI\\" + std::string("old_") + INJECT_API_X32)[0]);
	remove(&(GetSystemVolumine() + "\\NpAPI\\" + std::string("old_") + LOADER_X32_DLL)[0]);
	remove(&(GetSystemVolumine() + "\\NpAPI\\" + std::string("old_") + LOADER_X64_DLL)[0]);
}
bool CheckUpdateLocalDriverFiles(){
	//if is secure boot, windows 8+ will not use LoadApi in start of applications then its not necessaery
	//and our main application will have to handle this sitiation injecting code at running time.
	/*if (GetSecureBootState()){
	return true;
	}*/

	std::string dir = GetSystemVolumine() + "\\NpAPI";
	if (!(CreateDirectory(&dir[0], NULL) || ERROR_ALREADY_EXISTS == GetLastError())){
#if USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "FAIL IN CREATE API DIRECTORY", "Error", MB_OK | MB_TOPMOST);
		TerminateProcess(GetCurrentProcess(), NULL);
#else
		return true;
#endif
	}

	bool did_action = false;
	bool did_update = UpdateRegister(did_action);
	if (!did_update){
#if USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "FAIL IN REGISTER API OPERATIONS", "Error", MB_OK | MB_TOPMOST);
		TerminateProcess(GetCurrentProcess(), NULL);
#else
		return true;
#endif
	}

	CleanOldUpdateFiles();
	if (!HasSomeFileToUpdate()){
		if (reboot_after_update && did_action && USE_APP_INIT_DLL){
			WinExec("shutdown -r -f -t 5", SW_HIDE);
			MessageBox(main_hwnd, "System will reboot now!", "Launcher", MB_OK | MB_TOPMOST);
			TerminateProcess(GetCurrentProcess(), NULL);
		}
		return true;
	}

	if (!RequestUserRestartAfterUpdate()){
		if (reboot_after_update){
#if USE_APP_INIT_ERROR
			MessageBox(main_hwnd, "Fail request update!", "Launcher", MB_OK | MB_TOPMOST);
			TerminateProcess(GetCurrentProcess(), NULL);
#else
			return true;
#endif
		}
	}
	if (!UpdateAllFiles()){
#if USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "FAIL TO UPDATE API FILES", "Launcher - Error", MB_OK | MB_TOPMOST);
		TerminateProcess(GetCurrentProcess(), NULL);
#else
		return true;
#endif
	}
	bool has_file_to_update = (HasSomeFileToUpdate());
	if (has_file_to_update){
#if USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "FAIL IN MOVE API FILE OPERATIONS", "Launcher - Error", MB_OK | MB_TOPMOST);
		TerminateProcess(GetCurrentProcess(), NULL);
#else
		return true;
#endif
	}
	if (USE_APP_INIT_DLL){
		if (reboot_after_update){
			WinExec("shutdown -r -f -t 00", SW_HIDE);
			MessageBox(main_hwnd, "System will reboot now!", "Launcher", MB_OK | MB_TOPMOST);
		}


	}
	//TerminateProcess(GetCurrentProcess(), NULL);
	return true;
}


int exist_mem(char* block_1, int size_1, char* block_2, int size_2){
	if (!size_2)
		return -1;
	if (!size_1)
		return -1;
	for (int i = 0; i < size_1 - size_2 + 1; i++)
		if (memcmp(&block_1[i], &block_2[0], size_2) == 0)
			return i;
	return -1;
}
bool SetRegisterApiKey(bool x64, bool& did_action){
	did_action = false;
	char* strKey = "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Windows";
	std::string DLL_PATH;
	if (x64)
		DLL_PATH = GetSystemVolumine() + "\\NpAPI\\NpLoadApi64.dll";
	else
		DLL_PATH = GetSystemVolumine() + "\\NpAPI\\NpLoadApi32.dll";

	HKEY hRootKey = HKEY_LOCAL_MACHINE;
	HKEY hKey;
	LONG nError;
	if (x64)
		nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS | KEY_WOW64_64KEY, &hKey);
	else
		nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND){
		if (x64)
			nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE,
			KEY_ALL_ACCESS | KEY_WOW64_64KEY, NULL, &hKey, NULL);
		else
			nError = RegCreateKeyEx(hRootKey, strKey, NULL, NULL, REG_OPTION_NON_VOLATILE,
			KEY_ALL_ACCESS, NULL, &hKey, NULL);
	}
	if (nError){
		std::ostringstream os;
		os << "Fails To Create API REGISTER please contact support!" << "\n WITH ERROR CODE " <<
			nError;
#if USE_APP_INIT_DLL && USE_APP_INIT_ERROR

		MessageBox(main_hwnd, &os.str()[0], "Error", MB_OK | MB_TOPMOST);
		return false;
#else 
		return true;
#endif
	}
	char buffer[60000];
	DWORD size = sizeof(DWORD);
	DWORD type = REG_DWORD;

	DWORD data = 0;
	//LoadAppInit_DLLs
	nError = RegQueryValueEx(hKey, "LoadAppInit_DLLs", NULL, &type, (LPBYTE)&data, &size);
	if (nError && nError != ERROR_FILE_NOT_FOUND){
#if USE_APP_INIT_DLL && USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "Fails To GET DWORD STATUS API VALUE please contact support!", "Error", MB_OK | MB_TOPMOST);
		return false;
#else
		return true;
#endif
	}
	if (data != 1){
		CHECK_RESET_PERMISSION
			data = 1;
		nError = RegSetValueEx(hKey, "LoadAppInit_DLLs", NULL, REG_DWORD, (LPBYTE)&data, sizeof(DWORD));
		if (nError){
#if USE_APP_INIT_DLL && USE_APP_INIT_ERROR
			MessageBox(main_hwnd, "Fails To Set DWORD STATUS API VALUE please contact support!", "Error", MB_OK | MB_TOPMOST);
			return false;
#else
			return true;
#endif
		}
		did_action = true;
	}
	data = 1;
	//LoadAppInit_DLLs
	nError = RegQueryValueEx(hKey, "RequireSignedAppInit_DLLs", NULL, &type, (LPBYTE)&data, &size);
	if (nError && nError != ERROR_FILE_NOT_FOUND){
#if USE_APP_INIT_DLL && USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "Fails To GET DWORD STATUS API VALUE please contact support!", "Error", MB_OK | MB_TOPMOST);
		return false;
#else 
		return true;
#endif
	}
	if (data){
		CHECK_RESET_PERMISSION
			data = 0;
		nError = RegSetValueEx(hKey, "RequireSignedAppInit_DLLs", NULL, REG_DWORD, (LPBYTE)&data, sizeof(DWORD));
		if (nError){
#if USE_APP_INIT_ERROR
			MessageBox(main_hwnd, "Fails To Set DWORD STATUS API VALUE please contact support!", "Error", MB_OK | MB_TOPMOST);
			return false;
#else
			return true;
#endif
		}
		did_action = true;
	}


	//AppInit_DLLs += path of dll
	size = 60000;	type = REG_SZ;
	nError = RegQueryValueEx(hKey, "AppInit_DLLs", NULL, &type, (LPBYTE)&buffer, &size);

	if (nError == ERROR_FILE_NOT_FOUND)
		size = 0; // The value will be created and set to data next time SetVal() is called.
	else if (nError){
#if USE_APP_INIT_DLL && USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "Fails To QUERY INFO API VALUE please contact support!", "Error", MB_OK | MB_TOPMOST);
		return false;
#else
		return true;
#endif
	}
	while (size > 0 && (buffer[size - 1] == ' ' || buffer[size - 1] == '\n' || buffer[size - 1] == 0))
		size--;
	int pos;
	if ((pos = exist_mem(buffer, size, &DLL_PATH[0], DLL_PATH.length())) != -1){
#if USE_APP_INIT_DLL
		return true;
	}
#else
		int full_message_len = strlen(buffer);
		int dll_path_len = strlen(&DLL_PATH[0]);
		if (full_message_len == dll_path_len){
			buffer[0] = 0;
			goto jump;
		}
		//remove USE_APP_INIT_DLL
		if (pos == 0){
			int first_delimiter_place = -1;

			for (int i = 0; i < full_message_len; i++)
				if (buffer[i] == ','){
				first_delimiter_place = i;
				break;
				}
			int rest_len;
			if (first_delimiter_place != -1)
				rest_len = (full_message_len - first_delimiter_place + 1);
			else
				rest_len = (full_message_len - dll_path_len);

			for (int i = 0; i < rest_len; i++)
				buffer[i] = buffer[i + first_delimiter_place + 1];

			buffer[rest_len] = 0;
			goto jump;
		}
		else {
			int after_pos = pos + dll_path_len;
			int after_len = (full_message_len - after_pos);

			int demiliter_before_pos = 0;
			for (int i = pos; i > 0; i--)
				if (buffer[i] == ','){
				demiliter_before_pos = i;
				break;
				}

			for (int i = demiliter_before_pos; i <= (demiliter_before_pos + after_len); i++){

				if (i < (demiliter_before_pos + after_len))
					buffer[i] = buffer[pos + dll_path_len + i];
				else
					buffer[i] = 0;

			}

			if (std::string(buffer) != "")
				goto jump;

			return true;
		}
		//just a label
	jump:;
}
	else
		return true;
#endif
	//eliminate end zeroes

#if USE_APP_INIT_DLL
	buffer[size] = ','; size++;
	memcpy(&buffer[size], &DLL_PATH[0], DLL_PATH.length()); size += DLL_PATH.length();
	DLL_PATH[size] = 0;
#else
	size = strlen(buffer);
#endif

	CHECK_RESET_PERMISSION
		nError = RegSetValueEx(hKey, "AppInit_DLLs", NULL, REG_SZ, (LPBYTE)&buffer, size);
	if (nError){
#if USE_APP_INIT_DLL && USE_APP_INIT_ERROR
		MessageBox(main_hwnd, "Fails To Set REG_SZ API please contact support!", "Error", MB_OK | MB_TOPMOST);
		return false;
#else 
		return true;
#endif
	}
#if USE_APP_INIT_DLL
	did_action = true;
#endif
	return true;
}
bool UpdateRegister(bool& did_action){
	bool res = false;
	bool tmp_did_action = false;
	bool is_x64_bits = 0 != Is64BitWindows();
	if (is_x64_bits){
		res = SetRegisterApiKey(true, tmp_did_action);
		if (!res)
			return false;
	}

	if (tmp_did_action)
		did_action = true;

	res = SetRegisterApiKey(false, tmp_did_action);
	if (tmp_did_action)
		did_action = true;

	return res;
}
bool GetSecureBootState(){
	bool state = true;
	HKEY hRootKey = HKEY_LOCAL_MACHINE;
	HKEY hKey;

	char* strKey = "SYSTEM\\CurrentControlSet\\Control\\SecureBoot\\State";
	LONG nError = RegOpenKeyEx(hRootKey, strKey, NULL, KEY_ALL_ACCESS, &hKey);

	if (nError == ERROR_FILE_NOT_FOUND)
		return false;
	else if (nError)
		return true;



	DWORD size = sizeof(DWORD);
	DWORD type = REG_DWORD;
	DWORD data = 0;
	//UEFISecureBootEnabled
	nError = RegQueryValueEx(hKey, "UEFISecureBootEnabled", NULL, &type, (LPBYTE)&data, &size);
	if (!nError)
		return data == 1;

	else if (nError == ERROR_FILE_NOT_FOUND)
		return false;

	return true;
}



int RegisterStartup(std::string file_path){
	TCHAR exepath[MAX_PATH];
	GetModuleFileName(0, exepath, MAX_PATH);
	HKEY hKey;
	LONG lnRes = RegOpenKeyEx(
		HKEY_CURRENT_USER,
		"SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run",
		0, KEY_WRITE,
		&hKey
		);
	if (ERROR_SUCCESS == lnRes)
	{
		lnRes = RegSetValueEx(hKey,
			"Smart Bot Launcher",
			0,
			REG_SZ,
			(const BYTE*)&file_path[0],
			file_path.length());
	}

	system("PAUSE");
	return 0;
}