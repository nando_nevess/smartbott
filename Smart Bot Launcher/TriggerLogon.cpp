#include "TriggerLogon.h"

#define _WIN32_DCOM
#include "ApiFiles.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <comdef.h>
#include <Sddl.h>

#include <boost/filesystem/operations.hpp>
#include <boost/filesystem/path.hpp>

//  Include the task header file.
#include <taskschd.h>
#pragma comment(lib, "taskschd.lib")
#pragma comment(lib, "comsupp.lib")
#pragma comment(lib, "Advapi32.lib")


using namespace std;

bool IsWin7OrLater() {
	DWORD version = GetVersion();
	DWORD major = (DWORD)(LOBYTE(LOWORD(version)));
	DWORD minor = (DWORD)(HIBYTE(LOWORD(version)));
	return (major > 6) || ((major == 6) && (minor >= 0));
}

bool CreateLauncherTrigger(){
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	HMODULE hModule = GetModuleHandleA(NULL);
	char path[MAX_PATH];
	GetModuleFileNameA(hModule, path, MAX_PATH);
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	boost::filesystem::path full_path = path;
	if (!IsWin7OrLater())
		return RegisterStartup("\"" + full_path.branch_path().string() + "\\startup.exe" + "\"");
	else
		return CreateTask("NbTray", &("\"" + full_path.branch_path().string() + "\\startup.exe" + "\"")[0] , "");
}


bool CreateTask(char* szTaskName, char* pszPath, char* pszArg){

	BSTR bstrszTaskName = _com_util::ConvertStringToBSTR(szTaskName);
	BSTR bstrpszPath = _com_util::ConvertStringToBSTR(pszPath);
	BSTR bstrpszArg = _com_util::ConvertStringToBSTR(pszArg);

	ITaskService *pService;
	ITaskFolder *pRootFolder;
	ITaskDefinition *pTask;
	IRegistrationInfo *pRegInfo;
	IPrincipal *pPrincipal;
	ITaskSettings *pSettings;
	ITriggerCollection *pTriggerCollection;
	ITrigger *pTrigger;
	ILogonTrigger *pLogonTrigger;
	IActionCollection *pActionCollection;
	IAction *pAction;
	IExecAction *pExecAction;
	IRegisteredTask *pRegisteredTask;
	VARIANT var = { VT_EMPTY };
	bool triggersucc = false;
	bool actionsucc = false;
	bool succ = false;
	//std::cout << "\n  " << __LINE__;

	HRESULT hr = CoInitializeEx(NULL, COINIT_MULTITHREADED);
	if (FAILED(hr)){
		printf("\nCoInitializeEx failed: %x", hr);
		return 1;
	}
	//std::cout << "\n  " << __LINE__;
	hr = CoInitializeSecurity(
		NULL,
		-1,
		NULL,
		NULL,
		RPC_C_AUTHN_LEVEL_PKT_PRIVACY,
		RPC_C_IMP_LEVEL_IMPERSONATE,
		NULL,
		0,
		NULL);
	//std::cout << "\n  " << __LINE__;
	if (FAILED(hr)){
		printf("\nCoInitializeSecurity failed: %x", hr);
		CoUninitialize();

		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);

		return 1;
	}
	//std::cout << "\n  " << __LINE__;
	if (!SUCCEEDED(CoCreateInstance(CLSID_TaskScheduler, NULL, CLSCTX_INPROC_SERVER, IID_ITaskService, (void**)&pService))){
		//std::cout << "\n  " << __LINE__;
		SysFreeString(bstrszTaskName);
		//std::cout << "\n  " << __LINE__;
		SysFreeString(bstrpszPath);
		//std::cout << "\n  " << __LINE__;
		SysFreeString(bstrpszArg);
		//std::cout << "\n  " << __LINE__;
		return 1;
	}
	//std::cout << "\n  " << __LINE__;
	if (!SUCCEEDED(pService->Connect(var, var, var, var))){
		pService->Release();
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}
	//std::cout << "\n  " << __LINE__;
	if (!SUCCEEDED(pService->GetFolder(L"\\", &pRootFolder))){
		pRootFolder->Release();
		pService->Release();
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}
	//std::cout << "\n  " << __LINE__;
	if (!SUCCEEDED(pService->NewTask(0, &pTask))){
		pTask->Release();
		pRootFolder->Release();
		pService->Release();
		SysFreeString(bstrszTaskName);
		SysFreeString(bstrpszPath);
		SysFreeString(bstrpszArg);
		return 1;
	}//std::cout << "\n  " << __LINE__;
	if (SUCCEEDED(pTask->get_RegistrationInfo(&pRegInfo))){
		pRegInfo->put_Author(L"AUTOHR");
		pRegInfo->Release();
	}
	//std::cout << "\n  " << __LINE__;
	if (SUCCEEDED(pTask->get_Principal(&pPrincipal))){
		pPrincipal->put_RunLevel(TASK_RUNLEVEL_HIGHEST);
		pPrincipal->Release();
	}
	//std::cout << "\n  " << __LINE__;
	if (SUCCEEDED(pTask->get_Settings(&pSettings))){
		pSettings->put_StartWhenAvailable(VARIANT_BOOL(true));
		pSettings->put_DisallowStartIfOnBatteries(VARIANT_BOOL(false));
		pSettings->put_StopIfGoingOnBatteries(VARIANT_BOOL(false));
		pSettings->put_ExecutionTimeLimit(TEXT(L"PT0S"));
		pSettings->Release();
	}
	//std::cout << "\n  " << __LINE__;
	if (SUCCEEDED(pTask->get_Triggers(&pTriggerCollection))){
		if (SUCCEEDED(pTriggerCollection->Create(TASK_TRIGGER_LOGON, &pTrigger))){
			if (SUCCEEDED(pTrigger->QueryInterface(IID_ILogonTrigger, (void**)&pLogonTrigger))){
				if (SUCCEEDED(pLogonTrigger->put_Id(TEXT(L"AnyLogon"))) &&
					SUCCEEDED(pLogonTrigger->put_UserId(NULL))){
					triggersucc = true;
				}
				pLogonTrigger->Release();
			}
			pTrigger->Release();
		}
		pTriggerCollection->Release();
	}
	//std::cout << "\n  " << __LINE__;
	if (SUCCEEDED(pTask->get_Actions(&pActionCollection))){
		if (SUCCEEDED(pActionCollection->Create(TASK_ACTION_EXEC, &pAction))){
			if (SUCCEEDED(pAction->QueryInterface(IID_IExecAction, (void**)&pExecAction))){
				if (SUCCEEDED(pExecAction->put_Path(bstrpszPath)) &&
					SUCCEEDED(pExecAction->put_Arguments(bstrpszArg))){
					actionsucc = true;
				}
				pExecAction->Release();
			}
			pAction->Release();
		}
		pActionCollection->Release();
	}
	//std::cout << "\n  " << __LINE__;
	if (triggersucc && actionsucc
		&& SUCCEEDED(pRootFolder->RegisterTaskDefinition(bstrszTaskName, pTask, TASK_CREATE_OR_UPDATE, var,
		var, TASK_LOGON_INTERACTIVE_TOKEN, var, &pRegisteredTask))){
		succ = true;
		pRegisteredTask->Release();
	}

	//std::cout << "\n  " << __LINE__;
	SysFreeString(bstrszTaskName);
	SysFreeString(bstrpszPath);
	SysFreeString(bstrpszArg);
	return succ;
}