#include "main.h"
#include <vector>
#include <map>
#include <string>
#include <algorithm>
#include "TriggerLogon.h"
#include "ApiFiles.h"
#include "Downloader.h"

#pragma comment(linker,"\"/manifestdependency:type='win32' \
name='Microsoft.Windows.Common-Controls' version='6.0.0.0' \
processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")








BEGIN_MESSAGE_MAP(MyApp, CWinApp)

END_MESSAGE_MAP()
std::vector<std::string> GetCommandLineArgs(){
	LPWSTR *szArgList;
	int argCount;
	std::vector<std::string> res;
	szArgList = CommandLineToArgvW(GetCommandLineW(), &argCount);
	for (int i = 0; i < argCount; i++){
		std::wstring strw(szArgList[i]);
		res.push_back(std::string(strw.begin(), strw.end()));
	}

	LocalFree(szArgList);
	return res;
}


MyApp TheApp;

MyApp::MyApp()
{
	/*AllocConsole();
	FILE *pFile;
	freopen_s(&pFile, "CONIN$", "r", stdin);
	freopen_s(&pFile, "CONOUT$", "w", stdout);
	freopen_s(&pFile, "CONOUT$", "w", stderr);*/
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	CreateLauncherTrigger();
	args = GetCommandLineArgs();

	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	std::string init_version = "";
	/*for (auto arg = args.begin(); arg != args.end(); arg++){
		MessageBox(0,&(*arg)[0],0,MB_OK);
	}*/
		extern bool is_hidden;
		auto fw = std::find(args.begin(), args.end(), "restart");
		if (fw != args.end()){
			//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
			//restart
			for (int i = 0; i < 5; i++)
			{
				//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
				CreateMutex(NULL, TRUE, "SmartBotLauncher");
				if (GetLastError() == ERROR_ALREADY_EXISTS){
					break;
				}
				else if (i == 4){
				//	MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
					TerminateProcess(GetCurrentProcess(),0);
				}
				Sleep(1000);
			}//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
			is_hidden = true;
			hidden = false;
			return;
		}
		else{
			//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
			fw = std::find(args.begin(), args.end(), "restarthidden");
			if (fw != args.end()){
				for (int i = 0; i < 5; i++){
					CreateMutex(NULL, TRUE, "SmartBotLauncher");
					//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
					if (GetLastError() == ERROR_ALREADY_EXISTS){
						//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
						break;
					}
					else if (i == 4){
						//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
						TerminateProcess(GetCurrentProcess(),0);
					}
					Sleep(1000);
				}
				//restart
				is_hidden = true;
				hidden = true;
				return;
			}
			else{
				//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
				for (auto arg = args.begin(); arg != args.end(); arg++){
					//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
					if (arg->find("version") != std::string::npos){
						//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
						int spliter_pos = arg->find("|");
						if (spliter_pos != std::string::npos && spliter_pos < arg->length()){
							init_version = &(*arg)[spliter_pos];
						}
					}
				}//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
				CreateMutex(NULL, TRUE, "SmartBotLauncher");
				int error = (long long)GetLastError();
				if (error == ERROR_ALREADY_EXISTS){
					//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
					HWND w = FindWindowA(0, "Smart Bot Launcher");
					if (w){
						//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
						extern bool is_hidden;
						is_hidden = false;
						ShowWindow(w, SW_SHOW);// MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
						TerminateProcess(GetCurrentProcess(), NULL);
						return;
					}
				}
			}
		}
	

		//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
		
	
	//not made
}

BOOL MyApp::InitInstance()
{
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	INITCOMMONCONTROLSEX InitCtrls;

	InitCtrls.dwSize = sizeof(InitCtrls);
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	CWinApp::InitInstance();

	AfxEnableControlContainer();
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	// of your final executable, you should remove from the following
	// the specific initialization routines you do not need
	// Change the registry key under which our settings are stored
	// TODO: You should modify this string to be something appropriate
	// such as the name of your company or organization
	SetRegistryKey(_T("Local AppWizard-Generated Applications"));

	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	m_pMainWnd = &dlg;
	INT_PTR nResponse;

	if (hidden){ 
		if (dlg.Create(IDD_DIALOG1)){
			//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
			extern bool is_hidden;
			is_hidden = true;// MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
			dlg.ShowWindow(SW_HIDE);
			dlg.SetWindowTextA("Launcher");
			m_pMainWnd = &dlg; //MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
			nResponse = dlg.RunModalLoop(); //MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
		}
	}
	else{
		if (dlg.Create(IDD_DIALOG1)){
			//	dlg.ShowWindow(SW_HIDE);
			//dlg.SetWindowTextA("Launcher");
			dlg.SetWindowTextA("Launcher");
			m_pMainWnd = &dlg; //MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);

			/*	LONG lExStyle = lExStyle = dlg.GetExStyle();
				lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
				dlg.ModifyStyleEx(GWL_EXSTYLE, lExStyle);*/

			dlg.SetWindowTextA("Launcher");
				dlg.ShowWindow(SW_SHOW);
				dlg.SetWindowTextA("Launcher");
			nResponse = dlg.RunModalLoop(); //MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);


			/*	LONG lExStyle = dlg.GetExStyle();
			lExStyle &= ~(WS_EX_DLGMODALFRAME | WS_EX_CLIENTEDGE | WS_EX_STATICEDGE);
			dlg.ModifyStyleEx(GWL_EXSTYLE, lExStyle);*/

			//nResponse = dlg.DoModal();


			//SetWindowLong();
			//dlg.SetWindowTextA("Launcher");
		}
	}
	
	//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);
	
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}//MessageBox(0, &("line " + std::to_string((long long)__LINE__))[0], 0, MB_OK);

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
	//not made
}

