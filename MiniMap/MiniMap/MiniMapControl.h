#pragma once
#include "MiniMapFilesManager.h"
#include "Util.h"

#include <iostream>
using namespace System;
using namespace System::ComponentModel;
using namespace System::Collections;
using namespace System::Windows::Forms;
using namespace System::Data;
using namespace System::Drawing;
using namespace System::Collections::Generic;
using namespace System::Collections;

namespace MiniMap {
	public delegate void delegateCoordinateMouseEvent(MiniMap::Coordinate^, System::Windows::Forms::MouseEventArgs^  e);
	public delegate void delegateCoordinateClick(MiniMap::Coordinate^, System::EventArgs^  e);

	public delegate void delegateWaypoint(MiniMap::WaypointInfo^);
	public delegate void delegateWaypointChanged(MiniMap::WaypointInfo^ _old, MiniMap::WaypointInfo^ _new);

	public ref class MiniMapControl : public System::Windows::Forms::UserControl{
	public:
		MiniMapControl(void){
			down_level_count_show = 0;
			InitializeComponent();
			this->pictureBox1->MouseWheel += gcnew System::Windows::Forms::MouseEventHandler(this, &MiniMapControl::pictureBox1_MouseWheel);
			onCoordinateMouseMove += gcnew delegateCoordinateMouseEvent(this, &MiniMapControl::onCursorCoordinateMove);
			onCoordinateMouseClick += gcnew delegateCoordinateClick(this, &MiniMapControl::onCursorCoordinateClick);
			onCoordinateMouseDown += gcnew delegateCoordinateMouseEvent(this, &MiniMapControl::onCursorCoordinateMouseDown);
		}

		Image ^ResizeImage(Image^ source, RectangleF destinationBounds)
		{
			RectangleF^ sourceBounds = gcnew RectangleF(0.0f, 0.0f, (float)source->Width, (float)source->Height);
			RectangleF^ scaleBounds = gcnew RectangleF();

			Image^ destinationImage = gcnew Bitmap((int)destinationBounds.Width, (int)destinationBounds.Height);
			Graphics^ graph = Graphics::FromImage(destinationImage);
			graph->InterpolationMode =
				System::Drawing::Drawing2D::InterpolationMode::HighQualityBicubic;

			// Fill with background color
			graph->FillRectangle(gcnew SolidBrush(System::Drawing::Color::Transparent), destinationBounds);

			float resizeRatio, sourceRatio;
			float scaleWidth, scaleHeight;

			sourceRatio = (float)source->Width / (float)source->Height;

			if (sourceRatio >= 1.0f)
			{
				//landscape
				resizeRatio = destinationBounds.Width / sourceBounds->Width;
				scaleWidth = destinationBounds.Width;
				scaleHeight = sourceBounds->Height * resizeRatio;
				float trimValue = destinationBounds.Height - scaleHeight;
				graph->DrawImage(source, 0, (uint32_t)(trimValue / 2), (uint32_t)destinationBounds.Width, (uint32_t)scaleHeight);
			}
			else
			{
				//portrait
				resizeRatio = destinationBounds.Height / sourceBounds->Height;
				scaleWidth = sourceBounds->Width * resizeRatio;
				scaleHeight = destinationBounds.Height;
				float trimValue = destinationBounds.Width - scaleWidth;
				graph->DrawImage(source, (uint32_t)(trimValue / 2), 0, (uint32_t)scaleWidth, (uint32_t)destinationBounds.Height);
			}

			return destinationImage;

		}


		void loadImages(){
			_waypoint_image = Image::FromFile(".\\img\\minimap\\default_cursor.png");
			_cursor_image = Image::FromFile(".\\img\\minimap\\default_cursor.png");			
			_selected_waypoint_image = Image::FromFile(".\\img\\minimap\\selected_waypoint.png");

			//_waypoint_image = ResizeImage(_waypoint_image, System::Drawing::RectangleF(0, 0, (float)rect_sice_len, (float)rect_sice_len));
		}

		void set_to_draw_point(String^ key, PointF^ point){
			named_to_draw[key] = point;
		}

		void set_to_draw_point(String^ key, Point^ point){
			named_to_draw[key] = point;
		}

		void set_to_draw_effect_point(String^ key, uint32_t color, Coordinate^ coord, float decay, uint32_t radius){
			named_to_draw[key] = gcnew ToDrawEffectPoint(coord, color, decay, radius);
		}

		void set_to_draw_effect_wall(String^ key, uint32_t color,
			Coordinate^ start_coord, Coordinate^ end_coord, uint32_t width){
			named_to_draw[key] = gcnew ToDrawLineEffect(start_coord, end_coord, color, width);
		}

		void set_to_draw_line(String^ key, PointF start_pos, PointF end_pos, uint32_t width, uint32_t color_argb, float opacity){
			((unsigned char*)&color_argb)[3] = (unsigned char)(255 * opacity);
			named_to_draw[key] = gcnew MiniMap::Line(start_pos, end_pos, gcnew Pen(System::Drawing::Color::FromArgb(color_argb), (float)width));
		}

		void set_do_draw_rectangle(String^ key, uint32_t x, uint32_t y, uint32_t z, uint32_t width, uint32_t height, uint32_t argb,
			bool border, int border_width){
			DrawRectangle^ rectangle = gcnew DrawRectangle(
				gcnew System::Drawing::RectangleF((float)x, (float)y, (float)width, (float)height), border, argb);

			rectangle->border_width = border_width;

			rectangle->z = z;
			named_to_draw[key] = rectangle;
		}

		void remove_named_object_by_name(String^ name){
			named_to_draw.Remove(name);
		}

	protected: ~MiniMapControl(){
				   if (components)
					   delete components;

				   delete mapManager;

	}
	protected:
	public:

		event delegateCoordinateMouseEvent^ onCoordinateMouseDown;
		event delegateCoordinateMouseEvent^ onCoordinateMouseUp;
		event delegateCoordinateClick^ onCoordinateMouseClick;
		event delegateCoordinateMouseEvent^ onCoordinateMouseMove;
		event delegateWaypoint^ onWaypointMouseOut;
		event delegateWaypoint^ onWaypointMouseEnter;
		event delegateWaypoint^ onWaypointMoved;
		event delegateWaypointChanged^ onWaypointSelectedChanged;

		property Image^ waypoint_image{
			void set(Image^ img){
				_waypoint_image = img;
				update_images();
			}
			Image^ get(){
				return _waypoint_image;
			}
		}

		property Image^ cursor_image{
			void set(Image^ img){
				_cursor_image = img;
				request_redraw(false);
			}
			Image^ get(){
				return _cursor_image;
			}
		}

		property Image^ selected_waypoint_image{
			void set(Image^ img){
				_selected_waypoint_image = img;
				request_redraw(false);
			}
			Image^ get(){
				return _selected_waypoint_image;
			}
		}

		void increase_z(uint32_t count){
			if (!count)
				return;
			current_z += count;
			request_redraw(true);
		}

		void decrease_z(uint32_t count){
			if (!count)
				return;
			current_z -= count;
			request_redraw(true);
		}

		void addWaypoint(MiniMap::WaypointInfo^ waypoint){
			if (waypoints.Contains(waypoint))
				return;
			if (current_z == waypoint->z){
				set_to_draw_image("wpt_" + waypoint->identifier
					, _waypoint_image,
					waypoint->getPointF(), nullptr);
				request_redraw(false);
			}
			waypoints.Add(waypoint);
		}

		void removeWaypointById(String^ id){
			for each (WaypointInfo^ info in waypoints){
				if (info->identifier == id){


					if (selectedWaypoint == info)
						selectedWaypoint = nullptr;
					else if (hoveredWaypoint == info)
						hoveredWaypoint = nullptr;
					else if (dragingWaypoint == info)
						dragingWaypoint = nullptr;

					clear_named_to_draw_with("wpt_" + id, true);
					waypoints.Remove(info);
					request_redraw(false);
					break;
				}
			}
		}

		void removeAllWaypoints(){
			waypoints.Clear();
			selectedWaypoint = nullptr;;
			hoveredWaypoint = nullptr;
			dragingWaypoint = nullptr;
			clear_named_to_draw_with("wpt_", false);
			update(false);
		}

		void update(bool update_cache){
			request_redraw(update_cache);
		}

		WaypointInfo^ getWaypointInfoById(String^ id){
			for each (WaypointInfo^ info in waypoints)
			if (info->identifier == id)
				return info;
			return nullptr;
		}

		void set_current_coordinate(int x, int y, int z){ 
			if (x == current_x && y == current_y && z == current_z)
				return;
			
			if (x < 30000 || x > 50000)
				x = 33000;

			if (y < 30000 || y > 50000)
				y = 32000;

			if (z < -20 || z > 20)
				z = 0;

			current_x = x;
			current_y = y;
			current_z = z;
			must_update_cache = true;
			need_update = true;

			if (!this->refresh_timer->Enabled){
				loadImages();
				this->refresh_timer->Enabled = true;
			}
		}

		property MiniMap::WaypointInfo^ selectedWaypoint{
			MiniMap::WaypointInfo^ get(){
				return _selectedWaypoint;
			}
			void set(MiniMap::WaypointInfo^ value){
				onWaypointSelectedChanged(_selectedWaypoint, value);
				if (!value){
					clear_named_to_draw_with("selected", true);
				}
				else{
					if (_selected_waypoint_image)
						set_to_draw_image("selected"
						, _selected_waypoint_image,
						value->getPointF(), nullptr);
				}
				_selectedWaypoint = value;
			}
		}

		property MiniMap::WaypointInfo^ hoveredWaypoint{
			MiniMap::WaypointInfo^ get(){
				return _hoveredWaypoint;
			}

			void set(MiniMap::WaypointInfo^ value){
				_hoveredWaypoint = value;
			}
		}

		property MiniMap::WaypointInfo^ dragingWaypoint{
			MiniMap::WaypointInfo^ get(){
				return _dragingWaypoint;
			}

			void set(MiniMap::WaypointInfo^ value){
				_dragingWaypoint = value;
			}
		}

	public:	uint32_t down_level_count_show;

			MiniMapFilesManager* mapManager = new MiniMapFilesManager();

			WaypointInfo^ findWaypointById(String^ id){
				if (!id)
					return nullptr;
				for each (WaypointInfo^ info in waypoints)
				if (info->identifier == id)
					return info;
				return nullptr;
			}

			void update_images(){
				clear_named_to_draw_with("wpt_", false);
				for each(WaypointInfo^ info in waypoints){
					if (current_z == info->z)
						set_to_draw_image("wpt_" + info->identifier
						, _waypoint_image,
						info->getPointF(), nullptr);
				}
				request_redraw(false);
			}

			void request_redraw(bool update_cache){
				need_update = true;
				if (update_cache)
					must_update_cache = true;
			}

			void removeWaypoint(MiniMap::WaypointInfo^ waypoint){
				if (selectedWaypoint == waypoint)
					selectedWaypoint = nullptr;
				else if (hoveredWaypoint == waypoint)
					hoveredWaypoint = nullptr;
				else if (dragingWaypoint == waypoint)
					dragingWaypoint = nullptr;

				waypoints.Remove(waypoint);
			}

			System::Collections::Generic::List<WaypointInfo^> waypoints;
			String^ pathToShow;

			System::Windows::Forms::PictureBox^  pictureBox1;
	protected:

		int last_mouse_pos_x;
		int last_mouse_pos_y;
		int current_x = 0;
		int current_y = 0;
		int current_z = 0;
		int zoom = 12;
		int zoom_factor = 10;
		int zoom_required_map_count = zoom * zoom_factor * 2 + 1;
		bool draging_pos;
		bool need_update;
		bool must_update_cache;
		System::Drawing::Point last_mouse_down_point;
		Bitmap ^chacheMap;

		unsigned char* chaceMapLayerDataPtr;
		Bitmap^ cacheMapEdited;

		WaypointInfo^ _selectedWaypoint;
		WaypointInfo^ _hoveredWaypoint;
		WaypointInfo^ _dragingWaypoint;
		System::Drawing::Image^ _waypoint_image;
		System::Drawing::Image^ _cursor_image;
		System::Drawing::Image^ _selected_waypoint_image;

		System::Windows::Forms::Timer^  refresh_timer;
		System::ComponentModel::IContainer^  components;

#pragma region Windows Form Designer generated code
		void InitializeComponent(void)
		{
			this->components = (gcnew System::ComponentModel::Container());
			this->pictureBox1 = (gcnew System::Windows::Forms::PictureBox());
			this->refresh_timer = (gcnew System::Windows::Forms::Timer(this->components));
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->BeginInit();
			this->SuspendLayout();
			// 
			// pictureBox1
			// 
			this->pictureBox1->Dock = System::Windows::Forms::DockStyle::Fill;
			this->pictureBox1->Location = System::Drawing::Point(0, 0);
			this->pictureBox1->Margin = System::Windows::Forms::Padding(0);
			this->pictureBox1->Name = L"pictureBox1";
			this->pictureBox1->Size = System::Drawing::Size(380, 350);
			this->pictureBox1->TabIndex = 0;
			this->pictureBox1->TabStop = false;
			this->pictureBox1->KeyPress += gcnew System::Windows::Forms::KeyPressEventHandler(this, &MiniMapControl::waypointer_maker_KeyPress);
			this->pictureBox1->Click += gcnew System::EventHandler(this, &MiniMapControl::pictureBox1_Click);
			this->pictureBox1->MouseDown += gcnew System::Windows::Forms::MouseEventHandler(this, &MiniMapControl::pictureBox1_MouseDown);
			this->pictureBox1->MouseEnter += gcnew System::EventHandler(this, &MiniMapControl::pictureBox1_MouseEnter);
			this->pictureBox1->MouseMove += gcnew System::Windows::Forms::MouseEventHandler(this, &MiniMapControl::pictureBox1_MouseMove);
			this->pictureBox1->MouseUp += gcnew System::Windows::Forms::MouseEventHandler(this, &MiniMapControl::pictureBox1_MouseUp);
			// 
			// refresh_timer
			// 
			this->refresh_timer->Enabled = false;
			this->refresh_timer->Interval = 5;
			this->refresh_timer->Tick += gcnew System::EventHandler(this, &MiniMapControl::timer1_Tick);
			// 
			// MiniMapControl
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->Controls->Add(this->pictureBox1);
			this->Name = L"MiniMapControl";
			this->Size = System::Drawing::Size(380, 350);
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureBox1))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	public:
		void IncreaseZoom(int count){
			int new_zoom = zoom;
			new_zoom -= count;

			float new_value = (float)new_zoom * (float)zoom_factor * 2.0f;
			if (new_value > 0){
				zoom_required_map_count = (int)new_value;
				zoom = new_zoom;
			}
		}

		void DescreaseZoom(int count){
			int new_zoom = zoom;
			new_zoom += count;

			float new_value = (float)new_zoom * (float)zoom_factor * 2.0f;
			if (new_value > 0){
				zoom_required_map_count = (int)new_value;
				zoom = new_zoom;
			}
		}

		SizeF getCoordinateToPointRelation(){
			return SizeF(zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Width : 0,
				zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Height : 0);
		}

		PointF getCoordinateAsInterfacePoint(PointF coordinate){
			float sqm_cost_each_pixel_x = zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Width : 0;
			float sqm_cost_each_pixel_y = zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Height : 0;

			int initial_pos_x = (int)((float)current_x - (zoom_required_map_count ? (float)zoom_required_map_count / 2 : 0));
			int initial_pos_y = (int)((float)current_y - (zoom_required_map_count ? (float)zoom_required_map_count / 2 : 0));

			return PointF(((float)((float)coordinate.X - (float)initial_pos_x)) / (float)sqm_cost_each_pixel_x,
				(float)((float)coordinate.Y - (float)initial_pos_y) / (float)sqm_cost_each_pixel_y);
		}

		PointF getInterfacePointAsCoordinate(PointF point){
			float sqm_cost_each_pixel_x = (float)zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Width : 0;
			float sqm_cost_each_pixel_y = (float)zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Height : 0;

			float initial_pos_x = (float)current_x - (zoom_required_map_count ? (float)zoom_required_map_count / (float)2 : 0);
			float initial_pos_y = (float)current_y - (zoom_required_map_count ? (float)zoom_required_map_count / (float)2 : 0);

			return PointF((float)((float)point.X * (float)sqm_cost_each_pixel_x) + initial_pos_x,
				(float)((float)point.Y * (float)sqm_cost_each_pixel_y) + initial_pos_y);
		}

		int last_cache_x;
		int last_cache_y;
		int last_cache_z;
		int last_cache_zoom;

		System::Collections::Generic::Dictionary<String^, Image^> cacheImages;

		//update map
		Dictionary<String^, Object^> named_to_draw;
		List<Object^> to_draw;

		Dictionary<String^, Object^> direct_named_to_draw;
		List<Object^> direct_to_draw;

		void add_to_draw(Object^ obj){
			to_draw.Add(obj);
		}

		void clear_to_draw(bool named, bool unamed){
			if (named)
				named_to_draw.Clear();
			if (unamed)
				to_draw.Clear();
		}

		void clear_to_draw(){
			clear_to_draw(true, true);
		}

		void clear_named_to_draw_with(String^ content_part, bool entire_content){
			bool cleared = false;

			while (!cleared){
				cleared = true;
				for each(auto kp in direct_named_to_draw){
					if (entire_content){
						if (kp.Key == content_part){
							direct_named_to_draw.Remove(kp.Key);
							cleared = false;
							break;
						}
					}
					else{
						if (kp.Key->Contains(content_part)){
							direct_named_to_draw.Remove(kp.Key);
							cleared = false;
							break;
						}
					}
				}
			}
		}

		void set_to_draw_image(String^ key, Image^ img, PointF coordinate, System::Drawing::SizeF^ dimensions){
			direct_named_to_draw[key] = gcnew ToDrawImage(img, dimensions, coordinate);
		}

		Object^ get_named_obj(String^ key){
			if (direct_named_to_draw.ContainsKey(key))
				return direct_named_to_draw[key];
			return nullptr;
		}

		void set_to_draw(String^ key, Object^ value){
			named_to_draw[key] = value;
		}

		void set_to_draw_elipse(String^ key, Object^ value){
			//	named_to_draw[key] = (gcnew DrawElipse(value));
		}

		void remove_to_draw(String^ key){
			named_to_draw.Remove(key);
		}

		void DrawPixel(int x_in, int y_in){
			int start_x = (int)(current_x - (zoom_required_map_count / 2));
			int start_y = (int)(current_y - (zoom_required_map_count / 2));
			int end_x = (int)(start_x + zoom_required_map_count);
			int end_y = (int)(start_y + zoom_required_map_count);

			int offset_store_x = x_in - start_x;
			int offset_store_y = y_in - start_y;
			if (offset_store_x < zoom_required_map_count && offset_store_x > -1 && offset_store_y < zoom_required_map_count && offset_store_y > -1){
				((uint32_t*)chaceMapLayerDataPtr)[(offset_store_x + offset_store_y * zoom_required_map_count)] = 0xffffffff;
			}
		}

		void RegisterCircle(Point point, int radius){
			for (int x = point.X - (radius - 1); x < point.X + (radius - 1); x++){
				for (int y = point.Y - (radius - 1); y < point.Y + (radius - 1); y++){
					DrawPixel(x, y); // Octant 1
				}
			}
			return;
		}

		void DrawLineWall(System::Collections::Generic::KeyValuePair<PointF, PointF>^ line, int width){
			if (width <= 0)
				width = 1;
			/*DrawLine(line,5);
			return;*/
			int start_x = (int)(current_x - (zoom_required_map_count / 2));
			int start_y = (int)(current_y - (zoom_required_map_count / 2));

			float x1 = line->Key.X;
			float y1 = line->Key.Y;
			float x2 = line->Value.X;
			float y2 = line->Value.Y;

			// Bresenham's line algorithm
			const bool steep = (fabs(y2 - y1) > fabs(x2 - x1));
			if (steep){
				std::swap(x1, y1);
				std::swap(x2, y2);
			}

			if (x1 > x2)
			{
				std::swap(x1, x2);
				std::swap(y1, y2);
			}

			const float dx = x2 - x1;
			const float dy = fabs(y2 - y1);

			float error = dx / 2.0f;
			const int ystep = (y1 < y2) ? 1 : -1;
			int y = (int)y1;

			const int maxX = (int)x2;

			for (int x = (int)x1; x < maxX; x++){

				int offset_x = x - start_x;
				int offset_y = y - start_y;
				if (steep){
					offset_x = y - start_x;
					offset_y = x - start_y;
					RegisterCircle(Point(y, x), width);
				}
				else{
					RegisterCircle(Point(x, y), width);
				}
				if (offset_x < zoom_required_map_count && offset_x > -1 && offset_y < zoom_required_map_count && offset_y > -1){
					if (steep)
						DrawPixel(y, x);
					else
						DrawPixel(x, y);

					//RegisterCircle(Point(y, x), 1);
					//MapMinimap::special_area_map[0][offset_x][offset_y] = 1;
				}
				/*if (steep)
				{
				SetPixel(y, x, color);
				}
				else
				{
				SetPixel(x, y, color);
				}*/

				error -= dy;
				if (error < 0)
				{
					y += ystep;
					error += dx;
				}
			}
		}

		// Create an array of points for the curve in the second figure.

		List<WaypointInfo^>^ getWaypointsToRender(){
			List<WaypointInfo^>^ retval = gcnew List<WaypointInfo^>();
			for each(WaypointInfo^ info in waypoints)
			if (info->z == current_z)
				retval->Add(info);
			return retval;
		}

		void DrawLine(Graphics^ graphic, Object^ Value, System::Drawing::RectangleF^ display_area, bool direct_display_area){

			MiniMap::Line^ line = (MiniMap::Line^)Value;
			/*if (!direct_display_area){
				DrawLineWall(gcnew
				System::Collections::Generic::KeyValuePair<PointF, PointF>(line->start, line->end), 3);

				return;
				}*/

			if (direct_display_area){
				if (!MiniMap::LineIntersectsRect(line->start, line->end, display_area))
					return;

				PointF pstart = getCoordinateAsInterfacePoint(PointF(line->start.X + 0.5f, line->start.Y + 0.5f));
				PointF pend = getCoordinateAsInterfacePoint(PointF(line->end.X + 0.5f, line->end.Y + 0.5f));
				graphic->DrawLine(line->pen, pstart.X, pstart.Y, pend.X, pend.Y);
			}
			else{
				if (MiniMap::LineIntersectsRect(line->start, line->end, display_area)){
					graphic->DrawLine(line->pen, line->start.X - display_area->X, line->start.Y - display_area->Y
						, line->end.X - display_area->X, line->end.Y - display_area->Y);
				}
			}

		}

		void DrawAxis(Graphics^ graphic, Axis^ Value, System::Drawing::RectangleF^ display_area, bool direct_display_area){

			MiniMap::Line^ horizontal_line = Value->get_horizontal_line();
			MiniMap::Line^ vertical_line = Value->get_vertical_line();

			if (MiniMap::LineIntersectsRect(horizontal_line->start, horizontal_line->end, display_area) &&
				MiniMap::LineIntersectsRect(vertical_line->start, vertical_line->end, display_area)){
				graphic->DrawLine(Value->pen, horizontal_line->start.X - display_area->X, horizontal_line->start.Y - display_area->Y
					, horizontal_line->end.X - display_area->X, horizontal_line->end.Y - display_area->Y);
				graphic->DrawLine(Value->pen, vertical_line->start.X - display_area->X, vertical_line->start.Y - display_area->Y
					, vertical_line->end.X - display_area->X, vertical_line->end.Y - display_area->Y);
			}
		}

		void DrawImage(Graphics^ graphic, ToDrawImage^ Value, System::Drawing::RectangleF^ display_area, bool direct_display_area){
			if (direct_display_area){
				PointF p = getCoordinateAsInterfacePoint(Value->coordinate);
				PointF pend = getCoordinateAsInterfacePoint(PointF(Value->coordinate.X + 1, Value->coordinate.Y + 1));
				PointF center = PointF(p.X + (pend.X - p.X) / 2, p.Y + (pend.Y - p.Y) / 2);
				RectangleF r(center.X - 20.0f, center.Y - 40.0f, 40.0f, 40.0f);
				graphic->DrawImage(Value->img, r);
			}

		}

		void DrawToDrawLineEffect(Graphics^ gfx, Object^ obj, System::Drawing::RectangleF^ display_area){
			ToDrawLineEffect^ mline = (ToDrawLineEffect^)obj;

			DrawLineWall(gcnew
				System::Collections::Generic::KeyValuePair<PointF, PointF>(
				mline->start_coord->ToPointF(), mline->end_coord->ToPointF()), mline->width);

			if (mline->start_coord->z != current_z)
				return;

			if (!LineIntersectsRect(mline->get_line()->start, mline->get_line()->end, display_area))
				return;

			if (mline->radius <= 0)
				return;

			int start_x = current_x - (zoom_required_map_count / 2);
			int start_y = current_y - (zoom_required_map_count / 2);

			uint32_t img_pos_x = (uint32_t)(mline->get_line()->start.X - (float)start_x);
			uint32_t img_pos_y = (uint32_t)(mline->get_line()->start.Y - (float)start_y);

			uint32_t img_pos_end_x = (uint32_t)(mline->get_line()->end.X - (float)start_x);
			uint32_t img_pos_end_y = (uint32_t)(mline->get_line()->end.Y - (float)start_y);

			gfx->DrawLine(gcnew System::Drawing::Pen(System::Drawing::Color::FromArgb(mline->color)),
				mline->start_coord->ToPoint(), mline->end_coord->ToPoint());
		}

		void DrawPointEffect(Graphics^ graphic, Object^ obj, System::Drawing::RectangleF^ display_area){

			ToDrawEffectPoint^ mPoint = (ToDrawEffectPoint^)obj;
			if (mPoint->coord->z != current_z)
				return;
			if (!display_area->IntersectsWith(mPoint->get_area_not_ref()))
				return;

			if (mPoint->radius <= 0)
				return;

			RectangleF^ rect_area = mPoint->get_area();
			int x_right = (int)(rect_area->Left + rect_area->Width);
			int y_bottom = (int)(rect_area->Top + rect_area->Height);
			int x_center = (int)(mPoint->coord->x);
			int y_center = (int)(mPoint->coord->y);
			int radius = (int)(mPoint->radius);

			int start_x = (int)(current_x - (zoom_required_map_count / 2));
			int start_y = (int)(current_y - (zoom_required_map_count / 2));
			int end_x = (int)(start_x + zoom_required_map_count);
			int end_y = (int)(start_y + zoom_required_map_count);

			for (int x = (int)rect_area->Left; x < x_right; x++){
				for (int y = (int)rect_area->Top; y < y_bottom; y++){
					int translated_x = x - start_x;
					int translated_y = y - start_y;
					if (translated_x < 0 || translated_x > zoom_required_map_count
						||
						translated_y < 0 || translated_y > zoom_required_map_count){
						continue;
					}
					uint32_t index = translated_x * 4 + translated_y * zoom_required_map_count;
					if ((zoom_required_map_count * zoom_required_map_count) < (int32_t)index)
						continue;

					if (!isInRectangle(current_x, current_y,
						zoom_required_map_count, x, y))
						continue;

					if (isPointInCircle(x_center, y_center, radius, x, y)){
						float dist = getDistanceFromPoints((float)x_center, (float)y_center, (float)x, (float)y);
						uint32_t opacity = (uint32_t)((float)(255.0f) * (((float)radius - (float)dist) / (float)radius));
						if (opacity){
							if (!(((uint32_t*)chaceMapLayerDataPtr)[(translated_x + translated_y * zoom_required_map_count)])){
								uint32_t color = mPoint->color;
								((unsigned char*)&color)[3] = (unsigned char)opacity;
								((uint32_t*)chaceMapLayerDataPtr)[(translated_x + translated_y * zoom_required_map_count)] = color;
							}
							else{
								uint32_t color = mPoint->color;
								uint32_t new_color = (((uint32_t*)chaceMapLayerDataPtr)[(translated_x + translated_y * zoom_required_map_count)]);
								unsigned char* old = (unsigned char*)&(((uint32_t*)chaceMapLayerDataPtr)[(translated_x + translated_y * zoom_required_map_count)]);
								unsigned char* _new = (unsigned char*)&color;

								float old_opacity = old[3];
								float new_opacity = _new[3];


								((unsigned char*)&color)[3] =
									(unsigned char)((float)old[3] + ((float)_new[3] * (255.0f - (float)old[3]) / 255.0f));
								((unsigned char*)&color)[2] =
									(unsigned char)((old[2] * old_opacity / 255.0f) + (_new[2] * new_opacity * (255.0f - old_opacity) / (255.0f * 255.0f)));
								((unsigned char*)&color)[1] =
									(unsigned char)((old[1] * old_opacity / 255.0f) + (_new[1] * new_opacity * (255.0f - old_opacity) / (255.0f * 255.0f)));
								((unsigned char*)&color)[0] =
									(unsigned char)((old[0] * old_opacity / 255.0f) + (_new[0] * new_opacity * (255.0f - old_opacity) / (255.0f * 255.0f)));
								((uint32_t*)chaceMapLayerDataPtr)[(translated_x + translated_y * zoom_required_map_count)] = color;
							}


						}
					}
				}
			}
		}

		void DrawObject(Graphics^ graphic, Object^ Value, System::Drawing::RectangleF^ display_area, bool direct_display_area){

			int x_relation = 1;
			int y_relation = 1;

			System::Type^ type = Value->GetType();
			if (type == Point::typeid){
				Point^ point = (Point^)Value;

				graphic->FillRectangle(gcnew System::Drawing::SolidBrush(Color::Blue),
					(int)(point->X - display_area->X), (int)(point->Y - display_area->Y), 1, 1);
			}
			else if (type == PointF::typeid){
				//PointF
				if (graphic){
					PointF^ point = (PointF^)Value;
					
					graphic->FillRectangle(gcnew System::Drawing::SolidBrush(Color::White),
						(int)(point->X - display_area->X), (int)(point->Y - display_area->Y), 1, 1);
				}
			}
			else if (type == MiniMap::Line::typeid)
				DrawLine(graphic, Value, display_area, direct_display_area);
			else if (type == ToDrawLineEffect::typeid){
				ToDrawLineEffect^ to_draw = (ToDrawLineEffect^)Value;
				if (to_draw->start_coord->z != current_z)
					return;

				if (direct_display_area){

				}
				else{
					DrawLineWall(gcnew
						System::Collections::Generic::KeyValuePair<PointF, PointF>(
						System::Drawing::Point(
						(int32_t)((float)to_draw->start_coord->x),
						(int32_t)((float)to_draw->start_coord->y)),
						System::Drawing::Point(
						(int32_t)((float)to_draw->end_coord->x),
						(int32_t)((float)to_draw->end_coord->y))
						), to_draw->width);
					/*graphic->DrawLine(gcnew System::Drawing::Pen(System::Drawing::Color::FromArgb(to_draw->color), (float)to_draw->width),
						System::Drawing::Point(
						(int32_t)((float)to_draw->start_coord->x - display_area->X),
						(int32_t)((float)to_draw->start_coord->y - display_area->Y)),
						System::Drawing::Point(
						(int32_t)((float)to_draw->end_coord->x - display_area->X),
						(int32_t)((float)to_draw->end_coord->y - display_area->Y)));*/
				}
			}
			else if (type->IsSubclassOf(MiniMap::ToDraw::typeid)){
				MiniMap::ToDraw^ object_to_draw = (MiniMap::ToDraw^) Value;
				//draw rects 
				if (object_to_draw->optionals.ContainsKey(object_t::object_rectangle)){
					if (object_to_draw->optionals[object_t::object_rectangle] == rect_t::rect_rectangle){
						DrawRectangle^ rect = (DrawRectangle^)Value;
						if (rect->z != current_z)
							return;
						System::Drawing::RectangleF^ mrect = (System::Drawing::RectangleF^)rect->area;
						if (direct_display_area){
							graphic->DrawRectangle(
								gcnew Pen(Color::FromArgb((int)rect->argb_fill), (float)rect->border_width),
								System::Drawing::Rectangle((int)(mrect->X - display_area->X - (float)(mrect->Width ? mrect->Width / 2.0f : 0.0f)),
								(int)(mrect->Y - display_area->Y - (float)(mrect->Height ? mrect->Height / (float)2.0f : 0.0f)),
								(int)mrect->Width, (int)mrect->Width));
						}
						else{

							float x_draw = mrect->X - display_area->X;
							float y_draw = mrect->Y - display_area->Y;
							float width = mrect->Width;
							float height = mrect->Height;

							System::Drawing::SolidBrush^ myBrush = gcnew System::Drawing::SolidBrush(Color::FromArgb(rect->argb_fill));
							graphic->FillRectangle(
								myBrush,
								System::Drawing::Rectangle((int)x_draw, (int)y_draw, (int)width, (int)height));


							auto lines = rect->get_lines();
							for each(auto line in lines){
								DrawLineWall(gcnew
									System::Collections::Generic::KeyValuePair<PointF, PointF>(
									System::Drawing::Point(
									(int32_t)((float)line.Key.X),
									(int32_t)((float)line.Key.Y)),
									System::Drawing::Point(
									(int32_t)((float)line.Value.X),
									(int32_t)((float)line.Value.Y))
									), rect->border_width);
							}
						}

					}
					else if (object_to_draw->optionals[object_t::object_rectangle] == rect_t::rect_elipses){
						DrawElipse^ rect = (DrawElipse^)Value;
						System::Drawing::RectangleF^ mrect = (System::Drawing::RectangleF^)rect->area;
						graphic->DrawEllipse(gcnew Pen(Color::Blue), System::Drawing::Rectangle((int)(mrect->X - display_area->X - (mrect->Width ? mrect->Width / 2.0f : 0.0f))
							, (int)(mrect->Y - display_area->Y - (mrect->Height ? mrect->Height / (float)2.0f : 0.0f)),
							(int)mrect->Width, (int)mrect->Width));
					}

				}
				else if (object_to_draw->GetType() == Axis::typeid){
					DrawAxis(graphic, (Axis^)object_to_draw, display_area, false);
				}

			}
			else if (type == KeyValuePair<Object^/*location*/, Object^>::typeid){
				//KeyValuePair<Object^/*location*/, Object^>^ mPair = ((KeyValuePair<Object^/*location*/, Object^>^)Value);
				//if (mPair->Key->GetType() == Point::typeid){//at pos
				//	Point^ point = (Point^)mPair->Key;
				//	Image^ img = (Image^)mPair->Value;
				//	graphic->DrawImage(img,point,);
				//}
				//else if (mPair->Key->GetType() == System::Drawing::Rectangle::typeid){//at rect
				//	System::Drawing::Rectangle^ area = (System::Drawing::Rectangle^)mPair->Key;
				//	Image^ img = (Image^)mPair->Value;
				//}
			}
			else if (type == ToDrawImage::typeid){
				DrawImage(graphic, (ToDrawImage^)Value, display_area, true);
			}
			else if (type == ToDrawEffectPoint::typeid){
				DrawPointEffect(graphic, Value, display_area);
			}
		}

		void redraw(){
			TimeChronometer timer;
			if (last_cache_x != current_x ||
				last_cache_y != current_y ||
				last_cache_z != current_z ||
				last_cache_zoom != zoom
				|| need_update){
				if (last_cache_z != current_z || need_update){
					last_cache_z = current_z;
					update_images();
				}
				need_update = false;
				last_cache_x = current_x;
				last_cache_y = current_y;

				mapManager->update_current_pos(current_x, current_y, current_z);
				if (last_cache_zoom != zoom){
					last_cache_zoom = zoom;
					mapManager->update_view(zoom_required_map_count, zoom_required_map_count, down_level_count_show);
				}
				must_update_cache = true;
			}

			//PointF^ p_ = PointF(current_x, current_y);
			//set_to_draw("cursor", p_);

			if (must_update_cache){
				must_update_cache = false;
				update_cache();
			}
			else if (!need_update)
				return;
			
			if (!chacheMap)
				return;
			
			need_update = false;
			must_update_cache = false;
			
			Bitmap^ chacheMapLayer = gcnew Bitmap(zoom_required_map_count, zoom_required_map_count);
			Graphics^ layered_graphic = Graphics::FromImage(chacheMapLayer);
			//layered_graphic->Clear(System::Drawing::Color::Blue);

			Imaging::BitmapData^ bmpDataLayer;
			System::Drawing::Rectangle rect = System::Drawing::Rectangle(0, 0, chacheMapLayer->Width, chacheMapLayer->Height);
			bmpDataLayer = chacheMapLayer->LockBits(rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, chacheMapLayer->PixelFormat);
			IntPtr ptr = bmpDataLayer->Scan0;
			uint32_t bytes = zoom_required_map_count * zoom_required_map_count * 4;
			array<Byte>^rgbValues = gcnew array<Byte>(bytes);
			pin_ptr<unsigned char> p = &rgbValues[0];
			chaceMapLayerDataPtr = (unsigned char*)p;
			ZeroMemory(p, bytes);

			System::Drawing::RectangleF^ area = gcnew System::Drawing::RectangleF((float)current_x -
				((float)zoom_required_map_count ? (float)zoom_required_map_count / 2.0f : 0.0f),
				((float)zoom_required_map_count ? (float)current_y - (float)zoom_required_map_count / 2.0f : 0.0f)
				, (float)zoom_required_map_count, (float)zoom_required_map_count);

			System::Drawing::RectangleF^ direct_area = gcnew System::Drawing::RectangleF((float)current_x -
				((float)zoom_required_map_count ? (float)zoom_required_map_count / 2.0f : 0.0f),
				((float)zoom_required_map_count ? (float)current_y - (float)zoom_required_map_count / 2.0f : 0.0f)
				, (float)zoom_required_map_count, (float)zoom_required_map_count);
			
			System::Drawing::Pen^ white_pen = gcnew System::Drawing::Pen(System::Drawing::Color::FromArgb(255, 255, 255, 255), 1);
			cacheMapEdited = gcnew Bitmap(chacheMap);
			Graphics ^ cacheEdit = Graphics::FromImage(cacheMapEdited);
			cacheEdit->InterpolationMode = System::Drawing::Drawing2D::InterpolationMode::NearestNeighbor;

			for each(KeyValuePair<String^, Object^> entry in named_to_draw)
				DrawObject(cacheEdit, entry.Value, area, false);

			for each(Object^ Value in to_draw)
				DrawObject(cacheEdit, Value, area, false);


			/*
				for (int i = 0; i < zoom_required_map_count * zoom_required_map_count; i++){
				((uint32_t*)chaceMapLayerDataPtr)[i] = 0xffffffff;
				}
				*/

			System::Runtime::InteropServices::Marshal::Copy(rgbValues, 0, ptr, bytes);
			chacheMapLayer->UnlockBits(bmpDataLayer);


			//Bitmap^ chacheMapLayer = gcnew Bitmap(zoom_required_map_count, zoom_required_map_count);

			cacheEdit->DrawImage(chacheMapLayer, 0, 0, chacheMapLayer->Width, chacheMapLayer->Height);
			//DrawPixel(current_x, current_y);

			//cacheEdit->DrawLine(gcnew System::Drawing::Pen(System::Drawing::Color::Blue), System::Drawing::Point(0, 0), System::Drawing::Point(1, 1));
			//cacheEdit->DrawLine(gcnew System::Drawing::Pen(System::Drawing::Color::Blue), System::Drawing::Point(0,0), System::Drawing::Point(20,20));
			Bitmap ^ result = gcnew Bitmap(pictureBox1->Width, pictureBox1->Height);

			Graphics ^ graphics = Graphics::FromImage(result);
			graphics->PixelOffsetMode = System::Drawing::Drawing2D::PixelOffsetMode::HighQuality;
			graphics->InterpolationMode = System::Drawing::Drawing2D::InterpolationMode::NearestNeighbor;
			graphics->DrawImage(cacheMapEdited, 0, 0, result->Width, result->Height);


			Pen^ pen = gcnew Pen(Color::FromArgb(100, 255, 255, 255), 3);
			pen->DashStyle = System::Drawing::Drawing2D::DashStyle::Dash;
			pen->EndCap = System::Drawing::Drawing2D::LineCap::ArrowAnchor;
			pen->StartCap = System::Drawing::Drawing2D::LineCap::DiamondAnchor;

			pen->CustomEndCap = gcnew System::Drawing::Drawing2D::AdjustableArrowCap(5, 5);


			auto _waypoints = getWaypointsToRender();
			int sz = _waypoints->Count;
			if (sz > 1)
			for (int i = 0; i < sz; i++){
				auto found_next = findWaypointById(_waypoints[i]->identifier_next);

				if (found_next && found_next != _waypoints[i])
				if (found_next->z == current_z)
					DrawObject(graphics, gcnew Line(_waypoints[i]->getPointF(), found_next->getPointF(), pen), area, true);
			}



			for each(KeyValuePair<String^, Object^> entry in direct_named_to_draw)
				DrawObject(graphics, entry.Value, area, true);

			for each(Object^ Value in direct_to_draw)
				DrawObject(graphics, Value, area, true);

			Object^ selected_wpt_image = get_named_obj("selected");
			if (selected_wpt_image)
				DrawObject(graphics, selected_wpt_image, area, true);

			this->pictureBox1->BackgroundImage = result;

		}

		void update_cache(){
			chacheMap = gcnew Bitmap(zoom_required_map_count, zoom_required_map_count);
			Graphics^ gfx = Graphics::FromImage(chacheMap);
			;
			System::Drawing::Rectangle rect = System::Drawing::Rectangle(0, 0, chacheMap->Width, chacheMap->Height);
			Imaging::BitmapData^ bmpData = chacheMap->LockBits(rect, System::Drawing::Imaging::ImageLockMode::ReadWrite, chacheMap->PixelFormat);
			IntPtr ptr = bmpData->Scan0;
			int bytes = zoom_required_map_count * zoom_required_map_count * 4;

			array<Byte>^rgbValues = gcnew array<Byte>(bytes);


			pin_ptr<unsigned char> p = &rgbValues[0];

			mapManager->get_argb_map((uint32_t*)p, current_x - (zoom_required_map_count ? zoom_required_map_count / 2 : 0),
				current_y - (zoom_required_map_count ? zoom_required_map_count / 2 : 0),
				zoom_required_map_count, zoom_required_map_count, current_z, down_level_count_show);

			System::Runtime::InteropServices::Marshal::Copy(rgbValues, 0, ptr, bytes);
			chacheMap->UnlockBits(bmpData);
		}

		System::Void timer1_Tick(System::Object^  sender, System::EventArgs^  e) {
			// return;
			//TimeChronometer timer;

			//GC::Collect();
			//	try{

			redraw();

			//	}
			//	catch (Exception^ ex){
			//		Console::WriteLine(ex->ToString());
			//fix TODO overflow when value passed to positions on Graphics::DrawLine are too bigger ou to bigger at negative.
			//	}
			//GC::Collect();
		}

		System::Void pictureBox1_MouseHover(System::Object^  sender, System::EventArgs^  e) {
			pictureBox1->Focus();
		}

		System::Void pictureBox1_MouseWheel(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			pictureBox1->Focus();
			if (!e->Delta)
				return;
			if (e->Delta > 0)
				IncreaseZoom(1);
			else if (e->Delta < 0)
				DescreaseZoom(1);
			request_redraw(true);
		}

		System::Void pictureBox1_MouseDown(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e){
			last_mouse_pos_x = current_x;
			last_mouse_pos_y = current_y;
			last_mouse_down_point = e->Location;
			draging_pos = true;
			onCoordinateMouseDown(gcnew Coordinate(getInterfacePointAsCoordinate(PointF((float)e->Location.X, (float)e->Location.Y)), current_z), e);
		}

		System::Void pictureBox1_MouseUp(System::Object^  sender, System::Windows::Forms::MouseEventArgs^  e) {
			draging_pos = false;

			onCoordinateMouseUp(gcnew Coordinate(getInterfacePointAsCoordinate(PointF((float)e->Location.X, (float)e->Location.Y)), current_z), e);
			if (dragingWaypoint)
				dragingWaypoint = nullptr;
		}

		System::Void pictureBox1_MouseMove(System::Object^ sender, System::Windows::Forms::MouseEventArgs^  e){

			if (draging_pos && !dragingWaypoint){
				float factor_x = ((float)zoom_required_map_count / (float)pictureBox1->Width);
				float factor_y = ((float)zoom_required_map_count / (float)pictureBox1->Height);
				current_x = (int32_t)((((float)last_mouse_down_point.X - (float)e->X) * (float)factor_x) +
					(float)last_mouse_pos_x);
				current_y = (int32_t)((((float)last_mouse_down_point.Y - (float)e->Y) * (float)factor_y) +
					(float)last_mouse_pos_y);
				mapManager->update_current_pos(current_x, current_y, current_z);
			}

			float sqm_cost_each_pixel_x = zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Width : 0;
			float sqm_cost_each_pixel_y = zoom_required_map_count ? (float)zoom_required_map_count / (float)this->pictureBox1->Height : 0;

			PointF^ p = getInterfacePointAsCoordinate(PointF((float)e->X, (float)e->Y));
			set_to_draw("cursor", p);
			setCursorCoordinate(System::Drawing::PointF((float)(int)p->X, (float)(int)p->Y));
			onCoordinateMouseMove(gcnew Coordinate(p, current_z), e);

			need_update = true;
			must_update_cache = true;
		}

		System::Void waypointer_maker_KeyPress(System::Object^  sender, System::Windows::Forms::KeyPressEventArgs^  e) {
			if (e->KeyChar == '+'){
				current_z++;
			}
			else if (e->KeyChar == '-'){
				current_z--;
			}
		}

		System::Void pictureBox1_MouseEnter(System::Object^  sender, System::EventArgs^  e) {
			pictureBox1->Focus();
		}

		System::Void pictureBox1_Click(System::Object^  sender, System::EventArgs^  e) {
			System::Windows::Forms::MouseEventArgs^ _event_args = (System::Windows::Forms::MouseEventArgs^)e;

			onCoordinateMouseClick(gcnew Coordinate(getInterfacePointAsCoordinate(PointF((float)_event_args->Location.X, (float)_event_args->Location.Y)), current_z), e);
		}

		System::Void radButton1_Click(System::Object^  sender, System::EventArgs^  e) {

		}

		WaypointInfo^ findWaypointInfoNearToCoordinate(Coordinate^ coord, int max_radius){
			int last_dist = 1000000;
			WaypointInfo^ retval = nullptr;
			for each (WaypointInfo^ info in waypoints){
				int x_dist = Math::Abs((int)(info->x - coord->x));
				int y_dist = Math::Abs((int)(info->y - coord->y));
				if (x_dist + y_dist < last_dist){
					if ((info->x == coord->x && info->y == coord->y)
						|| isPointInCircle(coord->x, coord->y, max_radius, info->x, info->y)){
						last_dist = x_dist + y_dist;
						retval = info;
					}
				}
			}
			return retval;
		}

		System::Void setCursorCoordinate(PointF point){
			set_to_draw_image("cursor_img"
				, _cursor_image,
				point, System::Drawing::SizeF(40, 40));
		}

		System::Void onCursorCoordinateMove(Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args){

			if (dragingWaypoint){

				dragingWaypoint->x = coord->x;
				dragingWaypoint->y = coord->y;
				selectedWaypoint = dragingWaypoint;
				setCursorCoordinate(dragingWaypoint->getPointF());
				onWaypointMoved(dragingWaypoint);
			}
			else{
				WaypointInfo^ waypoint_hover = findWaypointInfoNearToCoordinate(coord, zoom_required_map_count / 100 * 3);
				if (waypoint_hover){
					if (hoveredWaypoint)
						onWaypointMouseOut(hoveredWaypoint);
					hoveredWaypoint = waypoint_hover;
					onWaypointMouseEnter(hoveredWaypoint);
				}
				else{
					if (hoveredWaypoint){
						onWaypointMouseOut(hoveredWaypoint);
						hoveredWaypoint = nullptr;
					}
				}
			}
		}

		System::Void onCursorCoordinateClick(Coordinate^ coord, System::EventArgs^ args){
			WaypointInfo^ waypoint_hover = findWaypointInfoNearToCoordinate(coord, zoom_required_map_count / 100 * 3);

		}

		System::Void onCursorCoordinateMouseDown(Coordinate^ coord, System::Windows::Forms::MouseEventArgs^ args){
			WaypointInfo^ waypoint_hover = findWaypointInfoNearToCoordinate(coord, zoom_required_map_count / 100 * 3);
			if (selectedWaypoint != waypoint_hover){
				selectedWaypoint = waypoint_hover;
			}
			dragingWaypoint = waypoint_hover;
		}
	};
}
