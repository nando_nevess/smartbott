#include <stdint.h>
#include <chrono>
#include <Windows.h>

class TimeChronometer{
	std::chrono::system_clock::time_point start_time;

public:
	TimeChronometer::TimeChronometer(){
		reset();
	}

	void TimeChronometer::reset(){
		start_time = std::chrono::high_resolution_clock::now();
	}

	int32_t TimeChronometer::elapsed_milliseconds(){
		return (int32_t)(std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::high_resolution_clock::now() - start_time)).count();
	}


	static TimeChronometer* get(){
		static TimeChronometer* mTimeChronometer = nullptr;
		if (!mTimeChronometer)
			mTimeChronometer = new TimeChronometer;
		return mTimeChronometer;
	}
};
using namespace System::Drawing;
using namespace System;
using namespace System::Collections::Generic;
namespace MiniMap{

	enum object_t : uint32_t{
		object_rectangle,
		object_axis
	};

	enum rect_t : uint32_t{
		rect_elipses,
		rect_rectangle
	};
	public ref class Line{
		PointF mstart;
		PointF mend;
		Pen^ mpen;
	public:
		Line() : mstart(0, 0), mend(0, 0), mpen(gcnew Pen(Color::White)){
		}
		Line(int x, int y, int x_end, int y_end, Pen^ _pen) : mpen(_pen){
			mstart = PointF((float)x, (float)y);
			mend = PointF((float)x_end, (float)y_end);
		}

		Line(PointF _start, PointF _end, Pen^ _pen) : mstart(_start), mend(_end), mpen(_pen){
		}

		property Pen^ pen{
			Pen^ get(){
				return mpen;
			}
			void set(Pen^ _pen){
				mpen = _pen;
			}
		}
		property PointF start{
			PointF get(){
				return mstart;
			}
			void set(PointF point){
				mstart = point;
			}
		}
		property PointF end{
			PointF get(){
				return mend;
			}
			void set(PointF point){
				mend = point;
			}
		}

		System::Drawing::RectangleF get_rect_area(){
			return System::Drawing::RectangleF(start.X, start.Y, end.X - start.X, end.Y - start.Y);
		}
	};

	public ref class Coordinate{
	public:
		Coordinate(uint32_t x, uint32_t y, uint32_t z){
			this->x = x;
			this->y = y;
			this->z = z;
		}

		Coordinate(System::Drawing::PointF^ point, uint32_t z){
			this->x = (uint32_t)point->X;
			this->y = (uint32_t)point->Y;
			this->z = z;
		}

		Coordinate(System::Drawing::Point^ point) : z(0){
			this->x = (uint32_t)point->X;
			this->y = (uint32_t)point->Y;
		}

		Coordinate(System::Drawing::PointF point, uint32_t z){
			this->x = (uint32_t)point.X;
			this->y = (uint32_t)point.Y;
			this->z = z;
		}

		Coordinate(System::Drawing::PointF point) : z(0){
			this->x = (uint32_t)point.X;
			this->y = (uint32_t)point.Y;
		}

		Coordinate(System::Drawing::Point point) : z(0){
			this->x = (uint32_t)point.X;
			this->y = (uint32_t)point.Y;
		}

		Coordinate(System::Drawing::Point point, uint32_t _z) : z(_z){
			this->x = (uint32_t)point.X;
			this->y = (uint32_t)point.Y;
		}

		PointF ToPointF(){
			return PointF((float)x, (float)y);
		}
		Point ToPoint()
		{
			return Point(x, y);
		}
		uint32_t x, y, z;
	};


	template <typename T, typename U>
	public ref class Pair{
		T^ mT;
		U^ mU;
	public:
		Pair() {
		}

		Pair(T^ first, U^ second) {
			this.First = first;
			this.Second = second;
		}


		property T^ First{
			T^ get(){
				return mT;
			}
			void set(T^ v){
				mT = v;
			}
		}


		property U^ Second{
			U^ get(){
				return mU;
			}
			void set(U^ v){
				mU = v;
			}
		}
	};




	static bool LineIntersectsLine(PointF^ l1p1, PointF^ l1p2, PointF^ l2p1, PointF^ l2p2)
	{
		float q = (l1p1->Y - l2p1->Y) * (l2p2->X - l2p1->X) - (l1p1->X - l2p1->X) * (l2p2->Y - l2p1->Y);
		float d = (l1p2->X - l1p1->X) * (l2p2->Y - l2p1->Y) - (l1p2->Y - l1p1->Y) * (l2p2->X - l2p1->X);

		if (d == 0)
		{
			return false;
		}

		float r = q / d;

		q = (l1p1->Y - l2p1->Y) * (l1p2->X - l1p1->X) - (l1p1->X - l2p1->X) * (l1p2->Y - l1p1->Y);
		float s = q / d;

		if (r < 0 || r > 1 || s < 0 || s > 1)
		{
			return false;
		}

		return true;
	}


	static bool LineIntersectsLine(Line^ l1, Line^ l2){
		return LineIntersectsLine(l1->start, l1->end, l2->start, l2->end);
	}
	static bool LineIntersectsRect(PointF p1, PointF p2, System::Drawing::RectangleF^ r)
	{
		return LineIntersectsLine(p1, p2, gcnew PointF(r->X, r->Y), gcnew PointF(r->X + r->Width, r->Y)) ||
			LineIntersectsLine(p1, p2, gcnew PointF(r->X + r->Width, r->Y), gcnew PointF(r->X + r->Width, r->Y + r->Height)) ||
			LineIntersectsLine(p1, p2, gcnew PointF(r->X + r->Width, r->Y + r->Height), gcnew PointF(r->X, r->Y + r->Height)) ||
			LineIntersectsLine(p1, p2, gcnew PointF(r->X, r->Y + r->Height), gcnew PointF(r->X, r->Y)) ||
			(r->Contains(p1) && r->Contains(p2));
	}

	static bool RectIntersectRect(System::Drawing::RectangleF^ rect, System::Drawing::RectangleF^ rectother){
		List<Line^> rect_lines;
		List<Line^> rect_other_lines;

		rect_lines.Add(gcnew Line(
			System::Drawing::PointF(rect->Left, rect->Top),
			System::Drawing::PointF(rect->Left + rect->Width, rect->Top), (System::Drawing::Pen^)nullptr));

		rect_lines.Add(gcnew Line(
			System::Drawing::PointF(rect->Left + rect->Width, rect->Top),
			System::Drawing::PointF(rect->Left + rect->Width, rect->Top + rect->Height), (System::Drawing::Pen^)nullptr));

		rect_lines.Add(gcnew Line(
			System::Drawing::PointF(rect->Left + rect->Width, rect->Top + rect->Height),
			System::Drawing::PointF(rect->Left, rect->Top + rect->Height), (System::Drawing::Pen^)nullptr));

		rect_lines.Add(gcnew Line(
			System::Drawing::PointF(rect->Left, rect->Top + rect->Height),
			System::Drawing::PointF(rect->Left, rect->Top), (System::Drawing::Pen^)nullptr));


		rect_other_lines.Add(gcnew Line(
			System::Drawing::PointF(rectother->Left, rectother->Top),
			System::Drawing::PointF(rectother->Left + rectother->Width, rectother->Top), (System::Drawing::Pen^)nullptr));

		rect_other_lines.Add(gcnew Line(
			System::Drawing::PointF(rectother->Left + rectother->Width, rectother->Top),
			System::Drawing::PointF(rectother->Left + rectother->Width, rectother->Top + rectother->Height), (System::Drawing::Pen^)nullptr));

		rect_other_lines.Add(gcnew Line(
			System::Drawing::PointF(rectother->Left + rectother->Width, rectother->Top + rectother->Height),
			System::Drawing::PointF(rectother->Left, rectother->Top + rectother->Height), (System::Drawing::Pen^)nullptr));

		rect_other_lines.Add(gcnew Line(
			System::Drawing::PointF(rectother->Left, rectother->Top + rectother->Height),
			System::Drawing::PointF(rectother->Left, rectother->Top), (System::Drawing::Pen^)nullptr));

		for each (Line^ lrect in rect_lines){
			for each(Line^ lrectother in rect_other_lines){
				if (LineIntersectsLine(lrect, lrectother))
					return true;
			}
		}
		return false;
	}

	public ref class ToDraw{
	public:
		Dictionary<uint32_t, uint32_t> optionals;
	};

	public ref class Axis : public ToDraw{
	public:
		Axis(PointF _point, SizeF _size, System::Drawing::Pen^ _pen){
			size = _size;
			point = _point;
			pen = _pen;
			optionals[object_t::object_axis] = 1;
		}

		System::Drawing::Pen^ pen;
		SizeF size;
		PointF point;

		MiniMap::Line^ get_horizontal_line(){
			int start_x = (int32_t)(point.X - (size.Width ? size.Width / 2 : 0));
			int start_y = (int32_t)point.Y;
			int end_x = (int32_t)(start_x + size.Width);
			int end_y = (int32_t)start_y;
			return gcnew Line(start_x, start_y, end_x, end_y,
				pen);
		}
		MiniMap::Line^ get_vertical_line(){
			int start_x = (int32_t)point.X;
			int start_y = (int32_t)(point.Y - (size.Height ? size.Height / 2 : 0));
			int end_x = (int32_t)start_x;
			int end_y = (int32_t)(start_y + size.Height);
			return gcnew Line(start_x, start_y, end_x, end_y,
				pen);
		}
	};

	public ref class DrawRectangle : public ToDraw{
	public:
		bool draw_border;
		uint32_t argb_fill;
		System::Drawing::RectangleF^ area;
		DrawRectangle(System::Drawing::RectangleF^ _area, bool _draw_border, uint32_t _fill_color){
			optionals[object_t::object_rectangle] = rect_t::rect_rectangle;
			area = _area;
			argb_fill = _fill_color;
			draw_border = _draw_border;
		}
		uint32_t border_width = 1;
		uint32_t z;

		PointF get_left_top(){
			return PointF(area->Left, area->Y);
		}

		PointF get_rigth_top(){
			return PointF(area->Right, area->Y);
		}

		PointF get_rigth_bottom(){
			return PointF(area->Right, area->Bottom);
		}

		PointF get_left_bottom(){
			return PointF(area->Left, area->Bottom);
		}

		KeyValuePair<PointF, PointF> get_top_line(){
			return KeyValuePair<PointF, PointF>(
				get_left_top(), get_rigth_top()
				);
		}

		KeyValuePair<PointF, PointF> get_rigth_line(){
			return KeyValuePair<PointF, PointF>(
				get_rigth_top(), get_rigth_bottom()
				);
		}

		KeyValuePair<PointF, PointF> get_bottom_line(){
			return KeyValuePair<PointF, PointF>(
				get_rigth_bottom(), get_left_bottom()
				);
		}

		KeyValuePair<PointF, PointF> get_left_line(){
			return KeyValuePair<PointF, PointF>(
				get_left_top(), get_left_bottom()
				);
		}

		LinkedList<KeyValuePair<PointF, PointF>>^ get_lines(){
			LinkedList<KeyValuePair<PointF, PointF>>^ retval = gcnew LinkedList<KeyValuePair<PointF, PointF>>();
			retval->AddLast(get_top_line());
			retval->AddLast(get_rigth_line());
			retval->AddLast(get_bottom_line());
			retval->AddLast(get_left_line());
			return retval;
		}
	};
	public ref class DrawElipse : public DrawRectangle{
	public:
		DrawElipse(System::Drawing::RectangleF^ _area, bool _draw_border, uint32_t _fill_color) :
			DrawRectangle(_area, _draw_border, _fill_color){
			optionals[object_t::object_rectangle] = rect_t::rect_elipses;
		}
	};


	public ref class ToDrawImage{
	public:
		ToDrawImage(Image^ _img, SizeF^ _size, PointF _coordinate) : img(_img), size(_size), coordinate(_coordinate){}
		Image^ img;
		System::Drawing::SizeF^ size;
		PointF coordinate;
	};


	public ref class ToDrawEffectPoint{
	public:
		ToDrawEffectPoint(Coordinate^ _coord,
			uint32_t _color, float _decay, uint32_t _radius) :
			coord(_coord), decay(_decay), radius(_radius), color(_color){
		}
		Coordinate^ coord;
		uint32_t color;
		float decay;
		uint32_t radius;
		System::Drawing::RectangleF^ get_area(){
			return gcnew System::Drawing::RectangleF((float)coord->x - (float)radius,
				(float)coord->y - (float)radius,
				(float)radius * 2.0f + 1.0f, (float)radius * 2.0f + 1.0f);
		}
		System::Drawing::RectangleF get_area_not_ref(){
			return System::Drawing::RectangleF((float)coord->x - (float)radius,
				(float)coord->y - (float)radius,
				(float)radius * 2.0f + 1.0f, (float)radius * 2.0f + 1.0f);
		}
	};

	ref class ToDrawLineEffect : public ToDraw{
	public:
		ToDrawLineEffect(Coordinate^ _start, Coordinate^ _end, uint32_t _color, uint32_t _width)
			: start_coord(_start), end_coord(_end), color(_color), width(_width){
		}

		Coordinate^ start_coord;
		Coordinate^ end_coord;
		uint32_t color;
		float decay;
		uint32_t width;
		uint32_t radius;
		Line^ get_line(){
			return gcnew Line(start_coord->x, start_coord->y, end_coord->x,
				end_coord->y, (System::Drawing::Pen^)nullptr);
		}
	};

	public ref class WaypointInfo{

		uint32_t _x;
		uint32_t _y;
		int8_t _z;

		String^ _label;
		uint32_t _type;
		String^ _identifier;
		String^ _identifier_next;
	public:

		property uint32_t x{
			
			uint32_t get(){
				return _x;
			}
			void set(uint32_t value){
				_x = value;
			}
		}

		property uint32_t y{
			uint32_t get(){
				return _y;
			}
			void set(uint32_t value){
				_y = value;
			}
		}

		property uint8_t z{
			uint8_t get(){
				return _z;
			}
			void set(uint8_t value){
				_z = value;
			}
		}

		property String^ label{
			String^ get(){
				return _label;
			}
			void set(String^ value){
				_label = value;
			}
		}

		property uint32_t type{
			uint32_t get(){
				return _type;
			}
			void set(uint32_t value){
				_type = value;
			}
		}
		property String^ identifier{
			String^ get(){
				return _identifier;
			}
			void set(String^ id){
				_identifier = id;
			}
		}
		
		property String^ identifier_next{
			String^ get(){
				return _identifier_next;
			}
			void set(String^ _identifier){
				_identifier_next = _identifier;
			}
		}

		PointF getPointF(){
			return PointF((float)x, (float)y);
		}

		Point getPoint(){
			return Point(x, y);
		}
		virtual bool Equals(Object^ other) override{
			if (other->GetType() != WaypointInfo::typeid)
				return false;
			WaypointInfo^ other_cast = (WaypointInfo^)other;
			if (!other)
				return false;
			if (other == this)
				return true;
			if (other_cast->identifier == this->_identifier)
				return true;
			return false;
		}
	};




	inline bool isInRectangle(double centerX, double centerY, double radius,
		double x, double y){
		return x >= centerX - radius && x <= centerX + radius &&
			y >= centerY - radius && y <= centerY + radius;
	}

	bool isPointInCircle(double centerX, double centerY,
		double radius, double x, double y){
		if (isInRectangle(centerX, centerY, radius, x, y)){
			double dx = centerX - x;
			double dy = centerY - y;
			dx *= dx;
			dy *= dy;
			double distanceSquared = dx + dy;
			double radiusSquared = radius * radius;
			return distanceSquared <= radiusSquared;
		}
		return false;
	}

	float getDistanceFromPoints(float x_1, float y_1, float x_2, float y_2){
		float diffY =y_1 - y_2;
		float diffX = x_1 - x_2;
		return sqrt((diffY * diffY) + (diffX * diffX));
	}
}

