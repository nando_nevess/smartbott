#pragma once
#include "Util.h"
#include "string"

struct SettingItem
{
private:
	std::string id;
	std::string type;
	std::string text;

public:
	GET_SET(id, std::string);
	GET_SET(std::string, type);
	GET_SET(std::string, text);
};

class SettingsManager
{
public:
	SettingsManager();
};

