#include <Windows.h>
#using <System.xml.dll>
#include <json\json.h>
#include <string>
#include <fstream>
#include <ostream>

using namespace System;
using namespace System::Collections;
using namespace System::Collections::Generic;
using namespace System::IO;
using namespace System::Xml;

#include <msclr\marshal_cppstd.h>

namespace managed_util{

	std::string fromSS(System::String^ in){
		std::string str = "";
		if (in != ""){
			using namespace System::Runtime::InteropServices;

			str = std::string((char*)(void*)Marshal::StringToHGlobalAnsi(in));
		}
		return str;
	}

	array<System::String^>^ convert_vector_to_array(std::vector<std::string> vct){
		array<String^>^ Arr = gcnew array<String^>(vct.size());

		for (uint32_t i = 0; i < vct.size(); i++){
			Arr[i] = gcnew String(vct[i].c_str());
		}
		return Arr;
	}

	std::string to_std_string(String^ in){

		return msclr::interop::marshal_as<std::string>(in);
	}
}


void loadDirectoriesAndFiles(DirectoryInfo^ dir, System::Collections::Generic::List<String^>^ out){
	for each(FileInfo^ f in dir->GetFiles()){
		if (f->Extension == ".xml"){
			out->Add(f->FullName);
		}
		Console::WriteLine(f->FullName);
	}

	for each(DirectoryInfo^ d in dir->GetDirectories()){
		Console::WriteLine(d->FullName);
		loadDirectoriesAndFiles(d, out);
	}
}

ref class Monster{
public:
	String^ name;
	System::Collections::Generic::List<String^> loots;
};

Monster^ getMonsterFromXmlFile(String^ path){
	XmlDocument^ xmlDoc = gcnew XmlDocument();
	Monster^ retval;
	try{
		xmlDoc->Load(path);
		auto element = xmlDoc["monster"];
		if (element){
			auto att_name = element->Attributes->GetNamedItem("name");
			auto att_loot = element["loot"];
			if (att_name && att_loot){
				String^ name = ((String^)att_name->Value);
				if (name){

					retval = gcnew Monster;
					retval->name = name; 
					Console::WriteLine(name);
					
					Console::WriteLine("Count:" + Convert::ToString(att_loot->ChildNodes->Count));
					for (int i = 0; i < att_loot->ChildNodes->Count; i++){

						if (att_loot->ChildNodes[i]->Attributes && att_loot->ChildNodes[i]->Attributes->GetNamedItem("id")){
							
							int may_int = 0;
							String^ nm = (String^)att_loot->ChildNodes[i]->Attributes["id"]->Value;
		
							if (System::Int32::TryParse(nm, may_int)){
								retval->loots.Add(Convert::ToString(may_int));
							}
						
							Console::WriteLine(nm);
						}
						
					}
					return retval;
				}
			}
		}

			//<monster name = "Salamander"
	}
	catch (...)
	{
		return nullptr;
	}
	
}



int main(){
	System::Collections::Generic::List<String^>^ files = gcnew System::Collections::Generic::List<String^>();
	System::Collections::Generic::List<Monster^>^ MonsterList = gcnew System::Collections::Generic::List<Monster^>();
	DirectoryInfo^ directory = gcnew DirectoryInfo(".\\");
	loadDirectoriesAndFiles(directory, files);


	
	for each(String^ xmlFIlePath in files){
		auto monster = getMonsterFromXmlFile(xmlFIlePath);
		if (monster){
			MonsterList->Add(monster);
		}
	}
	Console::WriteLine(Convert::ToString(MonsterList->Count));
	

	Json::Value JsonCreatures;
	for each (Monster^ monster in MonsterList){
		Json::Value creature;
		creature["name"] = managed_util::fromSS(monster->name);
		Json::Value& loot_list = creature["loot"];
		for each(String^ item_id in monster->loots){
			loot_list.append(managed_util::fromSS(item_id));
		}
		JsonCreatures.append(creature);
	}

	Json::Value Root;
	Root["MonsterManager"]["monsterList"] = JsonCreatures;
	std::ofstream jsonSet;

	jsonSet.open("filetest.json");
	if (!jsonSet.is_open())
		return false;

	jsonSet << Root.toStyledString();
	jsonSet.close();
	return true;
	/*
	{
   "MonsterManager" : {
      "monsterList" : [
         {
            "name" : "Training Monk"
         },
         {
            "name" : "Amazon"
         },
	*/
	/*String^ dirFiles = Directory::GetCurrentDirectory() + "\\stand by lua";

	if (!Directory::Exists(dirFiles))
		_mkdir("stand by lua");

	DirectoryInfo^ directory = gcnew DirectoryInfo(dirFiles);

	void LuaBackground::loadDirectoriesAndFiles(DirectoryInfo^ dir, Telerik::WinControls::UI::RadTreeNode^ parentNode){
		for each(FileInfo^ f in dir->GetFiles()){
			Telerik::WinControls::UI::RadTreeNode^ fileNode = gcnew Telerik::WinControls::UI::RadTreeNode(f->Name);
			fileNode->ImageIndex = 1;
			parentNode->Nodes->AddRange(fileNode);
		}

		for each(DirectoryInfo^ d in dir->GetDirectories()){
			Telerik::WinControls::UI::RadTreeNode^ dirNode = gcnew Telerik::WinControls::UI::RadTreeNode(d->Name);
			dirNode->ImageIndex = 0;
			parentNode->Nodes->AddRange(dirNode);
			loadDirectoriesAndFiles(d, dirNode);
		}
	}*/

	while (true){

	}
	return 0;

}